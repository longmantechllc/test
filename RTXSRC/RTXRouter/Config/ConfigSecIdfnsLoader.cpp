//
// Created by schen on 11/11/2020.
//

#include <string>
#include <memory>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <ctime>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include "auto_xerces_ptr.h"

#include "RTXUtils.h"
#include "Logger.h"
#include "LoaderBase.h"
#include "ConfigSecIdfnsBox.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigRouterLoader.h"
#include "ConfigGlobalLoader.h"
#include "ConfigUserLoader.h"
#include "ConfigUserLoader.h"

#include "Config.h"
#include "ConfigLoader.h"

ConfigSecIdfnsLoader::ConfigSecIdfnsLoader()
{
}
ConfigSecIdfnsLoader::ConfigSecIdfnsLoader(Logger* logger)
        :LoaderBase(logger)
{

}
ConfigSecIdfnsLoader::~ConfigSecIdfnsLoader()
{
    funcList.clear();
}

void ConfigSecIdfnsLoader::InitConfigSecIdfnsLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    auto  fptr1 = std::make_pair( "SecIdfn", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetSecIdfn(loader, node); } );
    funcList.insert(fptr1);
    auto  fptr2 = std::make_pair( "ListingExchange", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadListingExchange(loader, node); } );
    funcList.insert(fptr2);

    auto  fptr3 = std::make_pair( "RetailSWP", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadRetailSWP(loader, node); } );
    funcList.insert(fptr3);
    auto  fptr33 = std::make_pair( "SWP", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadSWP(loader, node); } );
    funcList.insert(fptr33);

//    auto  fptr4 = std::make_pair( "SWPSequentialRoutes", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddSWPSeqRoute(loader, node); } );
//    funcList.insert(fptr4);
//    auto  fptr5 = std::make_pair( "SWPParallelRoutes", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddSWPParalRoute(loader, node); } );
//    funcList.insert(fptr5);

    auto  fptr6 = std::make_pair( "Targeting", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadTarget(loader, node); } );
    funcList.insert(fptr6);
    auto  fptr7 = std::make_pair( "sequentialTargeting", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetSequentialTargeting(loader, node); } );
    funcList.insert(fptr7);
    auto  fptr8 = std::make_pair( "definedRoute", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddDefinedRoute(loader, node); } );
    funcList.insert(fptr8);
    auto  fptr9 = std::make_pair( "MDIDmapping", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddMDIDmapping(loader, node); } );
    funcList.insert(fptr9);

    auto  fptr10 = std::make_pair( "Posting", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadPost(loader, node); } );
    funcList.insert(fptr10);
    auto  fptr11 = std::make_pair( "rerouteFailure", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetRerouteFailure(loader, node); } );
    funcList.insert(fptr11);
    auto  fptr12 = std::make_pair( "quotePassover", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetQuotePassover(loader, node); } );
    funcList.insert(fptr12);
    auto  fptr13 = std::make_pair( "Routes", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadPostRoutes(loader, node); } );
    funcList.insert(fptr13);

    auto  fptr14 = std::make_pair( "MktOrderPrimaryOpenCheck", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetMktOrderPrimaryOpenCheck(loader, node); } );
    funcList.insert(fptr14);

    auto  fptr15 = std::make_pair( "orderParam", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadOrderParam(loader, node); } );
    funcList.insert(fptr15);
    auto  fptr16 = std::make_pair( "ListingExchangeVenue", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadListingExchangeVenue(loader, node); } );
    funcList.insert(fptr16);

    auto  fptr17 = std::make_pair( "MultiPosting", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadMultiPosting(loader, node); } );
    funcList.insert(fptr17);
    auto  fptr18 = std::make_pair( "multiRoutes", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadMultiRoutes(loader, node); } );
    funcList.insert(fptr18);
    auto  fptr19 = std::make_pair( "PreMarketListingExchangeVenue", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadPreMarketListingExchangeVenue(loader, node); } );
    funcList.insert(fptr19);

}

void ConfigSecIdfnsLoader::ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    InitConfigSecIdfnsLoader(loader, node);

    secIdObj = std::shared_ptr<SecIdfn>(new struct SecIdfn());
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(j) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
        {
            xercesc_3_2::XMLString::release(&nodeName) ;
            continue;
        }
        LoadExchange( loader, subNode);
        secIdObj.reset(new struct SecIdfn());

        xercesc_3_2::XMLString::release(&nodeName);
    }
    ValidateSecIdData(loader);
//    PrintExchanges(loader);
}

void ConfigSecIdfnsLoader::LoadExchange(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    std::string secId;
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }

        if(funcList.find(nodeName) != funcList.end())
        {
            if(strcmp(nodeName, "SecIdfn") == 0)
            {
                char *nodeText = xercesc_3_2::XMLString::transcode(subNode->getTextContent());
                secId = nodeText;
                xercesc_3_2::XMLString::release(&nodeText);
            }
            funcList[nodeName](loader,subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName);
    }
    loader->m_config->SecIdfnsConfig()->AddSecId(secId, secIdObj);
}

void ConfigSecIdfnsLoader::SetSecIdfn(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    secIdObj->secIdfn = nodeText;

    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigSecIdfnsLoader::SetMktOrderPrimaryOpenCheck(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    secIdObj->mktOrderPrimaryOpenCheck = RTXUtils::StrToBool(nodeText, true);

    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigSecIdfnsLoader::LoadListingExchange(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string venue = GetElement(node, "mdid") ;
    secIdObj->listingExchange = venue;
}

void ConfigSecIdfnsLoader::LoadListingExchangeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string mdid = GetElement(node, "venue") ;
    secIdObj->listingExchangeVenue = mdid;
}

void ConfigSecIdfnsLoader::LoadPreMarketListingExchangeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string mdid = GetElement(node, "venue") ;
    secIdObj->preMarketListingExchangeVenue = mdid;
}

void ConfigSecIdfnsLoader::LoadOrderParam(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        std::string value = GetElement(subNode, "value") ;
        std::string action = GetElement(subNode, "action") ;
        std::string venue = GetElement(subNode, "venue") ;

        secIdObj->AddOrderParam(nodeName, value, action, venue);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigSecIdfnsLoader::LoadRetailSWP(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string enabledStr = GetElement(node, "enabled");
    std::string SWPPriceCheckEnbaledStr = GetElement(node, "SWPPriceCheckEnbaled");

    secIdObj->retailSWP->enabled = RTXUtils::StrToBool(enabledStr, false);
    secIdObj->retailSWP->sWPPriceCheckEnbaled = RTXUtils::StrToBool(SWPPriceCheckEnbaledStr, true);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
//        if(funcList.find(nodeName) != funcList.end())
//        {
//            funcList[nodeName](loader,subNode);
//        }
        if(strcmp(nodeName, "SWPSequentialRoutes") == 0)
            AddSWPSeqRoute(loader,subNode, true);
        else if(strcmp(nodeName, "SWPParallelRoutes") == 0)
            AddSWPParalRoute(loader,subNode, true);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::LoadSWP(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string enabledStr = GetElement(node, "enabled");
    std::string SWPPriceCheckEnbaledStr = GetElement(node, "SWPPriceCheckEnbaled");

    secIdObj->sWP->enabled = RTXUtils::StrToBool(enabledStr, false);
    secIdObj->sWP->sWPPriceCheckEnbaled = RTXUtils::StrToBool(SWPPriceCheckEnbaledStr, true);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
//        if(funcList.find(nodeName) != funcList.end())
//        {
//            funcList[nodeName](loader,subNode);
//        }
        if(strcmp(nodeName, "SWPSequentialRoutes") == 0)
            AddSWPSeqRoute(loader,subNode);
        else if(strcmp(nodeName, "SWPParallelRoutes") == 0)
            AddSWPParalRoute(loader,subNode);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::AddSWPSeqRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node, bool isRetailSWP)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++) {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0) {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string name = GetElement(subNode, "name");
        if(isRetailSWP)
            secIdObj->retailSWP->AddSequentialRoute(name);
        else
            secIdObj->sWP->AddSequentialRoute(name);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::AddSWPParalRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node, bool isRetailSWP)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++) {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0) {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string name = GetElement(subNode, "name");
        std::string allocStr = GetElement(subNode, "allocationPercent");
        int allocationPercent = RTXUtils::StrToInt(allocStr, 100);
        if(isRetailSWP)
            secIdObj->retailSWP->AddParallelRoute(name, allocationPercent);
        else
            secIdObj->sWP->AddParallelRoute(name, allocationPercent);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::LoadTarget(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        if(funcList.find(nodeName) != funcList.end())
        {
            funcList[nodeName](loader,subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::SetSequentialTargeting(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    bool sequentialTargeting = RTXUtils::StrToBool(nodeText, false);
    secIdObj->targeting->sequentialTargeting = sequentialTargeting;

    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigSecIdfnsLoader::AddDefinedRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string name = GetElement(subNode, "name");
        secIdObj->targeting->AddDefinedRoute(name);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::AddMDIDmapping(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++) {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0) {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string mdid = GetElement(subNode, "mdid");
        std::string route = GetElement(subNode, "route");
        secIdObj->targeting->AddMDIDmapping(mdid, route);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::LoadPost(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string enabledStr = GetElement(node, "enabled");
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        if(funcList.find(nodeName) != funcList.end())
        {
            funcList[nodeName](loader,subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::SetRerouteFailure(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string counter = GetElement(node, "counter");
    std::string routes = GetElement(node, "routeList");
    secIdObj->posting->rerouteFailureCounter = RTXUtils::StrToInt(counter, 0);
    std::vector<std::string> list = RTXUtils::stringToVector(routes);
    for(auto rt : list)
    {
        secIdObj->posting->AddFailureRoute(rt);
    }
}

void ConfigSecIdfnsLoader::SetQuotePassover(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string modeStr = GetElement(node, "mode");
    std::string thresoldStr = GetElement(node, "thresold");
    std::string timerStr = GetElement(node, "timer");

    secIdObj->posting->quotePassoverMode = RTXUtils::StrToBool(modeStr, 0);
    secIdObj->posting->quotePassoverThresold = RTXUtils::StrToDouble(thresoldStr);
    secIdObj->posting->quotePassoverTimer = RTXUtils::StrToInt(timerStr);
}

void ConfigSecIdfnsLoader::LoadPostRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string routeName = GetElement(subNode, "name");
        secIdObj->posting->AddPostRoute(routeName);

        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::ValidateSecIdData(ConfigLoader* loader)
{
    for(auto& exch : loader->m_config->SecIdfnsConfig()->secIdfnList)
    {
        std::string mmid = "XXXX";
        if(exch.second->targeting->MDIDmapping.find(mmid) == exch.second->targeting->MDIDmapping.end())
            m_loggerSys->logErr("Passthrough mmid 'XXXX' is not configured.");

        auto itr = exch.second->targeting->MDIDmapping.begin();
        while( itr != exch.second->targeting->MDIDmapping.end())
        {
            if(exch.second->targeting->definedRoutes.find(itr->second) == exch.second->targeting->definedRoutes.end())
            {
                m_loggerSys->logErr("mmid:%s route:%s is not configured in 'definedRoute' section.", itr->first.c_str(), itr->second.c_str());
                itr = exch.second->targeting->MDIDmapping.erase(itr);
            }
            else
                ++itr;
        }

        auto cmp = [&](auto &q1, auto &q2) {
            if (q1->priority > q2->priority) return true;
            else return false;
        };
        if(!exch.second->multiPosting->multipostRoutes.empty())
            std::sort(exch.second->multiPosting->multipostRoutes.begin(), exch.second->multiPosting->multipostRoutes.end(), cmp);
    }
}

void ConfigSecIdfnsLoader::LoadMultiPosting(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string enabledStr = GetElement(node, "enabled");
    std::string cancelDelayStr = GetElement(node, "cancelDelay");
    secIdObj->multiPosting->enabled = RTXUtils::StrToBool(enabledStr, false);
    secIdObj->multiPosting->cancelDelay = RTXUtils::StrToInt(cancelDelayStr, 0);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        if(funcList.find(nodeName) != funcList.end())
        {
            funcList[nodeName](loader,subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::LoadMultiRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        std::string routeName = GetElement(subNode, "name");
        std::string allocationStr = GetElement(subNode, "allocation");
        std::string priorityStr = GetElement(subNode, "priority");
        int allocation = RTXUtils::StrToInt(allocationStr, 100);
        int priority = RTXUtils::StrToInt(priorityStr, 0);

        secIdObj->multiPosting->AddPostRoute(routeName, allocation, priority);
        xercesc_3_2::XMLString::release(&nodeName);
    }
}

void ConfigSecIdfnsLoader::PrintExchanges(ConfigLoader* loader)
{
    std::cout << std::endl << "Print Exchanges" << std::endl;
    std::cout << "Exchange list size: " << loader->m_config->SecIdfnsConfig()->secIdfnList.size() << std::endl;

    for(auto exch : loader->m_config->SecIdfnsConfig()->secIdfnList)
    {
        printf("Exchange Name: %s\n", exch.second->secIdfn.c_str());
        printf("List Exchange: %s\n", exch.second->listingExchange.c_str());
        printf("List Exchange Venue: %s\n", exch.second->listingExchangeVenue.c_str());
        std::cout << std::endl;

        std::cout << "retailSWP enabled: " << boolalpha << exch.second->retailSWP->enabled << std::endl;
        std::cout << "retailSWP sWPPriceCheckEnbaled: " << boolalpha << exch.second->retailSWP->sWPPriceCheckEnbaled << std::endl;
        for(auto rt : exch.second->retailSWP->sWPSequentialRoutes)
        {
            printf("retailSWP SequentialRoute name: %s\n", rt.c_str());
        }
        for(auto rt : exch.second->retailSWP->sWPParallelRoutes)
        {
            printf("retailSWP SequentialRoute name: %s\n", rt->name.c_str());
            printf("retailSWP SequentialRoute allocationPercent: %d\n", rt->allocationPercent);
        }
        std::cout << std::endl;

        std::cout << "SWP enabled: " << boolalpha << exch.second->sWP->enabled << std::endl;
        std::cout << "SWP sWPPriceCheckEnbaled: " << boolalpha << exch.second->sWP->sWPPriceCheckEnbaled << std::endl;
        for(auto rt : exch.second->sWP->sWPSequentialRoutes)
        {
            printf("SWP SequentialRoute name: %s\n", rt.c_str());
        }
        for(auto rt : exch.second->sWP->sWPParallelRoutes)
        {
            printf("SWP SequentialRoute name: %s\n", rt->name.c_str());
            printf("SWP SequentialRoute allocationPercent: %d\n", rt->allocationPercent);
        }
        std::cout << std::endl;

        std::cout << "Targeting sequentialTargeting: " << boolalpha << exch.second->targeting->sequentialTargeting << std::endl;
        for(auto rt : exch.second->targeting->definedRoutes)
        {
            printf("Targeting definedRoutes: %s\n", rt.c_str());
        }
        for(auto rt : exch.second->targeting->MDIDmapping)
        {
            printf("Targeting MDIDmapping mmid: %s, route: %s\n", rt.first.c_str(), rt.second.c_str());
        }
        std::cout << std::endl;

        printf("Posting rerouteFailure Counter: %d\n", exch.second->posting->rerouteFailureCounter);
        printf("Posting rerouteFailure RouteList size: %zu\n", exch.second->posting->postFailureRoutes.size());
        for(auto rt : exch.second->posting->postFailureRoutes)
        {
            printf("Posting rerouteFailure Route: %s\n", rt.c_str());
        }
        std::cout << "Posting quotePassover Mode: " << boolalpha << exch.second->posting->quotePassoverMode;
        printf(", Posting Thresold: %f, Timer: %d\n", exch.second->posting->quotePassoverThresold, exch.second->posting->quotePassoverTimer);
        for(auto rt : exch.second->posting->postRoutes)
        {
            printf("Posting postRoute: %s\n", rt.c_str());
        }
        std::cout << "MultiPosting enabled: " << boolalpha << exch.second->multiPosting->enabled << std::endl;
        std::cout << "MultiPosting cancelDelay: " << exch.second->multiPosting->cancelDelay << std::endl;
        for(auto rt : exch.second->multiPosting->multipostRoutes)
        {
            printf("MultiPosting Route name:%s, allocation:%d, priority:%d.\n", rt->name.c_str(), rt->allocation, rt->priority);
        }

        for(auto tp : exch.second->orderTifActionList ) {
            for (auto dest : tp.second->destinationList)
                std::cout << "<orderParam: Tif, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
        for(auto tp : exch.second->IPOActionList ) {
            for (auto dest : tp.second->destinationList)
                std::cout << "<orderParam: IPO, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
        std::cout << std::endl;
    }
}

