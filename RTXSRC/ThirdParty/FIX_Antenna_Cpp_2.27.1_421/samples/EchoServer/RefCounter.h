/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#ifndef H_B2BITS_Engine_Samples_RefCounter_H
#define H_B2BITS_Engine_Samples_RefCounter_H


#include <cstddef>

/// Reference counter auto pointer
template< typename T >
class RefCounter
{
public: // types
    /// Short name to RefCounter<T>
    typedef RefCounter<T> MyT;

private: // fields
    /// Pointer to the managed reference counter
    T* ptr_;

public: // methods
    /// Returns pointer to managed reference counter
    T* get() const {
        return this->ptr_;
    }

    /// Initializes manager with new reference counter
    void reset( T* ptr, bool increaseCounter = true ) {
        if( NULL != this->ptr_ ) {
            this->ptr_->release();
        }

        this->ptr_ = ptr;

        if( increaseCounter && NULL != this->ptr_ ) {
            this->ptr_->addRef();
        }
    }

public: // operators
    /// Implements operator->
    T* operator->() const {
        return this->get();
    }

    /// Implements operator&
    T& operator*() const {
        return *this->ptr_;
    }

    /// Compares two RefCounters
    bool operator==( MyT const& rv ) const {
        return this->ptr_ == rv.ptr_;
    }

    /// Compares two RefCounters
    bool operator!=( MyT const& rv ) const {
        return this->ptr_ != rv.ptr_;
    }

    /// Asign operator
    MyT& operator=( MyT const& src ) {
        this->reset( src.ptr_ );
        return *this;
    }

    /// Asign operator
    /// @note If you pass new created object, dont forget to call T::release method after operator= call.
    MyT& operator=( T* ptr ) {
        this->reset( ptr );
        return *this;
    }

public: // constructors
    /// Default constructor
    RefCounter()
        : ptr_( NULL ) {
    }

    /// Constructor. Initializes manager with reference counter.
    ///
    /// @param increaseCounter Pass true if your reference counter is created with RefCount == 1
    RefCounter( T* ptr, bool increaseCounter = true )
        : ptr_( NULL ) {
        this->reset( ptr, increaseCounter );
    }

    /// Copy constructor.
    RefCounter( MyT const& src )
        : ptr_( NULL ) {
        this->reset( src.ptr_ );
    }

    /// Destructor
    ~RefCounter() {
        this->reset( NULL );
    }
};

/// Compares RefCounter and pointer
template< typename T1, typename T2 >
inline bool operator ==( RefCounter<T1> const& lv, T2* rv )
{
    return lv.get() == rv;
}

/// Compares RefCounter and pointer
template< typename T1, typename T2 >
inline bool operator !=( RefCounter<T1> const& lv, T2* rv )
{
    return lv.get() != rv;
}

/// Compares RefCounter and pointer
template< typename T1, typename T2 >
inline bool operator ==( T1* lv, RefCounter<T2> const& rv )
{
    return lv == rv.get();
}

/// Compares RefCounter and pointer
template< typename T1, typename T2 >
inline bool operator !=( T1* lv, RefCounter<T2> const& rv )
{
    return lv != rv.get();
}

/// Compares RefCounter and pointer
template< typename T1 >
inline bool operator ==( RefCounter<T1> const& lv, int rv )
{
    assert( 0 == rv );
    return lv.get() == 0;
}

/// Compares RefCounter and pointer
template< typename T1 >
inline bool operator !=( RefCounter<T1> const& lv, int rv )
{
    assert( 0 == rv );
    return lv.get() != 0;
}

/// Compares RefCounter and pointer
template< typename T2 >
inline bool operator ==( int lv, RefCounter<T2> const& rv )
{
    assert( 0 == lv );
    return 0 == rv.get();
}

/// Compares RefCounter and pointer
template< typename T2 >
inline bool operator !=( int lv, RefCounter<T2> const& rv )
{
    assert( 0 == lv );
    return 0 != rv.get();
}

#endif // H_B2BITS_Engine_Samples_RefCounter_H
