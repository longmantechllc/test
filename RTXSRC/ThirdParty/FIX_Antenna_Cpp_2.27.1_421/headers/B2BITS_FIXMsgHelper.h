// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXMsgHelper.h
/// Contains Engine::FIXMsgHelper class declaration.

#ifndef __B2BITS_FIXMsgHelper_h__
#define __B2BITS_FIXMsgHelper_h__

#include "B2BITS_TagValue.h"
#include "B2BITS_FIXGroup.h"

namespace Engine {
    class FIXMsgHelper {
    public:
         /** Creates new entry in repeating group by leading field tag number.
         *
         * @param parent FIXMessage or FIXGroup entry, that contains target group.
         * @param leading_group_tag Tag number of the leading field value.
         *
         * @return Returns the newly created repeating group entry.
         *
         * @throw Utils::Exception if requested group not
         * defined this message type.
         */
        static TagValue* addEntryToNestedGroup(TagValue* parent, int leading_group_tag ) {
            int currentSize = parent->isEmpty(leading_group_tag) ? 0 : parent->getAsInt(leading_group_tag);
            parent->set(leading_group_tag, currentSize + 1);
            
            return parent->getGroup(leading_group_tag)->getEntry(currentSize);
        }
        static TagValue* addEntryToNestedGroup(TagValue& parent, int leading_group_tag ) {
            return addEntryToNestedGroup(&parent, leading_group_tag);
        }
        /** Creates new entry in repeating group by leading field tag number.
         *
         * @param parent FIXGroup, that contains target entry with target nested group.
         * @param leading_group_tag Tag number of the leading field value.
         * @param index Index of entry, that contains target nested group.
         *
         * @return Returns the newly created repeating group entry.
         *
         * @throw Utils::Exception if requested group not
         * defined this message type.
         */
        static TagValue* addEntryToNestedGroup(FIXGroup* parent, int leading_group_tag, int index ) {
            int currentSize = parent->isEmpty(leading_group_tag, index) ? 0 : parent->getAsInt(leading_group_tag, index);
            parent->set(leading_group_tag, currentSize + 1, index);
            
            return parent->getGroup(leading_group_tag, index)->getEntry(currentSize);
        }
        static TagValue* addEntryToNestedGroup(FIXGroup& parent, int leading_group_tag, int index ) {
            return addEntryToNestedGroup(&parent, leading_group_tag, index);
        }
    };
}


#endif // __B2BITS_FIXMsgHelper_h__