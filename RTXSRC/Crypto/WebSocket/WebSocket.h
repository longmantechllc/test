#pragma once


#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <iostream>
#include <string_view>
#include <map>
#include <vector>
#include <exception>

#include <libwebsockets.h>


namespace Crypto {

typedef int (*WS_CB)(std::string_view);

class WebSocket {
public:
	enum class Type : uint8_t {
		Regular,
		Huge
	};

private:
	static int event_cb( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len );
	static constexpr struct lws_protocols protocols[][2] = {
		{
			{
				"regular",
				event_cb,
				0,
				65536,
			},
			{ NULL, NULL, 0, 0 } /* terminator */
		},
		{
			{
				"huge",
				event_cb,
				0,
				65536*4,
			},
			{ NULL, NULL, 0, 0 } /* terminator */
		}
	};


	//std::map <struct lws *,WS_CB> handles ;

public:
	WebSocket(Type type);
	~WebSocket() {
		if (m_context) {
        	lws_context_destroy(m_context);
		}

    }
	//void connect_endpoint(const char *host, uint16_t port, const char *path, WS_CB cb);
	void enter_event_loop();
	struct lws_context *m_context {nullptr};
};

} // namespace Crypto