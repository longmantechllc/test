// <copyright> 
// 
// $Revision: 1.3 $ 
//  
// (c) B2BITS EPAM Systems Company 2000-2017. 
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS). 
//  
// This software is for the use of the paying client of B2BITS (which may be 
// a corporation, business area, business unit or single user) to whom it was 
// delivered (the "Licensee"). The use of this software is subject to 
// license terms. 
// 
// The Licensee acknowledges and agrees that the Software and Documentation 
// (the "Confidential Information") is confidential and proprietary to 
// the Licensor and the Licensee hereby agrees to use the Confidential 
// Information only as permitted by the full license  agreement between 
// the two parties, to maintain the confidentiality of the Confidential 
// Information and not to disclose the confidential information, or any part 
// thereof, to any other person, firm or corporation. The Licensee 
// acknowledges that disclosure of the Confidential Information may give rise 
// to an irreparable injury to the Licensor in-adequately compensable in 
// damages. Accordingly the Licensor may seek (without the posting of any 
// bond or other security) injunctive relief against the breach of the forgoing 
// undertaking of confidentiality and non-disclosure, in addition to any other 
// legal remedies which may be available, and the licensee consents to the 
// obtaining of such injunctive relief. All of the undertakings and 
// obligations relating to confidentiality and non-disclosure, whether 
// contained in this section or elsewhere in this agreement, shall survive 
// the termination or expiration of this agreement for a period of five (5) 
// years. 
// 
// The Licensor agrees that any information or data received from the Licensee 
// in connection with the performance of the support agreement relating to this 
// software shall be confidential, will be used only in connection with the 
// performance of the Licensor's obligations hereunder, and will not be 
// disclosed to third parties, including contractors, without the Licensor's 
// express permission in writing. 
// 
// Information regarding the software may be provided to the Licensee's outside 
// auditors and attorneys only to the extent required by their respective 
// functions. 
// 
// </copyright> 


#ifndef B2BITS_BATS_MESSAGES_H
#define B2BITS_BATS_MESSAGES_H

#include <cassert>
#include <algorithm>


#include <B2BITS_CompilerDefines.h>
#include <B2BITS_Decimal.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_String.h>
#include <B2BITS_BatsValueBlocks.h>


#define BATS_XML_PROTOCOL_VERSION "1.1"


const char BATS_PROTOCOL_VERSION_[] = BATS_XML_PROTOCOL_VERSION;

#ifndef BATS_MSG_FIELDS
#define BATS_MSG_FIELDS(d) d
#define BATS_MSG_F(t, n) t n;
#define BATS_MSG_C(t, n, s) t n[s];
#endif

namespace Bats {

/// @cond skipit
#ifdef B2BITS_FA_BIG_ENDIAN
System::u16 b2b_bats_leu16(System::u16 val);
System::u32 b2b_bats_leu32(System::u32 val);
System::u64 b2b_bats_leu64(System::u64 val);
#else
#define b2b_bats_leu16(val) (val)
#define b2b_bats_leu32(val) (val)
#define b2b_bats_leu64(val) (val)
#endif


static const System::i32 LongPriceDenominator = -4;
static const System::i32 ShortPriceDenominator = -2;
static const unsigned ShortPriceNormalizer = 100; // (10^-(LongPriceDenominator-ShortPriceDenominator))


B2B_BEGIN_PACK_1

template <bool> struct _assert;
template <> struct _assert<true>{ enum {value = 1}; };

template <typename _T>
struct _size_static_assert
{
    enum {value = _assert<(sizeof(_T) == _T::MessageLength)>::value};
};

#define bats_size_static_assert(T) ~T() { enum {size_valid = _size_static_assert<T>::value}; }

/// @endcond

/** @addtogroup bats_mds_pitch_msgs
* @{
*/

class SequencedUnitHeader
{
    System::u16     HdrLength;
    System::u8      HdrCount;
    System::u8      HdrUnit;
    System::u32     HdrSequence;
public:
    bats_size_static_assert(SequencedUnitHeader)
    enum { MessageLength = 8 };

    void setHdrLength( System::u16 val ) throw() { HdrLength = b2b_bats_leu16(val ); }
    System::u16 getHdrLength() const throw() { return b2b_bats_leu16(HdrLength); }

    void setHdrCount( System::u8 val ) throw() { HdrCount = val;}
    System::u8 getHdrCount() const throw() { return HdrCount; }

    void setHdrUnit( System::u8 val ) throw() { HdrUnit = val;}
    System::u8 getHdrUnit() const throw() { return HdrUnit; }

    void setHdrSequence( System::u32 val ) throw() { HdrSequence = b2b_bats_leu32(val ); }
    System::u32 getHdrSequence() const throw() { return b2b_bats_leu32(HdrSequence); }
};

class PitchMsg
{
    System::u8      Length;
    System::u8      MessageType;
public:
    PitchMsg(System::u8 length, System::u8 type) : Length(length), MessageType(type) {}

    System::u8 getLength() const throw() { return Length; }
    System::u8 getMessageType() const throw() { return MessageType; }
};

class TimedPitchMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     TimeOffset)
    )
public:
    TimedPitchMsg(System::u8 length, System::u8 type) : PitchMsg(length, type) {}

    void setTimeOffset( System::u32 val ) throw() { TimeOffset = b2b_bats_leu32(val ); }
    System::u32 getTimeOffset() const throw() { return b2b_bats_leu32(TimeOffset); }
};

class GRPLoginMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     SessionSubId, 4)
    BATS_MSG_C(char       ,     Username, 4)
    BATS_MSG_C(char       ,     Filler, 2)
    BATS_MSG_C(char       ,     Password, 10)
    )
public:
    bats_size_static_assert(GRPLoginMsg)
    enum { MessageLength = 22, MessageType = 0x01 };

    GRPLoginMsg () : PitchMsg( MessageLength, MessageType ) 
	{
		memset( SessionSubId, 0, sizeof( SessionSubId ) );
		memset( Username, 0, sizeof( Username ) );
		memset( Filler, 0, sizeof( Filler ) );
		memset( Password, 0, sizeof( Password ) );
	}

    void setSessionSubId( const Engine::AsciiString&  val ) throw() { assert(sizeof(SessionSubId) == val.size()); memcpy(SessionSubId, val.data(), sizeof(SessionSubId) ); }
    Engine::AsciiString getSessionSubId() const throw() { return Engine::AsciiString(SessionSubId, sizeof(SessionSubId) ); }

    void setUsername( const Engine::AsciiString&  val ) throw() { assert(sizeof(Username) == val.size()); memcpy(Username, val.data(), sizeof(Username) ); }
    Engine::AsciiString getUsername() const throw() { return Engine::AsciiString(Username, sizeof(Username) ); }

    void setFiller( const Engine::AsciiString&  val ) throw() { assert(sizeof(Filler) == val.size()); memcpy(Filler, val.data(), sizeof(Filler) ); }
    Engine::AsciiString getFiller() const throw() { return Engine::AsciiString(Filler, sizeof(Filler) ); }

    void setPassword( const Engine::AsciiString&  val ) throw() { assert(sizeof(Password) == val.size()); memcpy(Password, val.data(), sizeof(Password) ); }
    Engine::AsciiString getPassword() const throw() { return Engine::AsciiString(Password, sizeof(Password) ); }
};

class LoginResponseMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(char       ,     Status)
    )
public:
    bats_size_static_assert(LoginResponseMsg)
    enum { MessageLength = 3, MessageType = 0x02 };

    LoginResponseMsg () : PitchMsg( MessageLength, MessageType ),  Status( 0 ) {}

    void setStatus( char        val ) throw() {  assert( std::find( &GRPLoginStatusCodes[0], &GRPLoginStatusCodes[4], val) != &GRPLoginStatusCodes[4] ); Status = val;}
    char        getStatus() const throw() { return Status; }
};

class GapRequestMessageMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u8 ,     Unit)
    BATS_MSG_F(System::u32,     Sequence)
    BATS_MSG_F(System::u16,     Count)
    )
public:
    bats_size_static_assert(GapRequestMessageMsg)
    enum { MessageLength = 9, MessageType = 0x03 };

    GapRequestMessageMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setUnit( System::u8  val ) throw() { Unit = val;}
    System::u8  getUnit() const throw() { return Unit; }

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }

    void setCount( System::u16 val ) throw() { Count = b2b_bats_leu16( val ); }
    System::u16 getCount() const throw() { return b2b_bats_leu16( Count); }
};

class GapResponseMessageMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u8 ,     Unit)
    BATS_MSG_F(System::u32,     Sequence)
    BATS_MSG_F(System::u16,     Count)
    BATS_MSG_F(char       ,     Status)
    )
public:
    bats_size_static_assert(GapResponseMessageMsg)
    enum { MessageLength = 10, MessageType = 0x04 };

    GapResponseMessageMsg () : PitchMsg( MessageLength, MessageType ), Status( 0 ) {}

    void setUnit( System::u8  val ) throw() { Unit = val;}
    System::u8  getUnit() const throw() { return Unit; }

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }

    void setCount( System::u16 val ) throw() { Count = b2b_bats_leu16( val ); }
    System::u16 getCount() const throw() { return b2b_bats_leu16( Count); }

    void setStatus( char        val ) throw() {  assert( std::find( &GapResponseStatus[0], &GapResponseStatus[8], val) != &GapResponseStatus[8] ); Status = val;}
    char        getStatus() const throw() { return Status; }
};

class TimeMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Time)
    )
public:
    bats_size_static_assert(TimeMsg)
    enum { MessageLength = 6, MessageType = 0x20 };

    TimeMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setTime( System::u32 val ) throw() { Time = b2b_bats_leu32( val ); }
    System::u32 getTime() const throw() { return b2b_bats_leu32( Time); }
};

class AddOrderLongMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u32,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 6)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(char       ,     AddFlags)
    )
public:
    bats_size_static_assert(AddOrderLongMsg)
    enum { MessageLength = 34, MessageType = 0x21 };

	AddOrderLongMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 ), AddFlags( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() {  assert( std::find( &SideIndicatorCodes[0], &SideIndicatorCodes[2], val) != &SideIndicatorCodes[2] ); SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u32 val ) throw() { Quantity = b2b_bats_leu32( val ); }
    System::u32 getQuantity() const throw() { return b2b_bats_leu32( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }

    void setAddFlags( char        val ) throw() { AddFlags = val;}
    char        getAddFlags() const throw() { return AddFlags; }
};

class AddOrderShortMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u16,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 6)
    BATS_MSG_F(System::u16,     Price)
    BATS_MSG_F(char       ,     AddFlags)
    )
public:
    bats_size_static_assert(AddOrderShortMsg)
    enum { MessageLength = 26, MessageType = 0x22 };

    AddOrderShortMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 ), AddFlags( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() {  assert( std::find( &SideIndicatorCodes[0], &SideIndicatorCodes[2], val) != &SideIndicatorCodes[2] ); SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u16 val ) throw() { Quantity = b2b_bats_leu16( val ); }
    System::u16 getQuantity() const throw() { return b2b_bats_leu16( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu16( static_cast<System::u16>(price.changeExponent(ShortPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu16(Price), ShortPriceDenominator); }

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price) * ShortPriceNormalizer; }

    void setAddFlags( char        val ) throw() { AddFlags = val;}
    char        getAddFlags() const throw() { return AddFlags; }
};

class OrderExecutedMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u32,     ExecutedQuantity)
    BATS_MSG_F(System::u64,     ExecutionId)
    )
public:
    bats_size_static_assert(OrderExecutedMsg)
    enum { MessageLength = 26, MessageType = 0x23 };

    OrderExecutedMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setExecutedQuantity( System::u32 val ) throw() { ExecutedQuantity = b2b_bats_leu32( val ); }
    System::u32 getExecutedQuantity() const throw() { return b2b_bats_leu32( ExecutedQuantity); }

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }
};

class OrderExecutedAtPriceSizeMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u32,     ExecutedQuantity)
    BATS_MSG_F(System::u32,     RemainingQuantity)
    BATS_MSG_F(System::u64,     ExecutionId)
    BATS_MSG_F(System::u64,     Price)
    )
public:
    bats_size_static_assert(OrderExecutedAtPriceSizeMsg)
    enum { MessageLength = 38, MessageType = 0x24 };

    OrderExecutedAtPriceSizeMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setExecutedQuantity( System::u32 val ) throw() { ExecutedQuantity = b2b_bats_leu32( val ); }
    System::u32 getExecutedQuantity() const throw() { return b2b_bats_leu32( ExecutedQuantity); }

    void setRemainingQuantity( System::u32 val ) throw() { RemainingQuantity = b2b_bats_leu32( val ); }
    System::u32 getRemainingQuantity() const throw() { return b2b_bats_leu32( RemainingQuantity); }

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }
};

class ReduceSizeLongMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u32,     CanceledQuantity)
    )
public:
    bats_size_static_assert(ReduceSizeLongMsg)
    enum { MessageLength = 18, MessageType = 0x25 };

    ReduceSizeLongMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setCanceledQuantity( System::u32 val ) throw() { CanceledQuantity = b2b_bats_leu32( val ); }
    System::u32 getCanceledQuantity() const throw() { return b2b_bats_leu32( CanceledQuantity); }
};

class ReduceSizeShortMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u16,     CanceledQuantity)
    )
public:
    bats_size_static_assert(ReduceSizeShortMsg)
    enum { MessageLength = 16, MessageType = 0x26 };

    ReduceSizeShortMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setCanceledQuantity( System::u16 val ) throw() { CanceledQuantity = b2b_bats_leu16( val ); }
    System::u16 getCanceledQuantity() const throw() { return b2b_bats_leu16( CanceledQuantity); }
};

class ModifyLongMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u32,     Quantity)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(char       ,     ModifyFlags)
    )
public:
    bats_size_static_assert(ModifyLongMsg)
    enum { MessageLength = 27, MessageType = 0x27 };

    ModifyLongMsg () : TimedPitchMsg( MessageLength, MessageType ), ModifyFlags( 0 ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setQuantity( System::u32 val ) throw() { Quantity = b2b_bats_leu32( val ); }
    System::u32 getQuantity() const throw() { return b2b_bats_leu32( Quantity); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }

    void setModifyFlags( char        val ) throw() { ModifyFlags = val;}
    char        getModifyFlags() const throw() { return ModifyFlags; }
};

class ModifyShortMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(System::u16,     Quantity)
    BATS_MSG_F(System::u16,     Price)
    BATS_MSG_F(char       ,     ModifyFlags)
    )
public:
    bats_size_static_assert(ModifyShortMsg)
    enum { MessageLength = 19, MessageType = 0x28 };

    ModifyShortMsg () : TimedPitchMsg( MessageLength, MessageType ), ModifyFlags( 0 ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setQuantity( System::u16 val ) throw() { Quantity = b2b_bats_leu16( val ); }
    System::u16 getQuantity() const throw() { return b2b_bats_leu16( Quantity); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu16( static_cast<System::u16>(price.changeExponent(ShortPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu16(Price), ShortPriceDenominator); }

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price) * ShortPriceNormalizer; }

    void setModifyFlags( char        val ) throw() { ModifyFlags = val;}
    char        getModifyFlags() const throw() { return ModifyFlags; }
};

class DeleteMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    )
public:
    bats_size_static_assert(DeleteMsg)
    enum { MessageLength = 14, MessageType = 0x29 };

    DeleteMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }
};

class TradeLongMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u32,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 6)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(System::u64,     ExecutionId)
    )
public:
    bats_size_static_assert(TradeLongMsg)
    enum { MessageLength = 41, MessageType = 0x2a };

    TradeLongMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() { SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u32 val ) throw() { Quantity = b2b_bats_leu32( val ); }
    System::u32 getQuantity() const throw() { return b2b_bats_leu32( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }
};

class TradeShortMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u16,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 6)
    BATS_MSG_F(System::u16,     Price)
    BATS_MSG_F(System::u64,     ExecutionId)
    )
public:
    bats_size_static_assert(TradeShortMsg)
    enum { MessageLength = 33, MessageType = 0x2b };

    TradeShortMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 ) 
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() { SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u16 val ) throw() { Quantity = b2b_bats_leu16( val ); }
    System::u16 getQuantity() const throw() { return b2b_bats_leu16( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu16( static_cast<System::u16>(price.changeExponent(ShortPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu16(Price), ShortPriceDenominator); }

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price) * ShortPriceNormalizer; }

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }
};

class TradeBreakMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     ExecutionId)
    )
public:
    bats_size_static_assert(TradeBreakMsg)
    enum { MessageLength = 14, MessageType = 0x2c };

    TradeBreakMsg () : TimedPitchMsg( MessageLength, MessageType ) {}

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }
};

class EndOfSessionMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Timestamp)
    )
public:
    bats_size_static_assert(EndOfSessionMsg)
    enum { MessageLength = 6, MessageType = 0x2d };

    EndOfSessionMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setTimestamp( System::u32 val ) throw() { Timestamp = b2b_bats_leu32( val ); }
    System::u32 getTimestamp() const throw() { return b2b_bats_leu32( Timestamp); }
};

class SymbolMappingMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     FeedSymbol, 6)
    BATS_MSG_C(char       ,     OSISymbol, 21)
    BATS_MSG_F(char       ,     SymbolCondition)
    )
public:
    bats_size_static_assert(SymbolMappingMsg)
    enum { MessageLength = 30, MessageType = 0x2e };

    SymbolMappingMsg () : PitchMsg( MessageLength, MessageType ), SymbolCondition( 0 )
	{
		memset( FeedSymbol, 0, sizeof( FeedSymbol ) );
		memset( OSISymbol, 0, sizeof( OSISymbol ) );
	}

    void setFeedSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(FeedSymbol) == val.size()); memcpy(FeedSymbol, val.data(), sizeof(FeedSymbol) ); }
    Engine::AsciiString getFeedSymbol() const throw() { return Engine::AsciiString(FeedSymbol, sizeof(FeedSymbol) ); }

    void setOSISymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(OSISymbol) == val.size()); memcpy(OSISymbol, val.data(), sizeof(OSISymbol) ); }
    Engine::AsciiString getOSISymbol() const throw() { return Engine::AsciiString(OSISymbol, sizeof(OSISymbol) ); }

    void setSymbolCondition( char val ) throw() {  assert( std::find( &SymbolConditionCodes[0], &SymbolConditionCodes[2], val) != &SymbolConditionCodes[2] ); SymbolCondition = val;}
    char getSymbolCondition() const throw() { return SymbolCondition; }
};

class AddOrderExpandedMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u32,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 8)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(char       ,     AddFlags)
    BATS_MSG_C(char       ,     ParticipantID, 4)
    )
public:
    bats_size_static_assert(AddOrderExpandedMsg)
    enum { MessageLength = 40, MessageType = 0x2f };

    AddOrderExpandedMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 ), AddFlags( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
		memset( ParticipantID, 0, sizeof( ParticipantID ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() {  assert( std::find( &SideIndicatorCodes[0], &SideIndicatorCodes[2], val) != &SideIndicatorCodes[2] ); SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u32 val ) throw() { Quantity = b2b_bats_leu32( val ); }
    System::u32 getQuantity() const throw() { return b2b_bats_leu32( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }

    void setAddFlags( char        val ) throw() { AddFlags = val;}
    char        getAddFlags() const throw() { return AddFlags; }

    void setParticipantID( const Engine::AsciiString&  val ) throw() { assert(sizeof(ParticipantID) == val.size()); memcpy(ParticipantID, val.data(), sizeof(ParticipantID) ); }
    Engine::AsciiString getParticipantID() const throw() { return Engine::AsciiString(ParticipantID, sizeof(ParticipantID) ); }
};

class TradeExpandedMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u64,     OrderId)
    BATS_MSG_F(char       ,     SideIndicator)
    BATS_MSG_F(System::u32,     Quantity)
    BATS_MSG_C(char       ,     Symbol, 8)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(System::u64,     ExecutionId)
    )
public:
    bats_size_static_assert(TradeExpandedMsg)
    enum { MessageLength = 43, MessageType = 0x30 };

    TradeExpandedMsg () : TimedPitchMsg( MessageLength, MessageType ), SideIndicator( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setOrderId( System::u64 val ) throw() { OrderId = b2b_bats_leu64( val ); }
    System::u64 getOrderId() const throw() { return b2b_bats_leu64( OrderId); }

    void setSideIndicator( char        val ) throw() { SideIndicator = val;}
    char        getSideIndicator() const throw() { return SideIndicator; }

    void setQuantity( System::u32 val ) throw() { Quantity = b2b_bats_leu32( val ); }
    System::u32 getQuantity() const throw() { return b2b_bats_leu32( Quantity); }

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setPrice( const Engine::Decimal& price ) throw() { Price = b2b_bats_leu64( static_cast<System::u64>(price.changeExponent(LongPriceDenominator).getMantissa()) ); }
    Engine::Decimal getPrice() const throw() { return Engine::Decimal(b2b_bats_leu64(Price), LongPriceDenominator);}

    System::u64 getPrice64() const throw() { return b2b_bats_leu64(Price); }

    void setExecutionId( System::u64 val ) throw() { ExecutionId = b2b_bats_leu64( val ); }
    System::u64 getExecutionId() const throw() { return b2b_bats_leu64( ExecutionId); }
};

class TradingStatusMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     Symbol, 8)
    BATS_MSG_F(char       ,     HaltStatus)
    BATS_MSG_F(char       ,     RegSHOAction)
    BATS_MSG_F(char       ,     Reserved1)
    BATS_MSG_F(char       ,     Reserved2)
    )
public:
    bats_size_static_assert(TradingStatusMsg)
    enum { MessageLength = 18, MessageType = 0x31 };

    TradingStatusMsg () : TimedPitchMsg( MessageLength, MessageType ), HaltStatus( 0 ), RegSHOAction( 0 ), Reserved1( 0 ), Reserved2( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setHaltStatus( char        val ) throw() {  assert( std::find( &HaltStatusCodes[0], &HaltStatusCodes[3], val) != &HaltStatusCodes[3] ); HaltStatus = val;}
    char        getHaltStatus() const throw() { return HaltStatus; }

    void setRegSHOAction( char        val ) throw() {  assert( std::find( &RegSHOActionCodes[0], &RegSHOActionCodes[2], val) != &RegSHOActionCodes[2] ); RegSHOAction = val;}
    char        getRegSHOAction() const throw() { return RegSHOAction; }

    void setReserved1( char        val ) throw() { Reserved1 = val;}
    char        getReserved1() const throw() { return Reserved1; }

    void setReserved2( char        val ) throw() { Reserved2 = val;}
    char        getReserved2() const throw() { return Reserved2; }
};

class SpinImageAvailableMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Sequence)
    )
public:
    bats_size_static_assert(SpinImageAvailableMsg)
    enum { MessageLength = 6, MessageType = 0x80 };

    SpinImageAvailableMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }
};

class SpinRequestMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Sequence)
    )
public:
    bats_size_static_assert(SpinRequestMsg)
    enum { MessageLength = 6, MessageType = 0x81 };

    SpinRequestMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }
};

class SpinResponseMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Sequence)
    BATS_MSG_F(System::u32,     OrderCount)
    BATS_MSG_F(char       ,     Status)
    )
public:
    bats_size_static_assert(SpinResponseMsg)
    enum { MessageLength = 11, MessageType = 0x82 };

    SpinResponseMsg () : PitchMsg( MessageLength, MessageType ), Status( 0 ) {}

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }

    void setOrderCount( System::u32 val ) throw() { OrderCount = b2b_bats_leu32( val ); }
    System::u32 getOrderCount() const throw() { return b2b_bats_leu32( OrderCount); }

    void setStatus( char        val ) throw() {  assert( std::find( &SpinResponseStatusCodes[0], &SpinResponseStatusCodes[3], val) != &SpinResponseStatusCodes[3] ); Status = val;}
    char        getStatus() const throw() { return Status; }
};

class SpinFinishedMsg : public PitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_F(System::u32,     Sequence)
    )
public:
    bats_size_static_assert(SpinFinishedMsg)
    enum { MessageLength = 6, MessageType = 0x83 };

    SpinFinishedMsg () : PitchMsg( MessageLength, MessageType ) {}

    void setSequence( System::u32 val ) throw() { Sequence = b2b_bats_leu32( val ); }
    System::u32 getSequence() const throw() { return b2b_bats_leu32( Sequence); }
};

class AuctionUpdateMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     StockSymbol, 8)
    BATS_MSG_F(char       ,     AuctionType)
    BATS_MSG_F(System::u64,     ReferencePrice)
    BATS_MSG_F(System::u32,     BuyShares)
    BATS_MSG_F(System::u32,     SellShares)
    BATS_MSG_F(System::u64,     IndicativePrice)
    BATS_MSG_F(System::u64,     AuctionOnlyPrice)
    )
public:
    bats_size_static_assert(AuctionUpdateMsg)
    enum { MessageLength = 47, MessageType = 0x95 };

    AuctionUpdateMsg () : TimedPitchMsg( MessageLength, MessageType ), AuctionType( 0 )
	{
		memset( StockSymbol, 0, sizeof( StockSymbol ) );
	}

    void setStockSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(StockSymbol) == val.size()); memcpy(StockSymbol, val.data(), sizeof(StockSymbol) ); }
    Engine::AsciiString getStockSymbol() const throw() { return Engine::AsciiString(StockSymbol, sizeof(StockSymbol) ); }

    void setAuctionType( char        val ) throw() {  assert( std::find( &AuctionTypeCodes[0], &AuctionTypeCodes[4], val) != &AuctionTypeCodes[4] ); AuctionType = val;}
    char        getAuctionType() const throw() { return AuctionType; }

    void setReferencePrice( System::u64 val ) throw() { ReferencePrice = b2b_bats_leu64( val ); }
    System::u64 getReferencePrice() const throw() { return b2b_bats_leu64( ReferencePrice); }

    void setBuyShares( System::u32 val ) throw() { BuyShares = b2b_bats_leu32( val ); }
    System::u32 getBuyShares() const throw() { return b2b_bats_leu32( BuyShares); }

    void setSellShares( System::u32 val ) throw() { SellShares = b2b_bats_leu32( val ); }
    System::u32 getSellShares() const throw() { return b2b_bats_leu32( SellShares); }

    void setIndicativePrice( System::u64 val ) throw() { IndicativePrice = b2b_bats_leu64( val ); }
    System::u64 getIndicativePrice() const throw() { return b2b_bats_leu64( IndicativePrice); }

    void setAuctionOnlyPrice( System::u64 val ) throw() { AuctionOnlyPrice = b2b_bats_leu64( val ); }
    System::u64 getAuctionOnlyPrice() const throw() { return b2b_bats_leu64( AuctionOnlyPrice); }
};

class AuctionSummaryMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     StockSymbol, 8)
    BATS_MSG_F(char       ,     AuctionType)
    BATS_MSG_F(System::u64,     Price)
    BATS_MSG_F(System::u32,     Shares)
    )
public:
    bats_size_static_assert(AuctionSummaryMsg)
    enum { MessageLength = 27, MessageType = 0x96 };

    AuctionSummaryMsg () : TimedPitchMsg( MessageLength, MessageType ), AuctionType( 0 )
	{
		memset( StockSymbol, 0, sizeof( StockSymbol ) );
	}

    void setStockSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(StockSymbol) == val.size()); memcpy(StockSymbol, val.data(), sizeof(StockSymbol) ); }
    Engine::AsciiString getStockSymbol() const throw() { return Engine::AsciiString(StockSymbol, sizeof(StockSymbol) ); }

    void setAuctionType( char        val ) throw() {  assert( std::find( &AuctionTypeCodes[0], &AuctionTypeCodes[4], val) != &AuctionTypeCodes[4] ); AuctionType = val;}
    char        getAuctionType() const throw() { return AuctionType; }

    void setPrice( System::u64 val ) throw() { Price = b2b_bats_leu64( val ); }
    System::u64 getPrice() const throw() { return b2b_bats_leu64( Price); }

    void setShares( System::u32 val ) throw() { Shares = b2b_bats_leu32( val ); }
    System::u32 getShares() const throw() { return b2b_bats_leu32( Shares); }
};

class UnitClearMsg : public TimedPitchMsg
{
public:
    bats_size_static_assert(UnitClearMsg)
    enum { MessageLength = 6, MessageType = 0x97 };

    UnitClearMsg () : TimedPitchMsg( MessageLength, MessageType ) {}
};

class RetailPriceImprovementMsg : public TimedPitchMsg
{
BATS_MSG_FIELDS(
    BATS_MSG_C(char       ,     Symbol, 8)
    BATS_MSG_F(char       ,     RetailPriceImprovement)
    )
public:
    bats_size_static_assert(RetailPriceImprovementMsg)
    enum { MessageLength = 15, MessageType = 0x98 };

    RetailPriceImprovementMsg () : TimedPitchMsg( MessageLength, MessageType ), RetailPriceImprovement( 0 )
	{
		memset( Symbol, 0, sizeof( Symbol ) );
	}

    void setSymbol( const Engine::AsciiString&  val ) throw() { assert(sizeof(Symbol) == val.size()); memcpy(Symbol, val.data(), sizeof(Symbol) ); }
    Engine::AsciiString getSymbol() const throw() { return Engine::AsciiString(Symbol, sizeof(Symbol) ); }

    void setRetailPriceImprovement( char        val ) throw() {  assert( std::find( &RetailPriceImprovementCodes[0], &RetailPriceImprovementCodes[4], val) != &RetailPriceImprovementCodes[4] ); RetailPriceImprovement = val;}
    char        getRetailPriceImprovement() const throw() { return RetailPriceImprovement; }
};

/**@}*/

B2B_END_PACK

#undef BATS_MSG_FIELDS
#undef BATS_MSG_F
#undef BATS_MSG_C

#ifdef B2BITS_FA_BIG_ENDIAN
inline System::u16 b2b_bats_leu16(System::u16 val )
{
    unsigned char buff[2];
    buff[0] = ( val >> 8 ) & 0x00ff;
    buff[1] = ( val & 0x00ff );
    return ( ( ( System::u16 )buff[1] ) << 8 ) +
           ( ( System::u16 )buff[0] );
}
inline System::u32 b2b_bats_leu32(System::u32 val )
{
    unsigned char buff[4];
    buff[3] = ( val >> 24 ) & 0x000000ff;
    buff[2] = ( val >> 16 ) & 0x000000ff;
    buff[1] = ( val >> 8 ) & 0x000000ff;
    buff[0] = ( val & 0x000000ff );
    return ( ( ( System::u32 )buff[0] ) << 24 ) +
           ( ( ( System::u32 )buff[1] ) << 16 ) +
           ( ( ( System::u32 )buff[2] ) << 8 ) +
           ( ( System::u32 )buff[3] );
}
inline System::u64 b2b_bats_leu64(System::u64 val )
{
    unsigned char buff[8];
    buff[0] = ( val >> 56 ) & 0x00000000000000ffLL;
    buff[1] = ( val >> 48 ) & 0x00000000000000ffLL;
    buff[2] = ( val >> 40 ) & 0x00000000000000ffLL;
    buff[3] = ( val >> 32 ) & 0x00000000000000ffLL;
    buff[4] = ( val >> 24 ) & 0x00000000000000ffLL;
    buff[5] = ( val >> 16 ) & 0x00000000000000ffLL;
    buff[6] = ( val >> 8 ) & 0x00000000000000ffLL;
    buff[7] = ( val & 0x00000000000000ffLL );
    return ( ( ( System::u64 )buff[7] ) << 56 ) +
           ( ( ( System::u64 )buff[6] ) << 48 ) +
           ( ( ( System::u64 )buff[5] ) << 40 ) +
           ( ( ( System::u64 )buff[4] ) << 32 ) +
           ( ( ( System::u64 )buff[3] ) << 24 ) +
           ( ( ( System::u64 )buff[2] ) << 16 ) +
           ( ( ( System::u64 )buff[1] ) << 8 ) +
           ( ( ( System::u64 )buff[0] ) );
}

#endif  //  #ifdef B2BITS_FA_BIG_ENDIAN

}//namespace Bats

#endif    //B2BITS_BATS_MESSAGES_H
