// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_IntDefines.h
/// Contains integer types definitions

#ifndef B2BITS_System_B2BITS_IntDefines_h
#define B2BITS_System_B2BITS_IntDefines_h

#if defined(_WIN32)

namespace System
{
    typedef signed      __int8 i8;
    typedef signed      __int16 i16;
    typedef signed      __int32 i32;
    typedef signed      __int64 i64;

    typedef unsigned    __int8 u8;
    typedef unsigned    __int16 u16;
    typedef unsigned    __int32 u32;
    typedef unsigned    __int64 u64;
}

#elif defined( __sun )
#include <sys/types.h>
namespace System
{
    typedef int8_t i8;
    typedef int16_t i16;
    typedef int32_t i32;
    typedef int64_t i64;

    typedef uint8_t u8;
    typedef uint16_t u16;
    typedef uint32_t u32;
    typedef uint64_t u64;
}
#elif defined( _LINUX ) || defined( __linux )
#include <sys/types.h>

namespace System
{
    typedef int8_t i8;
    typedef int16_t i16;
    typedef int32_t i32;
    typedef int64_t i64;

    typedef u_int8_t u8;
    typedef u_int16_t u16;
    typedef u_int32_t u32;
    typedef u_int64_t u64;
}

#elif defined( _SOLARIS_INTEL )

namespace System
{
    typedef char i8;
    typedef short i16;
    typedef int i32;
    typedef long long i64;

    typedef unsigned char u8;
    typedef unsigned short u16;
    typedef unsigned int u32;
    typedef unsigned long long u64;
}

#else
#   error Unknown platform. How to define i16, i32, i64, u16, u32, u64?
#endif

#ifndef byte
/// Declares alias for unsigned char
typedef unsigned char byte;
#endif

typedef System::u64 Long;

namespace System
{
    typedef u64 affinity64_t;
}

#endif // B2BITS_System_B2BITS_IntDefines_h


