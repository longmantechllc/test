// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <limits>
#include <sys/timeb.h>

#include <B2BITS_FastCodec.h>
#include <B2BITS_Exception.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_SystemDefines.h>

namespace
{
    // template id, used for encoding fix messages
    const int BUFFER_CAPACITY = 200;
}

int main( int argc, char const* const argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if ( argc != 2 ) {
        std::cerr << "Usage: <bin_file>" << std::endl;
        return 1;
    }

    try {
        // Initialize FastCodec
        Engine::FastCodec fastCodec( "../../../../engine.license" );

        // Create FastCoder
        std::auto_ptr<Engine::FastCoder> fastCoder(
            fastCodec.createFastCoder(
                "fixdix50sp1_old.xml|additional.xml",
                "templates.xml",
                "FIX50SP2_CQG_FAST",
                "FIX50SP2_CQG_FAST" ) );

        // Create FastDecoder
        std::auto_ptr<Engine::FastDecoder> fastDecoder(
            fastCodec.createFastDecoder(
                "fixdix50sp1_old.xml|additional.xml",
                "templates.xml",
                "FIX50SP2_CQG_FAST",
                "FIX50SP2_CQG_FAST" ) );

        // Open the binary file to read messages
        std::ifstream file( argv[1], std::ios::binary );

        if ( !file ) {
            throw Utils::Exception( "cannot open file: " + std::string( argv[3] ) );
        }

        // Get the file size
        file.seekg( 0, std::ios::end );
        std::ifstream::pos_type size = file.tellg();
        file.seekg( 0, std::ios::beg );

        // Read the file to the vector of char
        std::vector<char> data( ( unsigned int )size );
        std::vector<char>::iterator dataStart = data.begin();
        const std::vector<char>::const_iterator dataEnd = data.end();

        file.read( &*dataStart, size );

        // Create a buffer
        Engine::FastCoder::Buffer buffer( BUFFER_CAPACITY );
        while ( dataStart != dataEnd ) {
            // Decode message
            std::size_t decoded( 0 );

            // Reset the dictionary before decoding
            fastDecoder->resetDictionary();
            std::auto_ptr<Engine::FIXMessage> msg( fastDecoder->decode( &*dataStart, dataEnd - dataStart, &decoded ) );

            dataStart += decoded;
            std::cout << *msg->toString( '|' ) << std::endl;

            // Reset the dictionary before encoding
            fastCoder->resetDictionary();

            // Encode message
            // Note: the buffer will be reused
            fastCoder->encode( msg.get(), &buffer );
        }
    } catch ( Utils::Exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 1;
    } catch ( std::exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 2;
    } catch ( ... ) {
        std::cerr << "Unknown error." << std::endl;
        return 3;
    }

    return 0;
}
