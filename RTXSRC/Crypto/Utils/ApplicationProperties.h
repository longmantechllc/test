#pragma once

#include <string>

/// Application parameters
struct ApplicationProperties
{
    std::string initiator_cfg, symbol_file;
    int interval {10000};
};

