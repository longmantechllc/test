// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FastScp11Messages.h
/// Contain Engine::FastScp11::Hello, Engine::FastScp11::Alert classes declaration.

#ifndef H_B2BITS_FastScp11Messages_H
#define H_B2BITS_FastScp11Messages_H

#include <string>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_Emptily.h>


#if (_MSC_VER >= 1500)
#   pragma warning(push)
    // 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

namespace Engine {
    /**
    * Fast session control protocol (scp) namespace
    *
    * Contains all fast scp related classes (Hello, Alert etc).
    */
    namespace FastScp11 {

        class V12_API Hello
        {
        public:
            std::string senderName_;
            std::string vendorId_;
        }; // class Hello

        class V12_API Alert
        {
        public:
            enum SeverityEnum {
                Fatal = 1,
                Error,
                Warn,
                Info
            } severity_; // enum
            enum CodeEnum {
                Close = 1,
                Unauthorized,
                UnknownTemplateId,
                UnknownTemplateName,
                Other
            } code_; // enum

            Utils::Emptily<int> value_;
            std::string description_;

        public:
            std::string toString() const {
                return std::string("[").append(toString( severity_ )).append("] ").append(
                       toString( code_ )).append(description_);
            }

            static char const* toString( SeverityEnum severity ) {
                switch( severity ) {
                case Fatal:
                    return "FATAL";
                case Error:
                    return "ERROR";
                case Warn:
                    return "WARNING";
                case Info:
                default:
                    return "INFO";
                }
            }

            static char const* toString( CodeEnum code ) {
                switch( code ) {
                case Close:
                    return "Close";
                case Unauthorized:
                    return "Unauthorized";
                case UnknownTemplateId:
                    return "UnknownTemplateId";
                case UnknownTemplateName:
                    return "UnknownTemplateName";
                case Other:
                default:
                    return "Other";
                }
            }
        }; // class Alert

    } // namespace FastScp11
} // namespace Engine


#if (_MSC_VER >= 1500)
#   pragma warning(pop)
#endif

#endif // H_B2BITS_FastScp11Messages_H
