//
// Created by schen on 10/2/2020.
//

#include <climits>
#include <iostream>
#include <iomanip>
#include <ctime>
#include <chrono>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <memory>
//#include <optional>
#include <vector>

#ifdef WIN32
//#include <Windows.h>
//#elif
#else
#include <unistd.h>
#endif

#include "RTXUtils.h"
#include "Logger.h"
#include "LogToFile.h"

#include "Config/LoaderBase.h"
#include "Config/ConfigGlobalLoader.h"
#include "Config/ConfigRouterLoader.h"
#include "Config/ConfigSecIdfnsLoader.h"
#include "Config/ConfigUserLoader.h"
#include "Config/ConfigLoader.h"
#include "Config/Config.h"
#include "Config/ConfigGlobalBox.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "../include/IOIDataIO.h"

#include "xTimer.h"
#include "TimerScheduler.h"

#include "CPU.h"
#include "../include/App.h"

using namespace std;


App::App()
{
}

App::~App()
{
    DESTROY(m_logToFileEx);
    DESTROY(m_logToFileSys);
    DESTROY(m_logToFile);
    DESTROY(m_config);
    DESTROY(m_configLoader);

    DESTROY(m_cpu);
}

bool App::InitializeApp(const std::string& rootDir )
{
    InitLogging();
    InitConfig();

    std::string asset = m_config->GlobalConfig()->AssetClass();
    if(asset != "USEquities") {
        m_logToFileSys->m_logger->logInfo("No support for '%s' yet", asset.c_str());
        return false;
    }
    m_RTXOrderIO = std::make_shared<RTXOrderIO>(this);
    m_MktDataIO = std::make_shared<MktDataIO>(this);
    m_IOIDataIO = std::make_shared<IOIDataIO>(this);
    InitCPU();

    return true;
}

bool App::InitLogging()
{
    m_logToFileEx = new LogToFile();
    m_loggerException = m_logToFileEx->InitLogger(AppConstants::LOG_TO_FILE::EXCEPTION);

    m_logToFileSys = new LogToFile();
    m_loggerSys = m_logToFileSys->InitLogger(AppConstants::LOG_TO_FILE::SYSTEM);

    m_logToFile = new LogToFile();
    m_logger = m_logToFile->InitLogger(AppConstants::LOG_TO_FILE::ORDER);

    return true;
}

bool App::InitConfig()
{
    uint64_t start = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    m_config = new Config();
    m_config->InitConfigBox();
    m_configLoader = new ConfigLoader (m_loggerSys, m_config);
    m_configLoader->InitConfigLoader();

    auto end = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    auto microseconds = end - start;
    std::cout << "InitConfig: microseconds: " << microseconds << endl << endl;

    return true;
}

bool App::InitCPU()
{
    m_cpu = new CPU(this);
    m_cpu->InitCPU();
}


