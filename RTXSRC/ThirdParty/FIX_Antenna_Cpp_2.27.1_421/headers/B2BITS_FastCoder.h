// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FastCoder.h
/// Contains Engine::FastCoder class declaration.

#ifndef H_B2BITS_Engine_FastCoder_H
#define H_B2BITS_Engine_FastCoder_H

#include <map>
#include <set>
#include <memory>
#include <cstddef>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_FIXMessage.h>

namespace Engine
{
    /**
    * Encodes FIX messages to FAST messages.
    */
    class V12_API FastCoder
    {
    public:
        class Impl;
        typedef System::u32 TID;

    public:
        /**
        * Buffer
        */
        class V12_API Buffer
        {
            friend class FastCoder;

        public:
            /**
            * Constructor.
            */
            Buffer();

            /**
            * Constructor.
            *
            * @param[in] capacity - count of bytes that should be reserved.
            */
            Buffer( std::size_t capacity );

            /**
            * Destructor.
            */
            ~Buffer();

            /**
            * Returns a pointer to the buffer.
            *
            * @return a pointer to the buffer.
            */
            char const* getPtr() const {
                return ptr_;
            }

            /**
            * Returns size of the buffer.
            *
            * @return size of the buffer.
            */
            std::size_t getSize() const {
                return size_;
            }

        private:
            /**
            * Resets the buffer with new values.
            *
            * @param[in] ptr - pointer to an allocated memory.
            * @param[in] size - count of useful bytes
            * @param[out] capacity - size of a memory block, ptr - points to.
            */
            void reset( char* ptr = NULL, std::size_t size = 0, std::size_t capacity = 0 );

            /**
            * Releases allocated memory.
            *
            * @param[out] pcapacity - used to store the buffer capacity; can be NULL.
            */
            char* release( std::size_t* pcapacity = NULL );

        private:
            Buffer( const Buffer& );
            Buffer& operator =( const Buffer& );

        private:
            char* ptr_;
            std::size_t size_;
            std::size_t capacity_;
        };  // Buffer

    public:
        /**
        * Constructor.
        */
        FastCoder( Impl* impl );

        /**
        * Destructor.
        */
        ~FastCoder();

        /**
        * Creates a skeleton of a fix message and returns it.
        *
        * @param[in] msgType - null-terminated std::string. Specifies type of new message.
        * @return instance of FIXMessage class.
        */
        std::auto_ptr<FIXMessage> newSkel( const std::string& msgType );

        /**
        * Parses the specified std::string and creates a corresponding FIXMessage instance;
        *
        * @param[in] rawMessage - null-terminated std::string. Should contain a raw fix message.
        * @return instance of FIXMessage class.
        */
        std::auto_ptr<FIXMessage> parse( const std::string& rawMessage );

        /**
        * Checks if the message can be encoded without errors, using the specified template. Silently returns a control
        * if no errors are detected; otherwise throws an std::exception with detailed information.
        * Usssualy used for debugging purpose only.
        * @param[in] msg - pointer to a fix message to be checked.
        * @param[in] tid - template id.
        * @throw an std::exception with detailed information if any errors detected.
        */
        void tryEncode( Engine::FIXMessage const* msg, System::u32 tid );

        /**
        * Encodes the specified fix message to the fast message using the specified template id.
        *
        * @param[in] msg - pointer to fix message to be encoded.
        * @param[in] tid - template id
        * @param[out] buffer - pointer to buffer that will contain fast message after encoding
        * @throw - an std::exception with detailed information if an error occurs.
        */
        void encode( Engine::FIXMessage const* msg, System::u32 tid, Buffer* buffer );

        /**
        * Encodes the specified fix message to the fast message
        *
        * @param[in] msg - pointer to fix message to be encoded.
        * @param[out] buffer - pointer to buffer that will contain fast message after encoding
        * @throw - an std::exception with detailed information if an error occurs.
        */
        void encode( Engine::FIXMessage const* msg, Buffer* buffer );

        /**
        * Resets the fast coder dictionary.
        */
        void resetDictionary();

        /** Overloaded delete operator */
        void operator delete( void* obj );

    protected:
        FastCoder( const FastCoder& );
        FastCoder& operator =( const FastCoder& );

    private:
        Impl* impl_;
    }; // FastCoder
} // namespace Engine
#endif // H_B2BITS_Engine_FastCoder_H
