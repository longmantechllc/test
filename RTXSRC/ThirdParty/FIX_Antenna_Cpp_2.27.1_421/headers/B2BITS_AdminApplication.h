// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_AdminApplication.h
/// Contains Engine::AdminApplication class declaration.

#ifndef __B2BITS_AdminApplication_h__
#define __B2BITS_AdminApplication_h__

#include <list>
#include <memory>

// This file requires Xerces-C library to be installed.
// Please go to http://xerces.apache.org/xerces-c/download.cgi and install v2.8 to proceed.
#include <xercesc/util/XercesDefs.hpp>

#include "B2BITS_FIXFieldValue.h"
#include "B2BITS_Application.h"
#include "B2BITS_Statistics.h"
#include "B2BITS_Mutex.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
    // A keyword was used that is not in the C++ standard, for example, one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
    // XML document comment target: badly-formed XML: reason
#   pragma warning(disable: 4635)
#endif

#ifdef _NATIVE_WCHAR_T_DEFINED
typedef wchar_t  XMLCh;
#elif defined XERCES_XMLCH_T
typedef XERCES_XMLCH_T XMLCh;
#else
typedef unsigned short  XMLCh;
#endif

XERCES_CPP_NAMESPACE_BEGIN
class DOMNode;
class DOMDocument;
XERCES_CPP_NAMESPACE_END


namespace Engine
{
    /** Success */
    const std::string RESULT_SUCCESS               = "0";
    /** Not implemented */
    const std::string RESULT_NOT_IMPLEMENTED       = "1";
    /** Not enough parameters */
    const std::string RESULT_NOT_ENOUGH_PARAMETERS = "2";
    /** Unknown session */
    const std::string RESULT_UNKNOWN_SESSION       = "3";
    /** Incorrect value */
    const std::string RESULT_INCORRECT_VALUE       = "4";
    /** Engine std::exception */
    const std::string RESULT_ENGINE_EXCEPTION      = "5";
    /** Unknown error */
    const std::string RESULT_UNKNOWN_ERROR         = "6";
    /** Operation rejected */
    const std::string RESULT_OPERATION_REJECTED    = "7";
    /** Conditions lack */
    const std::string RESULT_CONDITIONS_LACK       = "8";
    /** Invalid argument */
    const std::string RESULT_INVALIDARG            = "9";
    /** Logic error */
    const std::string RESULT_LOGIC_ERROR           = "10";
    /** Runtime error */
    const std::string RESULT_RUNTIME_ERROR         = "11";


    struct SessionId;
    class AdminApplicationImpl;

    /**
     * Keeps information about administrative session context
     */
    struct SessionContext {
        /**
         * Command RequestId
         */
        const std::string requestId_;

        /**
         * Command Monitoring request id
         */
        const std::string monitoringRequestId_;

        /**
         * Session
         */
        Session const* ssn_;

        SessionContext();
        SessionContext( const std::string& requestId, const std::string& monitoringRequestId, Session const* ssn );
        SessionContext( const SessionContext& v );

        SessionContext& operator=( SessionContext const& rv );
    };


    /**
    * Administrative application interface.
    */
    class V12_API AdminApplication : protected Application
    {
    protected:
        /**
        * Type is used to get description of available commands.
        * @param key    command name
        * @param value  command description in XML format:
        *   <[Command name]>
        *       <Description>[command description]</Description>
        *       <Parameter>
        *           <Name>[parameter name (required)]</Name>
        *           <Description>[parameter description (required)]</Description>
        *           <IsRequired>[true/false (required)]</IsRequired>
        *           <DefaultValue>[default value]</DefaultValue>
        *       </Parameter>
        *       ...
        *   </[Command name]>
        */
    protected:
        typedef std::map<std::string, std::string> CommandTable;

        //protected:
    public:

        /** Context within session calls AdminApplication event handlers. */
#ifdef _MSC_VER
#  pragma warning(push)
#  pragma warning(disable:4512)
#endif // _MSC_VER
        struct Context {
            /** ReuestID */
            const XMLCh* requestId_;

            /** Reference to the session */
            const Session& session_;

            /** Referencde to the FIX message */
            const FIXMessage& message_;

            Context( const Session& sn, const FIXMessage& msg, const XMLCh* requestId )
                : requestId_( requestId )
                , session_( sn )
                , message_( msg )
            {}
        };
#ifdef _MSC_VER
#  pragma warning(pop)
#endif // _MSC_VER

    public:
        /**
         * Posiable session actions
         */
        enum SessionAction { SSN_NEW = 0, SSN_DELETED, SSN_UPDATED };

    public:
        AdminApplication();
        virtual ~AdminApplication();

        /** Binds session with AdminApplication. Application takes reference to the session */
        void bindSession( Session* pSession );

        /** Binds session from embedded AdminApplication. Used for custom admin applications only */
        void bindApplication( AdminApplication* appl );

        /** Terminates all administrative sessions. */
        void terminate();
        /** Terminates all non-administrative sessions, this application has ownership of */
        void terminateNA();

        /**
        * Handle notification on sessions creation or deleting
        */
        void subscriptionListNotify( SessionAction status, Session* changedSsn, const SessionContext& reciverSsnInfo );

    protected:
        /**
        * Processes action.
        *
        * @param action name.
        * @param element xml element of action.
        * @return true if action was identify and succesfully executed.
        */
        virtual bool process( const std::string& action, const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Sends response message to client.
        * @param response response body.
        */
        void sendResponse( const XERCES_CPP_NAMESPACE::DOMDocument* response, const Context& context ) const;

        /**
        * Sends response message to client.
        * @param response response body.
        */
        virtual void sendResponse( const std::string& response, const Context& context ) const;

        /**
        * Sends response message to client in format:
        * <?xml version="1.0"?>
        * <Response ResultCode="[resultCode]">
        *   <Description>[description]</Description>
        *   [additionalData]
        * </Response>
        */
        void sendResponse( const std::string& resultCode, const std::string& description, const std::string& additionalData, const Context& context ) const;

        /**
        * Sends response message to client.
        * @param response Response body.
        * @param targetID Admin session's TargetCompID.
        * @param msg Admin session's TargetCompID.
        * @param requestId Command request ID.
        * @param msg The incoming message.
        */
        void sendResponse( const std::string& response, const std::string& targetID, const FIXMessage& msg, const XMLCh* requestId ) const;

        /**
        * Returns description of commands.
        * Derived class can modify list of commands at its discretion.
        */
        virtual void getAvailableCommands( CommandTable* commands ) const;

        /**
         * A call-back method to process incoming messages.
         * If the application can not process the given message, the FIX Engine will try to deliver it later
         * according to "Delayed Processing Algorithm".
         *
         * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
         *
         * @param msg the incoming message.
         * @param sn The corresponding FIX session.
         *
         * @return true if the application can process the given message, otherwise false.
         */
        virtual bool process( const FIXMessage& msg, const Session& sn ) B2B_OVERRIDE;

        /**
         * Method is called when application is binded with new session.
         */
        virtual void onBindAdminSession( const Session* /*pSession*/ ) {};

        /**
         * Sends Reject message into the session.
         */
        void sendReject( const Session& sn, const FIXMessage& msg, const std::string& text );

        /**
         * This call-back method is called to notify that the Logon message has been received from the counterpart.
         *
         * @param event Event information.
         * @param sn The corresponding FIX session.
         */
        virtual void onLogonEvent( const LogonEvent* event, const Session& sn ) B2B_OVERRIDE;

        /**
         * This call-back method is called to notify that the Logout message has been received from the counterpart
         * or the session was disconnected.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onLogoutEvent( const LogoutEvent* /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE;

        /**
         * This call-back method is called to notify about received unexpected message. For example - when first message
         * in session is not a Logon.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onUnexpectedMessageEvent( const UnexpectedMessageEvent* /*apEvent*/, const Session& /*aSn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when a message gap is detected in incoming messages.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onSequenceGapEvent( const SequenceGapEvent* /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when a session-level Reject message (MsgType = 3) is received.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onSessionLevelRejectEvent( const SessionLevelRejectEvent* /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /** This call-back method is called when a message have to be rejected.
          * @param event - object that encapsulates rejected message
          * @param sn - session object that rejects message
          */
        virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when a Heartbeat message (MsgType = 0)
         * with the TestReqID field (tag = 112) has been received.
         *
         * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onHeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when a Resend Request (MsgType = 2) has been received.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onResendRequestEvent( const ResendRequestEvent& /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when the session has changed its state.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onNewStateEvent( const NewStateEvent& /*event*/, const Session& /*sn*/ ) B2B_OVERRIDE {}

        /**
         * This call-back method is called when the message has to be routed to the session, which does not exist.
         *
         * @param event Information about occurred event.
         * @param sn The corresponding FIX session.
         */
        virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& sn ) B2B_OVERRIDE {
            B2B_USE_ARG( sn );
            B2B_USE_ARG( event );
        }

        /**
        * This call-back method is called when an outgoing message is about to be resent
        * as a reply to the incoming ResendRequest message.
        *
        * If the method returns 'true' then the Engine resends the message to counterpart,
        * otherwise it sends the SequenceReset Gap Fill message.
        *
        * @param msg Outgoing message.
        * @param sn FIX session.
        * @return true if the message should be resent, otherwise false.
        */
        virtual bool onResend( const FIXMessage& msg, const Session& sn ) B2B_OVERRIDE {
            B2B_USE_ARG( sn );
            B2B_USE_ARG( msg );
            return true;
        }

        /**
        * This call-back method is called when executing a CreateAcceptor command.
        * default implementation takes ownership of the created session and returns true
        *
        * @param sn FIX session.
        * @param logonMsg the incoming Logon message.
        * @return true if session sould be created, otherwise false.
        */
        virtual bool onCreateAcceptorEvent( Session& sn );

        /**
        * This call-back method is called when executing a CreateInitiator command.
        * default implementation takes ownership of the created session and returns true

        * @param sn FIX session.
        * @param customLogonMsg the custom Logon message.
        * @return true if session sould be created, otherwise false.
        */
        virtual bool onCreateInitiatorEvent( Session& sn, FIXMessage* customLogonMsg );


        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //Comand executers
        /**
         * Process the help command.
         */
        void executeHelp( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the SessionStatus command.
         */
        void executeSessionStatus( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the SessionList command.
         */
        void executeSessionList( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the SessionStat command.
         */
        void executeSessionStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the GeneralSessionStat command.
         */
        void executeGeneralSessionsStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );


        /**
         * Process the CreateAcceptor command.
         */
        void executeCreateAcceptor( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the Delete command.
         */
        void executeDelete( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the AverageReceivedStat command.
         */
        void executeAverageReceivedStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the AverageSentStat command.
         */
        void executeAverageSentStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the AverageValidateStat command.
         */
        void executeAverageValidateStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the ReceivedStat command.
         */
        void executeReceivedStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the SentStat command.
         */
        void executeSentStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the Proceed command.
         */
        void executeProceedStat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the ToBackup command.
         */
        void executeToBackup( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the ChangeSeqNum command.
         */
        void executeChangeSeqNum( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the ResetSeqNum command.
         */
        void executeResetSeqNum( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the TestRequest command.
         */
        void executeTestRequest( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the HeartBeat command.
         */
        void executeHeartbeat( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the SendMessage command.
         */
        void executeSendMessage( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
         * Process the DeleteAll command.
         */
        void executeDeleteAll( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the SessionParams command.
        */
        void executeSessionParams( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the CeateInitiator command.
        */
        void executeCreateInitiator( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the SessionsSnapshot command.
        */
        void executeSessionsSnapshot( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the ToPrimary command.
        */
        void executeToPrimary( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the RestartSessions command.
        */
        void executeRestartSessions( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the GetFIXProtocolsList command.
        */
        void executeGetFIXProtocolsList( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

        /**
        * Process the StopSession command.
        */
        void executeStopSession( const XERCES_CPP_NAMESPACE::DOMNode* element, const Context& context );

    protected:
        /**
         * Method locates child node and returns it's value
         */
        bool getChildNodeValue( const XERCES_CPP_NAMESPACE::DOMNode* element, const XMLCh* name, std::string* value ) const;

        /**
         * Method locates child nodes and returns values
         */
        bool getChildNodeValues(
            const XERCES_CPP_NAMESPACE::DOMNode* parentNode,
            const XMLCh* subnodeName,
            const XMLCh* paramName,
            std::list<std::string>& values ) const;


        /**
         * Method locates child nodes and returns values
         */
        bool getChildNodeValues( const XERCES_CPP_NAMESPACE::DOMNode* element, const XMLCh* name, std::list<std::string>& values ) const;


        /**
         * Method locates child node and returns integer value
         */
        bool getChildNodeValueInt( const XERCES_CPP_NAMESPACE::DOMNode* element, const XMLCh* name, int* value, std::string* ) const;

        /**
         * Method locates child node and returns boolearn value
         */
        bool getChildNodeValueBool( const XERCES_CPP_NAMESPACE::DOMNode* element, const XMLCh* name, bool* value, std::string* ) const;

    private:
        /**
        * Sends response message to client.
        * @param response response body.
        */
        virtual void sendResponseImpl( const std::string& response, const Context& context ) const;


        /**
         * Method creates Logon FIX message
         */
        FIXMessage* createLogon( FIXVersion version, const SessionExtraParameters* params, int HBI ) const;

        /**
         * Method extracts Sender and Target from received XML
         */
        bool getSenderTarget( const XERCES_CPP_NAMESPACE::DOMNode* element, std::string* sender, std::string* target, const Context& context ) const;

        /**
         * Method extracts SessionId from received XML
         */
        bool getSessionId( const XERCES_CPP_NAMESPACE::DOMNode* element, SessionId* sessionId, const Context& context ) const;

        /**
         * Method extracts Version from received XML
         */
        bool getVersionType( const XERCES_CPP_NAMESPACE::DOMNode* element, FIXVersion* version, std::string* type, const Context& context ) const;

        /**
         * Method prepares statistics form the specified message type
         */
        void createMsgProtocolTypeTSet( FIXVersion version, const std::string& type, std::set<StatisticsProvider::MsgProtocolTypeT>* typeSet ) const ;

        /**
         * Method locates specified session
         */
        Session* findSession( const std::string& sender, const std::string& target, const Context& context ) const;

        /**
         * Method locates specified session
         */
        Session* findSession( const SessionId& sessionId, const Context& context ) const;

        /**
         * Method terminates specified session
         */
        void terminateSession( Session* session, bool sendLogout, const std::string& logoutReason ) const;

        /**
         * Clear session list
         */
        void clearSessionList(bool administrative);

        /**
         * Register AdminSession to recive SessionList changes notifications
         */
        bool registerSessionsListSubscriber( const Context& context );

        /**
        * Unregister AdminSession form reciving SessionList changes notifications
        */
        bool unregisterSessionsListSubscriber( const Context& context );

        /**
         * parse View tag
         */
        bool translateView( const std::string& view, bool* status, bool* params, bool* stats );

        /**
         * Parse FixMessage from xml( Xerces parser can't parse SOH )
         */
        std::auto_ptr<FIXMessage> parseXmlFixMessage( const std::string& tag, const Context& context );

    private:
        AdminApplicationImpl* pImpl;


        /** Flag, is Engine is stopped */
        bool bStopped_;

        /** Syncronization object */
        mutable System::Mutex lock_;
    };

} // namespace Engine

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //__B2BITS_AdminApplication_h__

