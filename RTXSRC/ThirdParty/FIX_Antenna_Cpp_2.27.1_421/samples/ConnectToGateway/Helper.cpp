/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "Helper.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>

#include <B2BITS_Exception.h>
#include <B2BITS_FixEngine.h>
#include <B2BITS_FAProperties.h>

using namespace ::std;
using namespace ::Engine;
using namespace ::Utils;
using namespace ::System;
using namespace ::Aux;

////////////////////////////////////////////////////////////////////////////////
// Constants.
/// Application required arguments quantity.
const int REQUIRED_ARGS_QTY = 4;

// Screen output synchronizer.
System::Mutex Helper::outputLock_;

// Returns FIXVersion from it's string representation.
FIXVersion Helper::string2Version( const string& str )
{

    if( 0 == str.compare( "FIX40" ) ) {
        return FIX40;
    }
    if( 0 == str.compare( "FIX41" ) ) {
        return FIX41;
    }
    if( 0 == str.compare( "FIX42" ) ) {
        return FIX42;
    }
    if( 0 == str.compare( "FIX43" ) ) {
        return FIX43;
    }
    if( 0 == str.compare( "FIX44" ) ) {
        return FIX44;
    }
    if( 0 == str.compare( "FIX50" ) ) {
        return FIX50;
    }
    if( 0 == str.compare( "FIX50SP1" ) ) {
        return FIX50SP1;
    }
    if( 0 == str.compare( "FIX50SP2" ) ) {
        return FIX50SP2;
    }

    return NA;
}

// Retrives required arguments offset from application arguments array.
int Helper::findRequiredArguments( char** arguments, int quantity )
{

    int argumentsOffset = 0;

    // Skips optional arguments.
    while ( argumentsOffset < quantity &&
            '-' == *arguments[argumentsOffset] ) {
        argumentsOffset += 2;
    }
    // Reports error if offset not found.
    if ( quantity - REQUIRED_ARGS_QTY != argumentsOffset ) {
        throw AppException( "Requied arguments not found" );
    }
    // Returns found index.
    return argumentsOffset;
}

// Check passed optional arguments for validness
void Helper::checkOptionalArgument( char** arguments, int quantity )
{
    typedef std::set< std::string > Strings;
    Strings args;
    args.insert( "msgFile" );
    args.insert( "version" );
    args.insert( "inSeqNumber" );
    args.insert( "outSeqNum" );
    args.insert( "senderSubId" );
    args.insert( "targetSubId" );
    args.insert( "senderLocationId" );
    args.insert( "targetLocationId" );
    args.insert( "interactiveResend" );
    args.insert( "hbi" );
    args.insert( "intradayLogoutTolerance" );
    args.insert( "forceReconnect" );
    args.insert( "ignoreSeqNumTooLowAtLogon" );
    args.insert( "rejectMessageWhileNoConnection" );
    args.insert( "enableAggressiveSend" );
    args.insert( "user" );
    args.insert( "password" );

    for( int i( 0 ); i < quantity; ++i ) {
        if( '-' != arguments[i][0] ) {
            continue;
        }
        Strings::iterator it = args.find( arguments[i] + 1 );
        if( args.end() == it ) {
            throw AppException( string( "Unknown optional input parameter '" ) + arguments[i] + "'" );
        }
    }
}

// Finds optional argument value.
char* Helper::findOptionalArgument( const char* name,  char** arguments, int quantity )
{

    // iterates through all arguments.
    for ( int index = 0; index < quantity; ++index ) {

        // option value resides in the next argument.
        if ( '-' == *arguments[index] &&
             0   == strcmp( name, arguments[index] + 1 ) &&
             index + 1 < quantity ) {

            return arguments[index + 1];
        }
    }
    // Returns nothing.
    return NULL;
}

// Writes message to console.
void Helper::writeLineToConsole( const string& message )
{
    outputLock_.lock();
    clog << message << endl;
    outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine( const string& message )
{
    cerr << message << endl;
}

// Loads file content into std::string.
string Helper::loadString( const string& fileName )
{

    struct stat statistics;

    // Retrieves file size.
    if ( -1 == stat( fileName.c_str(), &statistics ) ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }

    // Opens file.
    ifstream ifs( fileName.c_str(), ios::binary );

    if ( !ifs.good() ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }

    string str;

    string tmp_ln;
    getline( ifs, tmp_ln );
    if ( !tmp_ln.empty() ) {
        string::size_type beg = tmp_ln.find( "\x01" "9=" );
        if ( beg == string::npos ) {
            throw Exception( "Must be size (tag 9) in message" );
        }
        string::size_type end = tmp_ln.find( "\x01", beg + 3 );
        if ( end == string::npos ) {
            throw Exception( "Must be size (tag 9) in message" );
        }
        string msg_size = tmp_ln.substr( beg + 3, end - beg - 3 );
        char* errPos = NULL;
        size_t size = strtol( msg_size.c_str(), &errPos, 10 );
        while ( size > tmp_ln.size() ) {
            if ( ifs.eof() ) {
                throw Exception( "Unexpected end of file" );
            }
            string tmp_ln2;
            char buf[10 * 1024];
            buf[0] = 10;
            ifs.read( &buf[1], size - tmp_ln.size() );
            tmp_ln += string( &buf[0], size - tmp_ln.size() + 1 );
            tmp_ln2.clear();
            getline( ifs, tmp_ln2 );
            tmp_ln += tmp_ln2;
        }
        if ( *tmp_ln.rbegin() == '\r' ) {
            tmp_ln.erase( tmp_ln.size() - 1 );
        }
        str = tmp_ln;
    }

    ifs.close();

    return str;
}

void Helper::parseCommandLine( const Aux::Properties& prop, ApplicationProperties* params )
{
    /*    // Checks for option arguments correctness
    Helper::checkOptionalArgument(arguments, quantity);

    // Tries to retrieve application required arguments.
    int reqArgsPos = Helper::findRequiredArguments(arguments, quantity);*/

    // Fills corresponding variables.
    params->sessionParams_.senderCompId_ = prop.getString( Aux::Properties::SENDER );
    params->sessionParams_.targetCompId_ = prop.getString( Aux::Properties::TARGET );

    params->sessionParams_.host_ = prop.getString( Aux::Properties::HOST );
    params->sessionParams_.port_ = prop.getInteger( Aux::Properties::PORT );

    // Retrieves optional sequence number parameter.
    if( prop.isPropertyExists( Aux::Properties::INSEQNUM ) ) {
        params->sessionParams_.inSeqNumber_ = prop.getInteger( Aux::Properties::INSEQNUM );
    }
    if( prop.isPropertyExists( Aux::Properties::OUTSEQNUM ) ) {
        params->sessionParams_.outSeqNumber_ = prop.getInteger( Aux::Properties::OUTSEQNUM );
    }

    // Retrieves optional filename in which example message is stored.
    if( prop.isPropertyExists( Aux::Properties::MSGFILE ) ) {
        params->messageFileName_ = prop.getString( Aux::Properties::MSGFILE );
    }

    // Retirves optional session version parameter.
    if( prop.isPropertyExists( Aux::Properties::VERSION ) ) {
        string value = prop.getString( Aux::Properties::VERSION );
        // Convers string into FIXVersion value.
        params->sessionParams_.fixVersion_ = Helper::string2Version( value );
        if ( NA == params->sessionParams_.fixVersion_ ) {
            // unknown version was specified, throws exception.
            throw AppException( "Unknown " + string( Aux::Properties::VERSION ) + " specified '" + value + "'" );
        }
    }

    // Retirves active connection parameter
    if( prop.isPropertyExists( Aux::Properties::ACTIVE_CONNECTION ) ) {
        string value = prop.getString( Aux::Properties::ACTIVE_CONNECTION );
        if( "primary" == value ) {
            params->sessionParams_.activeConnection_ = Session::PRIMARY_CONNECTION;
        } else if( "backup" == value ) {
            params->sessionParams_.activeConnection_ = Session::BACKUP_CONNECTION;
        } else if( "restore" == value ) {
            params->sessionParams_.activeConnection_ = Session::RESTORE_CONNECTION;
        } else {
            throw AppException( "Unknown " + string( Aux::Properties::ACTIVE_CONNECTION ) + " specified '" + value + "'" );
        }
    }

    if( prop.isPropertyExists( Aux::Properties::SENDERSUB ) ) {
        params->sessionParams_.senderSubId_ = prop.getString( Aux::Properties::SENDERSUB );
    }
    if( prop.isPropertyExists( Aux::Properties::TARGETSUB ) ) {
        params->sessionParams_.targetSubId_ = prop.getString( Aux::Properties::TARGETSUB );
    }

    if( prop.isPropertyExists( Aux::Properties::SENDERLOC ) ) {
        params->sessionParams_.senderLocationId_ = prop.getString( Aux::Properties::SENDERLOC );
    }
    if( prop.isPropertyExists( Aux::Properties::TARGETLOC ) ) {
        params->sessionParams_.targetLocationId_ = prop.getString( Aux::Properties::TARGETLOC );
    }

    if( prop.isPropertyExists( Aux::Properties::AUTO_SWITCH_BACKUP ) ) {
        params->sessionParams_.autoSwitchConnection_ = prop.getBool( Aux::Properties::AUTO_SWITCH_BACKUP );
    }
    if( prop.isPropertyExists( Aux::Properties::CYCLIC_SWITCH_BACKUP ) ) {
        params->sessionParams_.cyclicSwitchConnection_ = prop.getBool( Aux::Properties::CYCLIC_SWITCH_BACKUP );
    }

    if( prop.isPropertyExists( Aux::Properties::INTERACTIVERESEND ) ) {
        params->sessionParams_.interactiveResend_ = prop.getBool( Aux::Properties::INTERACTIVERESEND );
    }

    if( prop.isPropertyExists( Aux::Properties::SESSIONPROTOCOL ) ) {
        params->sessionParams_.isFIXTSsnProtocol_ = 0 == prop.getString( Aux::Properties::SESSIONPROTOCOL ).compare( "FIXT11" );
    }

    if( prop.isPropertyExists( Aux::Properties::HBI ) ) {
        params->sessionParams_.hbi_ = prop.getInteger( Aux::Properties::HBI );
    }

    if( prop.isPropertyExists( Aux::Properties::LOGOUTTOLERANCE ) ) {
        params->sessionParams_.intradayLogoutTolerance_ = prop.getBool( Aux::Properties::LOGOUTTOLERANCE );
    }

    if( prop.isPropertyExists( Aux::Properties::TCPBUFFERDISABLE ) ) {
        params->sessionParams_.disableTCPBuffer_ = prop.getBool( Aux::Properties::TCPBUFFERDISABLE );
    }

    if( prop.isPropertyExists( Aux::Properties::MAXMESSAGESINBUNCH ) ) {
        params->sessionParams_.maxMessageInBunch_ = prop.getInteger( Aux::Properties::MAXMESSAGESINBUNCH );
    }

    if( prop.isPropertyExists( Aux::Properties::AGGRESSIVESEND ) ) {
        params->sessionParams_.aggressiveSend_ = prop.getBool( Aux::Properties::AGGRESSIVESEND );
    }

    if( prop.isPropertyExists( Aux::Properties::FORCESEQNUMRESET ) ) {
        std::string str_val = prop.getString( Aux::Properties::FORCESEQNUMRESET );
        if( "true" == str_val ) {
            params->sessionParams_.forceSeqNumReset_ = FSNR_ON;
        } else if( "false" == str_val ) {
            params->sessionParams_.forceSeqNumReset_ = FSNR_OFF;
        } else {
            int val = atoi( str_val.c_str() );
            if( val == 0 ) {
                params->sessionParams_.forceSeqNumReset_ = FSNR_OFF;
            } else if( val == 1 ) {
                params->sessionParams_.forceSeqNumReset_ = FSNR_ON;
            } else if( val == 2 ) {
                params->sessionParams_.forceSeqNumReset_ = FSNR_ALWAYS;
            } else {
                params->sessionParams_.forceSeqNumReset_ = FSNR_NA;
            }
        }
    } else {
        params->sessionParams_.forceSeqNumReset_ = FixEngine::singleton()->getProperties()->forceSeqNumResetMode();
    }

    if( prop.isPropertyExists( Aux::Properties::FORCERECONNECT ) ) {
        params->sessionParams_.forceReconnect_ = prop.getBool( Aux::Properties::FORCERECONNECT );
    }

    if( prop.isPropertyExists( Aux::Properties::IGNORESEQNUMTOOLOW ) ) {
        params->sessionParams_.ignoreSeqNumTooLowAtLogon_ = prop.getBool( Aux::Properties::IGNORESEQNUMTOOLOW );
    }

    if( prop.isPropertyExists( Aux::Properties::REJECTWHILENOCONN ) ) {
        params->sessionParams_.rejectMessageWhileNoConnection_ = prop.getBool( Aux::Properties::REJECTWHILENOCONN );
    }

    if( prop.isPropertyExists( Aux::Properties::KEEPSTATE ) ) {
        params->sessionParams_.keepState_ = prop.getBool( Aux::Properties::KEEPSTATE );
    }

    if( prop.isPropertyExists( Aux::Properties::BACKUP_HOST ) ) {
        params->sessionParams_.backupHost_ = prop.getString( Aux::Properties::BACKUP_HOST );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_PORT ) ) {
        params->sessionParams_.backupPort_ = prop.getInteger( Aux::Properties::BACKUP_PORT );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_SENDERSUB ) ) {
        params->sessionParams_.backupSenderSubId_ = prop.getString( Aux::Properties::BACKUP_SENDERSUB );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_TARGETSUB ) ) {
        params->sessionParams_.backupTargetSubId_ = prop.getString( Aux::Properties::BACKUP_TARGETSUB );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_SENDERLOC ) ) {
        params->sessionParams_.backupSenderLocId_ = prop.getString( Aux::Properties::BACKUP_SENDERLOC );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_TARGETLOC ) ) {
        params->sessionParams_.backupTargetLocId_ = prop.getString( Aux::Properties::BACKUP_TARGETLOC );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_HBI ) ) {
        params->sessionParams_.backupHBI_ = prop.getInteger( Aux::Properties::BACKUP_HBI );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_LOGOUTTOLERANCE ) ) {
        bool val = prop.getBool( Aux::Properties::BACKUP_LOGOUTTOLERANCE );
        params->sessionParams_.backUpIntradayLogoutTolerance_ = val;
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_FORCERECONNECT ) ) {
        params->sessionParams_.backupForceReconn_ = prop.getBool( Aux::Properties::BACKUP_FORCERECONNECT );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_IGNORESEQNUMTOOLOW ) ) {
        params->sessionParams_.backupIgnoreSeqNumTooLow_ = prop.getBool( Aux::Properties::BACKUP_IGNORESEQNUMTOOLOW );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_REJECTWHILENOCONN ) ) {
        params->sessionParams_.backupRejectNoConn_ = prop.getBool( Aux::Properties::BACKUP_REJECTWHILENOCONN );
    }

    if( prop.isPropertyExists( Aux::Properties::BACKUP_AUTO_SWITCH_BACKUP ) ) {
        params->sessionParams_.backupAutoSwitchConnection_ = prop.getBool( Aux::Properties::BACKUP_AUTO_SWITCH_BACKUP );
    }
    if( prop.isPropertyExists( Aux::Properties::BACKUP_CYCLIC_SWITCH_BACKUP ) ) {
        params->sessionParams_.backupCyclicSwitchConnection_ = prop.getBool( Aux::Properties::BACKUP_CYCLIC_SWITCH_BACKUP );
    }

    if( prop.isPropertyExists( Aux::Properties::USERNAME ) ) {
        params->sessionParams_.user_ = prop.getString( Aux::Properties::USERNAME );
    }
    if( prop.isPropertyExists( Aux::Properties::PASSWORD ) ) {
        params->sessionParams_.password_ = prop.getString( Aux::Properties::PASSWORD );
    }

	if( prop.isPropertyExists( Aux::Properties::HIDDENLOGONCREDENTIALS ) ) {
		params->sessionParams_.hiddenLogonCredentials_ = prop.getBool( Aux::Properties::HIDDENLOGONCREDENTIALS );
    }

    if( prop.isPropertyExists( Aux::Properties::CUSTOM_LOGON ) ) {
        params->sessionParams_.customLogon_ = prop.getString( Aux::Properties::CUSTOM_LOGON );
    }
}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo( const string& applicationName )
{
    Helper::writeErrorLine();
    Helper::writeErrorLine( "Usage: " + applicationName + " <property file>" );
    Helper::writeErrorLine();
}

