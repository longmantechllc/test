// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SessionsManager.h
/// Contains Engine::SessionsManager class declaration.

#ifndef _B2BITS_Sessions_Manager__h_
#define _B2BITS_Sessions_Manager__h_

#include "B2BITS_V12_Defines.h"

namespace System
{
    struct IPAddr;
}

namespace Engine
{
    class FIXMessage;
    class Session;
    typedef System::IPAddr IPAddr;

    /**
    * An interface for managing sessions.
    *
    * @see FixEngine::registerSessionsManager()
    */
    class V12_API SessionsManager
    {
    public:
        /**
        * A derivable method to call by FIX Engine when a new incomming connection was established
        * and logon message was received from it.
        *
        * @return Return false to close connection. true otherwise.
        *
        * @param logonMsg the incoming Logon message
        * @param remoteAddr ip address of the remote host
        * @param remotePort the remote port number to which the client socket is connected
        * @param localPort the port number which was being listened when accepting incoming connection
        */
        virtual bool onIncomingConnection( const FIXMessage& logonMsg, const IPAddr& remoteAddr, int remotePort, int localPort ) {
            ( void )&logonMsg;
            ( void )&remoteAddr;
            ( void )&remotePort;
            ( void )&localPort;
            return true;
        }

        /**
        * A derivable method to call by FIX Engine when a new acceptor's session was created
        * without the registered application. It is possible to register application inside this method.
        *
        * @return Return false to reject session. true otherwise.
        *
        * @param pSn the newly created FIX session
        * @param logonMsg the incoming Logon message
        */
        virtual bool onUnregisteredAcceptor( Session* pSn, const FIXMessage& logonMsg ) = 0;

        /**
        * A derivable method to call by FIX Engine when a session has been terminated.
        *
        * @return Return true to re-start the session. false otherwise.
        *
        * @param pSn the just terminated FIX session
        * @param[out] restartDelayMs the session re-start delay in milliseconds. 
        *       -1 - use session ReconnectInterval property as delay if defined or immediately otherwise.
        *        0 - immediately.
        *        any other value - time in milliseconds to delay session re-start.
        *        default is -1.
        *       
        */
        virtual bool onSessionTerminated( Session* pSn, int& restartDelayMs ) {
            ( void )pSn;
            ( void )&restartDelayMs;
            return false;
        }

        /**
        * Destructor
        */
        virtual ~SessionsManager() {}
    };

} // namespace Engine{
#endif // #ifndef _Sessions_Manager__h_

