#pragma once

#include <string>
#include <vector>

/// Application parameters
struct ApplicationProperties
{
    std::string initiator_cfg, symbol_file;
    int interval {10000};
    int minOrderSize{100}, maxOrderSize{1000};
    int burstAfter{0}, burstTimes{10}, burstInterval{0};
    std::vector<std::string> desks, logins;
};

