//
// Created by schen on 10/5/2020.
//
#ifndef RTXROUTER_RTXORDERIO_H
#define RTXROUTER_RTXORDERIO_H

#include <optional>
#include <vector>
#include <memory>
#include <mutex>
#include <unordered_set>

using namespace std;

class Fundamentals;
class TopOfBook;
class Bbo;
class Order;
class ExecutionReport;

class CPU;
class App;
class Logger;
class RTXOrder;
class RTXApp;

class RTXOrderIO {
    friend CPU;
public:
    RTXOrderIO();
    RTXOrderIO(App* app);
    virtual ~RTXOrderIO();

//Order
    bool StartParentOrder( Order* order );                                      //OSM send parent parent order to SOR
    void CancelParentOrder( const int pOrderId);                                //OSM send cancel parent order to SOR

    void SendChildOrder( std::shared_ptr<Order> order );                                //SOR send child order to OSM -->orderIO->sendChild()
    void CancelChildOrderRequest( const int pOrderId, const std::string& coId );        //SOR send cancel child order request to OSM -->orderIO-> xxx

//  Execution report
    void ReceiveExecutionReport( const ExecutionReport* executionReport);   //OSM call this function to send Execution report to SOR
    void SendExecutionReport( const ExecutionReport* executionReport);      //SOR send fill or cancel report to OSM (client) --> orderIO->sendER(*er);
    void SendParentAck( const ExecutionReport* executionReport);            //send Ack to client

//Others
    bool IsDestinationAvailable( const std::string& destinationName );                          //SOR ask OSM the exchange status
    void UpdateDestinationAvailability( const std::string& destinationName, bool available );    //OSM inform SOR the exchange status change
    std::string RouteAdminCmd(std::string adminCmd);    // OSM send admin command to SOR

    App* m_app=nullptr;
    Logger* m_logger;           //for system
    std::mutex m_mutex;

    RTXApp* orderIO;                                                //OSM pointer
    void SetOrderIO(RTXApp* orderIOIn) { orderIO = orderIOIn; }     //OSM call this function to send OSM pointer to SOR

    std::unordered_map<int, std::shared_ptr<RTXOrder>>& GetOrderList() {return orderList;}
private:
    void EndOfDayCancelOrders(bool isPostMarketCancel=false);
    void RemoveParentOrder(const int poId);
    RTXOrder* GetParentptr(const int poId);

    std::unordered_map<int, std::shared_ptr<RTXOrder>> orderList;
    std::unordered_set<std::string> availabeDest;
};

#endif //RTXROUTER_RTXORDERIO_H
