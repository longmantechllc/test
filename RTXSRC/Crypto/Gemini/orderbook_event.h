#pragma once

#include "constants.h"
#include <json_dto/pub.hpp>

namespace Crypto::gemini {

// {"type":"trade","tid":101755123341,"price":"45935.65","amount":"0.29","makerSide":"bid"},]}
//{"type":"change","side":"bid","price":"45935.65","remaining":"0.44953623","delta":"-0.29","reason":"trade"}
//{"type":"change","side":"bid","price":"45935.65","remaining":"0","delta":"-0.44953623","reason":"trade"}
struct events_type_t {
    WS::EventType type = WS::EventType::INVALID_TYPE;
    WS::ReasonType reason = WS::ReasonType::INVALID_TYPE;
    std::string_view price = INVALID_STRING_VIEW;
    std::string_view remaining = INVALID_STRING_VIEW;
    WS::SideType side = WS::SideType::INVALID_TYPE;

    void reset() {
        type = WS::EventType::INVALID_TYPE;
        reason = WS::ReasonType::INVALID_TYPE;
        price = INVALID_STRING_VIEW;
        remaining = INVALID_STRING_VIEW;
        side = WS::SideType::INVALID_TYPE;
    }
};

struct orderbook_data_t {
    WS::ResponseType type = WS::ResponseType::INVALID_TYPE;
    std::vector<events_type_t> events;

    void reset() {
        type = WS::ResponseType::INVALID_TYPE;
        events.clear();
    }
};


}  // namespace Crypto::gemini


namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::gemini::events_type_t &m) {
    io
    & json_dto::mandatory("type", m.type);
    if (m.type == Crypto::gemini::WS::EventType::change) {
        io
        & json_dto::mandatory("reason", m.reason)
        & json_dto::mandatory("price", m.price)
        & json_dto::mandatory("remaining", m.remaining)
        & json_dto::mandatory("side", m.side);
    }
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::gemini::orderbook_data_t &m) {
    io
    & json_dto::mandatory("type", m.type)
    & json_dto::mandatory("events", m.events);
}

}  // namespace json_dto
