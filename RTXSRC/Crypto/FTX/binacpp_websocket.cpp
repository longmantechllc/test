#include "binacpp_websocket.h"
#include "MDCache.h"
#include "magic_enum/magic_enum.hpp"
#include "ws_subscribe.h"
#include "orderbook_event.h"
#include <filesystem>

struct lws_context *CryptoWebSocket::context = nullptr;

static void show_http_content(const char *p, size_t l)
{
	if (lwsl_visible(LLL_INFO))
	{
		while (l--)
			if (*p < 0x7f)
				putchar(*p++);
			else
				putchar('.');
	}
}

//--------------------------
int CryptoWebSocket::event_cb(struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len)
{
#if defined(LWS_WITH_TLS)
	union lws_tls_cert_info_results ci;
#if defined(LWS_HAVE_CTIME_R) && !defined(LWS_WITH_NO_LOGS)
	char date[32];
#endif
#endif

	auto *connection = reinterpret_cast<CryptoWebSocket *>(user);
//#ifndef NDEBUG
	spdlog::debug("user {}, lws {}, reason {}: in len {}, data {}",
				 user, (void *)wsi, magic_enum::enum_name(reason),
				 len, len > 0 ? (char *)in : "");
//#endif
	switch (reason)
	{
	case LWS_CALLBACK_CLIENT_ESTABLISHED:
		lws_callback_on_writable(wsi);
		// connection->on_connect(wsi);
		connection->subscribe();
		break;

	case LWS_CALLBACK_CLIENT_RECEIVE:
		lws_validity_confirmed(wsi);
		/* Handle incomming messages here. */
		try
		{
			spdlog::info("recevied data for idx {}, symbol {}, book {}, in {}",
						 connection->m_idx, connection->m_symbol, (void *)&connection->m_book,
						 std::string_view((char *)in, len));

			connection->cb(std::string_view((char *)in, len));
		}
		catch (exception &e)
		{
			spdlog::error("<CryptoWebSocket::event_cb> Error ! {}", e.what());
		}
		break;

	case LWS_CALLBACK_CLIENT_WRITEABLE:
	{
		// spdlog::info( "LWS_CALLBACK_CLIENT_WRITEABLE wsi {}", (void *)wsi);
		break;
	}

	case LWS_CALLBACK_CLOSED_CLIENT_HTTP:
	case LWS_CALLBACK_CLOSED:
	case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
		// spdlog::info( "LWS_CALLBACK_CLIENT_CONNECTION_ERROR wsi {}", (void *)wsi);
		//  connection->on_disconnect();
		break;

	case LWS_CALLBACK_ESTABLISHED_CLIENT_HTTP:
		lwsl_notice("lws_http_client_http_response %d\n",
					lws_http_client_http_response(wsi));
#if defined(LWS_WITH_TLS)
		if (!lws_tls_peer_cert_info(wsi, LWS_TLS_CERT_INFO_COMMON_NAME,
									&ci, sizeof(ci.ns.name)))
			lwsl_notice(" Peer Cert CN        : %s\n", ci.ns.name);

		if (!lws_tls_peer_cert_info(wsi, LWS_TLS_CERT_INFO_ISSUER_NAME,
									&ci, sizeof(ci.ns.name)))
			lwsl_notice(" Peer Cert issuer    : %s\n", ci.ns.name);

		if (!lws_tls_peer_cert_info(wsi, LWS_TLS_CERT_INFO_VALIDITY_FROM,
									&ci, 0))
#if defined(LWS_HAVE_CTIME_R)
			lwsl_notice(" Peer Cert Valid from: %s",
						ctime_r(&ci.time, date));
#else
			lwsl_notice(" Peer Cert Valid from: %s",
						ctime(&ci.time));
#endif
		if (!lws_tls_peer_cert_info(wsi, LWS_TLS_CERT_INFO_VALIDITY_TO,
									&ci, 0))
#if defined(LWS_HAVE_CTIME_R)
			lwsl_notice(" Peer Cert Valid to  : %s",
						ctime_r(&ci.time, date));
#else
			lwsl_notice(" Peer Cert Valid to  : %s",
						ctime(&ci.time));
#endif
		if (!lws_tls_peer_cert_info(wsi, LWS_TLS_CERT_INFO_USAGE,
									&ci, 0))
			lwsl_notice(" Peer Cert usage bits: 0x%x\n", ci.usage);
#endif
		break;

	/* chunked content */
	case LWS_CALLBACK_RECEIVE_CLIENT_HTTP_READ:
		lwsl_notice("LWS_CALLBACK_RECEIVE_CLIENT_HTTP_READ: %ld\n",
					(long)len);
		show_http_content((const char *)in, len);
		break;

	/* unchunked content */
	case LWS_CALLBACK_RECEIVE_CLIENT_HTTP:
	{
		char buffer[1024 + LWS_PRE];
		char *px = buffer + LWS_PRE;
		int lenx = sizeof(buffer) - LWS_PRE;

		/*
		 * Often you need to flow control this by something
		 * else being writable.  In that case call the api
		 * to get a callback when writable here, and do the
		 * pending client read in the writeable callback of
		 * the output.
		 *
		 * In the case of chunked content, this will call back
		 * LWS_CALLBACK_RECEIVE_CLIENT_HTTP_READ once per
		 * chunk or partial chunk in the buffer, and report
		 * zero length back here.
		 */
		if (lws_http_client_read(wsi, &px, &lenx) < 0)
			return -1;
	}
	break;

	default:
		// spdlog::info("<event_cb> [{}]: notice {}",
		//         (void *)wsi, magic_enum::enum_name(reason));
		break;
	}

	return 0;
}

// static const struct lws_extension extensions[]
// {
//     {
//         "permessage-deflate",
//         lws_extension_callback_pm_deflate,     //  undefined reference to `lws_extension_callback_pm_deflate'
//         "permessage-defalte; client_no_context_takeover; server_no_context_takeover; client_max_window_bits"
//     },
//     { NULL, NULL, NULL }
// };
//-------------------
void CryptoWebSocket::init()
{
	if (context != nullptr)
		return;
	struct lws_context_creation_info info;
	memset(&info, 0, sizeof(info));

	info.port = CONTEXT_PORT_NO_LISTEN;
	info.protocols = protocols;
	info.gid = -1;
	info.uid = -1;
	info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

	/*extra setting for SSL*/
	static const lws_retry_bo_t retry = {
		0,
		0,
		0,
		2,
		4,
		0};
	info.retry_and_idle_policy = &retry;
	info.timeout_secs = 5;
	info.ssl_cipher_list = "RC4-MD5:RC4-SHA:AES128-SHA:AES256-SHA:HIGH:!DSS:!aNULL";
#ifndef LWS_WITHOUT_EXTENSIONS
	// info.extensions = extensions;
#endif
	if (std::filesystem::exists("/etc/ssl/certs/ca-bundle.crt"))
	{
		info.client_ssl_ca_filepath = "/etc/ssl/certs/ca-bundle.crt";
	}
	else if (std::filesystem::exists("/etc/ssl/certs/ca-certificates.crt"))
	{
		info.client_ssl_ca_filepath = "/etc/ssl/certs/ca-certificates.crt";
	}
	// Enable keepalive in the lws context, and set it to 5 seconds
	info.ka_time = 5;
	// Try to get a response from the peer before giving up and killing the connection
	info.ka_probes = 5;
	// probe interval
	info.ka_interval = 5;

	context = lws_create_context(&info);
	spdlog::info("CryptoWebSocket::init on exch symbol {}, lws_context {}",
				 m_symbol, (void *)context);
}

//----------------------------
// Register call backs
void CryptoWebSocket::connect_endpoint(const char *path)
{

	/* Connect if we are not connected to the server. */
	struct lws_client_connect_info ccinfo = {0};
	ccinfo.context = context;
	ccinfo.address = m_host.data();
	ccinfo.port = m_port;
	ccinfo.path = path;
	// ccinfo.host 	= lws_canonical_hostname( context );
	// ccinfo.origin 	= "origin";
	// ccinfo.protocol = protocols[0].name;
	ccinfo.host = m_host.data();
	ccinfo.origin = m_host.data();
	ccinfo.protocol = nullptr;
	ccinfo.ssl_connection = LCCSCF_USE_SSL | LCCSCF_ALLOW_SELFSIGNED 
							| LCCSCF_SKIP_SERVER_CERT_HOSTNAME_CHECK | LCCSCF_PRIORITIZE_READS;
	// | LCCSCF_PIPELINE // only made thing worse

	ccinfo.userdata = (void *)this;

	/*struct lws* */ conn = lws_client_connect_via_info(&ccinfo);
	spdlog::info("<connect_endpoint> : idx={}, lws={}, path {}",
				 m_idx, (void *)conn, path);
}

static std::optional<double> cov_sv_to_double(const std::string_view &input)
{
	try
	{
		char *p_end;
		double out = std::strtod(input.data(), &p_end);
		return out;
	}
	catch (const std::exception &e)
	{
		return std::nullopt;
	}
}

void CryptoWebSocket::cb(std::string_view in)
{
	try
	{
		this->document_.SetNull();
		this->document_.GetAllocator().Clear();
		this->document_.Parse<rapidjson::kParseDefaultFlags>(
			in.data(), in.size());
		json_dto::check_document_parse_status(this->document_);
	}
	catch (const std::exception &e)
	{
		spdlog::error("<CryptoWebSocket::cb> : error parsing {} on {}",
					  e.what(), in);
	}

	try {
		static Crypto::ftx::orderbook_event_t book;
		book.reset();
		json_dto::from_json(this->document_, book);

		auto add_it = [&](auto &side, auto &input, bool snapshot)
		{
			if (snapshot) {
//#ifndef NDEBUG
				spdlog::debug("processing snapshot");
//#endif
				side.clear();
				for (const auto &price_point : input)
				{
					side.emplace(price_point[0], price_point[1]);
				}
			} else {
//#ifndef NDEBUG
				spdlog::debug("processing incremental");
//#endif
				for (const auto &price_point : input) {
					if (price_point[1] == 0)
						side.erase(price_point[0]);
					else
						side[price_point[0]] = price_point[1];
				}
			}
		};
		double o_bid_px{0.0}, o_bid_qty{0.0};
		double o_ask_px{0.0}, o_ask_qty{0.0};
		if (!m_book.bids.empty())
		{
			const auto &it = *m_book.bids.rbegin();
			o_bid_px = it.first;
			o_bid_qty = it.second;
		}
		if (!m_book.asks.empty())
		{
			const auto &it = *m_book.asks.begin();
			o_ask_px = it.first;
			o_ask_qty = it.second;
		}

		bool snapshot =  book.response_type == Crypto::ftx::WS::ResponseType::partial;
		add_it(m_book.bids, book.data.bids, snapshot);
		add_it(m_book.asks, book.data.asks, snapshot);

		if (const auto itor = m_book.bids.rbegin(); o_bid_px != itor->first || o_bid_qty != itor->second)
			m_cache->publish(true, m_book);
		if (const auto itor = m_book.asks.begin(); o_ask_px != itor->first || o_ask_qty != itor->second)
			m_cache->publish(false, m_book);
	}
	catch (const std::exception &e) {
		try {
			static Crypto::ftx::subscribe_response_t response;
			response.reset();
			json_dto::from_json(this->document_, response);
			spdlog::info("got subcribe response");
		}
		catch(...) {
			spdlog::error("unknown response :{}", in);
		}
	}
}
//----------------------------
// Entering event loop
void CryptoWebSocket::service()
{
	try
	{
		lws_service(context, -1);
	}
	catch (exception &e)
	{
		spdlog::error("service idx {}, symbol {} Error ! {}",
					  m_idx, m_symbol, e.what());
	}
}

std::vector<std::string> CryptoWebSocket::build_subscription_msg()
{
	std::string sub = R"(
		{"op": "subscribe", "channel": "orderbook", "market": ")";
	sub.append(m_symbol).append("\"}");
	spdlog::info("<build_subscription_msg> {}", sub);
	return {sub};
}

bool CryptoWebSocket::send_message(const std::string &msg)
{
	if (this->conn != nullptr)
	{
#ifndef NDEBUG
		spdlog::info("<WsCCryptoWebSocketonnection::send_message> : idx={} message {}",
					 m_idx, msg);
#endif
		std::uint8_t buf[LWS_PRE + 1024] = {0};
		std::memset(buf, '\0', LWS_PRE + 1024);
		int m;
		int n = lws_snprintf((char *)buf + LWS_PRE, 1024, "%s", msg.c_str());

		m = lws_write(conn, buf + LWS_PRE, n, LWS_WRITE_TEXT);
		if (m < n)
		{
			spdlog::info("<CryptoWebSocket::send_message> : idx={} sending failed...",
						 this->m_idx);
			return false;
		}
		lws_callback_on_writable(this->conn);
		return true;
	}
	return false;
}