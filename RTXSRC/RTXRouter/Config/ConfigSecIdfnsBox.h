//
// Created by schen on 11/11/2020.
//
#ifndef RTXROUTER_CONFIGSECIDFNSBOX_H
#define RTXROUTER_CONFIGSECIDFNSBOX_H

#include "ConfigGlobalBox.h"

using namespace std;

struct SWProute
{
    SWProute(){}
    SWProute(std::string nameIn, int allocationPercentIn)
            :name(nameIn),
             allocationPercent(allocationPercentIn)
    {}
    std::string name;
    int allocationPercent=100;
};

struct SWP
{
    ~SWP()
    {
        sWPSequentialRoutes.clear();
        sWPParallelRoutes.clear();
    }

    void AddSequentialRoute(const std::string& name)
    {
        sWPSequentialRoutes.push_back(name);
    }
    void AddParallelRoute(const std::string& name, int allocationPercent=0)
    {
        auto rtPtr = std::make_shared<SWProute>(name, allocationPercent);
        sWPParallelRoutes.push_back(rtPtr);
    }
    bool enabled = false;                                               //<SWP enabled="true" SWPPriceCheckEnbaled="false">
    bool sWPPriceCheckEnbaled = true;
    std::vector<std::string> sWPSequentialRoutes;                       //<route name="SWP-DRK1"/>
    std::vector<std::shared_ptr<SWProute>> sWPParallelRoutes;           //<route name="SWP-DRK4" allocationPercent="30"/>
};

struct Target
{
    Target();

    bool sequentialTargeting = false;
    std::unordered_set<std::string> definedRoutes;                  //<route name="TARG-NASD"/>
    std::unordered_map<std::string, std::string> MDIDmapping;       //<map mdid="XXXX" route="INDR-NASD"/>
    std::unordered_set<std::string> definedMmid;                    //<map mdid="NYSE" ... />

    void AddDefinedRoute(const std::string& routeName);
    void AddMDIDmapping(const std::string& mmid, const std::string& routeName);

};

struct Posting
{
    int rerouteFailureCounter;                                      //<rerouteFailure counter="100" routeList="INDR-NASD,INDR-NYSE" />
    std::vector<std::string> postFailureRoutes;

    bool quotePassoverMode = false;                                 //<quotePassover mode="true" thresold="0.01" timer="1000" />
    double quotePassoverThresold = 0.0;
    int quotePassoverTimer = 0;

    std::vector<std::string> postRoutes;                            //<route name="POST-NASD"/>

    void AddFailureRoute(const std::string& name)
    {
        postFailureRoutes.push_back(name);
    }
    void AddPostRoute(const std::string& name)
    {
        postRoutes.push_back(name);
    }
};

struct MultiPostRoute
{
    MultiPostRoute(){}
    MultiPostRoute(const std::string& nameIn, int allocationIn, int priorityIn)
            :name(nameIn),
             allocation(allocationIn),
             priority(priorityIn)
    {}
    std::string name;
    int allocation=100;
    int priority=0;
};

struct MultiPostingBox
{
    ~MultiPostingBox()
    {
        for(auto r : multipostRoutes)
        {
            delete r;
            r = nullptr;
        }
    }
    void AddPostRoute(const std::string& name, int allocation, int priority)
    {
        auto route = new MultiPostRoute(name, allocation, priority);
        multipostRoutes.push_back(route);
    }
    bool enabled = false;
    int cancelDelay = 0;
    std::vector<MultiPostRoute*> multipostRoutes;                            //<route name="POST-NASD" allocation="40" priority = "1" />
};

struct SecIdfn
{
    SecIdfn()
    {
        mktOrderPrimaryOpenCheck = true;
        retailSWP = new SWP();
        sWP = new SWP();
        targeting = new Target();
        posting = new Posting();
        multiPosting = new MultiPostingBox();
    }
    ~SecIdfn()
    {
        delete retailSWP;
        delete sWP;
        delete targeting;
        delete posting;
        delete multiPosting;
    }
    std::string secIdfn;                //<SecIdfn>NYSE</SecIdfn>
    std::string  listingExchange;       //<ListingExchange mdid="NYSE"/>
    std::string  listingExchangeVenue;       //<ListingExchangeVenue venue="NYSE"/>
    std::string  preMarketListingExchangeVenue;       //<PreMarketListingExchangeVenue venue="CTDL"/>
    bool mktOrderPrimaryOpenCheck;      //<MktOrderPrimaryOpenCheck>false</MktOrderPrimaryOpenCheck>

    std::unordered_map <int /* "1" */, std::shared_ptr<OrderParamAction>> orderTifActionList;
    std::unordered_map <std::string /* "Y" */, std::shared_ptr<OrderParamAction>> IPOActionList;
    void AddOrderParam(const std::string& type, std::string value, const std::string& action, std::string dest );

    struct SWP* retailSWP;
    struct SWP* sWP;
    struct Target* targeting;
    struct Posting* posting;
    struct MultiPostingBox* multiPosting;
};

class ConfigSecIdfnsBox
{
public:
    ConfigSecIdfnsBox();
    virtual ~ConfigSecIdfnsBox();
    void InitConfigSecIdfnsBox();

    void AddSecId(const std::string& name, const std::shared_ptr<SecIdfn>& secId);
    std::shared_ptr<SecIdfn>& GetExchange(const std::string& secId) ;    //"NYSE" ...

    std::shared_ptr<SecIdfn> secI_ = std::make_shared<SecIdfn>();
    std::unordered_map<std::string, std::shared_ptr<SecIdfn>> secIdfnList;
};

#endif //RTXROUTER_CONFIGSECIDFNSBOX_H
