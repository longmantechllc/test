#pragma once

#include <string>
#include <unordered_set>
#include <vector>
#include <utility>
#include <shared_mutex>

//#include <mutex>
/* InRush Headers */
#include "fh.h"
#include "MDInterface.h"

class RedlineMD : public MDInterface {
public:
    RedlineMD();
    ~RedlineMD();
    bool connectRedline();
    const auto& getSymbols() const { return m_symbols;}
    std::vector <std::shared_ptr<TopOfBook>>  subscribeTOB(const std::string& symbol ) override;
    Bbo subscribeBbo(const std::string& symbol);
    OpenTrade requestOpeningTrade(const std::string& symbol, int oid) override;

    void removeTOBSubscription(const std::string& symbol) override;
    void removeBboSubscription(const std::string& symbol);
    void cancelOpeningTradeRequest(const std::string& symbol) override;

    bool isInit() { m_init = connectRedline(); return m_init;}
    void onBookUpdate( const FhBookUpdate_t *update );
    auto getMDMsgQueuePtr() { return &m_queue;}
    void setCheckingTime(uint32_t checkingTime) { m_checkingTime = checkingTime;}
    void setClientName(const std::string& clientName) { m_clientName = clientName; }

private:
    FhClientHandle_t getClientHandle() const {return m_clientHandle;}
    std::string getClientName() { return m_clientName;}

    std::shared_mutex m_mutex;
    std::unordered_set<std::string> m_unsubs;
    std::unordered_set<std::string> m_subs;

    std::string m_clientName {"example_client"};
    bool m_init {false};
    std::unordered_map<std::string, bool> m_symbols;
    FILE *m_fp;
#ifdef MD_UNSUB
    std::unordered_map<std::string, FhSubscripHandle_t> m_BboSubscripts, m_TobSubscripts;
#else
    std::unordered_map<std::string, std::pair<FhSubscripHandle_t, bool>> m_BboSubscripts, m_TobSubscripts;
#endif
    FhClientHandle_t m_clientHandle;
    FhSubscriptionTemplate_t official_nbbo_template;
    FhSubscriptionTemplate_t composite_template;
    MDMsgQueue m_queue;
    uint32_t m_checkingTime;
};
