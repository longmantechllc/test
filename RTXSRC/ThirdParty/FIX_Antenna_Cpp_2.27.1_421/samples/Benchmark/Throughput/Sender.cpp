/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
*
* Benchmark: Sender.
* - creates session as a FIX initiator;
* - sends the configured number of messages.
*/

#ifdef _WIN32
#pragma warning(disable: 4786)
#endif // _WIN32

#include <iostream>
#include <fstream>
#include <sstream>
#include <stdio.h>
#include <assert.h>
#include <memory>

#include <B2BITS_V12.h>
#include <B2BITS_Thread.h>

#include "BenchmarkDefines.h"
#include "Properties.h"
#include "Exception.h"
#include "Auxiliary.h"
#include "SessionPtr.h"

using namespace std;
using namespace Utils;
using namespace Engine;
using namespace System;
using namespace Benchmark;

namespace BenchmarkSender
{

    /**
    * Application.
    */
    class Application : public Engine::Application
    {
    public:
        /**
        * Constructor.
        *
        * @param pSem Is posted when the Logout message is received.
        */
        Application( System::Semaphore* pSem, System::Semaphore* appSem ) : pLogoutSem_( pSem ), pmsgSem_( appSem ) {}

        /** A call-back method to process incoming messages.  */
        virtual bool process( const Engine::FIXMessage& /*msg*/, const Engine::Session& ) {
            pmsgSem_->post();
            return true;
        }

        /**
        * This call-back method is called to notify about the Logon event.
        */
        virtual void onLogonEvent( const Engine::LogonEvent*, const Engine::Session& sn ) {
            Benchmark::Auxiliary::notification( string( "The conforming Logon has been received for the session '" ) +
                                                *sn.getSender() + "_" + *sn.getTarget() + "'. Sending the measuring messages..." );
        }

        /**
        * This call-back method is called to notify about the Logout event.
        */
        virtual void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {
            pLogoutSem_->post();
        }

        /** This call-back method is called to notify about SequenceGap event. */
        virtual void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {}

        virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) {}

        /** This call-back method is called to notify about SessionLevelReject event. */
        virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent*, const Engine::Session& ) {}

        /**
        * This call-back method is called when a Heartbeat message (MsgType = 0)
        * with the TestReqID field (tag = 112) has been received.
        *
        * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
        */
        virtual void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ ) {
            ;
        }

        /**
        * This call-back method is called when a Resend Request (MsgType = 2) has been received.
        */
        virtual void onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ ) {
            ;
        }

        /**
        * This call-back method is called when the session has changed its state.
        */
        virtual void onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Session& /*sn*/ ) { }

        virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ ) {
            cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
        }


        /**
        * This call-back method is called when an outgoing message is about to be resent
        * as a reply to the incoming ResendRequest message.
        *
        * If the method returns 'true' then the Engine resends the message to counterpart,
        * otherwise it sends the SequenceReset Gap Fill message.
        *
        * @param msg Outgoing message.
        * @param sn FIX session.
        *
        @return true if the message should be resent, otherwise false.
        */
        virtual bool onResend( const FIXMessage& /*msg*/, const Session& /*sn*/ ) {
            return false;
        }

    private:
        /// Is posted when the Logout message is received.
        System::Semaphore* pLogoutSem_;
        System::Semaphore* pmsgSem_;
    };

    class SenderThread: public System::Thread
    {
    private:
        int hbi_;
        int port_;
        unsigned long volume_;

        string sender_;
        string target_;
        string host_;
        string rawMsg_;
        FIXVersion              fixVersion_;
        SessionExtraParameters  params_;
        int delay_;
        bool useSSL_;

    public:
        SenderThread( const int hbi,        const int port,       const int volume,
                      const string& sender, const string& target, const string& fixVersion,
                      const string& host,   const string& rawMsg, const string& storageType,
                      bool aggressiveSend = false,                bool tcpBuffer = true,
                      int maxMsgAmountBunch = 0,                  int delay = 0, bool useSSL = false )
            : hbi_( hbi )
            , port_( port )
            , volume_( volume )
            , sender_( sender )
            , target_( target )
            , host_( host )
            , rawMsg_( rawMsg )
            , fixVersion_( Auxiliary::string2version( fixVersion ) )
            , delay_( delay )
            , useSSL_(useSSL) {
            params_.socketPriority_ = aggressiveSend
                                      ? AGGRESSIVE_SEND_SOCKET_OP_PRIORITY
                                      : EVEN_SOCKET_OP_PRIORITY;

            if ( !tcpBuffer ) {
                params_.disableTCPBuffer_ = true;
            }
            if ( maxMsgAmountBunch != 0 ) {
                params_.maxMessagesAmountInBunch_ = maxMsgAmountBunch;
            }
            params_.storageType_ = Auxiliary::string2storageType( storageType );
        }

        ~SenderThread() {
        }

        void run() {
            try {
                Semaphore sem;
                Semaphore msgSem;
                Application app( &sem, &msgSem );

                if(useSSL_)
                    params_.sslContext_ = System::SSLClientContext();

                // Create a session.
                SessionPtr s( FixEngine::singleton()->createSession( &app, sender_, target_, fixVersion_,
                              &params_ ) );

                s->connect( hbi_, host_, port_ ); // Connect as Initiator.

                std::auto_ptr<FIXMessage>pFixMsg( FIXMsgProcessor::singleton()->parse( rawMsg_ ) );

                // Send the messages.
                unsigned long startTime_ = Benchmark::getCurrentTime();
                for ( unsigned long i = 0; i < volume_; ++i ) {
                    s->put( pFixMsg.get() );
                    if( delay_ != 0 ) {
                        System::Thread::sleep( delay_ );
                    }
                }
                unsigned long finishTime_ = Benchmark::getCurrentTime();
                double duration = (finishTime_ - startTime_) / 1000.0;
                if ( 0 == duration ) {
                    duration = 1;
                }
                unsigned long performance = (unsigned long)(volume_ / duration);

                {
                    stringstream msg;
                    msg << "Sender puts messages during " << duration
                        << " sec, with performance " << performance << " msg/sec";

                    Benchmark::Auxiliary::notification( msg.str() );
                }

                //FIXMessage::release(pFixMsg); // Free resources.
                msgSem.wait();

                // Disconnect and wait for the conforming Logout.
                s->disconnect( "Benchmark complete.", true );
                {
                    stringstream msg;
                    msg << "Sender '" << sender_ << "' sent all "
                        << volume_ << " messages. Waiting for Logout...";
                    Benchmark::Auxiliary::notification( msg.str() );
                }
                sem.wait(); // Wait until the conforming Logout message is received.
                {
                    stringstream msg;
                    msg << "Benchmark completed for '" << sender_ << "'. See Receiver's output.";
                    Benchmark::Auxiliary::notification( msg.str() );
                }
            } catch( const Utils::Exception& ex ) {
                Benchmark::Auxiliary::error( ex.what() );
            } catch( const Benchmark::Exception& ex ) {
                Benchmark::Auxiliary::error( ex.what() );
            }
        }
    };
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // command line parameters
    std::string engineConfig  = "engine.sender.properties";
    std::string reciverConfig = "benchmark.properties";
    if ( argc == 3 ) {
        engineConfig = argv[1];
        reciverConfig = argv[2];
    } else if( argc > 1 ) {
        cout << "Usage:\n\t" << argv[0] << " <engine properties file> <sender properties file>" << endl;
        return 1;
    }

    try {
        ifstream in( reciverConfig.c_str() );
        if ( !in ) {
            Benchmark::Auxiliary::error( string( "Can not open file '" ) + reciverConfig + "'" );
            return 1;
        }
        // load settings
        Benchmark::Properties p( &in );
        in.close();

        int hbi             = p.getInteger( Benchmark::Properties::HBI );
        int port            = p.getInteger( Benchmark::Properties::PORT );
        int volume          = p.getInteger( Benchmark::Properties::VOLUME );
        int sessionCount    = p.getInteger( Benchmark::Properties::SESSIONCOUNT );
        string sender       = p.getString( Benchmark::Properties::SENDER );
        string target       = p.getString( Benchmark::Properties::TARGET );
        string fixVersion   = p.getString( Benchmark::Properties::FIXVERSION );
        string storageType  = p.getString( Benchmark::Properties::STORAGE_TYPE );
        string host         = p.getString( Benchmark::Properties::HOST );
        string msgFileName  = p.getString( Benchmark::Properties::MESSAGE );

        if( "localhost" == host ) {
            cout << "WARNING! Receiver and sender are started on the same machine. Results might be inaccurate." << endl;
        }

        int maxMessageAmountBunch = p.getInteger( Benchmark::Properties::MAXMSGAMOUNTBUNCH );
        bool enableAggressiveSend = p.getString( Benchmark::Properties::AGGRESSIVE_SEND ) == "true";
        bool tcpBuffer            = p.getString( Benchmark::Properties::TCPBUFFERENABLED ) == "true";
        int delay                 = p.getInteger( Benchmark::Properties::DELAY );
        bool useSSL               = p.getString( Benchmark::Properties::USESSL ) == "true";

        // Load the measuring message.
        ifstream msgFileNameIn( msgFileName.c_str() );
        string rawMsg;
        if( ! msgFileNameIn ) {
            //Benchmark::Auxiliary::error( string("Can not open file with the measuring message '") + msgFileName + "'");
            //return 2;
            rawMsg = "8=FIX.4.49=13435=D49=TESTI56=TESTA34=450=3073797=Y52=20030204-08:46:1411=9000100855=TESTB54=160=20060217-10:00:0038=400040=110=024";
            Benchmark::Auxiliary::notification( string( "Set default measuring message '" ) + rawMsg + "'" );
        } else {
            getline( msgFileNameIn, rawMsg );
            msgFileNameIn.close();
            string::size_type tail = rawMsg.find_last_of( '\x01' );
            rawMsg.resize( tail + 1 );
        }

        // create senders
        Benchmark::Auxiliary::notification( "Creating senders." );
        vector<BenchmarkSender::SenderThread*> senders;
        senders.reserve( volume );
        for( int i = 0; i < sessionCount; ++i ) {
            senders.push_back( new BenchmarkSender::SenderThread( hbi, port, volume,
                               ( 1 == volume ) ? ( sender ) : ( sender + i2str( i ) ),
                               target, fixVersion, host, rawMsg, storageType,
                               ( ( 0 == i ) && ( enableAggressiveSend ) ),
                               tcpBuffer, maxMessageAmountBunch, delay, useSSL ) );
        }

        // Initialize FE
        FixEngine::init( engineConfig );

        Benchmark::Auxiliary::notification( "Starting senders." );
        // Start all senders
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != senders.at( i ) );
            senders.at( i )->start();
        }

        // Wait until senders finish
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != senders.at( i ) );
            senders.at( i )->join();
        }

        Benchmark::Auxiliary::notification( "Destroying senders." );
        // destroy senders
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != senders.at( i ) );
            delete senders.at( i );
        }

        // Uninitialize FE
        FixEngine::destroy();
    } catch( const Utils::Exception& ex ) {
        Benchmark::Auxiliary::error( ex.what() );
        return 3;
    } catch( const Benchmark::Exception& ex ) {
        Benchmark::Auxiliary::error( ex.what() );
        return 4;
    }

    return 0;
}
