//
// Created by schen on 2/22/2021.
//

#ifndef RTXROUTER_CPU_H
#define RTXROUTER_CPU_H
#include <atomic>

using namespace std;

class Logger;
class App;

class RTXOrderIO;
class MktDataIO;

class xTimer;
class TimerScheduler;
class SecMasterBox;
class RetailMarketBox;

enum class MarketSession
{
    Pre_Market,
    In_Market,
    Post_Market
};

class CPU
{
public:
    CPU(){}
    CPU(App* app);
    ~CPU();
    void InitCPU();
    void InitTimerScheduler();
    void InitSecMaster();
    void InitRetailMarketBox();
    MarketSession GetMarketSession() { return m_marketSession;}

    TimerScheduler* m_timerScheduler = nullptr;
    SecMasterBox* m_secMasterBox = nullptr;
    RetailMarketBox* m_retailMarketBox = nullptr;

    void StartMarketOpenTimer();
    void FireMarketOpenTimer(xTimer& timer);
    void StartMarketEndTimer();
    void FireMarketEndTimer(xTimer& timer);
    void StartPostMarketEndTimer();
    void FirePostMarketEndTimer(xTimer& timer);

    int  m_marketStartTimeMssm = 34200000;            //9:30    ms
    int  m_marketEndTimeMssm = 57600000;              //default to 16*60*60*1000 ms
    int  m_postMarketEndTimeMssm = 66600000;        //default to 18.5*60*60*1000 ms
    bool m_postMarketEnded = false;
    MarketSession m_marketSession = MarketSession::Pre_Market;
    atomic<bool> m_marketClosed = false;
    int UtcLocalTimeDiff() { return m_utcLocalTimeDiff; }

//    int  m_swpStartTimeMssm = 34200000;                     //9:30    ms
//    int  m_swpEndTimeMssm = 57600000;                       //default to 16*60*60*1000 ms
//    atomic<bool> m_inSWPSession = false;


    ////////////
    atomic<int> m_oId=0;;
    atomic<int> m_erCnt=0;
    atomic<int> m_coNumber=0;
    xTimer* m_testTimer = nullptr;
    void StartTestTimer();
    void FireTestTimer(xTimer& timer);

private:
    xTimer* m_marketOpenTimer = nullptr;
    xTimer* m_marketEndTimer = nullptr;
    xTimer* m_postMarketEndTimer = nullptr;

    int m_utcLocalTimeDiff = 0;
    Logger* m_loggerSys = nullptr;        //for system
    std::shared_ptr<RTXOrderIO> m_RTXOrderIO;
    App* m_app = nullptr;
};


#endif //RTXROUTER_CPU_H
