// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_AutoPtr2.h
/// Contains Utils::AutoPtr2 class declaration.

#ifndef __FIX_Utils_B2BITS_AutoPtr2_h__
#define __FIX_Utils_B2BITS_AutoPtr2_h__

#include <cstddef>

namespace Utils
{
    /// std::auto_ptr analogue with ability to delete object by object's method.
    template <typename T, void ( *RELEASE_FUNC )( T const* obj )>
    class AutoPtr2
    {
    private:
        /// Pointer to the owned object.
        T* ptr_;

    public: // methods
        /// Default constructor.
        AutoPtr2() throw()
            : ptr_( NULL ) {
        }

        /// Constructor.
        /// @param ptr Pointer to the object to be owned.
        explicit AutoPtr2( T* ptr ) throw()
            : ptr_( ptr ) {
        }

        /// Copy constructor.
        AutoPtr2( AutoPtr2<T, RELEASE_FUNC> const& ptr ) throw()
            : ptr_( NULL ) {
            reset( ptr.ptr_ );

            const_cast<AutoPtr2<T, RELEASE_FUNC>&>( ptr ).ptr_ = NULL;
        }

        /// Copy operator.
        AutoPtr2& operator=( AutoPtr2<T, RELEASE_FUNC> const& ptr ) throw() {
            reset( ptr.ptr_ );

            const_cast<AutoPtr2<T, RELEASE_FUNC>&>( ptr ).ptr_ = NULL;

            return *this;
        }

        /// Destructor.
        ~AutoPtr2() throw() {
            ( *RELEASE_FUNC )( ptr_ );
        }

        /// Release underlying pointer and return it.
        /// @return underlying pointer.
        T* release() throw() {
            T* ptr = ptr_;
            ptr_ = NULL;

            return ptr;
        }

        /// Deletes object by underlying pointer and set it to new value.
        /// @param ptr New value to set.
        void reset( T* ptr = NULL ) throw() {
            ( *RELEASE_FUNC )( ptr_ );

            ptr_ = ptr;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T* get() const throw() {
            return ptr_;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T* get() throw() {
            return ptr_;
        }

        /// Overloaded operator*.
        T& operator*() throw() {
            return *ptr_;
        }

        /// Overloaded operator*.
        T& operator*() const throw() {
            return *ptr_;
        }

        /// Overloaded operator->.
        T* operator->() throw() {
            return get();
        }

        /// Overloaded operator->.
        T* operator->() const throw() {
            return get();
        }
    }; // class AutoPtr2
}

#endif
