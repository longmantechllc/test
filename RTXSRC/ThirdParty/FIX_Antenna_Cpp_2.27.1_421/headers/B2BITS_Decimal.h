// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Decimal.h
/// Contains Engine::Decimal class declaration.

#ifndef B2BITS_FIX_TYPES_DECIMAL_H
#define B2BITS_FIX_TYPES_DECIMAL_H

#include <cassert>
#include <iosfwd>
#include <string>

#include <B2BITS_IntDefines.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_String.h>


namespace Engine
{
    /// \brief Encapsulates float type
    class V12_API Decimal
    {
    private:
        System::i64 mantissa_;
        System::i32 exponent_;

    public:
        // -"0." + 63zeros + 18446744073709551616
        /// Maximum buffer size required to store value
        static unsigned int const ValueSize = 2 + 63 + sizeof( "18446744073709551616" );

        /// Returns mantissa part of the value
        /// @return mantissa part of the value
        System::i64 getMantissa() const throw() {
            return mantissa_;
        }

        /// Returns exponent part of the value
        /// @return exponent part of the value
        System::i32 getExponent() const throw() {
            return exponent_;
        }

        /// Returns mantissa part of the value
        /// @return mantissa part of the value
        System::i64 mantissa() const throw() {
            return mantissa_;
        }

        /// Returns exponent part of the value
        /// @return exponent part of the value
        System::i32 exponent() const throw() {
            return exponent_;
        }

        /// Converts stored value to float
        /// @return float representation of the value
        float toFloat() const throw();

        /// Converts stored value to double
        /// @return double representation of the value
        double toDouble() const throw();

        /// Compares two Decimals
        ///
        /// @param rv Value to compare with.
        /// @returns true if equals; false otherwise
        bool equals( const Decimal& rv ) const throw();

    public:
        /// Default contructor
        Decimal() throw();

        /// Contructor
        /// Constructs object from mantissa and exponent
        ///
        /// @param m Mantissa
        /// @param e Exponent
        Decimal( System::i64 m, System::i32 e ) throw();

        /// Contructor
        /// Constructs object from double
        ///
        /// @param v Value of the double type
        Decimal( double v ) throw();

        /// Contructor
        /// Constructs object from 64-bit integer
        ///
        /// @param x Value of the i64 type
        Decimal( System::i64 x ) throw();

        /// Returns new Decimal value with required exponent.
        /// @param newExponent Required exponent. Must be in range [currentExponent-18 .. currentExponent+18]
        Decimal changeExponent( System::i32 newExponent ) const throw();

        /// Converts string to Decimal
        static Decimal fromString( AsciiString str ) throw();

        /// Converts string to Decimal
        static Decimal fromFixString( AsciiString str ) throw() {
            return fromString( str );
        }

        /// Contructor
        /// Constructs object from double
        ///
        /// @warning This method is lossy.
        ///
        /// @param v Value of the double type
        /// @param precision Number of digits after point. Must be in range [0..17]
        static Decimal fromDouble( double v, int precision ) throw();

        /// Contructor
        /// Constructs object from double
        ///
        /// @warning This method is lossy.
        ///
        /// @param v Value of the double type
        static Decimal fromDouble( double v ) throw();

        /// Converts wide-string to Decimal
        static Decimal fromString( WideString str ) throw();

        /// Converts decimal to FIX string
        /// @param[out] buf Buffer to convert to. Should be enough to store Engine::Decimal::ValueSize bytes.
        /// @return Pointer to the first character of the value
        char* toString( char* buf ) const throw();

        /// Converts decimal to FIX string.
        /// Converts decimal to FIX string
        /// @return FIX representation of the Decimal value
        std::string toStdString() const;

        /// If result string is less than passed buffer, remain chars will be filled with zeros according to FIX protocol.
        ///
        /// @param[out] buf Buffer to convert to.
        /// @param[in] size Size of the buffer. Must be positive.
        char* toString( char* buf, std::size_t size ) const throw();

    public:
        /// Compares two decimals.
        /// @return true if two decimals are equal; false otherwise.
        bool operator==( Engine::Decimal const& val ) const throw();        

        /// Compares two decimals.
        /// @return true if two decimals are NOT equal; false otherwise.
        bool operator!=( Engine::Decimal const& val ) const throw();

        /// Compares two decimals.
        /// @return true if left decimal is larger than right; false otherwise.
        bool operator>( Engine::Decimal const& val ) const throw();

        /// Compares two decimals.
        /// @return true if left decimal is smaller than right; false otherwise.
        bool operator<( Engine::Decimal const& val ) const throw();

        /// Compares two decimals.
        /// @return true if left decimal is larger or equal than right; false otherwise.
        bool operator>=( Engine::Decimal const& val ) const throw();

        /// Compares two decimals.
        /// @return true if left decimal is smaller or equal than right; false otherwise.
        /// @note Please note this method works only if both decimal have same exponent
        bool operator<=( Engine::Decimal const& val ) const throw();

        /// Addition assignment
        /// @note Please note this method works only if both decimal have same exponent
        Decimal& operator+=( Engine::Decimal const& val ) throw() {
            assert( this->exponent_ == val.exponent_ && "Decimal::operator+=: Exponent must be equal" );
            this->mantissa_ += val.mantissa_;
            return *this;
        }
        /// Addition operator
        /// @note Please note this method works only if both decimal have same exponent
        Decimal operator+( Engine::Decimal const& val ) const throw() {
            assert( this->exponent_ == val.exponent_ && "Decimal::operator+: Exponent must be equal" );
            return Decimal( this->mantissa_ + val.mantissa_, val.exponent_ );
        }

        /// Prefix increment
        Decimal& operator++() throw() {
            ++this->mantissa_;
            return *this;
        }

        /// Postfix increment
        Decimal operator++(int) throw() {
            return Engine::Decimal( this->mantissa_++, this->exponent_ );
        }

        /// Subtraction assignment
        /// @note Please note this method works only if both decimal have same exponent
        Decimal& operator-=( Engine::Decimal const& val ) throw() {
            assert( this->exponent_ == val.exponent_ && "Decimal::operator-=: Exponent must be equal" );
            this->mantissa_ -= val.mantissa_;
            return *this;
        }

        /// Substraction operator
        /// @note Please note this method works only if both decimal have same exponent
        Decimal operator-( Engine::Decimal const& val ) const throw() {
            assert( this->exponent_ == val.exponent_ && "Decimal::operator-: Exponent must be equal" );
            return Decimal( this->mantissa_ - val.mantissa_, val.exponent_ );
        }

        /// Prefix decrement
        Decimal& operator--() throw() {
            --this->mantissa_;
            return *this;
        }

        /// Postfix decrement
        Decimal operator--(int) throw() {
            return Engine::Decimal( this->mantissa_--, this->exponent_ );
        }

        /// Multiplication operator
        Decimal operator*( Engine::Decimal const& val ) const throw() {
            return Decimal( this->mantissa_ * val.mantissa_, val.exponent_ + this->exponent_ );
        }

        /// Multiplication assignment
        Decimal& operator*=( Engine::Decimal const& val ) throw() {
            this->mantissa_ *= val.mantissa_;
            this->exponent_ += val.exponent_;
            return *this;
        }
    };

    /// Stream writer
    V12_API std::ostream& operator<< ( std::ostream& s, const Decimal& rv );

    /// StringEx writer
    V12_API StringEx& operator<< ( StringEx& s, const Decimal& rv );
} // namespace FastImpl

#endif //B2BITS_FIX_TYPES_DECIMAL_H

