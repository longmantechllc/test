//
// Created by schen on 12/4/2020.
//

#ifndef RTXROUTER_CHILDORDERINFO_H
#define RTXROUTER_CHILDORDERINFO_H

//#include <string>
//#include <memory>
//#include <unordered_map>

#include "../include/Order.h"

using namespace std;


enum class ChildOrderType
{
    CHILD_ORDER_TYPE_RETAIL_SWP,
    CHILD_ORDER_TYPE_SWP,
    CHILD_ORDER_TYPE_TARGETING,
    CHILD_ORDER_TYPE_POSTING,
    CHILD_ORDER_TYPE_MPOST,
    CHILD_ORDER_TYPE_REROUTE,
    CHILD_ORDER_TYPE_RETAIL,

    CHILD_ORDER_TYPE_UNAVAILABLE
};

static std::string ChildOrderTypeString( ChildOrderType childOrderType )
{
    switch (childOrderType)
    {
        case ChildOrderType::CHILD_ORDER_TYPE_RETAIL_SWP : return "RetailSWP";
        case ChildOrderType::CHILD_ORDER_TYPE_SWP : return "SWP";
        case ChildOrderType::CHILD_ORDER_TYPE_TARGETING : return "Targeting";
        case ChildOrderType::CHILD_ORDER_TYPE_POSTING : return "Posting";
        case ChildOrderType::CHILD_ORDER_TYPE_MPOST : return "mPOST";
        case ChildOrderType::CHILD_ORDER_TYPE_REROUTE: return "RE_ROUTE";
        case ChildOrderType::CHILD_ORDER_TYPE_RETAIL: return "RETAIL";

        default : return "UNKNOWN";
    }
}

struct ChildOrder
{
    ChildOrder()
    : m_childOrderType(ChildOrderType::CHILD_ORDER_TYPE_UNAVAILABLE)
    {
    }
    ChildOrder( std::shared_ptr<Order> order, ChildOrderType childOrderType)
            :   m_order( order ),
                m_childOrderType( childOrderType ),
                m_qtyFilled( 0 ),
                m_qtyPending( order->size_ ),
                m_orderId(0)
    {}

    virtual ~ChildOrder() {};
    std::shared_ptr<Order> m_order;
    enum ChildOrderType m_childOrderType;
    long m_qtyPending=0;
    long m_qtyFilled=0;
    long m_orderId=0;

};

class ChildOrderInfo
{
public:
    ChildOrderInfo(){m_tpChild = std::make_shared<ChildOrder>();}
    ~ChildOrderInfo(){m_childOrderList.clear();}

    void insert(const std::string& coid, std::shared_ptr< ChildOrder > childOrder);
    std::shared_ptr< ChildOrder > GetChildByCoid(const std::string& coid);
    void RemoveChildByCoid(const std::string& coid);

    std::shared_ptr< ChildOrder > m_tpChild;

    std::unordered_map<std::string /* coid */, std::shared_ptr< ChildOrder>> m_childOrderList;
};



#endif //RTXROUTER_CHILDORDERINFO_H
