/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstddef>
#include <cassert>
#include <memory>
#include <limits>
#include <cmath>
#include <algorithm>

#include <B2BITS_SystemDefines.h>
#include <B2BITS_Semaphore.h>
#include <B2BITS_FixEngine.h>
#include <B2BITS_Session.h>
#include <B2BITS_Application.h>
#include <B2BITS_PreparedMessage.h>
#include <B2BITS_SessionParameters.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXMsgProcessor.h>

#include "B2BITS_HRTimer.h"
#include "B2BITS_Thread.h"

#include "B2BITS_StringUtils.h"
#include "B2BITS_AutoPtr2.h"

// SenderCompID
static std::string const SENDER = "sender";
// TargetCompID
static std::string const TARGET = "target";
// Raw FIX message used for measurement
static std::string const RAW_MSG = "8=FIX.4.2""\001""9=1""\001""35=D""\001""49=BLP""\001""56=SCHB""\001""34=01""\001""50=30737""\001""52=20000809-20:20:50""\001""11=90001008""\001""1=10030003""\001""21=2""\001""55=TESTA""\001""54=1""\001""38=4000""\001""40=2""\001""59=0""\001""44=30""\001""47=I""\001""60=20000809-18:20:32""\001""10=000""\001";

static size_t const MsgSeqNumFieldSize = 7;

struct ExperimentTimes
{
    // Time before Session::put is called
    System::u64 putTime_;
    // Time when message is delivered to the user application
    System::u64 processTime_;

    static void delete_array(const ExperimentTimes* objects ) { delete[] objects; }
};

// Application is registered for events as session-acceptor observer
class RTTMeasurementApplication: public Engine::Application
{
private:
    // Experiment indexes
    size_t putTimeIndex_;
    size_t processTimeIndex_;
    // Experiments count
    size_t count_;
    // Experiment times
    Utils::AutoPtr2<ExperimentTimes, &ExperimentTimes::delete_array> times_;
    // notify when message is received
    bool useMessageReceivedNotifications_;
    // Session to send messages to
    Engine::Session* initiator_;
    // Synchronization event
    System::Semaphore msgIsReceivedSem_;
    System::Semaphore lastMsgIsReceivedSem_;

    Engine::PutMessageOptions putOptions_;

    void nextIndex(size_t& index)
    {
        assert(index < count_);
        if(index < count_)
        {
            ++index;
            return;
        }
        throw std::out_of_range("index is out of range");
    }

public:

    // Basic initialization.
    // Must be called before starting tests series (e.g. in case of multiple runs)
    // @param initiator Session to send/receive messages
    // @param count Number of the experiments/messages to send
    // @param useMessageReceivedNotifications enables or disables message is received notifications
    void init( Engine::Session* initiator, size_t count, bool useMessageReceivedNotifications ) {
        // Make sure session is not NULL
        assert( NULL == initiator_ );

        // Initialize variables, reset to initial values
        initiator_ = initiator;
        putTimeIndex_ =  processTimeIndex_ = 0;
        count_ = count;
        times_.reset( new ExperimentTimes[count_+1] );
        memset( times_.get(), 0, count_*sizeof( ExperimentTimes ) );
        useMessageReceivedNotifications_ = useMessageReceivedNotifications;

        putOptions_.overrideSendingTime_ = false;
        putOptions_.overrideSenderCompID_ = false;
        putOptions_.overrideTargetCompID_ = false;
    }

    void recordTime() {
        // Record current time as the start time for sending round-trip time
        times_.get()[putTimeIndex_].putTime_ = System::HRTimer::value();
        nextIndex(putTimeIndex_);
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::FIXMessage* msg, bool usePutOptions  ) {
        // Make sure session is not NULL
        assert( NULL != initiator_ );

        // Put message to the session
        if(usePutOptions)
            initiator_->put( msg, &putOptions_ );
        else
            initiator_->put( msg );
    }

    /**
    * Sets static session level fields in the message and reserves space for volatile session level fields.
    * @param[out] pMsg Pointer to the FIXMessage to store field values
    * @param[in] msgSeqNumSize Number of characters for MsgSeqNum field to reserve.
    * @param[in] options Extra parameters. @see Engine::PutMessageOptions.
    */
    void inflate( Engine::FIXMessage* msg ) const {
        Engine::PutMessageOptions putOptions;
        putOptions.overrideSendingTime_ = false;
        initiator_->inflate( msg, MsgSeqNumFieldSize, &putOptions );
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::PreparedMessage* msg ) {
        // Make sure session is not NULL
        assert( NULL != initiator_ );

        // Put message to the session
        initiator_->put( msg );
    }

    // Wait untill message sent to the session-initiator is received by session-acceptor
    void waitForReceivedMessage() {
        assert(useMessageReceivedNotifications_);
        msgIsReceivedSem_.wait();
    }
    void waitForLastReceivedMessage() {
        lastMsgIsReceivedSem_.wait();
    }

    double round( double value ) {
        return floor( value + 0.5 );
    }

    // Calculate results and print them
    // @param file stream to print results to
    void printResult(size_t skip, size_t count) {
        
        if(0 < skip)
        {
            std::cout << "Skip first "<< skip << " results" << std::endl;
        }
        size_t effective_count = count - skip;

        std::vector<System::u64> total_rtt;
        total_rtt.resize(effective_count);

        char const* file_name = "round-trip_time.csv";
        std::ofstream file;
        file.open( file_name );

        // System::HRTimer::frequency() is number of operations per
        // second. To get frequency in nanoseconds,
        // devide this value to 10^9
        double ns_frequency = System::HRTimer::frequency() / 1000000000.0;

        System::u64 tot_summ = 0;

        file << "PutToProcess" << std::endl;

        for( size_t i = 0; i < effective_count; ++i ) {
            total_rtt[i] = ( System::u64 ) round( ( times_.get()[i+skip].processTime_ - times_.get()[i+skip].putTime_ ) / ns_frequency );

            file << total_rtt[i] << std::endl;
            tot_summ += total_rtt[i];
        }

        std::sort(total_rtt.begin(), total_rtt.end());

        std::cout << "Total RTT (nanoseconds): " << std::endl;

        std::cout << "\tMIN: " << total_rtt.front() << std::endl;
        std::cout << "\tMAX: " << total_rtt.back() << std::endl;
        std::cout << "\tAVG: " << tot_summ / effective_count << std::endl;
        std::cout << "\t50th percentile: " << total_rtt[static_cast<size_t>(0.50*effective_count)] << std::endl;
        std::cout << "\t85th percentile: " << total_rtt[static_cast<size_t>(0.85*effective_count)] << std::endl;
        std::cout << "\t95th percentile: " << total_rtt[static_cast<size_t>(0.95*effective_count)] << std::endl;

        std::cout << std::endl << "Time samples have been saved to " << file_name << std::endl;
    }

public:

    // Constructor
    RTTMeasurementApplication()
        : putTimeIndex_(0)
        , processTimeIndex_(0)
        , count_(0)
        , initiator_( NULL )
        , useMessageReceivedNotifications_(false)
    {
    }

private:


    /**
     * A call-back method to process incoming messages.
     * If the application can not process the given message, the FIX Engine will try to deliver it later
     * according to "Delayed Processing Algorithm".
     *
     * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
     *
     * @param msg the incoming message.
     * @param sn The corresponding FIX session.
     *
     * @return true if the application can process the given message, otherwise false.
     */
    bool process( const Engine::FIXMessage&, const Engine::Session& ) {
        // Record current time as the end time for receiving round-trip time
        times_.get()[processTimeIndex_].processTime_ = System::HRTimer::value();
        nextIndex(processTimeIndex_);

        // Notify sending thread that the  message is received, time is recorded
        // so the show can go on
        if(useMessageReceivedNotifications_)
            msgIsReceivedSem_.post();
        if(processTimeIndex_ == count_)
            lastMsgIsReceivedSem_.post();

        return true;
    }

    void onBeforeMessageIsSent( int, const Engine::Session& , size_t ) {}
    void onAfterMessageIsReceived( char const*, size_t, const Engine::Session& ) {}
    void onLogonEvent( const Engine::LogonEvent*, const Engine::Session& ) {}
    void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {}
    void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {}
    void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent*, const Engine::Session& ) {}
    void onMsgRejectEvent( const Engine::MsgRejectEvent*, const Engine::Session& ) {}
    void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent&, const Engine::Session& ) {}
    void onResendRequestEvent( const Engine::ResendRequestEvent&, const Engine::Session& ) {}
    void onNewStateEvent( const Engine::NewStateEvent&, const Engine::Session& ) {}
    void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent&, const Engine::Session& ) {}
    bool onResend( const Engine::FIXMessage&, const Engine::Session& ) {
        return false;
    }
};

void loadArgs(std::vector<std::string>* args, size_t index)
{
    size_t n = 0;
    std::string::size_type pos = std::string::npos;
    std::string key("benchmark.client.arg"), line, arg;
    std::ifstream fs(args->at(index).c_str());
    while(std::getline(fs,line)){
        if( 0 == line.compare(0, key.size(), key) && std::string::npos != (pos = line.find_first_of('=')) ){
            std::istringstream iss(line.substr(pos + 1));
            while(iss >> arg){
                args->insert(args->begin() + index + ++n, arg);
            }
        }
    }
}

// Main measurement routine
void run( int argc, char* argv[] )
{
    if( argc < 3 ) {
        std::cout << "Usage: RTTClient <host> <port> [default|null|transient|persistent|splitPersistent|persistentMM] [useSSL [ON|OFF]] [usePreparedMsg ON|OFF] [usePutOptions ON|OFF] [aff <NUMBER>] [speed <NUMBER>] [count <NUMBER>] [skip <NUMBER>] [conf <engine configuration file>]" << std::endl;
        return;
    }

    //required parameters
    std::string host = argv[1];
    int port = Utils::strtoi32(argv[2], strlen(argv[2]));

    //optional parameters
    bool useSSL = false;
    bool usePreparedMsg = true;
    bool usePutOptions = true;
    System::u64 aff = 0;
    Engine::MessageStorageType storgeType = Engine::default_storageType;
    int speed = 1000;
    size_t count = 10000;
    size_t skip = 100; //warming up msgs count
    std::string confFile = "engine.client.properties";

    std::vector<std::string> args;
    for(int i = 3; i < argc; ++i) args.push_back( argv[i] );

    for(size_t i = 0; i < args.size(); ++i) {
        if(args[i] == "useSSL") {
            if(i + 1 < args.size() && (args[i + 1] == "ON" || args[i + 1] == "OFF")){
                useSSL = (args[++i] == "ON");
            } else {
                useSSL = true;
            }
        }
        else if(args[i] == "usePreparedMsg") {
            if (++i < args.size())
                usePreparedMsg = (args[i] == "ON");
        }
        else if(args[i] == "usePutOptions") {
            if (++i < args.size())
                usePutOptions = (args[i] == "ON");
        }
        else if(args[i] == "aff") {
            if (++i < args.size())
                aff = Utils::strtou64(args[i].c_str(), args[i].size());
        }
        else if(args[i] == "speed") {
            if (++i < args.size())
                speed = Utils::strtoi32(args[i].c_str(), args[i].size());
        }
        else if(args[i] == "count") {
            if (++i < args.size())
                count = Utils::strtou32(args[i].c_str(), args[i].size());
        }
        else if(args[i] == "skip") {
            if (++i < args.size())
                skip = Utils::strtou32(args[i].c_str(), args[i].size());
        }
        else if(args[i] == "conf") {
            if (++i < args.size())
                confFile = args[i];
                loadArgs(&args, i);
        }
        else {
            storgeType = Engine::Session::stringToMessageStorageType( args[i].c_str() );
        }
    }

    skip = std::min( skip, count-1);


    // Create an instance of user applciation
    std::auto_ptr<RTTMeasurementApplication> application( new RTTMeasurementApplication() );

    // Initialize B2BITS FIX engine
    Engine::FixEngine::init(confFile);

    // Settings
    // For convenience settings are moved to engine.properties file
    Engine::SessionExtraParameters i_params;
    i_params.storageType_ = storgeType;
  //  i_params.sendCpuAffinity_ = 0x1;
  //  i_params.recvCpuAffinity_ = 0x2;

    if(useSSL)
    {
        std::cout << "Using SSL." << std::endl;
        i_params.sslContext_ = System::SSLClientContext();
    }

    std::cout << "Using " << Engine::Session::messageStorageTypeToString( storgeType ) << " message storage." << std::endl;
    std::cout << "Create session..." << std::endl;

    // Create session-initiator
    Engine::Session* initiator = Engine::FixEngine::singleton()->createSession(
                                     application.get(), SENDER, TARGET, Engine::FIX42, &i_params );

    const bool useMessageReceivedNotifications = (0 == speed);
    application->init( initiator, count, useMessageReceivedNotifications );

    std::cout << "Connecting..." << std::endl;
    // Establish connection
    initiator->connect( 0, host, port );

    // Wait while connection is not established
    while( initiator->getState() != Engine::Session::ESTABLISHED ) {
        System::Thread::sleep( 1 );
    }

    if(aff != 0) {
    	std::cout << "Affinity of main thread is " << aff << std::endl;
    	System::Thread::setCurrentThreadAffinity(aff);
    }

	// Create object message
    Utils::AutoPtr2<Engine::FIXMessage, &Engine::FIXMessage::release> msg2send;
    std::auto_ptr<Engine::GenericPreparedMessage> prepared_msg2send;

    msg2send.reset( Engine::FIXMsgProcessor::singleton()->parse( RAW_MSG ) );
    application->inflate( msg2send.get() );

    if(usePreparedMsg){
        msg2send->reserve( FIXFields::ClOrdID,       0, 10 );
        msg2send->reserve( FIXFields::Price,         1, 7 );
        msg2send->reserve( FIXFields::OrderQty,      2, 2 );
        msg2send->reserve( FIXFields::TransactTime,  3, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
        msg2send->reserve( FIXFields::SendingTime,   4, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
        prepared_msg2send.reset( new Engine::GenericPreparedMessage( msg2send.get() ) );
    }

    std::cout << "Testing..." << std::endl;

    std::cout << "Sending " << count << " messages at " << speed << " msgs/s" << std::endl;
    // Main sending cycle
    System::u64 start_time = System::HRTimer::value();
    double timer_frequency = static_cast<double>(System::HRTimer::frequency());
    size_t sz;
    for( size_t i = 0; i < count; ++i ) {
        application->recordTime();

        if(usePreparedMsg){
            prepared_msg2send->set( 0, static_cast<int>( i ) ); // ClOrdID
            prepared_msg2send->set( 1, Engine::Decimal( 100000 + i%100, -2 ) ); // Price
            prepared_msg2send->set( 2, 1 + i%10 ); // OrderQty
            prepared_msg2send->set( 3, Engine::UTCTimestamp::now(true) ); // TransactTime
            prepared_msg2send->set( 4, prepared_msg2send->get( 3, &sz ), Engine::UTCTimestamp::ValueSizeWithMilliseconds ); // TransactTime
            application->sendMessage( prepared_msg2send.get() );
        }
        else {
            msg2send->set( FIXFields::ClOrdID, static_cast<int>( i ) ); // ClOrdID
            msg2send->set( FIXFields::Price, Engine::Decimal( 100000 + i%100, -2 ) ); // Price
            msg2send->set( FIXFields::OrderQty, 1 + i%10 ); // OrderQty
            msg2send->set( FIXFields::TransactTime, Engine::UTCTimestamp::now(true) ); // TransactTime
            if(usePutOptions){
                msg2send->set( FIXFields::SendingTime, msg2send->getAsString(FIXFields::TransactTime) ); // TransactTime
                application->sendMessage( msg2send.get(), true );
            }
            else {
                application->sendMessage( msg2send.get(), false );
            }
        }

        if(useMessageReceivedNotifications)
            application->waitForReceivedMessage();
        else {
            //wait send time
            double next_send_time = static_cast<double>(i+1)/speed;
            while(true){
                double current_time = static_cast<double>(System::HRTimer::value() - start_time)/timer_frequency;
                double to_sleep = next_send_time - current_time;
                if(to_sleep < 0.0)
                    break;
                if(0.025 < to_sleep)
                    System::Thread::sleep( static_cast<int>(1000.0*0.9*to_sleep) );
            }
        }
    }
    System::u64 stop_time = System::HRTimer::value();

    // Release object message resources
    msg2send.reset();
    prepared_msg2send.reset();

    std::cout << "Sending done, at effective speed "<< (count/((stop_time - start_time)/timer_frequency)) <<" msgs/s, waiting for last message is received... ";
    application->waitForLastReceivedMessage();
    System::u64 done_time = System::HRTimer::value();
    std::cout << "done in "<< static_cast<int>(1000000*((done_time - stop_time)/timer_frequency)) <<" ns." << std::endl;

    // Print the results
    application->printResult(skip, count);

    // Close connection
    initiator->disconnectSync();

    initiator->release();

    // Destroy FIX engine
    Engine::FixEngine::destroy();
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if( System::HRTimer::frequency() < 1000000 ) {
        std::cerr << "Timer frequency is not sufficient to perform measurement." << std::endl;
        return 1;
    }

    try {
        run( argc, argv );
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: "  << e.what() << std::endl;
        return 1;
    }

    return 0;
}
