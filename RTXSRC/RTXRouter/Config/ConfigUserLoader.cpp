//
// Created by schen on 11/16/2020.
//

#include <string>
#include <memory>
#include <iostream>
#include <unordered_map>
#include <functional>
#include <ctime>
#include <unordered_set>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include "auto_xerces_ptr.h"

#include "RTXUtils.h"
#include "Logger.h"
#include "LoaderBase.h"
#include "ConfigUserBox.h"

#include "ConfigGlobalLoader.h"
#include "ConfigRouterLoader.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigUserLoader.h"

#include "Config.h"
#include "ConfigLoader.h"


ConfigUserLoader::ConfigUserLoader()
{

}
ConfigUserLoader::ConfigUserLoader(Logger* logger)
        :LoaderBase(logger)
{

}

ConfigUserLoader::~ConfigUserLoader()
{

}

void ConfigUserLoader::InitConfigUserLoader(ConfigLoader *loader, xercesc_3_2::DOMNode *node)
{
    auto  fptr = std::make_pair( "user", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetUserName(loader, node); } );
    funcList.insert(fptr);
    auto  fptr1 = std::make_pair( "Account", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetUserName(loader, node); } );
    funcList.insert(fptr1);
    auto  fptr2 = std::make_pair( "AcceptMarketOrder", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetAcceptMarketOrder(loader, node); } );
    funcList.insert(fptr2);
    auto  fptr3 = std::make_pair( "startTime", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetStartTime(loader, node); } );
    funcList.insert(fptr3);
    auto  fptr4 = std::make_pair( "sessionId", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetSessionId(loader, node); } );
    funcList.insert(fptr4);
    auto  fptr5 = std::make_pair( "excludeVenue", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddExcludeVenue(loader, node); } );
    funcList.insert(fptr5);
    auto  fptr6 = std::make_pair( "excludeRoute", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddExcludeRoute(loader, node); } );
    funcList.insert(fptr6);
    auto  fptr7 = std::make_pair( "Argos", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadArgos(loader, node); } );
    funcList.insert(fptr7);
    auto  fptr8 = std::make_pair( "excludeRetailMmid", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ AddExcludeRetailMmid(loader, node); } );
    funcList.insert(fptr8);
    auto  fptr9 = std::make_pair( "retail", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadRetailRouter(loader, node); } );
    funcList.insert(fptr9);
//    auto  fptr10 = std::make_pair( "useOrderLimitPriceRetail", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ SetUseOrderLimitPriceRetail(loader, node); } );
//    funcList.insert(fptr10);
}

void ConfigUserLoader::ProcessLoading(ConfigLoader *loader, xercesc_3_2::DOMNode *node)
{
    InitConfigUserLoader(loader, node);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(j) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
        {
            xercesc_3_2::XMLString::release(&nodeName) ;
            continue;
        }
        LoadUser(loader, subNode);
    }
    SetRetailParam(loader);
//    PrintUser(loader);
}

void ConfigUserLoader::LoadUser(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    userObj = std::shared_ptr<struct User>(new struct User());
    SetUserName(loader, node);
    xercesc_3_2::DOMNodeList *nodeList1 = node->getChildNodes();
    for (int j = 0; j != nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(j);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
        {
            xercesc_3_2::XMLString::release(&nodeName);
            continue;
        }
        if (funcList.find(nodeName) != funcList.end())
        {
            funcList[nodeName](loader, subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName);
    }
    loader->m_config->UserConfig()->AddUser(userObj->userNameList, userObj);
    userObj.reset(new struct User());
}

void ConfigUserLoader::SetUserName(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeName = xercesc_3_2::XMLString::transcode(node->getNodeName());
    std::string typeStr = nodeName;
    unsigned int type = 0;              //default to login
    if(typeStr == "Account")
        type = 1;
    userObj->type = type;

    std::string nameList = GetElement(node, "name") ;
    userObj->AddNameList(nameList);
    xercesc_3_2::XMLString::release(&nodeName) ;
}

void ConfigUserLoader::SetAcceptMarketOrder(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    userObj->acceptMarketOrder = RTXUtils::StrToBool(nodeText, true);

    loader->m_config->UserConfig()->AddAcceptMarketOrder(userObj->userNameList, nodeText);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::SetStartTime(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    userObj->SetStartTime(nodeText);

    loader->m_config->UserConfig()->AddStartTime(userObj->userNameList, nodeText);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::SetSessionId(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    auto sid = userObj->SetSessionId(nodeText);

    loader->m_config->UserConfig()->AddSessionId(userObj->userNameList, sid);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::AddExcludeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    userObj->AddExcludeVenue(loader->m_config->UserConfig(), nodeText);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::AddExcludeRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    userObj->AddExcludeRoute(loader->m_config->UserConfig(), nodeText);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::AddExcludeRetailMmid(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
    userObj->AddExcludeRetailMmid(loader->m_config->UserConfig(), nodeText);
    xercesc_3_2::XMLString::release(&nodeText);
}

void ConfigUserLoader::LoadRetailRouter(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string mpi = GetElement(node, "MPI") ;
    std::string mpfof = GetElement(node, "MPFOF") ;
    std::string bdcm = GetElement(node, "BDCM") ;
    std::string pi = GetElement(node, "pIWeight") ;
    std::string pfoF = GetElement(node, "pfoFWeight") ;
    std::string useOrderLimitPrice = GetElement(node, "useOrderLimitPrice") ;

    userObj->retailMPI = RTXUtils::StrToPrice(mpi, 0.0);
    userObj->retailMPFOF = RTXUtils::StrToPrice(mpfof, 0.0);
    userObj->retailBDCM = RTXUtils::StrToInt(bdcm, 0);
    userObj->retailpIWeight = RTXUtils::StrToInt(pi, 1);
    userObj->retailpfoFWeight = RTXUtils::StrToInt(pfoF, 1);

    bool usingLimit = RTXUtils::StrToBool(useOrderLimitPrice, false);
    if(useOrderLimitPrice.empty())
        userObj->retailUseOrderLimitPrice = -1;
    else if(!usingLimit)
        userObj->retailUseOrderLimitPrice = 0;
    else
        userObj->retailUseOrderLimitPrice = 1;
}

void ConfigUserLoader::SetRetailParam(ConfigLoader* loader)
{
    for (auto usr : loader->m_config->UserConfig()->userList)
    {
        if(usr.second->argosMp["RetailRouting"] == -1)
        {
            usr.second->retailMPI = Price(0.0);
            usr.second->retailMPFOF = Price(0.0);
            usr.second->retailBDCM = 0;
            usr.second->retailpIWeight = 1;
            usr.second->retailpfoFWeight = 1;
            usr.second->retailUseOrderLimitPrice = -1;
        }
    }
    for (auto acct : loader->m_config->UserConfig()->accountList)
    {
        if(acct.second->argosMp["RetailRouting"] == -1) {
            acct.second->retailMPI = Price(0.0);
            acct.second->retailMPFOF = Price(0.0);
            acct.second->retailBDCM = 0;
            acct.second->retailpIWeight = 1;
            acct.second->retailpfoFWeight = 1;
            acct.second->retailUseOrderLimitPrice = -1;
        }
    }
}
//
//void ConfigUserLoader::SetUseOrderLimitPriceRetail(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
//{
//    char *nodeText = xercesc_3_2::XMLString::transcode(node->getTextContent());
//    userObj->retailUseOrderLimitPrice = RTXUtils::StrToBool(nodeText, false);
//}

void ConfigUserLoader::LoadArgos(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;
        char* nodeText = xercesc_3_2::XMLString::transcode(subNode->getTextContent()) ;
        std::string txt = nodeText;
        userObj->AddArgo(nodeName, txt);

        xercesc_3_2::XMLString::release(&nodeText) ;
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}



void ConfigUserLoader::PrintUser(ConfigLoader* loader)
{
    std::cout << std::endl << "Print User:" << std::endl;

    for (auto usr : loader->m_config->UserConfig()->userList) {
        printf("User name: %s\n", usr.first.c_str());
        printf("User type: %d\n", usr.second->type);
        std::cout << "User acceptMarketOrder: " << boolalpha << usr.second->acceptMarketOrder << std::endl;
        printf("User startTimeStr: %s\n", usr.second->startTimeStr.c_str());
        printf("User startTime: %d\n", (int) usr.second->startTime);
        printf("User sessionId: %d\n", usr.second->sessionId);
        printf("User retailMPI: %f, retailMPFOF: %f, retailBDCM: %d, retailpI: %d, retailpfoF: %d \n",
               usr.second->retailMPI.ToDouble(), usr.second->retailMPFOF.ToDouble(), usr.second->retailBDCM, usr.second->retailpIWeight, usr.second->retailpfoFWeight);
        std::cout << "User useOrderLimitPriceRetail: " << usr.second->retailUseOrderLimitPrice << std::endl;


        printf("User excludeVenueList: ");
        for (auto nm : usr.second->excludeVenueList)
            printf("%s, ", nm.c_str());
        std::cout << std::endl;

        printf("User excludeRouteList:");
        for (auto nm : usr.second->excludeRouteList)
            printf(" %s, ", nm.c_str());
        std::cout << std::endl;

        printf("User excludeRetailMmid:");
        for (auto nm : usr.second->excludeRetailMmidList)
            printf(" %s, ", nm.c_str());
        std::cout << std::endl;

        for(auto ar : usr.second->Argos() )
            std::cout << "Argos:name: " << ar.first << ", val:" << ar.second << std::endl;

        std::cout << std::endl;
    }

    for (auto acct : loader->m_config->UserConfig()->accountList) {
        printf("acct name: %s\n", acct.first.c_str());
        printf("acct type: %d\n", acct.second->type);
        std::cout << "acct acceptMarketOrder: " << boolalpha << acct.second->acceptMarketOrder << std::endl;
        printf("acct startTimeStr: %s\n", acct.second->startTimeStr.c_str());
        printf("acct startTime: %d\n", (int) acct.second->startTime);
        printf("acct sessionId: %d\n", acct.second->sessionId);
        printf("User retailMPI: %f, retailMPFOF: %f, retailBDCM: %d, retailpI: %d, retailpfoF: %d \n",
               acct.second->retailMPI.ToDouble(), acct.second->retailMPFOF.ToDouble(), acct.second->retailBDCM, acct.second->retailpIWeight, acct.second->retailpfoFWeight);
        std::cout << "acct useOrderLimitPriceRetail: " << acct.second->retailUseOrderLimitPrice << std::endl;

        printf("acct excludeVenueList: ");
        for (auto nm : acct.second->excludeVenueList)
            printf(" %s, ", nm.c_str());
        std::cout << std::endl;

        printf("acct excludeRouteList: ");
        for (auto nm : acct.second->excludeRouteList)
            printf(" %s,", nm.c_str());
        std::cout << std::endl;

        printf("acct excludeRetailMmid: ");
        for (auto nm : acct.second->excludeRetailMmidList)
            printf(" %s,", nm.c_str());
        std::cout << std::endl;

        for(auto ar : acct.second->Argos() )
            std::cout << "Argos:name: " << ar.first << ", val:" << ar.second << std::endl;
        std::cout << std::endl;
    }
    std::cout << std::endl;
//
//    for (auto usr : loader->m_config->UserConfig()->acceptMarketOrder) {
//        printf("usr name: %s --- ", usr.first.c_str());
//        std::cout << "usr acceptMarketOrder: " << boolalpha << usr.second << std::endl;
//    }
//    for (auto usr : loader->m_config->UserConfig()->startTimeSsm) {
//        printf("usr name: %s --- ", usr.first.c_str());
//        std::cout << "usr startTimeSsm: " << usr.second << std::endl;
//    }
//    for (auto usr : loader->m_config->UserConfig()->sessionId) {
//        printf("usr name: %s --- ", usr.first.c_str());
//        std::cout << "usr sessionId: " << usr.second << std::endl;
//    }
//
//    for (auto exch : loader->m_config->UserConfig()->excludeVenueDict) {
//        printf("exclude venue name: %s --- ", exch.first.c_str());
//
//        for (auto usr : exch.second) {
//            printf(" %s,", usr.c_str());
//        }
//        std::cout << std::endl;
//    }
//    for (auto exch : loader->m_config->UserConfig()->excludeRouteDict) {
//        printf("exclude route name: %s --- ", exch.first.c_str());
//        for (auto usr : exch.second) {
//            printf(" %s,", usr.c_str());
//        }
//        std::cout << std::endl;
//    }
    std::cout << std::endl;
}

