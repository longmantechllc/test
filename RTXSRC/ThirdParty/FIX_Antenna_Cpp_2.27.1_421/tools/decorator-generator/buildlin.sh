#!/bin/bash

#############################################################################################################
# Script entry point
#############################################################################################################
print_usage() {
    echo "USAGE:"
    echo "  $0 HEADERS_PATH LIB_PATH -s SCP_PROTOCOL -a APP_PROTOCOL -f DICTIONARY [-f DICTIONARY ...]"
    echo "    -h - Print this help message end exit"
    echo "    -i HEADERS_PATH - Folder to store generated headers"
    echo "    -l LIB_PATH - Folder to store generated libraries (*.so)"
    echo "    -s SCP_PROTOCOL - Name of session-level protocol. Should be same to fixdic id"
    echo "    -a APP_PROTOCOL - Name of application-level protocol. Should be same to fixdic id"
    echo "    -f DICTIONARY - Path to the FIX dictionary XML file"
}

print_commandline() {
    echo "EXAMPLES"
    echo "buildlin -i ../../headers -l ../../lib -s FIX40 -a FIX40 -f ../../data/fixdic40.xml"
    echo "Generates a FIX40 decorator using dictionary description from ../../data/fixdic40.xml"
    echo ""
    echo "buildlin -i ../../headers -l ../../lib -s FIXT11 -a FIX44 -f ../../data/fixdict11.xml -f ../../data/fixdic44.xml"
    echo "Generates a FIX44 ower FIXT11 decorator"
}

#############################################################################################################
# Script entry point
#############################################################################################################
generate() {
    echo "generate()"
    SCP_PROTOCOL=$1
    APP_PROTOCOL=$2
    DICTIONARIES=$3

    echo DICTIONARIES=$DICTIONARIES    
    if ${DIR}/b2b_fix_msgs_generator2 -s $SCP_PROTOCOL -a $APP_PROTOCOL -f $DICTIONARIES -o ${DIR}/decorator
    then
        cp -f -R ${DIR}/decorator/headers/FixMessages $HEADERS_PATH
    else    
        echo "Generator exit with error: $?"
        exit $?
    fi
}

#############################################################################################################
# clean decorator generated files
#############################################################################################################
cleanup() {
    rm -rf ${DIR}/decorator/headers
    rm -rf ${DIR}/decorator/src
}

#############################################################################################################
# print_gcc_short
#############################################################################################################
print_gcc_short() {
	touch ${DIR}/t.cpp
	MAJOR=$(${CC} -E -dM ${DIR}/t.cpp | grep __GNUC__ | awk '{print $3}')
	MINOR=$(${CC} -E -dM ${DIR}/t.cpp | grep __GNUC_MINOR__ | awk '{print $3}')
	echo "gcc${MAJOR}${MINOR}"
	rm ${DIR}/t.cpp
}

#############################################################################################################
# compile_impl PROTCOL_PREFIX GCC_PREFIX ARCH
#############################################################################################################
compile_impl() {
    echo "compile_impl()"

    PROTCOL_PREFIX=$1
    GCC_PREFIX=$2
    ARCH=$3

    OUTLIB_NAME=libDecorator${PROTCOL_PREFIX}_${GCC_PREFIX}_${ARCH}.so
    SHORT_OUTLIB_NAME=libDecorator${PROTCOL_PREFIX}.so
    SRC=${DIR}/decorator/src/${PROTCOL_PREFIX}_ObjectFields.cpp\ ${DIR}/decorator/src/${PROTCOL_PREFIX}_Message_*.cpp

    CFLAGS=-D_LINUX\ -Werror\ -Wall\ -Wextra\ -Winit-self\ -Wmissing-include-dirs\ -Wno-parentheses\ -pedantic\ -Wno-long-long\ -Wno-unused-parameter\ -Wredundant-decls\ -Wnon-virtual-dtor\ -Wshadow\ -Woverloaded-virtual
    OPTIM_FLAGS=-O3\ -DNDEBUG
    HEADERS=-I$HEADERS_PATH
    LDFLAGS=-L$LIB_PATH\ -lV12\ -lrt
    SHFLAGS=-fPIC
    SOFLAGS=-shared
    BIN=$LIB_PATH/${OUTLIB_NAME}
    SHORT_OUTLIB_NAME=$LIB_PATH/libDecorator${PROTCOL_PREFIX}.so
    OBJDIR=${DIR}/obj

    if $CC $CFALGS $OPTIM_FLAGS $HEADERS $LDFLAGS $SHFLAGS $SOFLAGS -o $BIN $SRC
    then
        echo ""
    else
        echo "Compilation error: $?"
        exit $?
    fi

    CURRENT_DIR=`pwd`
    cd $LIB_PATH/
    ln -s $OUTLIB_NAME $SHORT_OUTLIB_NAME
    cd $CURRENT_DIR
}

#############################################################################################################
# compile_simple SCP_PROTOCOL ARCH GCC_PREFIX
#############################################################################################################
compile_simple() {
    echo "compile_simple()"

    PROTCOL_PREFIX=$1
    ARCH=$2
    GCC_PREFIX=$3
    compile_impl $PROTCOL_PREFIX $GCC_PREFIX $ARCH
}

#############################################################################################################
# compile_complex SCP_PROTOCOL APP_PROTOCOL ARCH GCC_PREFIX
#############################################################################################################
compile_complex() {
    echo "compile_complex()"

    SCP_PREFIX=${1}
    APP_PREFIX=${1}_${2}
    ARCH=$3
    GCC_PREFIX=$4
    compile_impl $SCP_PREFIX $GCC_PREFIX $ARCH
    compile_impl $APP_PREFIX $GCC_PREFIX $ARCH
}

#############################################################################################################
# compile SCP_PROTOCOL APP_PROTOCOL PROCESSOR_ATCH GCC_PREFIX
#############################################################################################################
compile() {
    echo "compile()"
    SCP_PROTOCOL=$1
    APP_PROTOCOL=$2
    ARCH=$3
    GCC_PREFIX=$4

    if [ "$SCP_PROTOCOL" == "$APP_PROTOCOL" ]
    then
        compile_simple $SCP_PROTOCOL $ARCH $GCC_PREFIX     
    else
        compile_complex $SCP_PROTOCOL $APP_PROTOCOL $ARCH $GCC_PREFIX
    fi 

    cleanup
}


#############################################################################################################
# Script entry point
#############################################################################################################

# init default parameters
DISPLAY_HELP=0

# Prase command-line
while getopts "hs:a:f:l:i:" opt; do
    case $opt in
        h) DISPLAY_HELP=1 ;;
        s) SCP_PROTOCOL=$OPTARG ;;
        a) APP_PROTOCOL=$OPTARG ;;
        f) DICTIONARIES=$DICTIONARIES\ $( readlink -f "$OPTARG" ) ;;
        i) HEADERS_PATH=$OPTARG ;;
        l) LIB_PATH=$OPTARG ;;
    esac
done


if [ $DISPLAY_HELP -eq 1 ] || [ "$HEADERS_PATH" == "" ] || [ "$LIB_PATH" == "" ] || [ "$SCP_PROTOCOL" == "" ] || [ "$APP_PROTOCOL" == "" ] || [ "$DICTIONARIES" == "" ]
then
    print_usage
    print_commandline
    exit 0
fi  

if [ "$CC" == "" ]
then
    CC=gcc
fi

DIR="$( dirname "$( readlink -f "$0" )" )"
HEADERS_PATH="$( readlink -f "$HEADERS_PATH" )"
LIB_PATH="$( readlink -f "$LIB_PATH" )"
GCC_PREFIX=$(print_gcc_short)
ARCH=$(uname -m)

generate $SCP_PROTOCOL $APP_PROTOCOL "$DICTIONARIES"
compile $SCP_PROTOCOL $APP_PROTOCOL $ARCH $GCC_PREFIX
