//
// Created by schen on 11/16/2020.
//

#ifndef RTXROUTER_CONFIGUSERLOADER_H
#define RTXROUTER_CONFIGUSERLOADER_H

class Logger;
class ConfigUserBox;
class ConfigLoader;

class ConfigUserLoader : public LoaderBase
{
public:
    ConfigUserLoader();
    ConfigUserLoader(Logger* logger);
    virtual ~ConfigUserLoader();
    void InitConfigUserLoader(ConfigLoader *loader, xercesc_3_2::DOMNode *node);
    void ProcessLoading(ConfigLoader *loader, xercesc_3_2::DOMNode *node) override;

    void LoadUser(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetUserName(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetAcceptMarketOrder(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetStartTime(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetSessionId(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddExcludeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddExcludeRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddExcludeRetailMmid(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadRetailRouter(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetRetailParam(ConfigLoader* loader);

    void LoadArgos(ConfigLoader* loader, xercesc_3_2::DOMNode* node);


    void PrintUser(ConfigLoader* loader);

    std::shared_ptr<struct User> userObj;
    std::unordered_map <std::string /*Name*/, std::function<void(ConfigLoader* loader, xercesc_3_2::DOMNode* node) > > funcList;
};

#endif //RTXROUTER_CONFIGUSERLOADER_H
