/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <algorithm>
#include <string> 
#include <memory>

#include <B2BITS_V12.h>
#include "FIXApp.h"

using namespace std;

const char* LISTENER_IP = "127.0.0.1";
const int LISTENER_PORT = 9016;
const int LISTENER_SSL_PORT = 9017;

int main(int argc, char **argv)
{
    cout << "starting FIX Sender..." << endl;

    try {
        // Initializes engine.
        // If error appears during initialization (properties file is not found, required property is missing, etc.) then exception is thrown.
        Engine::FixEngine::init( "sender.engine.properties" );

        // Create Application instance. CFIXApp class derived from Engine::Application
        CFIXApp application;

        cout << "starting FIX Sender session..." << endl;

        Engine::SessionExtraParameters params;
        params.intradayLogoutToleranceMode_ = true;
        int port = LISTENER_PORT;
        std::string useSSLparam;
        if(argc > 1 &&  argv[1])
            useSSLparam = argv[1];
        std::transform(useSSLparam.begin(), useSSLparam.end(), useSSLparam.begin(), ::tolower);
        if(useSSLparam == "-usessl")
        {
            params.sslContext_ = System::SSLClientContext();
            port = LISTENER_SSL_PORT;
        }
        // Create FIX session instance
        Engine::Session* pSI =  Engine::FixEngine::singleton()->createSession( &application, "TargetCompID", "SenderCompID",  Engine::FIX44, &params );
        // Connect session as initiator
        pSI->connect( 30, LISTENER_IP, port );

        // create FIX 4.4 New Order Single using the Flat model
        std::auto_ptr<Engine::FIXMessage> pMessage( Engine::FIXMsgFactory::singleton()->newSkel( Engine::FIX44, "D" ) );
        std::string clorgid;
        Engine::UTCTimestamp::now().toFixString( &clorgid );
        pMessage->set( FIXField::ClOrdID, clorgid );
        pMessage->set( FIXField::Symbol, "MSFT" );
        pMessage->set( FIXField::Side, '1' ); // Buy
        pMessage->set( FIXField::OrderQty, 400 );
        pMessage->set( FIXField::OrdType, '2' ); // Limit
        pMessage->set( FIXField::Price, Engine::Decimal( 1132, -2 ) );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );

        cout << "sending the message: " << *pMessage->toString( '|' ) << endl << endl;

        // Send order to session initiator
        pSI->put( pMessage.get() );

        // Wait for trade
        application.tradeReceived_.wait();

        // Closing the session
        pSI->disconnect();

        // Releasing resources
        pSI->registerApplication( NULL );
        pSI->release();

        // Destroying the engine
        Engine::FixEngine::destroy();
    } catch( const std::exception& ex ) {
        cout << "Error: " << ex.what() << endl;

        // Destroying the engine
        Engine::FixEngine::destroy();
    } catch ( ... ) {
        cout << "Unknown error." << endl;

        // Destroying the engine
        Engine::FixEngine::destroy();
    }

    return 0;
}

