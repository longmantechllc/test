// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Dictionary.h
/// Contains FixDictionary2::Dictionary class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_Dictionary_H
#define H_B2BITS_FixDictionary2_Dictionary_H

#include <string>
#include <map>
#include <list>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** Forward declaration of the Protocol class*/
    class Protocol;

    /** Forward declaration of the DictionaryImpl struct */
    struct DictionaryImpl;

    /** @class Dictionary
    *   @brief Represents Dictionary interface.
    */
    class V12_API Dictionary B2B_SEALED
        : public Item
    {
        friend struct DictionaryImpl;
    public:
        /** Protocol by name map*/
        typedef std::map<std::string, ProtocolT::Ref> ProtocolByNameMap;
		typedef std::list<std::string> Warnings;

    public:
        /** Constructor. */
        Dictionary();

        /** Constructor. */
        explicit Dictionary( std::string const& xmlFilePath, bool loadComments = false );

    public: // Public methods
        /** Loads protocols from either FIX Dictionary file or
        *   FIX protocol customization file.
        */
        void load( std::string const& xmlFilePath, bool loadComments = false, bool keepXmlInCommentsS = false );

        /** Loads protocols from thestring. */
        void loadFromString( std::string const& xmlString, bool loadComments = false, bool keepXmlInComments = false );

        /** Get duplicates (if any) */
        const Warnings& getWarnings() const;
		
		/** Creates new protocol using name, version and title
        * @return pointer to new created protocol.
        */
        ProtocolT::Ptr createProtocol( std::string const& name,
                                       std::string const& version,
                                       std::string const& title );

        /** Register new protocol in Dictionary
        *
        * @param protocol Protocol for registering
        * @throw Utils::Exception if protocol with given id is already registered
        */
        void registerProtocol( ProtocolT::Ptr protocol );

        /** Returns registered protocol using protocol id. If
        * protocol by given name is unavailable, method returns null.
        */
        ProtocolT::CPtr getProtocol( std::string const& name ) const throw();

        /** Returns all protocols */
        ProtocolByNameMap const* getProtocols() const throw();

    public: // Item contract
        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

    protected:
        /** Destructor */
        ~Dictionary() throw();

    private:
        Dictionary( Dictionary const& dictionary );

        /** Make copy of object */
        DictionaryT::Ptr clone() const;

    private:
        /** Implementation details */
        typedef DictionaryImpl Impl;
        Impl* impl_;
    }; // class Dictionary : public Item
} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_Dictionary_H
