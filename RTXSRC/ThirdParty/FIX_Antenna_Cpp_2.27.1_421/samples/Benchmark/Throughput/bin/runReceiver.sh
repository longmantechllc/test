#!/bin/bash

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

if [ ! -f /logs.receiver  ]; then
    mkdir logs.receiver
fi

if [ ! -f /logs.receiver/backup  ]; then
    mkdir logs.receiver/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh
	if [ "$OutputFile" = "" ]
	then 
		./Receiver engine.receiver.properties benchmark.properties
	else
		./Receiver engine.receiver.properties benchmark.properties >> $OutputFile
	fi
	sleep 5
done

