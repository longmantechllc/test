/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "FIXApp.h"

#include <cstddef>
#include <cstdio>
#include <iostream>
#include <string>
#include <memory>

using namespace std;

FIXApp::FIXApp( void )
    : execId_( 0 )
    , orderId_( 0 )
{
}

FIXApp::~FIXApp( void )
{
}

bool FIXApp::process( const Engine::FIXMessage& fixMsg, const Engine::Session& aSn )
{
    // WARNING: This method should complete as quickly as possible.
    // Do not perform time-consuming tasks inside it.

    // Just print the message to console here.
    cout << "Message in session " << *aSn.getId() << endl;
    cout << *fixMsg.toString( '|' ) << endl;
    cout << endl;

    // Process New Order Single and send Execution Reports
    if( "X" == fixMsg.type() || "F" == fixMsg.type() ) {
        char order_id_str[128];
        sprintf( order_id_str, "%d", ++orderId_ );

        char buf[128];
        Engine::Session* volatile_session = const_cast<Engine::Session*>( &aSn );

        // Send Execution Report - New
        std::auto_ptr<Engine::FIXMessage> er_new( volatile_session->newSkel( "8" ) );

        er_new->set( FIXFields::ClOrdID, Engine::UTCTimestamp::now() );
        er_new->set( FIXFields::Symbol, "MSFSX" );
        er_new->set( FIXFields::Side, '1' );
        er_new->set( FIXFields::OrderQty, 22.0f );

        sprintf( buf, "%d", ++execId_ );
        er_new->set( FIXFields::OrderID, order_id_str );
        er_new->set( FIXFields::ExecID, buf );
        er_new->set( FIXFields::OrdStatus, '0' ); // New
        er_new->set( FIXFields::ExecType, '0' );  // New
        er_new->set( FIXFields::LeavesQty, 11.0f );
        er_new->set( FIXFields::LastQty, 0 );
        er_new->set( FIXFields::LastPx, 0 );
        er_new->set( FIXFields::CumQty, 0 );
        er_new->set( FIXFields::AvgPx, 0 );
        er_new->set( FIXFields::TransactTime, Engine::UTCTimestamp::now() );

        volatile_session->put( er_new.get() );
    }

    // Return 'true' if the application can process the given message, otherwise 'false'.
    // If the application can not process the given message,
    // the FIX Engine will try to deliver it later according to "Delayed Processing Algorithm".
    return true;

}

void FIXApp::onLogonEvent( const Engine::LogonEvent* apEvent, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logon message has been received from the counterpart.

    cout << string( "Logon received for the session " ) << *aSn.getId() << ":" << *apEvent->m_pLogonMsg->toString( '|' ) << endl << endl;
}

void FIXApp::onLogoutEvent( const Engine::LogoutEvent* apEvent, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logout message has been received from the counterpart or the session was disconnected.

    cout << string( "Logout received for the session " ) << *aSn.getId();
    if( NULL != apEvent && NULL != apEvent->m_pLogoutMsg ) {
        cout << ": " << *apEvent->m_pLogoutMsg->toString( '|' );
    }
    cout << endl << endl;
}

void FIXApp::onMsgRejectEvent( const Engine::MsgRejectEvent* /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when a message have to be rejected.
}

void FIXApp::onSequenceGapEvent( const Engine::SequenceGapEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a message gap is detected in incoming messages.
}

void FIXApp::onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a session-level Reject message (MsgType = 3) is received.
}

void FIXApp::onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    hbReceived_.post();
}

void FIXApp::onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when a Resend Request (MsgType = 2) has been received.
}

void FIXApp::onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when the session has changed its state.
}

void FIXApp::onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when the message has to be routed to the session, which does not exist.
}

bool FIXApp::onResend( const Engine::FIXMessage& /*msg*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when an outgoing message is about to be resent as a reply to the incoming ResendRequest message.

    // Return 'true' if the message should be resent, otherwise 'false'
    // If the method returns 'true' then the Engine resends the message to counterpart,
    // otherwise it sends the SequenceReset Gap Fill message.
    return true;
}
