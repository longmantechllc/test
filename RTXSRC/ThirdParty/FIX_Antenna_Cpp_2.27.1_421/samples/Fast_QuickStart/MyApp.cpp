/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "MyApp.h"

#include <iostream>
#include <stdexcept>
#include <cassert>

using namespace std;
using namespace Engine;
using namespace System;

bool MyApp::process( const FIXMessage& msg, const Engine::Session& /*aSn*/ )
{
    cout << "MyApp::process: " << *msg.toString() << endl;
    ++m_nMsg;
    return true;
}

MyApp::~MyApp()
{
    if( NULL != m_pSn ) {
        m_pSn->registerApplication( NULL );
        m_pSn->release();
    }
}

void MyApp::onLogonEvent( const LogonEvent* /*apEvent*/, const Session& /*aSn*/ )
{
}

void MyApp::onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    m_sem.post();
}

void MyApp::onLogoutEvent( const LogoutEvent* /*apEvent*/, const Session& /*aSn*/ )
{
    m_bClosed = true;
    m_sem.post();
    m_ping.post();
}

void MyApp::onSequenceGapEvent( const SequenceGapEvent* /*apEvent*/, const Session& /*aSn*/ ) {}

void MyApp::onSessionLevelRejectEvent( const SessionLevelRejectEvent* /*apEvent*/, const Session& /*aSn*/ ) {}

void MyApp::waitForEstablished()
{
    assert( NULL != m_pSn );
    while( m_pSn->getState() != Session::ESTABLISHED ) {
        m_sem.wait();
    }
}

void MyApp::waitUntilSesClosed()
{
    assert( NULL != m_pSn );

    while( !m_bClosed ) {
        m_sem.wait();
    }
}

void MyApp::onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ )
{
    cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
}


void MyApp::setSession( Session* apSn )
{
    m_pSn = apSn;
}

Session* MyApp::getSession()
{
    return m_pSn;
}

void MyApp::close()
{
    if ( ! m_bClosed ) {
        m_pSn->disconnect( "The session was closed by EchoServer", true );
    }
}

bool MyApp::isSesClosed()
{
    return m_bClosed;
}

long MyApp::getNMsg()
{
    return m_nMsg;
}

void MyApp::put( Engine::FIXMessage* pMessage )
{
    if( NULL == m_pSn ) {
        throw std::logic_error( "Unable to send message to a session. Reason: session is not registered yet." );
    }
    if( NULL != FixEngine::singleton() ) {
        m_pSn->put( pMessage );
    }
}

void MyApp::onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    m_ping.post();
}

void MyApp::waitForTestRequestResponse()
{
    if (! m_bClosed)
        m_ping.wait();
}

void MyApp::ping( std::string const& msg )
{
    if (! m_bClosed)
        m_pSn->ping( msg );
}
