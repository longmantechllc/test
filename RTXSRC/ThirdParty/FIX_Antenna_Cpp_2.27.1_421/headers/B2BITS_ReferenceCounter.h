// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_ReferenceCounter.h
/// Contains Utils::ReferenceCounter class declaration.

#ifndef _B2BITS_ReferenceCounter__h_
#define _B2BITS_ReferenceCounter__h_

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>

#include <cstddef>

/// FIX Antenna Utils framework
namespace Utils
{

    /**
     * A generic reference counter.
     */
    class V12_API ReferenceCounter
    {
    private:
        /** The counter. */
        volatile mutable System::u32 m_nRef;

    public:
        /** Constructor. */
        ReferenceCounter() throw();

    protected:
        /** Destructor. */
        virtual ~ReferenceCounter();

    public:
        /**
         * Increments reference counter.
         * @return Return value should be ignored.
         */
        virtual bool addRef() const throw();

        /**
         * Decrements reference counter. If it becomes equal to 0 then calls destructor.
         * @return 0 if there are no more references to this object; otherwise any positive value.
         */
        virtual long release() const throw();

        /**
         * Returns the current value of the reference counter.
         */
        virtual long getNRef() const throw();

    protected:
        /** Decrements counter and returns new counter value. */
        long decrement() const throw();

        /** Increments counter and returns new counter value. */
        long increment() const throw();

    private:
         ReferenceCounter( ReferenceCounter const& );
         ReferenceCounter& operator=( ReferenceCounter const& );
    };

    template <typename T>
    class ReferenceCounterSharedPtr
    {
    private:
        /// Pointer to the owned object.
        T* ptr_;
    public: // methods
            /// Default constructor.
        ReferenceCounterSharedPtr() throw()
            : ptr_( NULL ) {
        }

        /// Constructor.
        /// @param ptr Pointer to the object to be owned.
        /// Takes ownership over the object. DOESN'T call addRef()!
        explicit ReferenceCounterSharedPtr( T* ptr ) throw()
            : ptr_( ptr ) {
        }

        /// Copy constructor.
        ReferenceCounterSharedPtr( ReferenceCounterSharedPtr<T> const& ptr ) throw()
            : ptr_( ptr.ptr_ ) {

            if (ptr_)
                ptr_->addRef();
        }

        /// Copy constructor.
        template<typename U> explicit ReferenceCounterSharedPtr( ReferenceCounterSharedPtr<U> const& ptr ) throw()
            :ptr_(NULL) {
            T* pTyped = NULL;
            pTyped = dynamic_cast<T*>(ptr.get());

            if (pTyped)
                pTyped->addRef();

            reset( pTyped );
        }

        /// Copy operator.
        ReferenceCounterSharedPtr& operator = (ReferenceCounterSharedPtr<T> const& ptr ) throw() {
            if (ptr.ptr_)
                ptr.ptr_->addRef();
            
            reset( ptr.ptr_ );

            return *this;
        }

        /// Takes ownership over the object. DOESN'T call addRef()!
        ReferenceCounterSharedPtr& operator = ( T* ptr ) throw() {
            reset( ptr );

            return *this;
        }

        /// Destructor.
        ~ReferenceCounterSharedPtr() throw() {
            if (ptr_)
                ptr_->release();
        }

        /// Takes ownership over the object. DOESN'T call addRef()!
        void reset( T* ptr = NULL ) throw() {
            if (ptr_)
                ptr_->release();

            ptr_ = ptr;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T* get() const throw() {
            return ptr_;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T* get() throw() {
            return ptr_;
        }

        /// Overloaded operator*.
        T& operator*() throw() {
            return *ptr_;
        }

        /// Overloaded operator*.
        T& operator*() const throw() {
            return *ptr_;
        }

        /// Overloaded operator->.
        T* operator->() throw() {
            return get();
        }

        /// Overloaded operator->.
        T* operator->() const throw() {
            return get();
        }

        /// Overloaded operator!.
        bool operator!() const  throw() {
            return NULL == ptr_;
        }

        /// Overloaded operator bool().
        operator bool() const  throw() {
            return NULL != ptr_;
        }

    };

} // namespace Utils

#endif
