/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "MySessionsManager.h"

#include <memory>
#include <algorithm>
#include <stdexcept>

#include <B2BITS_Guard.h>
#include <sstream>

using namespace std;
using namespace Engine;
using namespace System;
using namespace Utils;

typedef Guard<System::Mutex> MGuard;

MySessionsManager::MySessionsManager( bool isEchoMode )
    : logger_(Utils::Log::LogSystem::singleton()->createCategory( "Session Manager" )),
    isEchoMode_(isEchoMode),
	autoRestartSessions_(true)
{
}

bool MySessionsManager::onUnregisteredAcceptor( Engine::Session* apSn, const FIXMessage& /*logonMsg*/ )
{
    const string* pSID = apSn->getId();

    // put application to the internal map
    std::pair<MyApps::iterator, bool> insert_result;

    {
        MGuard l( lock_ );
        insert_result = apps_.insert( std::make_pair( *pSID, static_cast<MyApp*>( NULL ) ) );

        if( insert_result.second ) {
            std::auto_ptr<MyApp> pApp( new MyApp( isEchoMode_ ) );
            insert_result.first->second = pApp.release();
        }
    }

    // assign  session with application
    bool result = insert_result.first->second->setSession( apSn );

    if( !result ) {
        if(logger_)
            logger_->note( "Incoming connection rejected: " + *pSID );
    }

    return result;
}

bool MySessionsManager::onIncomingConnection(const Engine::FIXMessage& logonMsg, const System::IPAddr& remoteAddr, int remotePort, int localPort)
{
    if (logger_)
    {
        int size = 0;
        const char* msg = logonMsg.toRaw(&size);
        std::stringstream strstream;
        strstream << "Incoming connection from " << int(remoteAddr.a1) << '.'
                                                 << int(remoteAddr.a2) << '.'
                                                 << int(remoteAddr.a3) << '.'
                                                 << int(remoteAddr.a4) << ':'
                                                 << remotePort 
                                                 << " to " << localPort
                                                 << " with logon: " + std::string(msg, size);
        logger_->note(strstream.str());
    }
    return true;
}

bool MySessionsManager::onSessionTerminated( Engine::Session* /*apSn*/, int& restartDelayMs )
{
	restartDelayMs = 0;
	return autoRestartSessions_;
}

void MySessionsManager::showStat()
{
    MGuard l( lock_ );

    int appNum = 1;
    long sumAppMsgNum = 0;

    for( MyApps::iterator i = apps_.begin(); i != apps_.end(); ++i ) {
        RefCounter<Session> ssn = i->second->getSession();

        if( NULL == ssn.get() ) {
            continue;
        }

        long curAppMsgNum = i->second->getNMsg();

        if(logger_)
        {
            std::stringstream strstream;
            strstream << "Session <" << *ssn->getId() + ">: " << curAppMsgNum << " messages were sent";
            logger_->note( strstream.str() );
        }
        sumAppMsgNum += curAppMsgNum;

        ++appNum;
    }
    if(logger_)
    {
        std::stringstream strstream;
        strstream << "Sum: " << sumAppMsgNum << " messages";
        logger_->note( strstream.str() );
    }
}

void MySessionsManager::showEngineStat()
{
    Statistics stat;
    FixEngine::singleton()->getStatistics( &stat );
    if(logger_)
    {
        std::stringstream strstream;
        strstream << endl << "Number of sessions:" << endl
            << "acceptors: " << stat.m_nAcceptors << endl
            << "active: " << stat.m_nActive << endl
            << "initiators: " <<  stat.m_nInitiators << endl
            << "correctly terminated: " <<  stat.m_nCorrectlyTerminated << endl
            << "non-gracefully terminated: " <<  stat.m_nNonGracefullyTerminated;
        logger_->note(strstream.str());
    }
}

void MySessionsManager::closeSessions()
{
    MGuard l( lock_ );

    for( MyApps::iterator i = apps_.begin(); i != apps_.end(); ++i ) {
        i->second->close();
    }

    for( MyApps::iterator i = apps_.begin(); i != apps_.end(); ++i ) {
		i->second->getSession()->waitForTerminated();
        i->second->resetSession();
    }

    apps_.clear();
}

std::string MySessionsManager::createAcceptor( const std::string& sender, const std::string& target, Engine::FIXVersion ver )
{
    std::auto_ptr<MyApp> pApp( new MyApp( isEchoMode_ ) );
    RefCounter<Session> pSn( FixEngine::singleton()->createSession( pApp.get(), sender, target, ver ), false );
    pApp->setSession( pSn.get() );

    const string* pSID = pSn->getId();
    assert( NULL != pSID );
    string sid = *pSID;

    {
        MGuard l( lock_ );
        apps_.insert( MyApps::value_type( sid, pApp.release() ) );
    }

    pSn->connect();

    return sid;
}

std::string MySessionsManager::createSession(const Engine::SessionId& sessionId, const Engine::SessionExtraParameters& params)
{
    std::auto_ptr<MyApp> pApp( new MyApp( isEchoMode_ ) );
	RefCounter<Session> pSn( FixEngine::singleton()->createSession( pApp.get(), sessionId, Engine::NA, &params ), false );
    pApp->setSession( pSn.get() );

	const string* pSID = pSn->getId();
    assert( NULL != pSID );
    string sid = *pSID;

    {
        MGuard l( lock_ );
        apps_.insert( MyApps::value_type( sid, pApp.release() ) );
    }

    pSn->connect();

    return sid;
}

bool MySessionsManager::isSessionExist(const Engine::SessionId& sessionId)
{
	for(MyApps::const_iterator it = apps_.begin(); it != apps_.end(); ++it)
	{
		if(it->second->getSession()->getSessionId() == sessionId)
			return true;
	}
	return false;
}

void MySessionsManager::put( const string& sid, FIXMessage* pMessage )
{
    MGuard l( lock_ );
    MyApps::iterator it = apps_.find( sid );
    if( apps_.end() == it ) {
        throw std::logic_error( "Session with ID '" + sid + "' does not exists." );
    }
    it->second->put( pMessage );
}
