// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FastCodec.h
/// Contains Engine::FastCodec class declaration.

#ifndef H_B2BITS_Engine_FastCodec_H
#define H_B2BITS_Engine_FastCodec_H

#include <memory>
#include <map>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_FastCoder.h>
#include <B2BITS_FastDecoder.h>
#include <B2BITS_FastScp11Decoder.h>
#include <B2BITS_FastMappingOptions.h>

/// Platform specific classes
namespace System {}

/// Useful classes and methods
namespace Utils {}

/**
* FIX Engine namespace.
*
* Contains all FIX related classes (FIXMessage, FIXGroup etc).
*/
namespace Engine
{
    /**
    * A factory for FastCoder and FastDecoder.
    */
    class V12_API FastCodec
    {
    public:
        /**
        * Constructor
        *
        * @param[in] pathToLicense - a path to a license file
        * @throw an exception if the specified license is not valid
        */
        FastCodec( const std::string& pathToLicense );
        ~FastCodec();

        /**
        * Creates FastCoder
        * @param[in] pathToAdditionalXml - path to additional xml; can be empty
        * @param[in] pathToTemplatesXml - path to a xml file which contains templates
        * @param[in] scpVer - a trasnport protocol version
        * @param[in] appVer - an application protocol version
        * @param[in] mappingOptions - mapping options of coder
        * @return - new instance of FastCoder class
        */
        std::auto_ptr<FastCoder> createFastCoder(
            const std::string& pathToAdditionalXml,
            const std::string& pathToTemplatesXml,
            FIXVersion scpVer,
            FIXVersion appVer,
            const FastMappingOptions& mappingOptions = FastMappingOptions()
            );

        /**
        * Creates FastCoder
        * @param[in] pathToAdditionalXml - path to additional xml; can be empty
        * @param[in] pathToTemplatesXml - path to a xml file which contains templates
        * @param[in] scpProtocolName - a trasnport protocol name
        * @param[in] appProtocolName - an application protocol name
        * @param[in] mappingOptions - mapping options of coder
        * @return - new instance of FastCoder class
        */
        std::auto_ptr<FastCoder> createFastCoder(
            const std::string& pathToAdditionalXml,
            const std::string& pathToTemplatesXml,
            const std::string& scpProtocolName,
            const std::string& appProtocolName,
            const FastMappingOptions& mappingOptions = FastMappingOptions()
            );

        /**
        * Creates FastDecoder
        * @param[in] pathToAdditionalXml - path to additional xml; can be empty
        * @param[in] pathToTemplatesXml - path to a xml file which contains templates
        * @param[in] scpVer - a trasnport protocol version
        * @param[in] appVer - an application protocol version
        * @param[in] mappingOptions - mapping options of decoder
        * @return - new instance of FastDecoder class
        */
        std::auto_ptr<FastDecoder> createFastDecoder(
            const std::string& pathToAdditionalXml,
            const std::string& pathToTemplatesXml,
            FIXVersion scpVer,
            FIXVersion appVer,
            const FastMappingOptions& mappingOptions = FastMappingOptions()
            );

        /**
        * Creates FastDecoder
        * @param[in] pathToAdditionalXml - path to additional xml; can be empty
        * @param[in] pathToTemplatesXml - path to a xml file which contains templates
        * @param[in] scpProtocolName - a trasnport protocol name
        * @param[in] appProtocolName - an application protocol name
        * @param[in] mappingOptions - mapping options of decoder
        * @return - new instance of FastDecoder class
        */
        std::auto_ptr<FastDecoder> createFastDecoder(
            const std::string& pathToAdditionalXml,
            const std::string& pathToTemplatesXml,
            const std::string& scpProtocolName,
            const std::string& appProtocolName,
            const FastMappingOptions& mappingOptions = FastMappingOptions()
            );

        /**
        * Functions generator used to create FastScp11::Decoder.
        *
        * @param[in] template_fn - path to fast template file
        * @param[in] protocol_customization_fn - semicolon separated list of xml files that contains description of FIX session protol messages and FIX market data protocol messages
        * @param[in] scp_protocol_ver - name of the FIX SCP protocol (FIXT11 usual)
        * @param[in] app_protocol_ver - name of the FIX protocol (FIX40, FIX41, ... FIX50SP1CME)
        * @param[in] listener - listener of "on scp message" events (OnFastHello, OnFastAlert, OnFastReset). Can be NULL value. FastScp11::Decoder resets Dictionaris before calling OnFastReset.
        * @param[in] mappingOptions - mapping options of decoder
        * @return instance of FastScp11::Decoder class
        * @throw - an std::exception with detailed information if an error occurs.
        */
        std::auto_ptr<FastScp11::Decoder> createFastScp11Decoder(
            std::string const& template_fn,
            std::string const& protocol_customization_fn,
            std::string const& scp_protocol_ver,
            std::string const& app_protocol_ver,
            FastScp11::Decoder::Listener* listener,
            const FastMappingOptions& mappingOptions = FastMappingOptions()
            );


    protected:
        FastCodec( const FastCodec& );
        FastCodec& operator =( const FastCodec& );

    private:
        class Impl;
        Impl* impl_;
    }; // FastCodec
}
#endif // H_B2BITS_Engine_FastCodec_H
