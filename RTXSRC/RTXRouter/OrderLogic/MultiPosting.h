//
// Created by schen on 5/19/2021.
//
#ifndef RTXROUTER_MULTIPOSTING_H
#define RTXROUTER_MULTIPOSTING_H

#include <queue>
//#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "IOrderLogicBase.h"

using namespace std;

class ExecutionReport;
class RTXOrder;
struct MultiPostRoute;

class xTimer;
class TimerScheduler;

class MultiPosting : public IOrderLogicBase
{
public:
    MultiPosting() =default;
    MultiPosting(RTXOrder* order);
    ~MultiPosting() override;
    bool InitOrder() override;

    bool ProcessOrder() override;
    virtual bool SendChildOrder(std::shared_ptr<Order> order) override;
    bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child) override;
    bool ProcessMktData() override;

private:
    bool IsRouteEligible(const std::string& routeName);
    bool IsEligible();
    bool CancelPostedChildOrder();

    xTimer* m_cancelPostedChildOrderTimer = nullptr;
    void StartCancelPostedChildOrderTimer();
    void FireCancelPostedChildOrderTimer(xTimer& timer);

    std::vector<MultiPostRoute*> m_mPostRoutes;
    std::unordered_map<std::string/*routeName*/, std::string /*coid*/> m_postedRoute;
    std::queue<std::string> m_fulfilledRoute;
    std::unordered_set<std::string> m_tryCancellingRoute;
};


#endif //RTXROUTER_MULTIPOSTING_H
