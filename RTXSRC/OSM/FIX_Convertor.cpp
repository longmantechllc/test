#include <fstream>
#include <stdint.h>

#include "FIX_Convertor.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "Util.h"


using namespace std;
using namespace ::OSM;

//-------------------------------------------------------------------------
// Order convertors
//-------------------------------------------------------------------------
// SOR child order to GW, no account, subId
void FIX_Convertor::fromRTX(const Order& ord, FIX44::NewOrderSingle& gwMsg, const VirtualDestMap& virtualDestMap)
{
    try {
        gwMsg.set(FIX::ClOrdID(ord.childOrderId_));
        gwMsg.set(FIX::Symbol(ord.symbol_));

        gwMsg.set(FIX::TimeInForce(ord.TIF_+'0'));
        gwMsg.set(FIX::Side(ord.side_+'0'));
        gwMsg.set(FIX::OrdType(ord.type_+'0'));

        gwMsg.set(FIX::OrderQty(ord.size_));
        gwMsg.set(FIX::Price(ord.price_.value()));


        if (ord.displaySize_)
            gwMsg.set(FIX::MaxFloor(ord.displaySize_));
        if (ord.minQty_)
            gwMsg.set(FIX::MinQty(ord.minQty_));
        if (!ord.execInst_.empty())
            gwMsg.set(FIX::ExecInst( ord.execInst_));
        if (ord.expireTime_)
            gwMsg.set(FIX::ExpireTime(ord.expireTime_));
        if (!ord.exchange_.empty()) {
            const auto itor = virtualDestMap.find(ord.exchange_);
            if (itor != virtualDestMap.end())
                gwMsg.set(FIX::ExDestination(itor->second.dest));
            else
                gwMsg.set(FIX::ExDestination(ord.exchange_));
        }
        if (ord.discretion_ > Price())
            gwMsg.set(FIX::DiscretionOffsetValue(ord.discretion_.value()));
        auto t  = FIX::UtcTimeStamp();
        gwMsg.set(FIX::TransactTime(t));
        ord.transactTime_ = t.getTimeT();

    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught some EX");
    }
}

void
FIX_Convertor::fromOSM(const OSMOrder& ord, FIX44::OrderCancelRequest& gwMsg)
{
    gwMsg.set(FIX::OrderID(ord.exOrderId));
    gwMsg.set(FIX::ClOrdID(ord.clOrdId));
    gwMsg.set(FIX::OrigClOrdID(ord.oClOrdId));
    gwMsg.set(FIX::Symbol(ord.symbol));
    gwMsg.set(FIX::Side(ord.side+'0'));
    gwMsg.set(FIX::TransactTime());
}

// Client New order into OMS Order
void FIX_Convertor::toOSM(const FIX44::NewOrderSingle& msg, OSMOrder& ord)
{
    try  {
        ord.clOrdId = getFieldValue<FIX::ClOrdID>(msg);
        getFieldValueIf<FIX::Account>(msg, ord.account);
        getFieldValueIf<FIX::SenderSubID>(msg.getHeader(), ord.subAccount);
        getFieldValueIf<FIX::OnBehalfOfSubID>(msg.getHeader(), ord.logInId);
        ord.symbol = getFieldValue<FIX::Symbol>(msg);

        ord.tif = getFieldValue<FIX::TimeInForce>(msg) -'0';
        ord.side = getFieldValue<FIX::Side>(msg) -'0';
        ord.type = getFieldValue<FIX::OrdType>(msg) -'0';

        ord.qty = getFieldValue<FIX::OrderQty>(msg);
        if(ord.type != 1)  // none market order FIX::OrdType_MARKET
            ord.price = Price(getFieldValue<FIX::Price>(msg));
        // new fields
        getFieldValueIf<FIX::MinQty>(msg, ord.minQty);
        getFieldValueIf<FIX::MaxFloor>(msg, ord.displaySize);
        getFieldValueIf<FIX::DiscretionOffsetValue>(msg, ord.discretion);

        FIX::ExpireTime expirationTime;
        if (msg.isSetField(expirationTime)) {
            msg.get(expirationTime);
            //ord.expirationTime = expirationTime.getValue().getTimeT();
            auto expTime = expirationTime.getValue();
            int yy, mm, dd, hh, mi, sec, milli;
            expTime.getYMD(yy,mm,dd);
            expTime.getHMS(hh,mi,sec, milli);
            ord.expirationTime = ((((long)yy*100+mm)*100 + dd) *100 + hh)*100+mi;
            ord.expirationTime = (ord.expirationTime * 100 +sec)*1000+milli;
            /*logger << "expirationTime yy=" << yy << " ,mm=" << mm << ", dd=" << dd
                   << ", hh=" << hh << ", mi=" << mi << ", sec=" << sec
                   << ", ord.expirationTime = " << ord.expirationTime << endl;*/
        }

        getFieldValueIf<FIX::ExecInst>(msg,ord.execInst);
        ord.transactTime = FIX::UtcTimeStamp().getTimeT();

    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught some EX");
    }
}

//-------------------------------------------------------------------------
// Execution Report convertors
//-------------------------------------------------------------------------
// SOR ER  back to client
void FIX_Convertor::fromRTX(const ExecutionReport& er, FIX44::ExecutionReport& erMsg)
{
    try {
        erMsg.set(FIX::OrderID(to_string(er.orderId_)));
        if (!er.accountId_.empty())
            erMsg.set(FIX::Account(er.accountId_));
        if (!er.subAccount_.empty())
            erMsg.getHeader().set(FIX::SenderSubID(er.subAccount_));
        if (!er.logInId_.empty())
            erMsg.getHeader().set(FIX::OnBehalfOfSubID(er.logInId_));

        erMsg.set(FIX::ExecID(er.execId_));
        erMsg.set(FIX::Symbol(er.symbol_));

        erMsg.set(FIX::TimeInForce(er.TIF_ +'0'));
        erMsg.set(FIX::Side(er.side_+'0'));
        erMsg.set(FIX::OrdType(er.type_+'0'));
        erMsg.set(FIX::OrdStatus(er.status_+'0'));
        erMsg.set(FIX::ExecType(er.execType_));
        erMsg.set(FIX::LastQty(er.fillSize_));
        erMsg.set(FIX::CumQty(er.cumulativeFillQuantity_));
        erMsg.set(FIX::LeavesQty(er.leaveQty));

        erMsg.set(FIX::Price(er.price_.value()));
        erMsg.set(FIX::LastPx(er.fillPrice_.value()));
        erMsg.set(FIX::AvgPx(er.avgPrice_.value()));
        erMsg.set(FIX::TransactTime());
    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught some EX");
    }
}

// GW ER back to SOR
void FIX_Convertor::toRTX(const FIX44::ExecutionReport& msg, ExecutionReport& er)
{
    try  {
        er.symbol_ =  getFieldValue<FIX::Symbol>(msg);
        er.status_ = getFieldValue<FIX::OrdStatus>(msg)-'0';
        er.execType_ = getFieldValue<FIX::ExecType>(msg);
        if (er.status_ == ERSTATUS::FILLED || er.status_ == ERSTATUS::PARTIALY_FILLED) {
            er.fillSize_ = getFieldValue<FIX::LastQty>(msg);
            er.fillPrice_ = Price(getFieldValue<FIX::LastPx>(msg));
        }
        else {
            er.fillSize_ = 0;
            er.fillPrice_ = 0;
        }

        er.cumulativeFillQuantity_ = getFieldValue<FIX::CumQty>(msg);
        er.leaveQty = getFieldValue<FIX::LeavesQty>(msg);
        er.avgPrice_ = Price(getFieldValue<FIX::AvgPx>(msg));
        er.execId_ = getFieldValue<FIX::ExecID>(msg);
        er.transactTime_ = FIX::UtcTimeStamp().getTimeT();

    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught some EX");
    }
}
