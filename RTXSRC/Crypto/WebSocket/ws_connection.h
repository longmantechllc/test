#pragma once

#include <thread>
#include <utility>
#include <libwebsockets.h>
#include <memory>
#include <queue>
#include <chrono>

#include "common.h"
#include "WebSocket.h"
#include "Utils/l2_book.h"

using namespace std::chrono;

namespace Crypto {

class WsConnection {
 public:
    enum State {
        INIT = -1,
        CONNECTING,
        CONNECTED,
    };
    
 protected:
    enum Action {
        CONNECT = 0,
        BEGIN,
        CONNECT_TIMEOUT,
        CONNECT_SUCCESS,
        RECV_ERROR
    };

    std::shared_ptr<WebSocket> engine;
    struct lws *connection = nullptr;
    std::string host = "";
    std::int64_t port = 0;
    std::int64_t current_cycle = 0;
    std::string path = "";
    uint32_t ws_index_ = 0;
    std::vector<std::string> send_buffer;

    milliseconds connect_timeout;
    milliseconds connect_retry_delay;
    int connect_retry_count = 0;
    l2_book* book_ctrl_ = nullptr;

    on_message_t on_message_cb_;
    on_ws_pong_t on_ws_pong_cb_;
    on_connect_t on_connect_cb_;
    on_disconnect_t on_disconnect_cb_;
    build_subscription_msg_t build_subscription_msg;
    std::queue<Action> input_queue;

    State state = INIT;
    std::pair<milliseconds, time_point<system_clock>> backoff_time;

    void on_timeout(milliseconds timeout, Action action, std::int64_t cycle);

    void update_state_machine(Action action, time_point<system_clock> tp = system_clock::now());

    void update_backoff_time();

    void state_machine();
    void subscribe() {
        auto subs = this->build_subscription_msg();
        for(auto& req : subs) {
            this->send_message(req);
        }
    }

 public:
    bool is_connected_ = false;
    bool m_running = true;

    WsConnection(on_message_t on_message_cb,
        //on_ws_pong_t on_ws_pong_cb,
        on_connect_t on_connect_cb,
        on_disconnect_t on_disconnect_cb,
        build_subscription_msg_t build_subscription_msg,
        std::shared_ptr<WebSocket> engine,
        std::string path,
        std::string host,
        int port,
        int ws_index,
        milliseconds connect_timeout,
        milliseconds connect_retry_delay,
        int connect_retry_count);

    ~WsConnection();

    void send_message(const std::string &message);

    void on_message(struct lws *wsi, std::string_view);

    void on_ws_pong(struct lws *wsi);

    void on_connect(struct lws *);

    void on_disconnect();

    void connect();

    void reconnect();

    void disconnect();

    bool flush_send_buffer(lws *wsi);

    // Get connection info and state
    const std::string& get_host() const;
    const std::string& get_path() const;
    uint16_t get_port() const;
    State get_state() const;
    const struct lws* get_lws_connect() const;
    
    uint32_t get_index();
    bool is_connected() { return is_connected_; }
    void set_book_ctrl(l2_book* book_ctrl) {
        book_ctrl_ = book_ctrl;
    }
    void service();
};

inline uint32_t WsConnection::get_index() {
    return ws_index_;
}

}  // namespace Crypto
