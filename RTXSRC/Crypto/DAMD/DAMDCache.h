#pragma once

#include <stdint.h>
#include <unordered_map>
#include <unordered_set>
//#include <shared_mutex>
#include <mutex>
#include <cassert>
#include <memory>
#include <thread>
#include <atomic>

#include "quickfix/FileStore.h"
#include "quickfix/FileLog.h"
//#include "quickfix/SocketInitiator.h"
#include "quickfix/SocketAcceptor.h"
#include "quickfix/Log.h"
#include "quickfix/SessionSettings.h"
#include "quickfix/Session.h"
#include "quickfix/Application.h"
#include "quickfix/MessageCracker.h"
#include "quickfix/fix44/IOI.h"

#include <zmq.hpp>

#include "ApplicationProperties.h"

class Acceptor;

struct IOIData
{
    //std::string mmid;
    double pi {0.0};
    double pfof{0.0};
    double size{0.0};
    uint64_t timeStamp{0};
};

namespace FIX {
    namespace FIELD {
        const int IoiPi = 31370;
        const int IoiPfof = 31460;
    }
    USER_DEFINE_PRICE(IoiPi, FIELD::IoiPi);
    USER_DEFINE_PRICE(IoiPfof, FIELD::IoiPfof);
};

/// Application implementation (processes the incoming messages).
class DAMDCache : public FIX::Application, public FIX::MessageCracker
{
public:
    explicit DAMDCache(const ApplicationProperties& params);
    ~DAMDCache();

    // Application overloads
    void onCreate( const FIX::SessionID& ) override;
    void onLogon( const FIX::SessionID& sessionID ) override;
    void onLogout( const FIX::SessionID& sessionID ) override;
    void toAdmin( FIX::Message&, const FIX::SessionID& ) override;
    void toApp( FIX::Message&, const FIX::SessionID& )
        EXCEPT ( DoNotSend )  override;
    void fromAdmin( const FIX::Message&, const FIX::SessionID& )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon ) override;
    void fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType ) override;


    void stopAcceptor();

    bool isInited() {return m_init;}
    auto& getIoiCache() { return m_damdCache;}
    auto& getMutex() { return m_mutex;}
    std::vector<std::pair<std::string, IOIData>> getRLP(std::string symbol);

private: // Engine::Application contract implementation
    template<typename T>
    static auto getField(const auto& msg) {
        T field;
        msg.get(field);
        return field;
    };

    template<typename T>
    static auto getFieldValue(const auto& msg) {
        T field;
        msg.get(field);
        return field.getValue();
    };
    template<typename T>
    static auto getFieldValueIf(const auto& msg, auto& var) {
        T field;
        if (msg.isSetField(field)) {
            msg.get(field);
            var = field.getValue();
        }
    };
    void loadDataFile();
    void loadMMID();
    void loadRLPids();
    void loadSymbolFile();

    // MessageCracker overloads
    void onMessage( const FIX44::IOI& msg, const FIX::SessionID& sessionId) override;

    const ApplicationProperties& params_;

    std::unique_ptr<FIX::SocketAcceptor> m_initiator;
    //std::unique_ptr<FIX::SocketInitiator> m_initiator;
    FIX::SessionID session_, cliSession_;
    bool m_init{};
    std::mutex m_mutex;
    //symbol,side -> exchange -> data
    std::unordered_map<std::string, std::unordered_map<std::string, IOIData>> m_damdCache;
    //std::unordered_map<std::string, std::string> m_session2Exch;
    std::unordered_map<std::string, std::tuple<std::string, double, double>> m_session2Exch;
    std::unordered_map<std::string, std::pair<double, double>> m_exchPfof;
    std::unordered_map<std::string, std::unordered_map<std::string, std::string>> m_ioiIds;
    std::unordered_set<std::string> m_symbols;
    std::unique_ptr<Acceptor> m_acceptor;
    std::thread m_acceptorThread;
    std::thread m_randomizerThread;
    std::atomic<bool> m_randomizerStop{false};

    std::atomic<uint32_t> m_msgNum {0};
    std::thread m_loggingThread;
    std::atomic<bool> m_loggingRunning {true};
};


