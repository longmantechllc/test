// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#ifndef H_B2BITS_MarketDataStorage_H
#define H_B2BITS_MarketDataStorage_H

#include <vector>
#include <cstring>

#include <B2BITS_CqgDefines.h>

namespace MDUpdateAction
{
    static char const Insert    = '0';
    static char const Update    = '1';
    static char const Delete    = '2';
} // namespace MDUpdateAction {

/// Instrument description and real time statistics
struct MarketDataStorage {
    //---------------------------------------------------
    //
    // Nested types
    //
    //---------------------------------------------------
    /// Encapsulates price of the instrument and size.
    struct PriceSize {
        /// Price
        double price_;

        /// Size
        double size_;
    }; // struct PriceSize

    /// Book
    struct Book {
        /// One side. Either bid or ask.
        struct Side {
            enum {
                MAX_LEVEL_COUNT = 1000 ///< Maximum number of levels in the book
            };

            struct Action {
                Action( char action, PriceSize priceSize, size_t level )
                    : price_( priceSize )
                    , level_( level )
                    , action_( action )
                {}

                PriceSize price_;
                size_t level_;
                char action_;
            };

            Side() 
                : bookSize_( 0 ) 
            {
                memset( &prices_, 0, sizeof(prices_) );
            }

            /// Prices
            PriceSize prices_[ MAX_LEVEL_COUNT ];
            size_t bookSize_;
            std::vector<Action> actions_;

            void action( char actionCode, PriceSize price, size_t index );
            void reserveActions( size_t size );

            /// Inserts new price
            void insert( PriceSize price, size_t index );

            /// Inserts new price
            void insert( double price, double size, size_t index );

            /// Deletes price
            void remove( size_t index );

            void update( PriceSize price, size_t index, bool isIncrement = true );
            void update( double price, double size, size_t index, bool isIncrement = true );


            /// Resets object to the initial state
            void reset();

        }; // struct Side

        /// Maximum size of the book
        size_t maxSize_;

        /// Bid side
        Side bid_;

        /// Ask side
        Side ask_;

        bool isAskAction_;

        static size_t const deltixMaxDepth = 10;

        /// Default constructor
        Book()
            : maxSize_( Side::MAX_LEVEL_COUNT ) 
            , bid_()
            , ask_()
            , isAskAction_()
        {
        }

        /// Resets book to the initial state
        const std::vector<MarketDataStorage::Book::Side::Action>& actions() const;
        void reserveActions( size_t size );
        void reset();
    }; // struct Book

    class Trade: public PriceSize
    {
    public:
        int count_;
        double maxPrice_;
        double minPrice_;

        Trade()
            : count_( 0 )
            , maxPrice_( 0 )
            , minPrice_( 0 ) {
        }

    };

    struct TradeInformation {
        /// Last trade
        Trade trade_;

        /// Trading session high price
        double tradingSessionHighPrice_;

        /// Trading session low price
        double tradingSessionLowPrice_;

        /// Opening price
        double openingPrice_;

        /// Previous day closing price
        double prevClosingPrice_;

        /// Theoretical closing price
        double theoreticalClosingPrice_;

        /// Settlement price
        double settlementPrice_;

        /// Prior
        double prior_;

        /// Total trade volume
        double totalTradeVolume_;

        /// Just for testing purpose
        int totalTradeVolumeCount_;

        /// Open interest
        double openInterest_;

        /// Resets instance to the initial state
        void reset();
    }; // struct TradeInformation

    //---------------------------------------------------
    //
    // Fields
    //
    //---------------------------------------------------
    /// Security Description
    Cqg::SecurityDescription SecurityDescription_;

    /// SecirityID
    Cqg::SecurityID securityID_;

    std::string symbol_;

    /// DisplayFactor
    double displayFactor_;

    /// Book
    Book book_;

    /// Implied book
    Book impliedBook_;

    /// Trade Information
    TradeInformation tradeInformation_;

    int status_;

    //---------------------------------------------------
    //
    // Methods
    //
    //---------------------------------------------------

    /// Default constructor
    MarketDataStorage( Cqg::ASecurityDescription secDesc )
        : SecurityDescription_( secDesc )
        , securityID_( 0 )
        , displayFactor_( 1.0 ) {
        reset();
    }

    /// Resets Market data storage to the initial state
    void reset();
}; // struct MarketDataStorage


#endif // H_B2BITS_MarketDataStorage_H
