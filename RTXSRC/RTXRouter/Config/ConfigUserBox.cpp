//
// Created by schen on 11/16/2020.
//

#include <string>
#include <iomanip>
#include <unordered_map>
#include <unordered_set>
#include <vector>
#include <memory>
#include <climits>
#include <iostream>

#include "../include/Price.h"

#include "RTXUtils.h"
#include "ConfigUserBox.h"

User::User()
{
    InitArgo();
}

void User::AddNameList(std::string val)
{
    std::vector<std::string> names = RTXUtils::stringToVector( val, "," );
    for(auto nm : names)
        userNameList.insert(nm);
}

void User::AddExcludeVenue(ConfigUserBox* prnt, std::string val)
{
    std::vector<std::string> venue = RTXUtils::stringToVector( val, "," );
    for(auto vn : venue) {
        excludeVenueList.insert(vn);
//        prnt->excludeVenueDict[vn].insert(userNameList.begin(), userNameList.end());
    }
}

std::string User::AddExcludeVenue(std::string val)
{
    std::string returnStr;
    std::string addedVenue;
    std::vector<std::string> venue = RTXUtils::stringToVector( val, "," );
    for(auto vn : venue) {
        auto itr = excludeVenueList.find(vn);
        if (itr == excludeVenueList.end()) {
            excludeVenueList.insert(vn);
            addedVenue += (vn) + ",";
        }
    }
    if (!addedVenue.empty())
        returnStr += addedVenue + " excluded venue added.";
    return returnStr;
}

std::string User::RemoveExcludeVenue(std::string val)
{
    std::string returnStr;
    std::string removedVenue;
    std::vector<std::string> venue = RTXUtils::stringToVector( val, "," );
    for(auto vn : venue)
    {
        auto itr = excludeVenueList.find(vn);
        if(itr != excludeVenueList.end()) {
            itr = excludeVenueList.erase(itr);
            removedVenue += (vn) + ",";
        }
    }
    if(!removedVenue.empty())
        returnStr += removedVenue + " excluded venue removed.";

    return returnStr;
}

void User::AddExcludeRoute(ConfigUserBox* prnt, std::string val)
{
    std::vector<std::string> rts = RTXUtils::stringToVector( val, "," );
    for(auto rt : rts) {
        excludeRouteList.insert(rt);
//        prnt->excludeRouteDict[rt].insert(userNameList.begin(), userNameList.end());
    }
}

void User::AddExcludeRetailMmid(ConfigUserBox* prnt, std::string val)
{
    std::vector<std::string> rts = RTXUtils::stringToVector( val, "," );
    for(auto rt : rts) {
        excludeRetailMmidList.insert(rt);
    }
}

unsigned int  User::SetSessionId(const std::string& val)
{
    //1="1", 2="2", 3="3", 4="1,2", 5="2,3", 6="1,2,3"
    if(val=="1")
        sessionId = 1;
    else if(val=="2")
        sessionId = 2;
    else if(val=="3")
        sessionId = 3;
    else if(val=="1,2")
        sessionId = 4;
    else if(val=="2,3")
        sessionId = 5;
    else if(val=="1,2,3")
        sessionId = 6;
    else
        sessionId = 0;

    return sessionId;
}
void User::SetStartTime(const std::string& val)
{
    startTimeStr = val;
    startTime =  RTXUtils::StrToTimeSsm( val );
}

void User::InitArgo()
{
    auto arg4 = std::make_pair("RetailSWP", -1);
    argosMp.insert(arg4);
    auto arg3 = std::make_pair("RetailRouting", -1);
    argosMp.insert(arg3);
    auto arg = std::make_pair("SWP", -1);
    argosMp.insert(arg);
    auto arg1 = std::make_pair("Targeting", -1);
    argosMp.insert(arg1);
    auto arg2 = std::make_pair("Posting", -1);
    argosMp.insert(arg2);
}

void User::AddArgo(const std::string& val, std::string& strVal)
{
    if(argosMp.find(val) != argosMp.end())
    {
        if(RTXUtils::IsValidStrToBoolFormat(strVal) == false)
             return;

        int intVal = 0;
        bool valid = RTXUtils::StrToBool(strVal);
        if(valid)
            intVal = 1;

        argosMp[val] = intVal;
    }
}

ConfigUserBox::ConfigUserBox()
{

}

ConfigUserBox::~ConfigUserBox()
{
    userList.clear();
    accountList.clear();
}

void ConfigUserBox::InitConfigUserBox()
{
    m_EmptyUser = std::make_shared<User>();
}

void ConfigUserBox::AddAcceptMarketOrder(const std::unordered_set< std::string >& users, std::string val)
{
    bool acptMkt = RTXUtils::StrToBool(val);
    for(auto nm : users)
    {
        auto pr = std::make_pair(nm, acptMkt);
        acceptMarketOrder.insert(pr);
    }
}

void ConfigUserBox::AddStartTime(const std::unordered_set< std::string >& users, std::string val)
{
    int tm = RTXUtils::StrToTimeSsm(val);
    for(auto nm : users)
    {
        auto pr = std::make_pair(nm, tm);
        startTimeSsm.insert(pr);
    }
}

void ConfigUserBox::AddSessionId(const std::unordered_set< std::string >& users, std::string val, int deflt)
{
    unsigned int sid = RTXUtils::StrToInt(val, deflt);
    for(auto nm : users)
    {
        auto pr = std::make_pair(nm, sid);
        sessionId.insert(pr);
    }
}

void ConfigUserBox::AddSessionId(const std::unordered_set< std::string >& users, unsigned int val, int deflt)
{
    for(auto nm : users)
    {
        auto pr = std::make_pair(nm, val);
        sessionId.insert(pr);
    }
}

void ConfigUserBox::AddUser(const std::unordered_set<std::string>& name, const std::shared_ptr<User>& user)
{
    if(user->type == 0)             //login
    {
        for (auto nm : name)
        {
            if (userList.find(nm) == userList.end())
            {
                userList[nm] = user;
            }
        }
    }
    else if(user->type == 1)        //account
    {
        for (auto nm : name)
        {
            if (accountList.find(nm) == accountList.end())
            {
                accountList[nm] = user;
            }
        }
    }
}

const std::shared_ptr<User>& ConfigUserBox::GetUserConfig(const unsigned int userType, const std::string& name)
{
    if(userType == 0)       //login
    {
        auto user = userList.find(name);
        if(user != userList.end())
            return user->second;
    }
    else
    {
        auto user = accountList.find(name);
        if(user != accountList.end())
            return user->second;
    }
    return m_EmptyUser;
}

