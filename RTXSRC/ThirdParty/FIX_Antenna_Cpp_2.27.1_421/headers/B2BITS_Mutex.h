// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Mutex.h
/// Contains System::Mutex class declaration.

#ifndef _B2BITS_MUTEX__H
#define _B2BITS_MUTEX__H

#include <string>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_SystemDefines.h>
#include <B2BITS_CompilerDefines.h>

#if defined( _LINUX )
#   include <pthread.h>
#endif

namespace Utils
{
    template< class T > class CTGuard;
}

/**
 * Platform specific classes.
 */
namespace System
{

    /**
     * Mutex - provides synchronization, the ability to control how threads share
     * resources. You use mutexes to prevent multiple threads from modifying
     * shared data at the same time, and to ensure that a thread can read
     * consistent values for a set of resources (for example, memory) that may be
     * modified by other threads.
     */
    class V12_API Mutex
    {
        friend class Utils::CTGuard<Mutex>;
        friend class Condition;
    private:
        char const* name_;

#ifndef _WIN32
        pthread_mutex_t  mutex_;
        volatile pthread_t owner_;
#else
        CRITICAL_SECTION mutex_;
#endif

    public:
        /** Constructor. */
        explicit Mutex( std::string const& name );

        /** Constructor */
        explicit Mutex( char const* name );

        /** Constructor */
        Mutex();

        /** Destructor. */
        ~Mutex();

        /**
         * Locks the mutex. If the mutex is currently locked, the calling thread is
         * blocked until mutex is unlocked. On return, the thread owns the mutex until
         * it calls Mutex::unlock().
         */
        void lock();

        /**
         * Tries to acquire lock without blocking. 
         * @return true if lock was acquired; false otherwise.
         */
        bool tryLock();

        /**
         * Tries to acquire lock without blocking. This function is identical to `tryLock` and it's sole
         * purpose is to make `Mutex` compatible with Boost's Lockable concept.
         *
         * @return true if lock was acquired; false otherwise.
         */
        bool try_lock()
        {
            return tryLock();
        }

        /**
         * Unlocks the mutex. The mutex becomes unowned. If any threads are waiting
         * for the mutex, one is awakened.
         */
        void unlock();

        char const* name() const throw() { return name_; }

    private:
        void init( char const* aName );

    private:
        Mutex( const Mutex& aM );
        Mutex& operator=( const Mutex& aRhs );
    }; // class Mutex

} // namespace System

#endif // _B2BITS_MUTEX__H
