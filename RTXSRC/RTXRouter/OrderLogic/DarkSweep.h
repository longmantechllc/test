//
// Created by schen on 10/6/2020.
//
#ifndef RTXROUTER_DARKSWEEP_H
#define RTXROUTER_DARKSWEEP_H

class ExecutionReport;
class RTXOrder;

struct SWPRouteInfo
{
    SWPRouteInfo(const std::string& name, bool multiPounce = false, int maxNumberOfTryIn=1)
        :routeName(name),
         multiPounce(multiPounce),
         maxNumberOfTry(maxNumberOfTryIn)
     {

     }
    std::string routeName;
    bool multiPounce = false;
    int multiPounceCnt = 0;
    int maxNumberOfTry = 1;
};

class DarkSweep: public IOrderLogicBase
{
public:
    DarkSweep() =default;
    DarkSweep(RTXOrder* order);
    ~DarkSweep() override;
    bool InitOrder() override;

    bool ProcessOrder() override;
    virtual bool SendChildOrder(std::shared_ptr<Order> order) override;
    bool SequentialSweep();
    bool ParallelSweep();

    bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child) override;
    bool ProcessMktData() override;
    void SetRetailSwp(bool val){ m_isRetailSwp = val;}
private:
    Price CalculatePrice(const std::shared_ptr< RouteStruct>& route);
    bool IsRouteEligible(const std::string& routeName);
    bool IsEligible();
    bool IsRouteMarketable(const std::string& routeName);
    bool IsMarketable();
    bool TestForCancel();

    std::vector<std::string> m_sWPSequentialRoutes;
    std::vector<std::shared_ptr<SWProute>> m_sWPParallelRoutes;
    std::unordered_map<std::string /* coid */, std::shared_ptr<SWPRouteInfo>> m_routeList;

    bool m_isParallel = false;
    bool m_isDone = false;
    bool m_isRetailSwp = false;
    bool m_isPMP = false;
};



#endif //RTXROUTER_DARKSWEEP_H
