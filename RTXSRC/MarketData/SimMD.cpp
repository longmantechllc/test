#include <cstring>
#include <unordered_map>
#include <iostream>
#include <boost/algorithm/string/trim.hpp>
#include <algorithm>
#include <regex>

#include "SimMD.h"
#include "logger.h"

using namespace std;

SimMD::SimMD(const string& symbolFile,const string& exchs){

//    m_fp = fopen("SimMD.log", "w");
//     if (m_fp == NULL)
//     {
//         cout << "Couldn't open file SimMD.log" << endl;
//     }
    m_init = loadDataFile(symbolFile);
    loadExchanges(exchs);
    srand((unsigned) time(0));

    m_tobThread = std::thread([&] {
        while (m_tobThread_running) {
            {
                unique_lock guard(m_mutex);
                auto N = m_subs.size();
                if (N > 0) {
                    int M = rand() %N; // random choose a symbol
                    auto it = m_subs.begin();
                    for(int i = 0; i < M; ++i, ++it);
                    genTOB(*it);
                }
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }
    });
    m_tobThread_running = true;
}

SimMD::~SimMD() {
    m_tobThread_running = false;
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

bool SimMD::loadDataFile(const string& symbolFile) {
    ifstream in(symbolFile.c_str());
    if (!in) {
        logger2 << "Cannot open simMD symbol file" << symbolFile << endl;
        return false;
    }

    std::string line;
    while (std::getline(in, line)) {
        if (line.size()) {
            if (line[0] == '#')
                continue;
            boost::algorithm::trim(line);
            if (auto p = line.find(','); p != string::npos) {
                auto symbol = line.substr(0, p);
                double prc = stod(line.substr(p+1));
                logger2 << "symbol " << symbol <<", avgPrc " << prc << endl;
                m_symbols.emplace(symbol, PriceRecord{prc, 0.0,0.0});
            }
        }
    }
    logger2 << m_symbols.size() << " symbols in MDSim" << endl;
    return true;
}

void SimMD::loadExchanges(const string& exchanges) {
    auto line = exchanges;
    boost::algorithm::trim(line);
    logger2 << "process exchanges: [" << line << "]" << endl;
    regex re("[,]+");
    sregex_token_iterator it(line.begin(), line.end(), re, -1);
    sregex_token_iterator reg_end;
    for(;  it != reg_end; ++it)
        m_exchs.push_back( it->str());
       
    logger2 << m_exchs.size() << " exchanges in MDSim" << endl;
}

namespace {
    auto getMs() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        //auto now2 = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        //return now2.count();
    };
}
void SimMD::genBBO( const string& symbol ) {
    static std::chrono::milliseconds lastCheckingTime {0};

    // BBO data
    MDBBO mdbbo;
    auto& bbo = mdbbo.data;
    auto& rec = m_symbols[symbol];
    unsigned px =  rec.midPx * 100;
    bbo.m_bboBuy = rec.bid = ((100 - (rand()%21 - 10)) * px) / 10000.0;
    bbo.m_bboSell = rec.ask = rec.bid + 0.02;
    bbo.m_symbol = symbol;
    
    if (!m_queue.try_push(make_shared<MDBBO>(mdbbo))) {
        auto now = getMs();
        if ((now - lastCheckingTime).count() > 1000) {
            lastCheckingTime = now;
            logger2 << "failed to push NBBO, queue size = " << m_queue.size() << endl;
        }
    }
    
    logger2 << "push in msg queue. size = " << m_queue.size() 
        << ", NBBO on " << symbol << ", buy " << bbo.m_bboBuy.ToDouble()
        << ", sell " << bbo.m_bboSell.ToDouble() << endl;
}

void SimMD::genTOB( const string& symbol ) {
    static std::chrono::milliseconds lastCheckingTime {0};

    auto& rec = m_symbols[symbol];

    MDBatchTOB tob;
    auto& batch = tob.data;

    random_shuffle(m_exchs.begin(), m_exchs.end());
    const auto N = m_exchs.size();
    for (const auto &e: m_exchs) {
        double bid_spread = (rand() % N <= N/3? 0.0 : rand()%N / 100.0);
        double ask_spread = (rand() % N <= N/3? 0.0 : rand()%N / 100.0);
        batch.push_back(TopOfBook(e, symbol, Price(rec.ask + ask_spread), (rand() % 10 + 1) * 100,
            Price(rec.bid - bid_spread), (rand() % 10 + 1) * 100));
    }

    logger2 << "genTOB on " << symbol << " TOB: " << endl;
    for(auto&e : batch) {
        logger2 << "mpid=" << e.m_mpid << ", symbol=" << e.m_symbol
            << ", askPrice="<< e.m_askPrice.ToDouble() << ", askSize=" << e.m_askSize
            << ", bidPrice=" << e.m_bidPrice.ToDouble() << ", bidSize=" << e.m_bidSize << endl;
    }
    if (!m_queue.try_push(make_shared<MDBatchTOB>(tob))) {
        auto now = getMs();
        if ((now - lastCheckingTime).count() > 1000) {
            lastCheckingTime = now;
            logger2 << "failed to push BatchTOB, queue size = " << m_queue.size() << endl;
        }
    }
    logger2 << "push TOB in msg queue. size = " << m_queue.size() << endl;
}

std::vector <std::shared_ptr<TopOfBook>>  SimMD::subscribeTOB(const std::string& symbol ){
    logger2 << "subscribing TOB on symbol " << symbol << endl;
    if (!m_init || !m_symbols.count(symbol)) {
        logger2 << "symbol " << symbol << " doesn't exist" << endl;
        return {};
    }

    {
        std::unique_lock guard(m_mutex);

        if (m_subs.count(symbol)) { // it's subscribed before
            logger2 << symbol << " is subscribed on before, refresh" << endl;
        }
        else {
            logger2 << "subscribeTOB OK on " << symbol  << endl;
            m_subs.emplace(symbol);
        }
    }
    
    genBBO(symbol);
    genTOB(symbol);
    return {};
}

Bbo SimMD::subscribeBbo(const std::string& symbol){
    return {};
}

void SimMD::removeTOBSubscription(const std::string& symbol){
    //std::lock_guard<std::mutex> guard(m_mutex);
    unique_lock guard(m_mutex);
    m_subs.erase(symbol);
    logger2 << "removeTOBSubscription OK on " << symbol  << endl;
}

OpenTrade SimMD::requestOpeningTrade(const std::string& symbol, int oid){
    return {};
}

void SimMD::cancelOpeningTradeRequest(const std::string& symbol){
}
