//
// Created by schen on 11/11/2020.
//

#ifndef RTXROUTER_CONFIGSECIDFNSLOADER_H
#define RTXROUTER_CONFIGSECIDFNSLOADER_H

#include <memory>
#include <unordered_set>
#include "ConfigSecIdfnsBox.h"
#include <vector>

using namespace std;

class Logger;
class ConfigSecIdfnsBox;
class ConfigLoader;

class ConfigSecIdfnsLoader : public LoaderBase
{
public:
    ConfigSecIdfnsLoader();
    ConfigSecIdfnsLoader(Logger* logger);
    virtual ~ConfigSecIdfnsLoader();
    void InitConfigSecIdfnsLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node) override;

    void LoadExchange(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void SetSecIdfn(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadListingExchange(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadListingExchangeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadPreMarketListingExchangeVenue(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void LoadRetailSWP(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadSWP(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddSWPSeqRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node, bool isRetailSWP=false);
    void AddSWPParalRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node, bool isRetailSWP=false);

    void LoadTarget(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetSequentialTargeting(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddDefinedRoute(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void AddMDIDmapping(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void LoadPost(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetRerouteFailure(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void SetQuotePassover(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadPostRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void LoadMultiPosting(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadMultiRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void ValidateSecIdData(ConfigLoader* loader);
    void SetMktOrderPrimaryOpenCheck(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadOrderParam(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void PrintExchanges(ConfigLoader* loader);

    std::unordered_map <std::string /*Name*/, std::function<void(ConfigLoader* loader, xercesc_3_2::DOMNode* node) > > funcList;
    std::shared_ptr<SecIdfn> secIdObj;
};


#endif //RTXROUTER_CONFIGSECIDFNSLOADER_H
