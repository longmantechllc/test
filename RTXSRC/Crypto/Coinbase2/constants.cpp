#include <string>
#include <magic_enum/magic_enum.hpp>
#include "constants.h"

namespace json_dto {

#define read_json_for_enum(enum_type) \
template<> \
void read_json_value( \
    enum_type &v, \
    const rapidjson::Value &object) { \
    try { \
        std::string_view representation; \
        read_json_value(representation, object); \
        v = magic_enum::enum_cast<enum_type>( \
            representation).value(); \
    } \
    catch (const std::exception &ex) { \
        throw std::runtime_error{std::string{"unable to read : "} + ex.what()}; \
    } \
}

read_json_for_enum(Crypto::coinbase::WS::ChannelType);
read_json_for_enum(Crypto::coinbase::WS::RequestType);
read_json_for_enum(Crypto::coinbase::WS::ResponseType);

#undef read_json_for_enum

}  // namespace json_dto