// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FAProperties.h
/// Contains Engine::FAProperties class declaration.

#ifndef __FAProperties_h__
#define __FAProperties_h__

#include <string>
#include <B2BITS_V12_Defines.h>
#include "B2BITS_PubEngineDefines.h"
#include <B2BITS_IPAddr.h>
#include <B2BITS_SessionId.h>
#include <B2BITS_SessionParameters.h>
#include "B2BITS_SessionsScheduler.h"

namespace Engine
{
    /// List of all engine.properties parameters
    namespace FIXPropertiesNames
    {
        int const LISTEN_PORT_IN_DISABLED = -2;
        int const LISTEN_PORT_NA = -1;
        int const LISTEN_PORT_NETWORK_DISABLED = 0;

        //
        // Engine settings
        //

        const std::string ANCHOR_FILE_NAME = "AnchorFile";

        /// The top of the directory tree under which the engine's configuration,
        /// and log files are kept.
        /// 
        /// Do NOT add a slash at the end of the directory path.
        const std::string ENGINE_ROOT_PARAM = "EngineRoot";

        /// Engine's local IP address to bind to. It can be used on a multi-homed host
        /// for a FIX Engine that will only accept connect requests to one of its addresses.
        /// If this parameter is commented or empty, the engine will accept connections
        /// to any/all local addresses.
        const std::string LISTEN_ADDRESS_PARAM = "ListenAddress";

        /// Engine's local IP address to send messages from. It is used only  for multi-homed hosts
        /// If this parameter is commented or empty, the engine will send IP datagrams from any/all 
        /// local addresses.
        const std::string CONNECT_ADDRESS_PARAM = "ConnectAddress";

        /// Engine's listen port. Supports multiple values delimited by comma.
        /// Parameter is optional.
        const std::string LISTEN_PORT_PARAM = "ListenPort";
        
        /// Engine's listen SSL port. Supports multiple values delimited by comma.
        /// Parameter is optional.
        const std::string LISTEN_SSL_PORT_PARAM = "ListenSSLPort";

        /// Engine's SSL certificate file path.
        /// Parameter is mandatory if ListenSSLPort is not empty.
        const std::string SSL_CERTIFICATE_PARAM = "SSLCertificate";

        /// Engine's SSL certificate password if any.
        /// Parameter is optional
        const std::string SSL_CERTIFICATE_PASSWORD_PARAM = "SSLCertificatePassword";

        /// Engine's SSL certificate's private key file path.
        /// Parameter is conditionaly optional. It is optional if privete key is embeded into certificate file(.pfx, possibly .pem).
        const std::string SSL_PRIVATE_KEY_PARAM = "SSLPrivateKey";

        /// Engine's SSL private key password if any.
        /// Parameter is optional. 
        const std::string SSL_PRIVATE_KEY_PASSWORD_PARAM = "SSLPrivateKeyPassword";

        /// Engine's SSL protocols to use.
        /// Parameter is mandatory. it is a comma separated list of protocols to use. Valid values are SSLv2, SSLv3, TLSv1, TLSv1_1, TLSv1_2.
        const std::string SSL_PROTOCOLS_PARAM = "SSLProtocols";

        const std::string SSL_PROTOCOLS_SSLV2_VALUE = "SSLv2";
        const std::string SSL_PROTOCOLS_SSLV3_VALUE = "SSLv3";
        const std::string SSL_PROTOCOLS_TLSV1_VALUE = "TLSv1";
        const std::string SSL_PROTOCOLS_TLSV1_1_VALUE = "TLSv1_1";
        const std::string SSL_PROTOCOLS_TLSV1_2_VALUE = "TLSv1_2";

        /// Engine's SSL CA certificate file path.
        /// Parameter is optional.
        const std::string SSL_CA_CERTIFICATE_PARAM = "SSLCACertificate";

        /// Whatever client's certificate is mandatory.
        /// Decline clients without certificate if true.
        /// Parameter is optional. Default is false.
        const std::string SSL_VALIDATE_PEER_CERT_PARAM = "SSLValidatePeerCertificate";

        /// Ciphers list configuration string.
        /// Parameter is optional.
        const std::string SSL_CIPHERS_LIST_PARAM = "SSLCiphersList";


        /// DBL TCP listen address and port. Format: <XXX.XXX.XXX.XXX>:<port>.
        /// Parameter is optional.
        const std::string DBL_TCP_LISTEN_ADDRESSES = "Myricom.Dbl.TcpListenAddresses";

        /// This property tells how will be initialized Myricom DBL TCP API.
        /// If true it will be used SocketAdaptor to emulate DBL API via generic sockets.
        /// Default value is false.
        /// Parameter is optional.
        const std::string DBL_USE_SOCKET_ADAPTOR = "Myricom.Dbl.UseSocketAdaptor";

        /// Engine's log file name.
        ///
        /// If this parameter is commented or empty, the 'engine.log' will be used. 
        const std::string LOG_FILE_NAME_PARAM = "LogFileName";
        
        /// Default storage type of the created unregistered sessions. By default persistent storage 
        /// type used. Use "transient" value to use transient storage for the sessions.
        /// @deprecated Use UnregisteredAcceptor.SessionStorageType instead
        const std::string UNREG_SESSION_STORAGE_TYPE_PARAM_DEP = "UnregisteredSessionStorageType";
        
        /// Default storage type of the created unregistered sessions. By default persistent storage 
        /// type used. Use "transient" value to use transient storage for the sessions.
        /// @deprecated Use UnregisteredAcceptor.SessionStorageType instead
        const std::string SESSION_STORAGE_TYPE_PARAM_DEP2 = "SessionStorageType";
        
        /// Default storage type of the created unregistered sessions. By default persistent storage 
        /// type used. Use "transient" value to use transient storage for the sessions.
        const std::string UNREG_SESSION_STORAGE_TYPE_PARAM = "UnregisteredAcceptor.SessionStorageType";

        /// Number of threads that serve FIX sessions (heartbeat generation/check, 
        /// message rejecting, message delay delivering, etc)
        /// On Linux the recommended value is 2. On other platforms the recommended value is 10.
        /// The value must be integer and greater than zero.
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        const std::string NUMBER_OF_WORKERS_PARAM = "NumberOfWorkers";

        /// This property is the path of the directory in which the logs for all incoming 
        /// (if LogIncomingMessages is set to "true") and outgoing FIX messages are stored. 
        /// It is possible to specify a path related to the EngineRoot directory. 
        /// For example if LogDirectory is set to \"logs\" then the real path is $(EngineRoot)/logs.
        /// The specified directory must exist.
        const std::string LOG_DIRECTORY_PARAM = "LogDirectory";

        /// Relative path to the backup folder. This folder will be used for message storage files
        /// of the backup connections.
        const std::string BACKUP_DIRECTORY_PARAM = "BackupDirectory";

        /// This parameter contains name of the XML file with extensions of the FIX protocols.
        const std::string DICTIONARIES_FILES_LIST = "DictionariesFilesList";

        /// This parameter defines custom raw FIX parser list delimeted by semicolon.
        /// Format of the value: [PARSER_NAME@][FIXT_PROTOCOL1:APP_PROTCOL1,[APP_PROTCOL1[, ...]] | [APP_PROTCOL1] ; ...
        /// Examples of one entry. Entries can be combined with semicolon to define more than one parser:
        /// - FIX44
        /// - FIXT11:FIX50SP2
        /// - FIXT11:FIX50SP2,FIX50SP1
        /// - MyFixParser@FIXT11:FIX50SP2,FIX50SP1
        const std::string PARSERS_NAMES_LIST = "AdditionalParsersList";

        /// If this parameter is true, FIX Engine will try to restore index file from log file in case when
        /// index file is missed or corrupted
        /// This parameter is optional, default value is false
        /// @note Please be carefully enabling this property, restoring process uses some
        /// heuristic algorithms, and some data(not messages) could be missed
        const std::string INDEX_REBUILD_ENABLED_PARAM = "MessageStorage.IndexRebuildEnabled";

        /// If this option is enabled, FIX Engine will not rebuild index files for message storages,
        /// that does not support embedded Reject Flag Marker
        /// This parameter is optional, default value is true
        const std::string INDEX_REBUILD_REQUIRE_REJECT_FLAG = "MessageStorage.RequireRejectFlagSupport";

        //// This parameter sets the timestamp format for storages
        /// Valid values:
        /// millisecond - use millisecond precision in timeformat (default value)
        /// microsecond - use microsecond precision in timeformat
        /// nanosecond - use nanosecond precision in timeformat
        const std::string TIMESTAMP_UNIT_MESSAGE_STORAGE  = "MessageStorage.TimestampUnit";

        /// Path to timezones definition file.
        const std::string TIMEZONE_DB = "TimezoneDB";

        //
        // Session settings
        //

        /// This property provides an option to log incoming FIX messages (those received) from 
        /// a counterparty FIX Engine. They will be stored in the directory specified by 
        /// the LogDirectory parameter in a file with extension "in".
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// In the development should be true. In production should be false.
        const std::string LOG_INCOMING_MESSAGES_PARAM = "LogIncomingMessages";

        /// This parameter sets the time period after which a session is non-gracefully terminated 
        /// if a response is not received to a first "Logon" message (message type A). 
        /// The corresponding Logout message is sent to the counterparty.
        /// This value is in seconds. 
        /// The recommended value is 30 seconds for dedicated connections or private networks. 
        /// Trading connections via the internet will require calibration. 
        /// If it is set to "0", then the time period is unlimited. 
        /// The value must be integer and not negative.
        const std::string LOGON_TIME_FRAME_PARAM = "LogonTimeFrame";
        
        /// This parameter sets the time period after which a session is automatically terminated 
        /// if a response is not received to a "Logout message" (message type 5). 
        /// This value is in seconds. 
        /// The recommended value is 10 seconds for dedicated connections or private networks. 
        /// Trading connections via the internet will require calibration. 
        /// The value must be integer and greater than 0.
        const std::string LOGOUT_TIME_FRAME_PARAM = "LogoutTimeFrame";
        
        /// An option not to reset sequence numbers after Logout.
        /// Logout sender should initiate session recovery by sending Logon message
        /// with SeqNum = <last outgoing SeqNum> + 1;
        /// expecting reply Logon with SeqNum = <last incoming SeqNum> + 1.
        /// If a gap is detected, standard message recovery or gap filling process
        /// takes place.
        const std::string INTRADAY_LOGOUT_TOLERANCE_PARAM = "IntradayLogoutTolerance";
        
        /// This parameter specifies the delta (increment) to the Heartbeat interval between
        /// a TestRequest message being sent by FIX Engine and a Response Heartbeat being received. 
        /// The session attains a "telecommunication failed state" if no Response Heartbeat message 
        /// is received after the normal Heartbeat interval plus delta. For example if no message 
        /// (application or session level) is received during the Heartbeat interval then Engine sends 
        /// a TestRequest message. If the required Response Heartbeat message is not received during 
        /// Heartbeat interval plus Delta then the session moves to the state "Telecommunication link 
        /// failed". This parameter is specified in (Heartbeat Interval/100). The recommended value is 
        /// twenty percent.
        const std::string REASONABLE_TRANSMISSION_TIME_PARAM = "ReasonableTransmissionTime";
        
        /// FIX Engine has inbuilt FIX message routing capability and  fully supports 
        /// the "Deliver To On Behalf Of" mechanism as specified by the FIX protocol.
        /// If this parameter is set to "true" then Engine will redirect FIX messages automatically 
        /// to other FIX sessions it maintains if OnBehalfOfCompID field in the message is defined. 
        /// If this parameter is set to "false" Engine directs all messages received to the client 
        /// application.
        const std::string THIRD_PARTY_ROUTING_PARAM = "ThirdPartyRoutingIsEnabled";
        
        /// This parameter provides an option whereby FIX Engine will accept a FIX session for which it 
        /// has no registered application (an acceptor). If set to "true" Engine accepts incoming connection 
        /// and creates corresponding Session object. If there is no Application associated with session, 
        /// all application level messages are rejected with Application Level Reject(3) message. If 
        /// an application is registered behaviour is as standard. If set to false then Logon messages 
        /// are ignored and incoming connection is dropped.
        /// Property is obsolete, UnregisteredAcceptor.CreateSession should be used instead.
        const std::string CREATE_UNREGISTERED_ACCEPTOR_SESSION_PARAM = "CreateUnregisteredAcceptorSession";
        
        /// This parameter provides an option whereby FIX Engine will accept a FIX session for which it 
        /// has no registered application (an acceptor). If set to "true" Engine accepts incoming connection 
        /// and creates corresponding Session object. If there is no Application associated with session, 
        /// all application level messages are rejected with Application Level Reject(3) message. If 
        /// an application is registered behavior is as standard. If set to false then Logon messages 
        /// are ignored and incoming connection is dropped.
        const std::string UA_CREATE_SESSION_PARAM = "UnregisteredAcceptor.CreateSession";

        /// This parameter specifies the time interval between attempts to deliver an application level 
        /// message to a registered client application in the event the application does not confirm 
        /// receipt and operation upon the message. The value is specified in milliseconds. 
        /// The value must be integer and greater than 0.
        /// This parameter is required only if the DelayedProcessing.MaxDeliveryTries
        /// parameter is specified.
        const std::string DP_DELIVERY_TRIES_INTERVAL_PARAM = "DelayedProcessing.DeliveryTriesInterval";
        
        /// This parameter specifies the number of attempts that will be made to deliver an application 
        /// level message to the registered client application. If this value is exceeded then 
        /// the session will be closed with the logout reason "Application not available". The recommended value is 10.
        /// The value must be integer and not negative.
        /// This parameter is optional.
        const std::string DP_MAX_DELIVARY_TRIES_PARAM = "DelayedProcessing.MaxDeliveryTries";
        
        /// This parameter specifies the number of attempts to restore the session.
        /// The session is considered as restored if the telecommunication link was
        /// restored and the exchange of Logon messages was successful.
        /// If it is set to "-1", then the number of attempts is unlimited. 
        /// This value is integer. 
        const std::string RECONNECT_MAX_TRIES_PARAM = "Reconnect.MaxTries";
        
        /// This parameter specifies the time interval between reconnection attempts in order to restore
        /// a communications link. This value is specified in milliseconds (seconds*10-3). 
        /// The recommended value is 1000 for dedicated connections and private networks. 
        /// Internet connections require calibration. The value must be integer and greater than 0.
        const std::string RECONNECT_INTERVAL_PARAM = "Reconnect.Interval";
        
        /// This parameter sets the time period after which a message rejecting is 
        /// starting while session isn't exists.
        /// Parameter isn't required. Value is specified in milliseconds (seconds*10-3), 
        /// must be integer and > 0.
        const std::string MESSAGE_TIME_TO_LIVE_PARAM = "MessageTimeToLive";

        /// This parameter for forwarding fix messages to log system
        /// if set to true then incoming/outgoing fix messages add in log system with level UNFO
        /// if set to false then incoming/outgoing fix messages don't add in log system.
        /// default value is "false"
        const std::string IS_FORWARDING_FIXMESSAGES_TO_LOG = "IsForwardingFixMessagesToLog";


        /// This parameter sets size temporary map for processing out of sequence messages in strategy "Queue"
        /// default value is "2000"
        const std::string OUT_OF_SEQUENCE_MESSAGES_QUEUE_SIZE = "OutOfSequenceMessagesStrategy.QueueSize";

        /// This parameter sets strategy for out of sequence messages
        /// Valid values:
        /// RequestAlways - send resend request on any out of sequence message
        /// RequestOnce - Send resend request on gap. Suppress new resend request if gap not closed
        /// Queue - Use temporary queue for storing out of sequence messages and processing them after closing gap
        /// IgnoreGap - Send resend request on gap and processing out of sequence messages
        /// default value is "RequestAlways"
        const std::string OUT_OF_SEQUENCE_MESSAGES_STRATEGY = "OutOfSequenceMessagesStrategy";

        /// The license file path.
        const std::string LICENSE_FILE_PARAM = "LicenseFile";
        
        /// This parameter defines the upper limit to the number of outgoing messages that are resent 
        /// in the event of a Resend Request. The recommended value is 20000 if no data on mean activity is known.
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// Large storage increases application working set.
        const std::string OUTGOING_MESSAGE_STORAGE_SIZE_PARAM = "OutgoingMessagesStorageSize";
        
        /// Resend Request resend messages by blocks. This parameter defines how many messages 
        /// proceed in block. 0 - all messages will be resend in one block. By default parameter is 1000.
        /// The value must be integer and not less than 0.
        const std::string RESEND_MESSAGES_BLOCK_SIZE_PARAM = "ResendMessagesBlockSize";
        
        /// This parameter is an option whereby the version of FIX protocol used for the outgoing message
        /// is validated against that of the established session. If set to "true" then the application 
        /// must use the same version of the protocol as the established session otherwise an error 
        /// occurs. If set to false then the application level message will be sent to the counterparty. 
        /// The recommended value is "true".
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// In the development should be true. In production should be false.
        const std::string CHECK_VERSION_OUTGOING_MESSAGE_PARAM = "CheckVersionOfOutgoingMessages";
        
        /// If this parameter is true than file streams are flushed after each I/O operation. 
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// In the development and in production should be true. 
        const std::string EXTRA_SAFE_MODE_PARAM = "ExtraSafeMode";

        /// An option to send a Logon message with the ResetSeqNumFlag set
        /// after each 24 hour period of session's activity to establish a new set
        /// of sequence numbers (starting with 1).
        ///
        /// This parameter is optional, the default value is false.
        ///
        /// @note  This option does not affect sessions which use version 4.0 
        /// of the FIX protocol.
        const std::string RESET_SEQNUM_AFTER_24_HOURS_PARAM = "ResetSeqNumAfter24hours";
        
        /// An option to write timestamps in the log files. 
        /// This parameter is optional, the default value is true.
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// In the development should be true. In production should be false.
        const std::string TIMESTAMPS_IN_LOGS_PARAM = "TimestampsInLogs";
        
        /// Encryption config file
        const std::string ENCRYPTION_CONFIG_FILE_PARAM = "EncryptionConfigFile";
        
        /// This parameter allow to automatically resolve sequence gap problem. 
        /// When mode is ON session uses 141(ResetSeqNumFlag)
        /// tag in sending/confirming Logon message to reset SeqNum at the initiator or acceptor.
        /// Valid values:
        /// * "0" or "false" - Disable ForceSeqNumReset mode
        /// * "1" or "true" - Enable SeqNum reset at first time of session initiation
        /// * "2" - Enable SeqNum reset for every session initiation
        const std::string FORCE_SEQNUM_RESET_PARAM = "ForceSeqNumReset";
        
        /// Value of the ForceSeqNumReset parameter
        /// * "0" or "false" - Disable ForceSeqNumReset mode
        const std::string FORCE_SEQNUM_RESET_VALUE_OFF = "OFF";
        
        /// Value of the ForceSeqNumReset parameter
        /// * "1" or "true" - Enable SeqNum reset at first time of session initiation
        const std::string FORCE_SEQNUM_RESET_VALUE_ON = "ON";
        
        /// Value of the ForceSeqNumReset parameter
        /// * "2" - Enable SeqNum reset for every session initiation
        const std::string FORCE_SEQNUM_RESET_VALUE_ALWAYS = "ALWAYS";
        
        /// * "1" or "true" - Enable SeqNum reset at first time of session initiation
        const std::string FORCE_SEQNUM_RESET_VALUE_TRUE = "true";
        
        /// Value of the ForceSeqNumReset parameter
        /// * "0" or "false" - Disable ForceSeqNumReset mode
        const std::string FORCE_SEQNUM_RESET_VALUE_FALSE = "false";
        
        /// Value of the ForceSeqNumReset parameter
        /// * Not defined value.
        const std::string FORCE_SEQNUM_RESET_VALUE_NA = "NA";
        
        /// This parameter allow to resolve seqNum too low problem at logon.
        /// When it true - session continue with received seqNum.
        const std::string UA_IGNORE_SEQNUM_TOO_LOW_AT_LOGON_PARAM = "UnregisteredAcceptor.IgnoreSeqNumTooLowAtLogon";
        
        /// When true, unregistered acceptors will reject messages in case they couldn't be sent 
        /// during interval
        const std::string UA_REJECT_MESSAGE_WHILE_NO_CONNECTION_PARAM = "UnregisteredAcceptor.RejectMessageWhileNoConnection";
        
        /// When true, the TCP buffer (Nagle algorithm) will be disabled for the unregistered 
        /// acceptors. Otherwise, TCP may join and enqueue small packages until timeout 
        /// ends.
        const std::string UA_TCP_BUFFER_DISABLED_PARAM = "UnregisteredAcceptor.tcpBufferDisabled";
        
        /// FA able to join packages that wait for sending into the socket, this parameters 
        /// controls how many messages could be joined. 0 means infinite. Value should be 
        /// less than 1000000.
        const std::string UA_MAX_MESSAGES_AMOUNT_IN_BUNCH_PARAM = "UnregisteredAcceptor.maxMessagesAmountInBunch";
        
        /// This parameter controls existence of required tags in application level messages. 
        /// The possible values are "true" and "false". If set to "true" then all application 
        /// level messages are validated. If set to "false" then the responsibility for message validity 
        /// rests with the counterparty. Please note that session level messages are validated in 
        /// all cases. The recommended setting is "true".
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        /// In the development should be true. In production should be false.
        const std::string V_VALIDATE_MESSAGE_PARAM = "MessageMustBeValidated";
        
        const std::string V_ADDITIONAL_FIELDS_PARAM = "Validation.AdditionalFields";
        
        const std::string V_ADDITIONAL_FIELDS_FILE_NAME_PARAM = "Validation.AdditionalFieldsFileName";
        
        /// This parameter controls the validation of required fields in repeating group. 
        /// The possible values are "true" and "false". If set to "true" then repeating 
        /// groups will be checked for presence of required fields. 
        /// If set to "false" then the responsibility for repeating group validity 
        /// rests with the counterparty. 
        /// The recommended setting is "true". 
        /// This parameter is optional. 
        /// The default value is "true". 
        const std::string V_CHECK_REQUIRED_GROUP_FIELDS_PARAM = "Validation.CheckRequiredGroupFields";
        
        /// When true, raw message may contains tags without values - they will be ignored.
        /// Otherwise exception was fired.
        const std::string V_ALLOW_EMPTY_FIELD_VALUE_PARAM = "AllowEmptyFieldValue";
        
        /// When true, raw message may contains leading group tag with 0 value - it will be ignored.
        /// Otherwise exception was fired.
        const std::string V_ALLOW_ZERO_NUM_IN_GROUP_PARAM = "AllowZeroNumInGroup";
        
        /// This parameter controls tag values validation. 
        /// The possible values are "true" and "false". If set to "true" then all
        /// messages will be validated. If set to "false" then the responsibility for message validity 
        /// rests with the counterparty. 
        /// Default: false
        const std::string V_VERIFY_TAGS_VALUES = "VerifyTagsValues";
        
        /// This parameter controls unknown tag handling.
        /// The possible values are "true" and "false". If set to "false" then all unknown 
        /// fields will be stored to the message. If set to "true", engine will reject messages
        /// with unknown fields. 
        /// Default: false
        const std::string V_PROHIBIT_UNKNOWN_TAGS = "ProhibitUnknownTags";
        
        /// This parameter controls duplicate tag handling.
        /// The possible values are "true" and "false". If set to "false" then all duplicated 
        /// fields will be stored to the message. If set to "true", engine will reject messages
        /// with duplicated fields. 
        /// Default: false
        const std::string V_PROHIBIT_DUPLICATED_TAGS = "Validation.ProhibitDuplicatedTags";

        /// This parameter controls repeating group size checking. If true, engine will reject 
        /// messages with incorrect repeating group size. Otherwise message will be passed to the 
        /// application layer. In this case the responsibility for message validity 
        /// rests with the counterparty. 
        /// Default: false
        const std::string V_VERIFY_REPERATING_GROUP_BOUNDS = "VerifyReperatingGroupBounds";
        
        /// If true any field that is unknown to dictionary will be ingnored. 
	/// Should be false for development, true for production because
	/// if false FA stores all unknown fields in the FIXMessage in the list
	/// on the heap. With true it would ignore these fields thus limit
	/// storage on heap
	/// IMPORTANT NOTE: this value has priority over all other validation settings. for 
	/// example. Even If MessageMustBeValidated = true, or ProhibitUnknownTags = true, 
	/// or VerifyTagsValues= true unknown fields are ignored
	/// WARNING: Changing this value will impact upon the performance of FIX Engine. 
	/// In the development should be false and all unknown fields should 
	/// be listed in the dictionary file. In production should be true.
        const std::string V_IGNORE_UNKNOWN_FIELDS = "IgnoreUnknownFields";
        
        /// Specified how much same ResendRequests received before Application::onResendRequestLoop is called.
        /// This option is disabled if value is less than 2.
        const std::string DUPLICATE_RESEND_REQUEST_LIMIT_PARAM = "DuplicateResendRequestLimit";
        
        /// Setting property "true" will reserve 10Mb of disc space for logging. 
        /// If log reaches 10Mbs, another 10Mbs will be reserved and so on.
        /// When property is "true" FixAntenna performance is greatly increased.
        const std::string ENABLE_INCREMENTAL_LOG_FILE_CREATION_PARAM = "EnableIncrementalLogFileCreation";
        
        /// Specifies amount of memory in bytes reserved for memory mapped file message storage for *.out/*.in files.
        /// It defines how FIX Antenna reserves space on the HDD for PersistentMM (in any cases) 
        /// and Persistent (if EnableIncrementalLogFileCreation is ON). It is recommended to 
        /// set large enough value to minimize number of resizes during the storage life time. 
        /// Each resize can cause latency spike. 
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        const std::string LOG_INCREMENT_SIZE_PARAM = "Persistents.LogIncrementSize";
        
        /// Specifies amount of memory in bytes reserved for memory mapped file message storage for *.idx file
        /// It defines how FIX Antenna reserves space on the HDD for PersistentMM (in any cases) 
        /// and Persistent (if EnableIncrementalLogFileCreation is ON). It is recommended to 
        /// set large enough value to minimize number of resizes during the storage life time. 
        /// Each resize can cause latency spike. 
        /// @warning Changing this value will impact upon the performance of FIX Engine. 
        const std::string IDX_INCREMENT_SIZE_PARAM = "Persistents.IdxIncrementSize";
        
        /// Specifies maximum size of one slice for sliced message storage.
        /// @note Storage slices are created with step, specified with Persistents.LogIncrementSize
        /// parameter value. If MaxSliceSize value is no multiple of Persistents.LogIncrementSize,
        /// result file size will be less, than specified.
        /// If value is less than LogIncrementSize, LogIncrementSize value will be used
        /// Recommended value: any multiple of Persistents.LogIncrementSize
        /// Default value is 500000000 - 500Mb
        /// @see Engine::splitPersistent_storageType
        const std::string SLICED_STORAGE_MAX_FILESIZE = "SplitPersistentMsgStorage.MaxSliceSize";
        
        /// This property is the path of the directory, where sliced message storage will search
        /// for log files in case they not found in primary location. It is user responsibility to
        /// move files from primary location to backup.
        /// Default value is empty - backup path disabled.
        /// @see Engine::splitPersistent_storageType
        const std::string SLICED_STORAGE_BACKUP_DIRECTORY = "SplitPersistentMsgStorage.BackupDir";
        
        /// If this property value is true, all session parameters will be printed 
        /// to the engine.log file.
        const std::string DEBUG_LOG_SESSION_EXTRA_PARAMETERS_PARAM = "Debug.LogSessionExtraParameters";
        
        /// Specifies total amount of the memory (in MB) that active session may use. 0 - means infinite.
        /// When limit is reached, the "hardest" sessions will be closed non-gracefully.
        const std::string TOTAL_OUTGOING_STORAGE_MEMORY_LIMIT_PARAM = "TotalOutgoingStorageMemoryLimit";

        //
        // Engine monitoring
        //

        /// Enables/disables Engine monitoring
        const std::string MONITORING_ENABLE_PARAM = "Monitoring.Enable";

        /// The engine TCP listen port that is enabled for administrative sessions
        /// If port is configured it will be added to set of engine listen ports and administrative sessions will be accepted on this port only
        /// If not exists or empty common engine listen ports are used to accept administrative sessions
        const std::string M_LISTEN_PORT_PARAM = "Monitoring.ListenPort";

        /// TargetSubID (tag 57) - assigned value used to identify 
        /// specific individual or unit intended to receive message.
        const std::string M_ADMIN_SEESION_DEF_TARGET_SUB_ID_PARAM = "Monitoring.AdminSessionDef.TargetSubID";
        
        /// SenderLocationID (tag 142) - assigned value used to identify 
        /// specific message originator's location (i.e. geographic 
        /// location and/or desk, trader).
        const std::string M_ADMIN_SEESION_DEF_SENDER_LOCATION_ID_PARAM = "Monitoring.AdminSessionDef.SenderLocationID";
        
        /// TargetLocationID_ (tag 143) - assigned value used to identify 
        /// specific message destination's location (i.e. geographic 
        /// location and/or desk, trader).
        const std::string M_ADMIN_SEESION_DEF_TARGET_LOCATION_ID_PARAM = "Monitoring.AdminSessionDef.TargetLocationID";
        
        /// The expected value of the Username (Tag 553) field in the incoming Logon message. 
        /// If the real value is not equal to the expected one then the session is disconnected 
        /// without sending a message and the error condition is generated in the log output. 
        const std::string M_ADMIN_SEESION_DEF_USER_NAME_PARAM = "Monitoring.AdminSessionDef.Username";
        
        /// The expected value of the Password (Tag 554) field in the incoming Logon message. 
        /// If the real value is not equal to the expected one then the session is disconnected 
        /// without sending a message and the error condition is generated in the log output. 
        const std::string M_ADMIN_SEESION_DEF_PASSWORD_PARAM = "Monitoring.AdminSessionDef.Password";
        
        /// The expected value of the source IP address. If the real value is not equal to the expected one 
        /// then the session is disconnected without sending a message and the error condition is generated 
        /// in the log output.
        const std::string M_ADMIN_SEESION_DEF_SOURCE_IP_PARAM = "Monitoring.AdminSessionDef.SourceIPaddress";
        
        /// The expected value of the encryption method.  
        const std::string M_ADMIN_SEESION_DEF_ENCRYPT_METHOD_PARAM = "Monitoring.AdminSessionDef.EncryptMethod";
        
        /// Intraday logout tolerance mode.
        /// An option to reset or not to reset sequence numbers after Logout.
        const std::string M_ADMIN_SEESION_DEF_INTERDAY_LOGOUT_TOLERANCE_PARAM = "Monitoring.AdminSessionDef.IntradayLogoutToleranceMode";
        
        /// Force SeqNum reset mode.
        /// An option to use 141 tag in Logon message to reset sequence number.
        const std::string M_ADMIN_SEESION_DEF_FORCE_SEQNUM_RESET_PARAM = "Monitoring.AdminSessionDef.ForceSeqNumResetMode";
        
        /// When true, session ignore 'SeqNum too low' at incoming Logon message 
        /// and continue with received SeqNum. 
        const std::string M_ADMIN_SEESION_DEF_IGNORE_SEQNUM_TOO_LOW_LOGON_PARAM = "Monitoring.AdminSessionDef.IgnoreSeqNumTooLowAtLogon";
        
        /// When true TCP buffer (Nagle algorithm) will be disabled for session.
        const std::string M_ADMIN_SEESION_DEF_DISABLE_TCP_BUFFER_PARAM = "Monitoring.AdminSessionDef.DisableTCPBuffer";
        
        /// Enqueued outgoing messages could merged and sent as a single buffer. This parameter controls 
        /// how many messages could be merged into the bunch. The 0 means infinite amount. 
        const std::string M_ADMIN_SEESION_DEF_MAX_MESSAGES_IN_BUNCH_PARAM = "Monitoring.AdminSessionDef.MaxMessagesAmountInBunch";
        
        /// Priority of the socket SendReceive operations. 
        ///
        /// Valid values:
        ///   * EVEN (default) - share worker thread among all session in the Engine
        ///   * AGGRESSIVE_SEND - use dedicated thread to send outgoing messages
        ///   * AGGRESSIVE_RECEIVE - use dedicated thread to send outgoing messages
        const std::string M_ADMIN_SEESION_DEF_SOCKET_OP_PRIORITY_PARAM = "Monitoring.AdminSessionDef.SocketOpPriority";

        //// Engine measuring
        const std::string MEASURING_ENABLE_PARAM = "Measuring.Enable";

        //// DDoS attacks protection options

        /// Enables/disables protection from DDoS attacks.
        const std::string P_DDOS_PROTECTION_ENABLE_PARAM = "ProtectionTCP.Enabled";

        /// Maximum waiting time in milliseconds for first FIX message from new incoming TCP connection.
        /// If message is not received connection will be closed.
        /// 0 means waiting time is not limited.
        const std::string P_DDOS_PROTECTION_WAIT_LOGON_PARAM = "ProtectionTCP.WaitLogon";

        /// Maximum number of incoming TCP connections waiting logon from one host.
        /// Connections that exceed the limit will be closed.
        /// 0 means maximum number is not limited.
        const std::string P_DDOS_PROTECTION_SIZE_WAIT_HOST_MAX_PARAM = "ProtectionTCP.SizeWaitHostMax";

        /// Maximum size of buffer allocated for incoming message.
        /// Connections that exceed the limit will be closed.
        /// 0 means maximum size is not limited.
        const std::string P_DDOS_PROTECTION_SIZE_BUFFER_MAX_PARAM = "ProtectionTCP.SizeBufferMax";

        ////Logger settings
        const std::string LOG_DEVICE_PARAM = "Log.Device";
        const std::string LOG_DEBUG_IS_ON_PARAM = "Log.DebugIsOn";
        const std::string LOG_NOTE_IS_ON_PARAM = "Log.NoteIsOn";
        const std::string LOG_WARN_IS_ON_PARAM = "Log.WarnIsOn";
        const std::string LOG_ERROR_IS_ON_PARAM = "Log.ErrorIsOn";
        const std::string LOG_FATAL_IS_ON_PARAM = "Log.FatalIsOn";
        const std::string LOG_CYCLING_PARAM = "Log.Cycling";
        const std::string LOG_FILE_TIMEZONE_PARAM = "Log.File.TimeZone";
        const std::string LOG_FILE_ROOT_DIR_PARAM = "Log.File.RootDir";
        const std::string LOG_LOG_FILE_NAME_PARAM = "Log.File.Name";
        const std::string LOG_FILE_LOCKED_PARAM = "Log.File.Locked";
        const std::string LOG_FILE_ROTATE_PARAM = "Log.File.Rotate";
        const std::string LOG_FILE_BACKUP_TIME_PARAM = "Log.File.Backup.Time";
        const std::string LOG_FILE_RECREATE_PARAM = "Log.File.Recreate";
        const std::string LOG_FILE_AUTOFLUSH_PARAM = "Log.File.AutoFlush";
        const std::string LOG_EVENT_LOG_SOURCE = "Log.EventLog.EventSource";

        ////Failover settings
        const std::string FAILOVER_MODE_PARAM = "FailOver.Mode";
        const std::string FAILOVER_IS_PRIMARY_PARAM = "FailOver.IsPrimary";
        const std::string FAILOVER_HOST_PARAM = "FailOver.Host";
        const std::string FAILOVER_PORT_PARAM = "FailOver.Port";
        const std::string FAILOVER_COMMAND_PORT_PARAM = "FailOver.CommandPort";

        ////Misc
        const std::string ENABLE_AUTO_SWITCH_TO_BACKUP_CONNECTION_PARAM = "EnableAutoSwitchToBackupConnection";
        const std::string ENABLE_CYCLIC_SWITCH_BACKUP_CONNECTION = "EnableCyclicSwitchBackupConnection";

        /** When it's set to true there is being enabled backward resolve based DNS entry 
		 * spoofing detection mechanism in DNS related functions
         * @note this option is disabled by default
         * @attention If parameter is enabled, user should use Fully Qualified Domain Name (FQDN) or IP addresses
         * as configuration/function parameters (e.g. 'workstation1.epam.com' instead of 'workstation1'). 
		 * Otherwise errors about DNS spoofing will be reported.
         */
        const std::string ENABLE_DNS_ENTRY_SPOOFING_DETECTION = "EnableDnsEntrySpoofingDetection";

        //// Dispatcher settings
        
		/** Defines number of workers to handle outgoing messages. Default is 1 */
        const std::string DISPATCHER_SEND_WORKERS_COUNT = "Dispatcher.SendWorkersCount";
        
		/** Defines number of workers to handle incoming messages. Default value is 3 */
        const std::string DISPATCHER_RECV_WORKERS_COUNT = "Dispatcher.RecvWorkersCount";
        
		/** Defines number of milliseconds to wait for readiness of the socket to send data. 
         * -1 means no polling (low CPU usage but higher latency). 
         * 0 means infinite polling without delay (decreases latency but causes 100% CPU usage). 
         * Default is -1.
        */
		const std::string DISPATCHER_SEND_WORKERS_TIMEOUT = "Dispatcher.SendWorkersTimeout";
        
		/** Defines number of milliseconds to wait for readiness of the socket to receive data. 
         * -1 means no polling (low CPU usage but higher latency). 
         * 0 means infinite polling without delay (decreases latency but causes 100% CPU usage). 
         * Default is -1.
        */
        const std::string DISPATCHER_RECV_WORKERS_TIMEOUT = "Dispatcher.RecvWorkersTimeout";

		/**
		* Defines how to serialize UTCTimestamp or UTCTimeOnly field values:
		* 0 - default mode -  if the value of milliseconds is not equal to 0 then it will be serialized.
		* 1 - always serialize milliseconds
		* 2 - always truncate milliseconds
		* By default this parameter is 0
		* @note It is a global option for FixEngine. 
		* @note It works only when a value is passed as Engine::UTCTimestamp or Engine::UTCTimeOnly.
		*/
		const std::string KEEP_MILLISECONDS_IN_UTCTIME_FIELDS	= "KeepMillisecondsInUTCTimeFields";

		// thread affinity parameters
		/**
		* Defines an affinity mask for all worker pool threads. Default value is 0
		*/
		const std::string WORKER_CPU_AFFINITY	= "WorkerCpuAffinity";
		/**
		* Defines an affinity mask for dispatcher and other auxiliary threads threads. Default value is 0
		*/
		const std::string HELPER_CPU_AFFINITY	= "HelperCpuAffinity";

    }

    /// Session specific parameters prefixes
    namespace SessionName
    {
        const std::string Root = "Session.";
        const std::string Default = "Default";
        const char SENDER_TARGET_DELIMITER = '/';
    }

    /// Schedule specific parameters prefixes
    namespace ScheduleName
    {
        const std::string Root = "Schedule.";
        const std::string Default = "Default";
    }

    /// List of all session specific parameters which can be configured in the engine.properties
    namespace SessionParameters
    {
        /// @see Engine::SessionExtraParameters::host_
        const std::string Host                          = "Host";
        /// @see Engine::SessionExtraParameters::port_
        const std::string Port                          = "Port";
        /// @see Engine::SessionExtraParameters::hbi_
        const std::string HBI                           = "HBI";
        /// @see Engine::SessionExtraParameters::sessionRole_
        const std::string Role                          = "Role";
        /// Initiator role of the session
        const std::string Role_Initiator                = "Initiator";
        /// Acceptor role of the session
        const std::string Role_Acceptor                 = "Acceptor";
        /// @see Engine::SessionExtraParameters::customLogonMessageFileName_
        const std::string CustomLogonMessageFileName    = "CustomLogonMessageFileName";
        /// @see Engine::SessionExtraParameters::parserVersion_
        const std::string ParserVersion                 = "ParserVersion";

        /// @see Engine::SessionExtraParameters::ssl_
        const std::string SSL                           = "SSL";
        /// @see Engine::SessionExtraParameters::sslProtocols_
        const std::string SSLProtocols = "SSLProtocols";
        /// @see Engine::SessionExtraParameters::sslCiphersList_
        const std::string SSLCiphersList = "SSLCiphersList";
        /// @see Engine::SessionExtraParameters::sslCertificate_
        const std::string SSLCertificate = "SSLCertificate";
        /// @see Engine::SessionExtraParameters::sslCertificatePassword_
        const std::string SSLCertificatePassword = "SSLCertificatePassword";
        /// @see Engine::SessionExtraParameters::sslPrivateKey_
        const std::string SSLPrivateKey = "SSLPrivateKey";
        /// @see Engine::SessionExtraParameters::sslPrivateKeyPassword_
        const std::string SSLPrivateKeyPassword = "SSLPrivateKeyPassword";
        /// @see Engine::SessionExtraParameters::sslCACertificate_
        const std::string SSLCACertificate = "SSLCACertificate";
        /// @see Engine::SessionExtraParameters::sslValidatePeerCertificate_
        const std::string SSLValidatePeerCertificate = "SSLValidatePeerCertificate";

        /// @see Engine::SessionExtraParameters::pSenderSubID_
        const std::string SenderSubID                   = "SenderSubID";
        /// @see Engine::SessionExtraParameters::pTargetSubID_
        const std::string TargetSubID                   = "TargetSubID";
        /// @see Engine::SessionExtraParameters::pSenderLocationID_
        const std::string SenderLocationID              = "SenderLocationID";
        /// @see Engine::SessionExtraParameters::pTargetLocationID_
        const std::string TargetLocationID              = "TargetLocationID";
        /// @see Engine::SessionExtraParameters::userName_
        const std::string UserName                      = "Username";
        /// @see Engine::SessionExtraParameters::password_
        const std::string Password                      = "Password";
        /// @see Engine::SessionExtraParameters::userNameTag_
        const std::string UsernameTag                   = "UsernameTag";
        /// @see Engine::SessionExtraParameters::passwordTag_
        const std::string PasswordTag                   = "PasswordTag";
        /// @see Engine::SessionExtraParameters::maskedTags_
		const std::string MaskedTags                   = "MaskedTags";
		/// @see Engine::SessionExtraParameters::hiddenLogonCredentials_
        const std::string HiddenLogonCredentials                   = "HiddenLogonCredentials";
		/// @see Engine::SessionExtraParameters::customSessionType_
        const std::string CustomSessionType             = "CustomSessionType";
		/// @see Engine::SessionExtraParameters::sendingTimestampUnit_
        const std::string SendingTimestampUnit             = "SendingTimestampUnit";

		 /// @see Engine::SessionExtraParameters::fixKey_
        const std::string FixKey	                    = "FixKey";
		/// Value of the Engine::SessionParameters::CustomSessionType parameter
		const std::string CustomSessionType_LME_SELECT	= "LME_SELECT";
        /// Value of the Engine::SessionParameters::CustomSessionType parameter
        const std::string CustomSessionType_CME_SECURE_LOGON = "CME_SECURE_LOGON";
		/// Value of the Engine::SessionParameters::CustomSessionType parameter
		const std::string CustomSessionType_GENERIC		= "GENERIC";
        /// @see Engine::SessionExtraParameters::sourceIPaddress_
        const std::string SourceIPaddress               = "SourceIPaddress";
        const std::string SourceIPaddress_alias1        = "SourceIPAddress";
        /// @see Engine::SessionExtraParameters::intradayLogoutToleranceMode_
        const std::string IntradayLogoutToleranceMode   = "IntradayLogoutToleranceMode";
        /// @see Engine::SessionExtraParameters::forceSeqNumReset_
        const std::string ForceSeqNumReset              = "ForceSeqNumReset";
        /// Valid values:
        /// - NONE
        /// - PKCS
        /// - DES
        /// - PKCS_DES
        /// - PGP_DES
        /// - PGP_DES_MD5
        /// - PEM_DES_MD5
        /// - NA
        /// @see Engine::SessionExtraParameters::encryptMethod_
        const std::string EncryptMethod                 = "EncryptMethod";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_NONE            = "NONE";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_PKCS            = "PKCS";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_DES             = "DES";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_PKCS_DES        = "PKCS_DES";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_PGP_DES         = "PGP_DES";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_PGP_DES_MD5     = "PGP_DES_MD5";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_PEM_DES_MD5     = "PEM_DES_MD5";
        /// Value of the Engine::SessionParameters::EncryptMethod parameter
        const std::string EncryptMethod_NA              = "NA";
        /// @see Engine::SessionExtraParameters::forcedReconnect_
        const std::string ForcedReconnect               = "ForcedReconnect";
        /// @see Engine::SessionExtraParameters::enableMessageRejecting_
        const std::string EnableMessageRejecting        = "EnableMessageRejecting";
        /// @see Engine::SessionExtraParameters::ignoreSeqNumTooLowAtLogon_
        const std::string IgnoreSeqNumTooLowAtLogon     = "IgnoreSeqNumTooLowAtLogon";
        /// @see Engine::SessionExtraParameters::keepConnectionState_
        const std::string KeepConnectionState           = "KeepConnectionState";
        /// @see Engine::SessionExtraParameters::useAsyncConnect_
        const std::string UseAsyncConnect               = "UseAsyncConnect";
        /// @see Engine::SessionExtraParameters::disableTCPBuffer_
        const std::string DisableTCPBuffer              = "DisableTCPBuffer";
        /// @see Engine::SessionExtraParameters::socketRecvBufSize_
        const std::string SocketRecvBufSize             = "SocketRecvBufSize";
        /// @see Engine::SessionExtraParameters::socketSendBufSize_
        const std::string SocketSendBufSize             = "SocketSendBufSize";
        /// @see Engine::SessionExtraParameters::socketBusyPollTime__
        const std::string SocketBusyPollTime            = "SocketBusyPollTime";
        /// @see Engine::SessionExtraParameters::socketTcpMaxSeg__
        const std::string SocketTcpMaxSeg               = "SocketTcpMaxSeg";
        /// @see Engine::SessionExtraParameters::storageType_
        /// Valid values:
        /// - persistent
        /// - transient
        /// - persistentMM
        /// - splitPersistent
        /// - null
        const std::string StorageType                       = "StorageType";
        /// @see Engine::SessionExtraParameters::storageRecoveryStrategy_
        /// Valid values:
        /// - NONE
        /// - CREATE_NEW_ON_ERROR
        const std::string StorageRecoveryStrategy       = "StorageRecoveryStrategy";
        /// @see Engine::SessionExtraParameters::maxMessagesAmountInBunch_
        const std::string MaxMessagesAmountInBunch      = "MaxMessagesAmountInBunch";
        /// Valid values:
        /// - NA
        /// - EVEN
        /// - AGGRESSIVE_SEND
        /// - AGGRESSIVE_RECEIVE
        /// - AGGRESSIVE_SEND_AND_RECEIVE
        /// - DIRECT_SEND
        ///
        /// @see Engine::SessionExtraParameters::socketPriority_
        const std::string SocketPriority                = "SocketOpPriority";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_NA             = "NA";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_EVEN           = "EVEN";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_AGGRESSIVE_SEND    = "AGGRESSIVE_SEND";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_AGGRESSIVE_RECEIVE = "AGGRESSIVE_RECEIVE";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_AGGRESSIVE_SEND_AND_RECEIVE = "AGGRESSIVE_SEND_AND_RECEIVE";
        /// Value of the Engine::SessionParameters::SocketPriority parameter
        const std::string SocketPriority_DIRECT_SEND = "DIRECT_SEND";
        /// @see Engine::SessionExtraParameters::aggressiveReceiveDelay_
        const std::string AggressiveReceiveDelay            = "AggressiveReceiveDelay";
        /// @see Engine::SessionExtraParameters::enableAutoSwitchToBackupConnection_
        const std::string EnableAutoSwitchToBackupConnection    = "EnableAutoSwitchToBackupConnection";
        /// @see Engine::SessionExtraParameters::cyclicSwitchBackupConnection_
        const std::string CyclicSwitchBackupConnection      = "CyclicSwitchBackupConnection";
        /// @see Engine::SessionExtraParameters::handleSeqNumAtLogon_
        const std::string HandleSeqNumAtLogon               = "HandleSeqNumAtLogon";
        /// @see Engine::SessionExtraParameters::reconnectMaxTries_
        const std::string ReconnectMaxTries                 = "ReconnectMaxTries";
        /// @see Engine::SessionExtraParameters::reconnectInterval_
        const std::string ReconnectInterval                 = "ReconnectInterval";
        /// @see Engine::SessionExtraParameters::connectAddress_
        const std::string ConnectAddress                    = "ConnectAddress";
        /// @see Engine::SessionExtraParameters::connectPort_
        const std::string ConnectPort                       = "ConnectPort";
        /// @see Engine::SessionExtraParameters::messagesStorageSize_
        const std::string MessagesStorageSize               = "MessagesStorageSize";
        /// @see Engine::SessionExtraParameters::allowMessageWithoutPossDupFlag_
        const std::string AllowMessageWithoutPossDupFlag    = "AllowMessageWithoutPossDupFlag";
        /// @see Engine::SessionExtraParameters::suppressDoubleResendRequest_
        const std::string SuppressDoubleResendRequest       = "SuppressDoubleResendRequest";
        /// @see Engine::SessionExtraParameters::deliverAppMessagesOutOfOrder_
        const std::string DeliverAppMessagesOutOfOrder      = "DeliverAppMessagesOutOfOrder";
        /// @see Engine::SessionExtraParameters::sendLastMsgSeqNumProcessed_
        const std::string SendLastMsgSeqNumProcessed        = "SendLastMsgSeqNumProcessed";
        /// @see Engine::SessionExtraParameters::validateCheckSum_
        const std::string ValidateCheckSum                  = "ValidateCheckSum";
        /// @see Engine::SessionExtraParameters::generateCheckSum_
        const std::string GenerateCheckSum                  = "GenerateCheckSum";
        /// @see Engine::SessionExtraParameters::logIncomingMessages_
        const std::string LogIncomingMessages               = "LogIncomingMessages";
        /// @see Engine::SessionExtraParameters::resendRequestBlockSize_
        const std::string ResendRequestBlockSize            = "ResendRequestBlockSize";
        /// @see Engine::SessionExtraParameters::outOfSequenceMessagesQueueSize_
        const std::string OutOfSequenceMessagesQueueSize    = "OutOfSequenceMessagesStrategy.QueueSize";
        /// @see Engine::SessionExtraParameters::OutOfSequenceMessagesStrategy_
        const std::string OutOfSequenceMessagesStrategy    = "OutOfSequenceMessagesStrategy";
        /// @see Engine::SessionExtraParameters::IsForwardingFixMessagesToLog
        const std::string IsForwardingFixMessagesToLog    = "IsForwardingFixMessagesToLog";
        /// @see Engine::SessionExtraParameters::useBlockingSockets_
        const std::string UseBlockingSockets                = "UseBlockingSockets";
        /// @see Engine::Transport
        const std::string Transport_SOCKETS                 = "SOCKETS";
        /// @see Engine::Transport
        const std::string Transport_MYRICOM_DBL             = "MYRICOM_DBL";
        /// @see Engine::SessionExtraParameters::transport_
        const std::string Transport                         = "Transport";
        /// @see Engine::SessionExtraParameters::cmeSecureKeysFile_
        const std::string CMESecureKeysFile                     = "CMESecureKeysFile";
        /// @see Engine::SessionExtraParameters::ValidationParameters::isEnabled_
        const std::string Validation_IsEnabled                  = "Validation.IsEnabled";
        /// @see Engine::SessionExtraParameters::ValidationParameters::prohibitTagsWithoutValue_
        const std::string Validation_ProhibitTagsWithoutValue   = "Validation.ProhibitTagsWithoutValue";
        /// @see Engine::SessionExtraParameters::ValidationParameters::verifyTagsValues_
        const std::string Validation_VerifyTagsValues           = "Validation.VerifyTagsValues";
        /// @see Engine::SessionExtraParameters::ValidationParameters::prohibitUnknownTags_
        const std::string Validation_ProhibitUnknownTags        = "Validation.ProhibitUnknownTags";
        /// @see Engine::SessionExtraParameters::ValidationParameters::verifyReperatingGroupBounds_
        const std::string Validation_VerifyReperatingGroupBounds = "Validation.VerifyReperatingGroupBounds";
        /// @see Engine::SessionExtraParameters::ValidationParameters::checkRequiredGroupFields_
        const std::string Validation_CheckRequiredGroupFields   = "Validation.CheckRequiredGroupFields";
        /// @see Engine::SessionExtraParameters::ValidationParameters::allowZeroNumInGroup_
        const std::string Validation_AllowZeroNumInGroup        = "Validation.AllowZeroNumInGroup";
        /// @see Engine::SessionExtraParameters::ValidationParameters::prohibitDuplicatedTags_
        const std::string Validation_ProhibitDuplicatedTags     = "Validation.ProhibitDuplicatedTags";
        /// @see Engine::SessionExtraParameters::ValidationParameters::ignoreUnknownFields_
        const std::string Validation_IgnoreUnknownFields        = "Validation.IgnoreUnknownFields";
        /// @see Engine::FastSessionExtraParameters::enableFastScp_
        const std::string EnableFastScp        = "EnableFastScp";
        /// @see Engine::FastMappingOptions::boolYtoString_
        const std::string FASTCodecParameters_BoolYtoString = "FASTCodecParameters.BoolYtoString";
        /// @see Engine::FastMappingOptions::boolNtoString_
        const std::string FASTCodecParameters_BoolNtoString = "FASTCodecParameters.BoolNtoString";

        /// Defines tag of the field to be used by FIX Engine when dispatching incoming connection.
        /// SessionQualifier is passed in this field. SenderCompID(49), TargetCompID(56)
        /// and this field are used for search of appropriated acceptor among existing registered 
        /// acceptors. The field is optional if application doesn't intend to accept incoming connection.
        const std::string LogonMessageSessionQualifierTag       = "LogonMessageSessionQualifierTag";

        // thread affinity parameters
        /// @see Engine::SessionExtraParameters::cpuAffinity_
        const std::string CpuAffinity           = "CpuAffinity";
        /// @see Engine::SessionExtraParameters::recvCpuAffinity_
        const std::string RecvCpuAffinity       = "RecvCpuAffinity";
        /// @see Engine::SessionExtraParameters::sendCpuAffinity_
        const std::string SendCpuAffinity       = "SendCpuAffinity";

        const std::string CustomRawDataTagStrategies_Prefix = "CustomRawDataTagStrategies.";
        const std::string CustomRawDataTagStrategies_Count = "CustomRawDataTagStrategies.Count";
        const std::string CustomRawDataTagStrategies_Tag_Suffix = ".Tag";
        const std::string CustomRawDataTagStrategies_Strategy_Suffix = ".Strategy";
        const std::string CustomRawDataTagStrategies_Encoding_Suffix = ".Encoding";

        /// @see Engine::SessionExtraParameters::schedule_
        const std::string Schedule = "Schedule";

    }

    /// List of all schedule specific parameters which can be configured in the engine.properties
    namespace ScheduleParameters
    {
        /// @see Engine::CronSessionsScheduleParameters::tradePeriodBegin
        const std::string TradePeriodBegin = "TradePeriodBegin";
        /// @see Engine::CronSessionsScheduleParameters::tradePeriodEnd
        const std::string TradePeriodEnd = "TradePeriodEnd";
        /// @see Engine::CronSessionsScheduleParameters::dayOffs
        const std::string DayOffs = "DayOffs";
        /// @see Engine::CronSessionsScheduleParameters::timezone
        const std::string Timezone = "Timezone";
        /// @see Engine::CronSessionsScheduleParameters::lateExecution
        const std::string LateExecution = "LateExecution";
    }

    /**
     * Loader for any file with properties like 'engine.properties'.
     */
    class PropertiesFile
    {
    public:
        typedef std::map<std::string, std::string> PropertyMap;

        /**
         * Returns properties loaded from file.
         * @throw Utils::Exception if file read error.
         */
        static void load( const std::string& path, PropertyMap* properties)
        {
            loadImpl(path.c_str(), properties, &addProperty);
        }

    private:
        V12_API static void loadImpl(const char* path, PropertyMap* properties, void (*addPropertyFun)(const char* name, const char* value, PropertyMap* properties));

        static void addProperty(const char* name, const char* value, PropertyMap* properties)
        {
            (*properties)[name] = value;
        }
    };

    /**
     * FixEngine's configuration parameters.
     */
    class V12_API FAProperties
    {
    public:
        /// Returns log directory's name.
        std::string const& getLogDir() const;

        /// Returns engine's log file name.
        std::string const& getLogFileName() const;

        /**
         * Returns engine listen port number.
         * @note if the value is 0 then the communication level must be disabled.
         */
        int getListenPort() const;

        /**
         * Returns engine listen ports.
         * @note if the number of the ports is 0 then the communication level must be disabled.
         */
        ListenPorts getListenPorts() const;

        /**
         * Returns the number of workers.
         */
        int getNumberOfWorkers() const;

        /**
         * Returns the logon time frame.
         */
        int getLogonTimeFrame() const;

        /**
         * Returns the logout time frame.
         */
        int getLogoutTimeFrame() const;

        /**
         * Returns true if the validity of the input message must be checked,
         * false otherwise.
         */
        bool getMessageMustBeValidated() const;

        /**
         * Returns reasonable transmission time - in % from HeartBeat value.
         */
        int getReasonableTransmissionTime() const;

        /**
         * Returns true if the third-party routing is enabled, false otherwise.
         */
        bool isThirdPartyRoutingEnabled() const;

        /**
         * Returns engine's local IP address to bind to.
         */
        std::string getListenAddress() const;

        /**
         * Returns engine's local IP address to send from.
         */
        std::string getConnectAddress() const;

        /**
         * Returns true if "CreateUnregisteredAcceptorSession" mode must be used, false otherwise.
         */
        bool isCreateUnregisteredAcceptorSessionMode() const;

        /**
         * Returns true if "UnregisteredAcceptor.IgnoreSeqNumTooLowAtLogon" mode must be used, false otherwise.
         */
        bool isIgnoreseqNumTooLowAtLogon() const;

        /**
         * Returns true when "AllowZeroNumInGroup" defined and true, false otherwise.
         */
        bool isAllowZeroNumInGroup() const;

        /**
         * Returns true when "AllowEmptyFieldValue" defined and true, false otherwise.
         */
        bool isAllowEmptyFieldValue() const;

        /**
         * Returns storage type of the unregistered sessions.
         */
        MessageStorageType getUnregisteredAcceptorSessionStorageType() const;

        /**
         * Returns the time interval between tries to restore the telecommunications link (in milliseconds)
         */
        int getReconnectInterval() const;

        /**
         * Returns the maximum number of tryes to deliver application message.
         */
        int getMaxDeliveryTries() const;

        /**
         *  Returns the time interval between tries to deliver application message (in milliseconds).
         */
        int getDeliveryTriesInterval() const;

        /**
         * Returns the maximum number of tries to to restore the telecommunications link.
         * If -1 then the number of tries is unlimited.
         */
        int getMaxReconnectTries() const;

        /**
         * Returns true if the incoming messages from a remote FIX Engine must be logged, otherwise false.
         */
        bool getLogIncomingMessages () const;

        /**
         * Returns the maximum number of outgoing messages that can be retransmitted in case of Resend Requests.
         * If '-1' then the size is unlimited.
         */
        int getOutgoingMessagesStorageSize() const;

        /**
         * Returns the license file name.
         */
        std::string const& getLicenseFileName() const;

        /**
         * Returns true if it is necessary to check that the version of outgoing messages is equal to
         * the version of the session otherwise false.
         */
        bool getCheckOutgoingMsgsVer() const;

        /**
         * Returns true if the automatic failover mechanism is on. If false then it is off.
         */
        bool isFailOverMode() const;

        enum FAIL_OVER_ROLE { NA, PRIMARY, STAND_BY};

        /**
         * Returns the current failover role.
         */
        FAIL_OVER_ROLE getFailOverRole() const;

        /**
         * Returns the stand-by node address.
         */
        std::string getFailOverHost() const;

        /**
         * Returns Failover Data Port.
         */
        int getFailOverDataPort() const;

        /**
         * Returns Failover Command Port.
         */
        int getFailOverCommandPort() const;

        /**
         * Returns true if the Intraday Logout Tolerance mode is active, otherwise - false.
         */
        bool isIntradayLogoutToleranceMode() const;

        /**
         * Returns the top of the directory tree under which the engine's configuration,
         * and log files are kept.
         */
        std::string const& getRoot() const;

        bool getGatherSessionStatistic() const;

        bool isExtraSafeMode() const;

        /**
         * Returns true if it is necessary to check that the version of outgoing messages is equal to
         * the version of the session otherwise false.
         */
        bool getCheckRequiredGroupFields() const;

        /**
        * FIX tags that do not belong to the message in compliance with the version
        * specified for a FIX session, but that should still be
        * allowed (or required) instead of causing a rejection of the FIX message.
        * The example below means:
        * for FIX 4.2, MsgType 8 (Execution Report): allow fields 639 and 204
        * for FIX 4.1, MsgType 9 (Order Cancel Reject): require field 41 (because the '?' symbol is absent).
        *
        * FIX42:8:639?,204?;FIX41:9:41
        */
        std::string const& getAdditionalFieldsFileName() const;

        /**
         * An option to send a Logon message with the ResetSeqNumFlag set
         * after each 24 hour period of session's activity to establish a new set
         * of sequence numbers (starting with 1).
         *
         * @note This option does not affect sessions which use version 4.0 of the FIX
         * protocol.
         */
        bool isResetSeqNumAfter24hoursMode() const;

        /**
         * An option to write timestamps in the log files.
         */
        bool isTimestampsInLogsMode() const;

        /**
         * An option to automatically reset sequence number of the unregistered acceptors.
         */
        ForceSeqNumResetMode forceSeqNumResetMode() const;

        /**
         * Returns true if "UnregAcceptorsRejectMessageWhileNoConnection" contains true.
         */
        bool unregisteredAcceptorMessageRejectingEnabled()const;

        /** Returns value of the "MessageTimeToLive".  */
        int messageTimeToLive()const;

        bool isRebuildIndexEnabled() const;
        bool isRejectFlagSupportRequired() const;

        unsigned int getResendMessagesBlockSize()const;

        /** Returns value of "DictionariesFilesList" property*/
        std::string const& getDictionariesFilesList() const;

        /** Returns max slice size for SlicedPersistentMsgStorage */
        std::size_t getSlicedStorageSliceSize() const;

        /** Returns path to SlicedPersistentMsgStorage backup directory */
        std::string const& getSlicedStorageBackupDir() const;

        /** Returns std::string value of given property*/
        std::string getStringProperty( const std::string& propertyName ) const;

        /** Returns bool value of given property*/
        bool getBinaryProperty( const std::string& propertyName ) const;

        /** Returns int value of given property*/
        int getIntegerProperty( const std::string& propertyName ) const;

        /** Returns true if property is exists*/
        bool existsProperty( const std::string& propertyName ) const;

        /** Returns SSL CA certificates path*/
        std::string getSSLCACertificatePath() const;

        /** Returns SSL certificate path*/
        std::string getSSLCertificatePath() const;
        
        /** Returns SSL private key path*/
        std::string getSSLPrivateKeyPath() const;
        
        /** Returns is SSL sould validate peer certificate*/
        bool isSSLValidatePeerCertificate() const;

        /** Returns SSL protocols value acceptable by SSLClientContext*/
        int getSSLUseProtocols() const;

        /** Returns configured SSL ciphers list*/
        std::string getSSLCiphersList() const;

        /** Returns server specific SSL context populated with properties configured values including certificate and private key data using getSSLCertificatePath() and getSSLPrivateKeyPath() respectively. */
        System::SSLServerContext getSSLServerContext() const;

        /** Returns client specific SSL context populated with properties configured values. 
        * @param configurator - custom SSL context configurator.
        */
        System::SSLClientContext getSSLClientContext( const System::SSLContextConfiguratorPtr& configurator = System::SSLContextConfiguratorPtr() ) const;

        /** Returns SSLContextConfigurator instance to use as configuration source in for SSLContext server specific SSL context (call to getSSLClientContext() for example). */
        static System::SSLContextConfiguratorPtr getSSLContextConfiguratorInstance( int protocols, const std::string& ciphersList, const std::string& caFile, const std::string& certificateFile, const std::string& privatekeyFile, const std::string& certificatePassword, const std::string& privateKeyPassword, bool validatePeerCertificate );

        typedef std::map<SessionId, SessionExtraParameters> ConfiguredSessionsMap;
        /** Returns map of configured sessions parameters that can be used during sessions creating with createSession() routine*/
        ConfiguredSessionsMap getConfiguredSessions(const SessionRole filterInRole = NA_SESSION_ROLE) const;

        /** Returns path to Timezones definition file*/
        const std::string getTimezoneDB() const;

        /** Returns Timezones converter instance created using Timezones definition file configured*/
        TimezoneConverterPtr getTimezoneConverter() const;

        typedef std::map<std::string, CronSessionsScheduleParameters> ConfiguredSchedulesMap;
        /** Returns map of configured schedules parameters*/
        ConfiguredSchedulesMap getConfiguredSchedules() const;
    private:
        friend class FixEngineImpl;
        friend class FixEngine;

        /**
        * Constructor.
        */
        FAProperties() {}

        /**
        * Copy constructor.
        */
        FAProperties( const FAProperties& );

        /**
        * Destructor.
        */
        ~FAProperties() {}
    }; // class FAProperties
} // namespace Engine
#endif //__FAProperties_h__
