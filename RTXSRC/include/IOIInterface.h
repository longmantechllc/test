#pragma once
#include <string>
#include <vector>

struct IOIMarketData
{
    std::string mmid;
    Price pi {0.0};
    Price pfof{0.0};
    int size {0};
    int priority {0};
    uint64_t timeStamp {0};
};
// C.127: A class with a virtual function should have a virtual or protected destructor
class IOIInterface {
public:
    //virtual ~IOIInterface() = default;
    virtual std::vector<IOIMarketData> SubscribeIOI(std::string symbol, bool isBuy) = 0;
};

class IOISubscriber {
public:
    //virtual ~IOISubscriber() = default;
    //virtual void OnIOIUpdate(const IOIMarketData& ioiData) = 0;                               //OSM call this function to send ioi data to SOR
    virtual void SetIOIInterface(IOIInterface* ioiInt) = 0;                                   //OSM call this function to sendioi data to SOR

protected:
    IOIInterface *m_ioiInterface;
};
