#pragma once
#include <unordered_map>

#include <rapidjson/document.h>
#include <json_dto/pub.hpp>
#include "spdlog/spdlog.h"
#include "ws_connection.h"
#include "common.h"

namespace Crypto {
class WsApi {
protected:
    rapidjson::Document document_;
    std::shared_ptr<WebSocket> engine_;
    uint64_t check_connection_interval_ = 0;
    std::string ws_api_name_ = "";

    using ws_conn_hdlr_ptr = std::shared_ptr<WsConnection>;
    std::unordered_map<uint32_t, ws_conn_hdlr_ptr> connections_;
    uint32_t next_send_ws_index_ = 0;

    template<typename EVT_TYPE, typename MSG_HEADER_T, typename SUBSCRIBE_RESPONSE_T = void, 
        typename INTRVL_MSG_T = void>
    json_message_t on_public_message(
            std::string_view in,
            std::function<void(EVT_TYPE etype, const rapidjson::Document &)> 
                on_event_cb) {
#ifndef NDEBUG
    spdlog::debug("<WsApi::on_message> : got message size {}: {}", in.size(), in);
#endif
        try {
            this->document_.SetNull();
            this->document_.GetAllocator().Clear();
            this->document_.Parse<rapidjson::kParseDefaultFlags>(
                in.data(), in.size());
            json_dto::check_document_parse_status(this->document_);
        } catch (const std::exception &e) {
            spdlog::error("<WsApi::on_public_message> : error parsing {} on {}", 
                        e.what(), in);
        }

        try {
            static MSG_HEADER_T stream_header;
            stream_header.reset();
            json_dto::from_json(this->document_, stream_header);
            on_event_cb(stream_header.event_type, this->document_);
        } catch (const std::exception &e) {
            if constexpr (!std::is_same_v<void, SUBSCRIBE_RESPONSE_T>) {
                try {
                    json_dto::from_json<SUBSCRIBE_RESPONSE_T>(this->document_);
                    spdlog::info("<WsApi::on_public_message> SUBSCRIBE_SUCCESS {}", in);
                    return {
                        Crypto::exchange_message_t::SUBSCRIBE_SUCCESS, 
                        nullptr
                    };
                } catch (const std::exception &e) {
                    if constexpr (!std::is_same_v<void, INTRVL_MSG_T>) {
                        try {
                            json_dto::from_json<INTRVL_MSG_T>(this->document_);
                            return {
                                Crypto::exchange_message_t::DEFAULT,
                                nullptr
                            };
                        } catch (const std::exception &e) {

                        }
                    }
                    
                    spdlog::error("<WsApi::on_public_message> : {} on {}", e.what(), in);
                }
            }
        }
        return {Crypto::exchange_message_t::DEFAULT, nullptr};
    }

    WsApi(std::shared_ptr<WebSocket> engine, 
        uint32_t check_connection_interval = 0) 
    : engine_(std::move(engine))
    , check_connection_interval_(check_connection_interval) { // in ms

        if (check_connection_interval_ > 0) {
            // spdlog::info("<WsApi> : start receiving timeout check in [{}] ms interval...",
            //     check_connection_interval_);
            // crypto::comm::selector::add_timer(
            //     [&] {
            //         this->check_receive_timeout(crypto::comm::selector::cycle_now());
            //         // request every specified seconds (recursive interval)
            //         // in nanoseconds
            //         return uint64_t(check_connection_interval_ * 1'000'000);
            //     },
            //     // first delay is 1 minute + offset seconds
            //     uint64_t(60'000'000'000 + (rand() % 60) * 1'000'000'000)
            // );
        }
    }

public:
    inline void disconnect_ws_conn(uint32_t conn_idx) {
        auto itr = this->connections_.find(conn_idx);
        if (itr != this->connections_.end()) {
            itr->second->disconnect();
        }
    }

    // inline void check_receive_timeout(uint64_t now_ts) {
    //     uint64_t bad_conns = 0;
    //     std::string bad_report;
    //     for(auto& [index, ws_connection_ptr] : connections_) {
    //         if (ws_connection_ptr->receive_data_timeout(now_ts)) {
    //             ws_connection_ptr->reconnect();
    //             bad_conns++;
    //             bad_report += std::to_string(index)+ ":" 
    //                 + std::to_string(ws_connection_ptr->get_recv_count()) + ":"
    //                 + std::to_string(ws_connection_ptr->get_last_recv_count()) + ",";
    //         } 
    //         // else {
    //         //     good_conns++;
    //         //     good_report +=std::to_string(index)+ ":" 
    //         //         + std::to_string(ws_connection_ptr->get_recv_count()) + ",";
    //         // }
    //     }
    //     if (bad_conns > 0) {
    //         spdlog::info("ws_api stats, bad_conns({}):[{}]", 
    //             bad_conns, bad_report);
    //     }
    // }

    inline bool send_msg(uint32_t ws_idx, const std::string& msg) {
        auto itr = this->connections_.find(ws_idx); 
        if (itr == this->connections_.end()) {
            spdlog::error("<ws send_msg> cannot find ws connection with idx={}", ws_idx);
            return false;
        }
        auto& ws_conn = itr->second;
        ws_conn->send_message(msg);
        return true;
    }

    inline bool send_msg(const std::string& msg) {
        auto sz = this->connections_.size();
        this->next_send_ws_index_ = (this->next_send_ws_index_ + 1) % sz;
        auto ws_conn = this->connections_[this->next_send_ws_index_];

        size_t count = 0;
        while(!ws_conn->is_connected()) {
            this->next_send_ws_index_ = (this->next_send_ws_index_ + 1) % sz;
            ws_conn = this->connections_[this->next_send_ws_index_];
            count++;
            if (count >= sz) {
                break;
            }
        }
        if (ws_conn->is_connected()) {
            ws_conn->send_message(msg);
            return true;
        }
        return false;
    }

    void service() {
        while(1) {
            for(auto& [idx,c] : connections_) {
                spdlog::debug("running WS connection {}", idx);
                c->service();
            }
        }
    }

};

}  // Crypto