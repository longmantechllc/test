// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#ifndef B2BITS_BATSSAMPLE_INSTR_LISTENER_H
#define B2BITS_BATSSAMPLE_INSTR_LISTENER_H

#include <string>
#include <vector>

#include <B2BITS_BatsApplication.h>
#include <B2BITS_Mutex.h>
#include <B2BITS_Guard.h>

class BatsClient;

/* -------------------------------------------------------------------------------------/
    instrument listener
*/
struct DepthItem
{
    DepthItem() : price(0), qty(0), orders(0) {}
    DepthItem(double p, System::u32 q, System::u32 o) : price(p), qty(q), orders(o) {}
    double price;
    System::u32 qty;
    System::u32 orders;
};

class InstrListener : public Bats::InstrumentListener
{
    typedef Utils::Guard<System::Mutex> guard_type;
public:
    InstrListener(BatsClient* bc, Bats::UnitService* const svc, const std::string& symbol, System::u16 depth) 
        : bc_(bc), svc_(svc), symbol_(symbol)
        , depth_(depth), askDepth_(0), bidDepth_(0), askBook_(depth), bidBook_(depth){}

    const std::string& getSymbol() const { 
        return symbol_; 
    }
    Bats::UnitService* getService() const { 
        return svc_; 
    }
    std::vector<DepthItem> getBook(char side) {
        guard_type guard(mutex_);
        return side_book(side);
    }
    System::u16 getDepth() const {
        return depth_;
    }
    
    virtual void onReset(Bats::ResetReason::Type rr);
    virtual void onUnsubscribed();
    virtual void onMessage(const Bats::AuctionSummaryMsg* /*msg*/, System::u32 /*seconds*/, Bats::Timestamp& /*ts*/) {}
    virtual void onMessage(const Bats::AuctionUpdateMsg* /*msg*/, System::u32 /*seconds*/, Bats::Timestamp& /*ts*/) {}

    virtual void addBookItem(const Bats::InstrumentListener::InstrumentCtx& iCtx, const Bats::InstrumentListener::OrderCtx& ctx, System::u32 orders, System::u16 level, Bats::Timestamp& ts);
    virtual void changeBookItem(const Bats::InstrumentListener::InstrumentCtx& iCtx, const Bats::InstrumentListener::OrderCtx& ctx, System::u32 orders, System::u16 level, Bats::Timestamp& ts);
    virtual void removeBookItem(const Bats::InstrumentListener::InstrumentCtx& iCtx, char side, System::u16 level, Bats::Timestamp& ts);
private:
    std::vector<DepthItem>& side_book(char side) { return side == 'B' ? bidBook_ : askBook_; }
private:
    BatsClient* const bc_;
    Bats::UnitService* const svc_;
    const std::string symbol_;
    const System::u16 depth_;
    System::u16 askDepth_;
    System::u16 bidDepth_;
    std::vector<DepthItem> askBook_;
    std::vector<DepthItem> bidBook_;
    mutable System::Mutex mutex_;
};


#endif  //  B2BITS_BATSSAMPLE_INSTR_LISTENER_H