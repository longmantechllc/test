#include <magic_enum.hpp>
#include <exchanges/utils/ws_engine.h>
#include <exchanges/utils/ws_connection.h>
#include <filesystem>
#include <influx/influx_client.hpp>
#include <utils/perf_counter.hpp>
#include <utils/utils.h>

namespace crypto::exchanges::connection {

std::string WsEngine::decompressed_data = {};

static crypto::utils::PerfCounter lws_event_cb_cycles = crypto::utils::PerfCounter("lws_event_cb_cycles");
static crypto::utils::PerfCounter lws_callback_client_receive_cycles = crypto::utils::PerfCounter("lws_callback_client_receive_cycles");
static crypto::utils::PerfCounter lws_callback_decompress_cycles = crypto::utils::PerfCounter("lws_callback_decompress_cycles");
static crypto::utils::PerfCounter lws_on_message_cycles = crypto::utils::PerfCounter("lws_on_message_cycles");

WsEngine::WsEngine(protocol_type type) : context(nullptr) {
    WsEngine::decompressed_data.reserve(4096);
    this->init_context(type);
    crypto::influx::RegisterCounter(&lws_event_cb_cycles);
    crypto::influx::RegisterCounter(&lws_callback_client_receive_cycles);
    crypto::influx::RegisterCounter(&lws_callback_decompress_cycles);
    crypto::influx::RegisterCounter(&lws_on_message_cycles);
}

WsEngine::~WsEngine() {
    if (context) {
        lws_context_destroy(context);
    }
}

template<bool skip_compression>
int WsEngine::event_cb(
        struct lws *wsi, enum lws_callback_reasons reason, void *user, 
        void *in, size_t len) {
    auto *connection = reinterpret_cast<WsConnection *> (user);
    int64_t start = crypto::utils::get_utc_timestamp_nanoseconds().count();
    switch (reason) {
        case LWS_CALLBACK_CLIENT_ESTABLISHED: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_ESTABLISHED...",
                connection->get_index());
            connection->on_connect(wsi);
            break;
        }
        case LWS_CALLBACK_RECEIVE_PONG: {
            lws_validity_confirmed(wsi);
        }
        case LWS_CALLBACK_CLIENT_RECEIVE: {
        	int64_t client_receive_start = crypto::utils::get_utc_timestamp_nanoseconds().count();
#ifndef NDEBUG
            spdlog::debug("<event_cb> [{}]: LWS_CALLBACK_CLIENT_RECEIVE...",
                connection->get_index());
#endif
            lws_validity_confirmed(wsi);
            std::string_view str_result;
            auto x = static_cast<char*>(in);

            if constexpr (skip_compression) {
                 str_result = std::string_view(x, len);
            } else {
                if (lws_frame_is_binary(wsi)) {
                    int64_t decompress_start = crypto::utils::get_utc_timestamp_nanoseconds().count();
                    WsEngine::decompressed_data = gzip::decompress(
                        x, len, memcmp(x, "\x1f\x8b\b", 2) != 0);
                    str_result = std::string_view(WsEngine::decompressed_data);
                    int64_t decompress_end = crypto::utils::get_utc_timestamp_nanoseconds().count();
                    lws_callback_decompress_cycles.IncrementCounter(decompress_end - decompress_start);
                } else {
                    str_result = std::string_view(x, len);
                }
            }
// #ifndef NDEBUG
//             spdlog::debug("<event_cb> [{}]: received len={}, {} ...",
//                 connection->get_index(), str_result.size(), str_result.substr(0,300));
// #endif
            int64_t on_message_start = crypto::utils::get_utc_timestamp_nanoseconds().count();
            connection->on_message(wsi, str_result);
            int64_t client_receive_end = crypto::utils::get_utc_timestamp_nanoseconds().count();
            lws_callback_client_receive_cycles.IncrementCounter(client_receive_end - client_receive_start);
            lws_on_message_cycles.IncrementCounter(client_receive_end - on_message_start);
            break;
        }
        case LWS_CALLBACK_CLIENT_WRITEABLE: {
            spdlog::debug("<event_cb> [{}]: LWS_CALLBACK_CLIENT_WRITEABLE...",
                connection->get_index());
            bool flag = connection->flush_send_buffer(wsi);
            if (!flag) {
                spdlog::info("<event_cb>: disconnecting ws[{}]...",
                    connection->get_index());
                return 1;
            }
            break;
        }
        case LWS_CALLBACK_GET_THREAD_ID: {
            return std::hash<std::thread::id>{}(std::this_thread::get_id());
        }
        case LWS_CALLBACK_CLIENT_CLOSED: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_CLOSED...",
                connection->get_index());
            connection->on_disconnect();
            return -1;
        }
        case LWS_CALLBACK_CLOSED_CLIENT_HTTP: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLOSED_CLIENT_HTTP...",
                connection->get_index());
            connection->on_disconnect();
            return -1;
        }
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_CONNECTION_ERROR...",
                connection->get_index());
            connection->on_disconnect();
            return 1;
        }
        case LWS_CALLBACK_CLIENT_RECEIVE_PONG: 
            //spdlog::info("<event_cb> : LWS_CALLBACK_CLIENT_RECEIVE_PONG");
            connection->on_ws_pong(wsi);
            break;
        default: 
            if (connection) {
                spdlog::info("<event_cb> [{}]: notice {}", 
                    connection->get_index(), magic_enum::enum_name(reason));
                connection->on_connect(wsi);
            } else {
                spdlog::info("<event_cb> w/o connection: notice {}", 
                    magic_enum::enum_name(reason));
            }
            break;
    }
    int64_t end = crypto::utils::get_utc_timestamp_nanoseconds().count();
    lws_event_cb_cycles.IncrementCounter(end - start);
    return 0;
}

static const struct lws_extension extensions[]
{
    {
        "permessage-deflate",
        lws_extension_callback_pm_deflate,        
        "permessage-defalte; client_no_context_takeover; server_no_context_takeover; client_max_window_bits"
    },
    { NULL, NULL, NULL }    
};

void WsEngine::init_context(protocol_type type) {
    struct lws_context_creation_info info{};
    memset(&info, 0, sizeof(info));

    info.port = CONTEXT_PORT_NO_LISTEN;
    info.protocols = protocols[static_cast<uint8_t>(type)];
    static const lws_retry_bo_t retry = {
        0,
        0,
        0,
        2,
        4,
        0
    };
    info.retry_and_idle_policy = &retry;
    info.timeout_secs = 2;
    info.gid = -1;
    info.uid = -1;
    info.ssl_cipher_list = "RC4-MD5:RC4-SHA:AES128-SHA:AES256-SHA:HIGH:!DSS:!aNULL";    
#ifndef LWS_WITHOUT_EXTENSIONS
    info.extensions = extensions;
#endif
    // This option is needed here to imply LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT
    // option, which must be set on newer versions of OpenSSL.
    info.options = LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
    
    if (std::filesystem::exists("/etc/ssl/certs/ca-bundle.crt"))
    {
        info.client_ssl_ca_filepath = "/etc/ssl/certs/ca-bundle.crt";  
    }
    else if (std::filesystem::exists("/etc/ssl/certs/ca-certificates.crt"))
    {
        info.client_ssl_ca_filepath = "/etc/ssl/certs/ca-certificates.crt";    
    }  
	// Enable keepalive in the lws context, and set it to 5 seconds
    info.ka_time = 5;
    // Try to get a response from the peer before giving up and killing the connection
    info.ka_probes = 3;
    // probe interval
    info.ka_interval = 5;    
    this->context = lws_create_context(&info);
    spdlog::info("<init_context> : initialisation VX589 complete...");    
}

void WsEngine::service() {
    lws_service(this->context, -1);
}

}  // namespace crypto::exchanges::connection
