#pragma once

#include <string>
#include <map>
#include <iosfwd>

namespace Aux
{

    class Properties
    {

    public:
        static const char* const ACCEPTOR_CFG;
        static const char* const INITIATOR_CFG;
        /// send pending cancel
        static const char* const PENDING_CANCEL;
        /// send pending cancel
        static const char* const PENDING_CANCEL_REPLACE;
        /// Router instance name
        static const char* const ROUTER_INSTANCE_NAME;
        /// Virtual destination file
        static const char* const VIRTUAL_DEST_FILE;
        /// how often to report MD queue full
        static const char* const MD_CHECKING_TIME;
        /// Admin port
        static const char* const ADMIN_PORT;
        /// extra logging for total/open orders
        static const char* const EXTRA_LOGGING;
        /// MD client name
        static const char* const MD_CLIENT_NAME;
        /// recovery mode
        static const char* const RECOVERY_MODE;
        /// IOI server
        static const char* const IOI_SERVER;
        static const char* const IOI_PORT;
        /// DB connection parameters
        static const char* const DB_HOST;
        static const char* const DB_PORT;
        static const char* const DB_USER;
        static const char* const DB_PWD;
        static const char* const DB_LOGGING;

        static const char* const SIM_MD;
        static const char* const SYMBOL_FILE;
        static const char* const SIM_EXCH;

        /**
         * Constructor.
         * Reads a property list (key and element pairs) from the input stream.
         *
         * A line that contains only whitespace or whose first non-whitespace
         * character is an ASCII # or ! is ignored
         * (thus, # or ! indicate comment lines).
         *
         * Note: If required property is not found the default value is used.
         */
        Properties( std::istream* apIStream );

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter
         *
         * @throw Exception if such key does not exist.
         */
        std::string getString( const std::string& key ) const;

        bool isPropertyExists( const std::string& key )const;

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter; if property holds incorrect value will be
         *  set to 0.
         *
         * @throw Exception if such key does not exist.
         */
        int getInteger( const std::string& key ) const;

        bool getBool( const std::string& key ) const;

    private:
        typedef std::map<std::string, std::string> MapType;
        MapType m_pairs;
    };

}



