// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_BlockRef.h
/// Contains FixDictionary2::BlockRef class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_BlockRef_H
#define H_B2BITS_FixDictionary2_BlockRef_H

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_MessageItem.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class BlockRef
    *   @brief Represents BlockRef interface.
    */
    class V12_API BlockRef B2B_SEALED
        : public MessageItem
    {
    public:
        /** Copy Constructor. */
        BlockRef( BlockRef const& blockRef );

        /** Constructor.
        * @param id Id of the referenced block
        */
        BlockRef( std::string const& id );

        /** Returns id of referenced Block
        *
        * @return id of referenced Block
        */
        std::string const& id() const;

        /** Returns id of referenced Block
        *
        * @return id of referenced Block
        */
        std::string const& getId() const {
            return id();
        }

        /** Returns description of the blockref */
        std::string const& getDescription() const throw();

        /** Assigns description of the blockref */
        void setDescription(std::string const& value);

        /** Returns description of the blockref */
        std::string const& description() const throw() {
            return getDescription();
        }

    public: // MessageItem contract
        /** Make copy of object */
        virtual BlockRefT::Ptr clone() const B2B_OVERRIDE {
            return new BlockRef( *this );
        }

        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

        /** 
        * Returns type of the Message Item
        */
        virtual MessageItemName messageItemName() const throw() B2B_OVERRIDE {
            return MESSAGEITEM_BLOCKREF;
        }

        /** Try downcast to BlockRef class
        *
        * @return downcast to BlockRef class
        */
        virtual BlockRefT::Ptr toBlockRef() B2B_OVERRIDE;

        /** Try downcast to BlockRef class
        *
        * @return downcast to BlockRef class
        */
        virtual BlockRefT::CPtr toBlockRef() const B2B_OVERRIDE;

    protected:
        /** Destructor */
        virtual ~BlockRef() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;

        BlockRef& operator= (BlockRef const&) B2B_DELETE_FUNCTION;
    };

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_BlockRef_H

