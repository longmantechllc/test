// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#ifndef B2BITS_BATSSAMPLE_CLIENT_H
#define B2BITS_BATSSAMPLE_CLIENT_H

#include <map>
#include <utility>
#include <string>
#include <deque>
#include <vector>

#include <B2BITS_BatsApplication.h>
#include <B2BITS_AutoEvent.h>
#include <B2BITS_Mutex.h>
#include <B2BITS_Guard.h>

#include "UnitListener.h"

using namespace Bats;

class BatsClient;
class InstrListener;

/* -------------------------------------------------------------------------------------/
    application listener
*/
struct AppListener : Bats::ApplicationListener
{
    AppListener(BatsClient* bc) : bc_(bc), incThreads_(0), workerThreads_(0) {}

    void onStartThread(ThreadType::Type type, const std::string& /*channelId*/, System::u8 /*unitId*/, unsigned /*index*/, unsigned /*count*/) {
        ++(type == ThreadType::WorkerThread ? workerThreads_ : incThreads_);
    }
    void onStopThread(ThreadType::Type type, const std::string& /*channelId*/, System::u8 /*unitId*/, unsigned /*index*/, unsigned /*count*/) {
        --(type == ThreadType::WorkerThread ? workerThreads_ : incThreads_);
    }
    void onServiceAdded(UnitService* service);
    void onServiceRemoving(UnitService* service);

    BatsClient* const bc_;
    volatile unsigned incThreads_;
    volatile unsigned workerThreads_;
};


/* -------------------------------------------------------------------------------------/
    BatsClient class
*/
class BatsClient
{
    typedef Utils::Guard<System::Mutex> guard_type;
    typedef std::map<Bats::UnitService*, UnitListener*> unit_listener_map;
    typedef std::vector<InstrListener*> instr_listener_vector;
public:
    BatsClient() : application_(NULL), appListener_(this), obIndex_(0) {}
    ~BatsClient();
    void run(Bats::ApplicationOptions options);

    void onUnsubscribed(InstrListener* l);
    void onServiceAdded(UnitService* service);
    void onServiceRemoving(UnitService* service);

    void notify() { event_.set(); }
private:
    bool onKeyPressed(int key);
    void printBook();
private:
    Bats::Application* application_;
    AppListener appListener_;
    unsigned obIndex_;

    unit_listener_map unitsMap_;
    instr_listener_vector instrListeners_;

    System::Mutex unitsMutex_;
    System::Mutex instrMutex_;
    System::AutoEvent event_;
};

inline void AppListener::onServiceAdded(UnitService* service) {
    bc_->onServiceAdded(service);
}

inline void AppListener::onServiceRemoving(UnitService* service) {
    bc_->onServiceRemoving(service);
}

#endif  //  B2BITS_BATSSAMPLE_CLIENT_H