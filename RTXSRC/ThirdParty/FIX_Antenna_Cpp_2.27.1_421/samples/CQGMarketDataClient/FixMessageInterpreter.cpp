// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include "FixMessageInterpreter.h"

#include <cstddef>
#include <algorithm>
#include <cstddef>

#include <B2BITS_TagValue.h>
#include <B2BITS_FIXGroup.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_String.h>

#include "MarketDataStorage.h"
#include "SecurityDef.h"

#include <iostream>

using namespace Engine;

namespace Cqg
{
    namespace FIXFields
    {
        using namespace ::FIXFields;

        static const int OpenCloseSettleFlag   = 286;
        static const int NoMdFeedTypes         = 1141;
        static const int MDFeedType            = 1022;
    } // namespace Cqg::FIXFields {

    namespace MDUpdateAction
    {
        static char const New       = '0';
        static char const Change    = '1';
        static char const Delete    = '2';
        static char const Overlay   = '5';
    } // namespace MDUpdateAction {

    namespace OpenCloseSettleFlag
    {
        static const int Prev           = 4;
        static const int Theoretical    = 5;
    } // namespace OpenCloseSettleFlag

    namespace QuoteCondition
    {
        static char const ExchangeBest    = 'C';
        static char const Implied         = 'K';
    } // namespace QuoteCondition

    namespace MDEntryType
    {
        /// 0
        static char const Bid                       = '0';
        /// 1
        static char const Ask                       = '1';
        /// 2
        static char const Trade                     = '2';
        /// 4
        static char const OpeningPrice              = '4';
        /// 5
        static char const ClosingPrice              = '5';
        /// 6
        static char const SettlementPrice           = '6';
        /// 7
        static char const TradingSessionHighPrice   = '7';
        /// 8
        static char const TradingSessionLowPrice    = '8';
        /// B
        static char const TotalTradeVolume          = 'B';
        /// C
        static char const OpenInterest              = 'C';
        /// E
        static char const SimulatedSell             = 'E';
        /// F
        static char const SimulatedBuy              = 'F';
        /// M
        static char const Prior                     = 'M';
        /// N
        static char const SessionHighBid            = 'N';
        /// O
        static char const SessionLowOffer           = 'O';
    } // namespace MDEntryType {
}// namespace Cqg {

namespace
{
    inline bool isBestPrice( Engine::AsciiString value )
    {
        return NULL != memchr( value.data(), Cqg::QuoteCondition::ExchangeBest, value.size() );
    }

    inline bool isImplied( Engine::AsciiString value )
    {
        return NULL != memchr( value.data(), Cqg::QuoteCondition::Implied, value.size() );
    }

    inline bool isBestPrice( Engine::TagValue const& entry )
    {
        return !entry.isEmpty( Cqg::FIXFields::QuoteCondition )
               && isBestPrice( entry.getAsString( Cqg::FIXFields::QuoteCondition ) );
    }

    inline bool isImplied( Engine::TagValue const& entry )
    {
        return !entry.isEmpty( Cqg::FIXFields::QuoteCondition )
               && isImplied( entry.getAsString( Cqg::FIXFields::QuoteCondition ) );
    }

    int updateSide( MarketDataStorage::Book::Side* side, size_t bookSize, Engine::TagValue const& entry, bool isIncrement = true )
    {
        ( void )bookSize;

        unsigned int level = entry.getAsUInt( FIXFields::MDPriceLevel );
        assert( level <= MarketDataStorage::Book::Side::MAX_LEVEL_COUNT && level > 0 );

        if( entry.isEmpty( FIXFields::MDEntryPx ) ) {
            side->update( 0.0, 0.0, level - 1, isIncrement );
        } else {
            double price = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
            double size = 0.0;

            if( !entry.isEmpty( FIXFields::MDEntrySize ) ) {
                size = entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble();
            }

            side->update( price, size, level - 1, isIncrement );
        }

        return level;
    }

    void insertPrice( MarketDataStorage::Book::Side* side, size_t bookSize, Engine::TagValue const& entry )
    {
        ( void )bookSize;

        unsigned int level = entry.getAsUInt( FIXField::MDPriceLevel );
        assert( level <= MarketDataStorage::Book::Side::MAX_LEVEL_COUNT && level > 0 );


        side->insert(
            entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble(),
            entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble(),
            level - 1 );
    }

    void overlayPrice( MarketDataStorage::Book::Side* side, size_t bookSize, Engine::TagValue const& entry )
    {
        updateSide( side, bookSize, entry );
    }

    void changePrice( MarketDataStorage::Book::Side* side, size_t bookSize, Engine::TagValue const& entry )
    {
        ( void )bookSize;

        if( isBestPrice( entry ) ) {
            return;
        }

        unsigned int level = entry.getAsUInt( FIXFields::MDPriceLevel );
        assert( level <= MarketDataStorage::Book::Side::MAX_LEVEL_COUNT && level > 0 );
        assert( level <= side->bookSize_ );

        side->update( entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble(),
                      entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble(),
                      level - 1 );
    }

    void deletePrice( MarketDataStorage::Book::Side* side, size_t bookSize, Engine::TagValue const& entry )
    {
        ( void ) bookSize;
        unsigned int level = entry.getAsUInt( FIXField::MDPriceLevel );
        assert( level <= MarketDataStorage::Book::Side::MAX_LEVEL_COUNT && level > 0 );
        side->remove( level - 1 );
    }

    void setTrade( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.trade_.size_ = entry.isEmpty( FIXFields::MDEntrySize ) ? 0.0 : entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble();
        data->tradeInformation_.trade_.price_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();


        data->tradeInformation_.trade_.maxPrice_ = std::max( data->tradeInformation_.trade_.price_, data->tradeInformation_.trade_.maxPrice_ );
        if( 0.0 == data->tradeInformation_.trade_.minPrice_ ) {
            data->tradeInformation_.trade_.minPrice_ = data->tradeInformation_.trade_.price_;
        } else {
            data->tradeInformation_.trade_.minPrice_ = std::min( data->tradeInformation_.trade_.minPrice_, data->tradeInformation_.trade_.price_ );
        }

        ++data->tradeInformation_.trade_.count_;
    }

    void setHigh( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.tradingSessionHighPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
    }

    void setLow( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.tradingSessionLowPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
    }

    void setOpeningPrice( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.openingPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
    }

    void setClosingPrice( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        switch( entry.getAsInt( Cqg::FIXFields::OpenCloseSettleFlag ) ) {
        case Cqg::OpenCloseSettleFlag::Prev:
            data->tradeInformation_.prevClosingPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
            break;
        case Cqg::OpenCloseSettleFlag::Theoretical:
            data->tradeInformation_.theoreticalClosingPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
            break;
        }
    }

    void setSettlement( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.settlementPrice_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
    }

    void setPrior( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.prior_ = entry.getAsDecimal( FIXFields::MDEntryPx ).toDouble();
    }

    void setTotalTradeVolume( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.totalTradeVolume_ = entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble();
        ++data->tradeInformation_.totalTradeVolumeCount_;
    }

    void setOpenInterest( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        data->tradeInformation_.openInterest_ = entry.getAsDecimal( FIXFields::MDEntrySize ).toDouble();
    }

    void applyIncrement( MarketDataStorage::Book::Side* side, size_t size, Engine::TagValue const& entry )
    {
        switch( entry.getAsChar( FIXFields::MDUpdateAction ) ) {
        case Cqg::MDUpdateAction::New:
            insertPrice( side, size, entry );
            break;

        case Cqg::MDUpdateAction::Overlay:
            overlayPrice( side, size, entry );
            break;

        case Cqg::MDUpdateAction::Change:
            changePrice( side, size, entry );
            break;

        case Cqg::MDUpdateAction::Delete:
            deletePrice( side, size, entry );
            break;

        default:
            assert( false && "FixMessageInterpreter::applyIncrement: unsupported MDUpdateAction" );
        }
    }

    void applyIncrement( MarketDataStorage* data, Engine::TagValue const& entry )
    {
        assert( NULL != data );

        switch( entry.getAsChar( FIXFields::MDEntryType ) ) {
        case Cqg::MDEntryType::Bid: {
            MarketDataStorage::Book* book = &( isImplied( entry ) ? data->impliedBook_ : data->book_ );
            book->isAskAction_ = false;
            applyIncrement( &book->bid_, book->maxSize_, entry );
        }
        break;
        case Cqg::MDEntryType::Ask: {
            MarketDataStorage::Book* book = &( isImplied( entry ) ? data->impliedBook_ : data->book_ );
            book->isAskAction_ = true;
            applyIncrement( &book->ask_, book->maxSize_, entry );
        }
        break;

        case Cqg::MDEntryType::Trade:
            setTrade( data, entry );
            break;

        case Cqg::MDEntryType::TradingSessionHighPrice:
            setHigh( data, entry );
            break;

        case Cqg::MDEntryType::TradingSessionLowPrice:
            setLow( data, entry );
            break;

        case Cqg::MDEntryType::OpeningPrice:
            setOpeningPrice( data, entry );
            break;

        case Cqg::MDEntryType::ClosingPrice:
            setClosingPrice( data, entry );
            break;

        case Cqg::MDEntryType::SettlementPrice:
            setSettlement( data, entry );
            break;

        case Cqg::MDEntryType::Prior:
            setPrior( data, entry );
            break;

        case Cqg::MDEntryType::TotalTradeVolume:
            setTotalTradeVolume( data, entry );
            break;

        case Cqg::MDEntryType::OpenInterest:
            setOpenInterest( data, entry );
            break;

        default:
            //Ignore other entries
            break;
        }
    }
} // namespace {

void fireOnL2MessageEvent( const MarketDataStorage* book )
{
    std::cout << "Symbol = " << book->symbol_ << std::endl;
    std::cout << "SecurityDescription = " << book->SecurityDescription_ << std::endl;
}

void setPriceSize( int idx, double price, double size )
{
    switch ( idx ) {
    case 0:
    case 1:
    case 2:
    case 3:
    case 4:
    case 5:
    case 6:
    case 7:
    case 8:
    case 9:
        std::cout << "[" << idx << "]\t" << price << ":" << size << std::endl;
        break;
    default:
        assert( false && "CqgFeedProvider::setPriceSize: invalid index (only 0-9)" );
    }
}

void FixMessageInterpreter::applySecurityDef( Engine::FIXMessage const& msg, SecurityDef* data )
{
    assert( "d" == msg.type() );
    assert( NULL != data );

    //if( !msg.isEmpty( FIXFields::DisplayFactor ) )
    //    data->displayFactor_ = msg.getAsDecimal( FIXFields::DisplayFactor ).toDouble();

    data->symbol_ = msg.getAsString( FIXFields::Symbol ).toStdString();
    data->securityID_ = msg.getAsUInt64( FIXFields::SecurityID );

    const Engine::FIXGroup* group = msg.getGroup( Cqg::FIXFields::NoMdFeedTypes );

    if( NULL != group ) {
        for( int i = 0; i < group->size(); ++i ) {
            Engine::TagValue const* entry = group->getEntry( i );

            assert( NULL != entry );
            assert( !entry->isEmpty( Cqg::FIXFields::MDFeedType ) );
            assert( !entry->isEmpty( Cqg::FIXFields::MarketDepth ) );

            int depth = -1;

            switch( entry->getAsInt( Cqg::FIXFields::MarketDepth ) ) {
            case 0:
                depth = MarketDataStorage::Book::Side::MAX_LEVEL_COUNT;
                break;
            case 1:
                depth = 1;
                break;
            default:
                assert( "Unsupported MarketDepth value" );
            };

            if( Engine::AsciiString::fromStr( "CQGC" ) == entry->getAsString( Cqg::FIXFields::MDFeedType ) ) {
                data->compositeDepth_ = depth;
            } else if( Engine::AsciiString::fromStr( "CQGI" ) == entry->getAsString( Cqg::FIXFields::MDFeedType ) ) {
                data->impliedDepth_ = depth;
            }
        }
    }
}

void FixMessageInterpreter::applySnapshot( Engine::FIXMessage const& msg, MarketDataStorage* data )
{
    assert( "W" == msg.type() );
    assert( NULL != data );

    data->reset();

    if( msg.isEmpty( FIXFields::NoMDEntries ) ) {
        return;
    }

    Engine::FIXGroup const& grp = msg.getAsGroup( FIXFields::NoMDEntries );
    int const entries_count = grp.size();

    for( int i = 0; i < entries_count; ++i ) {
        Engine::TagValue const& entry( *grp.getEntry( i ) );


        switch( entry.getAsChar( FIXFields::MDEntryType ) ) {
        case Cqg::MDEntryType::Bid: {
            MarketDataStorage::Book* book = &( isImplied( entry ) ? data->impliedBook_ : data->book_ );
            updateSide( &book->bid_, book->maxSize_, entry, false );
        }
        break;

        case Cqg::MDEntryType::Ask: {
            MarketDataStorage::Book* book = &( isImplied( entry ) ? data->impliedBook_ : data->book_ );
            updateSide( &book->ask_, book->maxSize_, entry, false );
        }
        break;

        case Cqg::MDEntryType::Trade:
            setTrade( data, entry );
            break;

        case Cqg::MDEntryType::TradingSessionHighPrice:
            setHigh( data, entry );
            break;

        case Cqg::MDEntryType::TradingSessionLowPrice:
            setLow( data, entry );
            break;

        default:
            // Ignore other entries
            break;
        }
    }

}

void FixMessageInterpreter::applyIncrements( Engine::FIXMessage const* const* msgs, size_t msgsCount, MarketDataStorage* data )
{
    for( size_t i = 0; i < msgsCount; ++i ) {
        Engine::FIXGroup const* mdEntriesGrp = msgs[ i ]->getGroup( FIXFields::NoMDEntries );

        assert( 0 != mdEntriesGrp->size() );
        data->book_.reserveActions( static_cast<size_t>( mdEntriesGrp->size() ) );
        for( int j = 0, e = mdEntriesGrp->size(); j < e; ++j ) {
            applyIncrement( data, *mdEntriesGrp->getEntry( j ) );
            // bool implied = isImplied( *entries[i] );
        }
        fireOnL2MessageEvent( data );
    }
}
