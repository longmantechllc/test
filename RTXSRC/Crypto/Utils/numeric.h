#pragma once

#include <charconv>
//#include <date/date.h>
#include <optional>

namespace Crypto::numeric {

    static constexpr uint64_t pow10[20] = {
    1U,
    10U,
    100U,
    1000U,
    10000U,
    100000U,
    1000000U,
    10000000U,
    100000000U,
    1000000000U,
    10000000000U,
    100000000000U,
    1000000000000U,
    10000000000000U,
    100000000000000U,
    1000000000000000U,
    10000000000000000U,
    100000000000000000U,
    1000000000000000000U,
    10000000000000000000U
    };

    static constexpr double neg_pow10[10] = {
    1,
    0.1,
    0.01,
    0.001,
    0.0001,
    0.00001,
    0.000001,
    0.0000001,
    0.00000001,
    0.000000001
    };

    static int64_t conv_double_to_int(double input, uint8_t scale) {
        return int64_t(input * pow10[scale] + 0.01);
    }

    static int64_t conv_string_to_int(std::string_view input, uint8_t scale) {
        char* p_end;
        double out = std::strtod(input.data(), &p_end);
        out *= double(pow10[scale]);
        out += 0.01;
        return int64_t(out);
    }

    static double conv_int_to_double(int64_t input, uint8_t scale) {
        return input / double(pow10[scale]);
    }

    static std::optional<int32_t> cov_sv_to_int32(const std::string_view & input)
    {
        try {
            char* p_end;
            int64_t out = std::strtol(input.data(), &p_end, 10);
            return out;
        } catch(const std::exception &e) {
            return std::nullopt;
        }
    }

    static std::optional<int64_t> cov_sv_to_int64(const std::string_view & input)
    {
        try {
            char* p_end;
            int64_t out = std::strtoll(input.data(), &p_end, 10);
            return out;
        } catch(const std::exception &e) {
            return std::nullopt;
        }
    }

    static std::optional<uint64_t> cov_sv_to_uint64(const std::string_view & input)
    {
        try {
            char* p_end;
            uint64_t out = std::strtoull(input.data(), &p_end, 10);
            return out;
        } catch(const std::exception &e) {
            return std::nullopt;
        }
    }

    static std::optional<double> cov_sv_to_double(const std::string_view & input)
    {
        try {
            char* p_end;
            double out = std::strtod(input.data(), &p_end);
            return out;
        } catch(const std::exception &e) {
            return std::nullopt;
        }
    }

    static int64_t cov_double_sv_to_int64(
        const std::string_view & input, uint8_t scale)
    {
        try {
            char* p_end;
            double out = std::strtod(input.data(), &p_end);
            return conv_double_to_int(out, scale);
        } catch(const std::exception &e) {
            return 0;
        }
    }

/*
static std::chrono::seconds get_utc_timestamp_seconds() {
    return std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::system_clock::now().time_since_epoch());
}

static std::chrono::microseconds get_utc_timestamp_microseconds() {
    return std::chrono::duration_cast<std::chrono::microseconds>(
        std::chrono::system_clock::now().time_since_epoch()); // .count();
}

static std::chrono::milliseconds get_utc_timestamp_milliseconds() {
    return std::chrono::duration_cast<std::chrono::milliseconds>(
        std::chrono::system_clock::now().time_since_epoch());
}

static std::chrono::nanoseconds get_utc_timestamp_nanoseconds() {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
        std::chrono::system_clock::now().time_since_epoch());
}

uint64_t tp_to_ts_in_nanoseconds(const tp_t& tp) {
    return std::chrono::duration_cast<std::chrono::nanoseconds>(
        tp.time_since_epoch()).count();
}

using tp_t = std::chrono::time_point<std::chrono::system_clock>;
uint64_t tp_to_ts_in_nanoseconds(const tp_t& tp);

    // conv_timestamp_to_sequence_number uses digits from timestamp input
    // and converts to uint64_t. It takes the 19 least sig digits (rightmost) of the timestmap
    // since they should be more significant as a sequence number
    static uint64_t conv_timestamp_to_sequence_number(std::string_view ts) {
        // The sequence number is actually the timestamp stripped of all non-numeric characters
        // e.g. "2019-04-16T10:17:28.507Z" -> "20190416101728507"
        size_t count = 0;
        constexpr uint8_t max_digits = 20;
        uint64_t res = 0;
        auto it = ts.end() - 1;
        while (it != ts.begin()) {
            if (count == max_digits) {
                break;
            }
            if (isdigit(*it)) {
                res += (*it - '0') * pow10[count];
                count++;
            }
            --it;
        }
        return res;
    }

    using tp_t = std::chrono::time_point<std::chrono::system_clock>;

    static uint64_t conv_timestamp_to_nanoseconds(
            std::string_view ts, const std::string& format = "%Y-%m-%dT%T%Ez") {
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        tp_t tp;
        in >> date::parse(format.data(), tp);

        if (in.fail() || in.bad()) {
            auto t = std::chrono::duration_cast<std::chrono::nanoseconds>(
                tp.time_since_epoch()).count();
            return {};
        }
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
            tp.time_since_epoch()).count();
    }

    static uint64_t conv_timestamp_to_milliseconds(
            std::string_view ts, const std::string& format = "%Y-%m-%dT%TZ") {
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        tp_t tp;
        in >> date::parse(format.data(), tp);

        if (in.fail() || in.bad()) {
            auto t = std::chrono::duration_cast<std::chrono::milliseconds>(
                tp.time_since_epoch()).count();
            return {};
        }
        return std::chrono::duration_cast<std::chrono::milliseconds>(
            tp.time_since_epoch()).count();
    }

    static uint64_t conv_timestamp_str_to_microseconds(
            std::string_view ts, const std::string& format = "%Y-%m-%d %T") {
        tp_t tp;
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        in >> date::parse(format.data(), tp);

        if (in.fail() || in.bad()) {
            auto t = std::chrono::duration_cast<std::chrono::microseconds>(
                tp.time_since_epoch()).count();
            return {};
        }
        return std::chrono::duration_cast<std::chrono::microseconds>(
            tp.time_since_epoch()).count();
    }

    template<typename TS>
    static tp_t conv_datetime_str_to_time_point(
            TS ts, const std::string& format = "%Y-%m-%d %T") {
        tp_t tp;
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        in >> date::parse(format.data(), tp);
        return tp;
    }

    //"time": "2014-11-07T08:19:27.028459Z",
    static uint64_t conv_iso8601_to_nanoseconds(
            std::string_view ts, const std::string& format = "%Y-%m-%dT%TZ") {
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        tp_t tp;
        in >> date::parse(format.data(), tp);

        if (in.fail() || in.bad()) {
            //auto t = std::chrono::duration_cast<std::chrono::nanoseconds>(
            //    tp.time_since_epoch()).count();
            return {};
        }
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
            tp.time_since_epoch()).count();
    }

    //"time": "20211007-18:46:40.611",
    static uint64_t conv_FIX_timestamp_to_nanoseconds(
            std::string_view ts, const std::string& format = "%Y%m%d-%T") {
        std::istringstream in{std::string(ts)};
        // in.rdbuf()->pubsetbuf(const_cast<char *>(ts.data()), ts.size());
        tp_t tp;
        in >> date::parse(format.data(), tp);

        if (in.fail() || in.bad()) {
            //auto t = std::chrono::duration_cast<std::chrono::nanoseconds>(
            //    tp.time_since_epoch()).count();
            return {};
        }
        return std::chrono::duration_cast<std::chrono::nanoseconds>(
            tp.time_since_epoch()).count();
    }

    static int decimal_presicion( double d )
    {
        constexpr double precision{ 1e-7 };
        double temp{0.0}; 
        int count{0};
        do {
            d *= 10;
            temp = d - int(d);
            count++;
        } while( temp > precision && count < std::numeric_limits<double>::digits10 );
        
        return count;
    }

    static int decimal_presicion_v2( double d )
    {
        constexpr double precision{ 1e-10 };
        int count{0};
        d -= int(d);
        while( d > precision && count < std::numeric_limits<double>::digits10 ) {
            d *= 10;
            d -= int(d);
            count++;
        };
        
        return count;
    }
*/
}  // namespace Crypto::numeric