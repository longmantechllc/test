// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Application.h
/// Contains Engine::Application class declaration.

#ifndef __B2BITS_Application_h__
#define __B2BITS_Application_h__

#include <cstddef>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_CompilerDefines.h>

namespace Engine
{
    class Session;
    class FIXMessage;
    class LogonEvent;
    class LogoutEvent;
    class UnexpectedMessageEvent;
    class SequenceGapEvent;
    class SessionLevelRejectEvent;
    class MsgRejectEvent;
    class HeartbeatWithTestReqIDEvent;
    class ResendRequestEvent;
    class NewStateEvent;
    class SwitchConnectionEvent;
    class UnableToRouteMessageEvent;
    class InMsgSeqNumTooLowEvent;

    /**
    * Generic application interface. Processes the incoming messages.
    * All user's applications must be inherited from this class.
    */
    class V12_API Application
    {
    public:
        /**
        * Destructor
        */
        virtual ~Application() {}

        /**
        * A call-back method to process incoming business level messages.
        * If the application can not process the given message, the FIX Engine will try to deliver it later
        * according to "Delayed Processing Algorithm".
        *
        * Callback is called out of the scope of the session lock.
        *
        * @note To receive Session Level Reject message please handle 
        * Application::onSessionLevelRejectEvent event.
        *
        * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
        *
        * @param msg the incoming message.
        * @param sn The corresponding FIX session.
        *
        * @return true if the application can process the given message, otherwise false.
        */
        virtual bool process( const FIXMessage& msg, const Session& sn ) = 0;

        /**
        * This call-back method is called to notify that the Logon message has been received from the counterpart.
        * Callback is called in the scope of the session lock.
        *
        * If std::exception is raised inside this event, incoming connection will be closed.
        * Exception text will be stored to the engine.log file.
        *
        * @param event Event information.
        * @param sn The corresponding FIX session.
        */
        virtual void onLogonEvent( const LogonEvent* event, const Session& sn ) = 0;

        /**
        * This call-back method is called to notify that the Logout message has been received from the counterpart
        * or the session was disconnected. In case of session disconnect, LogoutEvent::m_pLogoutMsg is null.
        *
        * Callback is called in the scope of the session lock.
        *
        * @note It is prohibited to call Session::connect inside this callback. To restart 
        * the session user should create new thread and call Session::connect there. If session is 
        * acceptor, to restart session event->LogoutEvent::reconnectFlag_ = true may be set.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onLogoutEvent( const LogoutEvent* event, const Session& sn ) = 0;

        /**
        * This call-back method is called to notify about received unexpected message. For example - when first message
        * in session is not a Logon.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onUnexpectedMessageEvent( const UnexpectedMessageEvent* event, const Session& aSn ) {
            B2B_USE_ARG( event );
            B2B_USE_ARG( aSn );
        }

        /**
        * This call-back method is called when a message gap is detected in incoming messages.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onSequenceGapEvent( const SequenceGapEvent* event, const Session& sn ) = 0;

        /**
        * This call-back method is called when a session-level Reject message (MsgType = 3) is received.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onSessionLevelRejectEvent( const SessionLevelRejectEvent* event, const Session& sn ) = 0;

        /** This call-back method is called when a message have to be rejected.
        * @param event - object that encapsulates rejected message
        * @param sn - session object that rejects message
        */
        virtual void onMsgRejectEvent( const MsgRejectEvent* event, const Session& sn ) = 0;

        /**
        * This call-back method is called when a Heartbeat message (MsgType = 0)
        * with the TestReqID field (tag = 112) has been received.
        *
        * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onHeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& event, const Session& sn ) = 0;

        /**
        * This call-back method is called when a Resend Request (MsgType = 2) has been received.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onResendRequestEvent( const ResendRequestEvent& event, const Session& sn ) = 0;

        /**
        * This call-back method is called when a ill-formed Resend Request (MsgType = 2) has been received.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onInvalidResendRequestEvent( const ResendRequestEvent& event, const Session& sn ) {
            B2B_USE_ARG( event );
            B2B_USE_ARG( sn );
        }

        /**
        * This call-back method is called when #DuplicateResendRequestLimit ResendRequest messages are
        * received.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        * @return To reset #DuplicateResendRequestLimit counter return true; false otherwise.
        */
        virtual bool onResendRequestLoop( const ResendRequestEvent& event, const Session& sn ) {
            B2B_USE_ARG( event );
            B2B_USE_ARG( sn );
            return true;
        }

        /**
        * This call-back method is called each time session tries to reconnect
        * Callback is called in the scope of the session lock.
        *
        * @param remainTries The remain reconnect tries count.
        * @param sn The corresponding FIX session.
        */
        virtual void onReconnectTryEvent( int remainTries, const Session& sn ) {
            B2B_USE_ARG( remainTries );
            B2B_USE_ARG( sn );
        }

        /**
        * This call-back method is called when the session has changed its state.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onNewStateEvent( const NewStateEvent& event, const Session& sn ) = 0;

        /**
        * This call-back method is called when the session is switching to another connection.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onSwitchConnectionEvent( const SwitchConnectionEvent& event, const Session& sn ) {
            B2B_USE_ARG( event );
            B2B_USE_ARG( sn );
        }

        /**
        * This call-back method is called when the message has to be routed to the session, which does not exist.
        * Callback is called in the scope of the session lock.
        *
        * @param event Information about occurred event.
        * @param sn The corresponding FIX session.
        */
        virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& sn ) = 0;

        /**
        * This call-back method is called when an outgoing message is about to be resent
        * as a reply to the incoming ResendRequest message.
        *
        * If the method returns 'true' then the Engine resends the message to counterpart,
        * otherwise it sends the SequenceReset Gap Fill message.
        * Callback is called in the scope of the session lock.
        *
        * @param msg Outgoing message.
        * @param sn FIX session.
        * @return true if the message should be resent, otherwise false.
        */
        virtual bool onResend( const FIXMessage& msg, const Session& sn ) = 0;

        /**
        * Fired before message is sent to socket. Designed specifically for benchmarks.
        * Callback is called in the scope of the session lock.
        *
        * @param[in] msgSeqNum Sequence number of the sent message.
        * @param[in] sn Reference to FIX session related to the event.
        * @param[in] msgCount Messages count to send
        */
        virtual void onBeforeMessageIsSent( int msgSeqNum, const Session& sn, std::size_t msgCount ) {
            B2B_USE_ARG( sn );
            B2B_USE_ARG( msgSeqNum );
            B2B_USE_ARG( msgCount );
        }

        /**
        * Fired when message is prepared for sending to socket.
        * Callback is called in the scope of the session lock.
        *
        * @note Available if ENABLE_FAILOVER_CLUSTER_API is on.
        * @param[in] message Prepared message.
        * @param[in] sn Reference to FIX session related to the event.
        */
        virtual void onMessagePreparedForSending( const FIXMessage& msg, const Session& sn ) {
            B2B_USE_ARG( msg );
            B2B_USE_ARG( sn );
        }

        /**
        * Fired just after message is received from socket.
        * Callback is called in the scope of the session lock.
        *
        * @param[in] sn Reference to FIX session related to the event.
        * @param[in] msg Pointer to the buffer with message.
        * @param[in] msgSize Size of the message buffer.
        */
        virtual void onAfterMessageIsReceived( char const* msg, std::size_t msgSize, const Session& sn ) {
            B2B_USE_ARG( sn );
            B2B_USE_ARG( msg );
            B2B_USE_ARG( msgSize );
        }

        /**
        * Fired when Session Level Reject message is received with the sequence number too high.
        * Callback is called in the scope of the session lock.
        *
        * @note It can occur when remote site sends reject on Resend Request. In this case user can reset
        * incoming/outgoing sequences and restart the session.
        *
        * @return true if message is processed; false [default] to send resend request message.
        */
        virtual bool onSessionLevelRejectWithSeqNumTooHigh( const FIXMessage& msg, const Session& sn ) {
            B2B_USE_ARG( msg );
            B2B_USE_ARG( sn );
            return false;
        }

        /**
         * Fired when Session is terminated and all internal threads are stopped.
         * @param[in] sn Reference to FIX session related to the event.
         */
        virtual void onTerminated( const Session& sn ) {
            B2B_USE_ARG( sn );
        }

        /**
         * Fired when async connect operation has completed.
         * Callback is called in the scope of the session lock.
         * @param[in] error Error code of the connect operation, 0 means no error
         * @param[in] sn Reference to FIX session related to the event.
         */
        virtual void onAsyncConnectCompleted( int error, const Session& sn ) {
            B2B_USE_ARG( error );
            B2B_USE_ARG( sn );
        }

        /**
         * Faired on incoming message with sequence number too low without PossDupFlag set.
         * If message is processed, InMsgSeqNumTooLowEvent::processed_ should be set to true; otherwise session will be stopped.
         */
        virtual void onInMsgSeqNumTooLowEvent( const InMsgSeqNumTooLowEvent *event, const Session& sn ) {
            B2B_USE_ARG( event );
            B2B_USE_ARG( sn );
        }


		 /**
         * Fired after incoming message is parsed and stored in storage. Before any message validation.
         * Callback is called in the scope of the session lock.
		 * @param msg the incoming message.
         * @param sn Reference to FIX session related to the event.

		 * @note This callback is fired for FIX sessions only, not for FAST.

         * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
		 		 
		 * @warning This callback allows to modify or read an incoming FIX Message before automated validation. Be careful with modifying tags from standard header and trailer.
		 */
		virtual void onAfterMessageIsParsed(FIXMessage& msg, const Session& sn){
			 B2B_USE_ARG( msg );
             B2B_USE_ARG( sn );
		}


		 /**
         * Fired after outgoing message is validated and before it is stored in the storage and sent to socket. 
         * Callback is called in the scope of the session lock.
		 * @param msg the incoming message.
         * @param sn Reference to FIX session related to the event.

		 * @note This callback is fired for FIX sessions only, not for FAST.

         		 		 
		 * @warning This callback allows to modify or read an outgoing FIX Message after automated validation. Be careful with modifying tags from standard header and trailer.
		 */
		virtual void onBeforeMessageIsSerialized(FIXMessage& msg, const Session& sn){
			 B2B_USE_ARG( msg );
             B2B_USE_ARG( sn );
		}



		/**
		* Fired just after the incoming seqnum is changed
		* @param oldValue old incoming sequence number
		* @param newValue new incoming sequence number
		* @param sn Reference to FIX session related to the event.
		
		* @note This callback is fired for FIX sessions only, not for FAST.
		*/

		virtual void onInSeqNumChanged(int oldValue, int newValue, const Session& sn){
			B2B_USE_ARG( oldValue );
			B2B_USE_ARG( newValue );
			B2B_USE_ARG( sn );
		}


    }; // class Application
} // namespace Engine

#endif // __B2BITS_Application_h__


