/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "MyApp.h"

#include <iostream>

#include <B2BITS_FixEngine.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_FIXMsgProcessor.h>
#include <B2BITS_SessionEvent.h>
#include <B2BITS_FAProperties.h>

#include "Helper.h"

using namespace ::std;
using namespace ::Engine;
using namespace ::System;
using namespace ::Utils;

MyApp::MyApp( ApplicationProperties::SessionParameters const& params)
    : params_( params )
    , resendInProgress_( false )
    , resendCommand_( false )
    , sessionConfiguredWithEngineProperties_( false )
{
    // Advanced session initialization parameters.
    SessionExtraParameters extraParams;
    if( ! params_.senderSubId_.empty() ) {
        extraParams.pSenderSubID_ = params_.senderSubId_;
    }

    if( ! params_.targetSubId_.empty() ) {
        extraParams.pTargetSubID_ = params_.targetSubId_;
    }

    if( ! params_.senderLocationId_.empty() ) {
        extraParams.pSenderLocationID_ = params_.senderLocationId_;
    }

    if( ! params_.targetLocationId_.empty() ) {
        extraParams.pTargetLocationID_ = params_.targetLocationId_;
    }
    if( ! params_.user_.empty() ) {
        extraParams.userName_ = params_.user_;
    }
    if( ! params_.password_.empty() ) {
        extraParams.password_ = params_.password_;
    }

	extraParams.hiddenLogonCredentials_ = params_.hiddenLogonCredentials_;

    extraParams.intradayLogoutToleranceMode_ = params_.intradayLogoutTolerance_;
    extraParams.forceSeqNumReset_ = params_.forceSeqNumReset_;
    extraParams.ignoreSeqNumTooLowAtLogon_ = params_.ignoreSeqNumTooLowAtLogon_;
    extraParams.enableMessageRejecting_ = params_.rejectMessageWhileNoConnection_;
    extraParams.forcedReconnect_ = params_.forceReconnect_;
    extraParams.keepConnectionState_ = params_.keepState_;
    extraParams.enableAutoSwitchToBackupConnection_ = params_.autoSwitchConnection_;
    extraParams.cyclicSwitchBackupConnection_ = params_.cyclicSwitchConnection_;


    extraParams.disableTCPBuffer_ = params_.disableTCPBuffer_;
    if ( params_.aggressiveSend_ ) {
        extraParams.socketPriority_ = AGGRESSIVE_SEND_AND_RECEIVE_SOCKET_OP_PRIORITY;
    } else {
        extraParams.socketPriority_ = EVEN_SOCKET_OP_PRIORITY;
    }

    extraParams.maxMessagesAmountInBunch_ = params_.maxMessageInBunch_;
    extraParams.storageType_  = Engine::persistentMM_storageType;

    FAProperties::ConfiguredSessionsMap sessions = FixEngine::singleton()->getProperties()->getConfiguredSessions(INITIATOR_SESSION_ROLE);
    if (!sessions.empty())
    {
        FAProperties::ConfiguredSessionsMap::const_iterator sessionIt = sessions.begin();
        session_.reset(
            FixEngine::singleton()->createSession(
                this,
                sessionIt->first,
                NA,
                &sessionIt->second),
            false );
        sessionConfiguredWithEngineProperties_ = true;
    }
    else
    {
        // Creates session with appropriate parameters.
        session_.reset(
            FixEngine::singleton()->createSession(
                this,
                params_.senderCompId_,
                params_.targetCompId_,
                params_.fixVersion_,
                &extraParams,
                Engine::default_storageType,
                params_.isFIXTSsnProtocol_ ? FIXT11_TCP : FIX_TCP ),
            false );
    }

    if( -1 != params_.inSeqNumber_ ) {
        session_->setInSeqNum( params_.inSeqNumber_ );
    }

    if( -1 != params_.outSeqNumber_ ) {
        session_->setOutSeqNum( params_.outSeqNumber_ );
    }
}

MyApp::~MyApp()
{
    if( resendInProgress_ ) {
        resendCommand_ = false;
        resendSemaphore_.post();
    }

    session_->registerApplication( NULL );
}

void MyApp::switchConnection()
{
    session_->switchConnection();
}

void MyApp::disconnect()
{
    // Closes session with the given logout text.
    session_->disconnect( "The session was closed by ConnectToGateway", true );
}

void MyApp::connect()
{
    SessionBackupParameters backupParam;
    bool backupDefined = !params_.backupHost_.empty();
    if( !params_.backupHost_.empty() ) {
        backupParam.host_ = params_.backupHost_;
    }
    backupParam.port_ = params_.backupPort_;
    backupParam.hbi_ = params_.backupHBI_;
    SessionExtraParameters backupExtraParams;
    if( !params_.backupSenderSubId_.empty() ) {
        backupExtraParams.pSenderSubID_ = params_.backupSenderSubId_;
    }
    if( !params_.backupTargetSubId_.empty() ) {
        backupExtraParams.pTargetSubID_ = params_.backupTargetSubId_;
    }
    if( !params_.backupSenderLocId_.empty() ) {
        backupExtraParams.pSenderLocationID_ = params_.backupSenderLocId_;
    }
    if( !params_.backupTargetLocId_.empty() ) {
        backupExtraParams.pTargetLocationID_ = params_.backupTargetLocId_;
    }
    backupExtraParams.intradayLogoutToleranceMode_ = params_.backUpIntradayLogoutTolerance_;
    backupExtraParams.forcedReconnect_ = params_.backupForceReconn_;
    backupExtraParams.ignoreSeqNumTooLowAtLogon_ = params_.backupIgnoreSeqNumTooLow_;
    backupExtraParams.enableMessageRejecting_ = params_.backupRejectNoConn_;
    backupExtraParams.enableAutoSwitchToBackupConnection_ = params_.backupAutoSwitchConnection_;
    backupExtraParams.cyclicSwitchBackupConnection_ = params_.backupCyclicSwitchConnection_;
    backupExtraParams.keepConnectionState_ = params_.keepState_;
    backupParam.params_ = backupExtraParams;
    std::auto_ptr<Engine::FIXMessage> customLogon;

    if( !params_.customLogon_.empty() ) {
        customLogon.reset( Engine::FIXMsgProcessor::singleton()->parse( params_.customLogon_ ) );
    }

    if( NULL == customLogon.get() ) {
        if (sessionConfiguredWithEngineProperties_)
        {
            session_->connect();
        }
        else
        {
            // Establishes the FIX session as Initiator.
            session_->connect( params_.hbi_,
                params_.host_,
                params_.port_,
                backupDefined ? &backupParam : NULL,
                params_.activeConnection_ );
        }
    } else {
        // Establishes the FIX session as Initiator.
        session_->connect( params_.hbi_,
                           *customLogon,
                           params_.host_,
                           params_.port_,
                           backupDefined ? &backupParam : NULL,
                           params_.activeConnection_ );
    }
}

bool MyApp::onResend( const FIXMessage& msg, const Session& /*sn*/ )
{
    if( ! params_.interactiveResend_ ) {
        return true;
    } else {
        Helper::writeLineToConsole( "Do you want to resend the message '" + *msg.toString() + "' ? (y/n)" );
        resendInProgress_ = true;
        resendSemaphore_.wait();
        resendInProgress_ = false;

        if ( resendCommand_ ) {
            Helper::writeLineToConsole( "The message will be resent." );
            return true;
        } else {
            Helper::writeLineToConsole( "The Sequence Reset Gap Fill message will be sent." );
            return false;
        }
    }
}

// A call-back method to process ingoing messages.
bool MyApp::process( const FIXMessage& msg, const Engine::Session& /*aSn*/ )
{
    if( 6400 > msg.toString()->size() ) {
        Helper::writeLineToConsole( "Received: " + * ( msg.toString() ) + "\n" );
    } else {
        Helper::writeLineToConsole( "Received: Message too big to show it!!!\n" );
    }
    return true;
}

// This call-back method is called to notify about Logon event.
void MyApp::onLogonEvent( const LogonEvent* apEvent, const Session& /*aSn*/ )
{
    Helper::writeLineToConsole( "LogonEvent, the Logon message was received: " +
                                * ( apEvent->m_pLogonMsg->toString() ) + "\n" );
}

// This call-back method is called to notify about Logout event.
void MyApp::onLogoutEvent( const LogoutEvent* /*apEvent*/, const Session& /*aSn*/ )
{
    Helper::writeLineToConsole( "LogoutEvent, the session was terminated.\n" );
    semaphore_.post();
}

// Blocks the calling thread until the session is closed.
void MyApp::waitUntilClosed()
{
    semaphore_.wait();
}

void MyApp::onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& event, const Engine::Session& /*sn*/ )
{
    cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
}

bool MyApp::isResendInProgress() const
{
    return resendInProgress_;
}

void MyApp::postResendCommand( std::string const& command )
{
    resendCommand_ = "y" == command || "Y" == command;
    resendSemaphore_.post();
}
