/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "InitiatorApp.h"

#include <B2BITS_Session.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_LogSystem.h>

using namespace std;

InitiatorApp::InitiatorApp( void )
    : logger_(Utils::Log::LogSystem::singleton()->createCategory("Initiator Application"))
{
}

InitiatorApp::~InitiatorApp( void )
{
}

bool InitiatorApp::process( const Engine::FIXMessage& fixMsg, const Engine::Session& aSn )
{
    // WARNING: This method should complete as quickly as possible.
    // Do not perform time-consuming tasks inside it.

    // Just print the message to console here.
    if (logger_)
    {
        logger_->note("Message in session " + *aSn.getId());
        logger_->note(*fixMsg.toString( '|' ));
    }
    if( "8" == fixMsg.type() ) {
        if( 'F' == fixMsg.getAsChar( FIXFields::ExecType ) ) {
            tradeReceived_.post();
        }
    }

    // Return 'true' if the application can process the given message, otherwise 'false'.
    // If the application can not process the given message,
    // the FIX Engine will try to deliver it later according to "Delayed Processing Algorithm".
    return true;

}

void InitiatorApp::onLogonEvent( const Engine::LogonEvent* /*apEvent*/, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logon message has been received from the counterpart.
    if (logger_)
        logger_->note( "Logon received for the session " + *aSn.getId());
}

void InitiatorApp::onLogoutEvent( const Engine::LogoutEvent* /*apEvent*/, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logout message has been received from the counterpart or the session was disconnected.
    if (logger_)
        logger_->note("Logout received for the session " + *aSn.getId());
}

void InitiatorApp::onMsgRejectEvent( const Engine::MsgRejectEvent* /*event*/, const Engine::Session& /* sn*/ )
{
    // This call-back method is called when a message have to be rejected.
}

void InitiatorApp::onSequenceGapEvent( const Engine::SequenceGapEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a message gap is detected in incoming messages.
}

void InitiatorApp::onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a session-level Reject message (MsgType = 3) is received.
}

void InitiatorApp::onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /* sn*/ )
{
}

void InitiatorApp::onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /* sn*/ )
{
    // This call-back method is called when a Resend Request (MsgType = 2) has been received.
}

void InitiatorApp::onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /* sn*/ )
{
    // This call-back method is called when the session has changed its state.
}

void InitiatorApp::onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& /*event*/, const Engine::Session& /* sn*/ )
{
    // This call-back method is called when the message has to be routed to the session, which does not exist.
}

bool InitiatorApp::onResend( const Engine::FIXMessage& /*msg*/, const Engine::Session& /* sn*/ )
{
    // This call-back method is called when an outgoing message is about to be resent as a reply to the incoming ResendRequest message.

    // Return 'true' if the message should be resent, otherwise 'false'
    // If the method returns 'true' then the Engine resends the message to counterpart,
    // otherwise it sends the SequenceReset Gap Fill message.
    return true;
}
