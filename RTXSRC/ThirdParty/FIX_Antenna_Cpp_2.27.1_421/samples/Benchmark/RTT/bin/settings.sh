#!/bin/bash

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

export ListenPort=9001
export ListenSSLPort=9000
export ServerAdress=127.0.0.1
export ServerPort=9001

export OnloadServerProfile=latency
export OnloadClientProfile=latency
export TasksetServerCores=16-19
export TasksetClientCores=16-19
