// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Condition.h
/// Contains System::Condition class declaration.

#ifndef _B2BITS_CONDITION__H
#define _B2BITS_CONDITION__H


#include "B2BITS_V12_Defines.h"
#include "B2BITS_SystemDefines.h"

#ifdef _LINUX
#   include <ctime>
#   include <pthread.h>
#endif

/**
 * Platform specific classes.
 */
namespace System
{

    class Mutex;

    /**
     * Condition class provides communication, the ability to wait for some shared
     * resource to reach some desired state, or to signal that it has reached some
     * state in which another thread may be interested. Each condition variable is
     * closely associated with a mutex that protects the state of the resource.
     *
     * @note A conditional variable wait always returns with the mutex locked.
     *
     * @warning Conditional variables are for signaling, not for mutual exclusion.
     *
     * @note Conditional variables and thier predicates are "linked" - for best results,
     * treat them that way!
     */
    class V12_API Condition B2B_SEALED
    {
    public:
        /**
         * Constructor.
         *
         * @throw SystemException
         */
        Condition();

        /**
         * Destructor.
         */
        ~Condition();

        /**
         * Broadcasts condition, waking all current waiters.
         *
         * @note Use when more than one waiter may respond to predicate change or
         * if any waiting thread may not be able to respond.
         *
         * @throw SystemException
         */
        void broadcast();

        /**
         * Signals condition, waking one waiting thread.
         *
         * @note Use when any waiter can respond, and only one need respond. All waiters are equal.
         *
         * @throw SystemException
         */
        void signal();

        /**
         * Waits on condition, until awakened by a signal or broadcast.
         * The current thread must own specified mutex.
         *
         * @throw SystemException
         *
         * @note The function shall block on a condition variable. It shall be
         * called with mutex locked by the calling thread or undefined behavior
         * results. Upon successful return, the mutex shall have been locked and
         * shall be owned by the calling thread.
         */
        void wait( Mutex* apMutex );

        /**
         * Waits on condition, until awaked by a signal or broadcast, or until
         * a specified amount of time has elapsed.
         * If awaked by a signal or broadcast returns true, otherwise false.
         * The current thread must own specified mutex.
         *
         * @note The function shall block on a condition variable. It shall be
         * called with mutex locked by the calling thread or undefined behavior
         * results. Upon successful return, the mutex shall have been locked and
         * shall be owned by the calling thread.
         *
         * @param aTimeout the maximum time to wait in milliseconds.
         * @param apMutex System::Mutex object on which to wait.
         *
         * @throw SystemException
         */
        bool timedWait( Mutex* apMutex, u64 aTimeout );

#ifdef _WIN32
        /**
         * Waits on condition, until awaked by a signal or broadcast, or until
         * a specified event fired.
         * If awaked by a signal or broadcast returns true, otherwise false.
         * The current thread must own specified mutex.
         *
         * @note The function shall block on a condition variable. It shall be
         * called with mutex locked by the calling thread or undefined behavior
         * results. Upon successful return, the mutex shall have been locked and
         * shall be owned by the calling thread.
         *
         * @param event - event descriptor to stop waiting.
         * @param apMutex System::Mutex object on which to wait.
         *
         * @throw SystemException
         */
        bool timedWaitUntilEvent( Mutex* apMutex, HANDLE event );
#endif

    private:
        Condition( const Condition& );
        Condition& operator=( const Condition& );


#ifdef _WIN32
        typedef struct {
            unsigned int waitersCount;
            // Count of the number of waiters.

            // Serialize access to <waitersCount>
            CRITICAL_SECTION waitersCountLock;

            enum {
                SIGNAL = 0,
                BROADCAST = 1,
                MAX_EVENTS = 2
            };

            HANDLE events_[MAX_EVENTS];
            // Signal and broadcast event HANDLEs.

        } pthread_cond_t;
#endif

        pthread_cond_t m_cond;

    };

}

#endif


