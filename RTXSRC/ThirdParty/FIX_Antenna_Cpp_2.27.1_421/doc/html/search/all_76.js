var searchData=
[
  ['v1',['v1',['../classCqg_1_1MDApplicationParams.html#a3474a2748578754b3c0cd73b26f07f69ad0d35a51d58d4fcc9013331f41aa3e88',1,'Cqg::MDApplicationParams']]],
  ['v2',['v2',['../classCqg_1_1MDApplicationParams.html#a3474a2748578754b3c0cd73b26f07f69ace2ab7b3123138a05d979d65505e160e',1,'Cqg::MDApplicationParams']]],
  ['v_5fadditional_5ffields_5ffile_5fname_5fparam',['V_ADDITIONAL_FIELDS_FILE_NAME_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#aa8554f2cf472535718250624752fc86e',1,'Engine::FIXPropertiesNames']]],
  ['v_5fadditional_5ffields_5fparam',['V_ADDITIONAL_FIELDS_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#abc90380eea409f8f999631be1568b5bc',1,'Engine::FIXPropertiesNames']]],
  ['v_5fallow_5fempty_5ffield_5fvalue_5fparam',['V_ALLOW_EMPTY_FIELD_VALUE_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#ab29d6a36693ace65d3cf44a9241b68cb',1,'Engine::FIXPropertiesNames']]],
  ['v_5fallow_5fzero_5fnum_5fin_5fgroup_5fparam',['V_ALLOW_ZERO_NUM_IN_GROUP_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#a985fa7ba87c092770d9a6fb40daaa2a1',1,'Engine::FIXPropertiesNames']]],
  ['v_5fcheck_5frequired_5fgroup_5ffields_5fparam',['V_CHECK_REQUIRED_GROUP_FIELDS_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#a9566d9bbaf1daf6a6510e60b06639898',1,'Engine::FIXPropertiesNames']]],
  ['v_5fignore_5funknown_5ffields',['V_IGNORE_UNKNOWN_FIELDS',['../namespaceEngine_1_1FIXPropertiesNames.html#ac39af2b4fd2f0cf050b14d2d45871138',1,'Engine::FIXPropertiesNames']]],
  ['v_5fprohibit_5fduplicated_5ftags',['V_PROHIBIT_DUPLICATED_TAGS',['../namespaceEngine_1_1FIXPropertiesNames.html#a00cf3ed89d84fb2bc41c62f46dab412c',1,'Engine::FIXPropertiesNames']]],
  ['v_5fprohibit_5funknown_5ftags',['V_PROHIBIT_UNKNOWN_TAGS',['../namespaceEngine_1_1FIXPropertiesNames.html#a6e629d826c29cb4cb23601ac9cd41bec',1,'Engine::FIXPropertiesNames']]],
  ['v_5fvalidate_5fmessage_5fparam',['V_VALIDATE_MESSAGE_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#a72b1fd7d45148a4b1e5b72f8e77d3d54',1,'Engine::FIXPropertiesNames']]],
  ['v_5fverify_5freperating_5fgroup_5fbounds',['V_VERIFY_REPERATING_GROUP_BOUNDS',['../namespaceEngine_1_1FIXPropertiesNames.html#a3a789ac6a119996d5c08d7f8bfcda5b5',1,'Engine::FIXPropertiesNames']]],
  ['v_5fverify_5ftags_5fvalues',['V_VERIFY_TAGS_VALUES',['../namespaceEngine_1_1FIXPropertiesNames.html#a0853892d447747d9d25f237765bf2f16',1,'Engine::FIXPropertiesNames']]],
  ['valblock',['ValBlock',['../classFixDictionary2_1_1ValBlock.html',1,'FixDictionary2']]],
  ['valblock',['ValBlock',['../classFixDictionary2_1_1ValBlock.html#a90219d0d1de15c55f8a9ec1c3b767385',1,'FixDictionary2::ValBlock::ValBlock(std::string const &amp;id)'],['../classFixDictionary2_1_1ValBlock.html#add64d7f96af60af96dd714105bb16ca1',1,'FixDictionary2::ValBlock::ValBlock(ValBlock const &amp;src)']]],
  ['valblockt',['ValBlockT',['../namespaceFixDictionary2.html#ab5b701b6636c7f4ea76149303fb75044',1,'FixDictionary2']]],
  ['validatechecksum',['ValidateCheckSum',['../namespaceEngine_1_1SessionParameters.html#a7c6eac0dcde6f0b8470b264414d2735a',1,'Engine::SessionParameters']]],
  ['validatechecksum_5f',['validateCheckSum_',['../structEngine_1_1SessionExtraParameters.html#a20198ca1c9ae58d57ea627f09cc2f785',1,'Engine::SessionExtraParameters']]],
  ['validation_5f',['validation_',['../structEngine_1_1SessionExtraParameters.html#a2969c18461501f5084de603e332572df',1,'Engine::SessionExtraParameters']]],
  ['validation_5fallowzeronumingroup',['Validation_AllowZeroNumInGroup',['../namespaceEngine_1_1SessionParameters.html#ad0341d2fe7b690da6ea4396814498643',1,'Engine::SessionParameters']]],
  ['validation_5fcheckrequiredgroupfields',['Validation_CheckRequiredGroupFields',['../namespaceEngine_1_1SessionParameters.html#a99260ab97a376521bca5aa982f940bfb',1,'Engine::SessionParameters']]],
  ['validation_5fignoreunknownfields',['Validation_IgnoreUnknownFields',['../namespaceEngine_1_1SessionParameters.html#a573b7415e87091d7a18eefdc8c77501c',1,'Engine::SessionParameters']]],
  ['validation_5fisenabled',['Validation_IsEnabled',['../namespaceEngine_1_1SessionParameters.html#aac2770372ec02c0da50094c07e63799a',1,'Engine::SessionParameters']]],
  ['validation_5fprohibitduplicatedtags',['Validation_ProhibitDuplicatedTags',['../namespaceEngine_1_1SessionParameters.html#ae27fec5dccece6aa3d5fe006623feee2',1,'Engine::SessionParameters']]],
  ['validation_5fprohibittagswithoutvalue',['Validation_ProhibitTagsWithoutValue',['../namespaceEngine_1_1SessionParameters.html#a5f1ec2861b74578dcacec0c82ddb0944',1,'Engine::SessionParameters']]],
  ['validation_5fprohibitunknowntags',['Validation_ProhibitUnknownTags',['../namespaceEngine_1_1SessionParameters.html#a76f557010354e04dfd58c5a1be9bb428',1,'Engine::SessionParameters']]],
  ['validation_5fverifyreperatinggroupbounds',['Validation_VerifyReperatingGroupBounds',['../namespaceEngine_1_1SessionParameters.html#a77be4a57d29664e202ddd0581e8bba0a',1,'Engine::SessionParameters']]],
  ['validation_5fverifytagsvalues',['Validation_VerifyTagsValues',['../namespaceEngine_1_1SessionParameters.html#ae8ee4142d0750ab700c4daa028f65fc7',1,'Engine::SessionParameters']]],
  ['validationparameters',['ValidationParameters',['../structEngine_1_1SessionExtraParameters_1_1ValidationParameters.html',1,'Engine::SessionExtraParameters']]],
  ['validuntiltime',['ValidUntilTime',['../namespaceFIXFields.html#a1c78055fdd128a9e46d35ab229d953ef',1,'FIXFields']]],
  ['valuationmethod',['ValuationMethod',['../namespaceFIXFields.html#a1407bdcb226070444f00b18f1e44b178',1,'FIXFields']]],
  ['value',['value',['../classSystem_1_1HRTimer.html#a3a2d3706b0116479bb01274f1de47557',1,'System::HRTimer']]],
  ['value_5f',['value_',['../classEngine_1_1FastScp11_1_1Alert.html#a07ce220a51a6fffbcf917afdf4e09e21',1,'Engine::FastScp11::Alert::value_()'],['../structEngine_1_1FixField.html#ac338ce05a73308f237a5197341423137',1,'Engine::FixField::value_()'],['../structEngine_1_1PreparedMessage_1_1Field.html#ad6be6c5e56527f3a9ce838da99bdb5aa',1,'Engine::PreparedMessage::Field::value_()']]],
  ['value_5ftype',['value_type',['../classUtils_1_1Emptily.html#ad9f5a1f776e4b005be8f9de27b64e5d6',1,'Utils::Emptily::value_type()'],['../structEngine_1_1FIXGroup_1_1forward__iteratorT.html#a8820602188f550ba90b9c12ac2056b56',1,'Engine::FIXGroup::forward_iteratorT::value_type()'],['../classEngine_1_1LocalMktDate.html#a1ecaaae62d4cfcc0d2ef4bb044b9ab7e',1,'Engine::LocalMktDate::value_type()'],['../classUtils_1_1NullableValue.html#a8cd2a2b794e593ded6350a7d462b66c4',1,'Utils::NullableValue::value_type()'],['../classEngine_1_1MemBlock.html#a824071743b5927d3d1196d1c050daebf',1,'Engine::MemBlock::value_type()'],['../classEngine_1_1BasicString.html#a9c29a31a95a321d7fa7c951530affc1b',1,'Engine::BasicString::value_type()'],['../classEngine_1_1UTCDateOnly.html#a0e4e3d925bfd235eaef984ff0e4677d9',1,'Engine::UTCDateOnly::value_type()'],['../classEngine_1_1UTCTimeOnly.html#a392b929d069f76fdfbcd68e3488737f0',1,'Engine::UTCTimeOnly::value_type()'],['../classEngine_1_1UTCTimestamp.html#a070532a09050a224e100ec042fb9af2b',1,'Engine::UTCTimestamp::value_type()']]],
  ['valuedescription',['ValueDescription',['../structFixDictionary2_1_1ValBlock_1_1ValueDescription.html',1,'FixDictionary2::ValBlock']]],
  ['valuedescription',['ValueDescription',['../structFixDictionary2_1_1ValBlock_1_1ValueDescription.html#abac763852c521f0ec428e09e516a11f4',1,'FixDictionary2::ValBlock::ValueDescription::ValueDescription()'],['../structFixDictionary2_1_1ValBlock_1_1ValueDescription.html#a23133f905f4403bd803daea4db7f2826',1,'FixDictionary2::ValBlock::ValueDescription::ValueDescription(std::string const &amp;description, std::string const &amp;id)']]],
  ['valueoffutures',['ValueOfFutures',['../namespaceFIXFields.html#adf44e5aab54b8804dddb8bb164bac81d',1,'FIXFields']]],
  ['values',['Values',['../classFixDictionary2_1_1ValBlock.html#a22e80298b251105fbcd48765f31f2024',1,'FixDictionary2::ValBlock']]],
  ['valuesize',['ValueSize',['../classEngine_1_1Decimal.html#acaa50cc0fef3e08d7d8c772078b8b82b',1,'Engine::Decimal::ValueSize()'],['../classEngine_1_1LocalMktDate.html#aea1fa0a5f7d5242b2f33932f9c5f83e7',1,'Engine::LocalMktDate::ValueSize()'],['../classEngine_1_1MonthYear.html#ac6cbb56896ce73be23ebb08b47080186',1,'Engine::MonthYear::ValueSize()'],['../classEngine_1_1UTCDateOnly.html#ad1beefbc5c29d66366880d7fa1a38b83',1,'Engine::UTCDateOnly::ValueSize()'],['../classEngine_1_1UTCTimeOnly.html#aa880c2787a4848925072cabe7be6f795',1,'Engine::UTCTimeOnly::ValueSize()'],['../classEngine_1_1UTCTimestamp.html#a1af5baa3ef2778e937f575265e82047d',1,'Engine::UTCTimestamp::ValueSize()']]],
  ['valuesizebuffertztimeonly',['ValueSizeBufferTZTimeOnly',['../namespaceEngine_1_1TZTimeHelper.html#a7f78add1e148946e498b593b7a65e072',1,'Engine::TZTimeHelper']]],
  ['valuesizebuffertztimestamp',['ValueSizeBufferTZTimestamp',['../namespaceEngine_1_1TZTimeHelper.html#a6bed7f351bb53f81bc78820d06fc9fc8',1,'Engine::TZTimeHelper']]],
  ['valuesizebufferutcdateonly',['ValueSizeBufferUTCDateOnly',['../namespaceEngine_1_1TZTimeHelper.html#aa430def7b65ed9130b305780c02ffa54',1,'Engine::TZTimeHelper']]],
  ['valuesizebufferutctimeonly',['ValueSizeBufferUTCTimeOnly',['../namespaceEngine_1_1TZTimeHelper.html#af0443eb83e0ee9f47b2cdce565f6e810',1,'Engine::TZTimeHelper']]],
  ['valuesizebufferutctimestamp',['ValueSizeBufferUTCTimestamp',['../namespaceEngine_1_1TZTimeHelper.html#a74c870d420398afc0de29a8eac0991d3',1,'Engine::TZTimeHelper']]],
  ['valuesizenoday',['ValueSizeNoDay',['../classEngine_1_1MonthYear.html#a042f2f508a3d7a2c0ab72104000150d1',1,'Engine::MonthYear']]],
  ['valuesizenomilliseconds',['ValueSizeNoMilliseconds',['../classEngine_1_1UTCTimeOnly.html#a26ada5503d9f9c0550fdf106a1210013',1,'Engine::UTCTimeOnly::ValueSizeNoMilliseconds()'],['../classEngine_1_1UTCTimestamp.html#ae03386784127f7f2d718a3c2259e6f7b',1,'Engine::UTCTimestamp::ValueSizeNoMilliseconds()']]],
  ['valuesizeutctimeonlymicro',['ValueSizeUTCTimeOnlyMicro',['../namespaceEngine_1_1TZTimeHelper.html#a6d19922bba8d7b4ccf4e2c3e0c5ed3c9',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimeonlymilli',['ValueSizeUTCTimeOnlyMilli',['../namespaceEngine_1_1TZTimeHelper.html#acc7e9ac0fa96b3a50a6fe278a9b5e9ad',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimeonlynano',['ValueSizeUTCTimeOnlyNano',['../namespaceEngine_1_1TZTimeHelper.html#a2c8a5039d0ae64e463d75f7bb13811ab',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimeonlypico',['ValueSizeUTCTimeOnlyPico',['../namespaceEngine_1_1TZTimeHelper.html#a2bf0d586f86ebd56b863c936a034e81d',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimeonlysec',['ValueSizeUTCTimeOnlySec',['../namespaceEngine_1_1TZTimeHelper.html#a5a93dd61cda8db05d740fedec2f93eb3',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimestampmicro',['ValueSizeUTCTimestampMicro',['../namespaceEngine_1_1TZTimeHelper.html#a9644d6aea732e70610b98d35d725faca',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimestampmilli',['ValueSizeUTCTimestampMilli',['../namespaceEngine_1_1TZTimeHelper.html#ac12347b0ff90da65124c5acd6aeb26e8',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimestampnano',['ValueSizeUTCTimestampNano',['../namespaceEngine_1_1TZTimeHelper.html#a27da2132befdd1ce5a119a6fee308b54',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimestamppico',['ValueSizeUTCTimestampPico',['../namespaceEngine_1_1TZTimeHelper.html#aa54b7b055e639c684da3fb4f71274ea6',1,'Engine::TZTimeHelper']]],
  ['valuesizeutctimestampsec',['ValueSizeUTCTimestampSec',['../namespaceEngine_1_1TZTimeHelper.html#a1a4a1dd9f8a799bff6a89a49e370b2e6',1,'Engine::TZTimeHelper']]],
  ['valuesizewithday',['ValueSizeWithDay',['../classEngine_1_1MonthYear.html#a432e804c8bbe29dfa6c12ca41ec11f44',1,'Engine::MonthYear']]],
  ['valuesizewithmilliseconds',['ValueSizeWithMilliseconds',['../classEngine_1_1UTCTimeOnly.html#a5b56c2ac8ef94c8cf73682ca8e055993',1,'Engine::UTCTimeOnly::ValueSizeWithMilliseconds()'],['../classEngine_1_1UTCTimestamp.html#a4851c15be5129b9f39364fc2d0042420',1,'Engine::UTCTimestamp::ValueSizeWithMilliseconds()']]],
  ['valuesizewithweek',['ValueSizeWithWeek',['../classEngine_1_1MonthYear.html#ae2a9e73e6a0e11e5a8f1215321f0e0db',1,'Engine::MonthYear']]],
  ['valuestate',['ValueState',['../namespaceUtils.html#ab71835c56c9c827143a8562ff231552a',1,'Utils']]],
  ['valuetype',['valueType',['../classFixDictionary2_1_1FieldType.html#adacc3ad145c7e6a77e9ba991ddb2f774',1,'FixDictionary2::FieldType']]],
  ['vendorid_5f',['vendorId_',['../classEngine_1_1FastScp11_1_1Hello.html#acf42cf01328b55c389b9b97b414375f0',1,'Engine::FastScp11::Hello']]],
  ['venuetype',['VenueType',['../namespaceFIXFields.html#a02ea693ea47038b566bf13fc7b96f42b',1,'FIXFields']]],
  ['verify_5fdata_5ftags_5fsequence',['VERIFY_DATA_TAGS_SEQUENCE',['../classEngine_1_1FIXMsgProcessor.html#a66a0b64cc2db49da91f984924ec477b8ac81c5e918742eacfc2144cd8d4a8698e',1,'Engine::FIXMsgProcessor']]],
  ['verify_5freperating_5fgroup_5fbounds',['VERIFY_REPERATING_GROUP_BOUNDS',['../classEngine_1_1FIXMsgProcessor.html#a66a0b64cc2db49da91f984924ec477b8a87dbae480f1ba26aac8dea6a90a10658',1,'Engine::FIXMsgProcessor']]],
  ['verify_5ftags_5fvalues',['VERIFY_TAGS_VALUES',['../classEngine_1_1FIXMsgProcessor.html#a66a0b64cc2db49da91f984924ec477b8a0367993385016ad0a302b163b77403c5',1,'Engine::FIXMsgProcessor']]],
  ['verifyreperatinggroupbounds_5f',['verifyReperatingGroupBounds_',['../structEngine_1_1SessionExtraParameters_1_1ValidationParameters.html#a7761400b36f510006aded68481fa55d5',1,'Engine::SessionExtraParameters::ValidationParameters']]],
  ['verifytagsvalues_5f',['verifyTagsValues_',['../structEngine_1_1SessionExtraParameters_1_1ValidationParameters.html#a380120fd344cd1ba453916ae5a5cbc38',1,'Engine::SessionExtraParameters::ValidationParameters']]],
  ['version',['version',['../classFixDictionary2_1_1Protocol.html#a63b42646a42d280b88abfc0c02929178',1,'FixDictionary2::Protocol']]],
  ['version_5f',['version_',['../structEngine_1_1StorageMgr_1_1PersistentSessionInfo.html#aef2c631045c0cd30c43549bc0874aca1',1,'Engine::StorageMgr::PersistentSessionInfo']]],
  ['volatilesensitivepointer',['VolatileSensitivePointer',['../classSystem_1_1ThreadSafe_1_1VolatileSensitivePointer.html',1,'System::ThreadSafe']]],
  ['volatilesensitivepointer',['VolatileSensitivePointer',['../classSystem_1_1ThreadSafe_1_1VolatileSensitivePointer.html#a47b02c75294241610b8101ae651f42d4',1,'System::ThreadSafe::VolatileSensitivePointer']]],
  ['volatility',['Volatility',['../namespaceFIXFields.html#aae365a21740a0cec5443abaeeef44cdd',1,'FIXFields']]],
  ['vsassigned',['vsAssigned',['../namespaceUtils.html#ab71835c56c9c827143a8562ff231552aa7a23c2adc41cf1690b7a9f46e0d6d470',1,'Utils']]],
  ['vsempty',['vsEmpty',['../namespaceUtils.html#ab71835c56c9c827143a8562ff231552aa337dce9da74fc81ae9efc21b46843bb5',1,'Utils']]],
  ['vsundefined',['vsUndefined',['../namespaceUtils.html#ab71835c56c9c827143a8562ff231552aa0302902554da2463f9afa82b7d7c224f',1,'Utils']]]
];
