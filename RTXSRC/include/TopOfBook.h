//
// Created by schen on 10/5/2020.
//

#ifndef RTXROUTER_TOPOFBOOK_H
#define RTXROUTER_TOPOFBOOK_H

#include "Price.h"

class TopOfBook
{
public:
    TopOfBook() {};
    TopOfBook(
            const std::string& mpid,
            const std::string& symbol,
            const Price&       askPrice,
            const int          askSize,
            const Price&       bidPrice,
            const int          bidSize
    )
     :       m_mpid(mpid),
             m_symbol(symbol),
             m_askPrice(askPrice),
             m_askSize(askSize),
             m_bidPrice(bidPrice),
             m_bidSize(bidSize)
    {}

    std::string m_mpid;
    std::string m_symbol;
    Price       m_askPrice;
    int         m_askSize;
    Price       m_bidPrice;
    int         m_bidSize;

};

using BatchTOB = std::vector<TopOfBook>;
#endif //RTXROUTER_TOPOFBOOK_H
