// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MessageItem.h
/// Contains FixDictionary2::MessageItem class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_MessageItem_H
#define H_B2BITS_FixDictionary2_MessageItem_H

#include <string>
#include <map>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class MessageItem
    *   @brief Represents MessageItem interface.
    */
    class V12_API MessageItem : public Item
    {
    public: // Types
        /** Dictionary Item Names */
        enum MessageItemName {
            MESSAGEITEM_FIELDREF,           ///< Field ref item name
            MESSAGEITEM_REPEATINGGROUP,     ///< Repeating group item name
            MESSAGEITEM_BLOCKREF,           ///< BlockRef item name
        };

    public: // Item contract

        /** Make copy of object */
        virtual MessageItemT::Ptr clone() const = 0;

        /** Converts instance of the Item to MessageItem. 
         * Reimplemented from the Item.
         */
        virtual MessageItemT::Ptr toMessageItem() B2B_OVERRIDE;

        /** Converts instance of the Item to MessageItem. 
         * Reimplemented from the Item.
         */
        virtual MessageItemT::CPtr toMessageItem() const B2B_OVERRIDE;

        /** 
        * Returns type of the Message Item
        */
        virtual MessageItemName messageItemName() const throw() = 0;

    public:
        /** Returns true, if item is required or conditionaly required.
        *
        * @return true, if item is required or conditionaly required.
        */
        bool required() const throw();

        /** Returns true, if item is required or conditionaly required.
        *
        * @return true, if item is required or conditionaly required.
        */
        bool isRequired() const throw() {
            return required();
        }

        /** Set true, if item is required or conditionaly required.
        *
        * @param required value
        */
        void setRequired( bool required ) throw();

        /** Returns condition of MessageItem
        *
        * @return condition of MessageItem
        */
        std::string const& condition() const throw();

        /** Returns condition of MessageItem
        *
        * @return condition of MessageItem
        */
        std::string const& getCondition() const throw() {
            return condition();
        }

        /** Set condition of MessageItem
        *
        * @return condition value
        */
        void setCondition( std::string const& condition );

    public:
        /** Constructor. */
        MessageItem();

        /** Copy Constructor. */
        MessageItem( MessageItem const& msgItem );

        /** Destructor */
        virtual ~MessageItem() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;

        MessageItem& operator= (MessageItem const&) B2B_DELETE_FUNCTION;
    }; // class MessageItem

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_MessageItem_H

