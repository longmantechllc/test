//
// Created by schen on 10/6/2020.
//


#include "../include/ExecutionReport.h"
#include "../include/Order.h"

Order::Order(const Order& odr)
{
    orderID_ = odr.orderID_;
    childOrderId_ = odr.childOrderId_;
    cltOrderId_ = odr.cltOrderId_;
    account_ = odr.account_;
    subAccount_ = odr.subAccount_;
    logInId_ = odr.logInId_;

    symbol_ = odr.symbol_;
    side_ = odr.side_;
    size_ = odr.size_;
    displaySize_ = odr.displaySize_;
    type_ = odr.type_;
    price_ = (odr.price_.value());
    discretion_ = (odr.discretion_.value());
    TIF_ = odr.TIF_;
    expireTime_ = odr.expireTime_;
    minQty_ = odr.minQty_;
    exchange_ = odr.exchange_;

    text_ = odr.text_;
    securityType_ = odr.securityType_;
    tradingSessionID_ = odr.tradingSessionID_;
    transactTime_ = 0;
}

std::shared_ptr<ExecutionReport> Order::CreatER()
{
    auto er = std::make_shared<ExecutionReport>();

    er->orderId_ = orderID_ ;
    er->childOrderId_ = "";
    er->childOrderId_ = childOrderId_;
    er->execId_ = "0";
    er->accountId_ = account_;
    er->subAccount_ = subAccount_;
    er->logInId_ = logInId_;
    er->status_ = 0;

    er->symbol_ = symbol_;
    er->side_ = side_;
    er->size_ = size_;
    er->displaySize_ = displaySize_;
    er->price_ = price_;
    er->discretion_ = discretion_;
    er->type_ = type_;
    er->exchange_ = exchange_;
    er->TIF_ = TIF_;

    er->parentOrderSize_ = size_;
    er->cumulativeFillQuantity_ = 0;
    er->fillSize_ = 0;
    er->fillPrice_ = price_;

    er->transactTime_ = 0;

    return er;
}