/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
 * $Revision: 1.3 $
 */

#ifndef _My_App__h_
#define _My_App__h_

#include "B2BITS_V12.h"

#include <iostream>

class MyApp : public Engine::Application
{

public:

    /** Constructor */
    MyApp()
        : m_pSn( NULL )
        , m_nMsg( 0 )
        , m_bClosed( false )
    {}

    /** Destructor */
    virtual ~MyApp();

    /**
     * Reimplemented from Engine::Application.
     * Processes incoming messages.
     */
    virtual bool process( const Engine::FIXMessage&, const Engine::Session& aSn );

    /*
     * Sets the value of session pointer
     * for the current application instance.
     */
    void setSession( Engine::Session* apSn );

    /*
     * Returns the value of session pointer
     * for the current application instance.
     */
    const Engine::Session* getSession();

    /** Sends message to a session */
    void put( Engine::FIXMessage* pMessage );

    /*
     * Checks if application session was closed
     */
    bool isSesClosed();

    /*
     * Closes the session.
     */
    void close();

    /*
     * Returns number of received messages.
     */
    long getNMsg();

    /**
     * Blocks the calling thread until the session is closed
     */
    void waitUntilSesClosed();

    /**
    * This call-back method is called to notify about Logon event.
    */
    virtual void onLogonEvent( const Engine::LogonEvent* apEvent, const Engine::Session& aSn );

    /**
     * This call-back method is called to notify about Logout event.
     */
    virtual void onLogoutEvent( const Engine::LogoutEvent* apEvent, const Engine::Session& aSn );

    /**
     * This call-back method is called to notify about SequenceGap event.
     */
    virtual void onSequenceGapEvent( const Engine::SequenceGapEvent* apEvent, const Engine::Session& aSn );

    virtual void onMsgRejectEvent( const Engine::MsgRejectEvent* /*event*/, const Engine::Session& /*sn*/ ) {}
    /**
     * This call-back method is called to notify about SessionLevelReject event.
     */
    virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* apEvent, const Engine::Session& aSn );

    /**
    * This call-back method is called when a Heartbeat message (MsgType = 0)
    * with the TestReqID field (tag = 112) has been received.
    *
    * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    virtual void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    /**
     * This call-back method is called when a Resend Request (MsgType = 2) has been received.
     */
    virtual void onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    /**
     * This call-back method is called when the session has changed its state.
     */
    virtual void onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    virtual void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& event, const Engine::Session& sn );

    /**
    * This call-back method is called when an outgoing message is about to be resent
    * as a reply to the incoming ResendRequest message.
    *
    * If the method returns 'true' then the Engine resends the message to counterpart,
    * otherwise it sends the SequenceReset Gap Fill message.
    *
    * @param msg Outgoing message.
    * @param sn FIX session.
    *
    @return true if the message should be resent, otherwise false.
    */
    virtual bool onResend( const Engine::FIXMessage& /*msg*/, const Engine::Session& /*sn*/ ) {
        return true;
    }

protected:
    System::Semaphore m_sem; // Session closing semaphore
    Engine::Session* m_pSn; // Session pointer
    long m_nMsg; // Processed message number
    bool m_bClosed; // Session closed flag
};

#endif // #define _My_App__h_
