#pragma once

#include <stdint.h>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <cassert>
#include <memory>
#include <thread>
#include <atomic>

#include "quickfix/FileStore.h"
#include "quickfix/FileLog.h"
#include "quickfix/SocketInitiator.h"
#include "quickfix/Log.h"
#include "quickfix/SessionSettings.h"
#include "quickfix/Session.h"
#include "quickfix/Application.h"
#include "quickfix/MessageCracker.h"
#include "quickfix/fix44/IOI.h"

#include "Utils/ApplicationProperties.h"
#include "binacpp_websocket.h"

namespace FIX {
    namespace FIELD {
        const int IoiPi = 31370;
        const int IoiPfof = 31460;
    }
    USER_DEFINE_PRICE(IoiPi, FIELD::IoiPi);
    USER_DEFINE_PRICE(IoiPfof, FIELD::IoiPfof);
};

struct Book {
    std::string symbol; // system symbol
    uint64_t timeStamp{0}; // last updat time
    std::map<double, double> bids, asks;
};

/// Application implementation (processes the incoming messages).
class MDCache : public FIX::Application, public FIX::MessageCracker
{
public:
    explicit MDCache(const ApplicationProperties& params);
    ~MDCache();

    // Application overloads
    void onCreate( const FIX::SessionID& ) override;
    void onLogon( const FIX::SessionID& sessionID ) override;
    void onLogout( const FIX::SessionID& sessionID ) override;
    void toAdmin( FIX::Message&, const FIX::SessionID& ) override;
    void toApp( FIX::Message&, const FIX::SessionID& )
        EXCEPT ( DoNotSend )  override;
    void fromAdmin( const FIX::Message&, const FIX::SessionID& )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon ) override;
    void fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType ) override;

    bool isInited() {return m_init;}
    auto& getIoiCache() { return m_ioiCache;}
    auto& getMutex() { return m_mutex;}
    void publish(bool buy, const Book& book);
    void run();
private: // Engine::Application contract implementation
    template<typename T>
    static auto getField(const auto& msg) {
        T field;
        msg.get(field);
        return field;
    };

    template<typename T>
    static auto getFieldValue(const auto& msg) {
        T field;
        msg.get(field);
        return field.getValue();
    };
    template<typename T>
    static auto getFieldValueIf(const auto& msg, auto& var) {
        T field;
        if (msg.isSetField(field)) {
            msg.get(field);
            var = field.getValue();
        }
    };
    void loadDataFile();
    void subscribeMarketData();

    const ApplicationProperties& params_;

    std::unique_ptr<FIX::SocketInitiator> m_initiator;
    FIX::SessionID session_; //, cliSession_;
    bool m_init{}, m_connected{};
    std::mutex m_mutex;
    std::vector<std::pair<std::string, std::string>> m_symbols; //internal symbol, exch symbol

    //internal symbol -> book, sorted by price(pi)
    std::unordered_map<std::string, Book> m_ioiCache;
    std::unordered_map<std::string, std::tuple<std::string, double, double>> m_session2Exch;
    struct pair_hash
    {
        template <class T1, class T2>
        std::size_t operator() (const std::pair<T1, T2> &pair) const {
            return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
        }
    };
    std::vector<std::unordered_map<std::pair<std::string, bool>, uint64_t, pair_hash>> m_ioiIds;
    std::vector<CryptoWebSocket> m_fhs;
};


