// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Field.h
/// Contains FixDictionary2::Field class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_Field_H
#define H_B2BITS_FixDictionary2_Field_H

#include <string>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class Field
    *   @brief Represents Field interface.
    */
    class V12_API Field B2B_SEALED
        : public Item
    {
    public:
        /** Copy Constructor. */
        Field( Field const& field );

        /** Constructor. */
        Field( std::string const& typeName,
               int tag,
               std::string const& name,
               std::string const& fixmlName );

        /** Make copy of object */
        FieldT::Ptr clone() const {
            return new Field( *this );
        }

        /** Returns type of Field
        *
        * @return type of Field
        */
        std::string const& typeName() const throw();

        /** Returns type of Field
        *
        * @return type of Field
        */
        std::string const& getTypeName() const throw() {
            return typeName();
        }

        /**
        *  Assigns field type
        */
        void setTypeName( std::string const& typeName );

        /** Returns tag of Field
        *
        * @return tag of Field
        */
        int tag() const throw();

        /** Returns tag of Field
        *
        * @return tag of Field
        */
        int getTag() const throw() {
            return tag();
        }

        /**
        * Assigns field tag
        */
        void setTag( int tag ) throw();

        /** Returns name of Field
        *
        * @return name of Field
        */
        std::string const& name() const throw();

        /** Returns name of Field
        *
        * @return name of Field
        */
        std::string const& getName() const throw() {
            return name();
        }

        /**
        * Assigns field name
        */
        void setName( std::string const& name );

        /** Returns fixmlName of Field
        *
        * @return fixmlName of Field
        */
        std::string const& fixmlName() const throw();

        /** Returns fixmlName of Field
        *
        * @return fixmlName of Field
        */
        std::string const& getFixmlName() const throw() {
            return fixmlName();
        }

        /**
        * Assigns FIXML field name
        */
        void setFixmlName( std::string const& fixmlName );

        /**
        * Assigns FIXML field name
        */
        void fixmlName( std::string const& fixmlName ) {
            setFixmlName( fixmlName );
        }

        /**
        * Assigns list of valid values.
        * Methods aqures one reference to the ValBlock
        */
        void setValBlock( ValBlockT::Ptr valBlockPtr ) throw();

        /** Returns list of valid values. Can be NULL */
        ValBlockT::CPtr getValBlock() const throw();

        /** Returns name of the external ValBlock */
        std::string const& getValBlockRef() const throw();

        /** Assigns reference to the external named ValBlock */
        void setValBlockRef( std::string const& blockId );

        /**
        * Returns tag of the associated length field. 
        * If no field is associated, function returns -1.
        * Applicable only for data fields.
        */
        int lenField() const throw();

        /**
        * Returns tag of the associated length field. 
        * Applicable only for data fields.
        */
        int getLenField() const throw() {
            return lenField();
        }

        /**
        * Assigns tag of the associated length field. 
        * Applicable only for data fields.
        */
        void setLenField( int tag ) throw();

        /**
        * Assigns tag of the associated length field. 
        * Applicable only for data fields.
        */
        void lenField( int tag ) throw() {
            setLenField( tag );
        }

        /** Returns description of the field */
        std::string const& getDescription() const throw();

        /** Assigns description of the field */
        void setDescription(std::string const& value);

        /** Returns description of the field */
        std::string const& description() const throw() {
            return getDescription();
        }
        
        /** Returns true if field is part of transport protocol */
        bool getIsTransport() const throw();

        /** Assigns description of the field */
        void setIsTransport(bool value);

        /** Returns true if field is part of transport protocol */
        bool isTransport() const throw() {
            return getIsTransport();
        }

    public: // Item contract
        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

        /** Try downcast to Field class
        *
        * @return downcast to Field class
        */
        virtual FieldT::Ptr toField() B2B_OVERRIDE;

        /** Try downcast to Field class
        *
        * @return downcast to Field class
        */
        virtual FieldT::CPtr toField() const B2B_OVERRIDE;

    protected:
        /** Destructor */
        virtual ~Field() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;

        Field& operator= (Field const&) B2B_DELETE_FUNCTION;
    }; // class Field

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_Field_H
