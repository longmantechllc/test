// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CustomRawDataTagProcessingStrategy.h
/// Contains defines for customization of interpretation *Len fields

#ifndef H_B2BITS_Engine_CustomRawDataTagProcessingStrategy_H
#define H_B2BITS_Engine_CustomRawDataTagProcessingStrategy_H

namespace Engine
{

/*
 * @brief Processing raw data tag strategies defines
 *
 * */
namespace RawDataTypeProcessingStrategies {
/*
 * @brief Processing strategy variants
 *
 * */
enum RawDataTypeProcessingStrategy {
    BY_BYTES,   //!< Length field contains number of bytes
    BY_CHARS    //!< Length field contains number of characters in corresponding encoding
};

/*
 * @brief Available to interpretation encodings
 *
 * */
enum Encoding{
    HEADER,     //!< Use the encoding from Message Header @attention onlu usable for Encoded* fields
    ASCII,      //!< Use ACSII encoding
    UTF8       //!< Use UTF8 encoding
};

struct CustomRawDataTagProcessingStrategy {

    CustomRawDataTagProcessingStrategy(int tag = 0,
                                       RawDataTypeProcessingStrategy strategy = BY_BYTES,
                                       Encoding encoding = ASCII) : Tag_(tag),
        Strategy_(strategy),
        Encoding_(encoding) {

    }

    bool operator == (const CustomRawDataTagProcessingStrategy& other) const {
        return Tag_ == other.Tag_ && Strategy_ == other.Strategy_ && Encoding_ == other.Encoding_;
    }

    int Tag_;                                   //!< tag, to which this strategy is applied. -1 for all tags with type RawData, 0 indicates emptyniess
    RawDataTypeProcessingStrategy Strategy_;    //!< How the Length field is interpreted
    Encoding Encoding_;                         //!< Used when Length is counted in Chars and the string is in different encoding than specified in Message Header;
};

} // namespace RawDataTypeProcessingStrategies


} // namespace Engine


#endif // H_B2BITS_Engine_CustomRawDataTagProcessingStrategy_H
