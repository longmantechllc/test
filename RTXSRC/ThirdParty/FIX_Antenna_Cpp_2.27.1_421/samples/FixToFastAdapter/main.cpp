// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>


#include <iostream>

#include <B2BITS_V12.h>
#include <B2BITS_FastCoder.h>
#include <B2BITS_FastDecoder.h>

const char FIX50SP1CME[] = "FIX50SP1CME";

void runSample()
{
    // Create Fast Coder
    std::auto_ptr<Engine::FastCoder> coder( Engine::FixEngine::singleton()->createFastCoder( "templates.xml", Engine::FIX50SP1CME ) );

    // Creare fast Decoder
    std::auto_ptr<Engine::FastDecoder> decoder( Engine::FixEngine::singleton()->createFastDecoder( "templates.xml", Engine::FIX50SP1CME ) );

    // Create FIXMessage to encode/decode
    std::auto_ptr<Engine::FIXMessage> x_msg( coder->newSkel( "X" ) );
    x_msg->set( FIXFields::ApplVerID, "9" );
    x_msg->set( FIXFields::SenderCompID, "CME" );
    x_msg->set( FIXFields::MsgSeqNum, 1 );
    x_msg->set( FIXFields::SendingTime, Engine::UTCTimestamp::now() );
    x_msg->set( FIXFields::NoMDEntries, 1 );
    Engine::FIXGroup* grp = x_msg->getGroup( FIXFields::NoMDEntries );
    Engine::TagValue* entry = grp->getEntry( 0 );
    entry->set( FIXFields::MDEntryType, "0" );
    entry->set( FIXFields::SecurityIDSource , "8" );
    entry->set( FIXFields::MDEntryTime, Engine::UTCTimeOnly::now() );
    entry->set( FIXFields::MDUpdateAction, '0' );
    entry->set( FIXFields::MDPriceLevel, 1 );
    entry->set( FIXFields::SecurityID, 11223344 );
    entry->set( FIXFields::RptSeq, 12345 );
    entry->set( FIXFields::MDEntryPx, Engine::Decimal( 1234, 0 ) );
    entry->set( FIXFields::MDEntrySize, 4321 );

    // Print message
    int size;
    char const* raw = x_msg->toRaw( &size );
    std::cout << "Sources message: " << Engine::AsciiString( raw, size ) << std::endl;

    try {
        // Verify message (optional)
        coder->tryEncode( x_msg.get(), 81 );

        // Encode messafge
        Engine::FastCoder::Buffer buffer;
        coder->encode( x_msg.get(), &buffer );

        // Decode message
        std::auto_ptr<Engine::FIXMessage> x_msg2( decoder->decode( buffer.getPtr(), buffer.getSize() ) );

        // Print message
        raw = x_msg2->toRaw( &size );
        std::cout << "Decoded message: " << Engine::AsciiString( raw, size ) << std::endl;
    } catch( std::exception const& e ) {
        std::cerr << "PROBE Error: " << e.what() << std::endl;
    }
}

int main()
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        Engine::FixEngine::init();
        runSample();
        Engine::FixEngine::destroy();
    } catch( Utils::Exception const& e ) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }
}
