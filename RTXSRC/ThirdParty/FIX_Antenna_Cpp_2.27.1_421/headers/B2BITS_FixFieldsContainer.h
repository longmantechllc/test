// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FixFieldsContainer.h
/// Contains FixFieldsContainer class definition.

#ifndef H_B2BITS_Engine_FixFieldsContainer_H
#define H_B2BITS_Engine_FixFieldsContainer_H

#include <string>
#include <vector>

#include "B2BITS_V12_Defines.h"
#include "B2BITS_String.h"

namespace Engine
{

    /** FIX field (tag and value). */
    struct FixField {
        enum FieldRole {
            FieldRole_GenericField     = 1,   ///< Indicates generic field (String, Int, month-year, etc)
            FieldRole_LeadingTag       = 2,   ///< Indicates leading tag of the repeating group
            FieldRole_LastFieldInGroup = 4,   ///< Indicates last field in the repeating group
            FieldRole_StartTag         = 8    ///< Indicates first tag of the repeating group entry
        };

        int tag_;                             ///< FIX field tag
        std::string value_;                   ///< Value
        unsigned int role_;                      ///< Role

        /// Constructor
        /// @param tag FIX field tag
        /// @param value Field value
        /// @param role Field role
        FixField( int tag, const std::string& value, FieldRole role )
            : tag_( tag )
            , value_( value )
            , role_( role ) {
        }
#if __cplusplus >= 201103L
        FixField(int tag, std::string&& value, FieldRole role) : tag_(tag)
          , value_(value)
          , role_(role) {
        }
#endif
    };

    /// Base class for iterating over fields of FIXMessage
    ///
    /// For instance the following code prints all fields of the message to std::cout
    ///
    /// struct MessageDumper: public FixFieldsFunctor {
    ///     void operator()(int tag, const Engine::AsciiString& value, int role) {
    ///         std::cout << tag << '=' << value.data() << std::endl;
    ///     }
    /// };
    ///
    /// FIXMessage* msg; // fill it in
    /// MessageDumper dumper;
    /// msg->apply(dumper);
    ///
    class V12_API FixFieldsFunctor {
    public:
	/// operate on a field of a FIXMessage
	/// @param tag
	/// @param value non-owning reference to the value valid within the call.
	/// @param role field role (FixField::FieldRole)
        virtual void operator()(int tag, const Engine::AsciiString& value, int role) = 0;
        virtual bool needsRole() const { return true; }
        virtual ~FixFieldsFunctor() { }
    };

    /** Typename for the fields list*/
    typedef std::vector<FixField> FixFieldsList;

    /** Auxiliary class that encapsulates FixFieldsList and helps to use it in another CRT*/
    class V12_API FixFieldsContainer
    {
        friend class FixMsg;
        friend class FixGrp;
        friend class FixMsgHelper;
    public:
        /** Constructor */
        FixFieldsContainer();

        /** Constructor */
        FixFieldsContainer( FixFieldsList** inner_container );

        /** Releases container and destroys this object */
        void release() const;

        /** Releases container and destroys this object */
        void release();

        /** Returns container size */
        std::size_t size()const;

        /** Returns container element by index */
        FixField const& operator[]( std::size_t idx ) const;

        /** Clears container */
        void clear();

        /** Returns iterator of the first element */
        FixFieldsList::iterator begin();

        /** Returns iterator of the container end */
        FixFieldsList::iterator end();

        /** Erase element from container */
        void erase( FixFieldsList::iterator it );

    private:
        virtual ~FixFieldsContainer();
        FixFieldsList* cont_;

    private:
        /** Copy constructor */
        FixFieldsContainer( FixFieldsContainer const& );
        /** Assign operator */
        FixFieldsContainer& operator=( FixFieldsContainer const& );
    }; // class V12_API FixFieldsContainer


} // namespace Engine


#endif // H_B2BITS_Engine_FixFieldsContainer_H
