//
// Created by schen on 10/6/2020.
//

#ifndef RTXROUTER_PASSIVEPOSTING_H
#define RTXROUTER_PASSIVEPOSTING_H

class ExecutionReport;
class RTXOrder;

class PassivePosting : public IOrderLogicBase
{
public:
    PassivePosting() =default;
    PassivePosting(RTXOrder* order);
    ~PassivePosting() override;
    bool InitOrder() override;

    bool ProcessOrder() override;
    virtual bool SendChildOrder(std::shared_ptr<Order> order) override;
    bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child) override;
    bool ProcessMktData() override;
    bool m_cancelPosting = false;

private:
    bool IsRouteEligible(const std::string& routeName);
    bool IsEligible();
    bool CancelPostedChildOrder();
    bool GetPostRoute(std::string& routeName, std::vector<std::string>& postRoutes);
    bool RemovePostRoute(const std::string& routeName, std::vector<std::string>& postRoutes);

    std::vector<std::string> m_postRoutes;
    std::vector<std::string> m_marketDataFailureTimeOutPostRoutes;
    std::unordered_set<std::string> m_postedChild;

};

#endif //RTXROUTER_PASSIVEPOSTING_H
