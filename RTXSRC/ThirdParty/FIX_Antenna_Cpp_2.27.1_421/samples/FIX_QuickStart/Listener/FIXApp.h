/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#pragma once

#include <B2BITS_V12.h>

class FIXApp :
    public Engine::Application
{
public:
    System::Semaphore hbReceived_;
    int execId_;
    int orderId_;

public:
    FIXApp( void );
    ~FIXApp( void );

    virtual bool process( const Engine::FIXMessage& fixMsg, const Engine::Session& aSn );

    virtual void onLogonEvent( const Engine::LogonEvent* apEvent, const Engine::Session& aSn );

    virtual void onLogoutEvent( const Engine::LogoutEvent* apEvent, const Engine::Session& aSn );

    virtual void onMsgRejectEvent( const Engine::MsgRejectEvent* event, const Engine::Session& sn );

    virtual void onSequenceGapEvent( const Engine::SequenceGapEvent* apEvent, const Engine::Session& aSn );

    virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* apEvent, const Engine::Session& aSn );

    virtual void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& event, const Engine::Session& sn );

    virtual void onResendRequestEvent( const Engine::ResendRequestEvent& event, const Engine::Session& sn );

    virtual void onNewStateEvent( const Engine::NewStateEvent& event, const Engine::Session& sn );

    virtual void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& event, const Engine::Session& sn );

    virtual bool onResend( const Engine::FIXMessage& msg, const Engine::Session& sn );

};
