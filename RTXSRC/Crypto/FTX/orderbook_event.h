#pragma once

#include <json_dto/pub.hpp>
#include "constants.h"
#include "public_stream_event.h"

namespace Crypto::ftx {

struct orderbook_data_t {
    uint32_t checksum = 0;
    double time = 0;
    std::vector<std::vector<double>> bids;
    std::vector<std::vector<double>> asks;
    orderbook_data_t() {
        bids.reserve(512);
        asks.reserve(512);
    }

    void reset(){
        checksum = 0;
        time = 0;
        bids.clear();
        asks.clear();
    }
 
};

struct orderbook_event_t : public public_stream_event_t {
    orderbook_data_t data;

    void reset() {
        public_stream_event_t::reset();
        data.reset();
    }
};


}  // namespace Crypto::ftx

namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::orderbook_data_t &m) {
    io
    & json_dto::mandatory("time", m.time)
    & json_dto::mandatory("checksum", m.checksum)
    & json_dto::mandatory("bids", m.bids)
    & json_dto::mandatory("asks", m.asks);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::orderbook_event_t &m) {
    json_dto::json_io(io, static_cast<Crypto::ftx::public_stream_event_t &>(m));
    io
    & json_dto::mandatory("data", m.data);
}

}  // namespace json_dto
