#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <fstream>

std::ofstream logfs2("./logs/MD.log", std::ofstream::trunc);

const char*
udate()
{
    static char udate[60];
    time_t now = time(0);
    struct tm* tm = localtime(&now);
    struct timeval tv;
    gettimeofday(&tv, 0);
    sprintf(udate, "%02d/%02d/%04d %02d:%02d:%02d.%06ld",
            tm->tm_mon + 1, tm->tm_mday, tm->tm_year + 1900,
            tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec);

    return udate;
}

