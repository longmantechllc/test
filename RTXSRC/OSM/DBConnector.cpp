//
// Created by gary.guo on 2/21/2021.
//
#include "DBConnector.h"
#include <cstdlib>
using namespace mysqlx;
using namespace std;

namespace {
    inline auto getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::chrono::seconds now2 = std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch());
        //return now2.count();
        return std::chrono::duration_cast<std::chrono::seconds>(now.time_since_epoch()).count();
    }

}

void DBConnector::add(const tblState &rec) {
    m_state->insert("parentorderid", "childorderid", "execid", "refpoid", "instancename", "timestamp", "state")
            .values(rec.parentorderid, rec.childorderid, rec.execid, rec.refpoid, rec.instancename, rec.timeStamp, rec.state)
            .execute();
    ++m_tblRecordNum[2];
}

void DBConnector::add(const tblChildOrder &rec) {
    m_child->insert("parentorderid", "childorderid", "gatewayid","senddatetime", "instancename", "size", "displaysize", "type",
                   "limitprice", "discretion", "tif", "venue",  "expirytime", "execinstrution", "cminqty","message")
            .values(rec.parentorderid, rec.childorderid, rec.gatewayid, rec.senddatetime, rec.instancename, rec.size, rec.displaysize, rec.type,
                    rec.limitprice, rec.discretion, rec.tif, rec.venue, rec.expirytime, rec.execinstrution, rec.cminqty, rec.message)
            .execute();
    ++m_tblRecordNum[1];
}

void DBConnector::add(const tblExecution &rec) {
    m_execution->insert("parentorderid", "childorderid", "execid", "instancename", "recdatetime", "limitprice", "venue",
                       "tif", "fillsize", "fillprice", "cumqty", "avgprice", "message", "transacttime" )
            .values(rec.parentorderid, rec.childorderid, rec.execid, rec.instancename, rec.recdatetime, rec.limitprice, rec.venue,
                    rec.tif, rec.fillsize, rec.fillprice, rec.cumqty, rec.avgprice, rec.message, rec.transacttime)
            .execute();
    ++m_tblRecordNum[3];
}

void DBConnector::add(const tblParentOrder &rec) {
    m_parent->insert("parentorderid", "tradedatetime", "instancename", "refpoid", "cltordid", "symbol",
                     "side","size", "displaysize", "type", "limitprice", "discretion", "tif",
                     "expirationtime", "execinst", "minqty", "desk", "user","message")
            .values(rec.parentorderid, rec.tradedatetime, rec.instancename, rec.refpoid, rec.cltordid, rec.symbol,
                    rec.side, rec.size, rec.displaysize, rec.type, rec.limitprice, rec.discretion, rec.tif,
                    rec.expirationtime, rec.execinst, rec.minqty, rec.desk, rec.user, rec.message)
            .execute();
    ++m_tblRecordNum[0];
}

std::tuple<unsigned, unsigned, unsigned> DBConnector::getMaxIds(const std::string& instanceName) {
    RowResult myRows = m_maxIds->select("parentorderid", "gatewayid", "execid")
            .where("instancename = :instancename")
            .bind("instancename", instanceName)
            .execute();
    for (Row row : myRows.fetchAll())
    {
        return {row[0], row[1], row[2]};
    }
    return {0,0,0};
}

void DBConnector::getOpenOrders(const std::string &instanceName,
                                std::unordered_map<int, OSMRecoveredOrder>& recoveryPOrderMap,
                                std::unordered_map<std::string, RecoveredGWOrder>& recoveryCOrderMap) {
    RowResult myRows = m_recovery->select("parentorderid",  "refpoid", "cltordid", "symbol",
                                          "side","size", "displaysize", "type", "limitprice", "discretion", "tif",
                                          "expirationtime", "execinst", "minqty", "desk", "user", "cumqty", "avgprice",
                                          "childorderid", "gatewayid", "venue")
            .where("instancename = :instancename")
            .bind("instancename", instanceName)
            .execute();

    for (Row row : myRows.fetchAll())
    {
        OSMRecoveredOrder order;
        int i = 0;
        order.pOrderId = row[i++];
        order.refOrderId = row[i++];
        order.clOrdId = std::string(row[i++]);
        order.symbol = std::string(row[i++]);
        order.side = row[i++];
        order.qty = row[i++];
        order.displaySize = row[i++];
        order.type = row[i++];
        order.price = row[i++];
        order.discretion = row[i++];
        order.tif = row[i++];
        char *end;
        //order.expirationTime = strtoll(row[i++].c_str(), &end, 10);
        std::string exp = std::string(row[i++]);
        order.expirationTime = strtoll(exp.c_str(), &end, 10);
        order.execInst = std::string(row[i++]);
        order.minQty = row[i++];
        order.desk = std::string(row[i++]);
        order.user = std::string(row[i++]);
        order.accumQty = row[i++];
        order.avgPx = row[i++];

        recoveryPOrderMap.emplace(order.pOrderId, order);
        auto childOrderId = std::string(row[i++]);
        auto gatewayId = std::string(row[i++]);
        auto venue = std::string(row[i++]);
        if (!gatewayId.empty() && !childOrderId.empty()) {
            recoveryCOrderMap.emplace(gatewayId, RecoveredGWOrder{order.pOrderId, childOrderId, venue});
        }
    }

}

bool DBConnector::init() {
    try {
        m_sess = make_unique<Session>(m_hostName, m_portNum, m_user, m_pwd);
        m_db = make_unique<Schema>(m_sess->getSchema("RTX_TRADES"));
        m_state = make_unique<Table>(m_db->getTable("tblState"));
        m_parent = make_unique<Table>(m_db->getTable("tblParentOrder"));
        m_child = make_unique<Table>(m_db->getTable("tblChildOrder"));
        m_execution = make_unique<Table>(m_db->getTable("tblExecution"));
        m_recovery = make_unique<Table>(m_db->getTable("vw_recovery"));
        m_maxIds = make_unique<Table>(m_db->getTable("vw_maxID"));
        //m_stmt = make_unique<>stmt = con->createStatement();
    }
    catch (const mysqlx::Error &err) {
        spdlog::error("Could not connect to mysql server on {}: {}", m_hostName, err.what());
    }
    spdlog::info("DB is connected");
}

void DBConnector::processingMessage() {

    auto pq = [this](auto& q) {
        while (q.front()) {
            addRecord(*q.front());
            q.pop();
        }
    };
    pq(m_parentQ);
    pq(m_childQ);
    pq(m_stateQ);
    pq(m_exeQ);
    auto nowTime = getNow();
    if (m_logFlag && (nowTime - m_lastTime) > 300) {
        m_lastTime = nowTime;
        spdlog::info("Total DB insert: parent: {}, child: {}, state: {}, execution: {}",
                    m_tblRecordNum[0], m_tblRecordNum[1], m_tblRecordNum[2], m_tblRecordNum[3] );
        spdlog::info("DB queue current size: parent: {}, child: {}, state: {}, execution: {}",
                    m_parentQ.size(), m_childQ.size(), m_stateQ.size(), m_exeQ.size() );
    }
    std::this_thread::sleep_for(1ms);
}
