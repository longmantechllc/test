//
// Created by schen on 10/6/2020.
//
#ifndef RTXROUTER_IORDERLOGICBASE_H
#define RTXROUTER_IORDERLOGICBASE_H

#include "../include/Order.h"
#include "OrderLogic/_DataStruct/ChildOrderInfo.h"

class ExecutionReport;
class RTXOrder;

class IOrderLogicBase
{
public:
    IOrderLogicBase() = default;
    explicit IOrderLogicBase( RTXOrder* order)
    : m_order(order)
    {};
    virtual ~IOrderLogicBase(){};
    virtual bool InitOrder()=0;

    virtual bool ProcessOrder()=0;
    virtual bool SendChildOrder(std::shared_ptr<Order> order)=0;
    virtual bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)=0;
    virtual bool ProcessMktData()=0;

    bool m_isInited = false;
    RTXOrder* m_order;
};

#endif //RTXROUTER_IORDERLOGICBASE_H
