// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SessionParameters.h
/// Contains Session parameters class declaration.

#ifndef B2BITS_ENGINE_SessionParameters_H
#define B2BITS_ENGINE_SessionParameters_H

#include <B2BITS_V12_Defines.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_FastMappingOptions.h>
#include "B2BITS_Emptily.h"
#include <B2BITS_IPAddr.h>
#include <B2BITS_TZTimeHelper.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

#include <B2BITS_CustomRawDataTagProcessingStrategy.h>

namespace Engine
{
    class FIXMessage;

    /**
    * Session's extra parameters.
    *
    * If these parameters are present FIX Engine adds them to
    * Standard Message Header of all outgoing messages unless another
    * value has been explicitly set in the particular message.
    */
    struct V12_API SessionExtraParameters {
        /**
        * Validation parameters
        */
        struct ValidationParameters {
            /**
            * Option to enable message validation. Pass true to enable; false to disable.
            */
            TriBool isEnabled_;

            /**
            * When false, raw message may contains tags without values - they will be ignored.
            * Otherwise std::exception was fired.
            */
            TriBool prohibitTagsWithoutValue_;

            /**
            * This parameter controls tag values validation.
            * The possible values are "true" and "false". If set to "true" then all
            * messages will be validated. If set to "false" then the responsibility for message validity
            * rests with the counterparty.
            * Default: false
            */
            TriBool verifyTagsValues_;

            /**
            * This parameter controls unknown tag handling.
            * The possible values are "true" and "false". If set to "true" then all unknown
            * fields will be stored to the message. If set to "false", engine will reject messages
            * with unknown fields.
            * Default: false
            */
            TriBool prohibitUnknownTags_;

            /**
            * This parameter controls repeating group size checking. If true, engine will reject
            * messages with incorrect repeating group size. Otherwise message will be passed to the
            * application layer. In this case the responsibility for message validity
            * rests with the counterparty.
            * Default: false
            */
            TriBool verifyReperatingGroupBounds_;

            /**
            * This parameter controls the validation of required fields in repeating group.
            * The possible values are "true" and "false". If set to "true" then repeating
            * groups will be checked for presence of required fields.
            * If set to "false" then the responsibility for repeating group validity
            * rests with the counterparty.
            * The recommended setting is "true".
            * This parameter is optional.
            * The default value is "true".
            */
            TriBool checkRequiredGroupFields_;

            /**
            * When true, raw message may contains leading group tag with 0 value - it will be ignored.
            * Otherwise reject will be fired.
            */
            TriBool allowZeroNumInGroup_;

            /**
            * When false, raw message may contains duplicated field definitions.
            * Otherwise reject will be fired.
            */
            TriBool prohibitDuplicatedTags_;

            /**
            * Ignore undefined fields during message parsing
            */
            TriBool ignoreUnknownFields_;
        }; // struct ValidationParameters


        /** 
        * Host for initiator session 
        */
        std::string host_;
        /**
        * Port for initiator session
        */
        TriInt port_;
        /**
        * HBI for initiator session
        */
        TriInt hbi_;
        /**
        * Role of the session Initiator/Acceptor.
        */
        SessionRole sessionRole_;
        /**
        * Protocol/parser identification
        */
        std::string parserVersion_;
        /**
        * Custom logon message file name for initiator
        */
        std::string customLogonMessageFileName_;

        /**
        * Whatever to use SSL for the session
        */
        TriBool ssl_;
        /// SSL protocols to use.
        /// Parameter is mandatory if SSL is true. it is a comma separated list of protocols to use. Valid values are SSLv2, SSLv3, TLSv1, TLSv1_1, TLSv1_2.
        TriInt sslProtocols_;
        /// Ciphers list configuration string.
        /// Parameter is optional.
        std::string sslCiphersList_;
        /*
        * Certificate authorities file.
        */
        std::string sslCACertificate_;
        /*
        * Certificate file to use.
        * Can be in .pfx, .pem, .der formats
        */
        std::string sslCertificate_;
        /*
        * Password for certificate contained inside sslCertificate_ file
        * Optional.
        */
        std::string sslCertificatePassword_;
        /*
        * Private key file to use.
        * Optional for .pfx or .pem formated certificates (private key is embedded into them)
        */
        std::string sslPrivateKey_;
        /*
        * Password for private key contained inside sslPrivateKey_ file
        * Optional.
        */
        std::string sslPrivateKeyPassword_;
        /*
        * What ever to validate peer certificate.
        * default is false.
        */
        TriBool sslValidatePeerCertificate_;

        /**
        * SenderSubID (tag 50) - assigned value used to identify
        * specific message originator (desk, trader, etc.).
        * Ignored if NULL value specified.
        */
        std::string pSenderSubID_;

        /**
        * TargetSubID (tag 57) - assigned value used to identify
        * specific individual or unit intended to receive message.
        * 'ADMIN' reserved for administrative messages not intended
        * for a specific user.
        * Ignored if NULL value is specified.
        */
        std::string pTargetSubID_;

        /**
        * SenderLocationID (tag 142) - assigned value used to identify
        * specific message originator's location (i.e. geographic
        * location and/or desk, trader).
        * Ignored if NULL value is specified.
        */
        std::string pSenderLocationID_;

        /**
        * TargetLocationID_ (tag 143) - assigned value used to identify
        * specific message destination's location (i.e. geographic
        * location and/or desk, trader).
        * Ignored if NULL value specified.
        */
        std::string pTargetLocationID_;

        /**
        * User name.
        * - For initator: Value of this field will transfered to acceptor in the Username (Tag 553)
        *                 field of the Logon(A) message;
        * - For aceptor: Value of this field will be compared to value of the Username (Tag 553) field
        *                of the incoming Logon(A) message. If values are not equal, Logon will be rejected.
        */
        std::string userName_;

        /**
        * User's password.
        * - For initator: Value of this field will transfered to acceptor in the Password (Tag 554)
        *                 field of the Logon(A) message;
        * - For aceptor: Value of this field will be compared to value of the Password (Tag 554) field
        *                of the incoming Logon(A) message. If values are not equal, Logon will be rejected.
        */
        std::string password_;

        /**
        * Specifies a tag used to store an username
        */
        TriInt userNameTag_;


		/**
		 * Masks password in in/out logs
		 */

		bool hiddenLogonCredentials_;

		/**
        * Specifies a FIX key for LME sessions
        */
		std::string fixKey_;

		/**
        * Specifies whether a session should encrypt password each time
        */
		Utils::Emptily<CustomSessionType> customSessionType_;

		/**
        * The expected value of the timestamp unit for 52 and 112 tags. (default 0)
        */
        Utils::Emptily< Engine::TZTimeHelper::TimeFlags > sendingTimestampUnit_;

        /**
        * Specifies a tag used to store a password
        */
        TriInt passwordTag_;

        /**
	   * Specifies a tags which will be masked in log files
	   */
	   std::vector<std::string> maskedTags_;

        /**
        * The expected value of the source IP address. If the real value is not equal to the expected one
        * then the session is disconnected without sending a message and the error condition is generated
        * in the log output.
        */
        Addresses sourceIPaddress_;

        /**
        * Intraday logout tolerance mode.
        * An option to reset or not to reset sequence numbers after Logout.
        */
        TriBool intradayLogoutToleranceMode_;

        /**
        * Force SeqNum reset mode.
        * An option to use 141 tag in Logon message to reset sequence number.
        */
        ForceSeqNumResetMode forceSeqNumReset_;

        /**
        * Encryption method.
        */
        EncryptMethod encryptMethod_;

        
		/**
		* Optional.
		* Forced Reconnect mode - Session will try to reconnect in following situations:
		*	-# In WaitForConfirmLogon state when session received not a confirming logon
		*	-# In WaitForConfirmLogon state when logon frame expired
		*	-# In Reconnect state when session was closed
		*	-# In Initial state of session-initiator, when there were no acceptor exists. In this case this property works with the 'Reconnect.MaxTries' property in 'engine.properties' 
		*		(reconnectMaxTries_ in SessionExtraParameters) for registered session:
		*		- if forcedReconnect_ = false, the parameter Reconnect.MaxTries will be ignored, Utils::Exception will be thrown
		*		- If forcedReconnect_ = true, the parameter Reconnect.MaxTries will be used
        */
		TriBool forcedReconnect_;

        /**
        * When true, session reject application messages, when session unable to
        * sent them during specified period or was disconnected.
        */
        TriBool enableMessageRejecting_;


        /**
        * When true, session ignore 'SeqNum too low' at incoming Logon message and continue with received SeqNum.
        */
        TriBool ignoreSeqNumTooLowAtLogon_;

        /** When true, primary to backup (and back) connection switching continue using
        existing message storage. */
        TriBool keepConnectionState_;

        /** When true, session initiator will use async TCP connect */
        TriBool useAsyncConnect_;

        /** When true TCP buffer (Nagle algorithm) will be disabled for session.*/
        TriBool disableTCPBuffer_;

        /** Socket incoming buffer size */
        TriInt socketRecvBufSize_;

        /** Socket outgoing buffer size */
        TriInt socketSendBufSize_;

        /** Sets the approximate time in microseconds to busy poll on a
        * blocking receive when there is no data. (since Linux 3.11) */
        TriInt socketBusyPollTime_;

        /** Sets TCP maximum-segment size */
        TriInt socketTcpMaxSeg_;

        /** Message storage type */
        TriMessageStorageType storageType_;

        /** Recovery storage parameters*/
        TriStorageRecoveryStrategy storageRecoveryStrategy_;

        /** Enqueued outgoing messages could merged and sent as a single buffer. This parameter controls
        how many messages could be merged into the bunch. The 0 means infinite amount.
        */
        TriInt maxMessagesAmountInBunch_;

        /** Priority of the socket send/receive operations. By default Engine::EVEN_SOCKET_OP_PRIORITY used.*/
        SocketOpPriority socketPriority_;

        /**
         * Defines a polling interval in microseconds for reading data from the socket. 
         * Works only with aggressive sessions i.e. Engine::AGGRESSIVE_RECEIVE_SOCKET_OP_PRIORITY or Engine::AGGRESSIVE_SEND_AND_RECEIVE_SOCKET_OP_PRIORITY modes. 
         * Setting lower value reduces the latency but causes high CPU utilization.
         * The default value is 500 microseconds.
         */
        TriInt aggressiveReceiveDelay_;

        /** When true, automatic switch mode is enabled. By default automatic switch mode is disabled.*/
        TriBool enableAutoSwitchToBackupConnection_;

        /** When true, connection will be switched from primary to backup and back until connection will be established.
        By default cycle switch mode is disabled. */
        TriBool cyclicSwitchBackupConnection_;

        /** Handle sequence gaps using NextExpectedMsgSeqNum field in the Logon. By default it equals to false*/
        TriBool handleSeqNumAtLogon_;

        /**
        * This parameter specifies the number of attempts to restore the session.
        * The session is considered as restored if the telecommunication link was
        * restored and the exchange of Logon messages was successful.
        * If it is set to "-1", then the number of attempts is unlimited.
        */
        TriInt reconnectMaxTries_;

        /**
         * This parameter specifies the time interval between reconnection attempts in order to restore
         * a communications link. This value is specified in milliseconds (seconds*10-3).
         * The recommended value is 1000 for dedicated connections and private networks.
         * Internet connections require calibration. The value must be integer and greater than 0.
        */
        TriInt reconnectInterval_;

        /**
         * Specifies IP of the NIC interface used to connect to the remote site.
         */
        std::string connectAddress_;

        /**
         * Specifies TCP port used to connect to the remote site.
         */
        TriInt connectPort_;

        /**
        * This parameter specifies in-memory messages storage size.
        */
        TriInt messagesStorageSize_;

        /**
         * This parameter is an option whereby the repeated messages are received without PossDupFlag (43) field. It's used for incoming messages only.
         * If set to "true" and a message is received with a sequence number less than expected without PossDupFlag (43), a message will be processed. 
         * If set to "false", the connection will be closed. The default value is "false".
         */
        TriBool allowMessageWithoutPossDupFlag_;

        /**
         * Suppresses session to send Resend request message on every incoming message
         * with sequence greater than expected.
         */
        TriBool suppressDoubleResendRequest_;

        /** 
         * Forces Session to deliver Application Level messages if sequence number is 
         * too high. Required by CME. Messages are delivered to SessionListener::process
         * callback.
         */
        TriBool deliverAppMessagesOutOfOrder_;

        /**
         * Allows automatic adding the LastMsgSeqNumProcessed(369) tag to outgoing messages
         */
        TriBool sendLastMsgSeqNumProcessed_;

        /**
         * If true, session skip CRC validation for incoming messages
         * Default is true
         */
        TriBool validateCheckSum_;

        /**
         * If false, session will not calculate CRC for outgoing messages
         * Default is true
         */
        TriBool generateCheckSum_;

        /**
         * If true, session will store incoming FIX messages.
         * Default is true.
         */
        TriBool logIncomingMessages_;

        /**
        * Validation parameters
        */
        ValidationParameters validation_;

        /**
        * Defines number of messages requested using Resend Request messages.
        * If message sequence gap is larger than SessionExtraParameters::resendRequestBlockSize_, Session
        * will split resend requests by blocks of SessionExtraParameters::resendRequestBlockSize_ size.
        */
        TriInt resendRequestBlockSize_;


        /**
        * This parameter sets size temporary queue for processing out of sequence messages
        * default value is engine parameter OutOfSequenceMessagesStrategy.QueueSize
        */
        TriInt outOfSequenceMessagesQueueSize_;

        /**
        * This parameter sets strategy for out of sequence messages
        * default value is engine parameter OutOfSequenceMessagesStrategy
        */
        Utils::Emptily< Engine::OutOfSequenceMessagesStrategyEnum > outOfSequenceMessagesStrategy_;


        /**
         * @brief isForwardingFixMessagesToLog_ if true then all fix messages forwaring to the log system
         * default value is engine parameter IsForwardingFixMessagesToLog
         */
        TriBool isForwardingFixMessagesToLog_;


        /** Low level transport of the session */
        Utils::Emptily<Transport> transport_;

        /**
         * If true, session will use blocking I/O. 
         * 
         * This mode is useful for OpenOnload and enabled if aggressive mode is ON.
         * Engine::SessionExtraParameters::aggressiveReceiveDelay_ is used as timeout parameter.
         * Default is false.
         *
         * @see Engine::SessionExtraParameters::socketPriority_, Engine::SessionExtraParameters::aggressiveReceiveDelay_
         */
        TriBool useBlockingSockets_;

        /**
         * Defined path to the secure keys.
         */
        std::string cmeSecureKeysFile_;

        /**
        * SSL context. If set then used to establish SSL connection.
        * Use FAProperties::getSSLClientContext() to create client context or FAProperties::getSSLServerContext() to create server context.
        * Don't create classes instances your self or set this parameter to classes other than System::SSLContext, System::SSLClientContext or System::SSLServerContext, 
        * since this parameter can be overriden by the engine using the instance of the one mentioned above classes and your extentions to them will be lost.
        */
        Utils::Emptily<System::SSLContext> sslContext_;
        
		/**
		* Specifies affinity mask for dedicated threads of session.
		* It makes sense only for aggressive modes. Default value is 0 (the mask is not set)
		*/
        TriAffinity64 cpuAffinity_;

        /**
		* Specifies affinity mask for dedicated receiving thread of session.
		* It makes sense only for aggressive receive mode. Default value is 0 (the mask is not set)
		*/
        TriAffinity64 recvCpuAffinity_;

        /**
		* Specifies affinity mask for dedicated sending thread of session.
		* It makes sense only for aggressive send mode. Default value is 0 (the mask is not set)
		*/
        TriAffinity64 sendCpuAffinity_;

        /**
         * Defines tag of the field to be used by FIX Engine when dispatching incoming connection.
         * SessionQualifier is passed in this field. SenderCompID(49), TargetCompID(56)
         * and this field are used for search of appropriated acceptor among existing registered
         * cceptors. The field is optional if application doesn't intend to accept incoming connection.
        */
        TriInt logonMessageSessionQualifierTag_;

        /**
         * @brief rawDataTagProcessingStrategies_ processing *Len tags customization rules
         */
        std::vector<RawDataTypeProcessingStrategies::CustomRawDataTagProcessingStrategy> rawDataTagProcessingStrategies_;

        /**
        * Specifies schedule defined for the session. This parameter is not used directly by the engine.
        * So not automatice session binding happens.
        */
        std::string schedule_;

        /** Initializes all parameters to default (NULL) values. */
        SessionExtraParameters();

        /** Initializes all parameters to values from properties or to default (NULL) values if property not found. */
        SessionExtraParameters(const std::map<std::string, std::string>& properties, const std::string& prefix = std::string() );

        typedef std::map<std::string, std::string> Properties;

        /** Updates the parameters to values from properties. */
        void update(const Properties& properties, const std::string& prefix = std::string());

        /**
         * @brief The ParametersGetter class for get string value for parameters
         * @private
         */
        class ParametersGetter
        {
        public:
            /**
             * @brief The ValueOperation enum value of parameter
             */
            enum ValueOperation
            {
                KEEP_VALUE,     //parameter is undefined
                DEFAULT_VALUE,  //value is null
                SET_VALUE,
            };
            /**
             * @brief getValue return value of parameter
             * @param key key of parameter
             * @param value value of parameter
             * @return state of parameter
             */
            virtual ValueOperation getValue( const std::string & key , std::string * value ) const = 0;

            /**
             * @brief getAllOfPrefix add all parameters with specific prefix in map
             * @param prefix prefix of parameters
             * @param properties properties map
             * @return summary state of parameters.
             *    KEEP_VALUE    - updates properties don't contain parameters with prefix
             *    DEFAULT_VALUE - updates properties contain all parameters with state DEFAULT_VALUE . properties is empty.
             *    SET_VALUE - updates properties contain at least one parameter with state SET_VALUE
             */
            virtual ValueOperation getAllOfPrefix( const std::string & prefix , Properties* properties ) const = 0;
        };

        /**
         * @private
         * @brief modify modify parameters. If value of parameter is DEFAULT_VALUE then use default value. This function is internal. Don't use it in your code.
         * @param getter interface for getting parameters value
         * @param prefix start prefix for getting parameters
         * @param fullLoad true to modify all parameters, using default value if parameter is undefined ; false to modify parameters which state is defined.
         */
        virtual void modify(ParametersGetter * getter, const std::string& prefix = std::string() , bool fullLoad = false );


        typedef  Utils::Emptily<std::string> OptionalProperty;
        typedef std::map<std::string, OptionalProperty > OptionalProperties;

        /**
         * @brief modifyOptional modify parameters from optional values
         * @param properties optional properties for modification
         * @param prefix start prefix for getting parameters
         * @param fullLoad true to modify all parameters, using default value if parameter is undefined; false to modify parameters which state is defined.
         */
        void modifyOptional(const OptionalProperties &properties, const std::string& prefix = std::string() , bool fullLoad = false );

    }; // struct V12_API SessionExtraParameters

    typedef Utils::Emptily<SessionExtraParameters> TriSessionExtraParameters;

    /** Parameters of the FIX-over-FAST session */
    struct V12_API FastSessionExtraParameters: public SessionExtraParameters {
        TriBool enableFastScp_;

        FastMappingOptions fastMappingOptions_;

        /** Default constructor. */
        FastSessionExtraParameters();

        /** Initializes all parameters to values fom properties or to default (NULL) values if property not found. */
        FastSessionExtraParameters(const std::map<std::string, std::string>& properties, const std::string& prefix = std::string() );

        /**
        * Conversion constructor.
        * Converts SessionExtraParameters to FastSessionExtraParameters
        */
        FastSessionExtraParameters( SessionExtraParameters const& src );

        /**
         * @private
         * @brief modify modify parameters. If value of parameter is DEFAULT_VALUE then use default value. This function is internal. Don't use it in your code.
         * @param getter interface for getting parameters value
         * @param prefix start prefix for getting parameters
         * @param fullLoad true to modify all parameters, using default value if parameter is undefined; false to modify parameters which state is defined.
         */
        virtual void modify(ParametersGetter * getter, const std::string& prefix = std::string() , bool fullLoad = false );
    }; // struct V12_API FastSessionExtraParameters

    /** Encapsulates connection's host and port*/
    struct V12_API SessionBackupParameters {
        /** Default constructor */
        SessionBackupParameters()
            : port_( 0 )
            , hbi_( 0 ) {
        }

        /** Backup connection host*/
        std::string host_;

        /** Backup connection post*/
        int port_;

        /** Backup connection HBI, when it 0 HBI interval of the primary
        connection is used*/
        int hbi_;

        /** Backup connection auxiliary properties*/
        TriSessionExtraParameters params_;
    }; // struct SessionBackupParameters

    /** The FIX_TCP underlying protocol properties*/
    struct FixTcpParameters {
        /** remote FIX engine's host name or address. For the session-initiator only. */
        std::string host_;
        /** remote FIX engine's port. For the session-initiator only. */
        int port_;
        /** the encrypt method */
        EncryptMethod encryption_;
        /** heartbeat interval (in seconds). For the session-initiator only. */
        int hbi_;
        /** enabled the TCP buffer (Nagle algorithm) for the session. */
        bool enableTcpBuffer_;
        /** Custom Logon message. The FixTcpParameters doesn't own the custom logon message. */
        const FIXMessage* customLogonMsg_;

        /** Container for auxiliary named properties. */
        const StringProperties* protocolConfig_;

        /** Constructor for the session-acceptor*/
        FixTcpParameters( FIXMessage* customLogonMsg = NULL, EncryptMethod encryption = NONE, bool enableTcpBuffer = true )
            : host_()
            , port_( 0 )
            , encryption_( encryption )
            , hbi_( 0 )
            , enableTcpBuffer_( enableTcpBuffer )
            , customLogonMsg_( customLogonMsg )
            , protocolConfig_( NULL ) {
        }

        /** Constructor for the session-initiator*/
        FixTcpParameters( const char* host, int port, int hbi = 30, FIXMessage* customLogonMsg = NULL,
            EncryptMethod encryption = NONE, bool enableTcpBuffer = true )
            : host_( host? host: "" )
            , port_( port )
            , encryption_( encryption )
            , hbi_( hbi )
            , enableTcpBuffer_( enableTcpBuffer )
            , customLogonMsg_( customLogonMsg )
            , protocolConfig_( NULL ) {
        }
    }; // struct FixTcpParameters

    /** The FIX_TCP underlying protocol backup properties*/
    struct FixTcpBackupParameters {
        /** Backup parameters for the underlying protocol*/
        FixTcpParameters protocolParamss_;

        /** Backup session properties*/
        const SessionExtraParameters* sessionExtraParams_;

        /** Constructor for backup session-acceptor */
        FixTcpBackupParameters( FIXMessage* customLogonMsg = NULL, EncryptMethod encryption = NONE,
                                bool enableTcpBuffer = true, const SessionExtraParameters* params = NULL )
            : protocolParamss_( customLogonMsg, encryption, enableTcpBuffer )
            , sessionExtraParams_( params ) {
        }

        /** Constructor for the backup session-initiator*/
        FixTcpBackupParameters( const char* backuphost, int backupport, int hbi = 30, FIXMessage* customLogonMsg = NULL,
                                EncryptMethod encryption = NONE, bool enableTcpBuffer = true,
                                const SessionExtraParameters* params = NULL )
            : protocolParamss_( backuphost, backupport, hbi, customLogonMsg, encryption, enableTcpBuffer )
            , sessionExtraParams_( params ) {
        }
    }; // struct FixTcpBackupParameters

    /** The FIXT11_TCP underlying protocol properties*/
    struct FixT11TcpParameters: public FixTcpParameters {
        /** The default application protocol. */
        FIXVersion defaultApplProtocol_;

        /** Constructor for the session-acceptor*/
        FixT11TcpParameters( FIXMessage* customLogonMsg = NULL, EncryptMethod encryption = NONE,
                             bool enableTcpBuffer = true, FIXVersion defaultApplProtocol = FIX50 )
            : FixTcpParameters( customLogonMsg, encryption, enableTcpBuffer )
            , defaultApplProtocol_( defaultApplProtocol ) {
        }

        /** Constructor for the session-initiator*/
        FixT11TcpParameters( const char* host, int port, int hbi = 30, FIXMessage* customLogonMsg = NULL,
            EncryptMethod encryption = NONE, bool enableTcpBuffer = true,
            FIXVersion defaultApplProtocol = FIX50 )
            : FixTcpParameters( host, port, hbi, customLogonMsg, encryption, enableTcpBuffer )
            , defaultApplProtocol_( defaultApplProtocol ) {
        }
    }; // struct FixT11TcpParameters

    /** The FIXT11_TCP underlying protocol backup properties*/
    struct FixT11TcpBackupParameters {
        /** Backup parameters for the underlying protocol*/
        FixT11TcpParameters protocolParamss_;

        /** Backup session properties*/
        const SessionExtraParameters* sessionExtraParams_;

        /** Constructor for backup session-acceptor */
        FixT11TcpBackupParameters( FIXMessage* customLogonMsg = NULL, EncryptMethod encryption = NONE,
                                   bool enableTcpBuffer = true, FIXVersion defaultApplProtocol = FIX50,
                                   const SessionExtraParameters* params = NULL )
            : protocolParamss_( customLogonMsg, encryption, enableTcpBuffer, defaultApplProtocol )
            , sessionExtraParams_( params ) {
        }

        /** Constructor for the backup session-initiator*/
        FixT11TcpBackupParameters( const char* backuphost, int backupport, int hbi = 30, FIXMessage* customLogonMsg = NULL,
                                   EncryptMethod encryption = NONE, bool enableTcpBuffer = true,
                                   FIXVersion defaultApplProtocol = FIX50, const SessionExtraParameters* params = NULL )
            : protocolParamss_( backuphost, backupport, hbi, customLogonMsg, encryption, enableTcpBuffer, defaultApplProtocol )
            , sessionExtraParams_( params ) {
        }
    }; // struct FixT11TcpBackupParameters

	/** Encapsulates the internal state*/
	struct V12_API InternalState {
		virtual ~InternalState() {}
	}; // struct V12_API InternalState

} // namespace Engine

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // B2BITS_ENGINE_SessionParameters_H

