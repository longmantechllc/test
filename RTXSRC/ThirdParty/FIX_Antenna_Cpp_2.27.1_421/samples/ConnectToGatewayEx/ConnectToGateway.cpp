/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
* This application connects to the remote FIX Engine and sends New Order messages.
*/

#include <iostream>
#include <time.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fstream>
#include <list>
#include <algorithm>

#ifndef _WIN32
#   include <pthread.h>
#   include <signal.h>
#   include <set>
#endif // #ifndef _WIN32

#include "B2BITS_V12.h"
#include <B2BITS_Thread.h>

using namespace Engine;
using namespace Utils;
using namespace std;

////////////////////////////////////////////////////////////////////////////////
// Constants.

/// Application required arguments quantity.
const int REQUIRED_ARGS_QTY = 4;

/// Session heart beat interval.
const int HEART_BEAT_INTERVAL = 30;

////////////////////////////////////////////////////////////////////////////////
// Global variables used in this example.

bool g_showMessages = true;

/// Session's sender identifier.
string g_senderCompId;

/// Session's target identifier.
string g_targetCompId;

/// Host address.
string g_host;

/// Port number.
int g_port;

/// The expected MsgSeqNum of the next incoming message.
int g_inSeqNumber( -1 );

/// Session version (default is the oldest).
FIXVersion g_fixVersion( FIX44 );

/// Filename in which example message is stored.
string g_messageFileName;

string g_senderSubId; // SenderSubID (tag = 50)
string g_targetSubId; // TargetSubID (tag = 57)
string g_senderLocationId;
string g_targetLocationId;

int g_msgPackageSize = 100;
int g_msgPackageDelay = 1000;
int g_maxMessagesInBunch = 0;
bool g_tcpBufferDisabled = false;

bool g_interactiveResend = false;
string g_command;
bool g_resendInProgress = false;
System::Semaphore g_resendSemaphore;
bool g_isEngineTerminated = false;
bool g_isFIXTSsnProtocol = false;


////////////////////////////////////////////////////////////////////////////////
/// Application level exception.

class AppException : public std::exception
{

public:
    /// Remembers reason of exception.
    AppException( const string& reason ) throw()
        : reason_( reason ) {}

    /// Destructor to satisfy C++ compliance.
    virtual ~AppException() throw() {}

    /// Reimplements std::exception interface.
    virtual const char* what() const throw() {
        return reason_.c_str();
    }

private:
    std::string reason_;
};

////////////////////////////////////////////////////////////////////////////////
/// Class Helper is a collection of useful routines.

class Helper
{

public:
    /// Returns FIXVersion from it's string representation.
    static FIXVersion string2Version( const string& str );

    /// Retrives required arguments offset from application arguments array.
    static void checkOptionalArgument( char** arguments, int quantity );

    /// Retrives required arguments offset from application arguments array.
    static int findRequiredArguments( char** arguments, int quantity );

    /// Sets default values for required arguments
    static void setReqDefaultValues();

    /// Finds optional argument value.
    static char* findOptionalArgument( const char* name,
                                       char** arguments, int quantity );

    /// Loads file content into std::string.
    static string loadString( const string& fileName );
    static void loadStringSet( const string& fileName, list<string>* strL );

    /// Writes line to console.
    static void writeLineToConsole( const string& message = string() );

    /// Writes line to stderr.
    static void writeErrorLine( const string& message = string() );

    /// screen output synchronizer.
    static System::Mutex outputLock_;

private:
    /// Prohibits class instances appearance.
    Helper() {
        ;
    };
};

// Screen output synchronizer.
System::Mutex Helper::outputLock_;

////////////////////////////////////////////////////////////////////////////////
/// FIX engine EventListener implementation.

class Listener : public Engine::EventListener
{
public:
    /// Processes FIX engine notifications.
    virtual void  onNotification( const Engine::Notification& aNotification );

    /// Processes FIX engine warnings.
    virtual void  onWarning( const Engine::Warning& aWarning );

    /// Processes FIX engine errors.
    virtual void  onError( const Engine::Error& aError );
};

//////////////////////////////////////////////////////////////////////////
///
class SenderThread: public System::Thread
{
public:
    virtual void run() {
        Helper::writeLineToConsole( "\n\n\n\tPress '4' and ENTER to stop the infinitely sending\n\n\n" );
        while ( !stop_ ) {
            for( list<FIXMessage*>::iterator it = pMessageList_.begin(); it != pMessageList_.end(); ++it ) {
                ++messageSended;
                if ( messageSended >= g_msgPackageSize ) {
                    messageSended = 0;
                    Thread::sleep( g_msgPackageDelay );
                }
                pSession_->put( *it );
                // Reports about sent message.
                if( g_showMessages ) {
                    if( 6400 > ( *it )->toString()->size() ) {
                        Helper::writeLineToConsole( "Sent: " + * ( ( *it )->toString() ) + "\n" );
                    } else {
                        Helper::writeLineToConsole( "Sent: Message too big to show it!!!\n" );
                    }
                }
            }
            if( g_showMessages ) {
                Helper::writeLineToConsole( "\n\n\n\tPress '4' and ENTER to stop the infinitely sending\n\n\n" );
            }
        }
    }

    void stop() {
        messageSended = 0;
        stop_ = true;
    }

    SenderThread( list<FIXMessage*> pMessageList, Session* pSession )
        : pMessageList_( pMessageList ), pSession_( pSession ), stop_( false ), messageSended( 0 ) {
    }
private:
    list<FIXMessage*> pMessageList_;
    Session* pSession_;
    bool stop_;
    int messageSended;
};

////////////////////////////////////////////////////////////////////////////////
/// Application implementation (processes the incoming messages).

class MyApp : public Application
{
public:
    MyApp() {
        pT = NULL;
    };

    /// A call-back method to process ingoing messages.
    virtual bool process( const FIXMessage& fixMsg, const Engine::Session& aSn );

    /// Blocks the calling thread until the session is closed.
    void waitUntilClosed();

    /// This call-back method is called to notify about Logon event.
    virtual void onLogonEvent( const LogonEvent* apEvent, const Session& aSn );

    /// This call-back method is called to notify about Logout event.
    virtual void onLogoutEvent( const LogoutEvent* apEvent, const Session& /*aSn*/ );

    virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) {}

    /// This call-back method is called to notify about SequenceGap event.
    virtual void onSequenceGapEvent( const SequenceGapEvent* /*apEvent*/, const Session& /*aSn*/ ) {
        ;
    }

    /// This call-back method is called to notify about SessionLevelReject event.
    virtual void onSessionLevelRejectEvent( const SessionLevelRejectEvent* /*apEvent*/, const Session& /*aSn*/ ) {}

    /**
    * This call-back method is called when a Heartbeat message (MsgType = 0)
    * with the TestReqID field (tag = 112) has been received.
    *
    * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    virtual void onHeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when a Resend Request (MsgType = 2) has been received.
    */
    virtual void onResendRequestEvent( const ResendRequestEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when the session has changed its state.
    */
    virtual void onNewStateEvent( const NewStateEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ ) {
        cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
    }

    /**
    * This call-back method is called when an outgoing message is about to be resent
    * as a reply to the incoming ResendRequest message.
    *
    * If the method returns 'true' then the Engine resends the message to counterpart,
    * otherwise it sends the SequenceReset Gap Fill message.
    *
    * @param msg Outgoing message.
    * @param sn FIX session.
    *
    @return true if the message should be resent, otherwise false.
    */
    virtual bool onResend( const FIXMessage& msg, const Session& sn );

    SenderThread* pT;
private:
    System::Semaphore semaphore_;
};

bool MyApp::onResend( const FIXMessage& msg, const Session& /*sn*/ )
{
    if( ! g_interactiveResend ) {
        return true;
    } else {
        Helper::writeLineToConsole( "Do you want to resend the message '" + *msg.toString() + "' ? (y/n)" );
        g_resendInProgress = true;
        g_resendSemaphore.wait();
        string choice = g_command;
        g_resendInProgress = false;

        if ( "y" == choice || "Y" == choice ) {
            Helper::writeLineToConsole( "The message will be resent." );
            return true;
        } else {
            Helper::writeLineToConsole( "The Sequence Reset Gap Fill message will be sent." );
            return false;
        }
    }
}

void MyApp::onLogoutEvent( const LogoutEvent* /*apEvent*/, const Session& /*aSn*/ )
{
    Helper::writeLineToConsole( "LogoutEvent, the session was terminated.\n" );
    semaphore_.post();
    if ( pT != NULL ) {
        pT->stop();
        pT = NULL;
        Helper::writeLineToConsole( "Please choose command:" );
        Helper::writeLineToConsole( "'1' - to send the message" );
        Helper::writeLineToConsole( "'2' - to close the session" );
        Helper::writeLineToConsole( "'3' - to send infinitely" );
        Helper::writeLineToConsole( "'4' - to stop the infinitely sending" );
    }
}


///////////////////////////////////////////////////////////////////////////////
// Forward definitions.

/// Retrieves global variables values from command line.
void parseCommandLine( char** arguments, int quantity );

/// Writes application usage instructions.
void writeUsageInfo( const string& applicationName );

///////////////////////////////////////////////////////////////////////////////
/// Application entry point.


int main( int argc, char* argv[] )
{

#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // executable name.
    string applicationName( argv[0] );

    try {
        // Fills global variables with command line arguments.
        parseCommandLine( argv + 1, argc - 1 );
    } catch ( const exception& ex ) {

        // Something was wrong in command line arguments.
        Helper::writeErrorLine( "Incorrect arguments: " +
                                string( ex.what() ) + "." );

        // Dumps brief usage description.
        writeUsageInfo( applicationName );

        return -1;
    }

    try {
        // Initializes engine.
        FixEngine::init();

        // Sample message to send to server.
        list<FIXMessage*> pMessageList;

        if ( !g_messageFileName.empty() ) {

            // Load message from file, as specified in command line.
            list<string> rawMessageList;
            Helper::loadStringSet( g_messageFileName, &rawMessageList );

            // Parses raw message with preferred version parameter.

            for( list<string>::iterator it = rawMessageList.begin(); it != rawMessageList.end(); ++it )
                pMessageList.push_back( FIXMsgProcessor::singleton()->parse(
                                            *it,
                                            NULL,
                                            false,
                                            false,
                                            g_isFIXTSsnProtocol ? g_fixVersion : Engine::NA ) );
        } else {

            // Creates 'New Order - Single' message skeleton.
            pMessageList.push_back( FIXMsgFactory::singleton()->newSkel( g_fixVersion, "D" ) );

            char uniqueId[20];

            // Makes unigue identifier.
#if 1400 != _MSC_VER
            sprintf( uniqueId, "%d", ( int )time( NULL ) );
#else
            sprintf_s( uniqueId, 19, "%d", ( int )time( NULL ) );
#endif

            // Sets up values for message.
            pMessageList.front()->set( FIXField::ClOrdID, uniqueId );
            pMessageList.front()->set( FIXField::HandlInst,    "2" );
            pMessageList.front()->set( FIXField::Symbol,     "IBM" );
            pMessageList.front()->set( FIXField::OrderQty,   "200" );
            pMessageList.front()->set( FIXField::Side,         "1" );
            pMessageList.front()->set( FIXField::OrdType,      "5" );
            pMessageList.front()->set( FIXField::TimeInForce,  "0" );

            // 'TransactTime' (tag 60) field is required
            // in FIX 4.2, 4.3 & 4.4 versions.
            if ( FIX42 == g_fixVersion ||
                 FIX43 == g_fixVersion ||
                 FIX44 == g_fixVersion ||
                 FIX50 == g_fixVersion ) {

                pMessageList.front()->set( FIXField::TransactTime,
                                           "20030101-00:00:00" );
            }
        }

        Listener listener;

        // Registers event listener.
        FixEngine::singleton()->registerEventListener( &listener );

        // Application instance.
        MyApp application;

        // Advanced session initialization parameters.
        SessionExtraParameters extraParams;
        if( ! g_senderSubId.empty() ) {
            extraParams.pSenderSubID_ = g_senderSubId.c_str();
        }

        if( ! g_targetSubId.empty() ) {
            extraParams.pTargetSubID_ = g_targetSubId.c_str();
        }

        if( ! g_senderLocationId.empty() ) {
            extraParams.pSenderLocationID_ = g_senderLocationId.c_str();
        }

        if( ! g_targetLocationId.empty() ) {
            extraParams.pTargetLocationID_ = g_targetLocationId.c_str();
        }

        extraParams.maxMessagesAmountInBunch_ = g_maxMessagesInBunch;
        extraParams.disableTCPBuffer_ = g_tcpBufferDisabled;
        extraParams.storageType_ = Engine::persistentMM_storageType;

        // Creates session with appropriate parameters.
        if( NULL == FixEngine::singleton() ) {
            return 1;
        }
        Session* pSession = FixEngine::singleton()->createSession( &application,
                            g_senderCompId, g_targetCompId, g_fixVersion, &extraParams, Engine::default_storageType, ( g_isFIXTSsnProtocol ) ? ( FIXT11_TCP ) : ( FIX_TCP ) );

        // Reports session state.
        Helper::writeLineToConsole( string( "State=" ) +
                                    Session::state2string( pSession->getState() ) );

        if( -1 != g_inSeqNumber ) {
            pSession->setInSeqNum( g_inSeqNumber );
        }

        if( NULL == FixEngine::singleton() ) {
            return 1;
        }
        // Establishes the FIX session as Initiator.
        pSession->connect( HEART_BEAT_INTERVAL, g_host, g_port );

        /*SenderThread**/
        application.pT = NULL;

        // Processes commands.
        do {
            // Writes menu.
            if( ! g_resendInProgress ) {
                Helper::writeLineToConsole( "Please choose command:" );
                Helper::writeLineToConsole( "'1' - to send the message" );
                Helper::writeLineToConsole( "'2' - to close the session" );
                Helper::writeLineToConsole( "'3' - to send infinitely" );
                Helper::writeLineToConsole( "'4' - to stop the infinitely sending" );
            }

            getline( cin, g_command );
            if( ! cin ) {
                return 1;
            }

            if( g_resendInProgress ) {
                if( "y" == g_command || "n" == g_command || "Y" == g_command || "N" == g_command ) {
                    g_resendSemaphore.post();
                } else if( "2" == g_command ) {
                    g_command = "y";
                    g_resendSemaphore.post();
                    break;
                } else {
                    Helper::writeLineToConsole( "Please enter 'y' to resend the message, otherwise - 'n'" );
                }
            } else if ( "1" == g_command ) {
                try {
                    // Sends the given message to the remote FIX engine.
                    if( NULL != FixEngine::singleton() ) {
                        for( list<FIXMessage*>::iterator it = pMessageList.begin(); it != pMessageList.end(); ++it ) {
                            pSession->put( *it );
                            // Reports about sent message.
                            if( g_showMessages ) {
                                if( 6400 > ( *it )->toString()->size() ) {
                                    Helper::writeLineToConsole( "Sent: " + * ( ( *it )->toString() ) + "\n" );
                                } else {
                                    Helper::writeLineToConsole( "Sent: Message too big to show it!!!\n" );
                                }
                            }
                        }
                    }
                } catch( const Utils::Exception& ex ) {
                    Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
                }
            } else if ( "3" == g_command ) {
                try {
                    // Sends the given message to the remote FIX engine.
                    if( NULL != FixEngine::singleton() ) {
                        if ( application.pT == NULL ) {
                            application.pT = new SenderThread( pMessageList, pSession );
                            application.pT->start();
                        }
                    }
                } catch( const Utils::Exception& ex ) {
                    Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
                }
            } else if ( "4" == g_command ) {
                try {
                    if ( application.pT != NULL ) {
                        application.pT->stop();
                        application.pT = NULL;
                        Helper::writeLineToConsole( "Please choose command:" );
                        Helper::writeLineToConsole( "'1' - to send the message" );
                        Helper::writeLineToConsole( "'2' - to close the session" );
                        Helper::writeLineToConsole( "'3' - to send infinitely" );
                        Helper::writeLineToConsole( "'4' - to stop the infinitely sending" );
                    }
                } catch( const Utils::Exception& ex ) {
                    Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
                }
            }
        } while ( "2" != g_command );

        g_interactiveResend = false;

        // Releases the resources used by the given message.
        if( NULL == FixEngine::singleton() ) {
            return 1;
        }
        for( list<FIXMessage*>::iterator it = pMessageList.begin(); it != pMessageList.end(); ++it ) {
            FIXMessage::release( *it );
        }

        Helper::writeLineToConsole( "The session will be closed..." );

        // Closes session with the given logout text.
        if( NULL != FixEngine::singleton() ) {
            pSession->disconnect( "The session was closed by ConnectToGateway", true );
        }
        application.waitUntilClosed();

        // unregisters application before release() call.
        pSession->registerApplication( NULL );
        // Releases the resources used by the given session.
        pSession->release();

        Helper::writeLineToConsole( "Press Enter to exit." );
        cin.get();

        // Unregisters listener before engine destroying.
        if( NULL != FixEngine::singleton() ) {
            FixEngine::singleton()->unregisterEventListener( &listener );
        }
        // Releases engine resources.
        if( NULL != FixEngine::singleton() ) {
            FixEngine::destroy();
        }
    } catch ( const Utils::Exception& ex ) {
        Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
        return -1;
    } catch ( const std::exception& ex ) {
        Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
        return -1;
    }

    return 0;
}

////////////////////////////////////////////////////////////////////////////////
// class MyApp implementation.

// A call-back method to process ingoing messages.
bool MyApp::process( const FIXMessage& msg, const Engine::Session& /*aSn*/ )
{
    if( g_showMessages ) {
        if( 6400 > msg.toString()->size() ) {
            Helper::writeLineToConsole( "Received: " + * ( msg.toString() ) + "\n" );
        } else {
            Helper::writeLineToConsole( "Received: Message too big to show it!!!\n" );
        }
    }
    return true;
}

// This call-back method is called to notify about Logon event.
void MyApp::onLogonEvent( const LogonEvent* apEvent, const Session& /*aSn*/ )
{
    Helper::writeLineToConsole( "LogonEvent, the Logon message was received: " +
                                * ( apEvent->m_pLogonMsg->toString() ) + "\n" );
}

// Blocks the calling thread until the session is closed.
void MyApp::waitUntilClosed()
{
    semaphore_.wait();
}

////////////////////////////////////////////////////////////////////////////////
// class Listener implementation.

// Processes FIX engine notifications.
void  Listener::onNotification( const Engine::Notification& aNotification )
{
    Helper::writeLineToConsole( "Notification: " + *aNotification.what() + "\n" );
    if( 0 == aNotification.what()->compare( "The FIX Engine was stopped." ) ) {
        Helper::writeLineToConsole( "Error: FixAntenna was destroyed - application will be terminated!\n" );
#ifndef _WIN32
        exit( 2 );
#else
        ExitProcess( 2 );
#endif
    }
}

// Processes FIX engine warnings.
void  Listener::onWarning( const Engine::Warning& aWarning )
{
    Helper::writeLineToConsole( "Warning: " + *aWarning.what() + "\n" );
}

// Processes FIX engine errors.
void  Listener::onError( const Engine::Error& aError )
{
    Helper::writeLineToConsole( "Error: " + *aError.what() + "\n" );
}

////////////////////////////////////////////////////////////////////////////////
// class Helper implementation.

// Returns FIXVersion from it's string representation.
FIXVersion Helper::string2Version( const string& str )
{

    if( 0 == str.compare( "FIX40" ) ) {
        return FIX40;
    }
    if( 0 == str.compare( "FIX41" ) ) {
        return FIX41;
    }
    if( 0 == str.compare( "FIX42" ) ) {
        return FIX42;
    }
    if( 0 == str.compare( "FIX43" ) ) {
        return FIX43;
    }
    if( 0 == str.compare( "FIX44" ) ) {
        return FIX44;
    }
    if( 0 == str.compare( "FIX50" ) ) {
        return FIX50;
    }
    if( 0 == str.compare( "FIX50SP1" ) ) {
        return FIX50SP1;
    }
    if( 0 == str.compare( "FIX50SP2" ) ) {
        return FIX50SP2;
    }

    return NA;
}

// Retrives required arguments offset from application arguments array.
int Helper::findRequiredArguments( char** arguments, int quantity )
{

    int argumentsOffset = 0;

    // Skips optional arguments.
    while ( argumentsOffset < quantity &&
            '-' == *arguments[argumentsOffset] ) {
        argumentsOffset += 2;
    }
    // Reports error if offset not found.
    if ( quantity - REQUIRED_ARGS_QTY != argumentsOffset ) {
        //throw AppException("Requied arguments not found");
        setReqDefaultValues();
        return -1;
    }
    // Returns found index.
    return argumentsOffset;
}

// Check passed optional arguments for validness
void Helper::checkOptionalArgument( char** arguments, int quantity )
{
    typedef std::set< std::string > Strings;
    Strings args;
    args.insert( "msgFile" );
    args.insert( "version" );
    args.insert( "inSeqNumber" );
    args.insert( "senderSubId" );
    args.insert( "targetSubId" );
    args.insert( "senderLocationId" );
    args.insert( "targetLocationId" );
    args.insert( "interactiveResend" );
    args.insert( "showMessages" );
    args.insert( "msgPackageSize" );
    args.insert( "msgPackageDelay" );
    args.insert( "tcpBufferDisabled" );
    args.insert( "maxMessagesInBunch" );
    args.insert( "sessionProtocol" );

    for( int i( 0 ); i < quantity; ++i ) {
        if( '-' != arguments[i][0] ) {
            continue;
        }
        Strings::iterator it = args.find( arguments[i] + 1 );
        if( args.end() == it ) {
            throw AppException( string( "Unknown optional input parameter '" ) + arguments[i] + "'" );
        }
    }
}

// Finds optional argument value.
char* Helper::findOptionalArgument( const char* name,  char** arguments, int quantity )
{

    // iterates through all arguments.
    for ( int index = 0; index < quantity; ++index ) {

        // option value resides in the next argument.
        if ( '-' == *arguments[index] &&
             0   == strcmp( name, arguments[index] + 1 ) &&
             index + 1 < quantity ) {

            return arguments[index + 1];
        }
    }
    // Returns nothing.
    return NULL;
}

// Loads file content into std::string.
string Helper::loadString( const string& fileName )
{

    struct stat statistics;

    // Retrieves file size.
    if ( -1 == stat( fileName.c_str(), &statistics ) ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }

    // Opens file.
    ifstream ifs( fileName.c_str(), ios::binary );

    if ( !ifs.good() ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }

    string str;

    // Resize string buffer to load file content.
    str.resize( statistics.st_size );

    // Reads file content.
    ifs.read( const_cast<char*>( str.c_str() ),
              statistics.st_size );

    ifs.close();

    return str;
}

// Loads file content into std::string.
void Helper::loadStringSet( const string& fileName, list<string>* strL )
{

    struct stat statistics;

    // Retrieves file size.
    if ( -1 == stat( fileName.c_str(), &statistics ) ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }

    // Opens file.
    ifstream ifs( fileName.c_str(), ios::binary );

    if ( !ifs.good() ) {
#if 1400 != _MSC_VER
        throw AppException( strerror( errno ) );
#else
        char buf[1024];
        strerror_s( buf, 1023, errno );
        throw AppException( buf );
#endif
    }


    while ( !ifs.eof() ) {
        string tmp_ln;
        getline( ifs, tmp_ln );
        if ( !tmp_ln.empty() ) {
            string::size_type beg = tmp_ln.find( "\x01" "9=" );
            if ( beg == string::npos ) {
                throw Exception( "Must be size (tag 9) in message" );
            }
            string::size_type end = tmp_ln.find( "\x01", beg + 3 );
            if ( end == string::npos ) {
                throw Exception( "Must be size (tag 9) in message" );
            }
            string msg_size = tmp_ln.substr( beg + 3, end - beg - 3 );
            char* errPos = NULL;
            size_t size = strtol( msg_size.c_str(), &errPos, 10 );
            while ( size > tmp_ln.size() ) {
                if ( ifs.eof() ) {
                    throw Exception( "Unexpected end of file" );
                }
                string tmp_ln2;
                char buf[10 * 1024];
                buf[0] = 10;
                ifs.read( &buf[1], size - tmp_ln.size() );
                tmp_ln += string( &buf[0], size - tmp_ln.size() + 1 );
                tmp_ln2.clear();
                getline( ifs, tmp_ln2 );
                tmp_ln += tmp_ln2;
            }
            if ( *tmp_ln.rbegin() == '\r' ) {
                tmp_ln.erase( tmp_ln.size() - 1 );
            }
            strL->push_back( tmp_ln );
        }
    }
    ifs.close();
}

// Writes message to console.
void Helper::writeLineToConsole( const string& message )
{
    outputLock_.lock();
    clog << message << endl;
    outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine( const string& message )
{
    cerr << message << endl;
}

// Sets default values for required arguments
void Helper::setReqDefaultValues()
{
    g_senderCompId = "CTGX";
    g_targetCompId = "Acceptor";
    g_host = "localhost";
    g_port = 5555;
}

///////////////////////////////////////////////////////////////////////////////
// Retrieves global variables values from command line.

void parseCommandLine( char** arguments, int quantity )
{

    // Checks for option arguments correctness
    Helper::checkOptionalArgument( arguments, quantity );

    // Tries to retrieve application required arguments.
    int reqArgsPos = Helper::findRequiredArguments( arguments, quantity );

    if ( reqArgsPos != -1 ) {
        // Fills corresponding variables.
        g_senderCompId = arguments[reqArgsPos    ];
        g_targetCompId = arguments[reqArgsPos + 1];

        g_host = arguments[reqArgsPos + 2];
        g_port = atoi( arguments[reqArgsPos + 3] );
    }

    // Retrieves optional sequence number parameter.
    char* param = Helper::findOptionalArgument( "inSeqNumber", arguments, quantity );
    if ( NULL != param ) {
        g_inSeqNumber = atoi( param );
    }

    // Retrieves optional filename in which example message is stored.
    param = Helper::findOptionalArgument( "msgFile", arguments, quantity );
    if ( NULL != param ) {
        g_messageFileName = param;
    }


    param = Helper::findOptionalArgument( "msgPackageSize", arguments, quantity );
    if ( NULL != param ) {
        // Convers string into FIXVersion value.
        char* errPtr = NULL;
        g_msgPackageSize = strtol( param, &errPtr, 10 );

        if ( ( param + strlen( param ) != errPtr ) || ( g_msgPackageSize < 0 ) ) {
            throw AppException( "Invalid msgPackageSize value: '" + string( param ) + "'" );
        }
    }

    param = Helper::findOptionalArgument( "msgPackageDelay", arguments, quantity );
    if ( NULL != param ) {
        // Convers string into FIXVersion value.
        char* errPtr = NULL;
        g_msgPackageDelay = strtol( param, &errPtr, 10 );

        if ( ( param + strlen( param ) != errPtr ) || ( g_msgPackageDelay < 0 ) ) {
            throw AppException( "Invalid msgPackageDelay value: '" + string( param ) + "'" );
        }
    }

    param = Helper::findOptionalArgument( "tcpBufferDisabled", arguments, quantity );
    if ( NULL != param ) {
        if( 0 == strcmp( "true", param ) ) {
            g_tcpBufferDisabled = true;
        } else if( 0 == strcmp( "false", param ) ) {
            g_tcpBufferDisabled = false;
        } else {
            throw AppException( "Unknown value (" + string( param ) + ") of the tcpBufferDisabled parameter" );
        }
    }

    param = Helper::findOptionalArgument( "maxMessagesInBunch", arguments, quantity );
    if ( NULL != param ) {
        char* errPtr = NULL;
        g_maxMessagesInBunch = strtol( param, &errPtr, 10 );
    }

    param = Helper::findOptionalArgument( "sessionProtocol", arguments, quantity );
    if ( NULL != param ) {
        g_isFIXTSsnProtocol = ( 0 == strcmp( "FIXT11", param ) );
    }


    // Retirves optional session version parameter.
    param = Helper::findOptionalArgument( "version", arguments, quantity );

    if ( NULL != param ) {
        // Convers string into FIXVersion value.
        g_fixVersion = Helper::string2Version( param );

        if ( NA == g_fixVersion ) {
            // unknown version was specified, throws exception.
            throw AppException( "Unknown FIX version specified '" + string( param ) + "'" );
        }
    }

    param = Helper::findOptionalArgument( "senderSubId", arguments, quantity );
    if ( NULL != param ) {
        g_senderSubId = param;
    }

    param = Helper::findOptionalArgument( "targetSubId", arguments, quantity );
    if ( NULL != param ) {
        g_targetSubId = param;
    }

    param = Helper::findOptionalArgument( "senderLocationId", arguments, quantity );
    if ( NULL != param ) {
        g_senderLocationId = param;
    }

    param = Helper::findOptionalArgument( "targetLocationId", arguments, quantity );
    if ( NULL != param ) {
        g_targetLocationId = param;
    }

    param = Helper::findOptionalArgument( "interactiveResend", arguments, quantity );
    if ( NULL != param ) {
        if( 0 == strcmp( "true", param ) ) {
            g_interactiveResend = true;
        } else if( 0 == strcmp( "false", param ) ) {
            g_interactiveResend = false;
        } else {
            throw AppException( "Unknown value (" + string( param ) + ") of the interactiveResend parameter" );
        }
    }
    param = Helper::findOptionalArgument( "showMessages", arguments, quantity );
    if ( NULL != param ) {
        if( 0 == strcmp( "true", param ) ) {
            g_showMessages = true;
        } else if( 0 == strcmp( "false", param ) ) {
            g_showMessages = false;
        } else {
            throw AppException( "Unknown value (" + string( param ) + ") of the showMessages parameter" );
        }
    }

}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void writeUsageInfo( const string& applicationName )
{

    Helper::writeErrorLine();
    Helper::writeErrorLine( "Usage: " + applicationName +
                            " {-optionalParameterName Value} SenderCompId TargetCompId Host Port" );

    Helper::writeErrorLine();
    Helper::writeErrorLine( "Optional parameters:" );
    Helper::writeErrorLine();
    Helper::writeErrorLine( "  msgFile      Name of file in which example message is stored." );
    Helper::writeErrorLine( "  version      Session's version. " );
    Helper::writeErrorLine( "  inSeqNumber  The expected MsgSeqNum of the next incoming message." );
    Helper::writeErrorLine( "  senderSubId  SenderSubID (tag = 50)." );
    Helper::writeErrorLine( "  targetSubId  TargetSubID (tag = 57)." );
    Helper::writeErrorLine( "  senderLocationId  SenderLocationID (tag = 142)." );
    Helper::writeErrorLine( "  targetLocationId  TargetLocationID (tag = 143)." );
    Helper::writeErrorLine( "  interactiveResend (true/false) If 'true' then the resending of the messages will be interactive" );
    Helper::writeErrorLine( "  showMessages (true/false) If 'true' then sent and received messages will be showed at console" );
    Helper::writeErrorLine( "  msgPackageSize  Number of messages for one package sending" );
    Helper::writeErrorLine( "  msgPackageDelay  Delay between packages (msec)" );
    Helper::writeErrorLine( "  tcpBufferDisabled  When true - TCP buffer is disabled." );
    Helper::writeErrorLine( "  maxMessagesInBunch  Max amount joined outgoing messages." );
    Helper::writeErrorLine( "  sessionProtocol type of the session protocol ('FIX' or 'FIXT11')." );

    Helper::writeErrorLine();
}

////////////////////////////////////////////////////////////////////////////////
// End of file.
