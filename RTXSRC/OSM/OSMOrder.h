#pragma once
#include <vector>
#include <unordered_map>
#include "Price.h"
//#include <FieldTypes.h>

struct TagValue {
    int tag;
    std::string value;
};
struct VirtualDest {
    std::string dest;
    std::vector<TagValue> vec;
};

using VirtualDestMap = std::unordered_map<std::string, VirtualDest>;

struct OSMOrder {
    int pOrderId{};
    int side{};
    int qty{};
    int newQty{};
    int filledQty{};
    int lastQty{};
    int accumQty{};
    int leaveQty{};
    int tif{};
    int displaySize{};      // tag 111 MaxFloor
    int minQty{0};         //tag 110
    int type{};
    int status {-1};
    int refOrderId{};
    Price price{};
    Price lastPx{};
    Price avgPx {};
    Price newPx{};
    Price discretion{0.0};    // tag 389
    //time_t  expirationTime{};	// tag 126
    long expirationTime{};	// tag 126
    //FIX::UtcTimeStamp expirationTime{};	// tag 126

    mutable time_t transactTime{};
    //mutable FIX::UtcTimeStamp transactTime{};

    std::string symbol;
    std::string childOrderId;
    std::string clOrdId;
    std::string oClOrdId;
    std::string exOrderId;
    std::string exchange;   // tag 100, only for child order
    std::string account;
    std::string subAccount;
    std::string logInId;
    std::string  execInst;      // tag 18
    bool cxlReplaced {};
};

struct OSMRecoveredOrder {
    int pOrderId{};
    int side{};
    int qty{};
    int accumQty{};
    int tif{};
    int displaySize{};
    int minQty{0};
    int type{};
    int status {-1};
    int refOrderId{};
    double price{};
    double avgPx {};
    double discretion{0.0};
    //time_t expirationTime{};
    long expirationTime{};

    std::string symbol;
    std::string clOrdId;
    std::string execInst;
    std::string desk;
    std::string user;
};

struct RecoveredGWOrder{
    int pOrderId;
    std::string childOrderId;
    std::string venue;
};
