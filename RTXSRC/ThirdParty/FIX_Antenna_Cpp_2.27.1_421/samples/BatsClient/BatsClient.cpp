// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#include "BatsClient.h"

#include <iostream>
#include <iomanip>
#include <memory>
#include <sstream>
#include <stdexcept>

#include <B2BITS_FixEngine.h>
#include "InstrListener.h"

using namespace Bats;

#ifdef _WIN32
#include <conio.h>
#else
#include <ncurses.h>
#include <unistd.h>
#endif

/* -------------------------------------------------------------------------------------/
    helpers
*/
struct _setw
{
    mutable std::stringstream sstr_;    
    const unsigned w_;
    _setw(unsigned w) : w_(w) {}
};

const _setw& operator<<(std::ostream& /*os*/, const _setw& sw) {
    return sw;
}

// Console
#ifdef _WIN32
struct Console
{
    struct _endl {
        friend std::ostream& operator<<(std::ostream& os, const _endl& e);
    };

    static void init() {}
    static void destroy() {}

    static HANDLE handle() {
        static HANDLE h = GetStdHandle(STD_OUTPUT_HANDLE);
        return h;
    }
    static _endl endl() {
        return _endl();
    }
    static void clrscr()
    {
        COORD c = {0, 0};
        SetConsoleCursorPosition(handle(), c);
    }
    static void show_cursor(bool show)
    {
        CONSOLE_CURSOR_INFO info = {10, show};
        SetConsoleCursorInfo(handle(),&info);
    }
    static void cleanup_screen() 
    {
        CONSOLE_SCREEN_BUFFER_INFO info;
        GetConsoleScreenBufferInfo(handle(), &info);
        COORD currentPos = info.dwCursorPosition;
        SHORT sizeY = info.srWindow.Bottom;
        if (sizeY > currentPos.Y) {
            for(SHORT i = 0; i < sizeY - currentPos.Y; ++i)
                cout() << endl();    // Console namespace
        }
        SetConsoleCursorPosition(handle(), currentPos);
    }

    static int getchar(bool block = false)
    {
        if (block)
            return _getch();

        if (_kbhit())
            return _getch();
        return -1;
    }

    static std::ostream& cout() { return std::cout; }
    static std::istream& cin() { return std::cin; }
};

std::ostream& operator <<(std::ostream& os, const Console::_endl&) {
    CONSOLE_SCREEN_BUFFER_INFO info;
    GetConsoleScreenBufferInfo(Console::handle(), &info);
    os << std::string(info.dwSize.X - info.dwCursorPosition.X, ' ');
    return os;
}
#else
struct Console
{
    struct _endl {
        friend std::ostream& operator<<(std::ostream& os, const _endl& e);
    };

    static void init() 
    {
        initscr();
        noecho();
        nodelay(stdscr, true);
    }
    static void destroy() 
    {
        endwin();
    }

    static _endl endl() {
        return _endl();
    }
    static void clrscr()
    {
        move(0, 0);
    }
    static void show_cursor(bool show)
    {
        curs_set(show ? 1 : 0);
    }
    static void cleanup_screen() 
    {
        clrtobot();
        refresh();
    }

    static int getchar(bool block = false)
    {
        if (block) {
            nodelay(stdscr, false);
            int rc = getch();
            nodelay(stdscr, true);
            return rc;
        }

        return getch();
    }

    static std::ostream& cout() { 
        static std::stringstream coutstr(std::ios_base::out);
        return coutstr; 
    }
    static std::istream& cin() {
        static std::stringstream cinstr(std::ios_base::in);
        static bool firstTime = true;
        
        std::stringstream& cout = static_cast<std::stringstream&>(Console::cout());
        printw(cout.str().c_str());
        cout.str("");
        refresh();
        
        nodelay(stdscr, false);
        echo();
        
        if (firstTime || !cinstr.good())
        {
            firstTime  = false;
            
            char buffer[64];
            getnstr(buffer, 64);
            cinstr.str(buffer);
            cinstr.clear(std::ios_base::goodbit);
        }
        
        nodelay(stdscr, true);        
        noecho();

        return cinstr;
    }
};

std::ostream& operator <<(std::ostream& os, const Console::_endl&) {
    std::stringstream& cout = static_cast<std::stringstream&>(os);
    printw(cout.str().c_str());
    clrtoeol();
    printw("\n");
    cout.str("");
    return os;
}
#endif  //  WIN32

template<class T>
std::ostream& operator<<(const _setw& os, const T& v) {
    os.sstr_.precision(std::cout.precision());
    os.sstr_.flags(std::cout.flags());
    os.sstr_ << v; 
    std::string out = os.sstr_.str();
    if (os.w_ > out.size() ) {
        size_t w1 = (os.w_ - out.size()) / 2;
        size_t w2 = (os.w_ - out.size()) - w1;
        if (!(os.sstr_.flags() & std::ios::left)) std::swap(w1, w2);
        return Console::cout() << std::string(w1, std::cout.fill()) << out << std::string(w2, std::cout.fill());
    }
    return Console::cout() << out;
}

/* -------------------------------------------------------------------------------------/
    BatsClient class
*/

BatsClient::~BatsClient() {
    if (application_) {
        std::cout << "+++ > Destroying BATS application" << std::endl;
        application_->release();
    }
}
/* -------------------------------------------------------------------------------------/
    on key pressed
*/
bool BatsClient::onKeyPressed(int key)
{
    Console::show_cursor(true);
    Console::cout() << Console::endl();

    if (key == 'q')
        return false;
    
    try
    {
        if (key == 's') //  subscribe
        {
            std::string channel;
            std::string symbol;
            System::u16 depth; 
            
            Console::cout() << "Enter channel id: ";
            Console::cin() >> channel;
            
            Console::cout() << "Enter symbol to subscribe: ";
            Console::cin() >> symbol;
            
            Console::cout() << "Enter depth: ";
            Console::cin() >> depth;                        

            UnitService* svc = application_->getService(channel, symbol, "", "");
            if (!svc)
                throw std::runtime_error("Cannot find service for symbol");

            std::auto_ptr<InstrListener> listener(new InstrListener(this, svc, symbol, depth));
            svc->subscribeToInstrument(symbol, depth, listener.get(), false);
            {
                guard_type guard(instrMutex_);
                instrListeners_.push_back(listener.release());
            }
        }
        else
        if (key == 'u') //  unsubscribe
        {
            unsigned index;
            Console::cout() << "Enter subscribtion index: ";
            Console::cin() >> index;

            InstrListener* l = NULL;
            {
                guard_type guard(instrMutex_);
                if (index >= instrListeners_.size())
                    throw std::runtime_error("Wrong index");
                l = instrListeners_[index];
            }
            l->getService()->unsubscribeFromInstrument(l->getSymbol());
        }
        else
        if (key == 'b') // set book idnex
        {
            unsigned index;
            Console::cout() << "Enter instrument index: ";
            Console::cin() >> index;
            {
                guard_type guard(instrMutex_);
                if (index >= instrListeners_.size())
                    throw std::runtime_error("Wrong index");
            }

            obIndex_ = index;
        }
        else
        if (key == 'n') // create new
        {
            std::string channel;
            unsigned unit;
            Console::cout() << "Enter channel id: ";
            Console::cin() >> channel;
            Console::cout() << "Enter unit id: ";
            Console::cin() >> unit;

            application_->createService(channel, System::u8(unit), std::string());
        }
        else
        if (key == 'c' || key == 'd' || key == 'r') // connect/disconnect/remove
        {
            unsigned index;
            Console::cout() << "Enter service index: ";
            Console::cin() >> index;

            UnitService* svc = NULL;
            {
                guard_type guard(unitsMutex_);
                unsigned i = 0;
                unit_listener_map::iterator it = unitsMap_.begin();
                while(it != unitsMap_.end() && i < index ) { ++i;++it; }

                if (i != index || it == unitsMap_.end())
                    throw std::runtime_error("Wrong index");

                svc = it->first;
                if (key == 'c') {
                    if (it->second)
                        svc->connect(it->second);
                    else {
                        UnitListener* listener = new UnitListener(this);
                        unitsMap_[svc] = listener;
                        svc->connect(listener);
                    }
                }
            }
            assert(svc);
            if (key == 'r') {
                application_->removeService(svc->getChannelID(), svc->getUnitID());
            }
            else
            if (key == 'd')
                svc->disconnect();
        }
    }
    catch(std::exception& ex) {
        Console::cout() << "Failed: " << ex.what() << Console::endl();
        Console::cout() << "Press any key";
        Console::getchar(true);
    }
    Console::show_cursor(false);
    return true;
}

/* -------------------------------------------------------------------------------------/
    main event loop
*/
void BatsClient::run(Bats::ApplicationOptions options)
{
    Console::cout() << "+++ > Creating BATS Bats::Application...";
    application_ = Engine::FixEngine::singleton()->createBatsApplication(options, &appListener_);
    Console::cout() << "ok\n";

    Console::cout() << "+++ > Creating services...";
    application_->createServices(std::string());
    Console::cout() << "ok\n";
    Console::cout() << std::left;

    Console::init();
    Console::show_cursor(false);

    try 
    {
        bool update_scr = true;
    
        while(1) 
        {
            if (update_scr)
            {
                Console::clrscr();

                // print units
                {
                guard_type guard(unitsMutex_);
                if (!unitsMap_.empty()) 
                {
                    Console::cout() << "Threads: I=" << appListener_.incThreads_ << ", W=" << appListener_.workerThreads_ << Console::endl();
                    Console::cout() << "--------------------------       Services       -------------------------" << Console::endl();
                    Console::cout() << "N     channel    unit    connected    linkUP        msgs        seqUnits " << Console::endl();
                    Console::cout() << "-------------------------------------------------------------------------" << Console::endl();

                    unsigned i = 0;

                    for(unit_listener_map::const_iterator it = unitsMap_.begin(); it != unitsMap_.end(); ++it)
                    {
                        UnitService* svc = it->first;
                        Console::cout() << _setw(2) << i++ << "    " << _setw(7) << svc->getChannelID() << "    " << _setw(4) << int(svc->getUnitID());

                        Console::cout() << "    " << _setw(9) << ((it->second && it->second->isConnected()) ? "yes" : "no");

                        if (it->second) 
                        {
                            Console::cout() << "    " << _setw(6) << it->second->isLinkUp();
                            Console::cout() << "    " << _setw(12) << it->second->totalMessages();
                            Console::cout() << "    " << _setw(12) << it->second->totalUnits() << " ";
                            std::deque<std::string> lns = it->second->lastNotifications();
                            for (size_t k = 0; k < lns.size(); ++k) Console::cout() << ">" << lns[k];
                        }
                        Console::cout() << Console::endl();
                    }
                }
                }
                // print instruments
                {
                guard_type guard(instrMutex_);
                if(instrListeners_.size())
                {
                    Console::cout() << Console::endl();
                    Console::cout() << Console::endl();
                    Console::cout() << "------------------------       Instruments       ------------------------" << Console::endl();
                    Console::cout() << "N     symbol    channel    unit    depth" << Console::endl();
                    Console::cout() << "-------------------------------------------------------------------------" << Console::endl();
                    for(unsigned i = 0; i < instrListeners_.size(); ++i) 
                    {
                        InstrListener* l = instrListeners_[i];
                        Console::cout() << _setw(2) << i << "    " << _setw(6) << l->getSymbol() << "    " << _setw(7) << l->getService()->getChannelID();
                        Console::cout() << "    " << _setw(4) << int(l->getService()->getUnitID()) << "    " << _setw(5) << l->getDepth() << Console::endl();
                    }
                    printBook();
                }
                }

                Console::cout() << Console::endl() << Console::endl();
                Console::cout() << ":" << Console::endl();
                Console::cout() << "'n': " << "Create unit service" << Console::endl();
                Console::cout() << "'c': " << "Connect unit service" << Console::endl();
                Console::cout() << "'d': " << "Disconnect unit service" << Console::endl();
                Console::cout() << "'r': " << "Remove unit service" << Console::endl();
                Console::cout() << "'s': " << "Subscribe to instrument" << Console::endl();
                Console::cout() << "'u': " << "Unsubscribe from instrument" << Console::endl();
                Console::cout() << "'b': " << "Choose order book" << Console::endl();
                Console::cout() << "'q': "  << ". Exit" << Console::endl();

                Console::cleanup_screen();
            }

            try {
                update_scr = event_.wait(100);
            }
            catch(...) {
                update_scr = false;
            }

            int key = Console::getchar();
            if (key != -1) {
                update_scr = true;
                if (!onKeyPressed(key))
                    break;
            }
        }
    }
    catch(...)
    {
        Console::show_cursor(true);
        Console::destroy();

        throw;
    }
    Console::show_cursor(true);
    Console::destroy();
}

/* -------------------------------------------------------------------------------------/
    print orderbook
*/
void BatsClient::printBook()
{
    guard_type guard(instrMutex_);
    assert(obIndex_ < instrListeners_.size());
    InstrListener* l = instrListeners_[obIndex_];

    Console::cout() << Console::endl() << Console::endl();
    Console::cout() << "----------------------------  OrderBook( #" << _setw(2) << obIndex_ << ") --------------------------" << Console::endl();
    Console::cout() << "                 Bid               |                Ask                 " << Console::endl();
    Console::cout() << "------------------------------------------------------------------------" << Console::endl();
    Console::cout() << "    Ord    |    Qty    |    Px     |    Px     |    Qty    |    Ord     " << Console::endl();
    Console::cout() << "------------------------------------------------------------------------" << Console::endl();

    std::vector<DepthItem> askBook = l->getBook('S');
    std::vector<DepthItem> bidBook = l->getBook('B');
    for( size_t i = 0; i < l->getDepth(); ++i ) 
    {
        Console::cout() << _setw(11) << bidBook[i].orders << " ";
        Console::cout() << _setw(11) << bidBook[i].qty << " ";
        Console::cout() << _setw(11) << bidBook[i].price << " ";
        Console::cout() << _setw(11) << askBook[i].price << " ";
        Console::cout() << _setw(11) << askBook[i].qty << " ";
        Console::cout() << _setw(11) <<askBook[i].orders << Console::endl();
    }
    Console::cout() << "------------------------------------------------------------------------" << Console::endl();
}

/* -------------------------------------------------------------------------------------/
    misc events
*/
void BatsClient::onUnsubscribed(InstrListener* l)
{
    guard_type guard(instrMutex_);
    obIndex_ = 0;
    instr_listener_vector::iterator it = std::find(instrListeners_.begin(), instrListeners_.end(), l);
    if (it != instrListeners_.end() ) instrListeners_.erase(it);
    notify();
}

void BatsClient::onServiceAdded(UnitService* service)
{
    guard_type guard(unitsMutex_);
    assert(unitsMap_.find(service) == unitsMap_.end());
    unitsMap_[service] = NULL;
    notify();
}

void BatsClient::onServiceRemoving(UnitService* service)
{
    guard_type guard(unitsMutex_);
    unit_listener_map::iterator it = unitsMap_.find(service);
    if (it != unitsMap_.end()) {
        delete it->second;
        unitsMap_.erase(it);
    }
    notify();
}
