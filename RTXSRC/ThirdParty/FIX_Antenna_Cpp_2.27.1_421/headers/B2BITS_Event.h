// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Event.h
/// Contains Engine::Event classes declaration.

#ifndef _B2BITS_Event_h__
#define _B2BITS_Event_h__

#include <string>
#include <B2BITS_CompilerDefines.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_SessionId.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
    // A keyword was used that is not in the C++ standard, for example, one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
    // XML document comment target: badly-formed XML: reason
#   pragma warning(disable: 4635)
#endif

namespace Engine
{
    class Session;

    /**
     * Generic event.
     */
    class Event
    {
    public:
        /**
         * Describes the event.
         */
        virtual const std::string* what() const = 0;

        /** Event types. */
        enum Type {
            EVENT,          ///< Generic event
            ERROR_EVENT,    ///< Generic error
            NOTIFICATION,   ///< Generic error
            WARNING,        ///< Generic warning
            SESSION_ERROR,  ///< Generic Session error
            SESSION_LOGON_ERROR,            ///< Error while session logon
            PARSE_ERROR_IN_SESSION,         ///< Error while parsing
            NEW_INCOMING_CONNECTION,        ///< New incoming connection
            NEW_INCOMING_CONNECTION_ERROR,  ///< New incoming connection error
            SESSION_NOTIFICATION,           ///< Generic Session notification
            SESSION_WAS_REJECTED,           ///< Session was rejected event
            SESSION_WAS_CREATED,            ///< Session was closed event
            SESSION_WAS_CONNECTED,          ///< Session was connected event
            SESSION_WAS_CLOSED,             ///< Session was closed event
            SESSION_WAS_ESTABLISHED,        ///< Session was established event
            SESSION_WAS_RESTORED,           ///< Session was restored after disconnect event
            SYNCHRONIZATION_COMPLETE,       ///< Synchronization complete event
            SESSION_WARNING,                ///< Generic Session warning
            LINK_ERROR_WAS_DETECTED,        ///< Link error was detecetd error
            LINK_WAS_RESTORED,              ///< Link was restored event
            PERSISTENT_DATA_WERE_FOUND,     ///< Persistent data were found
            DEBUG_MESSAGE,                  ///< Debug message
            SESSION_DEBUG_MESSAGE           ///< Session debug message
        };

        /**
         * Returns the event type. Can be used instead of C++ RTTI.
         */
        virtual Type getType() const = 0;

    protected:
        /** Destructor. */
        virtual ~Event() {};
    };

    /**
    * Represents notification event.
    *
    * @see NotificationsMgr
    */
    class Notification : public Event
    {
    public:
        /** Constructor.
         *
         * @param msg std::string that describes the event.
         */
        Notification( const std::string& msg );

        /** Destructor. */
        virtual ~Notification() {
            ;
        }

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return NOTIFICATION;
        }

    protected:
        /// Contains std::string that describes the event
        std::string m_reason;
    };

    /**
    * Represents warning event.
    *
    * @see NotificationsMgr
    */
    class Warning : public Event
    {
    public:
        /** Constructor.
         *
         * @param msg std::string that describes the event.
         */
        Warning( const std::string& msg );

        /// Destructor
        virtual ~Warning() {
            ;
        }

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return WARNING;
        }

    protected:
        /// Contains std::string that describes the event
        std::string m_reason;
    };

    /**
     * Represents error event.
     *
     * @see NotificationsMgr
     */
    class Error : public Event
    {
    public:
        /** Constructor.
         *
         * @param msg std::string that describes the event.
         */
        Error( const std::string& msg );

        /// Destructor
        virtual ~Error() {}

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return ERROR_EVENT;
        }

    protected:
        /// Contains std::string that describes the event
        std::string m_reason;
    };

    /**
     * Represents debug message.
     *
     * @see NotificationsMgr
     */
    class DebugMessage : public Event
    {
    public:
        /** Constructor.
         *
         * @param msg std::string that describes the event.
         */
        DebugMessage( const std::string& msg );

        /// Destructor
        virtual ~DebugMessage() {}

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return DEBUG_MESSAGE;
        }

    protected:
        /// Contains std::string that describes the event
        std::string m_reason;
    };

    /**
     * Basic Event Listener.
     */
    class EventListener
    {
    public:
        /** This method is called when the FixEngine generates a notification.
        *
        * @param notification
        */
        virtual void onNotification( const Engine::Notification& notification ) = 0;

        /** This method is called when the FixEngine generates a warning. */
        virtual void onWarning( const Engine::Warning& warning ) = 0;

        /** This method is called when the FixEngine reports an error. */
        virtual void onError( const Engine::Error& error ) = 0;

        /** This method is called when the FixEngine reports an error. */
        virtual void onDebug( const Engine::DebugMessage& error ) {
            B2B_USE_ARG( error );
        }

        /** Destructor. */
        virtual ~EventListener() {};
    };

    /** New Incoming Connection. */
    class NewIncomingConnection B2B_SEALED : public Notification
    {
    public:
        /** Constructor.
         *
         * @param remoteAddress Remote host address.
         * @param remotePort Remote port.
         * @param localAddress Local host address.
         * @param localPort Local port.

         */
        NewIncomingConnection( const std::string& remoteAddress, int remotePort, const std::string& localAddress, int localPort );

        /// Destructor
        virtual ~NewIncomingConnection() {
            ;
        }

        virtual Type getType() const B2B_OVERRIDE {
            return NEW_INCOMING_CONNECTION;
        }
    };


    /** New Incoming Connection Error. */
    class NewIncomingConnectionError B2B_SEALED : public Error
    {
    public:
        /** Constructor.
         *
         * @param remoteAddress Remote host address.
         * @param remotePort Remote port.
         * @param localAddress Local host address.
         * @param localPort Local port.
         * @param msg std::string that describes the event.
         */
        NewIncomingConnectionError( const std::string& remoteAddress, int remotePort, const std::string& localAddress, int localPort, const std::string& msg );

        /// Destructor
        virtual ~NewIncomingConnectionError() {}

        virtual Type getType() const B2B_OVERRIDE {
            return NEW_INCOMING_CONNECTION_ERROR;
        }
    };


    class Session;
    class Fast2FixSession;
    /**
     * Represents notification event related to Session.
     */
    class SessionNotification : public Notification
    {
    public:
        /** Constructor */
        SessionNotification( const Session* pSn );

        /** Constructor */
        SessionNotification( const Session* pSn, const std::string& reason );

        SessionNotification( const SessionId& sessionId, const std::string& reason );

        /** Destructor. */
        virtual ~SessionNotification() {};

        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_NOTIFICATION;
        }

        /** Returns session's SenderCompID. */
        const std::string* getSenderCompID() const {
            return &sessionId_.sender_;
        }

        /** Returns session's TargetCompID. */
        const std::string* getTargetCompID() const {
            return &sessionId_.target_;
        }

        /** Returns session identifier. */
        const SessionId& getSessionId() const {
            return sessionId_;
        }
    protected:
        SessionId sessionId_;
    };

    /** This event is fired when the persistent information
     * about session is found.
     *
     */
    class PersistentDataWereFound B2B_SEALED : public SessionNotification
    {
    public:
        /// Constructor
        /// @param session Instance of Session assigned event with.
        PersistentDataWereFound( const Session* session );
        PersistentDataWereFound( const SessionId& sessionId );


        virtual Type getType() const B2B_OVERRIDE {
            return PERSISTENT_DATA_WERE_FOUND;
        }
    };

    /**
    * Represents warning event related to Session.
    */
    class SessionWarning : public Warning
    {
    public:
        /** Constructor */
        SessionWarning( const SessionId& sessionId, const std::string& reason );
        SessionWarning( const Session* pSn, const std::string& reason );

        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WARNING;
        }

        /** Destructor. */
        virtual ~SessionWarning() {
            ;
        }

        /** Returns session's SenderCompID. */
        const std::string* getSenderCompID() const {
            return &sessionId_.sender_;
        }

        /** Returns session's TargetCompID. */
        const std::string* getTargetCompID() const {
            return &sessionId_.target_;
        }

        /** Returns session identifier. */
        const SessionId& getSessionId() const {
            return sessionId_;
        }
    protected:
        SessionId sessionId_;
    };


    /**
    * Error related to Session.
    */
    class SessionError : public Error
    {
    public:
        /** Constructor */
        SessionError( const Session* sn );
        /** Constructor */
        SessionError( const SessionId& sessionId, const std::string& aReason );
        SessionError( const Session* sn, const std::string& aReason );

        /// Destructor
        virtual ~SessionError() {};

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_ERROR;
        }

        /** Returns session's SenderCompID. */
        const std::string* getSenderCompID() const {
            return &sessionId_.sender_;
        }

        /** Returns session's TargetCompID. */
        const std::string* getTargetCompID() const {
            return &sessionId_.target_;
        }

        /** Returns session identifier. */
        const SessionId& getSessionId() const {
            return sessionId_;
        }
    protected:
        SessionId sessionId_;
    };


    /**
    * Error related to Session Logon.
    */
    class SessionLogonError : public SessionError
    {
    public:
        /** Constructor */
        SessionLogonError( const Session* sn );
        /** Constructor */
        SessionLogonError( const SessionId& sessionId, const std::string& remoteAddress, int remotePort, const std::string& localAddress, int localPort, const std::string& aReason );
        SessionLogonError( const Session* sn, const std::string& aReason );

        /// Destructor
        virtual ~SessionLogonError() {};

        virtual const std::string* what() const B2B_OVERRIDE;

        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_LOGON_ERROR;
        }
    };

	/**
	* Warning related to Session Logon.
	*/
	class SessionLogonWarning : public SessionWarning
	{
	public:
		/** Constructor */
		SessionLogonWarning(const Session* sn);
		/** Constructor */
		SessionLogonWarning(const SessionId& sessionId, const std::string& remoteAddress, int remotePort, const std::string& localAddress, int localPort, const std::string& aReason);
		SessionLogonWarning(const Session* sn, const std::string& aReason);

		/// Destructor
		virtual ~SessionLogonWarning() {};

		virtual const std::string* what() const B2B_OVERRIDE;

		virtual Type getType() const B2B_OVERRIDE {
			return SESSION_LOGON_ERROR;
		}
	};


    /**
    * Parse error was detected.
    */
    class ParseErrorInSession B2B_SEALED : public SessionError
    {
    public:
        /** Constructor */
        ParseErrorInSession( const Session* sn, const Utils::Exception& aReason );

        virtual Type getType() const B2B_OVERRIDE {
            return PARSE_ERROR_IN_SESSION;
        }
    };

	/**
	* Parse error was detected.
	*/
	class ParseWarningInSession B2B_SEALED : public SessionWarning
	{
	public:
		/** Constructor */
		ParseWarningInSession(const Session* sn, const Utils::Exception& aReason);

		virtual Type getType() const B2B_OVERRIDE {
			return PARSE_ERROR_IN_SESSION;
		}
	};


    /**
    * Session was rejected.
    */
    class SessionWasRejected B2B_SEALED : public SessionNotification
    {
    public:
        /** Constructor */
        SessionWasRejected( const Session* sn, const std::string& aReason );
        SessionWasRejected( const SessionId& sessionId, const std::string& remoteAddress, int remotePort, const std::string& localAddress, int localPort, const std::string& aReason );
        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WAS_REJECTED;
        }
    };


    /**
    * Session was created.
    */
    class SessionWasCreated B2B_SEALED : public SessionNotification
    {
    public:
        /** Constructor */
        SessionWasCreated( const Session* sn );
        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WAS_CREATED;
        }
    };

    /**
    * Session was closed.
    */
    class SessionWasClosed B2B_SEALED : public SessionNotification
    {
    public:
        /// Constructor
        /// @param sn Session.
        /// @param reason Reason of session closing.
        SessionWasClosed( const Fast2FixSession* sn, const std::string& reason = "" );
        SessionWasClosed( const SessionId& sessionId, bool correctlyTerminated, const std::string& reason = "" );
        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WAS_CLOSED;
        }
    };

    /**
    * A telecommunication link was restored.
    */
    class LinkWasRestored B2B_SEALED : public SessionNotification
    {
    public:
        /// Constructor
        /// @param sn Session.
        /// @param message std::string that describes this event.
        LinkWasRestored( const Session* sn, const char* message );

        /// Constructor
        /// @param sender SenderCompID of the Session assigned with event
        /// @param target TargetCompID of the Session assigned with event
        /// @param message std::string that describes this event.
        LinkWasRestored( const SessionId& sessionId, const char* message );

        virtual Type getType() const B2B_OVERRIDE {
            return LINK_WAS_RESTORED;
        }
    };

    /**
    * A telecommunication link error was detected.
    */
    class LinkErrorWasDetected B2B_SEALED : public SessionWarning
    {
    public:
        /// Constructor
        /// @param sn Instance of Session assigned with event
        /// @param reason Message that describes event. Can be NULL;
        LinkErrorWasDetected( const Session* sn, const char* reason );

        /// Constructor
        /// @param sender SenderCompID of the Session assigned with event
        /// @param target TargetCompID of the Session assigned with event
        /// @param reason Message that describes event. Can be NULL;
        LinkErrorWasDetected( const SessionId& sessionId, const char* reason );

        virtual Type getType() const B2B_OVERRIDE {
            return LINK_ERROR_WAS_DETECTED;
        }
    };

    /**
    * Session was restored from Persistent Message Storage.
    */
    class SessionWasRestored B2B_SEALED : public SessionNotification
    {
    public:
        /// Constructor
        /// @param sn Instance of Session assigned with event
        SessionWasRestored( const Session* sn );
        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WAS_RESTORED;
        }
    };

    /**
    * Session was established.
    */
    class SessionWasEstablished B2B_SEALED : public SessionNotification
    {
    public:
        /// Constructor
        /// @param sn Instance of Session assigned with event
    	/// @param sslInfo Extra information about SSL for the session
        SessionWasEstablished( const Session* sn, const std::string& sslInfo = "" );
        /// Constructor
		/// @param sessionId Session ID of session assigned with event
        /// @param address Address of the remote host
        /// @param port Port of the remote host
		/// @param sslInfo Extra information about SSL for the session
		SessionWasEstablished( const SessionId& sessionId, const std::string& address, int port, const std::string& sslInfo = "");
        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_WAS_ESTABLISHED;
        }
    };

    /**
     * Represents debug message related to Session.
     */
    class SessionDebugMessage B2B_SEALED : public DebugMessage
    {
    public:
        /** Constructor */
        SessionDebugMessage( const Session* pSn, const std::string& reason );
        SessionDebugMessage( const SessionId& sessionId, const std::string& reason );

        /** Destructor. */
        virtual ~SessionDebugMessage() {}

        virtual Type getType() const B2B_OVERRIDE {
            return SESSION_DEBUG_MESSAGE;
        }

        /** Returns session's SenderCompID. */
        const std::string* getSenderCompID() const {
            return &sessionId_.sender_;
        }

        /** Returns session's TargetCompID. */
        const std::string* getTargetCompID() const {
            return &sessionId_.target_;
        }

        /** Returns session identifier. */
        const SessionId& getSessionId() const {
            return sessionId_;
        }
    protected:
        SessionId sessionId_;
    };
} // namespace Engine{

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif  // _B2BITS_Event_h__

