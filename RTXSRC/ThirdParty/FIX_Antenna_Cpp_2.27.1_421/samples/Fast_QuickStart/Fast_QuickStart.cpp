/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/


// Quick Start sample
// Demonstrates how to manipulate with FIX-over-FAST session
//
// The sample illuminates how to:
// 1. Create FIX-over-FAST
// 2. Establish connection as initiator and acceptor
// 3. Send message to remote session
// 4. Process incoming session


#include <iostream>
#include <string>
#include <memory>

#include <B2BITS_V12.h>

#include "MyApp.h"

using namespace ::std;
using namespace ::Engine;
using namespace ::Utils;

static void sample()
{
    // sender ID constant
    static const string SENDER = "fast_sender";
    // target ID constant
    static const string TARGET = "fast_target";
    // templates
    static const string TEMPLATES = "FIX42.xml";

    // declare FIX application for initiator and acceptor
    MyApp iApp;
    MyApp aApp;

    Engine::FastSessionExtraParameters params;
    params.storageType_ = Engine::persistent_storageType;

    // create initiator session
    iApp.setSession(
        FixEngine::singleton()->createFastSession( TEMPLATES, &iApp, SENDER, TARGET, Engine::FIX42, &params )
    );

    // create acceptor session
    aApp.setSession(
        FixEngine::singleton()->createFastSession( TEMPLATES, &aApp, TARGET, SENDER, Engine::FIX42, &params )
    );

    // start waiting for incoming FAST oriented client
    aApp.getSession()->connect( );

    // connect to remote ( localhost ;) ) FAST server
    iApp.getSession()->connect( 30, "localhost",  FixEngine::singleton()->getListenPort() );

    // wait until connection is established
    iApp.waitForEstablished();
    aApp.waitForEstablished();

    // create message
    auto_ptr<FIXMessage> msg( FIXMsgFactory::singleton()->newSkel( Engine::FIX42, "D", Engine::FIX42 ) );
    msg->set( FIXField::ClOrdID, "123456" );
    msg->set( FIXField::Symbol, "symbol" );
    msg->set( FIXField::Side, "1" );
    msg->set( FIXField::TransactTime, "20090101-10:00:00" );
    msg->set( FIXField::HandlInst, "1" );
    msg->set( FIXField::OrderQty, 10 );
    msg->set( FIXField::OrdType, "1" );
    msg->set( FIXField::TransactTime, "20090101-10:00:00" );

    // send message to the initiator
    aApp.put( msg.get() );

    // Wait for message is being processed
    aApp.ping( "1-2-3" );
    aApp.waitForTestRequestResponse();

    // close sessions
    iApp.close();
    aApp.close();
}


int main()
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        FixEngine::init();      // initialization
        sample();               // run sample
        FixEngine::destroy();   // release resources
    } catch( const Exception& ex ) {
        cerr << "EXCEPTION: " << ex.what() << endl;
        return 1;
    }

    return 0;
}

