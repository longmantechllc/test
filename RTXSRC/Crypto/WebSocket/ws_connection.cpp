#include "ws_connection.h"
#include "magic_enum/magic_enum.hpp"
#include "spdlog/spdlog.h"

namespace Crypto {

WsConnection::WsConnection(
        on_message_t on_message_cb,
        //on_ws_pong_t on_ws_pong_cb,
        on_connect_t on_connect_cb,
        on_disconnect_t on_disconnect_cb,
        build_subscription_msg_t build_subscription_msg,
        std::shared_ptr<WebSocket> engine,
        std::string path,
        std::string host,
        int port,
        int ws_index,
        milliseconds connect_timeout,
        milliseconds connect_retry_delay,
        int connect_retry_count)
: on_message_cb_(std::move(on_message_cb))
//, on_ws_pong_cb_(std::move(on_ws_pong_cb))
, on_connect_cb_(std::move(on_connect_cb))
, on_disconnect_cb_(std::move(on_disconnect_cb))
, build_subscription_msg(std::move(build_subscription_msg))
, engine(std::move(engine))
, path(std::move(path))
, host(std::move(host))
, port(port)
, ws_index_(ws_index)
, is_connected_(false)
, connection(nullptr)
, connect_timeout(connect_timeout)
, connect_retry_delay(connect_retry_delay)
, connect_retry_count(connect_retry_count) {

    spdlog::info("<WsConnection> idx={}", ws_index_);
    this->update_state_machine(BEGIN);
}

WsConnection::~WsConnection() {
    if (this->is_connected_) {
        spdlog::info(
            "<~WsConnection> idx={} disconnecting the ws connection", ws_index_);
        this->disconnect();
    } else {
        spdlog::info(
            "<~WsConnection> idx={} not connected, release ws connection",
            ws_index_);
    }
    this->is_connected_ = false;
}

void WsConnection::connect() {
    if (!this->engine->m_context) {
        throw std::runtime_error("lws init failed");
    }
    spdlog::info("connecting idx={} {} {} {}...",
        this->ws_index_, this->host, this->path, this->port);
    this->connection = nullptr;
    this->send_buffer.clear();
    struct lws_client_connect_info ccinfo = {nullptr};
    ccinfo.context = this->engine->m_context;
    ccinfo.address = this->host.c_str();
    ccinfo.host = this->host.c_str();
    ccinfo.origin = this->host.c_str();
    ccinfo.path = this->path.c_str();
    ccinfo.port = this->port;

    ccinfo.protocol = nullptr;
    ccinfo.userdata = (void *) this;
    ccinfo.ssl_connection = LCCSCF_USE_SSL | LCCSCF_ALLOW_SELFSIGNED
        | LCCSCF_SKIP_SERVER_CERT_HOSTNAME_CHECK;
        //| LCCSCF_PRIORITIZE_READS;

    lws_cancel_service(this->engine->m_context);
    lws_client_connect_via_info(&ccinfo);
}

void WsConnection::on_message(struct lws *_connection, std::string_view in) {
    // in case we subscribe to two channels (very unlikely : if exchange
    // responds after 3 seconds successfully)
    if (_connection == this->connection)
    {
        this->on_message_cb_(in);
    }
}

void WsConnection::on_ws_pong(struct lws *_connection) {
    if (_connection == this->connection)
    {
        //this->on_ws_pong_cb_();
        spdlog::info("<WsConnection[{}] got PONG", ws_index_);
    }
}

void WsConnection::on_connect(struct lws *new_connection) {
    this->connection = new_connection;
    this->update_state_machine(CONNECT_SUCCESS);
    on_connect_cb_();
}

void WsConnection::on_disconnect() {
    spdlog::error(
        "<WsConnection[{}]::on_disconnect>,host={},path={},port={},state={}",
        ws_index_, host,path,port,magic_enum::enum_name(state));
    this->is_connected_ = false;
    this->connection = nullptr;
    this->send_buffer.clear();
    this->update_state_machine(RECV_ERROR);
}

void WsConnection::disconnect() {
    this->send_message(UDS_CLOSE_CONNECTION);
}

bool WsConnection::flush_send_buffer(lws *wsi) {
    for (auto it = this->send_buffer.begin(); it != this->send_buffer.end(); ++it) {
        if (*it == UDS_CLOSE_CONNECTION) {
            spdlog::info(
                "<WsConnection::flush_send_buffer> : idx={} received UDS_CLOSE_CONNECTION...",
                this->ws_index_);
            this->send_buffer.clear();
            return false;
        }

        std::uint8_t buf[LWS_PRE + 1024] = {0};
        std::memset(buf, '\0', LWS_PRE + 1024);
        int m;
        int n = lws_snprintf((char *) buf + LWS_PRE, 1024, "%s", it->c_str());
#ifndef NDEBUG
        spdlog::info(
            "<WsConnection::flush_send_buffer> : idx={} sending message {}...", 
            this->ws_index_, buf + LWS_PRE);
#endif
        m = lws_write(wsi, buf + LWS_PRE, n, LWS_WRITE_TEXT);
        if (m < n) {
            spdlog::info("<WsConnection::flush_send_buffer> : idx={} sending failed...",
                this->ws_index_);
            this->send_buffer.clear();
            return false;
        }
    }
    this->send_buffer.clear();
    return true;
}

void WsConnection::send_message(const std::string &message) {
    if (this->connection != nullptr) {
        spdlog::info("<WsConnection::send_message> : idx={} queueing {}",
            ws_index_, message);
        this->send_buffer.push_back(message);
        lws_callback_on_writable(this->connection);
    }
}

void WsConnection::reconnect() {
    // never happens
    // @brief if the connection is in CONNECTING state, then the parent should
    // be aware already as it could have either been just started, which case
    // WsConnectionHandler is in INIT/CONNECTING state or on_disconnect() has
    // been called already.
    // if the connection is in CONNECTED state, it will send a RECV_ERROR in
    // on_disconnect() to WsConnectionHandler
    spdlog::error(
        "<WsConnection[{}]::reconnect>,shouldn't be here.host={},path={},port={},state={}",
        ws_index_, host,path,port,magic_enum::enum_name(state));
    this->update_state_machine(RECV_ERROR);
}

void WsConnection::on_timeout(
        milliseconds timeout, Action action, std::int64_t cycle) {
    spdlog::error(
        "<WsConnection[{}]::on_timeout>,delay_millisec={},host={},path={},"
        "port={},state={},action={}",
        ws_index_, timeout.count(), host, path, port,
        magic_enum::enum_name(state), magic_enum::enum_name(action));
    // need event_queue for timer
    // this->event_queue.push(system_clock::now() + timeout, [&]() {
    //     if (this->current_cycle == cycle) this->update_state_machine(action);
    // });
}

void WsConnection::update_backoff_time() {
    auto current_time = std::chrono::system_clock::now();
    if (current_time - this->backoff_time.second > 10s) {
        this->backoff_time.first = 20ms;
    } else {
        this->backoff_time.first = std::min(duration_cast<milliseconds>(
            this->backoff_time.first * 1.25), 2000ms);
    }
    this->backoff_time.second = current_time;
}

void WsConnection::update_state_machine(
        Action action, time_point<system_clock> tp) {
    spdlog::info("<WsConnection[{}]::update_state_machine> action {}",
                magic_enum::enum_name(action));
    this->input_queue.push(action);
    this->state_machine();
}

void WsConnection::state_machine() {
    //spdlog::info("<WsConnection[{}]::state_machine> get in", ws_index_);
    while(!this->input_queue.empty()) {

    WsConnection::Action action = this->input_queue.front();
    this->input_queue.pop();
    this->current_cycle = (this->current_cycle + 1) % 1000000;
    spdlog::info("<WsConnection[{}]::state_machine> : state : {} | action : {}",
        ws_index_, magic_enum::enum_name(this->state), magic_enum::enum_name(action));
    switch (this->state) {
        case INIT: {
            switch (action) {
                case BEGIN:
                    this->update_backoff_time();
                    this->update_state_machine(CONNECT,
                        system_clock::now() + this->backoff_time.first);
                    break;
                case CONNECT: {
                    this->state = CONNECTING;
                    this->connect();
                    this->on_timeout(this->connect_timeout, CONNECT_TIMEOUT,
                        this->current_cycle);
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case CONNECTING: {
            switch (action) {
                case CONNECT_SUCCESS: {
                    this->is_connected_ = true;
                    //this->on_connect();
                    this->state = CONNECTED;
                    subscribe();
                    break;
                }
                case CONNECT_TIMEOUT:
                case RECV_ERROR: {
                    this->state = INIT;
                    this->update_state_machine(BEGIN);
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case CONNECTED: {
            switch (action) {
                case RECV_ERROR: {
                    this->state = INIT;
                    this->on_disconnect_cb_();
                    this->disconnect();
                    this->update_state_machine(BEGIN);
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
    }// while
}

// Get connection info and state
const std::string& WsConnection::get_host() const {
    return host;
}

const std::string& WsConnection::get_path() const {
    return path;
}

uint16_t WsConnection::get_port() const {
    return port;
}

WsConnection::State WsConnection::get_state() const {
    return state;
}

const struct lws* WsConnection::get_lws_connect() const {
    return connection;
}

void WsConnection::service() {
    //spdlog::info( "WsConnection::service"); 
    state_machine();
    engine->enter_event_loop();
}

}  // namespace 
