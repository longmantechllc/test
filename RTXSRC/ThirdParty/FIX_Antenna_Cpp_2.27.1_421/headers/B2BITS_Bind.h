// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Bind.h
/// Defines Utils::bind function

#if defined( HAVE_PRAGMA_ONCE )
#   pragma once
#endif // HAVE_PRAGMA_ONCE

#ifndef H_B2BITS_UTILS_BIND_H
#define H_B2BITS_UTILS_BIND_H

#include "B2BITS_MethodPtr.h"

namespace Utils
{
    template< typename return_t, typename T, typename T2 >
    MethodPtr0<return_t> bind( return_t ( *func )( T* ), T2* pthis ) throw()
    {
        return MethodPtr0< return_t >::template create<T>( func, pthis );
    }

    template< typename return_t, typename T, typename arg_t, typename T2 >
    MethodPtr1<return_t, arg_t> bind( return_t ( *func )( T*, arg_t ), T2* pthis ) throw()
    {
        return MethodPtr1< return_t, arg_t >::template create<T>( func, pthis );
    }

    template< typename return_t, typename T, typename T2, typename arg1_t, typename arg2_t >
    MethodPtr2<return_t, arg1_t, arg2_t> bind( return_t ( *func )( T*, arg1_t, arg2_t ), T2* pthis ) throw()
    {
        return MethodPtr2< return_t, arg1_t, arg2_t >::template create<T>( func, pthis );
    }

    template< typename return_t, typename T, typename T2, typename arg1_t, typename arg2_t, typename arg3_t >
    MethodPtr3<return_t, arg1_t, arg2_t, arg3_t> bind( return_t ( *func )( T*, arg1_t, arg2_t, arg3_t ), T2* pthis ) throw()
    {
        return MethodPtr3< return_t, arg1_t, arg2_t, arg3_t >::template create<T>( func, pthis );
    }

}  // b2bits::utils
#endif // H_B2BITS_UTILS_BIND_H
