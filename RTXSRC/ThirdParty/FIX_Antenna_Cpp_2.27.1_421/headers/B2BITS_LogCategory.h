// <copyright>
//
// $Revision: 42192 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_LogCategory.h
/// Contains Utils::Log::LogCategory class definition

#ifndef __B2BITS_Utils_Log__LogCategory_h__
#define __B2BITS_Utils_Log__LogCategory_h__

#include <string>

#include <B2BITS_V12_Defines.h>
#include "B2BITS_LogDefines.h"

/** Utility namespace */
namespace Utils
{
    namespace LogImpl
    {
        class LogCategoryImpl;
        class LogSystemImpl;
    } // namespace LogImpl

    /** Logging system namespace */
    namespace Log
    {

        /**
        * Logging category.
        * Use Utils::Log::LogSystem to create category.
        */
        class V12_API LogCategory
        {
            friend class LogImpl::LogSystemImpl;

        public:
            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void note( const std::string& data );

            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void debug( const std::string& data );

            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void trace( const std::string& data );

            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void warn( const std::string& data );

            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void error( const std::string& data );

            /**
            * Writes logging record to the specified for this category devices.
            * @param data Message to write to log.
            * @note synchronized.
            */
            void fatal( const std::string& data );

            /** @return true if certain level is turned on. */
            inline bool isNoteOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_NOTE ) ) );
            }

            /** @return true if certain level is turned on. */
            inline bool isDebugOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_DEBUG ) ) );
            }

            /** @return true if certain level is turned on. */
            inline bool isTraceOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_TRACE ) ) );
            }

            /** @return true if certain level is turned on. */
            inline bool isWarnOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_WARN ) ) );
            }

            /** @return true if certain level is turned on. */
            inline bool isErrorOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_ERROR ) ) );
            }

            /** @return true if certain level is turned on. */
            inline bool isFatalOn() const {
                return ( 0 != ( levels_ & ( 1 << Log::LS_FATAL ) ) );
            }


            /** Returns name of the category */
            std::string const& name() const;

            /**
            * Set Log levels.
            * @param levels Log level (whether certain level is turn on/off).
            */
            void setLevels( const Log::Levels& levels );

        protected:
            /** Constructor */
            LogCategory( std::size_t id );

            /** Constructor */
            LogCategory( std::size_t id, const std::string& name );

            /** Destructor. */
            virtual ~LogCategory();

            /** @return Implementation class. */
            Utils::LogImpl::LogCategoryImpl* getImpl() const;

            /** Copy constructor is forbidden */
            LogCategory( const LogCategory& );

            /** Assignment operator is forbidden */
            LogCategory& operator =( const LogCategory& right );

        private:
            /** Implementation */
            Utils::LogImpl::LogCategoryImpl* pImpl_;

            /** Logging levels state (on/off)
            * nested here for performance */
            Log::Levels levels_;
        }; // class LogCategory

    } // namespace Log

} // namespace Utils

#endif // __B2BITS_Utils_Log__LogCategory_h__


