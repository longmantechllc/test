//
// Created by schen on 2/18/2021.
//

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <vector>
#include <functional>
#include <ctime>
#include <chrono>

//#include <optional>

#include "RTXUtils.h"
#include "../include/Price.h"

#include "AppConstants.h"
#include "../include/Order.h"
#include "../include/ExecutionReport.h"
#include "../include/TopOfBook.h"
#include "../include/Bbo.h"

#include "../include/App.h"
#include "Logger.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "xTimer.h"
#include "TimerScheduler.h"

#include "OrderLogic/_DataStruct/TOBQuote.h"
#include "OrderLogic/_DataStruct/TobList.h"
#include "OrderLogic/_DataStruct/ChildOrderInfo.h"
#include "IOrderLogicBase.h"
#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "CPU.h"

#include "DarkSweep.h"
#include "Targeting.h"
#include "PassivePosting.h"

#include "RTXOrder.h"

using namespace std::placeholders;  // for _1, _2, _3...

void RTXOrder::StartMarketDataFailureTimeOutTimerSwp()
{
    if(m_orderIsCancelling)
        return;
    if((m_isSWPDone || (!m_isSWPDone && m_isPMP))
    && (m_isRetailSWPDone || (!m_isRetailSWPDone && m_isRetailSWPPMP))
    && (!m_isRetailRouting || (m_isRetailRouting && m_isRetailRoutPMP)))
        return;
//    if(m_clientOrder->type_ == OrderType::TYPE_MARKET)
//        return;
    int duration = m_config->GlobalConfig()->MarketDataFailureTimeOutSwp();
    if(duration <= 0) {
        FireMarketDataFailureTimeOutTimerSwp(*m_marketDataFailureTimeOutTimerSwp);
        return;
    }
    m_logger->logInfo("oid:%d, Start SWP Timer. duration:%d ms", m_clientOrder->orderID_, duration);
    m_marketDataFailureTimeOutTimerSwp = new xTimer(m_timerScheduler);
    m_marketDataFailureTimeOutTimerSwp->StartTimer(duration, std::bind(&RTXOrder::FireMarketDataFailureTimeOutTimerSwp, this, _1));
    m_isSWPTimerStarted = true;
}

void RTXOrder::FireMarketDataFailureTimeOutTimerSwp(xTimer& timer)
{
    if(m_orderIsCancelling)
        return;
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    {
        if(!m_validBbo && m_validTob && !m_tobList.GetTobList().empty())
        {
            Price bestPx = m_tobList.GetBestQuotePrice();
            if(IsMarketable(EffectivePrice(), bestPx))
            {
                if(IsBuyOrder())
                    m_nbboAsk = bestPx;
                else
                    m_nbboBid = bestPx;
                m_validBbo = true;
                m_logger->logInfo("oid:%d, SWP timer expired, Use valid tob as nbbo.", (m_clientOrder->orderID_));
            }
        }
        if(!m_validBbo)
        {
            m_logger->logInfo("oid:%d, SWP timer expired!", m_clientOrder->orderID_);

            if ((m_argoPtr.size() == 1 && (!m_isSWPDone || !m_isRetailSWPDone || m_isRetailRouting))
                || (m_argoPtr.size() == 2 && ((!m_isSWPDone && !m_isRetailSWPDone) || (!m_isSWPDone && m_isRetailRouting) || (m_isRetailRouting && !m_isRetailSWPDone)))
                || (m_argoPtr.size() == 3 && (!m_isSWPDone && !m_isRetailSWPDone && m_isRetailRouting)) )
            {
                CancelParent("SWP timer expired, no SWP and no more routing." );
                return;
            }
            m_validTob = true;
            m_isSWPDone = true;
            m_isRetailSWPDone = true;
            m_isRetailRouting = false;
            m_marketDataFailureTimeOutPost = true;
        }
//        m_validTrade = true;            //???

        ProcessOrders();
        m_isSWPTimerStarted = false;
    }
}

void RTXOrder::StartMarketDataFailureTimeOutTimerTarg(const int dur)
{
    if(m_orderIsCancelling)
        return;
//    if(!m_isSWPDone || !m_isRetailSWPDone || m_isSWPTimerStarted)
    if(m_isSWPTimerStarted)
        return;
    if(!dur && m_isRetailRoutingInit)
        return;
//    if(m_clientOrder->type_ == OrderType::TYPE_MARKET)
//        return;
    int duration;
    if(dur > 0) {
//        duration = dur;
//        m_logger->logInfo("oid:%d, Force TOB Timer. duration:%d ms", m_clientOrder->orderID_, duration);
        m_isForcedTob = true;
        m_validTob = false;
    }
//    else
    {
        duration = m_config->GlobalConfig()->MarketDataFailureTimeOutTarg();
        if(duration <= 0) {
            FireMarketDataFailureTimeOutTimerSwp(*m_marketDataFailureTimeOutTimerTarg);
            return;
        }
        m_logger->logInfo("oid:%d, Start TOB Timer. duration:%d ms", m_clientOrder->orderID_, duration);
    }
    m_marketDataFailureTimeOutTimerTarg = new xTimer(m_timerScheduler);
    m_marketDataFailureTimeOutTimerTarg->StartTimer(duration, std::bind(&RTXOrder::FireMarketDataFailureTimeOutTimerTarg, this, _1));
}

void RTXOrder::FireMarketDataFailureTimeOutTimerTarg(xTimer& timer)
{
    if(m_orderIsCancelling)
        return;
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    {
        if(!m_isForcedTob)
        {
            m_logger->logInfo("oid:%d, TOB timer expired, post!", m_clientOrder->orderID_);
            m_marketDataFailureTimeOutPost = true;
        }
        m_validTob = true;
//        m_isSWPDone = true;
//        m_isRetailSWPDone = true;
//        m_validTrade = true;            //???

        ProcessOrders();
    }
}

void RTXOrder::TryStartCodTimer(const int tifVal, const std::string& coid)
{
    if(tifVal < 10) // <= 9 && tifVal >= 0)
        return;
    auto itr =  m_config->GlobalConfig()->CustomTif().find(tifVal);
    if(itr != m_config->GlobalConfig()->CustomTif().end())
    {
        int duration = itr->second->cxlTimer;
        m_logger->logInfo("oid:%d, Start Cod Timer for coid: %s, duration:%d ms", m_clientOrder->orderID_, coid.c_str(), duration);

        m_coId.intVal = 0;
        m_coId.stringVal = coid;

        m_codTimer = new xTimer(m_timerScheduler);
        m_codTimer->StartTimer(duration, std::bind(&RTXOrder::FireCodTimer, this, _1), &m_coId);
        m_coidForCodTimer = coid;
        m_isCodTimerActive = true;
    }
    else
        m_logger->logInfo("oid:%d, Start Cod Timer Error. cannot find COD Value:%d.", m_clientOrder->orderID_, tifVal);
}

void RTXOrder::FireCodTimer(xTimer& timer)
{
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);

    std::string coid = ((uData*)timer.m_data)->stringVal;
    m_logger->logInfo("oid:%d, Fire Cod Timer , cancel coid: %s", m_clientOrder->orderID_, coid.c_str());
    CancelChildOrder(coid);
    m_coidForCodTimer = "";
    m_isCodTimerActive = false;

//    auto child = m_childOrderInfo.GetChildByCoid(coid);
//    if(child->m_childOrderType == ChildOrderType::CHILD_ORDER_TYPE_POSTING)
//        m_passivePostPtr->m_cancelPosting = true;
}

void RTXOrder::TryToStartGttExpireTimer()
{
    if(m_expireTimeSsm <= 0)
        return;
    auto now = RTXUtils::NowSsm();
    auto diff = m_expireTimeSsm - now;
    if(diff < 0)
        return;
    if((m_expireTimeSsm * 1000) >= m_cpu->m_postMarketEndTimeMssm)
        return;
    m_expireTimeSsm = 0;

    diff = diff * 1000;
    m_logger->logInfo("oid:%d, Start Gtt expire timer, duration:%d ms", m_clientOrder->orderID_, diff);

    m_gttExpireTimer = new xTimer(m_timerScheduler);
    m_gttExpireTimer->StartTimer(diff, std::bind(&RTXOrder::FireGttExpireTimer, this, _1));
}

void RTXOrder::FireGttExpireTimer(xTimer& timer)
{
    if(m_orderIsCancelling)
        return;
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    char buf[128];
    sprintf(buf, "Gtt timer expired. Cancel parent, oid:%d.", m_clientOrder->orderID_);
    m_app->m_logger->logInfo("%s", buf);
    CancelParent(buf);
}

int RTXOrder::GetExpireTimeSsm()
{
    //20210420181054681, which is YYYYMMDDHHMMSSmmm ===> UTC time
    //2021-04-20 18:10:54.681,
    long expireTime = m_clientOrder->expireTime_;
    int hrPlus = (expireTime % 1000000000);     //181054681
    int hr = hrPlus / 10000000;                 //--18
    int minPlus = hrPlus % 10000000;            //1054681
    int min = minPlus / 100000;                 //--10
    int sec = (minPlus % 100000) / 1000;        //--54

    int utcDiff = m_cpu->UtcLocalTimeDiff();
    int ssm = ((hr - utcDiff) * 3600) + (min * 60) + sec ;
    m_app->m_logger->logInfo("oid:%d, Gtt Expire Time: %d:%d:%02d.", m_clientOrder->orderID_, (hr - utcDiff), min, sec);

    return ssm;
}


void RTXOrder::TryToStopTimer(const ExecutionReport* er, const xTimer& timer)
{
    if(m_coidForCodTimer != er->childOrderId_ ||  m_isCodTimerActive == false)
        return;
//    if(!m_timerScheduler->isTimerActive(timer.m_timerId))
//        return;
    if(er->status_ == ERSTATUS::CANCELED || er->status_ == ERSTATUS::FILLED || er->status_ == ERSTATUS::REJECTED) {
        if(m_timerScheduler->remove(timer.m_timerId)) {
            m_logger->logInfo("oid:%d, Removed Cod Timer for coid: %s", m_clientOrder->orderID_, er->childOrderId_.c_str());
            m_coidForCodTimer = "";
            m_isCodTimerActive = false;
//            DESTROY(m_codTimer);
        }
    }
}

