//
// Created by schen on 10/6/2020.
//
#ifndef RTXROUTER_RTXORDER_H
#define RTXROUTER_RTXORDER_H

#include <memory>
#include <vector>
#include <thread>
#include <mutex>

#include "OrderLogic/_DataStruct/ChildOrderInfo.h"
#include "OrderLogic/_DataStruct/TobList.h"
#include "../include/TopOfBook.h"

//class Fundamentals;
//class TopOfBook;
class Bbo;
class OpenTrade;

class ExecutionReport;
class ChildOrderInfo;

class CPU;
class App;
class Logger;
class Config;

class xTimer;
class TimerScheduler;

class TobList;
class PassivePosting;
class IOrderLogicBase;
class RTXOrderIO;
class MktDataIO;

using namespace std;

struct uData
{
    uData()
    {
        intVal = 0;
        stringVal = "";
    }
    uData(int x, std::string y)
            : intVal(x),
              stringVal(y)
    {
    }
    int intVal;
    std::string stringVal;
};

enum class OrderSession
{
    Pre_Market_Order,
    In_Market_Order,
    Post_Market_Order
};

struct QtyPricePr
{
    QtyPricePr(){}
    QtyPricePr(int qtyIn, double pxIn)
        : qty(qtyIn),
        px(pxIn)
    {

    }
    ~QtyPricePr(){}

    int qty=0;
    double px;
};

class RTXOrder
{
public:
    RTXOrder() { ; }
    RTXOrder( std::shared_ptr<Order> order, App* app);
    virtual ~RTXOrder();

    void InitOrder();
    bool InitValidate();
    void Cleanup();

    void ProcessOrders();

//Get market data:
    void SubscribeMarketData();
    void UnsubscribeMarketData();
    bool OnFundamentals();
    void OnTob( const TopOfBook& topOfBook, bool isInit=false );
    void OnTob( const BatchTOB& tobV, bool isInit=false);
    void OnBbo( const Bbo& bbo, bool isInit=false );
    void OnOpeningTrade(const OpenTrade& oTrade, bool isInit=false);
    void PrintTob(const std::vector < std::shared_ptr< TobQuote >>& quoteList, bool isCached=false);

//Order:
    std::shared_ptr<Order> CreateChildOrder();
    bool SendChildOrder(std::shared_ptr<Order>& order,  const ChildOrderType& type);
    void CancelChildOrder( const std::string& coId, bool fromCancellAll=false);
    bool CancelAllChildOrders();
    bool IsBuyOrder() {return (m_clientOrder->side_ == OrderSide::SIDE_BUY);}


    void CancelParent(const std::string& message, bool userInitiated = false );
    void RejectParent(const std::string& message);
    void EndOfDayCancelParents(bool isPostMarketCancel, const std::string& message);
    void CancelParentByClient(const std::string& message);

//ExecutionReport:
    void OnChildExecutionReport(const ExecutionReport* er);
    void SendParentFillER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child);
    void SendParentAck();
    void OnOpen(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child);
    void OnFill(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child);
    void OnCancel(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child);
    void SendCancelParentExRprt(); 
    void OnReject(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child);

//Qty
    int QtyUncommited() const {return m_qtyUncommited;}
    int QtyPending() const {return m_qtyPending;}
    int QtyRemain() const {return m_qtyRemain;}

    //tools
    Price NBBO();           //return far side bbo
    Price NBBO_nearSide();
    Price NbboMidpoint() const  {return Price((m_nbboBid.value() + m_nbboAsk.value()) / 2);}
    bool IsMarketable(Price p1, Price p2);
    bool ExcludedRoute(const std::string& routeName);
    bool ExcludedVenue(const std::string& venue);
    void SetEffectivePrice();
    Price EffectivePrice() {return m_effectivePrice;}


    CPU* m_cpu;
    App* m_app;
    Logger* m_logger;           //for orders
    Config* m_config;
    TimerScheduler* m_timerScheduler;
    std::shared_ptr<RTXOrderIO> m_orderIO;
    std::shared_ptr<MktDataIO> m_MktDataIO;
    std::shared_ptr<Order> m_clientOrder;
    ChildOrderInfo m_childOrderInfo;
    TobList m_tobList;

    std::shared_ptr<SecIdfn> m_exchange;
    std::shared_ptr<User> m_userConfig;              ////login
    std::shared_ptr<User> m_accountConfig;           ////sub account

    std::vector< std::string > m_argos;
    std::vector< std::shared_ptr<IOrderLogicBase> > m_argoPtr;
    std::shared_ptr<PassivePosting> m_passivePostPtr;

    Price m_nbboBid = Price(-9999.99);
    Price m_nbboAsk = Price(99999999.99);
    Price m_effectivePrice;

    int m_lotsize = 100;
    bool m_isMarketable = false;
    bool m_validBbo = false;
    bool m_ReceivedValidBbo = false;
    bool m_firstTob = true;
    bool m_validTob = false;
    bool m_isForcedTob = false;
    bool m_validTrade = false;
    int  m_marketStartTimeSsm = 0 ; //= 34200;            //9:30
    int  m_marketEndTimeSsm = 0 ;   //= 57600;              //default to 16*60*60
    OrderSession m_orderSession = OrderSession::In_Market_Order;
    bool m_reroute = false;
    bool m_marketDataFailureTimeOutPost = false;
    //SWP
    bool m_isSWPInit = false;
    bool m_isSWPDone = true;
    bool m_isPMP = false;
    //RetailRouting
    bool m_isRetailRoutingInit = false;
    bool m_isRetailRouting = false;
    bool m_isRetailRoutPMP = false;
    //RetailSWP
    bool m_isRetailSWPInit = false;
    bool m_isRetailSWPDone = true;
    bool m_isRetailSWPPMP = false;

    std::vector<std::string> m_rerouteDestinationList;

    std::recursive_timed_mutex m_mutex;
    bool m_orderIsDone = false;
    bool m_orderIsCancelling = false;
    bool m_orderIsCleaned = false;
    bool m_isCleaning = false;
    std::unordered_map< std::string, int /*retry count*/ > m_pendingCancelCoIdList;
    int m_marketDataUnsubscribed = true;

    void StartMarketDataFailureTimeOutTimerTarg(const int dur = 0);
    bool m_isSWPTimerStarted = false;

private:
//Timers
    xTimer* m_marketDataFailureTimeOutTimerSwp = nullptr;
    void StartMarketDataFailureTimeOutTimerSwp();
    void FireMarketDataFailureTimeOutTimerSwp(xTimer& timer);

    xTimer* m_marketDataFailureTimeOutTimerTarg = nullptr;
    void FireMarketDataFailureTimeOutTimerTarg(xTimer& timer);

    xTimer* m_codTimer = nullptr;
    void TryStartCodTimer(const int tifVal, const std::string& coid);
    void FireCodTimer(xTimer& timer);
    uData m_coId;
    std::string m_coidForCodTimer;
    bool m_isCodTimerActive = false;

    xTimer* m_gttExpireTimer = nullptr;
    void TryToStartGttExpireTimer();
    void FireGttExpireTimer(xTimer& timer);
    int m_expireTimeSsm = 0;
    int GetExpireTimeSsm();

    void TryToStopTimer(const ExecutionReport* er, const xTimer& timer);

//Order action
    bool OrderParamActions();
    template<class T2>
    int Actions(OrderParamAction* act, T2 type, std::string msg, int type2=-1);
    bool OrderParamRerouteExchangeLevel();

//Basic functions
    bool ProcessValidation();
    void ProcessFundamentalData(std::string secName = "", std::string msg = "");
    void LoadArgos();
    void QtyUpdate( const ExecutionReport* executionReport);
    Price AvgPx();

    std::vector< QtyPricePr > m_qtyPxList;
    std::unordered_set< std::string > m_pendingCoIdList;
//    std::unordered_map< std::string, int /*retry count*/ > m_pendingCancelCoIdList;

    bool m_ArgoAllStarted = false;

    int m_qtyTotal;
    int m_qtyRemain;
    int m_qtyUncommited;
    int m_qtyPending = 0;
    int m_qtyFilled = 0;

//    Price m_nbboBid = Price(-9999.99);
//    Price m_nbboAsk = Price(99999999.99);
//    Price m_effectivePrice;

    bool m_validFundamental = false;
    int m_coidCnt=0;
    int m_execidCnt=0;
    int m_isParentAcked = false;
};

#endif //RTXROUTER_RTXORDER_H
