// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SystemException.h
/// Contains System::SystemException and other descendant classes declarations.

#ifndef _B2BITS_SYSTEMEXCEPTION__H
#define _B2BITS_SYSTEMEXCEPTION__H

#include <B2BITS_V12_Defines.h>
#include "B2BITS_Exception.h"

/**
 * Platform specific classes.
 */
namespace System
{

    /**
     * System exception.
     */
    class V12_API SystemException : public Utils::Exception
    {
    public:
        /**
         * Default constructor.
         */
        SystemException();

        /**
         * Constructs SystemException with the given errno and detailed description.
         */
        SystemException( char const* msg, int errNo );

        /**
         * Constructs SystemException with the given errno and detailed description.
         * Warning Please be careful. std::string can change last error code.
         */
        SystemException( std::string const& msg, int errNo );

        /**
         * Constructs SystemException.
         */
        explicit SystemException( std::string const& msg );

        /**
         * Constructs SystemException.
         */
        explicit SystemException( char const* msg );

        /** Return the system-depended error code. */
        int getErrNo() const throw() {
            return errNo_;
        }

        /** Returns description for error code. */
        static std::string getErrorDescription( int errNo );

        /** Craete error message using given description and system error code. */
        static std::string createErrorMessage( char const* msg, int errNo );

        /** Returns error code of the last operation. */
        static int getLastError() throw ();

    private:
        int errNo_;

    private:
    }; // class SystemException

    /**
     * Socket exception.
     */
    class V12_API SocketException: public SystemException
    {
    public:
        /**
         * Constructs the system std::exception with the given errno.
         */
        SocketException( char const* msg, int errNo = SocketException::getLastError() );

        /**
         * Constructs the system std::exception with the given errno.
         * warning Please be careful. std::string can change last error code.
         */
        SocketException( std::string const& msg, int errNo );

        /** Returns error code of the last operation related to socket subsystem. */
        static int getLastError();
    }; // class SocketException

    /**
     * Would block socket exception.
     */
    class V12_API WouldBlockSocketException: public SocketException
    {
    public:
        /**
         * Constructs the WouldBlock std::exception with the given description.
         */
        WouldBlockSocketException( const std::string& descr, int errNo );
    }; // class WouldBlockSocketException

} // namespace System{
#endif // #ifndef _SYSTEMEXCEPTION__H
