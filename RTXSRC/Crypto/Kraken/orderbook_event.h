#pragma once

#include "constants.h"
#include <json_dto/pub.hpp>

namespace Crypto::kraken {

// {"type":"subscriptions","channels":[{"name":"level2","product_ids":["ETH-USD"]}]}
using product_ids_t = std::vector<std::string>;
struct channel_info_t {
    std::string_view name = INVALID_STRING_VIEW;
    product_ids_t product_ids;
    channel_info_t() { product_ids.reserve(16); }
    void reset() {
        name = INVALID_STRING_VIEW;
        product_ids.clear();
    }
};
struct subscribe_response_t {
    WS::ResponseType type = WS::ResponseType::INVALID_TYPE;
    std::vector<channel_info_t> channels;

    subscribe_response_t() {
        channels.reserve(4);
    }

    void reset() {
        type = WS::ResponseType::INVALID_TYPE;
        channels.clear();
    }
};

//  {"type":"snapshot","product_id":"BTC-USD","asks":[["39864.25","0.02586487"],["39865.88","0.06900000"]...],
// "bids":[...["0.03","94852.28038576"],["0.02","229307.91285728"],["0.01","2064891.56235128"]]}
using level2_item_t = std::vector<std::string_view>;
using level2_book_t = std::vector<level2_item_t>;
struct snapshot_t {
    level2_book_t bids; // price, size
    level2_book_t asks;

    snapshot_t() {
        bids.reserve(18500);
        asks.reserve(18500);
    }

    void reset() {
        bids.clear();
        asks.clear();
    }
};
//{"type":"l2update","product_id":"BTC-USD","changes":[["buy","39854.64","0.00000000"]],"time":"2022-04-13T02:11:53.143833Z"}
struct l2update_t {
    level2_book_t changes; // side, price, size
    std::string_view time = INVALID_STRING_VIEW;
    
    l2update_t() {
        changes.reserve(512);
    }
    
    void reset() {
        time = INVALID_STRING_VIEW;
        changes.clear();
    }
};

struct orderbook_event_t {
    WS::ResponseType type = WS::ResponseType::INVALID_TYPE;
    std::string_view product_id = INVALID_STRING_VIEW;
    std::optional<snapshot_t> snapshot;
    std::optional<l2update_t> update;

    void reset(){
        type = WS::ResponseType::INVALID_TYPE;
        product_id = INVALID_STRING_VIEW;
        snapshot.reset();
        update.reset();
    }
};


}  // namespace Crypto::kraken


namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::kraken::channel_info_t &m) {

    io
    & json_dto::mandatory("name", m.name)
    & json_dto::mandatory("product_ids", m.product_ids);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::kraken::subscribe_response_t &m) {
    io
    & json_dto::mandatory("type", m.type)
    & json_dto::mandatory("channels", m.channels);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::kraken::snapshot_t &m) {
    io
    & json_dto::mandatory("bids", m.bids)
    & json_dto::mandatory("asks", m.asks);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::kraken::l2update_t &m) {
    io
    & json_dto::mandatory("time", m.time)
    & json_dto::mandatory("changes", m.changes);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::kraken::orderbook_event_t &m) {
    io
    & json_dto::mandatory("type", m.type)
    & json_dto::mandatory("product_id", m.product_id);

    if (m.type == Crypto::kraken::WS::ResponseType::snapshot) {
        json_io(io, static_cast<Crypto::kraken::snapshot_t &>(*m.snapshot));
    } else {
        json_io(io, static_cast<Crypto::kraken::l2update_t &>(*m.update));
    }
}


}  // namespace json_dto
