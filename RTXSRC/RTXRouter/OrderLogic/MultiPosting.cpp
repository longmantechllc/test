//
// Created by schen on 5/19/2021.
//

#include <iostream>
#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <ctime>
//#include <memory>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"
#include "Logger.h"
#include "RTXUtils.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "RTXOrder.h"
//#include "IOrderLogicBase.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "xTimer.h"
#include "TimerScheduler.h"

#include "MultiPosting.h"

using namespace std::placeholders;  // for _1, _2, _3...

MultiPosting::MultiPosting(RTXOrder* order)
        : IOrderLogicBase { order }
{
}

MultiPosting::~MultiPosting()
{
}

bool MultiPosting::InitOrder()
{
    if(m_isInited == true)
        return true;
    m_isInited = true;

//    m_mPostRoutes = m_order->m_exchange->multiPosting->multipostRoutes;
    m_mPostRoutes.assign(m_order->m_exchange->multiPosting->multipostRoutes.begin(), m_order->m_exchange->multiPosting->multipostRoutes.end());
    return true;
}

bool MultiPosting::ProcessOrder()
{
    if(!IsEligible())
        return false;

    while(!m_fulfilledRoute.empty())
    {
        std::string routeName;
        routeName = m_fulfilledRoute.front();
        m_fulfilledRoute.pop();
        auto itr = std::find_if(m_mPostRoutes.begin(), m_mPostRoutes.end(), [&](auto& q){if(q) return (q->name == routeName); return false;});
        if(itr != m_mPostRoutes.end())
        {
            auto route = m_order->m_config->RouterConfig()->Route(routeName);
            auto cOrder = m_order->CreateChildOrder();
            cOrder->size_ =  m_order->QtyUncommited();
            cOrder->TIF_ = route->tif;
            cOrder->type_ = OrderType::TYPE_LIMIT;
            cOrder->price_ = m_order->m_effectivePrice;
            cOrder->exchange_ = route->venue;
            cOrder->routeName_ = routeName;

            SendChildOrder(cOrder);
            return true;
        }
    }

    double totalPct = 0;
    auto rt = m_mPostRoutes.begin();
    while (rt != m_mPostRoutes.end())
    {
        std::string routeName = (*rt)->name;
        if(m_postedRoute.find(routeName) != m_postedRoute.end())
        {
            ++rt;
            continue;
        }
        if(IsRouteEligible(routeName))
        {
            totalPct += (*rt)->allocation;
            ++rt;
        }
        else
        {
            m_order->m_logger->logInfo("oid:%d, Route:%s is not eligible at this time.", m_order->m_clientOrder->orderID_, (*rt)->name.c_str());
            rt = m_mPostRoutes.erase(rt);
        }
    }
    if(m_mPostRoutes.empty())
    {
        m_order->CancelParent("No eligible mPosting route available. Cannot processing MultiPosting.");
        return false;
    }

    std::string routeName;
    std::vector<std::shared_ptr<Order>> mPostChildOrderList;
    int qtyRemain = m_order->QtyUncommited();
    int qtyRemainTp = qtyRemain;
    int cnt = 0;
    int qSize = m_mPostRoutes.size();

    auto itr = m_mPostRoutes.begin();
    while (itr != m_mPostRoutes.end())
    {
        ++cnt;
        routeName = (*itr)->name;
        if(m_postedRoute.find(routeName) != m_postedRoute.end())
        {
            ++itr;
            continue;
        }
        auto route = m_order->m_config->RouterConfig()->Route(routeName);
        double Pct =(*itr)->allocation;
        int qty = qtyRemain * (Pct / totalPct);
        qty = RTXUtils::RoundToLotSize(qty, m_order->m_lotsize);
        if (qty >= qtyRemainTp || cnt == qSize)
        {
            qty = qtyRemainTp;
            if ((route->oddLot == false && qty < m_order->m_lotsize))
            {
                ++itr;
                continue;
            }
//            qtyRemainTp = 0;
        }
        auto cOrder = m_order->CreateChildOrder();
        cOrder->size_ = qty;
        cOrder->TIF_ = route->tif;
        cOrder->type_ = OrderType::TYPE_LIMIT;
        cOrder->price_ = m_order->m_effectivePrice;
        cOrder->exchange_ = route->venue;
        cOrder->routeName_ = routeName;

        SendChildOrder(cOrder);
        qtyRemainTp -= qty;
        if(qtyRemainTp <= 0)
            break;
        ++itr;
    }
    return true;
}

bool MultiPosting::SendChildOrder(std::shared_ptr<Order> order)
{
    m_order->SendChildOrder(order, ChildOrderType::CHILD_ORDER_TYPE_MPOST);
    m_postedRoute.insert({order->routeName_, order->childOrderId_});
//    auto pr = std::make_pair(order->routeName_, order->childOrderId_);
//    m_postedRoute.insert(pr);
    return false;
}

bool MultiPosting::ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if (child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_MPOST)
        return false;
    switch (er->status_) {
        case ERSTATUS::FILLED:
        {
            m_postedRoute.erase(child->m_order->routeName_);
            m_fulfilledRoute.push(child->m_order->routeName_);
            std::unordered_set<std::string>::iterator itr;
            if((itr = m_tryCancellingRoute.find(child->m_order->routeName_)) != m_tryCancellingRoute.end())
                itr =  m_tryCancellingRoute.erase(itr);

            if(m_postedRoute.size() > m_tryCancellingRoute.size())
                StartCancelPostedChildOrderTimer();
            break;
        }
        case ERSTATUS::CANCELED:
        case ERSTATUS::REJECTED:
        {
            m_postedRoute.erase(child->m_order->routeName_);

            std::unordered_set<std::string>::iterator itr;
            if((itr = m_tryCancellingRoute.find(child->m_order->routeName_)) != m_tryCancellingRoute.end())
            {
                itr =  m_tryCancellingRoute.erase(itr);
            }
            else
            {
                std::string routeName = child->m_order->routeName_;
                auto itr1 = std::find_if(m_mPostRoutes.begin(), m_mPostRoutes.end(), [&](auto& q){if(q) return (q->name == routeName); return false;});
                if(itr1 != m_mPostRoutes.end())
                {
                    itr1 = m_mPostRoutes.erase(itr1);
                    m_order->m_logger->logInfo("oid:%d, MultiPosting route:%s is removed.", m_order->m_clientOrder->orderID_, routeName.c_str());
                }
                if (m_mPostRoutes.empty() && m_postedRoute.empty())
                {
                    m_order->CancelParent("No MultiPosting route available.");
                    m_order->Cleanup();
                    return false;
                }
            }
            break;
        }
        default:
            break;
    }
}

bool MultiPosting::ProcessMktData()
{
    return true;
}

bool MultiPosting::CancelPostedChildOrder()
{
    auto rt = m_mPostRoutes.rbegin();
    while (rt != m_mPostRoutes.rend())
    {
        std::string routeName = (*rt)->name;
        auto pstRt = m_postedRoute.find(routeName);
        auto cancelingRt = m_tryCancellingRoute.find(routeName);
        if(pstRt != m_postedRoute.end() && cancelingRt == m_tryCancellingRoute.end())
        {
            m_order->CancelChildOrder(pstRt->second);
            m_tryCancellingRoute.insert(routeName);
            break;
        }
        ++rt;
    }
}

void MultiPosting::StartCancelPostedChildOrderTimer()
{
    if(m_order->m_orderIsCancelling)
        return;
    int duration = m_order->m_exchange->multiPosting->cancelDelay;
    if(duration <= 0) {
        FireCancelPostedChildOrderTimer(*m_cancelPostedChildOrderTimer);
        return;
    }
    m_order->m_logger->logInfo("oid:%d, Start MultiPosting Cancel PostedChild Order Timer. duration:%d ms", m_order->m_clientOrder->orderID_, duration);
    m_cancelPostedChildOrderTimer = new xTimer(m_order->m_timerScheduler);
    m_cancelPostedChildOrderTimer->StartTimer(duration, std::bind(&MultiPosting::FireCancelPostedChildOrderTimer, this, _1));
}

void MultiPosting::FireCancelPostedChildOrderTimer(xTimer& timer)
{
    m_order->m_logger->logInfo("oid:%d, MultiPosting Cancel PostedChild Order Timer expired, Cancel Posted ChildOrder.", (m_order->m_clientOrder->orderID_));
    CancelPostedChildOrder();
}

bool MultiPosting::IsRouteEligible(const std::string& routeName)
{
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!route)
        return false;
    if(m_order->ExcludedRoute(routeName) == true)
        return false;
    if(route->InOperationTime(RTXUtils::NowSsm()) == false)
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(route->venue))
        return false;

    return true;
}

bool MultiPosting::IsEligible()
{
    if(m_order->m_orderIsCancelling)
        return false;
    if(m_order->m_isRetailSWPDone == false)
        return false;
    if(m_order->m_isRetailRouting == true)
        return false;
    if(m_order->m_isSWPDone == false)
        return false;
    if(m_order->QtyUncommited() <= 0)
        return false;
    if(m_order->m_isMarketable == true)
        return false;
    ////waiting for all targeting child order came back
    if(m_order->m_isMarketable == false && m_order->QtyPending() > 0 && m_postedRoute.empty())
        return false;
    if(m_order->m_clientOrder->TIF_ == TimeInForce::TIF_IOC) {
        m_order->CancelParent("IOC order cannot find inside quote.");
        return false;
    }
    if(m_order->m_config->GlobalConfig()->UnsubscribeMDWhenPost()) {
        if(!m_order->m_marketDataUnsubscribed)
            m_order->m_logger->logInfo("oid:%d, Unsubscribe MD m-post.", m_order->m_clientOrder->orderID_);
        m_order->UnsubscribeMarketData();
    }
    return true;
}


