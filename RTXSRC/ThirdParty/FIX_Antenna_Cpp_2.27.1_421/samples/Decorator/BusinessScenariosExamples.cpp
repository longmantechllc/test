/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <B2BITS_Exception.h>
#include <B2BITS_FixEngine.h>

#ifdef WIN32
#include <windows.h>
#endif

#ifdef Yield
#   undef Yield
#endif

#include <FixMessages/SG_FIX44/AllMessages.h>
#include <FixMessages/SG_FIX44/Tags.h>
#include <FixMessages/SG_FIX44/Enums.h>

using namespace std;
using namespace Engine;
using namespace FixMessages;
using namespace FixMessages::SG_FIX44;

void Sample1();
void Sample2();
void Sample3();
void Sample4();
void Sample5();
void Sample6();
void Sample7();
void Sample8();
void Sample9();
void Sample10();
void Sample11();
void Sample12();
void Sample13();
void Sample14();
void Sample15();
void Sample16();

void runSamples()
{
    try {
        Sample1();
        Sample2();
        Sample3();
        Sample4();
        Sample5();
        Sample6();
        Sample7();
        Sample8();
        Sample9();
        Sample10();
        Sample11();
        Sample12();
        Sample13();
        Sample14();
        Sample15();
        Sample16();
    } catch( const Utils::Exception& e ) {
        cout << "Error: " << e.what() << endl;
    } catch( const std::exception& e ) {
        cout << "STD Error: " << e.what() << endl;
    } catch( ... ) {
        cout << "Fatal error: Unexpected exception!" << endl;
    }
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // Initialize FIX engine
    try {
        FixEngine::init();
        cout << "FixEngine was initialized. Executing samples..." << endl;

        runSamples();
        FixEngine::destroy();
    } catch( Utils::Exception const& e ) {
        cout << e.what() << endl;
    }

    return 0;
}

/// Buy 10 MSFT @ 1.25
void Sample1()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "12304" );
    order.Side().set( '1' );
    order.Symbol().set( "MSFT" );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 10 );
    order.OrdType().set( '2' );
    order.Price().set( 1.25 );
    order.check();

    cout << "To 'Buy 10 MSFT @ 1.25' use FIX message:" << endl
         << order.raw() << endl;
}

/// Buy 20 IBM @ 1.2 STP
void Sample2()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "111444" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::BUY );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 20 );
    order.OrdType().set( OrdType::STOP );
    order.StopPx().set( 1.2 );
    order.check();

    cout << "To 'Buy 20 IBM @ 1.2 STP' use FIX message:" << endl
         << order.raw() << endl;
}

/// Sell 10 IBM @ 1.25 GTC
void Sample3()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "78954" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::SELL );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 10 );
    order.OrdType().set( OrdType::LIMIT );
    order.Price().set( 1.25 );
    order.TimeInForce().set( TimeInForce::GTC );
    order.check();

    cout << "To 'Sell 10 IBM @ 1.25 GTC' use FIX message:" << endl
         << order.raw() << endl;
}

/// Sell 30 IBM @ 1.5 Good Till 20070405
void Sample4()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "13047" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::SELL );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 30 );
    order.OrdType().set( OrdType::LIMIT );
    order.Price().set( 1.5 );
    order.TimeInForce().set( TimeInForce::GTD );
    order.ExpireDate().set( LocalMktDateType::fromFixString( "20070405" ) );
    order.check();

    cout << "To 'Sell 30 IBM @ 1.5 Good Till 20070405' use FIX message:" << endl
         << order.raw() << endl;
}

/// Sell short 5 IBM @ MKT DAY
void Sample5()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "44454" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::SELL_SHORT );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 5 );
    order.OrdType().set( OrdType::MARKET );
    order.TimeInForce().set( TimeInForce::DAY );
    order.LocateReqd().set( false );
    order.check();

    cout << "To 'Sell short 5 IBM @ MKT DAY' use FIX message:" << endl
         << order.raw() << endl;
}

/// Sell 25 MSFT @ 1.23 IOC
void Sample6()
{
    NewOrderSingleMessage order;
    order.ClOrdID().set( "22255" );
    order.Symbol().set( "MSFT" );
    order.Side().set( Side::SELL );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 25 );
    order.OrdType().set( OrdType::LIMIT );
    order.Price().set( 1.23 );
    order.TimeInForce().set( TimeInForce::IOC );
    order.check();

    cout << "To 'Sell 25 MSFT @ 1.23 IOC' use FIX message:" << endl
         << order.raw() << endl;
}


/// Cancel Sell 5 IBM @ MKT
void Sample7()
{
    OrderCancelRequestMessage order;
    order.ClOrdID().set( "108978" );
    order.OrigClOrdID().set( "17789" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::SELL );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 5 );
    order.check();

    cout << "To 'Cancel Sell 5 IBM @ MKT' use FIX message:" << endl
         << order.raw() << endl;
}

/// Modify Price: Buy 15 MSFT @ 1.75
void Sample8()
{
    OrderCancelReplaceRequestMessage order;
    order.ClOrdID().set( "1255304" );
    order.OrigClOrdID().set( "122791" );
    order.HandlInst().set( '2' );
    order.Symbol().set( "MSFT" );
    order.Side().set( Side::BUY );
    order.TransactTime().set( UTCTimestampType::now() );
    order.OrderQty().set( 15 );
    order.OrdType().set( OrdType::LIMIT );
    order.Price().set( 1.75 );
    order.check();

    cout << "To 'Modify Price: Buy 15 MSFT @ 1.75' use FIX message:" << endl
         << order.raw() << endl;
}

/// New Advertisement: Buy 10 MSFT @ 1.33
void Sample9()
{
    AdvertisementMessage adv;
    adv.AdvId().set( "12540" );
    adv.AdvTransType().set( AdvTransType::NEW );
    adv.Symbol().set( "MSFT" );
    adv.AdvSide().set( AdvSide::BUY );
    adv.Quantity().set( 10 );
    adv.Price().set( 1.33 );
    adv.check();

    cout << "To 'New Advertisement: Buy 10 MSFT @ 1.33' use FIX message:" << endl
         << adv.raw() << endl;
}

/// New IOI: Buy MSFT
void Sample10()
{
    IndicationOfInterestMessage order;
    order.IOIid().set( "123654" );
    order.IOITransType().set( IOITransType::NEW );
    order.Symbol().set( "MSFT" );
    order.Side().set( Side::BUY );
    order.IOIQty().set( "10" );
    order.check();

    cout << "To 'New IOI: Buy MSFT' use FIX message:" << endl
         << order.raw() << endl;
}

/// Cancel IOI: Sell MSFT
void Sample11()
{
    IndicationOfInterestMessage order;
    order.IOIid().set( "145287" );
    order.IOITransType().set( IOITransType::CANCEL );
    order.IOIRefID().set( "100" );
    order.Symbol().set( "MSFT" );
    order.Side().set( Side::SELL );
    order.IOIQty().set( "10" );
    order.check();

    cout << "To 'Cancel IOI: Sell MSFT' use FIX message:" << endl
         << order.raw() << endl;
}

/// Request status of order
void Sample12()
{
    OrderStatusRequestMessage order;
    order.ClOrdID().set( "124565" );
    order.Symbol().set( "IBM" );
    order.Side().set( Side::SELL );
    order.check();

    cout << "To 'Request status of order' use FIX message:" << endl
         << order.raw() << endl;
}

/// Subscribe for Market Data updates
void Sample13()
{
    MarketDataRequestMessage req;
    req.MDReqID().set( "100112" );
    req.SubscriptionRequestType().set( SubscriptionRequestType::SNAPSHOT );
    req.MarketDepth().set( MarketDepth::FULL_BOOK );
    req.MDUpdateType().set( MDUpdateType::INCREMENTAL_REFRESH );

    MarketDataRequestMessage::MDEntryTypesGroupRef typeGrp = req.MDEntryTypes();
    typeGrp.resize( 2 );
    typeGrp[0].MDEntryType().set( MDEntryType::BID );
    typeGrp[1].MDEntryType().set( MDEntryType::OFFER );

    MarketDataRequestMessage::RelatedSymGroupRef relData = req.RelatedSym();
    relData.resize( 2 );
    relData[0].Symbol().set( "IBM" );
    relData[1].Symbol().set( "MSFT" );

    req.check();

    cout << "To 'Subscribe for Market Data updates' use FIX message:" << endl
         << req.raw() << endl;
}

/// Request Quotes for IBM and MSFT instruments
void Sample14()
{
    QuoteRequestMessage req;
    req.QuoteReqID().set( "111233" );

    QuoteRequestMessage::RelatedSymGroupRef relQuoteGrp = req.RelatedSym();
    relQuoteGrp.resize( 2 );
    relQuoteGrp[0].Symbol().set( "IBM" );
    relQuoteGrp[1].Symbol().set( "MSFT" );
    req.check();

    cout << "To 'Request Quotes for IBM and MSFT instruments' use FIX message:" << endl
         << req.raw() << endl;
}

/// Fill 10 MSFT @ 1.2
void Sample15()
{
    ExecutionReportMessage exec;
    exec.OrderID().set( "2247" );
    exec.ExecID().set( "221110" );
    exec.ExecType().set( ExecType::NEW );
    exec.OrdStatus().set( OrdStatus::FILLED );
    exec.Symbol().set( "MSFT" );
    exec.LeavesQty().set( 0 );
    exec.CumQty().set( 10 );
    exec.AvgPx().set( 1.2 );
    exec.LastQty().set( 10 );
    exec.LastPx().set( 1.2 );
    exec.Side().set( Side::BUY );
    exec.check();

    cout << "To 'Fill 10 MSFT @ 1.2' use FIX message:" << endl
         << exec.raw() << endl;
}

/// Allocate 10 MSFT to Account1 and Account2
void Sample16()
{
    AllocationInstructionMessage alloc;
    alloc.AllocID().set( "1114512" );
    alloc.AllocTransType().set( AllocTransType::NEW );
    alloc.AllocType().set( AllocType::BUYSIDE_READY_TO_BOOK_SINGLE );
    alloc.AllocNoOrdersType().set( AllocNoOrdersType::EXPLICIT_LIST_PROVIDED );

    AllocationInstructionMessage::OrdersGroupRef ordReportGrp = alloc.Orders();
    ordReportGrp.resize( 1 );
    ordReportGrp[0].ClOrdID().set( "1789" );

    alloc.Side().set( Side::BUY );
    alloc.Symbol().set( "MSFT" );
    alloc.Quantity().set( 10 );
    alloc.AvgPx().set( 1.2 );
    alloc.TradeDate().set( LocalMktDateType::fromFixString( "20070330" ) );

    AllocationInstructionMessage::AllocsGroupRef allocInstrGrp = alloc.Allocs();
    allocInstrGrp.resize( 2 );
    allocInstrGrp[0].AllocAccount().set( "Account1" );
    allocInstrGrp[0].AllocQty().set( 4 );
    allocInstrGrp[1].AllocAccount().set( "Account2" );
    allocInstrGrp[1].AllocQty().set( 6 );

    alloc.check();

    cout << "To 'Allocate 10 MSFT to Account1 and Account2' use FIX message:" << endl
         << alloc.raw() << endl;
}

