// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FixEngine.h
/// Contains Engine::FixEngine class declaration.

#ifndef __B2BITS_FixEngine_h__
#define __B2BITS_FixEngine_h__

#include <cstddef>
#include <string>
#include <map>

#include <B2BITS_FixEngineVersion.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_String.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_CompilerDefines.h>
#include <B2BITS_Dictionary.h>
#include <B2BITS_Scheduler.h>

namespace Globex
{
    class MDApplication;
    class MDApplicationListener;
    struct MDApplicationParams;
    class DataReaderAbstract;
}

namespace Globex2
{
    class GlobexApplication;
    class GlobexApplicationListener;
    struct GlobexApplicationParams;
}

namespace GlobexSbe
{
    class GlobexApplication;
    class GlobexApplicationListener;
    struct GlobexApplicationParams;
}

namespace Micex
{
    namespace Mfix
    {
        class MDApplication;
        struct MDApplicationParams;
        class MDApplicationListener;
        class DataReaderAbstract;
    }
}

namespace Cboe
{
    namespace Csm
    {
        class CboeCsmApplication;
        class CboeCsmApplicationListener;
        struct CboeCsmApplicationParams;
    }

    namespace MarketDepth
    {
        class CboeMarketDepthApplication;
        class CboeMarketDepthApplicationListener;
        struct CboeMarketDepthApplicationParams;
    }
}

namespace b2bits
{
    namespace CBOE
    {
        namespace CMi2
        {
            class Initiator;
            struct SessionParameters;
        }
    }
}

namespace FixDictionary2
{
    class Dictionary;
}

namespace Cqg
{
    class MDApplication;
    class MDApplicationParams;
    class MDApplicationListener;
}

namespace Mit
{
    struct ApplicationOptions;
    class Application;
    class ConnectionManager;
}

namespace Bovespa{
   class BovespaApplication;
   struct BovespaApplicationParams;
   class BovespaApplicationListener;
}

namespace Bats {
    class Application;
    class ApplicationListener;
    struct ApplicationOptions;
}

namespace Forts {
   class FortsApplication;
   struct FortsApplicationParams;
   class FortsApplicationListener;
}

namespace Spectra {
   class SpectraApplication;
   struct SpectraApplicationParams;
   class SpectraApplicationListener;
}

namespace Spectra {
   class SpectraApplication;
   struct SpectraApplicationParams;
   class SpectraApplicationListener;
}

namespace Cme
{
namespace Mdp
{
    class MarketDataService;
    struct MarketDataServiceOptions;
}
}

namespace MarketData
{
   class DataReaderAbstract;
}
/**
* FIX Engine namespace.
*
* Contains all FIX related classes (Session, FIXMessage, FIXGroup etc).
*/
namespace Engine
{
    typedef std::map<std::string, std::string> EngineProperties;

    class FixEngineImpl;
    class Application;
    class SessionsManager;
    class EventListener;
    class Session;
    class FixMsg;
    class FAProperties;
    class AdminApplication;
    class FastCoder;
    class FastDecoder;
    struct SessionExtraParameters;
    struct FastSessionExtraParameters;
    struct Statistics;
    struct SessionId;

    /** FIX Engine interface. */
    class V12_API FixEngine
    {
    public:
        /** FixEngine::init parameters. */
        struct InitParameters {
            /** The FIX Engine configuration parameters file name.
              * If it is equal to "" and InitParameters::properties_ parameter is empty,
              * then "./engine.properties" is used.
              */
            std::string propertiesFileName_;

            /** Engine's listen port. Overrides the value from the properties file.
             * If it is equal to -1 then the value from the proterties file is used.
             */
            int listenPort_;

            /**
            * Specifies whether listening to incoming connections should be started, default - true
            * @see Engine::FixEngine::startListeningIncomingConnections()
            */
            bool startListeningIncomingConnections_;

            /**
             * The top of the directory tree under which the engine's configuration,
             * and log files are kept. Do NOT add a slash at the end of the directory path.
             * Overrides the value from the properties file. If it is equal to "",
             * then the value from the properties file is used.
             */
            std::string engineRoot_;

            /**
             * Specifies additional messages fields.
             */
            std::string additionalFields_;
            /**
            * License key
            */
            std::string licenseKey_;

            /**
             * Instance of SessionsManager.
             */
            SessionsManager* sessionsManager_;

            /**
            * Event listener.
            */
            Engine::EventListener* eventListener_;

            /**
             * Engine properties.
             * Overrides values of the properties defined in the InitParameters::propertiesFileName_ file.
             */
            EngineProperties properties_;

            /**
            * Flag to control logger subsystem life time. 
            *
            * If true, FixEngine will initialize logger subsystem in the FixEngine::init 
            * method and will destroy logger in the FixEngine::destroy method; otherwise
            * if false, it is users's responsibility to initialize and destroy logger.
            *
            * @see Utils::Log::LogSystem
            */
            bool initUtilsLogger_;

            /**
            * SSLCustomContextProperties structure describing custom SSL configuration if any.
            * @see SSLCustomContextProperties
            */
            SSLCustomContextProperties customSSLContextProperties_;

            /**
            * System::Scheduler instance to run configured schedules on.
            * @see System::Scheduler
            */
            System::SchedulerPtr pScheduler_;

            /** Default constructor */
            InitParameters()
                : listenPort_( -1 )
                , startListeningIncomingConnections_( true )
                , sessionsManager_( NULL )
                , eventListener_( NULL )
                , initUtilsLogger_( true )
                , customSSLContextProperties_() 
                , pScheduler_() {
            }
        }; // struct InitParameters
    public:
        /**
        * Initializes the Engine.
        *
        * @param params Engine parameters.
        *
        * @return pointer to an instance of this class.
        *
        * @note Must be called before any other method of this library.
        */
        static FixEngine* init( InitParameters const& params );

        /**
        * Initializes the Engine.
        *
        * @param propertiesFileName the FIX Engine configuration parameters file name.
        * If it is equal to "", then "./engine.properties" is used.
        *
        * @param listenPort Engine's listen port. Overrides the value from the properties file.
        * If it is equal to -1 then the value from the properties file is used.
        *
        * @param engineRoot The top of the directory tree under which the engine's configuration,
        * and log files are kept. Do NOT add a slash at the end of the directory path.
        * Overrides the value from the properties file. If it is equal to "",
        * then the value from the properties file is used.
        *
        * @param additionalFields Specifies additional messages fields.
        *
        * @param licenseKey License key
        *
        * @param initUtilsLogger Specifies either FixEngine should initalize LogSystem. 
        *                        Pass true to force FixEngine to initialize LogSystem; false otherwise.
        *
        * @param customSSLContextProperties SSLCustomContextProperties interface used to custom SSL configuration if any.
        *
        * @return pointer to an instance of this class.
        *
        * @note Must be called before any other method of this library.
        * @see Utils::Log::LogSystem
        */
        static FixEngine* init( const std::string& propertiesFileName = "",
                                int listenPort = -1,
                                const std::string& engineRoot = "",
                                const std::string& additionalFields = "",
                                const std::string& licenseKey = "",
                                bool initUtilsLogger = true,
                                const SSLCustomContextProperties& customSSLContextProperties = SSLCustomContextProperties() );

        /**
        * Initializes the Engine.
        *
        * @param engineProperties Engine parameters. List of parameters can be find in the B2BITS_FAProperties.h file. 
        *
        * @param licenseKey License key
        *
        * @param initUtilsLogger Specifies either FixEngine should initalize LogSystem. 
        *                        Pass true to force FixEngine to initialize LogSystem; false otherwise.
        *
        * @param customSSLContextProperties SSLCustomContextProperties interface used to custom SSL configuration if any.
        *
        * @return pointer to an instance of this class.
        *
        * @note Must be called before any other method of this library.
        * @see Utils::Log::LogSystem
        */
        static FixEngine* init( const EngineProperties& engineProperties, 
                                const std::string& licenseKey = "",
                                bool initUtilsLogger = true,
                                const SSLCustomContextProperties& customSSLContextProperties = SSLCustomContextProperties() );

		/** 
		* Using by FA to log real revision number, to avoid static relink 
		*/
		static void reset_revision(const char *r);


        /**
        * Retrieves properties defaults
        *
        * @return FIX Engine configuration parameters
        */
        static const EngineProperties getEnginePropertiesDefaults();

        /**
        * Returns an instance of this class.
        *
        * @return pointer to an instance of this class.
        *
        * @see init()
        */
        static FixEngine* singleton();

        /**
        * Returns true if FixEngine was initialized; false otherwise.
        */
        static bool initialized();

        /**
        * Destroys the instance of this class.
        *
        * @note Must be called after the using of FIX Engine to free the allocated resources.
        */
        static void destroy();

        /**
        * Initializes the additional fields.
        *
        * @param propertiesFileName the FIX Engine configuration parameters file name.
        * If it is equal to "", then "./engine.properties" is used.
        *
        * @param engineRoot The top of the directory tree under which the engine's configuration,
        * and log files are kept. Do NOT add a slash at the end of the directory path.
        * Overrides the value from the properties file. If it is equal to "",
        * then the value from the proterties file is used.
        *
        * @param additionalFields Specifies additional messages fields.
        *
        * @note Do not use it with init() method.
        */
        static void setAdditionalFields( const std::string& propertiesFileName = "",
                                         const std::string& engineRoot = "",
                                         const std::string& additionalFields = "" );

        /**
        * Destroys the additional fields.
        *
        * @note Do not use it with destroy() method.
        */
        static void clearAdditionalFields();

        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param senderCompID SenderCompID of the session.
        * @param targetCompID TargetCompID of the session.
        * @param appVer Application FIX protocol version or default application FIX protocol in case of FIXT 1.1 session.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType - type of the message storage
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const std::string& senderCompID,
                                const std::string& targetCompID,
                                FIXVersion appVer,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                UnderlyingProtocol underlyingProtocolType = FIX_TCP );


        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param senderCompID SenderCompID of the session.
        * @param targetCompID TargetCompID of the session.
        * @param scpID FIX session control parser identifier.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType Message storage type
        * @param defaultAppProtocol Default application FIX protocol version.
        *        Required if scpID points to FIXT 1.1 protocol.
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const std::string& senderCompID,
                                const std::string& targetCompID,
                                ParserID scpID,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                FIXVersion defaultAppProtocol = NA );


        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param senderCompID SenderCompID of the session.
        * @param targetCompID TargetCompID of the session.
        * @param parserName Name of the parser ('id' attribute in the FIX Dictionary file).
        *                     FIX Parser for this should be created.
        *                     @see Engine::FixEngine::createFixParser.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType - type of the message storage
        * @param defaultAppProtocol Default application FIX protocol version.
        *        Required if underlyingProtocolType equals FIXT11_TCP or parserID points to FIXT 1.1 protocol.
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const std::string& senderCompID,
                                const std::string& targetCompID,
                                ParserName const& parserName,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                FIXVersion defaultAppProtocol = NA );

        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param sessionId Session Identifier
        * @param appVer Application FIX protocol version or default application FIX protocol in case of FIXT 1.1 session.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType - type of the message storage
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const SessionId& sessionId,
                                FIXVersion appVer,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                UnderlyingProtocol underlyingProtocolType = FIX_TCP );


        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param sessionId Session Identifier
        * @param scpID FIX session control parser identifier.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType Message storage type
        * @param defaultAppProtocol Default application FIX protocol version.
        *        Required if scpID points to FIXT 1.1 protocol.
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const SessionId& sessionId,
                                ParserID scpID,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                FIXVersion defaultAppProtocol = NA );


        /**
        * Creates a new FIX Session. It is user's responsibility to call Session::release method.
        *
        * @param pApp a pointer to the Application that will process the incoming messages.
        * @param sessionId Session Identifier
        * @param parserName Name of the parser ('id' attribute in the FIX Dictionary file).
        *                     FIX Parser for this should be created.
        *                     @see Engine::FixEngine::createFixParser.
        * @param pParam an option session parameters, can be NULL.
        * @param storageType - type of the message storage
        * @param defaultAppProtocol Default application FIX protocol version.
        *        Required if underlyingProtocolType equals FIXT11_TCP or parserID points to FIXT 1.1 protocol.
        *
        * @warning Do not delete the Application while the session is not terminated.
        *
        * @note two sessions with the same values of senderCompID and targetCompID can not be created.
        *
        * @return a pointer to the created session.
        */
        Session* createSession( Application* pApp,
                                const SessionId& sessionId,
                                ParserName const& parserName,
                                const SessionExtraParameters* pParam = NULL,
                                const MessageStorageType storageType = default_storageType,
                                FIXVersion defaultAppProtocol = NA );


        /**
        * Searches for a session with given senderCompID/targetCompID with empty session qualifier. Use overloaded 
        * method with SessionId parameter to get session with qualifier. Session can be found after Session::connect call
        * Calls addRef before returning the pointer, it is user's responsibility to call Session::release method.
        *
        * @param senderCompID SenderCompID of the session.
        * @param targetCompID TargetCompID of the session.
        *
        * @return a pointer to the session or NULL if session was not found.
        */
        Session* getSession( const std::string& senderCompID, const std::string& targetCompID );

        /**
        * Searches for a session by sessionId consisting of senderCompID, targetCompID and session qualifier.
        * Session can be found after Session::connect call.
        * Calls addRef before returning the pointer, it is user's responsibility to call Session::release method.
        *
        * @param sessionId Session Identifier
        *
        * @return a pointer to the session or NULL if session was not found.
        */
        Session* getSession( const SessionId& sessionId );

        /**
        * Creates FIX-over-FAST session
        * It is user's responsibility to call Session::release method.
        * @deprecated This version of the method is deprecated. Use another overloads.
        */
        B2B_DEPRECATED("This version of the method is deprecated. Use another overloads.")
        Session* createFastSession( std::string const& templateFn,
                                    Application* pApp,
                                    const std::string& senderCompID,
                                    const std::string& targetCompID,
                                    FIXVersion appVer,
                                    const SessionExtraParameters* pParam,
                                    const MessageStorageType storageType = default_storageType  );

        /**
        * Creates FIX-over-FAST session
        * It is user's responsibility to call Session::release method.
        */
        Session* createFastSession( std::string const& templateFn,
                                    Application* pApp,
                                    const std::string& senderCompID,
                                    const std::string& targetCompID,
                                    FIXVersion appVer,
                                    const FastSessionExtraParameters* pParam = NULL );

        /**
        * Creates FIX-over-FAST session
        * It is user's responsibility to call Session::release method.
        */
        Session* createFastSession( std::string const& templateFn,
                                    Application* pApp,
                                    const SessionId& sessionId,
                                    FIXVersion appVer,
                                    const FastSessionExtraParameters* pParam = NULL );

        /**
        * Creates FIX-over-FAST session
        * It is user's responsibility to call Session::release method.
        */
        Session* createFastSession( std::string const& templateFn,
                                    Application* pApp,
                                    const std::string& senderCompID,
                                    const std::string& targetCompID,
                                    FIXVersion appVer,
                                    FIXVersion scpVer,
                                    const FastSessionExtraParameters* pParam = NULL );

        /**
        * Creates FIX-over-FAST session
        * It is user's responsibility to call Session::release method.
        */
        Session* createFastSession( std::string const& templateFn,
                                    Application* pApp,
                                    const SessionId& sessionId,
                                    FIXVersion appVer,
                                    FIXVersion scpVer,
                                    const FastSessionExtraParameters* pParam = NULL );

        /**
        * Creates instance of the CME Globex FIX/FAST adapter
        * It is user's responsibility to call Globex::MDApplication::release method.
        */
        Globex::MDApplication* createMDApplication( Globex::MDApplicationParams const& params,
                Globex::MDApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CME Globex FIX/FAST adapter
        * It is user's responsibility to call Globex::MDApplication::release method.
        * @note For testing purposes only.
        */
        Globex::MDApplication* createMDApplication( Globex::MDApplicationParams const& params,
                Globex::DataReaderAbstract* dataReader,
                Globex::MDApplicationListener* appListener = NULL );
        
        /*!
        * Creates instance of the CME Globex SBE adapter
        * It is user's responsibility to call GlobexSbe::MDApplication::release method.
        * \param params application parameters
        * \param appListener application observer
        * \return Globex SBE application instance
        */
        GlobexSbe::GlobexApplication* createGlobexSbeApplication( GlobexSbe::GlobexApplicationParams const& params,
                                                                  GlobexSbe::GlobexApplicationListener* appListener = NULL );
        
        /*!
        * Creates instance of the CME Globex SBE adapter
        * It is user's responsibility to call GlobexSbe::MDApplication::release method.
        * \param params application parameters
        * \param appListener application observer
        * \return Globex SBE application instance
        */
        GlobexSbe::GlobexApplication* createGlobexSbeApplication( GlobexSbe::GlobexApplicationParams const& params,
                                                                  MarketData::DataReaderAbstract* dataReader,
                                                                  GlobexSbe::GlobexApplicationListener* appListener = NULL );
        
        /**
        * Creates instance of the CME Globex FIX/FAST adapter v.2
        * It is user's responsibility to call Globex::MDApplication::release method.
        */
        Globex2::GlobexApplication* createGlobexApplication( Globex2::GlobexApplicationParams const& params,
                Globex2::GlobexApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CME Globex FIX/FAST adapter v.2
        * It is user's responsibility to call Globex::MDApplication::release method.
        * @note For testing purposes only.
        */
        Globex2::GlobexApplication* createGlobexApplication( Globex2::GlobexApplicationParams const& params,
                MarketData::DataReaderAbstract* dataReader,
                Globex2::GlobexApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CBOE CSM adapter
        */
        Cboe::Csm::CboeCsmApplication* createCboeCsmApplication( Cboe::Csm::CboeCsmApplicationParams const& params,
                Cboe::Csm::CboeCsmApplicationListener* appListener = NULL );

        /**
        * @note For testing purposes only.
        */
        Cboe::Csm::CboeCsmApplication* createCboeCsmApplication( Cboe::Csm::CboeCsmApplicationParams const& params,
                MarketData::DataReaderAbstract* dataReader,
                Cboe::Csm::CboeCsmApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CBOE Market Depth adapter
        */
        Cboe::MarketDepth::CboeMarketDepthApplication* createCboeMarketDepthApplication( Cboe::MarketDepth::CboeMarketDepthApplicationParams const& params,
                Cboe::MarketDepth::CboeMarketDepthApplicationListener* appListener = NULL );

        /**
        * @note For testing purposes only.
        */
        Cboe::MarketDepth::CboeMarketDepthApplication* createMarketDepthApplication( Cboe::MarketDepth::CboeMarketDepthApplicationParams const& params,
                MarketData::DataReaderAbstract* dataReader,
                Cboe::MarketDepth::CboeMarketDepthApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CBOE CMi2 Initiator
        */
        b2bits::CBOE::CMi2::Initiator* createCboeCmi2Initiator(b2bits::CBOE::CMi2::SessionParameters const& params);

        /**
        * Creates instance of the MICEX MFIX adapter
        * It is user's responsibility to call Micex::Mfix::MDApplication::release method.
        */
        Micex::Mfix::MDApplication* createMICEXApplication( Micex::Mfix::MDApplicationParams const& params,
                Micex::Mfix::MDApplicationListener* appListener = NULL );

        /**
        * Creates instance of the CME Globex FIX/FAST adapter
        * It is user's responsibility to call Micex::Mfix::MDApplication::release method.
        * @note For testing puproses only
        */
        Micex::Mfix::MDApplication* createMICEXApplication( Micex::Mfix::MDApplicationParams const& params,
                Micex::Mfix::DataReaderAbstract* dataReader,
                Micex::Mfix::MDApplicationListener* appListener = NULL );

        /**
         * Creates instance of the CQG FIX/FAST Market Data adapter
         * It is user's responsibility to call Cqg::MDApplication::release method.
         */
        Cqg::MDApplication* createCqgMDApplication( Cqg::MDApplicationListener* listener,
                Cqg::MDApplicationParams const& params,
                MarketData::DataReaderAbstract* dataReader = NULL );

        /**
        * Creates an instance of the MilleniumIT Market Data Adapter.
        * The application should be destroyed with Application::release().
        * connectionManager should stay alive until Application::release() is called.
        * connectionManager can be 0 to create and use the default implementation.
        */
        Mit::Application* createMitApplication( Mit::ConnectionManager* connectionManager,
                                                const std::string& configurationFilename );

        /**
        * Creates an instance of the MilleniumIT Market Data Adapter.
        * The application should be destroyed with Application::release().
        * connectionManager should stay alive until Application::release() is called.
        * connectionManager can be 0 to create and use the default implementation.
        */
        Mit::Application* createMitApplication( Mit::ConnectionManager* connectionManager,
                                                const std::string& name,
                                                const Mit::ApplicationOptions& applicationOptions );

         /**
        * Creates instance of the Bovespa FIX/FAST adapter
        */
        Bovespa::BovespaApplication* createBovespaApplication(Bovespa::BovespaApplicationParams const& params,
            Bovespa::BovespaApplicationListener* appListener = NULL);


         /**
        * @note For testing purposes only.
        */
         Bovespa::BovespaApplication* createBovespaApplication(Bovespa::BovespaApplicationParams const& params,
            MarketData::DataReaderAbstract* dataReader,
            Bovespa::BovespaApplicationListener* appListener = NULL);

        /**
        * Creates instance of the MOEX RTS FORTS FIX/FAST adapter
        * It is user's responsibility to call Forts::MDApplication::release method.
        */
        Forts::FortsApplication* createFortsApplication(Forts::FortsApplicationParams const& params,
            Forts::FortsApplicationListener* appListener = NULL);


        /**
        * Creates instance of the MOEX RTS FORTS FIX/FAST adapter
        * It is user's responsibility to call Forts::MDApplication::release method.
        * @note For testing puproses only
        */
         Forts::FortsApplication* createFortsApplication(Forts::FortsApplicationParams const& params,
            MarketData::DataReaderAbstract* dataReader,
            Forts::FortsApplicationListener* appListener = NULL);


        /**
        * Creates instance of the MOEX RTS SPECTRA FIX/FAST adapter
        * It is user's responsibility to call Spectra::MDApplication::release method.
        */
        Spectra::SpectraApplication* createSpectraApplication(Spectra::SpectraApplicationParams const& params,
            Spectra::SpectraApplicationListener* appListener = NULL);


        /**
        * Creates instance of the MOEX RTS SPECTRA FIX/FAST adapter
        * It is user's responsibility to call Spectra::MDApplication::release method.
        * @note For testing puproses only
        */
         Spectra::SpectraApplication* createSpectraApplication(Spectra::SpectraApplicationParams const& params,
            MarketData::DataReaderAbstract* dataReader,
            Spectra::SpectraApplicationListener* appListener = NULL);

        /**
        * Creates an instance of the Bats Market Data Adapter.
        * The application should be destroyed with Application::release().
        */
        Bats::Application* createBatsApplication(const Bats::ApplicationOptions& options, Bats::ApplicationListener* listener);

        /**
        * Creates an instance of the CME MDP3 Market Data Adapter.
        * The adaper should be destroyed with MarketDataService::destroy().
        */
        Cme::Mdp::MarketDataService* createCmeMdpMarketDataService(const std::string& name, const Cme::Mdp::MarketDataServiceOptions& options);

        /** Returns the statistical information.
        *
        * @param[out] apS Instance of Statistics to store result.
        */
        void getStatistics( Statistics* apS );

        /** Registers SessionsManager with the engine.
        * @param apManager Instance of SessionsManager.
        */
        void registerSessionsManager( SessionsManager* apManager );

        /** Returns the current SessionsManager. */
        Engine::SessionsManager* getSessionsManager();

        /** Registers the given EventListener with FixEngine.
        * @param apListener EventListener
        */
        void registerEventListener( Engine::EventListener* apListener );

        /**
        * Unregisters the given EventListener.
        *
        * @param apListener EventListener
        * @return true if the EventListener was found, otherwise false.
        */
        bool unregisterEventListener( Engine::EventListener* apListener );

        /**
        * Register administrative application.
        *
        * @param app custom administrative application.
        * @throw Utils::Exception if administrative application already registered.
        */
        void registerAdminApplication( AdminApplication* app );

        /**
        * Returns administrative application.
        * If there is no registered custom application, embedded is returned.
        */
        AdminApplication* getAdminApplication();

        /**
         * Returns engine listen port number.
         * @note if the value is 0 then the communication level must be disabled.
         */
        int getListenPort() const;

        /**
         * Returns engine listen ports.
         * @note if the number of the ports is 0 then the communication level must be disabled.
         */
        ListenPorts getListenPorts() const;

        /**
        * Makes the engine listen to incoming connections
        * @see Engine::FixEngine::InitParameters::startListeningIncomingConnections_
        */
        void startListeningIncomingConnections();

        /**
        * Pauses the engine listen to incoming connections
        */
        void stopListeningIncomingConnections();

        /** Stops the engine's worker threads. */
        void stop();

        /**
        * Backups engine's log file under the given name and opens a new one.
        *
        * @param backupFilePath the current engine's log will be moved to this path.
        */
        void backupLogFile( const std::string& backupFilePath );

        /**
        * Sets the default unregistered session's storage type.
        * @param type storage type that will be used in new sessions.
        */
        void setUnregisteredAcceptorSessionStorageType( const MessageStorageType type );

        /**
        * Returns the default unregistered session's storage type.
        * @return type storage type that will be used in new sessions.
        */
        MessageStorageType getUnregisteredAcceptorSessionStorageType()const;

        /** Returns settings */
        const FAProperties* getProperties()const;

        /** Loads protocol definition/customization file
        * @param[in] fileName Path to the protocol file
        */
        void loadProtocolFile( std::string const& fileName );

        /** Creates protocol using loaded protocol definition files.
        * @param[in] scpProtocolName Protocol name (attribute 'id' in the XML file) to create parser for.
        * @param[in] appProtocols Set of application level protocols. Used if SCP protocol is FIXT 1.1.
        * @return unique parser ID.
        */
        ParserID createFixParser( DictionaryID const& scpProtocolName, FixVersionToProtocolName const* appProtocols = NULL);

        /** Creates FIXT11 protocol using loaded protocol definition files.
        * @param[in] scpProtocolName Protocol name (attribute 'id' in the XML file) to create parser for. Must be FIXT 1.1
        * @param[in] appProtocol Application level protocol name.
        * @return unique parser ID.
        */
        ParserID createFixParser( DictionaryID const& scpProtocolName, DictionaryID const& appProtocol);

        /** Creates FIXT11 protocol using loaded protocol definition files.
        * @param[in] parserName Unique parser name that will be assigned to the new parser.
        * @param[in] scpProtocolName Transport protocol name (attribute 'id' in the XML file) to create parser for. Must be FIXT 1.1
        * @param[in] appProtocols Set of application level protocols.
        * @return unique parser ID.
        */        
        ParserID createFixParser( ParserName const& parserName, DictionaryID const& scpProtocolName, FixVersionToProtocolName const* appProtocols );

        /** Creates FIXT11 protocol using loaded protocol definition files.
        * @param[in] parserName Unique parser name that will be assigned to the new parser.
        * @param[in] scpProtocolName Transport protocol name (attribute 'id' in the XML file) to create parser for. Must be FIXT 1.1
        * @param[in] appProtocol Application level protocol name.
        * @return unique parser ID.
        */        
        ParserID createFixParser( ParserName const& parserName, DictionaryID const& scpProtocolName, DictionaryID appProtocol );

        /** Returns set of application level protocols for given parser.
        *  @param[in] parserID Unique parser ID
        *  @throw Utils::Exception If parser with given ID is not found.
        *  @return set of application level protocols.
        */
        FixVersionToProtocolName const* getProtocols( ParserID parserID ) const;

        /** Returns transport protocol name for given parser.
        *  @param[in] parserID Unique parser ID
        *  @throw Utils::Exception If parser with given ID is not found.
        *  @return Transport protocol name.
        */
        DictionaryID const* getScpProtocolName( ParserID parserID ) const;

        /** Register new custom FIX Protocol and returns its identifier
        *
        * @param name Unique name of the protocol
        * @param baseVer Base FIX Version
        * @param additionalXml FIX Protocol customization file
        *
        * @throw Utils::Exception If protocol with given name already exists
        *        or additionalXml points to default additional.xml file.
        *
        * @return Unique FIX protocol identifier
        * @deprecated Use Engine::FixEngine::loadProtocolFile and Engine::FixEngine::createFixParser methods.
        */
        B2B_DEPRECATED( "Use Engine::FixEngine::loadProtocolFile and Engine::FixEngine::createFixParser methods." )
        ParserID createProtocol( std::string const& name, FIXVersion baseVer, std::string const& additionalXml );

        /** Returns base FIX protocol version.
        *
        * @throw Utils::Exception If protocol with given ID is not found.
        *
        * @return base FIX protocol version.
        */
        FIXVersion getBaseProtocol( ParserID parserID ) const;

        /** Returns unique parser name.
        *  @param[in] parserID Unique parser ID
        *
        *  @throw Utils::Exception If parser with given ID is not found.
        *
        *  @deprecated Use Engine::FixEngine::getParserName().
        *  @return unique parser name.
        */
        B2B_DEPRECATED( "Use Engine::FixEngine::getParserName( ParserID parserID )." )
        Engine::AsciiString getProtocolName( ParserID parserID ) const;

        /** Returns unique parser name.
        *  @param[in] parserID Unique parser ID
        *
        *  @throw Utils::Exception If parser with given ID is not found.
        *
        *  @return unique parser name.
        */
        ParserName const& getParserName( ParserID parserID ) const;

        /** Returns unique parser ID.
        *  @param[in] Unique parser name
        *
        *  @throw Utils::Exception If parser with given name is not found.
        *
        *  @return unique parser ID.
        ** @deprecated Use Engine::FixEngine::getParserID().
        */
        B2B_DEPRECATED( "Use Engine::FixEngine::getParserID()." )
        ParserID getProtocolID( const std::string& name ) const { return getParserID(name); }

        /** Returns unique parser ID.
        *  @param[in] Unique parser name
        *
        *  @return unique parser ID or INVALID_PARSER_ID if not found.
        */
        ParserID getParserID( ParserName const& name ) const;

        /** Returns parser description for given parser id.
        * @param parserID unique parser id
        * @return ParserDescription
        */
        ParserDescription getParserDescription( Engine::ParserID parserID ) const;

        /** Finds registered parser id by partially filled parser desription.
        * @param[in] parserDescription partially filled parser desription
        * @return parser id or Engine::INVALID_PARSER_ID.
        */
        ParserID findParser( const ParserDescription& parserDescription ) const;

        /** Returns set of registered parsers include custom parsers.
        *  @return set of parsers.
        */
        ParserIDToParserName const* getParsers() const;

        /** Returns reference to protocol dictionary */
        FixDictionary2::Dictionary const* getDictionary() const;

        /** Returns registered protocol by protocol name.
        * @param protocolName name of protocol
        * @throw Utils::Exception if protocol for given name is not found.
        * @return pointer to protocol
        */
        FixDictionary2::ProtocolT::CPtr getProtocol( std::string const& protocolName ) const;

        /** Returns base version of registered protocol by protocol name.
        * @param protocolName name of protocol
        * @throw Utils::Exception if protocol for given name is not found.
        * @return base version of registered protocol
        */
        FIXVersion getProtocolBaseVersion( std::string const& protocolName ) const;

        /** Returns version of the FixEngine.
        *  @return version of the FixEngine
        */
        static std::string getEngineVersion();

        /** Returns engine's scheduler used to run cobfigured schedules*/
        System::SchedulerPtr getScheduler() const;

        /**
        * Creates FastCoder
        * It is user's responsibility to destroy FastCoder instance.
        */
        FastCoder* createFastCoder( std::string const& template_fn, Engine::FIXVersion protocolVersion );


         /**
        * Creates FastCoder
        * It is user's responsibility to destroy FastCoder instance.
        */
        FastCoder* createFastCoder( std::string const& template_fn, Engine::FIXVersion appProtocolVersion, Engine::FIXVersion scpProtocolVersion);

        /**
        * Creates FastDecoder
        * It is user's responsibility to destroy FastDecoder instance.
        */
        FastDecoder* createFastDecoder( std::string const& template_fn, Engine::FIXVersion protocolVersion );


        /**
        * Creates FastDecoder
        * It is user's responsibility to destroy FastDecoder instance.
        */
        FastDecoder* createFastDecoder( std::string const& template_fn, Engine::FIXVersion appProtocolVersion, Engine::FIXVersion scpProtocolVersion );


        /**
         * Creates FIX acceptor. This session is experimental.
         *
         * @param port Listen TCP port
         * @param pApp Session event listener
         * @param senderCompID SenderCompID of the session.
         * @param targetCompID TargetCompID of the session.
         * @param scpID FIX session control protocol identifier.
         * @param pParam an option session parameters, can be NULL.
         *
         * @warning This session is experimental.
         */
        Session* createAcceptor(int port,
                                Application* pApp,
                                const std::string& senderCompID,
                                const std::string& targetCompID,
                                ParserID scpID,
                                const SessionExtraParameters* pParam = NULL);

        /**
         * Creates FIX initiator. This session is experimental.
         *
         * @param port Listen TCP port
         * @param pApp Session event listener
         * @param senderCompID SenderCompID of the session.
         * @param targetCompID TargetCompID of the session.
         * @param scpID FIX session control protocol identifier.
         * @param pParam an option session parameters, can be NULL.
         *
         * @warning This session is experimental.
         */
        Session* createInitiator(Application* pApp,
                                 const std::string& senderCompID,
                                 const std::string& targetCompID,
                                 ParserID scpID,
                                 const SessionExtraParameters* pParam = NULL);
    public:
        FixEngineImpl* _impl();

    private:
        /** Terminates all not disconnected session.*/
        static void terminateNotDisconnectedSessions();

        /** initialize engine*/
        static void initEngine( InitParameters const& params,
                                int& stage );

        /** cleanup failed engine initialization*/
        static void cleanupFailedEngineInit( int stage );

    private:
        /** Constructor. */
        FixEngine();

        /** Destructor. */
        virtual ~FixEngine();

    private:
        static FixEngine* s_pSingleton;

        /** Implementation details. */
        FixEngineImpl* pImpl_;
    }; // class FixEngine
} // namespace Engine

#endif // __B2BITS_FixEngine_h__
