//
// Created by schen on 10/6/2020.
//
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <vector>
#include <functional>
#include <ctime>
#include <chrono>

#include "../include/Price.h"
#include "../include/Order.h"
#include "../include/ExecutionReport.h"
//#include "../include/TopOfBook.h"
#include "../include/Bbo.h"

#include "Logger.h"
#include "AppConstants.h"
#include "RTXUtils.h"

#include "CPU.h"
#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "../include/App.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "MarketData/SecMasterBox.h"
#include "xTimer.h"
#include "TimerScheduler.h"

#include "OrderLogic/_DataStruct/TOBQuote.h"
#include "OrderLogic/_DataStruct/TobList.h"
#include "OrderLogic/_DataStruct/ChildOrderInfo.h"
#include "IOrderLogicBase.h"


#include "DarkSweep.h"
#include "Targeting.h"
#include "PassivePosting.h"
#include "Reroute.h"
#include "RetailRouting.h"
#include "MultiPosting.h"

#include "RTXOrder.h"

using namespace std::placeholders;  // for _1, _2, _3...

RTXOrder::RTXOrder(std::shared_ptr<Order> order, App* app)
    :m_app(app),
     m_cpu(app->m_cpu),
     m_logger(app->m_logger),
     m_config(app->m_config),
     m_timerScheduler(app->m_cpu->m_timerScheduler),
     m_orderIO(app->GetOrderIO()),
     m_MktDataIO(app->GetMktDataIO()),
     m_clientOrder(order),
     m_qtyTotal(order->size_),
     m_qtyRemain(order->size_),
     m_qtyUncommited(order->size_)
{
    m_logger->logInfo("Received parent order, oid:%d, side:%s, type:%s, symbol:%s, price:%.4f, disc:%.4f, size:%d, max:%d, tif:%s, login:%s, subAccount:%s, exp:%ld, minQty:%d",
          order->orderID_, OrderSideString(order->side_).c_str(), OrderTypeString(order->type_).c_str(), order->symbol_.c_str(), order->price_.ToDouble(),
          order->discretion_.ToDouble(), order->size_, order->displaySize_, OrderTifString(order->TIF_).c_str(), order->logInId_.c_str(),
          order->subAccount_.c_str(), order->expireTime_, order->minQty_);

    m_tobList.SetSide(m_clientOrder->side_);
    m_tobList.SetLog(app->m_loggerSys);

    m_userConfig = m_config->UserConfig()->GetUserConfig(0 /*login*/, order->logInId_);
    m_accountConfig =  m_config->UserConfig()->GetUserConfig(1 /*sub account*/, order->subAccount_);

    if(m_cpu->GetMarketSession() == MarketSession::In_Market)
        m_orderSession = OrderSession::In_Market_Order;
    else if(m_cpu->GetMarketSession() == MarketSession::Pre_Market)
        m_orderSession = OrderSession::Pre_Market_Order;
    else
        m_orderSession = OrderSession::Post_Market_Order;

    if(m_userConfig->startTime > 0)
        m_marketStartTimeSsm = m_userConfig->startTime;
    else if(m_accountConfig->startTime > 0)
        m_marketStartTimeSsm = m_accountConfig->startTime;
//    else
//        m_marketStartTimeSsm = m_config->GlobalConfig()->MarketOpenTime() / 1000;
    m_marketEndTimeSsm = m_config->GlobalConfig()->MarketCloseTime() / 1000;

    if(m_marketStartTimeSsm > 0)
    {
        auto now = RTXUtils::NowSsm();
        if(now < m_marketStartTimeSsm)
            m_orderSession = OrderSession::Pre_Market_Order;
        else if(now >= m_marketStartTimeSsm && now < m_marketEndTimeSsm)
            m_orderSession = OrderSession::In_Market_Order;
        else
            m_orderSession = OrderSession::Post_Market_Order;
    }
    else
        m_marketStartTimeSsm = m_config->GlobalConfig()->MarketOpenTime() / 1000;

//    m_logger->logInfo("oid:%d, orderSession %d.", order->orderID_, m_orderSession);
    SetEffectivePrice();
}

RTXOrder::~RTXOrder()
{


}

void RTXOrder::Cleanup()
{
    if(m_isCleaning)
        return;
    m_isCleaning = true;

    UnsubscribeMarketData();
//    m_orderIO->RemoveParentOrder(m_clientOrder->orderID_);
}

void RTXOrder::InitOrder()
{
    if(m_orderIsDone == true)
        return;
    SendParentAck();

    if(OnFundamentals() == false)
        return;
    OrderParamRerouteExchangeLevel();
    LoadArgos();
//    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    {
//        if((m_isPMP && !m_isRetailRouting ) || m_reroute || m_isRetailRoutPMP)
//        if(m_isPMP || m_reroute || m_isRetailRoutPMP)
        if( m_isRetailSWPPMP
            || (m_isRetailRoutPMP && m_isRetailSWPDone)
            || (m_isPMP && m_isRetailSWPDone && !m_isRetailRouting )
            || m_reroute == true)
            ProcessOrders();
        if(m_reroute == true)
            return;
    }
    SubscribeMarketData();
}

bool RTXOrder::InitValidate()
{
    char buf[128];
//    if(m_cpu->m_marketClosed)
//    {
//        sprintf(buf, "Market day closed.");
//        m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
//        RejectParent(buf);
//        return false;
//    }
    if(m_cpu->m_postMarketEnded)
    {
        sprintf(buf, "Post market session ended.", m_clientOrder->orderID_);
        m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
        RejectParent(buf); 
        return false;
    }
    if(!OrderParamActions()) {
        return false;
    }
    if(m_clientOrder->type_ == OrderType::TYPE_MARKET)
    {
//        int now = RTXUtils::NowSsm();
        if(m_cpu->GetMarketSession() == MarketSession::Post_Market)
        {
            sprintf(buf, "market order %d is not accepted at post market time.", m_clientOrder->orderID_);
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
            RejectParent(buf);
            return false;
        }
        if (m_userConfig->acceptMarketOrder == false || m_accountConfig->acceptMarketOrder == false)
        {
            sprintf(buf, "User(%s/%s) does not accept market order.", m_clientOrder->logInId_.c_str(), m_clientOrder->subAccount_.c_str());
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
            RejectParent(buf);
            return false;
        }
        if(m_clientOrder->TIF_ == TimeInForce::TIF_IOC)
        {
            sprintf(buf, "IOC Market Order not supported.");
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
            RejectParent(buf);
            return false;
        }
        if(m_clientOrder->minQty_ > 0)
        {
            sprintf(buf, "Market order with Min Qty not supported.");
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
            RejectParent(buf);
            return false;
        }
    }
    if(m_clientOrder->minQty_ > m_clientOrder->size_)
    {
        sprintf(buf, "Min Qty more than Order Qty.");
        m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
        RejectParent(buf);
        return false;
    }
    if(m_clientOrder->expireTime_ > 0)
    {
        m_expireTimeSsm = GetExpireTimeSsm();
        int now = RTXUtils::NowSsm();
        if(now > m_expireTimeSsm)
        {
            sprintf(buf, "GTT expire time is already passed...");
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, buf);
            RejectParent(buf);
            return false;
        }
    }
    return true;
}

void RTXOrder::ProcessOrders()
{
//    std::cout << "ProcessOrders" << std::endl;
    if(ProcessValidation() == false)
        return;

    for (auto argo : m_argoPtr)
    {
        if(!argo->InitOrder())
            break;
        if(argo.get()->ProcessOrder()== true)
            break;
    }
    TryToStartGttExpireTimer();
    //    SendParentAck();
}

bool RTXOrder::ProcessValidation()
{
    if(m_isCleaning || m_orderIsCancelling || m_orderIsDone)
        return false;
//    std::cout << "ProcessValidation: -- " << boolalpha << "m_isRetailSwpPMP:" << m_isRetailSWPPMP
//            << boolalpha  <<  ", m_isPMP:" << m_isPMP
//            << boolalpha  <<  ", m_isRetailRouting:" << m_isRetailRouting
//            << boolalpha  <<  ", m_reroute:" << m_reroute
//            << boolalpha  <<  ", m_isRetailRoutPMP:" << m_isRetailRoutPMP
//            << std::endl;
    if( m_isRetailSWPPMP
        || (m_isRetailRoutPMP && m_isRetailSWPDone)
        || (m_isPMP && m_isRetailSWPDone && !m_isRetailRouting )
        || m_reroute)
        return true;
//    if(m_clientOrder->type_ == OrderType::TYPE_LIMIT)
    {
        if(!m_isSWPDone || !m_isRetailSWPDone || m_isRetailRouting){
            if(!m_validBbo)
                return false;
        }
        else if (!m_validTob){
            return false;
        }
    }
    return true;
}

void RTXOrder::SubscribeMarketData()
{
    int oId = m_clientOrder->orderID_;
    std::string symb = m_clientOrder->symbol_;

//    auto bbo = m_MktDataIO->SubscribeBbo(symb, oId);
//    if(!bbo.m_symbol.empty())
//        OnBbo(bbo, true);

    StartMarketDataFailureTimeOutTimerSwp();
    StartMarketDataFailureTimeOutTimerTarg();
    auto tobV = m_MktDataIO->SubscribeTOB(symb, oId, this);
//    if(!tobV.empty())
//    {
//        for(auto tob : tobV)
//        {
//            OnTob( *tob, true );
//        }
//        PrintTob(m_tobList.GetTobList(), true);
//        m_firstTob = false;
//    }
//    if(m_tobList.GetTobList().empty())
//    StartMarketDataFailureTimeOutTimerSwp();
//    StartMarketDataFailureTimeOutTimerTarg();
    m_marketDataUnsubscribed = false;

//    if(m_exchange->mktOrderPrimaryOpenCheck == false)
//    {
//        m_validTrade = true;
//        return;
//    }
//    auto oTrade = m_MktDataIO->RequestOpeningTrade(symb, oId);
//    if(!oTrade.m_symbol.empty())
//        OnOpeningTrade(oTrade, true);
}

void RTXOrder::UnsubscribeMarketData()
{
    if(m_marketDataUnsubscribed)
        return;
    m_marketDataUnsubscribed = true;
    m_MktDataIO->RemoveTOBSubscription(m_clientOrder->symbol_, m_clientOrder->orderID_);
    m_logger->logInfo("oid:%d, Unsubscribe market data.", m_clientOrder->orderID_);
//    m_MktDataIO->RemoveBboSubscription(m_clientOrder->symbol_);
//    m_MktDataIO->CancelOpeningTradeRequest(m_clientOrder->symbol_);
}

void RTXOrder::OnTob( const BatchTOB& tobV, bool isInit)
{
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    if(m_isCleaning || m_orderIsCancelling || m_orderIsDone)
        return;
    if (!tobV.empty()) {
        for (auto tob : tobV) {
            OnTob(tob, isInit);
        }
        if(!m_tobList.GetTobList().empty()) {
            if (m_firstTob) {
                m_firstTob = false;
                PrintTob(m_tobList.GetTobList(), true);
            }
        }
        if(!m_validBbo || (!m_isSWPDone || !m_isRetailSWPDone) && !m_ReceivedValidBbo)
        {
            Price bestPx = m_tobList.GetBestQuotePrice();
            if(IsMarketable(EffectivePrice(), bestPx))
            {
                if(IsBuyOrder())
                    m_nbboAsk = bestPx;
                else
                    m_nbboBid = bestPx;
                m_validBbo = true;
                m_logger->logInfo("oid:%d, No nbbo, Use valid tob as nbbo.", m_clientOrder->orderID_);
            }
        }
    }
    if(m_validTob == true)
        ProcessOrders();
//    if(m_validTob == false) {
//        if (m_tobList.GetTobList().empty() && !m_marketDataFailureTimeOutTimer)
//            StartMarketDataFailureTimeOutTimer();
//    }
}

void RTXOrder::OnTob( const TopOfBook& topOfBook, bool isInit )
{
    int pri = 0;
    auto mmid = topOfBook.m_mpid;

    auto itr1 =  m_config->GlobalConfig()->QuoteFilters().find(mmid);
    if(itr1 != m_config->GlobalConfig()->QuoteFilters().end())
    {
//        int now = RTXUtils::NowSsm();
        if((itr1->second->premarket == false && m_cpu->GetMarketSession() == MarketSession::Pre_Market)
            || (itr1->second->postmarket == false && m_cpu->GetMarketSession() == MarketSession::Post_Market ))
            return;
    }

    auto itr = m_exchange->targeting->MDIDmapping.find(mmid);
    if(itr != m_exchange->targeting->MDIDmapping.end())
    {
        auto route = m_config->RouterConfig()->Route( itr->second );
        if(route)
            pri = route->priority;
    }
    m_tobList.AddTob(topOfBook, pri);

//    if(isInit == false) {
//        if(IsBuyOrder())
//            m_logger->logInfo(" %s %.02f/%d, ", topOfBook.m_mpid.c_str(), topOfBook.m_askPrice.ToDouble(), topOfBook.m_askSize);
//        else
//            m_logger->logInfo(" %s %.02f/%d, ", topOfBook.m_mpid.c_str(), topOfBook.m_bidPrice.ToDouble(), topOfBook.m_bidSize);
//        PrintTob();
//        ProcessOrders();
//    }

    if(m_validTob == false || m_isForcedTob)
    {
//        std::cout << "Got Tob." << std::endl;
//        m_logger->logInfo("Got Tob");
        m_validTob = true;
        m_isForcedTob = false;
        //////////////////////
        if(m_marketDataFailureTimeOutTimerTarg) {
            if(m_timerScheduler->remove(m_marketDataFailureTimeOutTimerTarg->m_timerId))
                m_logger->logInfo("oid:%d, Remove TOB Timer.", m_clientOrder->orderID_);
        }
    }
}

void RTXOrder::PrintTob(const std::vector < std::shared_ptr< TobQuote >>& quoteList, bool isCached)
{
    std::string printStr;
    char buf[32];

    if(isCached)
        printStr = "Init Snap Shot: oid:" + std::to_string(m_clientOrder->orderID_) + ", ";
    printStr += m_clientOrder->symbol_ + ":";
    bool found = false;
    for( auto tob : quoteList)
    {
        if(!isCached)
        {
            if(!IsMarketable(m_clientOrder->price_, tob->m_px))
                break;
        }
        if(m_clientOrder->price_.isLE(Price(1.00)))
            sprintf(buf, " %s %.04f/%d,", tob->m_mpid.c_str(), tob->m_px.ToDouble(), tob->m_qty);
        else
            sprintf(buf, " %s %.02f/%d,", tob->m_mpid.c_str(), tob->m_px.ToDouble(), tob->m_qty);
        printStr += buf;
        found = true;
    }
    if(found)
        m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_ , printStr.c_str());
}

void RTXOrder::OnBbo( const Bbo& bbo, bool isInit )
{
//    m_logger->logInfo("RTXOrder-%d got OnBbo update.", m_clientOrder->orderID_);
    if(m_isCleaning || m_orderIsCancelling || m_orderIsDone)
        return;
    if(bbo.m_symbol.empty())
        return;

    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    if((IsBuyOrder() && bbo.m_bboSell == Price(0.0)) || (!IsBuyOrder() && bbo.m_bboBuy == Price(0.0)))
        return;
    m_nbboBid = Price(bbo.m_bboBuy);
    m_nbboAsk = Price(bbo.m_bboSell);

    if(m_ReceivedValidBbo == false)
        m_ReceivedValidBbo = true;

    if(m_validBbo == false)
    {
        m_logger->logInfo("Received NBBO: oid:%d, bid:%.2f, ask:%.2f", m_clientOrder->orderID_, bbo.m_bboBuy.ToDouble(), bbo.m_bboSell.ToDouble());
        m_validBbo = true;
        //////////////////////////
//        if(!m_isSWPDone && m_marketDataFailureTimeOutTimerSwp) {
        if(m_marketDataFailureTimeOutTimerSwp) {
            if(m_timerScheduler->remove(m_marketDataFailureTimeOutTimerSwp->m_timerId)) {
                m_validTob = true;
                m_logger->logInfo("oid:%d, Remove SWP Timer.", m_clientOrder->orderID_);
                m_isSWPTimerStarted = false;
            }
        }
    }
    if(isInit == false)
        ProcessOrders();
}

void RTXOrder::OnOpeningTrade(const OpenTrade& oTrade, bool isInit)
{
    if(m_isCleaning || m_orderIsCancelling || m_orderIsDone)
        return;
    if(oTrade.m_symbol.empty())
        return;
    if (m_validTrade == false)
    {
        m_logger->logInfo("Received open trade: oid: d, %.2f", m_clientOrder->orderID_, oTrade.m_tradePrice.ToDouble());
        m_validTrade = true;
    }
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    if(isInit == false)
        ProcessOrders();
}

std::shared_ptr<Order> RTXOrder::CreateChildOrder()
{
    char ciodStr[32];
    auto ordr = std::make_shared<Order>(*m_clientOrder->Clone());
    ++m_coidCnt;
    sprintf(ciodStr, "c_%d_%d", ordr->orderID_, m_coidCnt);
    ordr->childOrderId_ = ciodStr;
    ordr->transactTime_ = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    ordr->expireTime_ = 0;

    return ordr;
}

bool RTXOrder::SendChildOrder(std::shared_ptr<Order>& order,  const ChildOrderType& type)
{
    int tempTif = 0;
    if(order->TIF_ > 9) {
        tempTif = order->TIF_;
        order->TIF_ = 0;
    }
    if(m_clientOrder->displaySize_)
    {
        if(type == ChildOrderType::CHILD_ORDER_TYPE_RETAIL)
            order->displaySize_ = 0;
        else {
            if (order->TIF_ == TimeInForce::TIF_Day) {
                if (order->size_ > m_clientOrder->displaySize_)
                    order->displaySize_ = m_clientOrder->displaySize_;
                else
                    order->displaySize_ = 0;
            } else if (order->TIF_ == TimeInForce::TIF_IOC)
                order->displaySize_ = 0;
        }
    }
    if(m_clientOrder->discretion_ > Price(0.0))
    {
        if(order->TIF_ == TimeInForce::TIF_IOC)
            order->discretion_ = 0;
    }
    m_orderIO->SendChildOrder(order);
    m_logger->logInfo("Send ChildOrder-%s: oid:%d, coid:%s, side:%s, type:%s, symbol:%s, exch:%s, price:%.4f, disc:%.4f, size:%d, max:%d, tif:%s, minQty:%d, route:%s ",
                      ChildOrderTypeString(type).c_str(), order->orderID_, order->childOrderId_.c_str(), OrderSideString(order->side_).c_str(),
                      OrderTypeString(order->type_).c_str(), order->symbol_.c_str(), order->exchange_.c_str(), order->price_.ToDouble(), order->discretion_.ToDouble(),
                      order->size_, order->displaySize_, OrderTifString(order->TIF_).c_str(), order->minQty_, order->routeName_.c_str());

    m_qtyPending += order->size_;
    m_qtyUncommited -= order->size_;
    auto childOrder = std::make_shared<ChildOrder>(ChildOrder( order, type));
    m_childOrderInfo.insert(order->childOrderId_, childOrder);
    m_pendingCoIdList.insert(order->childOrderId_);
    TryStartCodTimer(tempTif, order->childOrderId_);

    return true;
}

void RTXOrder::CancelChildOrder( const std::string& coid, bool fromCancellAll)
{
    if(m_pendingCancelCoIdList.find(coid) != m_pendingCancelCoIdList.end())
        return;
    auto child = m_childOrderInfo.GetChildByCoid(coid);
    if(child->m_order->TIF_ == TimeInForce::TIF_IOC)
        return;

    m_logger->logInfo("Send cancel request for oid:%d, %s", m_clientOrder->orderID_ , coid.c_str());
    m_orderIO->CancelChildOrderRequest(m_clientOrder->orderID_, coid);
    auto pr = std::make_pair(coid, 1);
    m_pendingCancelCoIdList.insert(pr);
//    m_logger->logInfo("CancelChildOrder-1 for %s, m_pendingCoIdList size: %d", coid.c_str(), m_pendingCoIdList.size());

    if(!fromCancellAll) {
//        m_logger->logInfo("CancelChildOrder-2 erase id for %s", coid.c_str());
//        m_pendingCoIdList.erase(coid);
    }
}

bool RTXOrder::CancelAllChildOrders()
{
//    m_logger->logInfo("CancelAllChildOrders=1.  m_pendingCoIdList size: %d", m_pendingCoIdList.size());
    for(auto coid : m_pendingCoIdList)
        CancelChildOrder(coid, true);

//    m_logger->logInfo("CancelAllChildOrders=2.  m_pendingCoIdList size: %d", m_pendingCoIdList.size());
//    m_pendingCoIdList.clear();
//    m_logger->logInfo("CancelAllChildOrders=3, clear(). m_pendingCoIdList size: %d", m_pendingCoIdList.size());
    return true;
}

void RTXOrder::CancelParent(const std::string& message, bool userInitiated )
{
    if(m_isCleaning || m_orderIsCancelling || m_orderIsDone)
        return;
    m_orderIsCancelling = true;
    m_logger->logInfo("CancelParent: oid:%d, %s", m_clientOrder->orderID_, message.c_str());

//    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    if(QtyPending() <= 0)
    {
        SendCancelParentExRprt();
        return;
    }
    CancelAllChildOrders();
}

void RTXOrder::RejectParent(const std::string& message)
{
    if(m_orderIsCancelling == true)
        return;
    m_orderIsCancelling = true;
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);

    auto er = m_clientOrder->CreatER();
    er->execId_ = std::to_string(++m_execidCnt);
    er->status_ = ERSTATUS::REJECTED;
    er->size_ = m_clientOrder->size_;
    er->fillSize_ = 0;  //m_qtyFilled;
    er->cumulativeFillQuantity_ = 0;        //m_qtyFilled;
    er->leaveQty = m_clientOrder->size_;    //m_qtyRemain;
    er->fillPrice_ = 0.0;
    er->avgPrice_ = 0.0;
    er->execType_ = '8';        //'F' for fill, '4' for cancel

//    er->exchange_ = message;
//    er->transactTime_ = er->transactTime_;

    m_logger->logInfo("RejectParent: oid:%d, symbol:%s, status:%d, size:%d, fillSize:%d, cumFillSize:%d, fillPx:%.2f, AvgPx:%.2f",
                      er->orderId_, er->symbol_.c_str(), er->status_, er->size_, er->fillSize_, er->cumulativeFillQuantity_, er->fillPrice_.ToDouble(), er->avgPrice_.ToDouble());

    m_orderIO->SendExecutionReport(er.get());
    Cleanup();
}

void RTXOrder::EndOfDayCancelParents(bool isPostMarketCancel, const std::string& message)
{
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    if(!isPostMarketCancel && m_orderSession < OrderSession::Post_Market_Order) {
        if((m_expireTimeSsm * 1000) >= m_cpu->m_marketEndTimeMssm)
            return;
        CancelParent(message);
    }
    else
        CancelParent(message);
}

void RTXOrder::CancelParentByClient(const std::string& message)
{
    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);
    CancelParent(message, true);
}

void RTXOrder::OnChildExecutionReport( const ExecutionReport* er )
{
    if(m_orderIsDone)
        return;
    m_logger->logInfo("Received ExecReport: oid:%d, coid:%s, symbol:%s, status:%d, exch:%s, size:%d, fillSize:%d, cumulativefillSize:%d, fillPrice:%.2f",
                      er->orderId_, er->childOrderId_.c_str(), er->symbol_.c_str(), er->status_, er->exchange_.c_str(),
                      er->size_, er->fillSize_, er->cumulativeFillQuantity_, er->fillPrice_.ToDouble());

    std::lock_guard<std::recursive_timed_mutex> lk(m_mutex);

    auto child = m_childOrderInfo.GetChildByCoid(er->childOrderId_);
    if(child->m_childOrderType == ChildOrderType::CHILD_ORDER_TYPE_UNAVAILABLE)
    {
        m_app->m_logger->logErr("ExecReport. oid:%d, Cannot find child order Id: %s", m_clientOrder->orderID_ , er->childOrderId_.c_str());
        return;
    }
    QtyUpdate(er);
    TryToStopTimer(er, *m_codTimer);

    switch (er->status_) {
        case ERSTATUS::OPEN:
            OnOpen(er, child);
            break;
        case ERSTATUS::FILLED:
        case ERSTATUS::PARTIALY_FILLED:
            OnFill(er, child);
            break;
        case ERSTATUS::CANCELED:
            OnCancel(er, child);
            break;
        case ERSTATUS::REJECTED:
            OnReject(er, child);
            break;
        default:
            break;
    }
    if(m_isCleaning)
        return;
    bool argoRtn = false;
    for (auto argo : m_argoPtr)
    {
        argoRtn = argo.get()->ProcessChildOrderER(er, child);
        if(argoRtn == true)
            break;
    }
    if(argoRtn == false)
        ProcessOrders();
}

void RTXOrder::OnOpen(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{

}

void RTXOrder::OnFill(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if(er->status_ == ERSTATUS::FILLED) {
        m_pendingCancelCoIdList.erase(er->childOrderId_);
        ///////
        m_pendingCoIdList.erase(er->childOrderId_);
        m_childOrderInfo.RemoveChildByCoid(er->childOrderId_);
    }
    SendParentFillER(er, child);
}

void RTXOrder::OnCancel(const ExecutionReport* erIn, std::shared_ptr<ChildOrder>& child)
{
    if(m_orderIsCancelling == true && QtyPending() == 0)
    {
        SendCancelParentExRprt();
    }
    ///////
    m_pendingCoIdList.erase(erIn->childOrderId_);
    m_pendingCancelCoIdList.erase(erIn->childOrderId_);
    m_childOrderInfo.RemoveChildByCoid(erIn->childOrderId_);
}

void RTXOrder::SendCancelParentExRprt()
{
    m_orderIsDone = true;
    shared_ptr<ExecutionReport> er;
    er = m_clientOrder->CreatER();
    er->execId_ = std::to_string(++m_execidCnt);
    er->status_ = ERSTATUS::CANCELED;
    er->size_ = m_clientOrder->size_;
    er->fillSize_ = m_qtyFilled;
    er->cumulativeFillQuantity_ = m_qtyFilled;
    er->leaveQty = m_qtyRemain;

    er->fillPrice_ = AvgPx();
    er->avgPrice_ = AvgPx();
    er->execType_ = '4';        //'4' for cancel 'F' for fill

//    er->transactTime_ = er->transactTime_;

    m_logger->logInfo("Send Parent Cancel ExecReport: oid:%d, symbol:%s, status:%d, exch:%s, "
                      "size:%d, fillSize:%d, cumFillSize:%d, fillPx:%.2f, AvgPx:%.2f",
                      er->orderId_, er->symbol_.c_str(), er->status_, er->exchange_.c_str(),
                      er->size_, er->fillSize_, er->cumulativeFillQuantity_, er->fillPrice_.ToDouble(), er->avgPrice_.ToDouble());

    m_orderIO->SendExecutionReport(er.get());
    Cleanup();
}

void RTXOrder::OnReject(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    ///////
    m_pendingCoIdList.erase(er->childOrderId_);
    m_pendingCancelCoIdList.erase(er->childOrderId_);
    m_childOrderInfo.RemoveChildByCoid(er->childOrderId_);
}


void RTXOrder::SendParentFillER(const ExecutionReport* erIn, std::shared_ptr<ChildOrder>& child)
{
    shared_ptr<ExecutionReport> er;
    if(QtyRemain() == 0)
    {
        m_orderIsDone = true;

        auto erFill = m_clientOrder->CreatER();
        erFill->status_ = ERSTATUS::FILLED;
        erFill->fillSize_ = erIn->fillSize_;
        erFill->exchange_ = erIn->exchange_;
        erFill->cumulativeFillQuantity_ = m_clientOrder->size_;
        erFill->leaveQty = 0;
        erFill->fillPrice_ = erIn->fillPrice_;
        erFill->avgPrice_ = AvgPx();
        erFill->execId_ = std::to_string(++m_execidCnt);
        erFill->execType_ = 'F';        //'4' for cancel
        //erFill->transactTime_ = erIn->transactTime_;

        m_logger->logInfo("Send Parent Fulfill ER: oid:%d, symbol:%s, status:%d, exch:%s, "
                          "size:%d, fillSize:%d, cumFillSize:%d, fillPx:%f, AvgPx:%f",
                          erFill->orderId_, erFill->symbol_.c_str(), erFill->status_, erFill->exchange_.c_str(),
                          erFill->size_, erFill->fillSize_, erFill->cumulativeFillQuantity_, erFill->fillPrice_.ToDouble(), erFill->avgPrice_.ToDouble());

        m_orderIO->SendExecutionReport(erFill.get());
        Cleanup();
    }
    else
    {
        auto order = child->m_order;
        er = order->CreatER();
        er->execId_ = std::to_string(++m_execidCnt);
        er->status_ = ERSTATUS::PARTIALY_FILLED;
        er->size_ = m_clientOrder->size_;
        er->fillSize_ = erIn->fillSize_;
        er->cumulativeFillQuantity_ = m_qtyFilled;
        er->leaveQty = m_qtyRemain;

        er->fillPrice_ = erIn->fillPrice_;
        er->avgPrice_ = AvgPx();
        er->execType_ = 'F';        //'4' for cancel

        er->transactTime_ = erIn->transactTime_;

        m_logger->logInfo("Send Parent ExecReport: oid:%d, symbol:%s, status:%d, exch:%s, "
                          "size:%d, fillSize:%d, cumFillSize:%d, fillPx:%.2f, AvgPx:%.2f",
                          er->orderId_, er->symbol_.c_str(), er->status_, er->exchange_.c_str(),
                          er->size_, er->fillSize_, er->cumulativeFillQuantity_, er->fillPrice_.ToDouble(), er->avgPrice_.ToDouble());

        m_orderIO->SendExecutionReport(er.get());
    }
}

void RTXOrder::SendParentAck()
{
//    if(m_isParentAcked == true)
//        return;

    auto ack = std::make_shared<ExecutionReport>();
    ack->orderId_ = m_clientOrder->orderID_;
    ack->status_ = 0;
    ack->symbol_ = m_clientOrder->symbol_;
    ack->side_ = m_clientOrder->side_;
    ack->size_ = m_clientOrder->size_;
    ack->cumulativeFillQuantity_ = 0;
    //ack->transactTime_ = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    m_logger->logInfo("Send Parent Ack: oid:%d, symbol:%s, status:%d, size:%d",
                      ack->orderId_, ack->symbol_.c_str(), ack->status_, ack->size_);

    m_orderIO->SendParentAck(ack.get());
//    m_isParentAcked = true;
}

bool RTXOrder::OnFundamentals()
{
    if(m_reroute == true)
        return true;
    std::string secName;
    std::string msg;
    if(m_clientOrder->minQty_ > 0)
    {
        char buf[128];
        if(!m_config->GlobalConfig()->MinQtySecIdfnIdentifier().empty()) {
            secName = m_config->GlobalConfig()->MinQtySecIdfnIdentifier();
            msg = "MinQtySecId";

            m_exchange = m_config->SecIdfnsConfig()->GetExchange(secName);
            if(m_exchange->secIdfn.empty()) {
                sprintf(buf, "Min Qty not supported.");
                m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_ , buf);
                RejectParent(buf);
                return false;
            }
        }
        else
        {
            sprintf(buf, "Min Qty not supported.");
            m_app->m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_ , buf);
            RejectParent(buf);
            return false;
        }
    }
    else if(!m_config->GlobalConfig()->UserSecIdfnIdentifier(m_clientOrder->logInId_).empty())
    {
        secName = m_config->GlobalConfig()->UserSecIdfnIdentifier(m_clientOrder->logInId_);
        msg = "UserSecId";
    }
    else if(!m_config->GlobalConfig()->AccountSecIdfnIdentifier(m_clientOrder->subAccount_).empty())
    {
        secName = m_config->GlobalConfig()->AccountSecIdfnIdentifier(m_clientOrder->subAccount_);
        msg = "AccountSecId";
    }
    else if(!m_config->GlobalConfig()->SymbolSecIdfnIdentifier(m_clientOrder->symbol_).empty())
    {
        secName = m_config->GlobalConfig()->SymbolSecIdfnIdentifier(m_clientOrder->symbol_);
        msg = "SymbolSecId";
    }
    else
    {
        secName = m_cpu->m_secMasterBox->GetExchangeName(m_clientOrder->symbol_);
        msg = "secMasterSecId";
    }
    ProcessFundamentalData(secName, msg);
    return true;
}

void RTXOrder::ProcessFundamentalData(std::string secName, std::string msg)
{
    bool found = false;
    if(!secName.empty())
    {
        m_exchange = m_config->SecIdfnsConfig()->GetExchange(secName);
        if(!m_exchange->secIdfn.empty()) {
            found = true;
            m_logger->logInfo("oid:%d, %s, Exchange: %s", m_clientOrder->orderID_ , msg.c_str(), m_exchange->secIdfn.c_str());
        }
    }
    else
    {
        std::string defaultExch = m_config->GlobalConfig()->DefaultSecIdfn();
        m_exchange = m_config->SecIdfnsConfig()->GetExchange(defaultExch);
        found = true;
        m_logger->logInfo("oid:%d, No SecId found, use default exchange: %s", m_clientOrder->orderID_ , defaultExch.c_str());
    }

    if(found == false)          //in case config error
    {
        std::string defaultExch = m_config->GlobalConfig()->DefaultSecIdfn();
        m_exchange = m_config->SecIdfnsConfig()->GetExchange(defaultExch);
        m_logger->logInfo("oid:%d, Config error, SecId: %s, use default exchange: %s", m_clientOrder->orderID_ , secName.c_str(), defaultExch.c_str());
    }
    m_tobList.SetDefinedMmid(m_exchange->targeting->definedMmid);
    m_validFundamental = true;
}


void RTXOrder::LoadArgos()
{
    if(m_ArgoAllStarted == true)
        return;

    if(m_reroute)
    {
        auto ptr = std::make_shared<Reroute>(this);
        m_argoPtr.push_back(ptr);
        return;
    }
    char msg[256];
    std::string msgStr;
    for(auto i=0; i < m_config->GlobalConfig()->argos.size(); ++i)
    {
        bool enabled = false;
        std::string name = m_config->GlobalConfig()->argos[i]->name;
//        std::cout << "LoadArgos, name: " << name << std::endl;
//        m_logger->logInfo("LoadArgos: %s", name.c_str());

//        if(name == AppConstants::ARGO_RetailSWP_str || name == AppConstants::ARGO_RetailRouting_str)
//        {
//            if (m_userConfig->argosMp[name] <= 0)
//                enabled = false;
//        }
//        else if (m_userConfig->argosMp[name] > -1)
        if (m_userConfig->argosMp[name] > -1)
        {
            if (m_userConfig->argosMp[name] == 1)
            {
                if(name == AppConstants::ARGO_RetailSWP_str || name == AppConstants::ARGO_RetailRouting_str)
                {
                    if(m_config->GlobalConfig()->argos[i]->enabled == true)
                        enabled = true;
                    else
                        enabled = false;
                }
                else
                    enabled = true;
            }
            else
                enabled = false;
//            std::cout << "m_userConfig, name: " << name << ", enabled: " << boolalpha << enabled << std::endl;
//            m_logger->logInfo("m_userConfig, name: %s, enabled: %s", name.c_str(), (enabled)? "true" : "false");
            sprintf(msg,"m_userConfig, name: %s, enabled: %s", name.c_str(), (enabled)? "true" : "false");
        }
        else if(m_accountConfig->argosMp[name] > -1)
        {
            if (m_accountConfig->argosMp[name] == 1)
            {
                if(name == AppConstants::ARGO_RetailRouting_str)
                {
                    if(m_config->GlobalConfig()->argos[i]->enabled == true)
                        enabled = true;
                    else
                        enabled = false;
                }
                else
                    enabled = true;
            }
            else
                enabled = false;
//            std::cout << "m_accountConfig, name: " << name << ", enabled: " << boolalpha << enabled << std::endl;
//            m_logger->logInfo("m_accountConfig, name: %s, enabled: %s", name.c_str(), (enabled)? "true" : "false");
            sprintf(msg,"m_accountConfig, name: %s, enabled: %s", name.c_str(), (enabled)? "true" : "false");
        }
        else if(m_config->GlobalConfig()->argos[i]->enabled == true)
        {
            if(name != AppConstants::ARGO_RetailRouting_str && name != AppConstants::ARGO_RetailSWP_str)
            {
                enabled = true;
//              std::cout << "GlobalConfig, name: " << name << ", enabled: " << boolalpha << enabled << std::endl;
//                m_logger->logInfo("GlobalConfig, name: %s, enabled: %s", name.c_str(), (enabled) ? "true" : "false");
                sprintf(msg,"GlobalConfig, name: %s, enabled: %s", name.c_str(), (enabled) ? "true" : "false");
            }
            else
                sprintf(msg,"%s is not enabled", name.c_str());
//                m_logger->logInfo("%s is not enabled", name.c_str());
        }
        else
        {
            sprintf(msg,"%s is not enabled", name.c_str());
//            m_logger->logInfo("%s is not enabled", name.c_str());
        }
        if (name == AppConstants::ARGO_PassivePosting_str)
        {
            if (m_exchange->multiPosting->enabled)
            {
                msgStr += ("MultiPosting: ");
//                sprintf(msg,"MultiPosting is enabled");
            }
            else
                msgStr += (name + ": ");
        }
        else
        {
            msgStr += (name + ": ");
        }
//        m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_ , msg);

        if(enabled)
        {
            if (name == AppConstants::ARGO_RetailSWP_str)
            {
                if (m_exchange->retailSWP->enabled == false)
                {
                    msgStr += ("Disabled.| ");
                    continue;
                }
                auto now = RTXUtils::NowMssm();
                if(now < m_config->GlobalConfig()->RetailSWPStartTime() || now >= m_config->GlobalConfig()->RetailSWPEndTime())
                {
                    msgStr += ("Enabled, but out of session.| ");
//                    m_logger->logInfo("oid:%d, Out of SWP session, No SWP.", m_clientOrder->orderID_ );
                    continue;
                }
                msgStr += ("Enabled");
                auto ptr = std::make_shared<DarkSweep>(this);
                ptr->SetRetailSwp(true);
                m_argoPtr.push_back(ptr);
                m_isRetailSWPDone = false;
                m_isRetailSWPInit = true;
                if(m_exchange->retailSWP->sWPPriceCheckEnbaled == false)
                    m_isRetailSWPPMP = true;
                msgStr += (".| ");
            }
            else if (name == AppConstants::ARGO_RetailRouting_str)
            {
                auto now = RTXUtils::NowMssm();
                if(now < m_config->GlobalConfig()->RetailRoutingStartTime() || now >= m_config->GlobalConfig()->RetailRoutingEndTime())
                {
                    msgStr += ("Enabled, but out of Retail Routing session, no RetailRouting.| ");
//                    m_logger->logInfo("oid:%d, Out of retail market session", m_clientOrder->orderID_ );
                    continue;
                }
                msgStr += ("Enabled");
                auto ptr = std::make_shared<RetailRouting>(this);
                m_argoPtr.push_back(ptr);
                m_isRetailRouting = true;
                m_isRetailRoutingInit = true;
                if(ptr->m_useOrderLimitPrice)
                {
                    msgStr += (", useOrderLimitPrice");
//                    m_logger->logInfo("oid:%d, use useOrderLimitPrice.", m_clientOrder->orderID_ );
                    m_isRetailRoutPMP = true;
                }
                msgStr += (".| ");
            }
            else if (name == AppConstants::ARGO_DarkSweep_str)
            {
                if (m_exchange->sWP->enabled == false)
                {
                    msgStr += ("Disabled.| ");
                    continue;
                }
//                if(m_orderSession != OrderSession::In_Market_Order) {
                auto now = RTXUtils::NowMssm();
                if(now < m_config->GlobalConfig()->SWPStartTime() || now >= m_config->GlobalConfig()->SWPEndTime())
                {
                    msgStr += ("Enabled, but out of session.| ");
//                    m_logger->logInfo("oid:%d, Out of SWP session, No SWP.", m_clientOrder->orderID_ );
                    continue;
                }
                msgStr += ("Enabled");
                auto ptr = std::make_shared<DarkSweep>(this);
                ptr->SetRetailSwp(false);
                m_argoPtr.push_back(ptr);
                m_isSWPDone = false;
                m_isSWPInit = true;
                if(m_exchange->sWP->sWPPriceCheckEnbaled == false)
                    m_isPMP = true;
                msgStr += (".| ");
            }
            else if (name == AppConstants::ARGO_Targeting_str)
            {
                msgStr += ("Enabled.| ");
                auto ptr = std::make_shared<Targeting>(this);
                m_argoPtr.push_back(ptr);
            }
            else if (name == AppConstants::ARGO_PassivePosting_str)
            {
                msgStr += ("Enabled.| ");
                if (m_exchange->multiPosting->enabled)
                {
                    auto ptr = std::make_unique<MultiPosting>(this);
                    m_argoPtr.push_back(std::move(ptr));
                }
                else {
                    auto ptr = std::make_shared<PassivePosting>(this);
//                    m_passivePostPtr = ptr;
                    m_argoPtr.push_back(std::move(ptr));
                }
            }
        }
        else
            msgStr += ("Disabled.| ");
    }
    if(m_argoPtr.empty())
    {
        CancelParent("No valid component is enabled." );
        return;
    }
    m_logger->logInfo("oid:%d, orderSession %d, %s", m_clientOrder->orderID_, m_orderSession, msgStr.c_str());
    m_ArgoAllStarted = true;
}

Price RTXOrder::NBBO()
{
    if(IsBuyOrder())
        return m_nbboAsk;
    else
        return m_nbboBid;
}

Price RTXOrder::NBBO_nearSide()
{
    if(IsBuyOrder())
        return m_nbboBid;
    else
        return m_nbboAsk;
}

void RTXOrder::SetEffectivePrice()
{
    if(m_clientOrder->discretion_.isLE(Price(0.0)) || m_clientOrder->discretion_.isGE(m_clientOrder->price_))
        m_effectivePrice = m_clientOrder->price_;
    else {
        if (IsBuyOrder())
            m_effectivePrice = m_clientOrder->price_ + m_clientOrder->discretion_;
        else
            m_effectivePrice = m_clientOrder->price_ - m_clientOrder->discretion_;
    }
}

bool RTXOrder::IsMarketable(Price p1, Price p2)
{
    bool isTrue = false;
    if(IsBuyOrder())
        isTrue = p1.isGE(p2);
//        isTrue = (p1 >= p2);
    else
        isTrue = p1.isLE(p2);
//        isTrue = (p1 <= p2);

    return isTrue;
}

bool RTXOrder::ExcludedRoute(const std::string& routeName)
{
    if(m_userConfig->excludeRouteList.find(routeName) != m_userConfig->excludeRouteList.end())
        return true;
    if(m_accountConfig->excludeRouteList.find(routeName) != m_accountConfig->excludeRouteList.end())
        return true;

    auto route = m_config->RouterConfig()->Route(routeName);
    if(!route)
        return true;
    if(ExcludedVenue(route->venue))
        return true;

    return false;
}
bool RTXOrder::ExcludedVenue(const std::string& venue)
{
    if(m_userConfig->excludeVenueList.find(venue) != m_userConfig->excludeVenueList.end())
        return true;
    if(m_accountConfig->excludeVenueList.find(venue) != m_accountConfig->excludeVenueList.end())
        return true;

    return false;
}

void RTXOrder::QtyUpdate( const ExecutionReport* er)
{
    switch (er->status_)
    {
        case ERSTATUS::FILLED:
        case ERSTATUS::PARTIALY_FILLED:
        {
            m_qtyPending -= er->fillSize_;
            m_qtyFilled += er->fillSize_;
            if(m_qtyPending < 0)
                m_qtyPending = 0;

            QtyPricePr pr(er->fillSize_, er->fillPrice_.value());
            m_qtyPxList.push_back(pr);
            break;
        }
        case ERSTATUS::CANCELED:
        case ERSTATUS::REJECTED:
        {
            m_qtyPending -= (er->size_ - er->cumulativeFillQuantity_);
            if(m_qtyPending < 0)
                m_qtyPending = 0;
            break;
        }
        default:
            break;
    }
    m_qtyRemain = m_qtyTotal - m_qtyFilled;
    m_qtyUncommited = m_qtyTotal - m_qtyFilled - m_qtyPending;
}

Price RTXOrder::AvgPx()
{
    double sum(0.0);
    int qtySum = 0;
    for(auto  pr : m_qtyPxList)
    {
        sum += pr.px * pr.qty;
        qtySum += pr.qty;
    }
    if(qtySum == 0)
        return Price(0.0);

    double avg = sum / qtySum;
    return Price(avg);
}

bool RTXOrder::OrderParamActions()
{
    std::unordered_map <int /* "3" */, std::shared_ptr<OrderParamAction>>::iterator itr;
    if((itr = m_config->GlobalConfig()->orderTypeActionList.find(m_clientOrder->type_)) != m_config->GlobalConfig()->orderTypeActionList.end())
    {
        int retn = Actions(itr->second.get(), m_clientOrder->type_,"Order type:", m_clientOrder->type_);
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    std::unordered_map <std::string /* "IBM" */, std::shared_ptr<OrderParamAction>>::iterator itr2;
    if((itr2 = m_config->GlobalConfig()->orderSymbolActionList.find(m_clientOrder->symbol_)) != m_config->GlobalConfig()->orderSymbolActionList.end())
    {
        int retn = Actions(itr2->second.get(), m_clientOrder->symbol_,"Symbol:");
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    if((itr = m_config->GlobalConfig()->orderTifActionList.find(m_clientOrder->TIF_)) != m_config->GlobalConfig()->orderTifActionList.end())
    {
        int retn = Actions(itr->second.get(), m_clientOrder->TIF_,"TIF:", m_clientOrder->TIF_);
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    if((itr2 = m_config->GlobalConfig()->userActionList.find(m_clientOrder->logInId_)) != m_config->GlobalConfig()->userActionList.end())
    {
        int retn = Actions(itr2->second.get(), m_clientOrder->logInId_,"User:");
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    if((itr2 = m_config->GlobalConfig()->accountActionList.find(m_clientOrder->subAccount_)) != m_config->GlobalConfig()->accountActionList.end())
    {
        int retn = Actions(itr2->second.get(), m_clientOrder->subAccount_,"Account:");
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    return true;
}

bool RTXOrder::OrderParamRerouteExchangeLevel()
{
    if(m_reroute == true)
        return true;
    std::unordered_map <int /* "3" */, std::shared_ptr<OrderParamAction>>::iterator itr;
    if((itr = m_exchange->orderTifActionList.find(m_clientOrder->TIF_) )!= m_exchange->orderTifActionList.end())
    {
        int retn = Actions(itr->second.get(), m_clientOrder->TIF_,"TIF:", m_clientOrder->TIF_);
        if(retn == 1) return false;
        else if(retn == 0) return true;
    }
    std::unordered_map <std::string /* "IBM" */, std::shared_ptr<OrderParamAction>>::iterator itr2;
    if(m_cpu->m_secMasterBox->GetIPO(m_clientOrder->symbol_))
    {
        if (!m_exchange->IPOActionList.empty())
        {
            int retn = Actions(m_exchange->IPOActionList.begin()->second.get(), "Y", "IPO:");
            if (retn == 1) return false;
            else if (retn == 0) return true;
        }
    }
    if(m_clientOrder->type_ == OrderType::TYPE_MARKET)
    {
        auto now = RTXUtils::NowSsm();
        if(now < m_marketStartTimeSsm)
        {
            m_reroute = true;
            std::string lstStr;
            if(!m_exchange->preMarketListingExchangeVenue.empty())
            {
                std::vector<std::string> list = RTXUtils::stringToVector(m_exchange->preMarketListingExchangeVenue);
                for(auto str : list)
                {
                    lstStr += str + ", ";
                    m_rerouteDestinationList.emplace_back(str);
                }
            }
            else if(!m_exchange->listingExchangeVenue.empty())
            {
                std::vector<std::string> list = RTXUtils::stringToVector(m_exchange->listingExchangeVenue);
                for(auto str : list)
                {
                    lstStr += str + ", ";
                    m_rerouteDestinationList.emplace_back(str);
                }
            }
            m_logger->logInfo("oid:%d, Market order pre-market, reroute - %s", m_clientOrder->orderID_, lstStr.c_str());
            return true;
        }
    }
    return true;
}

template<class T2>
int RTXOrder::Actions(OrderParamAction* itr, T2 type, std::string msgIn, int type2)
{
//    m_logger->logInfo(" Actions1: %s", msgIn.c_str());
    string tp;
    if(type2 >= 0)
        tp = std::to_string(type2);
    else
        tp = type;
    std::string msg = msgIn + tp;

    if(itr->action == "reject")
    {
        msg += ", reject.";
        m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, msg.c_str());
        RejectParent(msg);
        return 1;
    }
    else {
        m_reroute = true;
        m_rerouteDestinationList = itr->destinationList;
        msg += ", reroute - ";
        for(auto dest : itr->destinationList)
            msg += dest + " ";
        m_logger->logInfo("oid:%d, %s", m_clientOrder->orderID_, msg.c_str());
        return 0;
    }
}
