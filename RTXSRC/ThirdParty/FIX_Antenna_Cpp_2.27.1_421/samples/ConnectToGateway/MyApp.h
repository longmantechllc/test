/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#ifndef H_B2BITS_Engine_Samples_CTGW_MyApp_H
#define H_B2BITS_Engine_Samples_CTGW_MyApp_H

#include <B2BITS_Application.h>
#include <B2BITS_Session.h>
#include <B2BITS_SessionParameters.h>
#include <B2BITS_Semaphore.h>

#include "RefCounter.h"
#include "ApplicationProperties.h"

/// Application implementation (processes the incoming messages).
class MyApp : public Engine::Application
{
private: // fields
    System::Semaphore semaphore_;
    ApplicationProperties::SessionParameters params_;
	Engine::SessionExtraParameters eparams_;
    RefCounter<Engine::Session> session_;
    bool resendInProgress_;
    bool resendCommand_;
    System::Semaphore resendSemaphore_;
    bool sessionConfiguredWithEngineProperties_;

public: // methods
    /// Esatblishes new connection
    void connect();

    /// Esatblishes new connection
    void disconnect();

    /// Returns state of the connection
    Engine::Session::State getState() const {
        return session_->getState();
    }

    /// Sends message to the remote side
    void put( Engine::FIXMessage* msg ) {
        assert( NULL != msg );
        session_->put( msg );
    }

    /// Switch session to the another connection.
    /// When current is a primary - switch to the backup.
    /// When current is a backup - switch to the primary.
    void switchConnection();

    /// Returns true if application is processing Resend Request
    bool isResendInProgress() const;

    ///
    void postResendCommand( std::string const& command );

public: // constructors/destructor
    explicit MyApp( ApplicationProperties::SessionParameters const& params );
    ~MyApp();

private: // Engine::Application contract implementation
    /// A call-back method to process ingoing messages.
    virtual bool process( const Engine::FIXMessage& fixMsg, const Engine::Session& aSn );

    /// Blocks the calling thread until the session is closed.
    void waitUntilClosed();

    /// This call-back method is called to notify about Logon event.
    virtual void onLogonEvent( const Engine::LogonEvent* apEvent, const Engine::Session& aSn );

    /// This call-back method is called to notify about Logout event.
    virtual void onLogoutEvent( const Engine::LogoutEvent* apEvent, const Engine::Session& aSn );

    virtual void onMsgRejectEvent( const Engine::MsgRejectEvent* /*event*/, const Engine::Session& /*sn*/ ) {}

    /// This call-back method is called to notify about SequenceGap event.
    virtual void onSequenceGapEvent( const Engine::SequenceGapEvent* /*apEvent*/, const Engine::Session& /*aSn*/ ) {}

    /// This call-back method is called to notify about SessionLevelReject event.
    virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* /*apEvent*/, const Engine::Session& /*aSn*/ ) {}

    /**
    * This call-back method is called when a Heartbeat message (MsgType = 0)
    * with the TestReqID field (tag = 112) has been received.
    *
    * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    virtual void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ ) {}

    /**
    * This call-back method is called when a Resend Request (MsgType = 2) has been received.
    */
    virtual void onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ ) {}

    /**
    * This call-back method is called when the session has changed its state.
    */
    virtual void onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    virtual void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& event, const Engine::Session& /*sn*/ );

    /**
    * This call-back method is called when an outgoing message is about to be resent
    * as a reply to the incoming ResendRequest message.
    *
    * If the method returns 'true' then the Engine resends the message to counterpart,
    * otherwise it sends the SequenceReset Gap Fill message.
    *
    * @param msg Outgoing message.
    * @param sn FIX session.
    *
    @return true if the message should be resent, otherwise false.
    */
    virtual bool onResend( const Engine::FIXMessage& msg, const Engine::Session& sn );
};


#endif // H_B2BITS_Engine_Samples_CTGW_MyApp_H
