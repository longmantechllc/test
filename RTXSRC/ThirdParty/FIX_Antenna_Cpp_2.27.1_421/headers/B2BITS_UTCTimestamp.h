// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_UTCTimestamp.h
/// Contains Engine::UTCTimestamp class definition

#ifndef B2BITS_FIX_TYPES_UTCTimestamp_H
#define B2BITS_FIX_TYPES_UTCTimestamp_H

#include <string>
#include <iosfwd>
#include <ctime>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_String.h>

namespace Engine
{

    /// \brief Encapsulates UTCTimestamp FIX type
    class V12_API UTCTimestamp
    {
    public:

        /// \brief A type that represents internal representation of the date.
        typedef System::u64 value_type;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSizeWithMilliseconds = sizeof( "YYYYMMDD-HH:MM:SS.sss" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSizeNoMilliseconds = sizeof( "YYYYMMDD-HH:MM:SS" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int DecimalValueSizeWithMilliseconds = sizeof( "YYYYMMDDHHMMSSsss" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int DecimalValueSizeNoMilliseconds = sizeof( "YYYYMMDDHHMMSS" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSize = ValueSizeWithMilliseconds;

        /// \brief Default constructor.
        UTCTimestamp() throw();

        /// Constructor. Constructs object from year, month, day, hour, minute, second and millisecond values.
        UTCTimestamp( int year, int month, int day, int hour, int minute, int second, int millisecond = 0 ) throw();

        /// \brief Compare operators
        bool operator == ( const UTCTimestamp& v ) const throw();

        /// \brief Compare operators
        bool operator != ( const UTCTimestamp& v ) const throw();

        /// \brief Compare operators
        bool operator < ( const UTCTimestamp& v ) const throw();

        /// \brief Compare operators
        bool operator > ( const UTCTimestamp& v ) const throw();

        /// \brief Compare operators
        bool operator >= ( const UTCTimestamp& v ) const throw();

        /// \brief Compare operators
        bool operator <= ( const UTCTimestamp& v ) const throw();

        /// Returns year part of value.
        int year() const throw();

        /// Returns month part of value.
        int month() const throw();

        /// Returns day part of value.
        int day() const throw();

        /// Returns day hour of value.
        int hour() const throw();

        /// Returns day minute of value.
        int minute() const throw();

        /// Returns day second of value.
        int second() const throw();

        /// Returns day millisecond of value.
        int millisecond() const throw();

        /// Returns day of the week. 
        /// 0 means Sunday, 1 - Monday, 2 - Tuesday, ... 5 - Friday
        int dayOfWeek() const throw();

        /// Constructs object from FIX string (YYYYMMDD-HH:MM:SS.zzz).
        static UTCTimestamp fromFixString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMMDDHHMMSSzzz).
        static UTCTimestamp fromDecimal( System::u64 val ) throw();

        /// Constructs object from decimal date format (YYYYMMDDHHMMSSzzz).
        static UTCTimestamp fromDecimalString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMMDDHHMMSSzzz) or FIX string (YYYYMMDD-HH:MM:SS.zzz).
        static UTCTimestamp fromString( AsciiString val ) throw();

        /// Constructs object using the difference, measured in milliseconds, between
        /// the current time and midnight, January 1, 1970 UTC.
        /// In case of error (if passed value is too large) function returns zero
        /// UTCTimestamp (00000000-00:00:00.000)
        static UTCTimestamp fromUnixTimeInMillis( System::u64 val ) throw();

        /// Constructs object using time_t value.
        /// In case of error (if passed value is too large) function returns zero
        /// UTCTimestamp (00000000-00:00:00.000)
        static UTCTimestamp fromTimeT( time_t t, int millis = 0 ) throw();

        /// Converts value stored in object to FIX string 'YYYYMMDD-HH:MM:SS.zzz'.
        std::string& toFixString( std::string* dest, bool keepmsec = false ) const;

        /// Converts stored value to FIX string 'YYYYMMDD-HH:MM:SS.zzz'.
        ///
        /// @param buf Memory buffer enough to store string like YYYYMMDD-HH:MM:SS.zzz
        /// @return pointer to the first character of the result. Last character of result is last character of the 'buf' parameter.
        char* toFixString( char* buf, char* end, bool keepmsec = false ) const throw();

        /// Returns UTCTimestamp with current time
        /// @param withMIlliseconds Pass to to obtain time with milliseconds; false otherwise.
        /// @return Instance of the UTCTimestamp which contains current time.
        static UTCTimestamp now( bool withMilliseconds = false ) throw();

        /// Returns value in decimal format (YYYYMMDDHHMMSSzzz)
        System::u64 toDecimal() const throw();

        /// Erases milliseconds
        UTCTimestamp getWithoutMilliseconds() const throw();

    private:
        UTCTimestamp( value_type v ) throw() : v_( v ) {}

    private:
        value_type v_;
    };

    inline UTCTimestamp UTCTimestamp::getWithoutMilliseconds() const throw()
    {
        return UTCTimestamp( v_ - ( v_ % 1000 ) );
    }

    inline bool UTCTimestamp::operator == ( const UTCTimestamp& v ) const throw()
    {
        return v_ == v.v_;
    }

    inline bool UTCTimestamp::operator != ( const UTCTimestamp& v ) const throw()
    {
        return v_ != v.v_;
    }

    inline bool UTCTimestamp::operator < ( const UTCTimestamp& v ) const throw()
    {
        return v_ < v.v_;
    }

    inline bool UTCTimestamp::operator > ( const UTCTimestamp& v ) const throw()
    {
        return v_ > v.v_;
    }

    inline bool UTCTimestamp::operator >= ( const UTCTimestamp& v ) const throw()
    {
        return v_ >= v.v_;
    }

    inline bool UTCTimestamp::operator <= ( const UTCTimestamp& v ) const throw()
    {
        return v_ <= v.v_;
    }

    /// \brief Writes a UTCTimestamp into the output stream.
    extern V12_API std::ostream& operator << ( std::ostream& s, const UTCTimestamp& v );
}

#endif
