/*
<copyright>

$Revision: 1.3 $

(c) B2BITS EPAM Systems Company 2000-2017.
"Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

This software is for the use of the paying client of B2BITS (which may be
a corporation, business area, business unit or single user) to whom it was
delivered (the "Licensee"). The use of this software is subject to
license terms.

The Licensee acknowledges and agrees that the Software and Documentation
(the "Confidential Information") is confidential and proprietary to
the Licensor and the Licensee hereby agrees to use the Confidential
Information only as permitted by the full license agreement between
the two parties, to maintain the confidentiality of the Confidential
Information and not to disclose the confidential information, or any part
thereof, to any other person, firm or corporation. The Licensee
acknowledges that disclosure of the Confidential Information may give rise
to an irreparable injury to the Licensor in-adequately compensable in
damages. Accordingly the Licensor may seek (without the posting of any
bond or other security) injunctive relief against the breach of the forgoing
undertaking of confidentiality and non-disclosure, in addition to any other
legal remedies which may be available, and the licensee consents to the
obtaining of such injunctive relief. All of the undertakings and
obligations relating to confidentiality and non-disclosure, whether
contained in this section or elsewhere in this agreement, shall survive
the termination or expiration of this agreement for a period of five (5)
years.

The Licensor agrees that any information or data received from the Licensee
in connection with the performance of the support agreement relating to this
software shall be confidential, will be used only in connection with the
performance of the Licensor's obligations hereunder, and will not be
disclosed to third parties, including contractors, without the Licensor's
express permission in writing.

Information regarding the software may be provided to the Licensee's outside
auditors and attorneys only to the extent required by their respective
functions.

</copyright>
*/

#include "MyApp.h"

#include <stdexcept>
#include <memory>
#include <B2BITS_Guard.h>
#include <B2BITS_AutoPtr2.h>
#include <B2BITS_PubEngineDefines.h>

using namespace std;
using namespace Engine;
using namespace System;
using namespace Utils;

typedef Guard<System::Mutex> MGuard;

MyApp::MyApp( bool isEchoMode )
    : msgCount_( 0 )
    , logger_(Utils::Log::LogSystem::singleton()->createCategory( "Application" ))
    , isEchoMode_(isEchoMode)
{
}


MyApp::~MyApp()
{
    if( NULL != ssn_.get() ) {
        ssn_->registerApplication( NULL );
    }
}

bool MyApp::process( const FIXMessage& msg, const Engine::Session& /*aSn*/ )
{
    ++msgCount_;

    if ( !isEchoMode_ )
        return true;

    AutoPtr2<FIXMessage, &FIXMessage::release> pFixMsg( FIXMsgProcessor::singleton()->clone( msg ) );

    try {
        put( pFixMsg.get() );
    } catch( const std::exception& ex ) {
        if(logger_)
            logger_->error( ex.what() );
    }

    return true;
}

void MyApp::onLogonEvent( const LogonEvent* /*apEvent*/, const Session& aSn )
{
    if (logger_) {
        switch (aSn.getRole()) {
            case ACCEPTOR_SESSION_ROLE:
                logger_->note("Incoming connection established: " + *aSn.getSender() + '@' + *aSn.getTarget());
                break;
            case INITIATOR_SESSION_ROLE:
                logger_->note("Outgoing connection established: " + *aSn.getSender() + '@' + *aSn.getTarget());
                break;
            default:
                throw std::logic_error( "Wrong session role value: Reason: session role is NA." );
        }
    }
}

void MyApp::onSequenceGapEvent( const SequenceGapEvent* /*apEvent*/, const Session& /*aSn*/ )
{
}

void MyApp::onSessionLevelRejectEvent( const SessionLevelRejectEvent* /*apEvent*/, const Session& /*aSn*/ )
{
}

void MyApp::onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ )
{
    if(logger_)
        logger_->note( "Event onUnableToRouteMessage received - unable to route message into " + *event.getDeliverTo() );
}

void MyApp::resetSession()
{
    MGuard lock( ssnLock_ );

    if( NULL != ssn_.get() ) {
        ssn_->registerApplication( this );
        ssn_.reset( NULL );
    }
}

bool MyApp::setSession( Session* apSn )
{
    MGuard lock( ssnLock_ );

    if( NULL != ssn_.get() ) {
        return false;
    }

    ssn_.reset( apSn );

    if( NULL != apSn ) {
        apSn->registerApplication( this );
    }

    return true;
}

RefCounter<Engine::Session> MyApp::getSession()
{
    MGuard lock( ssnLock_ );
    return ssn_;
}

void MyApp::close()
{
    RefCounter<Session> session = getSession();

    if( NULL != session.get() ) {
        session->disconnect( "The session was closed by EchoServer", true );
    }
}

bool MyApp::isSesClosed()
{
    MGuard lock( ssnLock_ );
    return NULL == ssn_.get();
}

long MyApp::getNMsg()
{
    return msgCount_;
}

void MyApp::put( Engine::FIXMessage* pMessage )
{
    RefCounter<Session> session = getSession();

    if( NULL == session.get() ) {
        throw std::logic_error( "Unable to send message to a session. Reason: session is not registered yet." );
    }

    session->put( pMessage );
}
