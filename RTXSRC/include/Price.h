//
// Created by schen on 10/6/2020.
//

#ifndef RTXROUTER_PRICE_H
#define RTXROUTER_PRICE_H
////////

class Price {
public:
    inline Price() { value_  = 0.0; }
    inline Price(const Price& value) { value_  = value.value_ ; }
    explicit inline Price(const double value) { value_  = value; }
    explicit inline Price(const unsigned long value) { value_  = value; }

    inline Price& operator =( double d )
    {
        value_  = d;
        return *this;
    }

    inline Price& operator =( Price p2 )
    {
        value_  = p2.value_ ;
        return *this;
    }
    Price operator+(const Price& p2) const
    {
        return Price(value_ + p2.value_ );
    }
    Price operator-(const Price& p2) const
    {
        return Price(value_ - p2.value_ );
    }

    bool operator==(const Price& p2) const
    {
        return (value_  >= (p2.value_ - disc_)) && (value_  <= (p2.value_ + disc_)) ;
    }
    bool operator>(const Price& p2) const
    {
        return (value_  > (p2.value_ + disc_));
    }
    bool operator<(const Price& p2) const
    {
        return (value_  < (p2.value_ - disc_));
    }
    bool operator>=(const Price& p2) const
    {
        return (value_  >= (p2.value_ - disc_));
    }
    bool operator<=(const Price& p2) const
    {
        return (value_  <= (p2.value_ + disc_));
    }
    bool isGE(const Price& p2)
    {
        return (value_  >= (p2.value_ - disc_));
    }
    bool isLE(const Price& p2)
    {
        return (value_  <= (p2.value_ + disc_));
    }


    inline double ToDouble() const	{ return value_ ; }
    const double& value() const  { return value_ ; }

private:
    double value_ =0.0;
    double disc_ = 0.0000001;
};

#endif //RTXROUTER_PRICE_H
