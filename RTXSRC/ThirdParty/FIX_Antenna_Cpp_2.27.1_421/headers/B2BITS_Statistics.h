// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Statistics.h
/// Contains Engine::StatisticsProvider and Engine::Statistics classes declaration.

#ifndef _B2BITS_Statistics__h_
#define _B2BITS_Statistics__h_

#include <map>
#include <set>

#include "B2BITS_V12_Defines.h"
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_Session.h"
#include "B2BITS_CompilerDefines.h"

namespace Engine
{
    struct SessionId;

    /**
      * Provides detailed statistics about the current state of the FIX Engine.
      */
    class V12_API StatisticsProvider
    {
    public:
        /** Calculates average handling time for all received messages.
        * @return 0
        * @deprecated Switched off by performance reasons. Still present for compatibility.
        */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageReceivedMsgHandling()const;

        /** Type definition for pair version protocol and message type*/
        typedef std::pair<FIXVersion, std::string> MsgProtocolTypeT;

        /** Calculates average handling time of received messages for passed message type.
          * @param msgType - type of the message
          * @return 0
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageReceivedMsgHandlingByType( const MsgProtocolTypeT& msgType )const;

        /** Calculates average handling time of received messages for all passed message types.
          * @param types - contains set of types that handling time will be evaluated
          * @param[out] averages - contains MsgProtocolTypeT => 0 map
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        void getAverageReceivedMsgHandlingByTypes( const std::set<MsgProtocolTypeT>& types,
                std::map<MsgProtocolTypeT, System::u64>* averages )const;

        /** Calculates average handling time of sent messages for all messages.
         * @returns 0
         * @deprecated Switched off by performance reasons. Still present for compatibility.
         */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageSendMsgHandling()const;

        /** Calculates average handling time of sent messages for passed message.
          * @param msgType - type of the message
          * @return 0
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageSendMsgHandlingByType( const MsgProtocolTypeT& msgType )const;

        /** Calculates average handling time of sent messages for all passed message types.
          * @param types - contains set of types that handling time will be evaluated
          * @param[out] averages - contains MsgProtocolTypeT => 0 map
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        void getAverageSendMsgHandlingByTypes( const std::set<MsgProtocolTypeT>& types,
                                               std::map<MsgProtocolTypeT, System::u64>* averages )const;

        /** Calculates average validation time for all messages.
         * @returns 0
         * @deprecated Switched off by performance reasons. Still present for compatibility.
         */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageValidationTime()const;

        /** Calculates average validation time for passed message type.
          * @param msgType - type of the message
          * @return 0
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        System::u64 getAverageValidationTimeByType( const MsgProtocolTypeT& msgType )const;

        /** Calculates average validation time for all passed message types.
          * @param types - contains set of types that handling time will be evaluated
          * @param averages - contains MsgProtocolTypeT => 0 map
          * @deprecated Switched off by performance reasons. Still present for compatibility.
          */
        B2B_DEPRECATED("Switched off by performance reasons.")
        void getAverageValidationTimeByTypes( const std::set<MsgProtocolTypeT>& types,
                                              std::map<MsgProtocolTypeT, System::u64>* averages )const;

        /** Calculates received message amount
         * @returns 0
         * @deprecated Switched off by performance reasons. Still present for compatibility.
         */
        B2B_DEPRECATED("Switched off by performance reasons.")
        std::size_t getReceivedMsgCount()const;

        /** Calculates sent message amount
         * @returns 0
         * @deprecated Switched off by performance reasons. Still present for compatibility.
         */
        B2B_DEPRECATED("Switched off by performance reasons.")
        std::size_t getSentMsgCount()const;

        /** Calculates total proceed message amount
         * @returns 0
         * @deprecated Switched off by performance reasons. Still present for compatibility.
         */
        B2B_DEPRECATED("Switched off by performance reasons.")
        std::size_t getProceedMsgCount()const;

        /** Returns state of the session  */
        bool getSessionState( const std::string& sender, const std::string& target, Session::State* state )const;

        /** Returns state of the session  */
        bool getSessionState( const SessionId& sessionId, Session::State* state )const;
    };


    /**
     * Statistical information about the current state of FIX Engine.
     */
    struct Statistics {
        /** Number of active sessions. */
        int m_nActive;

        /** Number of reconnecting sessions (as Initiator). */
        int m_nInitiators;

        /** Number of sesssions, awaiting incoming connection (as Acceptor). */
        int m_nAcceptors;

        /** Number of correctly terminated sessions. */
        int m_nCorrectlyTerminated;

        /** Number of non-gracefully terminated sessions. */
        int m_nNonGracefullyTerminated;

        /** Detailed FIXAntenna statistics. */
        StatisticsProvider detailedStatistics_;
    };

} // namespace Engine
#endif // _Statistics__h_ 


