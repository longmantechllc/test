#pragma once

#include <string>
#include <vector>
#include "ExecutionReport.h"
#include "Order.h"

/// OSM interface to RTX
class RTXApp
{
public:
    virtual void sendChild(const Order&) = 0;
    virtual void sendACK(const ExecutionReport&) = 0; 
    virtual void sendER(const ExecutionReport&) = 0;
    virtual bool cancelChild(std::string childOrderId) = 0;
};


