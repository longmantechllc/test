//
// Created by schen on 2/8/2021.
//

#ifndef RTXROUTER_XTIMER_H
#define RTXROUTER_XTIMER_H

#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <queue>
#include <map>
#include <chrono>
#include <mutex>
#include <functional>
#include <condition_variable>

#include "TimerScheduler.h"

using namespace std;

class TimerScheduler;

class xTimer
{
private:

public:
    xTimer() {}

    xTimer(TimerScheduler* timerScheduler);
    ~xTimer();
    void StartTimer(int durationMs, std::function<void(xTimer&)> task, void* data = nullptr);

    std::chrono::system_clock::time_point m_timerId;
    std::chrono::system_clock::time_point m_fireTime;
    std::function<void(xTimer&)> m_task;
    void* m_data;

    TimerScheduler* m_timerScheduler;
};

#endif //RTXROUTER_XTIMER_H
