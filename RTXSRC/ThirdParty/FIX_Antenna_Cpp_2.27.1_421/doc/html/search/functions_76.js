var searchData=
[
  ['valblock',['ValBlock',['../classFixDictionary2_1_1ValBlock.html#a90219d0d1de15c55f8a9ec1c3b767385',1,'FixDictionary2::ValBlock::ValBlock(std::string const &amp;id)'],['../classFixDictionary2_1_1ValBlock.html#add64d7f96af60af96dd714105bb16ca1',1,'FixDictionary2::ValBlock::ValBlock(ValBlock const &amp;src)']]],
  ['value',['value',['../classSystem_1_1HRTimer.html#a3a2d3706b0116479bb01274f1de47557',1,'System::HRTimer']]],
  ['valuedescription',['ValueDescription',['../structFixDictionary2_1_1ValBlock_1_1ValueDescription.html#abac763852c521f0ec428e09e516a11f4',1,'FixDictionary2::ValBlock::ValueDescription::ValueDescription()'],['../structFixDictionary2_1_1ValBlock_1_1ValueDescription.html#a23133f905f4403bd803daea4db7f2826',1,'FixDictionary2::ValBlock::ValueDescription::ValueDescription(std::string const &amp;description, std::string const &amp;id)']]],
  ['valuetype',['valueType',['../classFixDictionary2_1_1FieldType.html#adacc3ad145c7e6a77e9ba991ddb2f774',1,'FixDictionary2::FieldType']]],
  ['version',['version',['../classFixDictionary2_1_1Protocol.html#a63b42646a42d280b88abfc0c02929178',1,'FixDictionary2::Protocol']]],
  ['volatilesensitivepointer',['VolatileSensitivePointer',['../classSystem_1_1ThreadSafe_1_1VolatileSensitivePointer.html#a47b02c75294241610b8101ae651f42d4',1,'System::ThreadSafe::VolatileSensitivePointer']]]
];
