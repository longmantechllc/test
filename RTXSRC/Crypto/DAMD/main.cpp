
#include <iostream>
#include <ctime>
#include <string_view>
#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "Properties.h"
#include "ApplicationProperties.h"
#include "DAMDCache.h"
#include "Version.h"
using namespace ::std;

/// Application entry point.
int main(int argc, char* argv[])
{
    try
    {
        // Create basic file logger (not rotated)
        auto my_logger = spdlog::basic_logger_mt("DAMDCache", "logs/DAMDCache.log");

        // create a file rotating logger with 5mb size max and 3 rotated files
        //auto file_logger = spdlog::rotating_logger_mt("file_logger", "myfilename", 1024 * 1024 * 5, 3);
        spdlog::set_default_logger(my_logger);
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
        return -1;
    }

    spdlog::info("DAMDCache release version {} coming up...", cache_version);

    // command line parameters
    std::string gw_properties_file = "DAMDCache.cfg";

    Helper::writeLineToConsole("DAMDCache © 2022\n");

    if (2 == argc) {
        gw_properties_file = argv[1];
    } else {
        Helper::writeLineToConsole("Usage: DAMDCache <DAMDCache properties file> \n");
        return 1;
    }

    try {
        spdlog::info("Init engine...");

        ifstream in(gw_properties_file.c_str());

        if (!in) {
            throw string("Cannot open file ").append(gw_properties_file);
        }

        Aux::Properties p(&in);
        ApplicationProperties appParams;
        Helper::parseCommandLine(p, &appParams);

        DAMDCache application(appParams);
        if (!application.isInited()) {
            spdlog::error("failed to initiate DAMDCache");
            return 0;
        }
        spdlog::info("DAMDCache starts");

        string cmd;
        while ("exit" != cmd) {
            cout << "Type 'exit' to exit > " << endl;
            getline(cin, cmd);
        }
        //std::this_thread::sleep_for(std::chrono::seconds(1));
        cout << "application shutdown..." << endl;

    } 
    catch (std::ifstream::failure e) {
        spdlog::error("Exception opening file {}", gw_properties_file);
    } 
    catch (const std::exception& ex) {
        spdlog::error("ERROR: {}", ex.what());
        return -1;
    }
    catch(...) {
        spdlog::error("unknown ERROR");
        return -1;
    }

    return 0;
}
