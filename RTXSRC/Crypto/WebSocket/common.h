#pragma once

#include <variant>
#include <utility>
#include <chrono>
#include <functional>
#include <cmath>
#include <array>
#include <string>
#include <string_view>
#include <vector>

#include <rapidjson/document.h>

#define UDS_CLOSE_CONNECTION "%$$%"

using namespace std::literals::chrono_literals;
using namespace std::chrono;
using namespace rapidjson;

namespace Crypto {

enum exchange_message_t {
    PING,
    PONG,
    AUTHENTICATE_SUCCESS,
    SUBSCRIBE_SUCCESS,
    DEFAULT,
};

using CRTValue = rapidjson::GenericValue<rapidjson::UTF8<>, rapidjson::CrtAllocator>;
using CRTDocument = rapidjson::GenericDocument<rapidjson::UTF8<>, rapidjson::CrtAllocator>;
using processed_data_t = std::variant<CRTDocument, std::nullptr_t, std::int64_t>;
using json_message_t = std::pair<exchange_message_t, processed_data_t>;

// const auto compare_pair_using_first_element = [](const auto &a, const auto &b) { 
//     return a.first > b.first; 
// }
    using on_update_t  = std::function<json_message_t(std::string_view)>;
    using on_ready_t  = std::function<void()>;
    using on_error_t  = std::function<void()>;
    using build_ping_msg_t  = std::function<std::string()>;
    using build_pong_msg_t  = std::function<std::string(std::int64_t)>;
    using build_auth_msg_t  = std::function<std::string()>;
    using build_subscription_msg_t  = std::function<std::vector<std::string>()>;



    using on_message_t = std::function<void(std::string_view)>;
    using on_ws_pong_t  = std::function<void()>;
    using on_connect_t = std::function<void()>;
    using on_disconnect_t = std::function<void()>;

}
