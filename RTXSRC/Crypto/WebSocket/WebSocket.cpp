#include "magic_enum/magic_enum.hpp"
#include "spdlog/spdlog.h"
#include "WebSocket.h"
#include "ws_connection.h"
namespace Crypto {

int WebSocket::event_cb( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{
	auto *connection = reinterpret_cast<WsConnection *> (user);
	switch( reason )
	{
		case LWS_CALLBACK_CLIENT_ESTABLISHED:
			spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_ESTABLISHED...",
                	connection->get_index());
            connection->on_connect(wsi);
			lws_callback_on_writable( wsi );
			break;
        case LWS_CALLBACK_RECEIVE_PONG: 
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_RECEIVE_PONG...",
                	connection->get_index());
            lws_validity_confirmed(wsi);
			break;
		case LWS_CALLBACK_CLIENT_RECEIVE:
			lws_validity_confirmed(wsi);
			/* Handle incomming messages here. */
			try {
				spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_ESTABLISHED...",
                	connection->get_index());
				std::string_view str_result(static_cast<char*>(in), len);
            	connection->on_message(wsi, str_result);

			} catch ( std::exception &e ) {
		 		spdlog::error( "<WebSocket::event_cb> Error {}", e.what() ); 
			}   	
			break;

		case LWS_CALLBACK_CLIENT_WRITEABLE:
		{
           spdlog::debug("<event_cb> [{}]: LWS_CALLBACK_CLIENT_WRITEABLE...",
                connection->get_index());
            bool flag = connection->flush_send_buffer(wsi);
            if (!flag) {
                spdlog::info("<event_cb>: disconnecting ws[{}]...",
                    connection->get_index());
                return 1;
            }
            break;
		}

        case LWS_CALLBACK_GET_THREAD_ID: {
            spdlog::info("<event_cb> : LWS_CALLBACK_GET_THREAD_ID...");
            return std::hash<std::thread::id>{}(std::this_thread::get_id());
        }
        case LWS_CALLBACK_CLIENT_CLOSED: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_CLOSED...",
                connection->get_index());
            connection->on_disconnect();
            return -1;
        }
        case LWS_CALLBACK_CLOSED_CLIENT_HTTP: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLOSED_CLIENT_HTTP...",
                connection->get_index());
            connection->on_disconnect();
            return -1;
        }
        case LWS_CALLBACK_CLIENT_CONNECTION_ERROR: {
            spdlog::info("<event_cb> [{}]: LWS_CALLBACK_CLIENT_CONNECTION_ERROR...",
                connection->get_index());
            // connection->on_disconnect();
            return 1;
        }
        case LWS_CALLBACK_CLIENT_RECEIVE_PONG: 
            //spdlog::info("<event_cb> : LWS_CALLBACK_CLIENT_RECEIVE_PONG");
            connection->on_ws_pong(wsi);
            break;
        default: 
            if (connection) {
                spdlog::info("<event_cb> [{}]: notice {}", 
                    connection->get_index(), magic_enum::enum_name(reason));
                //connection->on_connect(wsi);
            } else {
                spdlog::info("<event_cb> w/o connection: notice {}", 
                    magic_enum::enum_name(reason));
            }
            break;
	}

	return 0;
}


//-------------------
WebSocket::WebSocket(Type type) 
{
	struct lws_context_creation_info info;
	memset( &info, 0, sizeof(info) );

	info.port = CONTEXT_PORT_NO_LISTEN;
	info.protocols = protocols[(int)type];
	info.gid = -1;
	info.uid = -1;
	info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

	m_context = lws_create_context( &info );
}


//----------------------------
// Register call backs
// void WebSocket::connect_endpoint (const char *host, uint16_t port, const char *path, WS_CB cb) 
// {

// 	/* Connect if we are not connected to the server. */
// 	struct lws_client_connect_info ccinfo = {0};
// 	ccinfo.context 	= m_context;
// 	ccinfo.address 	= host;
// 	ccinfo.port 	= port;
// 	ccinfo.path 	= path;
// 	ccinfo.host 	= lws_canonical_hostname( m_context );
// 	ccinfo.origin 	= "origin";
// 	ccinfo.protocol = protocols[0].name;
// 	ccinfo.ssl_connection = LCCSCF_USE_SSL | LCCSCF_ALLOW_SELFSIGNED | LCCSCF_SKIP_SERVER_CERT_HOSTNAME_CHECK;

// 	struct lws* conn = lws_client_connect_via_info(&ccinfo);
// 	//handles[conn] = cb;

// }


//----------------------------
// Entering event loop
void 
WebSocket::enter_event_loop() 
{
	//while(true)
        try {	
            //spdlog::info( "WebSocket::enter_event_loop"); 
            lws_service( m_context, -1 );
        } catch ( std::exception &e ) {
            spdlog::error( "<WebSocket::enter_event_loop> Error !{}", e.what() ); 
        }

	
}

} // namespace Crypto

