//
// Created by schen on 10/5/2020.
//

#ifndef RTXROUTER_BBO_H
#define RTXROUTER_BBO_H

#include "Price.h"

class Bbo {
public:
    Bbo() {}

    Price m_bboBuy = Price(-9999.99);
    Price m_bboSell = Price(999999.99);

    std::string m_symbol;
};

class OpenTrade {
public:
    OpenTrade() {}

    Price m_tradePrice = Price(-9999.99);
    std::string m_symbol;
};

#endif //RTXROUTER_BBO_H
