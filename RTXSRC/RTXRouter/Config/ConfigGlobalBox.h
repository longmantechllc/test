//
// Created by schen on 10/22/2020.
//
#ifndef RTXROUTER_CONFIGGLOBALBOX_H
#define RTXROUTER_CONFIGGLOBALBOX_H

#include <functional>
#include "../include/Price.h"

using namespace std;

struct ArgoStruct
{
    ArgoStruct(std::string nameIn, bool enabledIn)
    :name(nameIn),
     enabled(enabledIn)
    {
    }
    std::string name;
    bool enabled = false;
};
struct SecurityMaster
{
    std::string path;
};
struct TradingHalts
{
    bool enabled = false;
};
struct QuoteFilter
{
    bool premarket = true;
    bool postmarket = true;
};
struct COD
{
    int cxlTimer = 0;
};
struct RetailRouterMMIDpIpfoF
{
    RetailRouterMMIDpIpfoF(){;}
    RetailRouterMMIDpIpfoF(const std::string& mmidIn, const Price& pIIn, const Price& pfoFIn, const int priorityIn)
        : mmid(mmidIn),
        pI(pIIn),
        pfoF(pfoFIn),
        priority(priorityIn)
    {
    }
    const std::string mmid;
    Price pI;
    Price pfoF;
    int priority;
};
struct OrderParamAction
{
    OrderParamAction(){;}
    OrderParamAction(const std::string& actionIn, const  std::vector<std::string>& destinationIn)
            : action(actionIn),
              destinationList(destinationIn)
    {
    }
    const std::string action; /* reject, reroute...*/
    const std::vector<std::string> destinationList;
};

class ConfigGlobalBox
{
public:
    ConfigGlobalBox();
    virtual ~ConfigGlobalBox();
    void InitConfigGlobalBox();
    ConfigGlobalBox* GetConfigGlobalBox(){ return this; }

    //single layer parameters
    void SetTimeZone(char* val) { timeZone = val; }
    std::string& TimeZone() { return timeZone; }

    void SetRouteName(char* val)  {routerName = val;}
    std::string& RouteName() { return routerName; }

    void SetDefaultSecIdfn(char* val)  {defaultSecIdfn = val;}
    std::string& DefaultSecIdfn() { return defaultSecIdfn; }

    void SetRetryOnCxlRej(char* val);
    int RetryOnCxlRej() const { return retryOnCxlRej; }

//    void SetMarketDataFailureTimeOut(char* val);
//    int MarketDataFailureTimeOut() const { return marketDataFailureTimeOut; }

    void SetMarketDataFailureTimeOutSwp(char* val);
    int MarketDataFailureTimeOutSwp() const { return marketDataFailureTimeOutSwp; }

    void SetMarketDataFailureTimeOutTarg(char* val);
    int MarketDataFailureTimeOutTarg() const { return marketDataFailureTimeOutTarg; }

    void SetShortSellTargetingReTries(char*  val);
    int ShortSellTargetingReTries() const { return shortSellTargetingReTries; }

    void SetCheckChildOrdLimitPriceViolation(char* val);
    bool CheckChildOrdLimitPriceViolation() const { return checkChildOrdLimitPriceViolation; }

    //double layer
    struct TradingHalts* TradingHalts() const { return tradingHalts; }
    struct SecurityMaster* SecurityMaster() const { return securityMaster; }

    //argo
    std::vector <std::shared_ptr<ArgoStruct> /*SWP, true*/> argos ;
    std::unordered_map <std::string /*SWP*/, bool> argosMp ;
    void InitArgo();
    void AddArgo(const std::string& val, std::string& strVal, bool deflt=false);
    void SetArgo();
    std::unordered_map <std::string /*DarkSweep*/, bool>& Argos() { return argosMp; }

    //quoteFilter
    std::unordered_map <std::string /*BATS*/, std::shared_ptr<QuoteFilter> > quoteFilters;
    void AddQuoteFilter(const std::string& name, const std::shared_ptr<QuoteFilter>& val);
    std::unordered_map <std::string /*NYSE*/, std::shared_ptr<QuoteFilter> >& QuoteFilters() { return quoteFilters; }

    //marketDataFailureRoutes
    std::vector < std::string /*INDR-NASD*/ > marketDataFailureRoutes;
    void AddMarketDataFailureRoute(const std::string& val);
    std::vector < std::string /*INDR-BATS*/ > & MarketDataFailureRoutes() { return marketDataFailureRoutes; }

    //customTifs
    std::unordered_map <int /*0*/, std::shared_ptr<struct COD>> customTifs;
    void AddCustomTif(std::string value, const std::shared_ptr<struct COD>& cxlTimer);
    std::unordered_map <int /*0*/, std::shared_ptr<struct COD>>& CustomTif() { return customTifs; }

    //secIdfnIdentifiers
    std::unordered_map <std::string/*Q*/, std::string /*NASD*/> secIdfnIdentifiers;
    void AddSecIdfnIdentifier(const std::string& value, const std::string& SecIdfn);
    std::unordered_map <std::string/*Q*/, std::string /*NASD*/>& SecIdfnIdentifier() { return secIdfnIdentifiers; }

    //SymbolSecIdfnIdentifier
    std::unordered_map <std::string/*user*/, std::string /*EXCH1*/> userSecIdfnIdentifier;
    std::unordered_map <std::string/*account*/, std::string /*EXCH1*/> accountSecIdfnIdentifier;
    std::unordered_map <std::string/*IBM*/, std::string /*EXCH1*/> symbolSecIdfnIdentifier;
    std::string /*EXCH1*/ minQtySecIdfnIdentifier;
    std::string /*EXCH1*/ mEmptyString="";
    void AddSyntheticSecIdfnIdentifier(const std::string& name, std::string value, const std::string& SecIdfn);
    std::string& UserSecIdfnIdentifier(const std::string& val);
    std::string& AccountSecIdfnIdentifier(const std::string& val);
    std::string& SymbolSecIdfnIdentifier(const std::string& val);
    std::string& MinQtySecIdfnIdentifier() {return minQtySecIdfnIdentifier;}

    //orderParamActions
//    std::unordered_map <std::string /*orderType, side*/, std::unordered_map <std::string /*"5" short*/, std::shared_ptr<OrderParamAction>>> orderParamActions;
    std::unordered_map <int /* "3" */, std::shared_ptr<OrderParamAction>> orderTypeActionList;
    std::unordered_map <std::string /* "IBM" */, std::shared_ptr<OrderParamAction>> orderSymbolActionList;
    std::unordered_map <int /* "1" */, std::shared_ptr<OrderParamAction>> orderTifActionList;
    std::unordered_map <std::string /* "Raj" */, std::shared_ptr<OrderParamAction>> userActionList;
    std::unordered_map <std::string /* "RTX" */, std::shared_ptr<OrderParamAction>> accountActionList;
    void AddOrderParam(const std::string& type, std::string value, const std::string& action, std::string dest );
//    std::unordered_map <std::string /*orderType, side*/, std::unordered_map <std::string /*"5" short*/, std::shared_ptr<OrderParamAction>>>& OrderParam() { return orderParamActions; }

    //Retail router
    std::string retailRouterContraQuotefilePath;
    bool retailRouterIoiNormalizerCache = false;
    std::unordered_map<std::string, std::string> retailRouterMMIDmap;                                           //<map mmid="CTDL" route="RTL_CTDL"/>
    std::unordered_map<std::string, std::shared_ptr<RetailRouterMMIDpIpfoF>> retailRouterMMIDpIpfoFmap;         //<map mmid="JANE" pI="0.01" pfoF="0.02" priority="1"/>


    void SetThreadNumber(char* val);
    int ThreadNumber() const { return threadNumber; }

    void SetMarketOpen(char* val);
    int MarketOpenTime() const { return m_marketStartTimeMssm; }
    std::string MarketOpenStr() const { return m_marketStartTimeStr; }

    void SetMarketClose(char* val);
    int MarketCloseTime() const { return m_marketEndTimeMssm; }
    std::string MarketCloseStr() const { return m_marketEndTimeStr; }

    void SetPostMarketClose(char* val);
    int PostMarketCloseTime() const { return m_postMarketCloseTimeMssm; }
    std::string PostMarketCloseStr() const { return m_postMarketCloseStr; }

    void SetRetailRoutingStartTime(char* val);
    int RetailRoutingStartTime() const { return m_retailRoutingStartTimeMssm; }
    std::string RetailRoutingStartTimeStr() const { return m_retailRoutingStartTimeStr; }

    void SetRetailRoutingEndTime(char* val);
    int RetailRoutingEndTime() const { return m_retailRoutingEndTimeMssm; }
    std::string RetailRoutingEndTimeStr() const { return m_retailRoutingEndTimeStr; }

    void SetRetailSWPStartTime(char* val);
    int RetailSWPStartTime() const { return m_retailSwpStartTimeMssm; }
    std::string RetailSWPStartTimeStr() const { return m_retailSwpStartTimeStr; }

    void SetRetailSWPEndTime(char* val);
    int RetailSWPEndTime() const { return m_retailSwpEndTimeMssm; }
    std::string RetailSWPEndTimeStr() const { return m_retailSwpEndTimeStr; }

    void SetSWPStartTime(char* val);
    int SWPStartTime() const { return m_swpStartTimeMssm; }
    std::string SWPStartTimeStr() const { return m_swpStartTimeStr; }

    void SetSWPEndTime(char* val);
    int SWPEndTime() const { return m_swpEndTimeMssm; }
    std::string SWPEndTimeStr() const { return m_swpEndTimeStr; }


    void SetUnsubscribeMDWhenPost(char* val);
    bool UnsubscribeMDWhenPost() const { return m_unsubscribeMDWhenPost; }

    void SetAssetClass(char* val);
    std::string AssetClass() const { return m_ssetClass; }
    void SetExtraLogging(char* val);
    bool ExtraLogging() const { return extraLogging; }

    std::unordered_map <std::string /*Name*/, std::function<void(char* arg)> > FuncList;

    std::string routerName;
    std::string timeZone; // = "EST5EDT";       //US east
    std::string defaultSecIdfn;
    int retryOnCxlRej = 3;
    int marketDataFailureTimeOut = 0;
    int marketDataFailureTimeOutSwp = 0;
    int marketDataFailureTimeOutTarg = 0;

    int shortSellTargetingReTries = 5;
    bool checkChildOrdLimitPriceViolation = false;
    std::string m_marketStartTimeStr = "9:30:00";
    std::string m_marketEndTimeStr = "16:00:00";
    std::string m_postMarketCloseStr = "18:30:00";
    int  m_marketStartTimeMssm = 34200000;                  //9:30    ms
    int  m_marketEndTimeMssm = 57600000;                    //default to 16*60*60*1000 ms
    int  m_postMarketCloseTimeMssm = 66600000;              //default to 18.5*60*60*1000 ms

    std::string m_retailRoutingStartTimeStr = "9:30:00";
    std::string m_retailRoutingEndTimeStr = "16:00:00";
    int  m_retailRoutingStartTimeMssm = 34200000;           //9:30    ms
    int  m_retailRoutingEndTimeMssm = 57600000;             //default to 16*60*60*1000 ms

    std::string m_swpStartTimeStr = "9:30:00";
    std::string m_swpEndTimeStr = "16:00:00";
    int  m_swpStartTimeMssm = 34200000;                     //9:30    ms
    int  m_swpEndTimeMssm = 57600000;                       //default to 16*60*60*1000 ms

    std::string m_retailSwpStartTimeStr = "9:30:00";
    std::string m_retailSwpEndTimeStr = "16:00:00";
    int  m_retailSwpStartTimeMssm = 34200000;                     //9:30    ms
    int  m_retailSwpEndTimeMssm = 57600000;                       //default to 16*60*60*1000 ms

    bool  m_unsubscribeMDWhenPost = true;

    int  threadNumber = 5;
    std::string m_ssetClass = "USEquities";
    bool  extraLogging = false;
    bool  logAsync = true;

    struct TradingHalts* tradingHalts;
    struct SecurityMaster* securityMaster;

};

#endif //RTXROUTER_CONFIGGLOBALBOX_H
