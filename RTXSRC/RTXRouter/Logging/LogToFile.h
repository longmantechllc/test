//
// Created by schen on 10/9/2020.
//

#ifndef RTXROUTER_LOGTOFILE_H
#define RTXROUTER_LOGTOFILE_H

class Logger;

class LogToFile
{
public:
    LogToFile();
    virtual ~LogToFile();
    Logger* InitLogger(AppConstants::LOG_TO_FILE dest);
    void CreatDirectory(std::string& filePath);
    std::string CreatDirectories(AppConstants::LOG_TO_FILE dest);
    std::string CreatTimeString();
    void logHeader();

    Logger* m_logger = nullptr;

private:
    std::string m_filePath;
};

#endif //RTXROUTER_LOGTOFILE_H
