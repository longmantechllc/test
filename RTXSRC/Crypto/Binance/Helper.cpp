#include "Helper.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>
#include <cstring>

using namespace ::std;
using namespace ::Aux;

// Writes message to console.
void Helper::writeLineToConsole(const string& message)
{
    //outputLock_.lock();
    clog << message << endl;
    //outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine(const string& message)
{
    cerr << message << endl;
}

void Helper::parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params)
{
    params->initiator_cfg = prop.getString(Aux::Properties::INITIATOR_CFG);
    params->symbol_file = prop.getString(Aux::Properties::SYMBOL_FILE);
    if (prop.isPropertyExists(Aux::Properties::INTERVAL)) {
        params->interval = prop.getInteger(Aux::Properties::INTERVAL);
    }

}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo(const string& applicationName)
{
    Helper::writeErrorLine();
    Helper::writeErrorLine("Usage: " + applicationName + " <property file>");
    Helper::writeErrorLine();
}

