//
// Created by schen on 6/2/2021.
//

#include <vector>
#include <unordered_map>
#include <unordered_set>
#include <memory>
#include <ctime>

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"
#include "Logger.h"

#include "CPU.h"
#include "../include/App.h"
#include "../include/RTXOrderIO.h"
#include "OrderLogic/RTXOrder.h"

#include "../include/IOIDataIO.h"



IOIDataIO::IOIDataIO()
{
}

IOIDataIO::IOIDataIO(App* app)
        :m_app(app),
         m_logger(app->m_loggerSys)
{

}

IOIDataIO::~IOIDataIO()
{
}

std::vector<IOIMarketData> IOIDataIO::SubscribeIOI(std::string symbol, bool isBuy)
{
    m_logger->logInfo("IOIDataIO::SubscribeIOI symbol: %s.", symbol.c_str());
    auto data = m_ioiInterface->SubscribeIOI(symbol, isBuy);
    return data;
}
