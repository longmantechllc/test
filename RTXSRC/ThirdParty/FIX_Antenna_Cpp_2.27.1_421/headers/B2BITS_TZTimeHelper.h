// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_TZTimeHelper.h
/// Contains Engine::TZTimeHelper function

#ifndef B2BITS_FIX_TYPES_TZTimeHelper_H
#define B2BITS_FIX_TYPES_TZTimeHelper_H

#ifdef _WIN32
struct _FILETIME;
struct _SYSTEMTIME;
#endif

#include <string>
#include <iosfwd>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_String.h>

namespace Engine
{

    namespace TZTimeHelper
    {
        /**
        * Time flags for converts to string
        */
        enum TimeFlags { 
                         Seconds              = 0x01 ,                              ///< Converts to string with seconds 
                         Milliseconds         = 0x02 | Seconds,                     ///< Converts to string with milliseconds 
                         Microseconds         = 0x04 | Milliseconds,                ///< Converts to string with microseconds 
                         Nanoseconds          = 0x08 | Microseconds,                ///< Converts to string with nanoseconds 
                         Picoseconds          = 0x10 | Nanoseconds,                 ///< Converts to string with picoseconds 
                         TrimTrailingZeros    = 0x80 ,                              ///< Trim zeros  
                         MillisecondsAndTrim  = Milliseconds | TrimTrailingZeros,   ///< Converts to string with milliseconds and trim zeros
                         MicrosecondsAndTrim  = Microseconds | TrimTrailingZeros, ///< Converts to string with microseconds and trim zeros
                         NanosecondsAndTrim   = Nanoseconds | TrimTrailingZeros, ///< Converts to string with nanoseconds and trim zeros
                         PicosecondsAndTrim   = Picoseconds | TrimTrailingZeros  ///< Converts to string with picoseconds and trim zeros
                       };

        /**
        * Time without UTC offset
        */
        struct V12_API UTCTimeOnly
        {
            /** hours since midnight - [0,23]  */
            System::u8 hour_;
            /** minutes after the hour - [0,59]  */
            System::u8 minute_;
            /** seconds after the minute - [0,59]  */
            System::u8 second_; 
            /** picoseconds after the second - [0,999999999999]  */
            System::u64 picosecond_;

            UTCTimeOnly() { }
            UTCTimeOnly(System::u8 hour, System::u8 minute, System::u8 second, System::u64 picosecond)
                : hour_(hour), minute_(minute), second_(second), picosecond_(picosecond)
            { }
        };

        /**
        * Data about time with UTC offset
        */
        struct V12_API TZTimeOnly : public UTCTimeOnly
        {
            /** utc offset in minutes - [ -720 , +720 ] */
            System::i16 utcOffset_;

            TZTimeOnly() { }
            TZTimeOnly(System::u8 hour, System::u8 minute, System::u8 second, System::u64 picosecond, System::i16 utcOffset)
                : UTCTimeOnly(hour, minute, second, picosecond)
                , utcOffset_(utcOffset) { }
        };

        /**
        * Data about date
        */
        struct V12_API UTCDateOnly
        {
            /** years */
            System::u16 year_; 
            /** months since January - [1,12] */
            System::u8 month_;
            /** day of the month - [1,31] */
            System::u8 day_;

            UTCDateOnly() { }
            UTCDateOnly(System::u16 year, System::u8 month, System::u8 day)
                : year_(year), month_(month), day_(day)
            { }
        };

        /**
        * Data about date and time without UTC offset
        */
        struct V12_API UTCTimestamp : public UTCTimeOnly , public UTCDateOnly
        {
            UTCTimestamp() { }
            UTCTimestamp(System::u16 year, System::u8 month, System::u8 day, System::u8 hour, System::u8 minute, System::u8 second, System::u64 picosecond)
                : UTCTimeOnly(hour, minute, second, picosecond)
                , UTCDateOnly(year, month, day)
            { }
        };

        /**
        * Data about date and time with UTC offset
        */
        struct V12_API TZTimestamp : public TZTimeOnly, public UTCDateOnly
        {
            TZTimestamp() { }
            TZTimestamp(System::u16 year, System::u8 month, System::u8 day, System::u8 hour, System::u8 minute, System::u8 second, System::u64 picosecond, System::i16 utcOffset)
                : TZTimeOnly(hour, minute, second, picosecond, utcOffset)
                , UTCDateOnly(year, month, day)
            { }
        };

        /** Minimum size buffer for convert to format UTCTimeOnly */
        static const unsigned int ValueSizeBufferUTCTimeOnly  =  sizeof( "HH:MM:SS.ssssssssssss" )-1;
        /** Minimum size buffer for convert to format TZTimeOnly */
        static const unsigned int ValueSizeBufferTZTimeOnly   =  sizeof( "HH:MM:SS.ssssssssssss+OO:OO" )-1;
        /** Minimum size buffer for convert to format UTCDateOnly */
        static const unsigned int ValueSizeBufferUTCDateOnly   =  sizeof( "YYYYMMDD" )-1;
        /** Minimum size buffer for convert to format UTCTimestamp */
        static const unsigned int ValueSizeBufferUTCTimestamp   =  sizeof( "YYYYMMDD-HH:MM:SS.ssssssssssss" )-1; 
        /** Minimum size buffer for convert to format TZTimestamp */
        static const unsigned int ValueSizeBufferTZTimestamp   =  sizeof( "YYYYMMDD-HH:MM:SS.ssssssssssss+OO:OO" )-1;

        /** Lenght of string format UTCTimestamp with picosecond */
        static const unsigned int ValueSizeUTCTimestampPico   =  sizeof( "YYYYMMDD-HH:MM:SS.ssssssssssss" )-1; 
        /** Lenght of string format UTCTimestamp with nanosecond */
        static const unsigned int ValueSizeUTCTimestampNano   =  sizeof( "YYYYMMDD-HH:MM:SS.sssssssss" )-1; 
        /** Lenght of string format UTCTimestamp with microsecond */
        static const unsigned int ValueSizeUTCTimestampMicro   =  sizeof( "YYYYMMDD-HH:MM:SS.ssssss" )-1; 
        /** Lenght of string format UTCTimestamp with millisecond */
        static const unsigned int ValueSizeUTCTimestampMilli   =  sizeof( "YYYYMMDD-HH:MM:SS.sss" )-1; 
        /** Lenght of string format UTCTimestamp with second */
        static const unsigned int ValueSizeUTCTimestampSec   =  sizeof( "YYYYMMDD-HH:MM:SS" )-1; 

        /** Lenght of string format UTCTimeOnly with picosecond */
        static const unsigned int ValueSizeUTCTimeOnlyPico   =  sizeof( "HH:MM:SS.ssssssssssss" )-1; 
        /** Lenght of string format UTCTimeOnly with nanosecond */
        static const unsigned int ValueSizeUTCTimeOnlyNano   =  sizeof( "HH:MM:SS.sssssssss" )-1; 
        /** Lenght of string format UTCTimeOnly with microsecond */
        static const unsigned int ValueSizeUTCTimeOnlyMicro   =  sizeof( "HH:MM:SS.ssssss" )-1; 
        /** Lenght of string format UTCTimeOnly with millisecond */
        static const unsigned int ValueSizeUTCTimeOnlyMilli   =  sizeof( "HH:MM:SS.sss" )-1; 
        /** Lenght of string format UTCTimeOnly with second */
        static const unsigned int ValueSizeUTCTimeOnlySec   =  sizeof( "HH:MM:SS" )-1; 

        /// Parses string UTCTimeOnly - "HH:MM:SS[.sss]" to UTCTimeOnly structure
        /// @param data Memory buffer with string
        /// @param lenght Lenght of string in data
        /// @param[out] time structure UTCTimeOnly with data parsing
        /// @return success parsing
        bool V12_API parseUTCTimeOnly(const char *data , size_t lenght, UTCTimeOnly & time );

        /// Parses string UTCTimestamp - "YYYYMMDD-HH:MM:SS[.sss]" to UTCTimestamp structure
        /// @param data Memory buffer with string
        /// @param lenght Lenght of string in data
        /// @param[out] timestamp structure UTCTimestamp with data parsing
        /// @return success parsing
        bool V12_API parseUTCTimestamp(const char *data , size_t lenght, UTCTimestamp & timestamp );

        /// Parses string UTCDateOnly - "YYYYMMDD" to UTCDateOnly structure
        /// @param data Memory buffer with string
        /// @param lenght Lenght of string in data
        /// @param[out] date structure UTCDateOnly with data parsing
        /// @return success parsing
        bool V12_API parseDateOnly(const char *data , size_t lenght, UTCDateOnly & date );

        /// Parses string UTCTimeOnly( "HH:MM:SS[.sss]" ) or TZTimeOnly( "HH:MM[:SS][.sss][Z | [ + | - hh[:mm]]]" ) to TZTimeOnly structure
        /// @param data Memory buffer with string
        /// @param lenght Lenght of string in data
        /// @param[out] time structure TZTimeOnly with data parsing
        /// @return success parsing
        bool V12_API parseTimeOnly(const char *data , size_t lenght, TZTimeOnly & time );

        /// Parses string UTCTimestamp( "YYYYMMDD-HH:MM:SS[.sss]" ) or TZTimestamp( "YYYYMMDD-HH:MM:SS[.sss][Z | [ + | - hh[:mm]]]" ) to TZTimestamp structure
        /// @param data Memory buffer with string
        /// @param lenght Lenght of string in data
        /// @param timestamp structure TZTimestamp with data parsing
        /// @return success parsing
        bool V12_API parseTimestamp(const char *data , size_t lenght, TZTimestamp & timestamp );

        /// Converts to char buffer TZTimeOnly( "HH:MM[:SS][.sss][Z | [ + | - hh[:mm]]]" ) without terminating zero
        /// @param[out] data Memory buffer
        /// @param lenght Size memory buffer ( use ValueSizeBufferTZTimeOnly or more )
        /// @param time structure TZTimeOnly with time data
        /// @param flags Time precision flags for conversion to string representation
        /// @return Lenght string
        //tzTimeOnlytoString
        size_t V12_API tzTimeOnlyToString( char *data , size_t lenght , const TZTimeOnly & time , TimeFlags flags = Milliseconds );

        /// Converts to char buffer UTCTimeOnly( "HH:MM:SS[.sss]" ) without terminating zero
        /// @param[out] data Memory buffer
        /// @param lenght Size memory buffer ( use ValueSizeBufferUTCTimeOnly or more )
        /// @param time structure UTCTimeOnly with time data
        /// @param flags Time precision flags for conversion to string representation
        /// @return Lenght string
        size_t V12_API utcTimeOnlyToString( char *data , size_t lenght , const UTCTimeOnly & time , TimeFlags flags = Milliseconds );
        
        /// Converts to char buffer UTCDateOnly( "YYYYMMDD" ) without terminating zero
        /// @param[out] data Memory buffer
        /// @param lenght Size memory buffer ( use sizeBufferTZDateOnly or more )
        /// @param date structure UTCDateOnly with date data
        /// @return Lenght string
        size_t V12_API utcDateOnlyToString( char *data , size_t lenght , const UTCDateOnly  date );

        /// Converts to string UTCTimestamp( "YYYYMMDD-HH:MM:SS[.sss]" ) without terminating zero
        /// @param[out] data Memory buffer
        /// @param lenght Size memory buffer ( use ValueSizeBufferUTCTimestamp or more )
        /// @param timestamp structure UTCTimestamp with date and time
        /// @param flags Time precision flags for conversion to string representation
        /// @return Lenght string
        size_t V12_API utcTimestampToString( char *data , size_t lenght , const UTCTimestamp & timestamp , TimeFlags flags = Milliseconds );

        /// Converts to char buffer TZTimestamp( "YYYYMMDD-HH:MM:SS[.sss][Z | [ + | - hh[:mm]]]" ) without terminating zero
        /// @param[out] data Memory buffer
        /// @param lenght Size memory buffer ( use ValueSizeBufferTZTimestamp or more )
        /// @param timestamp structure TZTimestamp with date and time
        /// @param flags Time precision flags for conversion to string representation
        /// @return Lenght string
        size_t V12_API tzTimestampToString( char *data , size_t lenght , const TZTimestamp & timestamp , TimeFlags flags  );

        /// Fills current time in TZTimeOnly
        /// @param time structure TZTimeOnly with time data
        void V12_API nowTZTimeOnly( TZTimeOnly & time );

        /// Get current time in local time zone
        TZTimeOnly V12_API nowTZTimeOnly();

        /// Fills current time in UTCTimeOnly
        /// @param time structure UTCTimeOnly with time data
        void V12_API nowUTCTimeOnly( UTCTimeOnly & time );

        /// Get current time in UTC
        UTCTimeOnly V12_API nowUTCTimeOnly();

        /// Fills current date in UTCDateOnly in local time zone
        /// @param date structure UTCDateOnly with date data
        void V12_API nowTZDateOnly( UTCDateOnly & date );

        /// Get current date with local time zone
        UTCDateOnly V12_API nowTZDateOnly();

        /// Fills current date in UTCDateOnly in UTC
        /// @param date structure UTCDateOnly with date data
        void V12_API nowUTCDateOnly( UTCDateOnly & date );

        /// Get current date in UTC
        UTCDateOnly V12_API nowUTCDateOnly();

        /// Fills current date and time in TZTimestamp
        /// @param timestamp structure TZTimeOnly with date and time in local time zone
        void V12_API nowTZTimestamp( TZTimestamp & timestamp );

        /// Get current time and date in local time zone
        TZTimestamp V12_API nowTZTimestamp();

        /// Fills current date and time in TZTimestamp
        /// @param timestamp structure TZTimeOnly with date and time in UTC
        void V12_API nowUTCTimestamp(UTCTimestamp & timestamp );

        /// Get current date and time in UTC
        UTCTimestamp V12_API nowUTCTimestamp();

        /// Converts TZTimestamp to UTCTimestamp 
        /// @param[out] utcTimestamp destination UTCTimestamp data
        /// @param tzTimestamp source TZTimestamp data
        void V12_API utcTimestampFromTZTimestamp( UTCTimestamp & utcTimestamp , const TZTimestamp & tzTimestamp );

        /// Converts TZTimestamp to time_t
        /// @param timestamp source TZTimestamp data
        /// @param convertToUTC Convert or not in UTC
        /// @return time_t data
        time_t V12_API timeTFromTZTimestamp( const TZTimestamp & timestamp , bool convertToUTC = true );

        /// Converts UTCTimestamp to time_t
        /// @return time_t data
        time_t V12_API timeTFromUTCTimestamp( const UTCTimestamp & timestamp );

        /// Converts time_t to UTCTimestamp
        /// @param[out] timestamp destination TZTimestamp data
        /// @param time_utc source time_t data in utc
        /// @param utcOffset UTC Offset for TZTimestamp
        void V12_API tzTimestampFromTimeT( TZTimestamp & timestamp ,  time_t time_utc , System::i16 utcOffset = 0 );

        /// Converts time_t to UTCTimestamp
        /// @param[out] timestamp destination UTCTimestamp data
        /// @param time_utc source time_t data in utc
        void V12_API utcTimestampFromTimeT( UTCTimestamp & timestamp ,  time_t time_utc );

#ifdef _WIN32


        /// Converts TZTimestamp to FILETIME
        /// @param[out] ft destination FILETIME data
        /// @param timestamp source TZTimestamp data 
        /// @param convertToUTC Convert or not in UTC
        void V12_API filetimeFromTZTimestamp( _FILETIME& ft, const TZTimestamp & timestamp , bool convertToUTC = true );

        /// Converts UTCTimestamp to FILETIME
        /// @param[out] ft destination FILETIME data
        /// @param timestamp source UTCTimestamp data 
        void V12_API filetimeTFromUTCTimestamp( _FILETIME& ft, const UTCTimestamp & timestamp );

        /// Converts FILETIME to TZTimestamp
        /// @param[out] timestamp destination TZTimestamp data 
        /// @param ft source FILETIME data
        /// @param utcOffset UTC Offset for TZTimestamp
        void V12_API tzTimestampFromFiletime( TZTimestamp & timestamp ,  const _FILETIME& ft , System::i16 utcOffset = 0 );

        /// Converts FILETIME to UTCTimestamp
        /// @param[out] timestamp destination UTCTimestamp data 
        /// @param ft source FILETIME 
        void V12_API utcTimestampFromFiletime( UTCTimestamp & timestamp ,  const _FILETIME& ft );

#else

        /// Converts TZTimestamp to timespec
        /// @param[out] ts destination timespec data
        /// @param timestamp source TZTimestamp data 
        /// @param convertToUTC Convert or not in UTC
        void V12_API timeSpecFromTZTimestamp( struct timespec& ts, const TZTimestamp & timestamp , bool convertToUTC = true );

        /// Converts UTCTimestamp to timespec
        /// @param[out] ts destination timespec data
        /// @param timestamp source UTCTimestamp data 
        void V12_API timeSpecTFromUTCTimestamp( struct timespec& ts, const UTCTimestamp & timestamp );

        /// Converts timespec to TZTimestamp
        /// @param[out] timestamp destination TZTimestamp data 
        /// @param ts source timespec data
        /// @param utcOffset UTC Offset for TZTimestamp
        void V12_API tzTimestampFromTimeSpec( TZTimestamp & timestamp ,  const struct timespec& ts , System::i16 utcOffset = 0 );

        /// Converts timespec to UTCTimestamp
        /// @param[out] timestamp destination UTCTimestamp data 
        /// @param ts source timespec data
        void V12_API utcTimestampFromTimeSpec( UTCTimestamp & timestamp ,  const struct timespec& ts );

#endif

    }

}

#endif
