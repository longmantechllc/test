//
// Created by schen on 10/14/2020.
//

#ifndef RTXROUTER_RTXUTILS_H
#define RTXROUTER_RTXUTILS_H

#include <climits>

using namespace std;

class Price;

#define DESTROY(Ptr)  delete Ptr; Ptr = nullptr;

class RTXUtils {
public:

    static double StrToDouble(char* val, double deflt=-999999.0);
    static Price StrToPrice(char* val, double deflt=-999999.0);
    static bool StrToBool(char* val, bool deflt=false);
    static int StrToInt(char* val, int deflt =INT_MIN);
    static __int64_t StrToLint(char* val, int deflt =INT_MIN);

    static double StrToDouble(std::string& val, double deflt=-999999.0);
    static Price StrToPrice(std::string& val, double deflt=-999999.0);
    static bool StrToBool(std::string& val, bool deflt=false);
    static int StrToInt(std::string& val, int deflt =INT_MIN);
    static __int64_t  StrToLint(std::string& val, int deflt =INT_MIN);
    static std::vector<std::string> stringToVector( std::string& str, const std::string& separator="," );
    static bool IsValidStrToBoolFormat(std::string val);

    static int RoundToLotSize(int val, int lotSize, int upOrDown = 0);  //0:nearest, 1:up, 2:down
    static bool DoubleEqual(double val1, double val2);
    static bool DoubleGE(double val1, double val2);
    static bool DoubleLE(double val1, double val2);
    static bool DoubleGT(double val1, double val2);
    static bool DoubleLS(double val1, double val2);


    static int StrToTimeSsm(const std::string& val, int deflt =-1);
    static int GetSsm( int hours, int mins, int secs );
    static int StrToTimeMssm(const std::string& val, int deflt =-1 );
    static int GetMssm( int hours, int mins, int secs, int msecs );
    static int NowSsm();
    static int NowMssm();
    static int TimeDiffLocalvsUTC();

    static void trimString(std::string& str);

};

#endif //RTXROUTER_RTXUTILS_H
