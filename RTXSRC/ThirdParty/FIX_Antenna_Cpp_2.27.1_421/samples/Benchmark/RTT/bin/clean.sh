#!/bin/sh

if [ "$1" == "client" ] || [ "$1" == "" ]
then
    rm -f logs.client/*.in logs.client/*.out logs.client/*.log logs.client/*.conf logs.client/_log
fi

if [ "$1" == "server" ] || [ "$1" == "" ]
then
    rm -f logs.server/*.in logs.server/*.out logs.server/*.log logs.server/*.conf logs.server/_log
fi
