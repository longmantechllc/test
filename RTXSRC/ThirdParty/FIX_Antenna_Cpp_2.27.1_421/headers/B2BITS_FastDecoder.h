// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FastDecoder.h
/// Contains Engine::FastDecoder class declaration.

#ifndef H_B2BITS_Engine_FastDecoder_H
#define H_B2BITS_Engine_FastDecoder_H

#include <map>
#include <set>
#include <memory>
#include <cstddef>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_FIXMessage.h>

namespace Engine
{
    /**
    * Decodes buffers, that contains FAST messages, to FIX messages.
    */
    class V12_API FastDecoder
    {
    public:
        class Impl;
        typedef System::u32 TID;

    public:
        /**
        * Constructor.
        */
        FastDecoder( Impl* impl );

        /**
        * Destructor.
        */
        ~FastDecoder();

        /**
        * Takes a FAST message from the specified buffer and decodes it to a FIX message.
        *
        * @param[in] fastMessage - pointer to array of char that contains FAST message to be decoded.
        * @param[in] size - the array size.
        * @param[out] decoded - amount bytes were decoded.
        * @return instance of FIXMessage class.
        * @throw - an std::exception with detailed information if an error occurs.
        */
        std::auto_ptr<FIXMessage> decode( const char* fastMessage, std::size_t size, std::size_t* decoded = NULL );

        /**
        * Returns the last used template id
        */
        TID getLastUsedTID() const;

        /**
        * Resets the fast decoder dictionary.
        */
        void resetDictionary();

        /** Overloaded delete operator */
        void operator delete( void* obj );

        /**
        * Decodes unsigned int32 at the beginning of the stream
        *
        * @param[in] fastMessage - pointer to array of char that contains FAST stream.
        * @param[in] size - the array size.
        * @param[out] decoded - amount bytes were decoded.
        * @return decoded unsigned int32.
        * @throw - an std::exception with detailed information if an error occurs.
        */
        System::u32 decode_u32( const char* fastMessage, std::size_t size, std::size_t* decoded = NULL );

    protected:
        FastDecoder( const FastDecoder& );
        FastDecoder& operator =( const FastDecoder& );

    private:
        Impl* impl_;
    }; // FastDecoder
} // namespace Engine
#endif // H_B2BITS_Engine_FastDecoder_H
