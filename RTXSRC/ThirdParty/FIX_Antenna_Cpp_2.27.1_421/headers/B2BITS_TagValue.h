// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_TagValue.h
/// Contains Engine::TagValue class declaration.

#ifndef __B2BITS_Engine_V12_TagValue_h__
#define __B2BITS_Engine_V12_TagValue_h__

#include <iosfwd>
#include <set>
#include <vector>

#include <B2BITS_IntDefines.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_Types.h>

namespace Engine
{
    class TagValueEx;
    class FixFieldsFunctor;

    /// Represents value of field
    template< typename TagValueT >
    class FieldValueT
    {
    public:
        /// Short name of this instantiation
        typedef FieldValueT<TagValueT> MyT;

        /// Constructor
        FieldValueT( TagValueT* aTv, int aTag ) :
            tv_( aTv ),
            tag_( aTag ) {
        }

        /// Returns value as 64bit signed integer
        /// @return value of the field
        System::i64 toInt64() const {
            return tv_->getAsInt64( tag_ );
        }

        /// Returns value as 64bit unsigned integer
        /// @return value of the field
        System::u64 toUInt64() const {
            return tv_->getAsUInt64( tag_ );
        }

        /// Returns value as 32bit signed integer
        /// @return value of the field
        System::i32 toInt32() const {
            return tv_->getAsInt32( tag_ );
        }

        /// Returns value as 32bit unsigned integer
        /// @return value of the field
        System::u32 toUInt32() const {
            return tv_->getAsUInt32( tag_ );
        }

        /// Returns value as signed integer
        /// @return value of the field
        int toInt() const {
            return tv_->getAsInt( tag_ );
        }

        /// Returns value as unsigned integer
        /// @return value of the field
        unsigned int toUInt() const {
            return tv_->getAsUInt( tag_ );
        }

        /// Returns value as double
        /// @return value of the field
        double toDouble() const {
            return tv_->getAsDouble( tag_ );
        }

        /// Returns value as Decimal
        /// @return value of the field
        Decimal toDecimal() const {
            return tv_->getAsDecimal( tag_ );
        }

        /// Returns value as char
        /// @return value of the field
        char toChar() const {
            return tv_->getAsChar( tag_ );
        }

        /// Returns value as std::string
        /// @return value of the field
        AsciiString toString() const {
            return tv_->getAsString( tag_ );
        }

        /// Returns value as byte array
        /// @return value of the field
        ByteArray toRaw() const {
            return tv_->getAsRaw( tag_ );
        }

        /// Returns value as byte array
        /// @return value of the field
        bool toBoolean() const {
            return tv_->getAsBoolean( tag_ );
        }

        /// Returns value as MonthYear
        /// @return value of the field
        MonthYear toMonthYear() const {
            return tv_->getAsMonthYear( tag_ );
        }

        /// Returns value as LocalMktDate
        /// @return value of the field
        LocalMktDate toLocalMktDate() const {
            return tv_->getAsLocalMktDate( tag_ );
        }

        /// Returns value as DateOnly
        /// @return value of the field
        UTCDateOnly toDateOnly() const {
            return tv_->getAsDateOnly( tag_ );
        }

        /// Returns value as TimeOnly
        /// @return value of the field
        UTCTimeOnly toTimeOnly() const {
            return tv_->getAsTimeOnly( tag_ );
        }

        /// Returns value as Timestamp
        /// @return value of the field
        UTCTimestamp toTimestamp() const {
            return tv_->getAsTimestamp( tag_ );
        }

        MultipleString toMultipleString() const {
            return tv_->getAsMultipleString( tag_ );
        }

        MultipleChar toMultipleChar() const {
            return tv_->getAsMultipleChar( tag_ );
        }

        /// Returns value as repeating group
        /// @return value of the field
        FIXGroup& toGroup() {
            return tv_->getAsGroup( tag_ );
        }

        /// Returns value as repeating group
        /// @return value of the field
        FIXGroup const& toGroup() const {
            return tv_->getAsGroup( tag_ );
        }

        /// Returns true if value is empty; false otherwise.
        /// @return true if value is empty; false otherwise.
        bool isEmpty() const {
            return tv_->isEmpty( tag_ );
        }

        /// Clears value of the field
        void reset() {
            tv_->reset( tag_ );
        }

        /// Compares field value with passed value
        bool equal( Integer value ) const {
            return tv_->isEqual( tag_, value );
        }

        /// Compares field value with passed value
        bool equal( UInteger value ) const {
            return tv_->isEqual( tag_, value );
        }

        /// Compares field value with passed value
        bool equal( ByteArray value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsRaw( tag_ ) == value;
        }

        /// Compares field value with passed value
        bool equal( AsciiString value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsString( tag_ ) == value;
        }

        /// Compares field value with passed value
        bool equal( char const* value ) const {
            return equal( AsciiString( value ) );
        }

        /// Compares field value with passed value
        bool equal( Decimal value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsDecimal( tag_ ) == value;
        }

        /// Compares field value with passed value
        bool equal( double value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsDouble( tag_ ) == value;
        }

        /// Compares field value with passed value
        template< typename T >
        bool equal( FieldValueT< T > value ) const {
            return tv_->isEqual( tag_, value.tv(), value.tag() );
        }

        /// Compares field value with passed value
        bool equal( MyT value ) const {
            return tv_->isEqual( tag_, value.tv(), value.tag() );
        }

        /// Compares field value with passed value
        bool equal( char value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsChar( tag_ ) == value;
        }

        /// Compares field value with passed value
        bool equal( bool value ) const {
            if( tv_->isEmpty( tag_ ) ) {
                return false;
            }

            return tv_->getAsBool( tag_ ) == value;
        }

        /// Writes field value to stream
        void write( std::ostream& stream ) const {
            tv_->write( tag_, stream );
        }

        /// Returns tag of the field
        int tag() const {
            return tag_;
        }

        /// Returns container of the field
        TagValueT* tv() const {
            return tv_;
        }

    private:
        TagValueT* tv_;
        int tag_;
    };

    /// Represents tag-value structure.
    /// Base class for FIX message and for repeating group entry
    class V12_API TagValue
    {
        template< typename >
        friend class FieldValueT;
    public:
        typedef FieldValueT< TagValue const > const ConstFieldValue;
        typedef FieldValueT< TagValue > FieldValue;

    public:
        /// Constructor
        TagValue();

        /// Returns value of the field specified by tag
        /// @return value of the field
        ConstFieldValue getField( int tag ) const {
            return ConstFieldValue( this, tag );
        }

        /// Returns value of the field specified by tag
        /// @return value of the field
        FieldValue getField( int tag ) {
            return FieldValue( this, tag );
        }

        /**
         * Reserves memory to store prepared field value.
         *
         * @param[in] tag Tag of the field.
         * @param[in] index User defined index of the prepared field.
         * @param[in] size Size of the field value to reserve.
         */
        virtual void reserve( int tag, PreparedFieldIndex index, std::size_t size ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, MonthYear value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, LocalMktDate value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, UTCDateOnly value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, UTCTimeOnly value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, UTCTimestamp value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, System::i32 value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, System::u32 value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, System::i64 value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, System::u64 value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, double value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, Decimal value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, AsciiString value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, ByteArray value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, char value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool set( int tag, bool value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        bool set( int tag, char const* value ) {
            return set( tag, AsciiString( value ) );
        }

        /**
        * Updates field value by tag number. The returned value indicates previous
        * value existance, i.e if previous value wasn't empty, set returns true,
        * otherwise false.
        *
        * @param tag FIX field tag to set data to.
        * @param value New value for field.
        *
        * @return "true" if request field had value before call of set,
        * otherwise "false" (i.e. tag was inserted into message).
        *
        * @throw Utils::Exception if requested field not defined this message type.
        */
        virtual bool set( int tag, const MultipleString& value ) = 0;

        /**
        * Updates field value by tag number. The returned value indicates previous
        * value existance, i.e if previous value wasn't empty, set returns true,
        * otherwise false.
        *
        * @param tag FIX field tag to set data to.
        * @param value New value for field.
        *
        * @return "true" if request field had value before call of set,
        * otherwise "false" (i.e. tag was inserted into message).
        *
        * @throw Utils::Exception if requested field not defined this message type.
        */
        virtual bool set( int tag, const MultipleChar& value ) = 0;

        /**
         * Returns list of the message fields and their values.
         *
         * @param[out] list List of the message fields and values.
         */
        virtual FixFieldsContainer* getFields() const = 0;

        /** Retrieves field value by tag number
         *
         * @param tag FIX field tag to get data from.
         *
         * @return pointer to the buffer contained requested field value.
         * If field value is empty returns NULL pointer. Can return NULL.
         *
         * @warning The returned buffer is shared for all calls. This means
         * stored content is valid untill the next call of the get().
         *
         * @throw Utils::Exception if requested field is not defined by FIX protocol for this message type.
         *
         * @see get( int, FIXFieldValue *) const
         * @deprecated Use Engine::TagValue::get( int, FIXFieldValue *) const instead.
         */
        B2B_DEPRECATED( "Use TagValue::get( int, FIXFieldValue *) const instead" )
        virtual const std::string* get( int tag ) const = 0;

        /**
         * Retrieves field value by tag number and stores it into instance of FIXFieldValue class.
         *
         * @param[out] value pointer to the FIXFieldValue object to store data. Cannot be NULL.
         * @param tag Tag number of field requested field value.
         *
         * @return "true" if requested field exists in message, "false" if
         * requested field does not exist.
         *
         * @throw Utils::Exception if requested field is not defined by FIX protocol for this message type.
        */
        virtual bool get( int tag, FIXFieldValue* value ) const = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined by FIX protocol for this message type.
         * message type.
         */
        virtual bool set( int tag, const FIXFieldValue& value ) = 0;

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined for this message type.
         */
        virtual bool set( int tag, const std::string& value ) = 0;


        /**
         * Empties field value by tag number. The returned value indicates previous
         * value existence, i.e if previous value wasn't empty, remove returns true,
         * otherwise false.
         *
         * @param tag Tag of field to remove.
         *
         * @return "true" if request field had value before call of remove,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field not defined this message type.
         */
        virtual bool remove( int tag ) = 0;

        /**
         * Removes passed tags from message.
         *
         * @param tags Set of field tags to remove.
         *
         * @throw Utils::Exception if one of the requested fields isn't defined this message type.
         */
        virtual void remove( const std::set<int>& tags ) = 0;

        /** Checks boolean field
         *
         * @param tag Tag number of the field to verify.
         *
         * @return "true" if the given flag is present and it
         * equals to "Y", otherwise "false".
         */
        virtual bool hasFlag( int tag ) const = 0;

        /**
         * Verifies is field acceptable in the message
         *
         * @param tag Tag number of the field to verify.
         *
         * @return "true" if field supported by this message,
         * otherwise returns false.
         */
        virtual bool isSupported( int tag ) const throw() = 0;

        /** Returns the repeating group instance by leading field tag number. 
         * The FIXGroup instance's memory is managed by FIX Engine. 
         * FIXGroup instance is guaranteed to exist within it's 
         * parent FIXMessage's lifetime or until trimming to zero length.
         *
         * @param tag Tag number of the leading field value.
         *
         * @return Returns the repeating group instance by leading field tag number. 
         * If leading field value is empty or its numerical represntaion
         * is zero, then returned value is NULL.
         *
         * @throw Utils::Exception if requested group not
         * defined this message type.
         */
        virtual FIXGroup const* getGroup( int tag ) const = 0;

        /** Returns the repeating group instance by leading field tag number.
         * The FIXGroup instance's memory is managed by FIX Engine. 
         * FIXGroup instance is guaranteed to exist within it's  
         * parent FIXMessage's lifetime or until trimming to zero length.
         *
         * @param tag Tag number of the leading field value.
         *
         * @return Returns the repeating group instance by leading field tag number.
         * If leading field value is empty or its numerical represntaion
         * is zero, then returned value is NULL.
         *
         * @throw Utils::Exception if requested group not
         * defined this message type.
         */
        virtual FIXGroup* getGroup( int tag ) = 0;

        /**
         * Writes field value to the stream
         * @param[in] tag Tag of the field.
         * @param[out] stream Output stream to write field value to.
         */
        virtual void write( int tag, std::ostream& stream ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual System::i64 getAsInt64( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual System::u64 getAsUInt64( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual System::i32 getAsInt32( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual System::u32 getAsUInt32( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual int getAsInt( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual unsigned int getAsUInt( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual double getAsDouble( int tag ) const = 0;
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual Decimal getAsDecimal( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual char getAsChar( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual AsciiString getAsString( int tag ) const = 0;

        /**
         * Returns true if field is not empty, false otherwise.
         * @param[in] tag Tag of the field.
         * @param[out] value Value of the field.
         */
        virtual bool get( int tag, std::string* value) const = 0;
        
        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual ByteArray getAsRaw( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual bool getAsBoolean( int tag ) const = 0;

        /**
         * Returns field value. Synonym of Engine::TagValue::getAsBoolean
         * @param[in] tag Tag of the field.
         */
        bool getAsBool( int tag ) const {
            return getAsBoolean( tag );
        }

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual MonthYear getAsMonthYear( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual LocalMktDate getAsLocalMktDate( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual UTCDateOnly getAsDateOnly( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual UTCTimeOnly getAsTimeOnly( int tag ) const = 0;

        /**
         * Returns field value.
         * @param[in] tag Tag of the field.
         */
        virtual UTCTimestamp getAsTimestamp( int tag ) const = 0;

        /**
        * Returns field value.
        * @param[in] tag Tag of the field.
        */
        virtual MultipleString getAsMultipleString( int tag ) const = 0;

        /**
        * Returns field value.
        * @param[in] tag Tag of the field.
        */
        virtual MultipleChar getAsMultipleChar( int tag ) const = 0;

        FIXGroup& getAsGroup( int tag );
        FIXGroup const& getAsGroup( int tag ) const;

        /** 
        * Copies field value to another instance of the TagValue.
        *
        * @throw Utils::Exception if source field is not defined by FIX specification.
        * @throw Utils::Exception if destination field is not defined by FIX specification.
        * @throw Utils::Exception if source field is value is not defined.
        */
        virtual void copyTo( TagValue* dest, int destTag, int srcTag ) const = 0;

        /** Compares values of the fields */
        virtual bool isEqual( int tag, TagValue const* rv, int rvTag ) const = 0;

        /** 
        * Returns true if value of the field is empty; false otherwise.
        * @throw Utils::Exception is given field is not defined for this entry.
        */
        virtual bool isEmpty( int tag ) const = 0;

        /** 
        * Returns true if value is defined; false otherwise.
        * @throw Utils::Exception is given field is not defined for this entry.
        */
        bool hasValue( int tag ) const;

        /** Returns extended interface of TagValue*/
        virtual TagValueEx* extendTV() throw() = 0;

        /** Returns extended interface of TagValue*/
        virtual TagValueEx const* extendTV() const throw() = 0;

        /** Releases FIX message instance.
         * @see release(TagValue const* msg)
         */
        virtual void release() const = 0;

        /** Releases FIX message instance.
         * @see release(TagValue const* msg)
         */
        virtual void release() = 0;

        /** Apply the given functor to every field
          * @see FixFieldsFunctor::operator(int tag, const Engine::AsciiString& value, int role)
          */
        virtual void apply(FixFieldsFunctor& f) const = 0;

        /** Releases assigned resources */
        static void release( TagValue const* obj );

    protected:
        /// Destructor
        virtual ~TagValue();
    }; // class TagValue

    /// Compares FieldValue and object
    template< typename TagValueT, typename T >
    inline bool operator==( FieldValueT<TagValueT> lv, T value )
    {
        return lv.equal( value );
    }

    /// Compares FieldValue and object
    template< typename TagValueT, typename T >
    inline bool operator!=( FieldValueT<TagValueT> lv, T value )
    {
        return !lv.equal( value );
    }

    /// Compares FieldValue and object
    template< typename TagValueT, typename T >
    inline bool operator==( T value, FieldValueT<TagValueT> rv )
    {
        return rv.equal( value );
    }

    /// Compares FieldValue and object
    template< typename TagValueT, typename T >
    inline bool operator!=( T value, FieldValueT<TagValueT> rv )
    {
        return !rv.equal( value );
    }

    /// Compares two FieldValues
    template< typename TagValueTL, typename TagValueTR >
    inline bool operator==( FieldValueT<TagValueTL> lv, FieldValueT<TagValueTR> value )
    {
        return lv.equal( value );
    }

    /// Compares two FieldValues
    template< typename TagValueTL, typename TagValueTR >
    inline bool operator!=( FieldValueT<TagValueTL> lv, FieldValueT<TagValueTR> value )
    {
        return !lv.equal( value );
    }

    /// Writes FieldValue to the std::ostream
    template< typename TagValueT >
    inline std::ostream& operator <<( std::ostream& stream, FieldValueT<TagValueT> value )
    {
        value.write( stream );
        return stream;
    }
} // namespace Engine

#endif //__B2BITS_Engine_V12_TagValue_h__

