#pragma once

#include <string>
#include "Properties.h"
#include "ApplicationProperties.h"

/// Class Helper is a collection of useful routines.
class Helper
{
public:
    /// Writes line to console.
    static void writeLineToConsole(const std::string& message = std::string());

    /// Writes line to stderr.
    static void writeErrorLine(const std::string& message = std::string());

    static void writeUsageInfo(const std::string& applicationName);

    static void parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params);

    Helper() = delete;
};

