#pragma once

#include <string>
#include <map>
#include <iosfwd>

namespace Aux
{

    class Properties
    {

    public:

        static const char* const ACCEPTOR_CFG;
        static const char* const LitVenueList;
        static const char* const IOCOrderFillRate;
        static const char* const LitIOCOrderFillRate;
        static const char* const DayOrderFillRate;
     
        /**
         * Constructor.
         * Reads a property list (key and element pairs) from the input stream.
         *
         * A line that contains only whitespace or whose first non-whitespace
         * character is an ASCII # or ! is ignored
         * (thus, # or ! indicate comment lines).
         *
         * Note: If required property is not found the default value is used.
         */
        Properties( std::istream* apIStream );

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter
         *
         * @throw Exception if such key does not exist.
         */
        std::string getString( const std::string& key ) const;

        bool isPropertyExists( const std::string& key )const;

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter; if property holds incorrect value will be
         *  set to 0.
         *
         * @throw Exception if such key does not exist.
         */
        int getInteger( const std::string& key ) const;

        bool getBool( const std::string& key ) const;

    private:
        typedef std::map<std::string, std::string> MapType;
        MapType m_pairs;
    };

}



