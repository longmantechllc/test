
#ifndef RTXROUTER_EXECUTIONREPORT_H
#define RTXROUTER_EXECUTIONREPORT_H

#include <string>

#include "Price.h"

struct ERSTATUS
{
    static const int OPEN = 0;
    static const int PARTIALY_FILLED = 1;
    static const int FILLED = 2;
    static const int DONE_FOR_DAY = 3;
    static const int CANCELED = 4;
    static const int PENDING_CANCEL = 6;
    static const int STOPPED = 7;
    static const int REJECTED = 8;
    static const int SUSPENDED = 9;
    static const int PENDING_CANCELREPLACE = 14;
    static const int CANCELREJECT = 16;
};

class ExecutionReport
{
public:
    int             orderId_;              //parent order id
    std::string     childOrderId_;
    std::string     execId_;               //for same child order, multi reports, like open, partial fill...
    std::string     accountId_;
    std::string     subAccount_;
    std::string     logInId_;

    std::string     symbol_;
    int             side_;                 //buy, sell, short
    int             size_;                 //child order size
    int             displaySize_;          //max_floor
    Price           price_;                //is double type, but easy to compare    uint8_t         type_;                 //limit, market
    int	            type_;                 //limit, market
    int             status_;               //open, partial fill, cancel, reject, cancel reject...
    std::string     exchange_;
    int             TIF_;                  //time in force

    Price           discretion_;
    int             parentOrderSize_;

    int             cumulativeFillQuantity_; //for same child order
    int             fillSize_;
    int             leaveQty;

    Price           fillPrice_;
    Price           avgPrice_;
    char            execType_;

    long            transactTime_;
};

#endif //RTXROUTER_EXECUTIONREPORT_H
