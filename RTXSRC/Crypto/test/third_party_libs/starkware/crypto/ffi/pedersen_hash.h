#ifndef STARKWARE_CRYPTO_FFI_PEDERSEN_HASH_H_
#define STARKWARE_CRYPTO_FFI_PEDERSEN_HASH_H_

#include "gsl/gsl-lite.hpp"
#include "starkware/crypto/ffi/utils.h"
#include "starkware/algebra/prime_field_element.h"
namespace starkware {

// int Hash(const char* in1, const char* in2, char* out);
int Hash(
    const gsl::byte in1[kElementSize], const gsl::byte in2[kElementSize],
    gsl::byte out[kOutBufferSize]);

} // namespace starkware

#endif  // STARKWARE_CRYPTO_FFI_PEDERSEN_HASH_H_
