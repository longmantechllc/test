/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
  */

#pragma once

#include "RefCounter.h"
#include "Params.h"
#include <B2BITS_V12.h>
#include <B2BITS_Guard.h>
#include <B2BITS_Mutex.h>
#include <B2BITS_PubEngineDefines.h>

namespace b2bitscme {
	typedef RefCounter<Engine::Session> SessionPtr;

	class SessionCollection
	{
	public:
		SessionCollection(void)
		{
	
		}
	
		~SessionCollection(void)
		{
			for (SessionByMarketSegmentIdT::iterator session = sessions_.begin(); session != sessions_.end(); ++session) 
			{
				session->second->registerApplication(NULL);
				session->second->disconnectNonGracefully();
			}
		}

		SessionPtr getSession(const std::string& marketSegmentId)
		{
			Utils::Guard<System::Mutex> g (mutex_);
			SessionByMarketSegmentIdT::iterator sessionIt = sessions_.find(marketSegmentId);
			if (sessions_.end() != sessionIt) 
				return sessionIt->second;
			
			return RefCounter<Engine::Session>();
		}


		void removeSession(const std::string& marketSegmentId)
		{
			Utils::Guard<System::Mutex> g (mutex_);
			SessionByMarketSegmentIdT::iterator sessionIt = sessions_.find(marketSegmentId);
			if (sessions_.end() != sessionIt) 
				sessions_.erase(sessionIt);
		}

		SessionPtr createSession( const std::string &marketSegmentId, Engine::Application* app, const Params& params)
		{
			// Enable input/output message validation  
			Engine::SessionExtraParameters ssnParams;
			ssnParams.validation_.isEnabled_ = true;
			ssnParams.validation_.checkRequiredGroupFields_ = true;
			ssnParams.validation_.prohibitDuplicatedTags_ = true;
			ssnParams.validation_.prohibitTagsWithoutValue_ = true;
			ssnParams.validation_.prohibitUnknownTags_ = true;
			ssnParams.validation_.verifyReperatingGroupBounds_ = true;
			ssnParams.validation_.verifyTagsValues_ = true;

			//These parameters are quite important for CME FIX Session
			if (params.processMessagesOutOfOrder_)
				ssnParams.outOfSequenceMessagesStrategy_ = Engine::OSMS_IGNORE_GAP;
			else {
				// enable internal session's out of sequence queue for message reordering
				ssnParams.outOfSequenceMessagesStrategy_ = Engine::OSMS_QUEUE;
				ssnParams.outOfSequenceMessagesQueueSize_ = 10000;
			}
			ssnParams.resendRequestBlockSize_ = 2500;
			ssnParams.sendLastMsgSeqNumProcessed_ = true;
			ssnParams.customSessionType_ = Engine::CME_SECURE_LOGON;
			ssnParams.cmeSecureKeysFile_ = params.keysFilePath_;

			ssnParams.storageType_ = Engine::persistent_storageType;

			// Make sure all outgoing messages contain tag 57 that equals to Market Data Segment Id
			if ("CGW" == marketSegmentId)
				ssnParams.pTargetSubID_ = 'G';
			else 
				ssnParams.pTargetSubID_ = marketSegmentId;

			ssnParams.pSenderLocationID_ = params.senderLocationID_;
			ssnParams.pSenderSubID_ = params.senderSubID_;

			// Get Primary Host from parameters
			Params::MarketSegmentByIdT::const_iterator paramsIt = params.marketSegments_.find(marketSegmentId);
			if (params.marketSegments_.end() == paramsIt) {
				return SessionPtr();
			}
			
			// Construct Session Qualifier as Id 
			std::string sessionQualifier = marketSegmentId;


			SessionPtr session (Engine::FixEngine::singleton()->createSession(
													app,											// pApp
													("CGW" == sessionQualifier) ?
																		Engine::SessionId(params.senderCompID_, "CME") :
																		Engine::SessionId(params.senderCompID_, "CME", sessionQualifier),
													Engine::FIX42,									// ver
													&ssnParams										// pParam
												), false);


			
			Utils::Guard<System::Mutex> g (mutex_);
			sessions_[marketSegmentId] = session;

			return session;
		}


	private:

		typedef std::map<std::string, SessionPtr > SessionByMarketSegmentIdT;
		SessionByMarketSegmentIdT sessions_;

		System::Mutex mutex_;

	};
}