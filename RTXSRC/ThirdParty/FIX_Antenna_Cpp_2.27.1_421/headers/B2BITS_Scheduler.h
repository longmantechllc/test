/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2019.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#ifndef H_B2BITS_System_Scheduler_H
#define H_B2BITS_System_Scheduler_H

#include <B2BITS_V12_Defines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_TZTimeHelper.h>
#include <B2BITS_Thread.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

namespace System
{
    /** Structure describing scheduler event happened */
    struct V12_API SchedulerTimeEvent
    {
        SchedulerTimeEvent( const Engine::TZTimeHelper::UTCTimestamp& eventTime, unsigned int eventTag, bool dontNotify )
            :eventTime_( eventTime )
            ,eventTag_( eventTag )
            ,dontNotify_( dontNotify )
        {
        }

        /** Event timestamp */
        Engine::TZTimeHelper::UTCTimestamp eventTime_;

        /** Event tag set when event was scheduled */
        unsigned int eventTag_;

        /** Notification suppression flag
         *  if this flag is true than no any notification/action is expected except next event scheduling if any.
         *  if this flag is false than full event processing is expected.
         */
        bool dontNotify_;
    };

    /** Abstract class(interface) that introduces API contract of events generator for a schedule.*/
    class V12_API ScheduleTimeline : public Utils::ReferenceCounter
    {
    public:
        /**
        * Generates next event based on timestamp provided.
        * @param now - a point in time to get next event after.
        * 
        * Returns next event generated.
        */
        virtual SchedulerTimeEvent getNextEvent( const Engine::TZTimeHelper::UTCTimestamp& now ) = 0;

        /**
        * Generates previous event based on timestamp provided.
        * @param now - a point in time to get event before.
        *
        * Returns previous event generated.
        */
        virtual SchedulerTimeEvent getPrevEvent( const Engine::TZTimeHelper::UTCTimestamp& now ) const = 0;
    };
    typedef V12_API Utils::ReferenceCounterSharedPtr<ScheduleTimeline> ScheduleTimelinePtr;

    /** Schedule abstract class that provides access to events generator and serves as callback sink for scheduler.*/
    class V12_API Schedule : public Utils::ReferenceCounter
    {
    public:
        /**
        * Constructor
        * @param scheduleId - schedule id.
        *
        * @param timelinePtr - events generator instance pointer.
        */
        Schedule( const std::string& scheduleId, const ScheduleTimelinePtr& timelinePtr )
            : scheduleId_( scheduleId )
            , scheduleTimeLinePtr_( timelinePtr )
        {
        }
        
        /** Returns smart pointer to events generator.*/
        ScheduleTimelinePtr getScheduleTimeline() const
        {
            return scheduleTimeLinePtr_;
        }

        /** Returns Schedule id.*/
        std::string getScheduleId() const
        {
            return scheduleId_;
        }

        /**
        * Callback routine that is called by scheduler when scheduled event has occurred.
        * @param hitEvent - the event occurred.
        *
        * @param timestamp - actual UTC timestamp when the event has occurred. 
        * This ussualy equals to hitEvent.eventTime_ except cases when scheduler wasn't able to execute event on time.
        * This is event's actual execution timestamp.
        */
        virtual void onTimeEvent( const SchedulerTimeEvent& hitEvent, const Engine::TZTimeHelper::UTCTimestamp& timestamp ) = 0;
    
    private:
        ScheduleTimelinePtr scheduleTimeLinePtr_;
        std::string scheduleId_;

    };
    typedef Utils::ReferenceCounterSharedPtr<Schedule> SchedulePtr;

    /** Abstract class(interface) that introduces API contract of scheduler.*/
    class V12_API Scheduler : public Utils::ReferenceCounter
    {
    public:
        /** Map of schedules registered.*/
        typedef std::map<std::string, SchedulePtr> Schedules;

        enum SchedulerState
        {
            SchedulerState_Stopped = 0,
            SchedulerState_Started = 1
        };

        /**
        * Registers schedule at scheduler.
        * @param schedulePtr - the schdule to register.
        */
        virtual void registerSchedule( const SchedulePtr& schedulePtr ) = 0;

        /**
        * Unregisters schedule from scheduler.
        * @param scheduleId - id of the schdule to unregister.
        */
        virtual void unregisterSchedule( const std::string& scheduleId ) = 0;

        /**
        * Returns schedules registered at the scheduler
        */
        virtual Schedules getSchedules() const = 0;

        /**
        * Returns scheduler registered by id.
        * @param scheduleId - id of the schedule  that was previously registered at the scheduler.
        */
        virtual SchedulePtr getSchedule( const std::string& scheduleId ) const = 0;

        /**
        * Unregisters all registered schedules.
        */
        virtual void unregisterAll() = 0;

        /**
        * Start scheduler operation
        */
        virtual void start() = 0;

        /**
        * Stop scheduler operation
        */
        virtual void stop() = 0;

        /**
        * Returns current scheduler's state.
        */
        virtual SchedulerState getState() = 0;
    };
    typedef Utils::ReferenceCounterSharedPtr<Scheduler> SchedulerPtr;

    /** Implementation of scheduler interface.*/
    class V12_API SchedulerImpl: public Scheduler
    {
    public:
        /**
        * Constructor
        * @param affinity - Affinity mask of scheduler's thread. 0 - means don't set affinity.
        *
        * @param priority - Priority of scheduler's thread.
        */
        SchedulerImpl( unsigned int affinity = 0, Thread::Priority priority = Thread::DefaultPriority );
        virtual ~SchedulerImpl();

        /**
        * Registers schedule at scheduler.
        * @param schedulePtr - the schdule to register.
        *
        * throws SchedulerException with the following error codes set:
        *        ErrorCode_BadSchedule - if schedulePtr doesn't point to valid schedule.
        *        ErrorCode_BadScheduleId - if schedule with the same id is already registered.
        */
        virtual void registerSchedule( const SchedulePtr& schedulePtr );

        /**
        * Unregisters schedule from scheduler.
        * @param scheduleId - id of the schdule to unregister.
        *
        * throws SchedulerException with ErrorCode_BadScheduleId error code set if there is no schedule with requested id is registered.
        */
        virtual void unregisterSchedule( const std::string& scheduleId );

        /**
        * Returns schedules registered at the scheduler
        */
        virtual Schedules getSchedules() const;

        /**
        * Returns scheduler registered by id.
        * @param scheduleId - id of the schedule  that was previously registered at the scheduler.
        *
        * throws SchedulerException with ErrorCode_BadScheduleId error code set if there is no schedule with requested id is registered.
        */
        virtual SchedulePtr getSchedule( const std::string& scheduleId ) const;

        /**
        * Unregisters all registered schedules.
        */
        virtual void unregisterAll();

        /**
        * Start scheduler operation
        */
        virtual void start();

        /**
        * Stop scheduler operation
        */
        virtual void stop();

        /**
        * Returns current scheduler's state.
        */
        virtual SchedulerState getState();
    private:
        class Impl;
        Impl *pImpl_;
    };

} // namespace System

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // H_B2BITS_System_Scheduler_H
