/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cstddef>
#include <cassert>
#include <memory>
#include <limits>
#include <cmath>
#include <algorithm>

#include <B2BITS_SystemDefines.h>
#include <B2BITS_AutoEvent.h>
#include <B2BITS_FixEngine.h>
#include <B2BITS_Session.h>
#include <B2BITS_Application.h>
#include <B2BITS_PreparedMessage.h>
#include <B2BITS_SessionParameters.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXMsgProcessor.h>

#include "B2BITS_HRTimer.h"
#include "B2BITS_Thread.h"

#include "B2BITS_StringUtils.h"
#include "B2BITS_AutoPtr2.h"

// SenderCompID
static std::string const SENDER = "target";
// TargetCompID
static std::string const TARGET = "sender";
//static std::string const RAW_MSG = "8=FIX.4.2""\001""9=1""\001""35=D""\001""49=BLP""\001""56=SCHB""\001""34=01""\001""50=30737""\001""52=20000809-20:20:50""\001""11=90001008""\001""1=10030003""\001""21=2""\001""55=TESTA""\001""54=1""\001""38=4000""\001""40=2""\001""59=0""\001""44=30""\001""47=I""\001""60=20000809-18:20:32""\001""10=000""\001";
static std::string const RAW_MSG = "8=FIX.4.2""\001""9=1""\001""35=8""\001""49=SCHB""\001""56=BLP""\001""34=01""\001""52=20000809-20:20:50""\001""37=00000000""\001""11=90001008""\001""17=00000000""\001""20=0""\001""39=0""\001""1=10030003""\001""150=0""\001""55=TESTA""\001""54=1""\001""38=4000""\001""40=2""\001""44=30""\001""32=0""\001""31=0""\001""151=4000""\001""14=0""\001""6=0""\001""10=173""\001";

static size_t const MsgSeqNumFieldSize = 7;

// Application is registered for events as session-acceptor observer
class EchoApplication: public Engine::Application
{
private:
    // Session to send messages to
    Engine::Session* acceptor_;
    bool usePreparedMsg_;
    bool usePutOptions_;
    System::AutoEvent loggedOutEvent_;

    Utils::AutoPtr2<Engine::FIXMessage, &Engine::FIXMessage::release> msg2send_;
    std::auto_ptr<Engine::GenericPreparedMessage> prepared_msg2send_;

    int uniqueId_;

    Engine::PutMessageOptions putOptions_;

public:

    // Basic initialization.
    // Must be called before starting tests series (e.g. in case of multiple runs)
    // @param initiator Session to send/receive messages
    // @param usePreparedMsg enables or disables prepared messages
    // @param usePutOptions enables or disables PutMessageOptions
    void init( Engine::Session* acceptor, bool usePreparedMsg, bool usePutOptions) {
        // Make sure session is not NULL
        assert( NULL == acceptor_ );

        // Initialize variables, reset to initial values
        acceptor_ = acceptor;
        usePreparedMsg_ = usePreparedMsg;
        usePutOptions_ = usePutOptions;
        putOptions_.overrideSendingTime_ = false;
        putOptions_.overrideSenderCompID_ = false;
        putOptions_.overrideTargetCompID_ = false;

        // Create object message
        msg2send_.reset( Engine::FIXMsgProcessor::singleton()->parse( RAW_MSG ) );
        inflate( msg2send_.get() );

        if(usePreparedMsg_) {
            msg2send_->reserve( FIXFields::ClOrdID,       0, 10 );
            msg2send_->reserve( FIXFields::Price,         1, 7 );
            msg2send_->reserve( FIXFields::OrderQty,      2, 2 );
            msg2send_->reserve( FIXFields::TransactTime,  3, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
            msg2send_->reserve( FIXFields::SendingTime,   4, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
            msg2send_->reserve( FIXFields::ExecID,        5, 10 );
            msg2send_->reserve( FIXFields::OrderID,       6, 10 );
            msg2send_->reserve( FIXFields::LeavesQty,     7, 2 );
            prepared_msg2send_.reset( new Engine::GenericPreparedMessage( msg2send_.get() ) );
        }
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::FIXMessage* msg ) {
        // Make sure session is not NULL
        assert( NULL != acceptor_ );

        // Put message to the session
        if(usePutOptions_)
            acceptor_->put( msg, &putOptions_ );
        else
            acceptor_->put( msg );
    }

    /**
    * Sets static session level fields in the message and reserves space for volatile session level fields.
    * @param[out] pMsg Pointer to the FIXMessage to store field values
    * @param[in] msgSeqNumSize Number of characters for MsgSeqNum field to reserve.
    * @param[in] options Extra parameters. @see Engine::PutMessageOptions.
    */
    void inflate( Engine::FIXMessage* msg ) const {
        Engine::PutMessageOptions putOptions;
        putOptions.overrideSendingTime_ = false;
        acceptor_->inflate( msg, MsgSeqNumFieldSize, &putOptions );
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::PreparedMessage* msg ) {
        // Make sure session is not NULL
        assert( NULL != acceptor_ );

        // Put message to the session
        acceptor_->put( msg );
    }

    void waitDisconnect()
    {
        loggedOutEvent_.wait();
    }

public:

    // Constructor
    EchoApplication()
        : acceptor_( NULL )
        , uniqueId_( 0 )
        , usePutOptions_(false)
        , usePreparedMsg_(false)
    {
    }

private:


    /**
     * A call-back method to process incoming messages.
     * If the application can not process the given message, the FIX Engine will try to deliver it later
     * according to "Delayed Processing Algorithm".
     *
     * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
     *
     * @param msg the incoming message.
     * @param sn The corresponding FIX session.
     *
     * @return true if the application can process the given message, otherwise false.
     */
    bool process( const Engine::FIXMessage& msg, const Engine::Session& ) {
        
        size_t sz;
        if(usePreparedMsg_) {
            prepared_msg2send_->set( 0, msg.getAsString(FIXFields::ClOrdID) ); // ClOrdID
            prepared_msg2send_->set( 1, msg.getAsString(FIXFields::Price) ); // Price
            prepared_msg2send_->set( 2, msg.getAsString(FIXFields::OrderQty) ); // OrderQty
            prepared_msg2send_->set( 3, Engine::UTCTimestamp::now(true) ); // TransactTime
            prepared_msg2send_->set( 4, prepared_msg2send_->get( 3, &sz ), Engine::UTCTimestamp::ValueSizeWithMilliseconds ); // SendingTime
            prepared_msg2send_->set( 5, static_cast<int>( ++uniqueId_ ) ); // ExecID
            prepared_msg2send_->set( 6, static_cast<int>( ++uniqueId_ ) ); // OrderID
            prepared_msg2send_->set( 7, msg.getAsString(FIXFields::OrderQty) ); // LeavesQty

            sendMessage(prepared_msg2send_.get());
        }
        else {
            msg2send_->set( FIXFields::ClOrdID,         msg.getAsString(FIXFields::ClOrdID) ); // ClOrdID
            msg2send_->set( FIXFields::Price,           msg.getAsString(FIXFields::Price) ); // Price
            msg2send_->set( FIXFields::OrderQty,        msg.getAsString(FIXFields::OrderQty) ); // OrderQty
            msg2send_->set( FIXFields::TransactTime,    Engine::UTCTimestamp::now(true) ); // TransactTime
            if(usePutOptions_)
                msg2send_->set( FIXFields::SendingTime,     msg2send_->getAsString( FIXFields::TransactTime )); // SendingTime
            msg2send_->set( FIXFields::ExecID,          static_cast<int>( ++uniqueId_ ) ); // ExecID
            msg2send_->set( FIXFields::OrderID,         static_cast<int>( ++uniqueId_ ) ); // OrderID
            msg2send_->set( FIXFields::LeavesQty,       msg.getAsString(FIXFields::OrderQty) ); // LeavesQty

            sendMessage(msg2send_.get());
        }

        return true;
    }

    void onBeforeMessageIsSent( int, const Engine::Session& , size_t ) {}
    void onAfterMessageIsReceived( char const*, size_t, const Engine::Session& ) {}
    void onLogonEvent( const Engine::LogonEvent*, const Engine::Session& ) {
        std::cout << "Logged in" << std::endl;
    }
    void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {
        std::cout << "Logged out" << std::endl;
        loggedOutEvent_.set();
    }
    void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {}
    void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent*, const Engine::Session& ) {}
    void onMsgRejectEvent( const Engine::MsgRejectEvent*, const Engine::Session& ) {}
    void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent&, const Engine::Session& ) {}
    void onResendRequestEvent( const Engine::ResendRequestEvent&, const Engine::Session& ) {}
    void onNewStateEvent( const Engine::NewStateEvent&, const Engine::Session& ) {}
    void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent&, const Engine::Session& ) {}
    bool onResend( const Engine::FIXMessage&, const Engine::Session& ) {
        return false;
    }
};

void loadArgs(std::vector<std::string>* args, size_t index)
{
    size_t n = 0;
    std::string::size_type pos = std::string::npos;
    std::string key("benchmark.server.arg"), line, arg;
    std::ifstream fs(args->at(index).c_str());
    while(std::getline(fs,line)){
        if( 0 == line.compare(0, key.size(), key) && std::string::npos != (pos = line.find_first_of('=')) ){
            std::istringstream iss(line.substr(pos + 1));
            while(iss >> arg){
                args->insert(args->begin() + index + ++n, arg);
            }
        }
    }
}

// Main routine
void run( int argc, char* argv[] )
{
    std::cout << "Usage: RTTServer [default|null|transient|persistent|splitPersistent|persistentMM] [usePreparedMsg ON|OFF] [usePutOptions ON|OFF] [conf <engine configuration file>]" << std::endl;

    //optional parameters
    bool usePreparedMsg = true;
    bool usePutOptions = true;
    Engine::MessageStorageType storgeType = Engine::default_storageType;
    std::string confFile = "engine.server.properties";

    std::vector<std::string> args;
    for(int i = 1; i < argc; ++i) args.push_back( argv[i] );

    for (size_t i = 0; i < args.size(); ++i) {
        if(args[i] == "usePreparedMsg") {
            if (++i < args.size())
                usePreparedMsg = (args[i] == "ON");
        }
        else if(args[i] == "usePutOptions") {
            if (++i < args.size())
                usePutOptions = (args[i] == "ON");
        }
        else if(args[i] == "conf") {
            if (++i < args.size())
                confFile = args[i];
                loadArgs(&args, i);
        }
        else {
            storgeType = Engine::Session::stringToMessageStorageType( args[i].c_str() );
        }
    }

    // Create an instance of user applciation
    std::auto_ptr<EchoApplication> application( new EchoApplication() );

    // Initialize B2BITS FIX engine
    Engine::FixEngine::init(confFile);

    // Settings
    // For convenience settings are moved to engine.properties file
    Engine::SessionExtraParameters a_params;
    a_params.storageType_ = storgeType;
  //  a_params.sendCpuAffinity_ = 0x1;
  //  a_params.recvCpuAffinity_ = 0x2;

    std::cout << "Using " << Engine::Session::messageStorageTypeToString( storgeType ) << " message storage." << std::endl;
    std::cout << "Create session..." << std::endl;

    // Create session-acceptor
    Engine::Session* acceptor = Engine::FixEngine::singleton()->createSession(
                                     application.get(), SENDER, TARGET, Engine::FIX42, &a_params );

    application->init( acceptor, usePreparedMsg, usePutOptions );

    std::cout << "Waiting connect..." << std::endl;
    // Establish connection
    acceptor->connect();
    application->waitDisconnect();
    std::cout << "Done." << std::endl;

    // Close connection
    //acceptor->disconnectNonGracefully();

    acceptor->release();

    // Destroy FIX engine
    Engine::FixEngine::destroy();
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if( System::HRTimer::frequency() < 1000000 ) {
        std::cerr << "Timer frequency is not sufficient to perform measurement." << std::endl;
        return 1;
    }

    try {
        run( argc, argv );
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: "  << e.what() << std::endl;
        return 1;
    }

    return 0;
}
