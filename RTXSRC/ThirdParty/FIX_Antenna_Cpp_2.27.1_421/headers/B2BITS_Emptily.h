// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Emptily.h
/// Contains Utils::Emptily class definition

#ifndef B2BITS_Utils_Emptily_h
#define B2BITS_Utils_Emptily_h

#include <ostream>
#include <cassert>

#include "B2BITS_V12_Defines.h"
#include "B2BITS_Exception.h"
#include "B2BITS_NullableValue.h"

/// FIX Antenna Utils framework
namespace Utils
{

    // Forward declarations
    template <typename T>
    class NullableValue;

    /// Represents entity with two states - initialized and not initialized.
    template <typename T>
    class Emptily
    {
    private:
        /// Flag. true if initialized; false otherwise.
        bool initialized_;

        /// If initialized, contains value.
        T value_;

    public:
        /// Type of the store value
        typedef T value_type;

        /// Self type
        typedef Emptily<value_type> _Myt;

        /// Conversion constructor.
        /// Converts value_type to self type.
        Emptily( const value_type& value )
            : initialized_( true )
            , value_( value )
        {}

        /// Conversion constructor
        /// Allow user to use "empty" identifier instead of Emptily.
        Emptily( Utils::EmptyValue )
            : initialized_( false )
        {}

        /// Initializes object with value and flag
        Emptily( const value_type& value, bool aIsEmpty )
            : initialized_( !aIsEmpty )
            , value_( value )
        {}

        /// Default constructor
        Emptily(): initialized_( false ) {}

        /// Generic copy constructor
        template< typename Other >
        Emptily( const Emptily<Other>& other )
            : initialized_( other.hasValue() ),
              value_( !initialized_ ? value_type() : value_type( *other ) ) {
        }

        /// Generic conversion constructor
        template< typename Other >
        Emptily( const Other& other )
            : initialized_( true ),
              value_( other ) {
        }

        /// Explicit conversion constructor from NullableValue
        /// User cannot pass NullableValue with undefined state
        explicit Emptily( const Utils::NullableValue<T>& value ) {
            assert( value.isDefined() );
            copy( value );
        }

        /// Returns true if value was NOT assigned
        bool isEmpty() const  {
            return !initialized_;
        }

        /// Returns true if value WAS assigned
        bool hasValue() const {
            return initialized_;
        }

        /// Initializes object with default value
        void init() {
            if( initialized_ ) {
                return;
            }

            initialized_ = true;
            value_ = value_type();
        }

        /// Assign operator
        _Myt& operator=( const _Myt& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        _Myt& operator=( const value_type& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        _Myt& operator=( const Utils::NullableValue<T>& value ) {
            copy( value );
            return *this;
        }

        /// Generic assign operator
        template< typename Other >
        _Myt& operator=( const Other& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator for "empty" identifier
        _Myt& operator=( Utils::EmptyValue ) {
            initialized_ = false;
            return *this;
        }

        /// Generic assign operator for Emptily
        template< typename Other >
        _Myt& operator=( const Emptily<Other>& value ) {
            copy( value );
            return *this;
        }

        /// Compares two Emptilies
        bool equals( const _Myt& rv ) const {
            if( this == &rv ) {
                return true;
            }

            return initialized_ == rv.initialized_ && ( !initialized_ || value_ == rv.value_ );
        }

        /// Returns strored value
        /// If value was not assigned, std::exception will be raised.
        T& operator*() {
            return *get();
        }

        /// Returns strored value
        /// If value was not assigned, std::exception will be raised
        const T& operator*() const {
            return *get();
        }

        /// Selector
        T* operator->() {
            return get();
        }

        /// Const selector
        const T* operator->() const {
            return get();
        }

        /// Returns pointer to strored value
        /// If value was not assigned, std::exception will be raised
        T* get() {
            initialized_ || throwCannotReturnValue();
            return &value_;
        }

        /// Returns pointer to strored value
        /// If value was not assigned, std::exception will be raised
        const T* get() const {
            initialized_ || throwCannotReturnValue();
            return &value_;
        }

        /// Compare operator
        /// Compares two emptilies
        bool operator !=( const _Myt& rv ) const {
            return !equals( rv );
        }

        /// Compare operator
        /// Compares two emptilies
        bool operator ==( const _Myt& rv ) const {
            return equals( rv );
        }

        /// Returns uninitialized value
        //static _Myt const& empty() { return empty_value_; }
    private:
        //const static _Myt empty_value_;

        template< typename Other >
        void copy( const Emptily<Other>& value ) {
            initialized_ = value.hasValue();
            if( initialized_ ) {
                value_ = *value;
            }
        }

        template< typename Other >
        void copy( const Other& value ) {
            value_ = value;
            initialized_ = true;
        }

        void copy( const T& value ) {
            value_ = value;
            initialized_ = true;
        }

        void copy( const Utils::NullableValue<T>& value ) {
            initialized_ = value.hasValue();
            if( initialized_ ) {
                value_ = *value;
            }
        }

        void copy( const _Myt& value ) {
            initialized_ = value.initialized_;
            if ( initialized_ ) {
                value_ = value.value_;
            }
        }

        static bool throwCannotReturnValue();
    };

    template <typename T>
    bool Emptily<T>::throwCannotReturnValue()
    {
        throw Utils::Exception( "Cannot return value. Value is not defined" );
        return false;
    }

    template< typename T >
    inline std::ostream& operator << ( std::ostream& s, const Emptily<T>& rv )
    {
        if( rv.isEmpty() ) {
            return s << "(empty)";
        } else {
            return s << *rv;
        }
    }

} // namepsace Utils

#endif //B2BITS_Utils_Emptily_h

