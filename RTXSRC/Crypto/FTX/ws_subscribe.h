#pragma once

#include <string_view>
#include <optional>
#include <json_dto/pub.hpp>

#include "constants.h"

namespace Crypto::ftx {

struct subscribe_request_t {
    WS::RequestType request_type = WS::RequestType::subscribe;
    WS::ChannelType channel_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
    std::string_view market = INVALID_STRING_VIEW;

    void reset() {
        request_type = WS::RequestType::subscribe;
        channel_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
        market = INVALID_STRING_VIEW;
    }
};

struct subscribe_data_t {
    double bid = 0;
    double ask = 0;
    double ts = 0;
    double last = 0;

    void reset() {
        bid = 0;
        ask = 0;
        ts = 0;
        last = 0;
    }
};

struct subscribe_response_t {
    WS::ResponseType response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
    WS::ChannelType channel_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
    std::string_view market = INVALID_STRING_VIEW;

    void reset() {
        response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
        channel_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
        market = INVALID_STRING_VIEW;
    }
};



struct subscribe_error_t {
    WS::ResponseType response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
    std::string_view code = INVALID_STRING_VIEW;
    std::string_view msg = INVALID_STRING_VIEW;

    void reset() {
        response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
        code = INVALID_STRING_VIEW;
        msg = INVALID_STRING_VIEW;
    }
};


}  // namespace Crypto::ftx

namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::subscribe_request_t &m) {
    io
    & json_dto::mandatory("op", m.request_type)
    & json_dto::mandatory("channel", m.channel_type)
    & json_dto::mandatory("market", m.market);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::subscribe_data_t &m) {
    io
    & json_dto::mandatory("bid", m.bid)
    & json_dto::mandatory("ask", m.ask)
    & json_dto::mandatory("ts", m.ts)
    & json_dto::mandatory("last", m.last);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::subscribe_response_t &m) {
    io
    & json_dto::mandatory("type", m.response_type)
    & json_dto::mandatory("channel", m.channel_type)
    & json_dto::mandatory("market", m.market);
}

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::subscribe_error_t &m) {
    io
    & json_dto::mandatory("type", m.response_type)
    & json_dto::mandatory("code", m.code)
    & json_dto::mandatory("msg", m.msg);
}

}  // namespace json_dto
