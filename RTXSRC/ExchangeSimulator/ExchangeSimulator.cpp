#include <iostream>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included
#include "ExchangeSimulator.h"

using namespace ::std;

namespace {
    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}

ExchangeSimulator::ExchangeSimulator(const ApplicationProperties& params)
    : params_(params)
{
    try {
        spdlog::info("acceptor_cfg: ", params.acceptor_cfg);
        FIX::SessionSettings settings( params.acceptor_cfg );
        FIX::FileStoreFactory storeFactory( settings );
        FIX::FileLogFactory logFactory( settings );
        m_acceptor = make_unique<FIX::SocketAcceptor>( *this, storeFactory, settings, logFactory);
        m_acceptor->start();
        spdlog::info("Listening for incoming connection...");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}",e.what());
        return;
    }

    m_init = true;
}

ExchangeSimulator::~ExchangeSimulator()
{
    m_acceptor->stop();
    spdlog::info("stop simulator sessions");
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void ExchangeSimulator::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void ExchangeSimulator::onLogon( const FIX::SessionID& sessionID )
{
    spdlog::info("Logon - {}", sessionID);
    session_ = sessionID;
}

void ExchangeSimulator::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("Logout - {}", sessionID);
}

void ExchangeSimulator::toAdmin( FIX::Message& message, const FIX::SessionID& )
{
    spdlog::info("Admin OUT: {}", message);
}

void ExchangeSimulator::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("Admin IN: {}", message);
}

void ExchangeSimulator::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("IN: {}", message);
    crack( message, sessionID );
}

void ExchangeSimulator::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("OUT: {}", message);
}

enum Action {
    NEW,
    FILL,
    CANCEL
};

void ExchangeSimulator::onMessage( const FIX44::NewOrderSingle& msg, const FIX::SessionID& sessionID)
{
    static uint32_t ioc{}, day{};
    srand((unsigned) time(0));
    try  {
        auto clOrdId = getFieldValue<FIX::ClOrdID>(msg);
        auto symbol = getFieldValue<FIX::Symbol>(msg);
        auto tif = getFieldValue<FIX::TimeInForce>(msg);
        auto side = getFieldValue<FIX::Side>(msg);
        auto type = getFieldValue<FIX::OrdType>(msg);
        auto qty = getFieldValue<FIX::OrderQty>(msg);
        auto price = getFieldValue<FIX::Price>(msg);
        auto venue = getFieldValue<FIX::ExDestination>(msg);

        FIX44::ExecutionReport erMsg;

        erMsg.set(FIX::ClOrdID(clOrdId));
        erMsg.set(FIX::OrderID(to_string(++m_orderId)));
        erMsg.set(FIX::ExecID(to_string(++m_execId)));
        erMsg.set(FIX::Symbol(symbol));
        erMsg.set(FIX::TimeInForce(tif));
        erMsg.set(FIX::Side(side));
        erMsg.set(FIX::OrdType(type));
        erMsg.set(FIX::Price(price));
        erMsg.set(FIX::OrderQty(qty));
        erMsg.set(FIX::TransactTime());

        auto fillIt = [&](Action action) {
            switch (action) {
            case Action::FILL:
                erMsg.set(FIX::OrdStatus(FIX::OrdStatus_FILLED));
                erMsg.set(FIX::ExecType(FIX::ExecType_TRADE));                
                erMsg.set(FIX::LastQty(qty));
                erMsg.set(FIX::CumQty(qty));
                erMsg.set(FIX::LeavesQty(0));
                erMsg.set(FIX::LastPx(price));
                erMsg.set(FIX::AvgPx(price));
                spdlog::info("fill clOrdId: {}", clOrdId);
                break;
            case Action::NEW:
                erMsg.set(FIX::OrdStatus(FIX::OrdStatus_NEW));
                erMsg.set(FIX::ExecType(FIX::ExecType_NEW));                
                erMsg.set(FIX::LastQty(0));
                erMsg.set(FIX::CumQty(0));
                erMsg.set(FIX::LeavesQty(qty));
                erMsg.set(FIX::LastPx(0));
                erMsg.set(FIX::AvgPx(0));
                m_orders.emplace(to_string(m_orderId), OSMOrder{side, qty, tif, type, price, symbol, clOrdId});
                // need store order for later cancel
                spdlog::info("new clOrdId: {}", clOrdId);
                break;
            case Action::CANCEL:
                erMsg.set(FIX::OrdStatus(FIX::OrdStatus_CANCELED));
                erMsg.set(FIX::ExecType(FIX::ExecType_CANCELLED));                
                erMsg.set(FIX::LastQty(0));
                erMsg.set(FIX::CumQty(0));
                erMsg.set(FIX::LeavesQty(qty));
                erMsg.set(FIX::LastPx(0));
                erMsg.set(FIX::AvgPx(0));
                spdlog::info("cancelled clOrdId: {}", clOrdId);
                break;
            default:
                break;
            }
        };

        if (tif == FIX::TimeInForce_DAY) {
            ++day;
            fillIt(Action::NEW);
            FIX::Session::sendToTarget(erMsg, sessionID);
            //if (day%11 == 0) {
            if (rand() % 100 < params_.dayOrderFillRate) {
                fillIt(Action::FILL);
                FIX::Session::sendToTarget(erMsg, sessionID);
            }
        }
        else {
            ++ioc;
            int ratio = params_.litVenueList.count(venue)?
                params_.litIOCOrderFillRate : params_.iOCOrderFillRate;
            fillIt(ioc%100 < ratio? Action::FILL : Action::CANCEL);
            FIX::Session::sendToTarget(erMsg, sessionID);
        }
        
        //FIX::Session::sendToTarget(erMsg, sessionID);
    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught some EX");
    }
}
void ExchangeSimulator::onMessage( const FIX44::OrderCancelRequest& msg, const FIX::SessionID& sessionID)
{
        auto orderId = getFieldValue<FIX::OrderID>(msg);

        if (auto itor = m_orders.find(orderId); itor != m_orders.end()) {
            FIX44::ExecutionReport erMsg;
            const auto& o = itor->second;
            auto clOrdId = getFieldValue<FIX::ClOrdID>(msg);
            auto oclOrdId = getFieldValue<FIX::OrigClOrdID>(msg);
            erMsg.set(FIX::OrderID(orderId));
            erMsg.set(FIX::ExecID(to_string(++m_execId)));
            erMsg.set(FIX::Symbol(o.symbol));
            erMsg.set(FIX::TimeInForce(o.tif));
            erMsg.set(FIX::Side(o.side));
            erMsg.set(FIX::OrdType(o.type));
            erMsg.set(FIX::Price(o.price));
            erMsg.set(FIX::OrderQty(o.qty));
            erMsg.set(FIX::TransactTime());
            //erMsg.set(FIX::ClOrdID(clOrdId));
            //erMsg.set(FIX::OrigClOrdID(oclOrdId));
            erMsg.set(FIX::ClOrdID(oclOrdId));
            erMsg.set(FIX::OrdStatus(FIX::OrdStatus_CANCELED));
            erMsg.set(FIX::ExecType(FIX::ExecType_CANCELLED));                
            erMsg.set(FIX::LastQty(0));
            erMsg.set(FIX::CumQty(0));
            erMsg.set(FIX::LeavesQty(0));
            erMsg.set(FIX::LastPx(0));
            erMsg.set(FIX::AvgPx(0));
            spdlog::info("cancelling orderId: {}", orderId);
            FIX::Session::sendToTarget(erMsg, sessionID);
        }       
}

void ExchangeSimulator::onMessage( const FIX44::OrderCancelReplaceRequest& msg, const FIX::SessionID& ) {
    auto clOrdId = getFieldValue<FIX::ClOrdID>(msg);
    spdlog::info("ignore cancel/replace request on clOrdId: {}", clOrdId);
}