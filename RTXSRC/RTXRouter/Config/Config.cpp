//
// Created by schen on 10/22/2020.
//
#include <string>
#include <unordered_map>
#include <vector>
#include <unordered_set>
#include <memory>
#include <iostream>
#include <functional>
#include <ctime>
#include <unordered_set>

#include "LoaderBase.h"
#include "ConfigGlobalLoader.h"

#include "ConfigGlobalBox.h"
#include "ConfigRouterBox.h"
#include "ConfigSecIdfnsBox.h"
#include "ConfigUserBox.h"

#include "Config.h"

Config::Config()
{

}
Config::~Config()
{
    delete m_globalConfig;
    delete m_routerConfig;
    delete m_secIdfnsConfig;
    delete m_userUserConfig;
}

void Config::InitConfigBox()
{
    m_globalConfig = new ConfigGlobalBox();
    m_globalConfig->InitConfigGlobalBox();

    m_routerConfig = new ConfigRouterBox();
    m_routerConfig->InitConfigRouterBox();

    m_secIdfnsConfig = new ConfigSecIdfnsBox();
    m_secIdfnsConfig->InitConfigSecIdfnsBox();

    m_userUserConfig = new class ConfigUserBox();
    m_userUserConfig->InitConfigUserBox();
}





