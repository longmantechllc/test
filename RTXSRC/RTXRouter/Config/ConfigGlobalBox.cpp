//
// Created by schen on 10/22/2020.
#include <string>
#include <iostream>
#include <vector>
#include <memory>
#include <functional>
#include <ctime>
#include <unordered_map>
#include <climits>
#include <stdio.h>

#include "RTXUtils.h"
#include "ConfigGlobalBox.h"

ConfigGlobalBox::ConfigGlobalBox()
{

}
ConfigGlobalBox::~ConfigGlobalBox()
{
    DESTROY(tradingHalts);
    DESTROY(securityMaster);

    argos.clear();
    quoteFilters.clear();
    marketDataFailureRoutes.clear();
    customTifs.clear();
    secIdfnIdentifiers.clear();

    orderTypeActionList.clear();
    orderSymbolActionList.clear();
    orderTifActionList.clear();
    userActionList.clear();
    accountActionList.clear();

//    for (auto tp : orderParamActions)
//        tp.second.clear();
//    orderParamActions.clear();
}

void ConfigGlobalBox::InitConfigGlobalBox()
{
    //single layer parameters
    auto  fptr = std::make_pair( "RouterName", [&](char* arg){ SetRouteName(arg); } );
    FuncList.insert(fptr);
    auto  fptr0 = std::make_pair( "TimeZone", [&](char* arg){ SetTimeZone(arg); } );
    FuncList.insert(fptr0);
    auto  fptr1 = std::make_pair( "DefaultSecIdfn", [&](char* arg){ SetDefaultSecIdfn(arg); } );
    FuncList.insert(fptr1);
    auto  fptr2 = std::make_pair( "RetryOnCxlRej", [&](char* arg){ SetRetryOnCxlRej(arg); } );
    FuncList.insert(fptr2);
//    auto  fptr3 = std::make_pair( "marketDataFailureTimeOut", [&](char* arg){ SetMarketDataFailureTimeOut(arg); } );
//    FuncList.insert(fptr3);
    auto  fptr4 = std::make_pair( "shortSellTargetingReTries", [&](char* arg){ SetShortSellTargetingReTries(arg); } );
    FuncList.insert(fptr4);
    auto  fptr5 = std::make_pair( "checkChildOrdLimitPriceViolation", [&](char* arg){ SetCheckChildOrdLimitPriceViolation(arg); } );
    FuncList.insert(fptr5);
    auto  fptr6 = std::make_pair( "ThreadNumber", [&](char* arg){ SetThreadNumber(arg); } );
    FuncList.insert(fptr6);
    auto  fptr7 = std::make_pair( "MarketOpen", [&](char* arg){ SetMarketOpen(arg); } );
    FuncList.insert(fptr7);
    auto  fptr8 = std::make_pair( "MarketClose", [&](char* arg){ SetMarketClose(arg); } );
    FuncList.insert(fptr8);
    auto  fptr9 = std::make_pair( "AssetClass", [&](char* arg){ SetAssetClass(arg); } );
    FuncList.insert(fptr9);
    auto  fptr10 = std::make_pair( "marketDataFailureTimeOutSwp", [&](char* arg){ SetMarketDataFailureTimeOutSwp(arg); } );
    FuncList.insert(fptr10);
    auto  fptr11 = std::make_pair( "marketDataFailureTimeOutTarg", [&](char* arg){ SetMarketDataFailureTimeOutTarg(arg); } );
    FuncList.insert(fptr11);
    auto  fptr12 = std::make_pair( "PostMarketClose", [&](char* arg){ SetPostMarketClose(arg); } );
    FuncList.insert(fptr12);
    auto  fptr13 = std::make_pair( "ExtraLogging", [&](char* arg){ SetExtraLogging(arg); } );
    FuncList.insert(fptr13);
    auto  fptr14 = std::make_pair( "RetailRoutingStartTime", [&](char* arg){ SetRetailRoutingStartTime(arg); } );
    FuncList.insert(fptr14);
    auto  fptr15 = std::make_pair( "RetailRoutingEndTime", [&](char* arg){ SetRetailRoutingEndTime(arg); } );
    FuncList.insert(fptr15);
    auto  fptr16 = std::make_pair( "UnsubscribeMDWhenPost", [&](char* arg){ SetUnsubscribeMDWhenPost(arg); } );
    FuncList.insert(fptr16);
    auto  fptr17 = std::make_pair( "SWPStartTime", [&](char* arg){ SetSWPStartTime(arg); } );
    FuncList.insert(fptr17);
    auto  fptr18 = std::make_pair( "SWPEndTime", [&](char* arg){ SetSWPEndTime(arg); } );
    FuncList.insert(fptr18);
    auto  fptr19 = std::make_pair( "RetailSWPStartTime", [&](char* arg){ SetRetailSWPStartTime(arg); } );
    FuncList.insert(fptr19);
    auto  fptr20 = std::make_pair( "RetailSWPEndTime", [&](char* arg){ SetRetailSWPEndTime(arg); } );
    FuncList.insert(fptr20);

    tradingHalts = new struct TradingHalts();
    securityMaster = new struct SecurityMaster();
    InitArgo();
}

void ConfigGlobalBox::SetRetryOnCxlRej(char* val)
{
    retryOnCxlRej = RTXUtils::StrToInt(val, 3);
}
//void ConfigGlobalBox::SetMarketDataFailureTimeOut(char* val)
//{
//    marketDataFailureTimeOut = RTXUtils::StrToInt(val, 1000);
//}
void ConfigGlobalBox::SetMarketDataFailureTimeOutSwp(char* val)
{
    marketDataFailureTimeOutSwp = RTXUtils::StrToInt(val, 0);
}
void ConfigGlobalBox::SetMarketDataFailureTimeOutTarg(char* val)
{
    marketDataFailureTimeOutTarg = RTXUtils::StrToInt(val, 0);
}
void ConfigGlobalBox::SetMarketOpen(char* val)
{
    m_marketStartTimeStr = val;
    m_marketStartTimeMssm = RTXUtils::StrToTimeSsm(val, 34200) * 1000;
}
void ConfigGlobalBox::SetMarketClose(char* val)
{
    m_marketEndTimeStr = val;
    m_marketEndTimeMssm = RTXUtils::StrToTimeSsm(val, 57600) * 1000;
}
void ConfigGlobalBox::SetPostMarketClose(char* val)
{
    m_postMarketCloseStr = val;
    m_postMarketCloseTimeMssm = RTXUtils::StrToTimeSsm(val, 66600) * 1000;
}
void ConfigGlobalBox::SetRetailRoutingStartTime(char* val)
{
    m_retailRoutingStartTimeStr = val;
    m_retailRoutingStartTimeMssm = RTXUtils::StrToTimeSsm(val, 34200) * 1000;
}
void ConfigGlobalBox::SetRetailRoutingEndTime(char* val)
{
    m_retailRoutingEndTimeStr = val;
    m_retailRoutingEndTimeMssm = RTXUtils::StrToTimeSsm(val, 57600) * 1000;
}

void ConfigGlobalBox::SetRetailSWPStartTime(char* val)
{
    m_retailSwpStartTimeStr = val;
    m_retailSwpStartTimeMssm = RTXUtils::StrToTimeSsm(val, 34200) * 1000;
}

void ConfigGlobalBox::SetRetailSWPEndTime(char* val)
{
    m_retailSwpEndTimeStr = val;
    m_retailSwpEndTimeMssm = RTXUtils::StrToTimeSsm(val, 57600) * 1000;
}

void ConfigGlobalBox::SetSWPStartTime(char* val)
{
    m_swpStartTimeStr = val;
    m_swpStartTimeMssm = RTXUtils::StrToTimeSsm(val, 34200) * 1000;
}

void ConfigGlobalBox::SetSWPEndTime(char* val)
{
    m_swpEndTimeStr = val;
    m_swpEndTimeMssm = RTXUtils::StrToTimeSsm(val, 57600) * 1000;
}

void ConfigGlobalBox::SetThreadNumber(char* val)
{
    threadNumber = RTXUtils::StrToInt(val, 5);
}

void ConfigGlobalBox::SetShortSellTargetingReTries(char* val)
{
    shortSellTargetingReTries = RTXUtils::StrToInt(val, 5);
}

void ConfigGlobalBox::SetCheckChildOrdLimitPriceViolation(char* val)
{
    checkChildOrdLimitPriceViolation = RTXUtils::StrToBool(val, false);
}

void ConfigGlobalBox::InitArgo()
{
    auto arg00 = std::make_shared<ArgoStruct>("RetailSWP", false);
    argos.emplace_back(arg00);
    auto arg0 = std::make_shared<ArgoStruct>("RetailRouting", false);
    argos.emplace_back(arg0);
    auto arg = std::make_shared<ArgoStruct>("SWP", false);
    argos.emplace_back(arg);
    auto arg1 = std::make_shared<ArgoStruct>("Targeting", false);
    argos.emplace_back(arg1);
    auto arg2 = std::make_shared<ArgoStruct>("Posting", false);
    argos.emplace_back(arg2);
}

void ConfigGlobalBox::AddArgo(const std::string& val, std::string& strVal, bool deflt)
{
    for(auto i=0; i < argos.size(); ++i)
    {
        if(argos[i]->name == val) {
            argos[i]->enabled = RTXUtils::StrToBool(strVal, deflt);
            break;
        }
    }
}
void ConfigGlobalBox::SetArgo()
{
    //must keep the order: RetailSwp->Retail->DarkSweep -> Targeting -> PassivePosting
    //but can miss some item
    bool rswp = false;
    bool rtl = false;
    bool dswp = false;
    bool tgt = false;
    bool pst = false;

    for(auto arg : argosMp)
    {
        if(arg.first == "RetailSWP" && arg.second == true)
            rswp = true;
        if(arg.first == "RetailRouting" && arg.second == true)
            rtl = true;
        if(arg.first == "SWP" && arg.second == true)
            dswp = true;
        if(arg.first == "Targeting" && arg.second == true)
            tgt = true;
        if(arg.first == "Posting" && arg.second == true)
            pst = true;
    }
}

void ConfigGlobalBox::AddQuoteFilter(const std::string& name, const std::shared_ptr<QuoteFilter>& val)
{
    if(quoteFilters.find(name) == quoteFilters.end())
    {
        auto arg = std::make_pair(name, val);
        quoteFilters.insert(arg);
    }
}

void ConfigGlobalBox::AddMarketDataFailureRoute(const std::string& val)
{
    marketDataFailureRoutes.push_back(val);
}

void ConfigGlobalBox::AddCustomTif(std::string value, const std::shared_ptr<struct COD>& val)
{
    int name = RTXUtils::StrToInt(value);
    if(customTifs.find(name) == customTifs.end())
    {
        auto arg = std::make_pair(name, val);
        customTifs.insert(arg);
    }
}

void ConfigGlobalBox::AddSecIdfnIdentifier(const std::string& value, const std::string& SecIdfn)
{
    if(secIdfnIdentifiers.find(value) == secIdfnIdentifiers.end())
    {
        auto pr = std::make_pair(value, SecIdfn);
        secIdfnIdentifiers.insert(pr);
    }
}

void ConfigGlobalBox::AddSyntheticSecIdfnIdentifier(const std::string& name, std::string value, const std::string& SecIdfn)
{
    if(name == "MinQty")
        minQtySecIdfnIdentifier = SecIdfn;      //the value always = "Y"
    else {
        std::vector<std::string> rts = RTXUtils::stringToVector(value, ",");
        for (auto rt : rts) {
            auto pr = std::make_pair(rt, SecIdfn);
            if (name == "User")
                userSecIdfnIdentifier.insert(pr);
            else if (name == "Account")
                accountSecIdfnIdentifier.insert(pr);
            else if (name == "Symbol")
                symbolSecIdfnIdentifier.insert(pr);
        }
    }
}

std::string& ConfigGlobalBox::UserSecIdfnIdentifier(const std::string& val)
{
    auto itr = userSecIdfnIdentifier.find(val);
    if(itr != userSecIdfnIdentifier.end())
        return itr->second;
    return mEmptyString;
}

std::string& ConfigGlobalBox::AccountSecIdfnIdentifier(const std::string& val)
{
    auto itr = accountSecIdfnIdentifier.find(val);
    if(itr != accountSecIdfnIdentifier.end())
        return itr->second;
    return mEmptyString;
}

std::string& ConfigGlobalBox::SymbolSecIdfnIdentifier(const std::string& val)
{
    auto itr = symbolSecIdfnIdentifier.find(val);
    if(itr != symbolSecIdfnIdentifier.end())
        return itr->second;
    return mEmptyString;
}

void ConfigGlobalBox::SetAssetClass(char* val)
{
    m_ssetClass = val;
}

void ConfigGlobalBox::AddOrderParam(const std::string& type, std::string value, const std::string& action, std::string destStr)
{
    //int act  = RTXUtils::StrToInt(action);                            //reject, reroute ...
    auto dest = RTXUtils::stringToVector( destStr, "," );
    auto ptr = std::make_shared<OrderParamAction>(action, dest);        //reroute - dest

    if(type == "orderType") {
        int tp = RTXUtils::StrToInt(value);
        auto pr1 = std::make_pair(tp, ptr);
        orderTypeActionList.insert(pr1);
    }
    else if(type == "symbol") {
        auto pr = std::make_pair(value, ptr);
        orderSymbolActionList.insert(pr);
    }
    else if(type == "orderTIF") {
        int tp = RTXUtils::StrToInt(value);
        auto pr2 = std::make_pair(tp, ptr);
        orderTifActionList.insert(pr2);
    }
    else if(type == "user"){
        auto pr = std::make_pair(value, ptr);
        userActionList.insert(pr);
    }
    else if(type == "Account"){
        auto pr = std::make_pair(value, ptr);
        accountActionList.insert(pr);
    }

//    auto mp = orderParamActions.find(type);
//    if(mp != orderParamActions.end())
//        mp->second.insert(pr);
//    else
//    {
//        std::unordered_map < std::string  /*"5" short*/, std::shared_ptr< OrderParamAction > > mp2;
//        mp2.insert(pr);
//        auto pr2 = std::make_pair(type, mp2);
//        orderParamActions.insert(pr2);
//    }
}

void ConfigGlobalBox::SetExtraLogging(char* val)
{
    extraLogging = RTXUtils::StrToBool(val, false);
}

void ConfigGlobalBox::SetUnsubscribeMDWhenPost(char* val)
{
    m_unsubscribeMDWhenPost = RTXUtils::StrToBool(val, true);
}