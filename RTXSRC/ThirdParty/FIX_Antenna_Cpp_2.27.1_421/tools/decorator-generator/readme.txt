Decorator generator tool readme

Decorators are convenient wrappers around raw FixMessages. By using Decorators developers gain strongly typed access to all available properties of a message, that increases development speed (you write less code to read/write to message properties) and reduces the number of validation errors (because each decorated message class contains only properties that are relevant to this class).
We should also underline, that Decorators do not add any performance overhead and their performance is comparable with that of Engine::FixMessage class.

Command line options of generator tool:
-help,-h                print help message and exit
-protocol-files,-f      file(s) with protocol description
-scp,-s                 SCP protocol name
-app,-a                 APP protocol name
-output-directory,-o    output directory (default_value is "../" )

Tool generates output .h files in <output_directory>/headers/FixMessages/<fix protocol> folder and .cpp files in <output_directory>/src folder.
These generated files could be added to existing project, or compiled separately as static or dynamic library.


Usage:

Windows platform
b2b_fix_msgs_generator2.exe -s SCP_VER -a APP_VER -f FILES

SCP_VER:    Name of session level protocol. Should be same to fixdic id
APP_VER:    Name of application protocol. Should be same to fixdic id
FILES:      List of XML dictionaries to load

Example
b2b_fix_msgs_generator2.exe -s FIX44 -a FIX44 -f ../../../data/fixdic44.xml
Generates a FIX44 decorator source files

cd ../../../data
b2b_fix_msgs_generator2.exe -s FIXT11 -a FIX42 -f fixdic42.xml fixdict11.xml
Generates a decorator source files for FIX42 over FIXT11 protocol


Linux platform
b2b_fix_msgs_generator2 -s SCP_VER -a APP_VER -f DICTIONARY [-f DICTIONARY ...]

SCP_VER:      Name of session-level protocol. Should be same to fixdic id
APP_VER:      Name of application-level protocol. Should be same to fixdic id
DICTIONARY:   Path to the FIX dictionary XML file

Example
b2b_fix_msgs_generator2 -s FIX40 -a FIX40 -f ../../data/fixdic40.xml
Generates a FIX40 decorator using dictionary description from ../../data/fixdic40.xml

b2b_fix_msgs_generator2 -s FIXT11 -a FIX44 -f ../../data/fixdict11.xml -f ../../data/fixdic44.xml
Generates a FIX44 ower FIXT11 decorator
