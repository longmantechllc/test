#pragma once

#include <stdint.h>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <cassert>
#include <memory>
#include <thread>
#include <atomic>

#include "quickfix/FileStore.h"
#include "quickfix/FileLog.h"
#include "quickfix/SocketInitiator.h"
#include "quickfix/Log.h"
#include "quickfix/SessionSettings.h"
#include "quickfix/Session.h"
#include "quickfix/Application.h"
#include "quickfix/MessageCracker.h"
#include "quickfix/fix44/NewOrderSingle.h"
#include "quickfix/fix44/ExecutionReport.h"

#include "ApplicationProperties.h"

/// Application implementation (processes the incoming messages).
class OrderSimulator : public FIX::Application, public FIX::MessageCracker
{
public:
    explicit OrderSimulator(const ApplicationProperties& params);
    ~OrderSimulator();

    // Application overloads
    void onCreate( const FIX::SessionID& ) override;
    void onLogon( const FIX::SessionID& sessionID ) override;
    void onLogout( const FIX::SessionID& sessionID ) override;
    void onMessage( const FIX44::ExecutionReport& msg, const FIX::SessionID& ) override;
    void toAdmin( FIX::Message&, const FIX::SessionID& ) override;
    void toApp( FIX::Message&, const FIX::SessionID& )
        EXCEPT ( DoNotSend )  override;
    void fromAdmin( const FIX::Message&, const FIX::SessionID& )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon ) override;
    void fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType ) override;

    bool isInited() {return m_init;}
    //auto& getIoiCache() { return m_ioiCache;}
    auto& getMutex() { return m_mutex;}

private: // Engine::Application contract implementation
    template<typename T>
    static auto getField(const auto& msg) {
        T field;
        msg.get(field);
        return field;
    };

    template<typename T>
    static auto getFieldValue(const auto& msg) {
        T field;
        msg.get(field);
        return field.getValue();
    };
    template<typename T>
    static auto getFieldValueIf(const auto& msg, auto& var) {
        T field;
        if (msg.isSetField(field)) {
            msg.get(field);
            var = field.getValue();
        }
    };
    void loadDataFile();
    void randomOrder(int i);

    const ApplicationProperties& params_;

    std::unique_ptr<FIX::SocketInitiator> m_initiator;
    FIX::SessionID session_;
    //uint64_t m_seqNum {};
    bool m_init{};
    std::atomic<bool> m_isLogon{};
    std::mutex m_mutex;
    //std::vector<std::pair<std::string, double>> m_symbols;
    //std::vector<FIX::SessionID> m_sessions;
    std::vector<std::string> m_symbols;
    //symbol,side -> exchange -> data
    //std::unordered_map<std::string, std::unordered_map<std::string, IOIData>> m_ioiCache;
    //std::unordered_map<std::string, std::tuple<std::string, double, double>> m_session2Exch;
    // struct pair_hash
    // {
    //     template <class T1, class T2>
    //     std::size_t operator() (const std::pair<T1, T2> &pair) const {
    //         return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
    //     }
    // };
    //std::vector<std::unordered_map<std::pair<std::string, bool>, uint64_t, pair_hash>> m_orderIds;
    std::thread m_randomizerThread;
    std::atomic<bool> m_randomizerStop{false};
};


