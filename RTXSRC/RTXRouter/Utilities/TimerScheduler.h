//
// Created by schen on 2/8/2021.
//
#ifndef RTXROUTER_TIMERSCHEDULER_H
#define RTXROUTER_TIMERSCHEDULER_H

#include <functional>
#include <chrono>

#include "xTimer.h"

using namespace std;

class Logger;
class xTimer;

typedef std::chrono::system_clock::time_point time_point;
typedef std::function<void(void)> task_;

class TimerScheduler
{
public:
    TimerScheduler() { }
    TimerScheduler(int numThreads, Logger* log);
    ~TimerScheduler();

    void insert(std::shared_ptr <xTimer> timer);
    bool isTimerActive(time_point tp);
    bool remove(time_point tp);
    void close();

    std::mutex lock_;

private:
    void StartTimer(int numThreads);
    void run();

private:
    std::map<time_point, std::shared_ptr<xTimer>> mTimerQ;
    std::condition_variable signal_;

    bool running_;
    bool trigger_ready_;
    std::vector<std::thread> mThreads;
    __int64_t mExcCnt = 0;
    int sleepTime = 10;
    Logger* m_loggerSys;        //log to system.log file
};

#endif //RTXROUTER_TIMERSCHEDULER_H
