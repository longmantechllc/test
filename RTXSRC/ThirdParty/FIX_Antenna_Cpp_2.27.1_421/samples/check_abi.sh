CXX11_ABI=`echo '#include <string>' | g++ -x c++ -E -dM - | fgrep _GLIBCXX_USE_CXX11_ABI | cut -d ' ' -f3`

if [ "x${CXX11_ABI}" == "x1" ]; then
    CXX11_ABI=1
else
    CXX11_ABI=0
fi

echo ${CXX11_ABI}

