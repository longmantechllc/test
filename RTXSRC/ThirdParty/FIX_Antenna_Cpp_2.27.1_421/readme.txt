(c) B2BITS EPAM Systems Company 2000-2018.

FIX Antenna C++ is an easy way to create a robust, reliable and rapid FIX application. It is a high performance library enabled to:
�	Establish connection with remote FIX server
�	Send/receive FIX messages
�	Manage incoming client connections
�	Connect to the Market Data feed (i.e CME Globex)

SUPPORTED PLATFORMS REQUIREMENTS
�	Windows 2000/XP/Vista/7 x32/x64 bit: Microsoft Visual Studio 2010/2012/2013/2015
�	Linux x64 bit: gcc 4.4/4.8/4.9/5.2/6.1 

USEFUL LINKS
�	summary product description:			http://www.b2bits.com/trading_solutions/fix_engines/fix_engine_cpp.html
�	set of FIX dictionaries:			http://www.btobits.com/fixopaedia/index.html
�	detailed information for each of FIX products:	https://kb.b2bits.com/display/B2BITS/EPAM-B2BITS+products+for+Capital+Markets
�	B2BITS FIX Antenna C++ programmer's guide:	http://corp-web.b2bits.com/fixacpp/doc/html/index.html
	
	If you have questions about package usage send email for support team to SupportFIXAntenna@epam.com

FEATURES
�	High performance / low latency
�	100% FIX standard compliance: FIX 4.0, 4.1, 4.2, 4.3, 4.4, 5.0, 5.0 SP1, 5.0 SP2, FAST 1.1
�	Receive market data from CME Globex Marked Data feed
�	Rich message composition API 
�	Customizable FIX protocol 
�	Guaranteed delivery 
�	Customizable FIX session level 
�	Rejecting and Later Delivery modes 
�	High availability 
�	Back-up connections 
�	Automatic message routing 
�	Optimized for Market Data 
�	Encryption, SSL 
�	Remote administration interface 	
	
GETTING STARTED 
Add path to ./headers folder as additional include directory in your project.
Link you project with V12* library from ./lib folder (e.g. V12-vc10-MD-x32.lib)

Look ./samples for more information.

TroubleShooting: 
Samples configuration expects the license file (i.e. engine.license) in the FIX Antenna package root dicrectory as <FIX Antenna package>/engine.license.
Samples for Windows may not work without Microsoft Visual C++ 2010 Redistributable installed. You can find it here: ./redist


PACKAGE CONTENT
FIX Antenna C++ installation package has the following structure

	./data
	Contains common Fix dictionaries, templates that are used by FIX Antenna.
	
	./doc
	B2BITS FIX Antenna C++ API Programmer Guide.
	
	./headers
	FIX Antenna C++ headers.
	
	./lib
	FIX Antenna C++ libraries.
	
	./samples
	Sources and binaries shows how to use B2BITS FIX Antenna C++ library.
	Windows: compiled with VC10 for 32-Bit platform.
	Linux: compiled with gcc 4.4 for 64-Bit platform.
	./samples/PortsUsed.txt - ports are used in samples.
	
	Benchmark projects:
	./samples/Benchmark/Latency/
	./samples/Benchmark/Parser/
	./samples/Benchmark/Throughput/
	Run Receiver and Sender to measure Throughput of B2BITS FIX Antenna. Run this samples on different PCs for get more accurate results.
	
	FAST Projects:
	./samples/FastCodec/BovespaDecoder
	./samples/FastCodec/CmeQuickStart
	./samples/FastCodec/CqgQuickStart
	./samples/FastCodec/FastCodecBenchmark
	./samples/FastCodec/FastCodecQuickStart
	./samples/FastCodec/FastDecoder
	./samples/FastCodec/RtsQuickStart
	./samples/FastCodec/SWXFastDecoder
	./samples/CQGMarketDataClient
	./samples/fast_decoder
	./samples/Fast_QuickStart
	./samples/FixToFastAdapter
	./samples/FortsClient
	./samples/SpectraClient	
	
	ITCH
	./samples/BatsClient
	
	SBE Projects:
	./samples/CmeMdp/client
	
	FIX Sessions Projects:
	./samples/ConnectToGateway
	./samples/ConnectToGatewayEx
	./samples/CustomAdminApp
	./samples/EncryptionQuickStart
	./samples/FIX_QuickStart
	./samples/TransientSession
	./samples/TrEchoServer
	./samples/EchoServer
	./samples/FIX50_QuickStart	
	./samples/ProtocolCustomization_QuickStart
	./samples/SessionQualifier
	
	other:
	./samples/FixLogger
	./samples/iLink
	./samples/MsgStorage

