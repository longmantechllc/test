#!/bin/sh

ulimit -c unlimited
ulimit -s 1024

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

mkdir -p logs/backup

./Fast_QuickStartD $*
