// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_ValBlock.h
/// Contains FixDictionary2::ValBlock class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_ValBlock_H
#define H_B2BITS_FixDictionary2_ValBlock_H

#include <string>
#include <map>

#include <B2BITS_CompilerDefines.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_DictionaryDefines.h>

#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class ValBlock
    *   @brief Represents ValBlock interface.
    */
    class V12_API ValBlock B2B_SEALED
        : public Item
    {
    public:
        /** ValBlock value */
        struct ValueDescription {
            /** Description */
            std::string description_;
            /** Unique identifier */
            std::string id_;

            /** Default constructor */
            ValueDescription() {}

            /** Constructs instance with specific values. */
            ValueDescription( std::string const& description, std::string const& id )
                : description_( description )
                , id_( id ) {
            }
		};

        /**
         * Values container. Key is name of the value. Value is description of the value.
         */
        typedef std::map<std::string, ValueDescription> Values;
    public:
        /** Constructor. */
        ValBlock( std::string const& id );

        /** Copy Constructor. */
        ValBlock( ValBlock const& src );

    public:
        /** Returns ID of the value block. */
        std::string const& id() const throw();

        /** Returns ID of the value block. */
        std::string const& getId() const throw() {
            return id();
        }

        /** Returns name of the value block. Optional. */
        std::string const& name() const throw();

        /** Returns name of the value block. Optional. */
        std::string const& getName() const throw() {
            return name();
        }

        /** Assigns value block name . */
        void setName( std::string const& name );

        /** Returns true if value block is empty; false otherwise.*/
        bool isEmpty() const throw();

        bool isMulti() const throw();
        
        void setIsMulti( bool value ) throw();

        /** Returns values */
        Values const& getValues() const throw();

        /** Adds new value of the value block */
        void addValue( std::string const& value, std::string const& description, std::string const& id );
		void addValue( std::string const& value, std::string const& description, std::string const& id,  bool& duplicate );
		void addValue( std::string const& value, bool& duplicate );

        /** Sets minimal value range */
        void setMinRangeValue( std::string const& strMinRangeVal );

        /** Sets maxumum value range */
        void setMaxRangeValue( std::string const& strMaxRangeVal );

        /** Sets range value type (int, float, etc.)*/
        void setRangeType( std::string const& strRangeType );

        /** Returns minimal value range */
        std::string const& getMinRangeValue() const throw();

        /** Returns maximum value range */
        std::string const& getMaxRangeValue() const throw();

        /** Returns range values type (int, float, etc.)*/
        std::string const& getRangeType() const throw();

        /** Returns description*/
        std::string const& getRangeDescription() const throw();

        /** Assigns description to range */
        void setRangeDescription(std::string const& value);

        /** Returns true if range is defined; false otherwise. */
        bool isRangePresent() const throw();

        /** Returns description of the valblock */
        std::string const& getDescription() const throw();

        /** Assigns description of the valblock */
        void setDescription(std::string const& value);

        /** Returns description of the valblock */
        std::string const& description() const throw() {
            return getDescription();
        }
    public: // Item contract
        /** Make copy of object */
        virtual ValBlockT::Ptr clone() const B2B_OVERRIDE;

        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

    protected:
        /** Destructor */
        ~ValBlock() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;

        ValBlock& operator= (ValBlock const&) B2B_DELETE_FUNCTION;
    };

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_ValBlock_H
