//
// Created by schen on 10/26/2020.
//
#include <iostream>
#include <unordered_map>
#include <functional>
#include <vector>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include "auto_xerces_ptr.h"

#include "Logger.h"
#include "LoaderBase.h"
#include "ConfigGlobalLoader.h"
#include "ConfigRouterLoader.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigUserLoader.h"
#include "ConfigLoader.h"



LoaderBase::LoaderBase(Logger* logger)
        :m_loggerSys(logger)
{

}

std::string LoaderBase::GetElement(xercesc_3_2::DOMNode* node, const char* name)
{
    try
    {
        xercesc_3_2::DOMNamedNodeMap* elements = node->getAttributes() ;
        if(!elements)
            return "";

        XMLCh* xmlName = xercesc_3_2::XMLString::transcode(name) ;
        xercesc_3_2::DOMNode* elemNode = elements->getNamedItem(xmlName) ;
        if(!elemNode)
        {
            xercesc_3_2::XMLString::release(&xmlName);
            return "";
        }

        char* xmlVal ;
        xmlVal = xercesc_3_2::XMLString::transcode(elemNode->getNodeValue()) ;

        xercesc_3_2::XMLString::release(&xmlName) ;
        return xmlVal ;
    }
    catch(...)
    {
        return "" ;
    }
}

