/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#ifdef _LINUX
#include <sys/time.h>
#endif

#include <stdio.h>
#include <B2BITS_PubEngineDefines.h>

#include "Auxiliary.h"
#include "Exception.h"

using namespace Engine;

namespace Benchmark
{
    static const std::string FIXVERSIONS[] = { "4.0", "4.1", "4.2", "4.3", "4.4", "5.0", "NA" };

    /// Returns the current time in milliseconds.
    unsigned long getCurrentTime()
    {
#ifndef _WIN32
        struct timeval tValue;
        if( 0 != gettimeofday( &tValue, NULL ) ) {
            throw Exception( "gettimeofday failed" );
        }
        long sec, uSec, rv;
        memset( &sec,  0, sizeof sec );
        memset( &uSec, 0, sizeof uSec );
        memset( &rv,   0, sizeof rv );
        sec   = tValue.tv_sec;
        uSec  = tValue.tv_usec;
        sec  *= 1000UL;
        uSec /= 1000UL;
        rv = sec +  uSec;
        return rv;
#else // _WIN32
        FILETIME time;
        GetSystemTimeAsFileTime( &time );
        ULARGE_INTEGER v;
        v.LowPart = time.dwLowDateTime;
        v.HighPart = time.dwHighDateTime;
        return ( static_cast<unsigned long>( v.QuadPart / 10000 ) );
#endif
    }

    /**
     * Converts integer value to string representattion
     * @param aV integer value for convertion
     * @return string equivalent of integer value
     */
    std::string i2str( int aV )
    {
        char buf[128];
#if 1400 != _MSC_VER
        sprintf( buf, "%d", aV );
#else
        sprintf_s( buf, 127, "%d", aV );
#endif
        return buf;
    }

}

using namespace Benchmark;
using namespace std;

System::Mutex Auxiliary::m_lock;

/**
 * Converts the given string to the corresponding FIX version.
 */
Engine::FIXVersion Auxiliary::string2version( const std::string& aFV )
{
    if( aFV == FIXVERSIONS[0] ) {
        return Engine::FIX40;
    } else if( aFV == FIXVERSIONS[1] ) {
        return Engine::FIX41;
    } else if( aFV == FIXVERSIONS[2] ) {
        return Engine::FIX42;
    } else if( aFV == FIXVERSIONS[3] ) {
        return Engine::FIX43;
    } else if( aFV == FIXVERSIONS[4] ) {
        return Engine::FIX44;
    } else if( aFV == FIXVERSIONS[5] ) {
        return Engine::FIX50;
    } else {
        return Engine::NA;
    }
}

Engine::MessageStorageType Auxiliary::string2storageType( const std::string& storageType )
{
    return Session::stringToMessageStorageType( storageType.c_str() );
}

void Auxiliary::notification( const std::string& aMsg )
{
    MGuard _( m_lock );
    cout << "NOTIFICATION: " << aMsg << endl;
}

void Auxiliary::warning( const std::string& aMsg )
{
    MGuard _( m_lock );
    cout << "WARNING: " << aMsg << endl;
}

void Auxiliary::error( const std::string& aMsg )
{
    MGuard _( m_lock );
    cout << "ERROR: " << aMsg << endl;
}

