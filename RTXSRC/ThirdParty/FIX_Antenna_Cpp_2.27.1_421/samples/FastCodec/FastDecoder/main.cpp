// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>
#include <fstream>

#include <B2BITS_FastCodec.h>
#include <B2BITS_SystemDefines.h>

namespace
{
    typedef std::vector<char> Buffer;

    char const* DEFAULT_TEMPLATES_FILE = "templates.xml";
    const std::size_t BUFFER_SIZE = 1024 * 1024;
}

void usage( std::ostream& out )
{
    out << "Usage: " <<
        std::endl << "\t-h,  --help       display this message" <<
        std::endl << "\t-i,  --input      input file; default - the standard input stream" <<
        std::endl << "\t-o,  --output     output file; default - the standard output stream" <<
        std::endl << "\t-t,  --templates  path to a xml file that contains FAST templates;" <<
        std::endl << "\t                  default - 'templates.xml'" <<
        std::endl << "\t-a,  --additional path to a xml file that contains extensions of" <<
        std::endl << "\t                  the FIX protocols (optional)" <<
        std::endl << "\t-s1, --skip1      count of bytes that needs to be skipped at the begin of" <<
        std::endl << "\t                  the input stream; default - 0" <<
        std::endl << "\t-s2, --skip2      count of bytes that needs to be skipped before each message;" <<
        std::endl << "\t                  default - 0" <<
        std::endl << "\t-r,  --reset      0 - means to reset the dictionary; 1 - not to reset" <<
        std::endl << "\t                  default - 0" <<
        std::endl << "\t-sv, --scpVer     trasnport protocol version; default - scpVer == appVer" <<
        std::endl << "\t-av, --appVer     application protocol version (required)" <<
        std::endl <<
        std::endl;
}

Engine::FIXVersion toFixVersion( const std::string& sver )
{
    if ( sver == "FIX40" ) {
        return Engine::FIX40;
    } else if ( sver == "FIX41" ) {
        return Engine::FIX41;
    } else if ( sver == "FIX42" ) {
        return Engine::FIX42;
    } else if ( sver == "FIX43" ) {
        return Engine::FIX43;
    } else if ( sver == "FIX44" ) {
        return Engine::FIX44;
    } else if ( sver == "FIX50" ) {
        return Engine::FIX50;
    } else if ( sver == "FIX50SP1" ) {
        return Engine::FIX50SP1;
    } else if ( sver == "FIX50SP2" ) {
        return Engine::FIX50SP2;
    } else if ( sver == "FIX50SP1CME" ) {
        return Engine::FIX50SP1CME;
    } else if ( sver == "FIXT11" ) {
        return Engine::FIXT11;
    }

    return Engine::NA;
}

inline const std::string xmls( const std::string& additionalXml, Engine::FIXVersion scpVer, Engine::FIXVersion appVer )
{
    std::string result;

    switch ( appVer ) {
    case Engine::FIX40:
        result = "../../../../data/fixdic40.xml";
        break;
    case Engine::FIX41:
        result = "../../../../data/fixdic41.xml";
        break;
    case Engine::FIX42:
        result = "../../../../data/fixdic42.xml";
        break;
    case Engine::FIX43:
        result = "../../../../data/fixdic43.xml";
        break;
    case Engine::FIX44:
        result = "../../../../data/fixdic44.xml";
        break;
    case Engine::FIX50:
        result = "../../../../data/fixdic50.xml";
        break;
    case Engine::FIX50SP1:
        result = "../../../../data/fixdic50sp1.xml";
        break;
    case Engine::FIX50SP2:
        result = "../../../../data/fixdic50sp2.xml";
        break;
    case Engine::FIX50SP1CME:
        result = "fixdic50sp1cme.xml";
        break;
    default:
        assert( "Unsupported FIX version" && 0 );
    }

    switch ( scpVer ) {
    case Engine::FIXT11:
        result += "|../../../../data/fixdict11.xml";
        break;
    }

    if ( !additionalXml.empty() ) {
        result = result + "|" + additionalXml;
    }

    return result;
}

void process( Engine::FastDecoder& fastDecoder, std::size_t s2, bool reset, char const* begin, char const* end, std::ostream& output )
{
    char const* cur = begin + s2;

    while ( cur < end ) {
        if ( reset ) {
            fastDecoder.resetDictionary();
        }

        std::size_t decoded( 0 );
        std::auto_ptr<Engine::FIXMessage> fixMessage = fastDecoder.decode( cur, end - cur, &decoded );

        int size( 0 );
        char const* msg = fixMessage->toRaw( &size );
        output << std::string( msg, size ) << std::endl;

        cur += decoded + s2;
    }
}

void process(
    Engine::FastDecoder& fastDecoder,
    std::size_t s1,
    std::size_t s2,
    bool reset,
    std::istream& input,
    std::ostream& output )
{
    if ( input.eof() ) {
        return;
    }

    input.ignore( s1 );

    Buffer buffer;
    std::streamsize read( 0 );
    while ( !input.eof() ) {
        buffer.resize( read + BUFFER_SIZE );
        input.read( &*buffer.begin() + read, buffer.size() - read );
        read += input.gcount();
    }

    buffer.resize( read );
    process( fastDecoder, s2, reset, &*buffer.begin(), &*buffer.begin() + buffer.size(), output );
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        char const* inputFile( NULL );
        char const* outputFile( NULL );
        char const* pathToAdditionalXml( NULL );
        char const* pathToTemplatesXml( DEFAULT_TEMPLATES_FILE );
        std::size_t s1( 0 );
        std::size_t s2( 0 );
        bool reset( false );

        Engine::FIXVersion scpVer( Engine::NA );
        Engine::FIXVersion appVer( Engine::NA );

        int argi( 1 );

        // Parse command line parameters
        for ( ; argi < argc; ++argi ) {
            if ( argc == 1 && argi == 1 &&
                 ( strcmp( argv[argi], "--help" ) == 0 || strcmp( argv[argi], "-h" ) == 0 ) ) {
                usage( std::cout );
                return 0;
            } else if ( argi + 1 == argc ) {
                usage( std::cerr );
                return 1;
            } else if ( ( strcmp( argv[argi], "--input" ) == 0 ) || ( strcmp( argv[argi], "-i" ) == 0 ) ) {
                inputFile = argv[++argi];
            } else if ( ( strcmp( argv[argi], "--output" ) == 0 ) || ( strcmp( argv[argi], "-o" ) == 0 ) ) {
                outputFile = argv[++argi];
            } else if ( ( strcmp( argv[argi], "--additional" ) == 0 ) || ( strcmp( argv[argi], "-a" ) == 0 ) ) {
                pathToAdditionalXml = argv[++argi];
            } else if ( ( strcmp( argv[argi], "--templates" ) == 0 ) || ( strcmp( argv[argi], "-t" ) == 0 ) ) {
                pathToTemplatesXml = argv[++argi];
            } else if ( ( strcmp( argv[argi], "--skip1" ) == 0 ) || ( strcmp( argv[argi], "-s1" ) == 0 ) ) {
                s1 = atoi( argv[++argi] );
            } else if ( ( strcmp( argv[argi], "--skip2" ) == 0 ) || ( strcmp( argv[argi], "-s2" ) == 0 ) ) {
                s2 = atoi( argv[++argi] );
            } else if ( ( strcmp( argv[argi], "--reset" ) == 0 ) || ( strcmp( argv[argi], "-r" ) == 0 ) ) {
                reset = atoi( argv[++argi] ) != 0;
            } else if ( ( strcmp( argv[argi], "--scpVer" ) == 0 ) || ( strcmp( argv[argi], "-sv" ) == 0 ) ) {
                scpVer = toFixVersion( argv[++argi] );

                if ( scpVer == Engine::NA ) {
                    throw Utils::Exception( std::string( "Invalid parameter: scpVer == " ) + argv[++argi] );
                }
            } else if ( ( strcmp( argv[argi], "--appVer" ) == 0 ) || ( strcmp( argv[argi], "-av" ) == 0 ) ) {
                appVer = toFixVersion( argv[++argi] );

                if ( appVer == Engine::NA ) {
                    throw Utils::Exception( std::string( "Invalid parameter: appVer == " ) + argv[argi] );
                }
            } else {
                usage( std::cerr );
                return 1;
            }
        }

        if ( appVer == Engine::NA ) {
            throw Utils::Exception( "appVer is required parameter" );
        }

        if ( scpVer == Engine::NA ) {
            scpVer = appVer;
        }

        // Initialize FastCodec
        Engine::FastCodec fastCodec( "../../../../engine.license" );
        std::auto_ptr<Engine::FastDecoder> fastDecoder( fastCodec.createFastDecoder(
                    xmls( pathToAdditionalXml, scpVer, appVer ),
                    pathToTemplatesXml,
                    scpVer, appVer ) );

        std::ifstream input( inputFile ? inputFile : "", std::ios::binary );
        std::ofstream output( outputFile ? outputFile : "", std::ios::binary );

        process( *fastDecoder, s1, s2, reset, inputFile ? input : std::cin, outputFile ? output : std::cout );
    } catch ( Utils::Exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 2;
    } catch ( std::exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 3;
    } catch ( ... ) {
        std::cerr << "Unknown error." << std::endl;
        return 4;
    }

    return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////
