/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <cstddef>
#include <cassert>
#include <cstdlib>
#include <fstream>

#include "B2BITS_CME/Params.h"

#include "SampleApplication.h"

char getChoice() {
    std::cout << "Your choice: ";
    char c;
    std::cin >> c;
    return c;
}

std::string requestMarketSegmentId() {
	std::string marketSegment;
	std::cout << "Enter Market Segment Id for the operation: ";
	std::cin >> marketSegment;

	return marketSegment;
}

void doBeginWeekLogon(SampleApplication* app) {
    std::string marketSegmentId = requestMarketSegmentId();

	try {
		app->beginWeekLogon(marketSegmentId);
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: Cannot perform Begin Week Logon: " << e.what() << std::endl;
    }
}

void doMidWeekLogon(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

	try {
		app->midWeekLogon(marketSegmentId);
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: Cannot perform Mid Week Logon: " << e.what() << std::endl;
    }
}

void doInSessionLogon(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

	try {
		app->inSessionLogon(marketSegmentId);
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: Cannot perform In-Session Logon: " << e.what() << std::endl;
    }
}

void doDisconnect(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

    app->disconnect(marketSegmentId);
}

void doSeqNumSetting(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

    int inSeqNum = 0;
    int outSeqNum = 0;

    std::cout << "[SeqNums settings]" << std::endl;
    std::cout << "Enter incoming SeqNum (or type 0 to leave it as-is): ";
    std::cin >> inSeqNum;

    std::cout << "Enter outgoing SeqNum (or type 0 to leave it as-is): ";
    std::cin >> outSeqNum;

    if (0 != inSeqNum) {
		app->setInSeqNum(marketSegmentId, inSeqNum);
    }

    if (0 != outSeqNum) {
		app->setOutSeqNum(marketSegmentId, outSeqNum);
    }
}

void doSwitchBackupToPrimary(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

	app->switchBackupToPrimary(marketSegmentId);
}

void doMidWeekReconnect(SampleApplication* app) {
	std::string marketSegmentId = requestMarketSegmentId();

	int startSequenceNumber = 0;
	std::cout << "Plase enter StartSequenceNumber (received in Session Level Reject message) or 0 to skip setting InSeqNum: ";
	std::cin >> startSequenceNumber;

	app->midWeekReconnect(marketSegmentId, startSequenceNumber);
}


void doSendMessage( SampleApplication* app ) {
	std::string marketSegmentId = requestMarketSegmentId();

    try {
        std::cout << "  [Message sending]" << std::endl;
		std::cin.ignore();

        for( ;; ) {
            std::cout << "  Message file name (or type / to cancel): ";

            std::string msgFileName;
            std::getline( std::cin, msgFileName );
            if( "" == msgFileName || "/" == msgFileName ) {
				break;
            }

            std::ifstream msgFile( msgFileName.c_str(), std::ios::binary );

            if( !msgFile ) {
                std::cerr << "  ERROR: Cannot open: " << msgFileName << std::endl;
                continue;
            }

            std::string msgBody;

            do {
                std::getline( msgFile, msgBody );
            } while( msgBody.empty() );
			           

			app->sendMessage(marketSegmentId, msgBody);
			
        }
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: Cannot send: " << e.what() << std::endl;
    }
}

void doSendTestRequest(SampleApplication* app) {
	std::string marketDataSegmentId = requestMarketSegmentId();

    std::string text;
    Engine::UTCTimestamp::now().toFixString(&text);

	app->sendTestRequest(marketDataSegmentId, text);
}

void printMenu() {
    std::cout << "=============================================" << std::endl;
    std::cout << "Menu:" << std::endl;
    std::cout << "=============================================" << std::endl;
    std::cout << "1. Begin Week Logon" << std::endl;
	std::cout << "2. Mid Week Logon" << std::endl;
	std::cout << "3. In-Session Logon" << std::endl;
	std::cout << "4. Disconnect session" << std::endl;
	std::cout << "5. Set Seq Nums" << std::endl;
	std::cout << "6. Switch Backup to Primary Host Ip" << std::endl;
	std::cout << "7. Reconnect Session Mid Week" << std::endl;
	std::cout << "8. Send message (for Trading interface only)" << std::endl;
	std::cout << "9. Send Test Request message (used in In-Session Logon)" << std::endl;
	std::cout << "0. Exit" << std::endl;
    std::cout << "=============================================" << std::endl;
}

void runSample() {
	

	//construct initiator application
	SampleApplication application;

	const b2bitscme::Params* params = application.getParams();

	params->showParams();

	for( ;; ) {
        printMenu();

        char c = getChoice();

        switch(c) {
        case '0': // exit
            return;

        case '1': // Begin Week Logon
            doBeginWeekLogon(&application);
            break;

        case '2': // Mid Week Logon
			doMidWeekLogon(&application);
            break;

        case '3': // In-Session Logon
            doInSessionLogon(&application);
            break;

        case '4': // disconnect session
            doDisconnect(&application);
            break;

		case '5': // Seq Num Setting
			doSeqNumSetting(&application);
            break;

		case '6': // Switch Backup To Primary
			doSwitchBackupToPrimary(&application);
            break;

		case '7': // Reconnect Session Mid Week
			doMidWeekReconnect(&application);
            break;

		case '8': // Reconnect Session Mid Week
			doSendMessage(&application);
            break;

		case '9': // Send test request
            doSendTestRequest(&application);
            break;

        default:
            std::cout << "Unrecognized command: " << c << std::endl;
            break;
        } // switch
    } // for( ;; )
}

int main() {
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        Engine::FixEngine::init();
        runSample();
        Engine::FixEngine::destroy();
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: " << e.what() << std::endl;
        Engine::FixEngine::destroy();

        return 1;
    }

    return 0;
}
