//
// Created by schen on 10/30/2020.
//

#ifndef RTXROUTER_CONFIGROUTERBOX_H
#define RTXROUTER_CONFIGROUTERBOX_H

#include "Price.h"

using namespace std;

struct RouteStruct
{
    bool InOperationTime(int currentTime) {return (currentTime >= startTimeSsm && currentTime < endTimeSsm);}
    std::string name;                       //SWP-DRK2
    std::string venue;                      //DRK2, "INDR_NASD"
    int         tif;                        //3
    int         swpMinimumSize=1;           //100
    bool        midpointEligibility=false;  //true
    bool        multiPounce=false;          //true
    int         maxNumberOfTry=1;           //3
    bool        oddLot=true;                //false

    std::string startTimeStr;               //"9:30:05"
    std::string endTimeStr;                 //"15:59:55"
    int         startTimeSsm = 0;           //default to 0
    int         endTimeSsm = 86400;         //default to 24*60*60

    int         priority;                           //1
    bool        quoteConsumed=false;                //1
    Price      aggressiveOffset = Price(0.0);       //0.02
};

class ConfigRouterBox
{
public:
    ConfigRouterBox();
    virtual ~ConfigRouterBox();
    void InitConfigRouterBox();

    void SetRoute(const std::string& name, const std::shared_ptr< RouteStruct>& route);
    void SetVenueToRoute(std::string& venueStr, const std::string& routeName);
    std::shared_ptr< RouteStruct>& Route(const std::string& name);
    std::unordered_map <std::string /*TARG-NASD*/, std::shared_ptr< RouteStruct>>& Routes(){ return routeList;}
    std::unordered_map <std::string /*0*/, std::unordered_set< std::string>>& VenueToRouteList() {return venueToRouteList;}
    std::unordered_set< std::string>& FindRoutesFromVenue(const std::string& venue);

private:
    std::unordered_map <std::string /*0*/, std::shared_ptr< RouteStruct>> routeList;
    std::unordered_map <std::string /*0*/, std::unordered_set< std::string>> venueToRouteList;

    std::shared_ptr< RouteStruct> tpRoute_;
    std::unordered_set< std::string> tpSet_;
};

#endif //RTXROUTER_CONFIGROUTERBOX_H
