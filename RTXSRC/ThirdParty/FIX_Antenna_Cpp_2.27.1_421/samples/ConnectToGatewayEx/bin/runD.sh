#!/bin/sh

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

mkdir -p logs/backup

./connectToGatewayExD -version FIX50 -msgFile msg.fix -interactiveResend true -sessionProtocol FIXT11 CTGX Acceptor localhost 9011 $*
