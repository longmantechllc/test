// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CqgInstrumentListener.h
/// Contains Cqg::InstrumentListener class definition

#ifndef H_B2BITS_CQG_InstrumentListener_H
#define H_B2BITS_CQG_InstrumentListener_H

#include <B2BITS_CompilerDefines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_CqgDefines.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_FIXGroup.h>
#include <B2BITS_FIXFields.h>
#include <vector>

namespace Engine
{
    class FIXMessage;
    class TagValue;
} // namespace Engine {

namespace Cqg
{

    /// Instrument listener. Receives instrument related events.
    class InstrumentListener: public virtual Utils::ReferenceCounter
    {
    public:

        /// Faired when update for instrument is available.
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param msgs Increment container with group entry inside Market Data - Incremental Refresh message assigned to instrument.
        /// @param incrementsCount Increments count into Msgs
        /// @return false if book need start recovery, otherwise true.
        virtual bool onIncrement( ASecurityDescription secDesc,
                                   ASecurityID secID,
                                   Engine::FIXMessage const* const* msgs, 
                                   size_t incrementsCount) = 0;
        /// Faired when user should reset book with the new values and Natural Refresh is used
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param nrMsgs Increment container with group entry inside Market Data - Incremental Refresh message assigned to instrument.
        /// @param incrementsCount Increments count into NRMsgs 
        /// @param isNewSequence If is true that NRMsgs contains new sequence of increments, otherwise NRMsgs contains continuation of previosly received increments sequence.
        /// @return true if book is recovered otherwise false
        virtual bool onNaturalRefresh( ASecurityDescription secDesc,
                                       ASecurityID secID,
                                       Engine::FIXMessage const* const* nrMsgs, 
                                       size_t incrementsCount, 
                                       bool isNewSequence) 
        {
            B2B_USE_ARG( secDesc );
            B2B_USE_ARG( secID );
            B2B_USE_ARG( nrMsgs );
            B2B_USE_ARG( incrementsCount );
            B2B_USE_ARG( isNewSequence );
            return false;
        }
      
        /// Faired on error (example: when second subscribing was attempt for the same instrument)
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param error text message which describes the error
        virtual void onError(ASecurityDescription secDesc,
                                  ASecurityID secID,
                                  Engine::AsciiString const& error ) = 0;

        
        /// Faired when information regard to instrument received
        virtual void process(ASecurityDescription secDesc, ASecurityID secID, Engine::FIXMessage const& msg ) = 0;

        /// Raised when Snapshot Full refresh message is received
        virtual void onSnapshot( ASecurityDescription secDesc,
                                 ASecurityID secID,
                                 Engine::FIXMessage const& wMsg ) = 0;

        /// Raised to reset instrument date to initial state
        virtual void onReset( ASecurityDescription secDesc, ASecurityID secID ) = 0;

        /// Raised when subscription to instrument is accepted by library
        virtual void onSubscribed( ASecurityDescription secDesc, ASecurityID secID ) {
            B2B_USE_ARG( secDesc );
            B2B_USE_ARG( secID );
        }

        /// Raised when unsubscription to instrument is accepted by library
        virtual void onUnsubscribed( ASecurityDescription secDesc, ASecurityID secID ) {
            B2B_USE_ARG( secDesc );
            B2B_USE_ARG( secID );
        }

        /// Faired when recovery is started
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onRecoveryStarted( ASecurityDescription secDesc, ASecurityID secID, RecoveryReason reason ) = 0;

        /// Faired when recovery is ended
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onRecoveryStopped( ASecurityDescription secDesc, ASecurityID secID, RecoveryReason reason ) = 0;

        virtual ~InstrumentListener() {}
    }; // class InstrumentListener
} // namespace Cqg {

#endif // H_B2BITS_CQG_InstrumentListener_H
