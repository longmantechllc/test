#pragma once

#include <string_view>
#include "json_dto/pub.hpp"

namespace Crypto::gemini {
static constexpr std::string_view INVALID_STRING_VIEW = "N/A";

namespace WS {

    enum class ResponseType : std::uint8_t {
        INVALID_TYPE = 0,
        update,
        heartbeat
    };

    enum class ReasonType : std::uint8_t {
        INVALID_TYPE = 0,
        initial,
        trade,
        cancel,
        place
    };
    enum class EventType : std::uint8_t {
        INVALID_TYPE = 0,
        change,
        trade,
        cancel,
        place,
        auction,
        block_trade
    };
     
    enum class SideType : std::uint8_t {
        INVALID_TYPE = 0,
        bid,
        ask
    };
}

}  // namespace Crypto::gemini

namespace json_dto {

#define read_json_for_enum(enum_type) \
template<> \
void read_json_value(enum_type &v, const rapidjson::Value &object)

read_json_for_enum(Crypto::gemini::WS::ReasonType);
read_json_for_enum(Crypto::gemini::WS::SideType);
read_json_for_enum(Crypto::gemini::WS::ResponseType);
read_json_for_enum(Crypto::gemini::WS::EventType);

#undef read_json_for_enum
}  // namespace json_dto