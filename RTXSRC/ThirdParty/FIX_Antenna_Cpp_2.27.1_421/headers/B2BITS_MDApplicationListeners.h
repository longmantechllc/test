// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MDApplicationListeners.h
/// Contains Globex::SecurityDefinitionListener, Globex::InstrumentListener classes definition

#ifndef __B2BITS_GLOBEX_APPLICATION_LISTENERS_H__
#define __B2BITS_GLOBEX_APPLICATION_LISTENERS_H__

#include <vector>

#include <B2BITS_String.h>
#include <B2BITS_MDTypes.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_ReferenceCounter.h>

namespace Engine
{
    class FIXMessage;
    class TagValue;
}

namespace Globex
{
    typedef std::vector<Engine::TagValue*> Increments;

    /// Receives Security Definition messages
    class SecurityDefinitionListener
        : virtual public Utils::ReferenceCounter
    {
    public:
        /// Faired when security definition message is received
        ///
        /// @return true to continue listening instrument replay; false otherwise.
        /// @note UDS Instrument Delete message will be passed to Globex::InstrumentListener::process event handler
        virtual bool onSecurityDefinition( SecurityDesc const& secDesc,
                                           SecurityID secID,
                                           Engine::FIXMessage const& dMsg,
                                           std::string const& channelID ) = 0;

        /// Faired when security definition sequence was ended
        /// @return true to continue listening into next security definition sequence; false otherwise.
        /// @note UDS Instrument Delete message will be passed to Globex::InstrumentListener::process event handler
        virtual bool onEndSecurityDefinitionSequence( Symbol const& /*symbol*/,
                SecurityID /*secID*/,
                SecurityDesc const& /*secDesc*/,
                std::string const& /*secGroup*/,
                std::string const& /*channel_id*/ ) {
            return true;
        }

    protected:
        /// Destructor
        virtual ~SecurityDefinitionListener() {}
    };

    /// Receives instrument related events
    class InstrumentListener
        : public SecurityDefinitionListener
    {
    public:
        /// Notifies about new subscription. Faired when user call Globex::MDApplication::subscribe methods.
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onSubscribed( SecurityDesc const& secDesc, SecurityID secID ) = 0;

        /// Faired when unsubscribed from security description
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onUnsubscribed( SecurityDesc const& secDesc, SecurityID secID ) = 0;

        /// Faired when information regard to instrument received
        ///
        /// @param msg Incoming FIX Message
        /// @note UDS Instrument Delete message will be passed to this event handler as well
        virtual void process( Engine::FIXMessage const& msg ) = 0;

        /// Faired when update for instrument is available.
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param msgs Increment container with group entry inside Market Data - Incremental Refresh message assigned to instrument.
        /// @param incrementsCount Increments count into Msgs
        /// @return false if book need start recovery, otherwise true.
        virtual bool onIncrement( SecurityDesc const& secDesc,
                                  SecurityID secID,
                                  Engine::TagValue const* const* msgs,
                                  size_t incrementsCount ) = 0;

        /// Faired when user should reset book with the new values and Natural Refresh is used
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param nrMsgs Increment container with group entry inside Market Data - Incremental Refresh message assigned to instrument.
        /// @param incrementsCount Increments count into NRMsgs
        /// @param isNewSequence If is true that NRMsgs contains new sequence of increments, otherwise NRMsgs contains continuation of previosly received increments sequence.
        /// @return true if book is recovered otherwise false
        virtual bool onNaturalRefresh( SecurityDesc const& secDesc,
                                       SecurityID secID,
                                       Engine::TagValue const* const* nrMsgs,
                                       size_t incrementsCount,
                                       bool isNewSequence ) = 0;

        /// Faired when user should reset book with the bnew values.
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param msg Market Data - Snapshot message
        virtual void onSnapshot( SecurityDesc const& secDesc, SecurityID secID, Engine::FIXMessage const& msg ) = 0;

        /// Faired when recovery is started
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onRecoveryStarted( SecurityDesc const& secDesc, SecurityID secID ) = 0;

        /// Faired when recovery is ended
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onRecoveryStopped( SecurityDesc const& secDesc, SecurityID secID, RecoveryReason reason ) = 0;

        /// Faired on error (example: when second subscribing was attempt for the same instrument)
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        /// @param error text message which describes the error
        virtual void onError( SecurityDesc const& secDesc, SecurityID secID, Engine::AsciiString const& error ) = 0;

        /// Faired on reset book (35=X and 269=J)
        ///
        /// @param secDesc Security Description of the instrument
        /// @param secID Security ID of the instrument
        virtual void onBookReset( SecurityDesc const& secDesc, SecurityID secID ) = 0;

    protected:
        virtual ~InstrumentListener() {}
    };
}

#endif  //__B2BITS_GLOBEX_APPLICATION_LISTENERS_H__
