//
// Created by schen on 10/4/2020.
//

#include <iostream>
#include <stdio.h>
#include <unordered_map>
#include <fstream>
#include <vector>
#include <functional>
#include <memory>
#include <boost/filesystem.hpp>
////#include <filesystem>

#include <xercesc/parsers/XercesDOMParser.hpp>
#include <xercesc/dom/DOM.hpp>
#include <xercesc/framework/LocalFileInputSource.hpp>
#include <xercesc/sax/HandlerBase.hpp>
#include "auto_xerces_ptr.h"

#include "RTXUtils.h"
#include "Logger.h"

#include "ConfigGlobalBox.h"
#include "Config.h"

#include "LoaderBase.h"
#include "ConfigGlobalLoader.h"
#include "ConfigRouterLoader.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigUserLoader.h"
#include "ConfigLoader.h"

using namespace std ;
using namespace xercesc ;


ConfigLoader::ConfigLoader(Logger* logger, Config* config)
    :m_loggerSys(logger),
     m_config(config)
{

}

ConfigLoader::~ConfigLoader()
{
    for (auto ldr : loaders)
    {
        delete ldr.second;
    }
}

bool ConfigLoader::InitConfigLoader()
{
    std::string configName =  "RTX_Router.xml";
    FillLoader();
    auto retrn = LoadConfigFile( configName );

    return retrn;
}


bool ConfigLoader::LoadConfigFile(const std::string& filename)
{
    XMLPlatformUtils::Initialize() ;
    auto_xerces_ptr<XMLCh> filenameXstr(XMLString::transcode( filename.c_str() ));
    LocalFileInputSource inputSource( filenameXstr.get() );

    try
    {
        auto parser = new XercesDOMParser() ;
        parser->setValidationScheme(XercesDOMParser::Val_Never) ;
        parser->setDoNamespaces(true);

        auto errHandler = (ErrorHandler*) new HandlerBase();
        parser->setErrorHandler(errHandler);
        parser->parse( inputSource );

        DOMElement* root = parser->getDocument()->getDocumentElement() ;
        DOMNodeList* nodeList = root->getChildNodes() ;

        for(int i=0; i!=nodeList->getLength(); i++)
        {
            DOMNode *subNode = nodeList->item(i);
            char *nodeName = XMLString::transcode(subNode->getNodeName());
            if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
                continue;

            auto pos = loaders.find(nodeName);
            if (pos != loaders.end())
            {
                (*pos).second->ProcessLoading(this, subNode);
            }
            XMLString::release(&nodeName);
        }
    }
    catch( std::invalid_argument& e )
    {
        m_loggerSys->logFatal("XML configuration parse error: %s\n", e.what() );
        return false;
    }
    catch( std::exception& e )
    {
        m_loggerSys->logFatal("XML configuration parse error: %s\n", e.what() );
        return false;
    }
    catch(SAXException e)
    {
        char* errorMsg = XMLString::transcode(e.getMessage()) ;
        m_loggerSys->logFatal("XML configuration error: %s\n", errorMsg);
        XMLString::release( &errorMsg );
        return false;
    }
    return true;
}



