//
// Created by schen on 10/30/2020.
//
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>
#include <memory>
#include <functional>
#include <climits>
#include <iomanip>
#include <unordered_set>

#include "RTXUtils.h"
#include "ConfigRouterBox.h"

ConfigRouterBox::ConfigRouterBox()
{

}

ConfigRouterBox::~ConfigRouterBox()
{
    routeList.clear();
}

void ConfigRouterBox::InitConfigRouterBox()
{

}

std::shared_ptr< RouteStruct>& ConfigRouterBox::Route(const std::string& name)
{
    auto rt = routeList.find(name);
    if(rt != routeList.end())
    {
        return rt->second;
    }

    return tpRoute_;
}

void ConfigRouterBox::SetRoute(const std::string& name, const std::shared_ptr< RouteStruct>& routePtr)
{
    if(routeList.find(name) == routeList.end())
    {
        routePtr->startTimeSsm = RTXUtils::StrToTimeSsm(routePtr->startTimeStr, 0);
        routePtr->endTimeSsm = RTXUtils::StrToTimeSsm(routePtr->endTimeStr, 86400);     //24*60*60
        auto pr = std::make_pair(name, routePtr);
        routeList.insert(pr);
    }
}

void ConfigRouterBox::SetVenueToRoute(std::string& venueStr, const std::string& routeName)
{
    venueToRouteList[venueStr].insert(routeName);
}

std::unordered_set< std::string>& ConfigRouterBox::FindRoutesFromVenue(const std::string& venue)
{
    auto st = venueToRouteList.find(venue);
    if(st != venueToRouteList.end())
        return st->second;

    return tpSet_;
}