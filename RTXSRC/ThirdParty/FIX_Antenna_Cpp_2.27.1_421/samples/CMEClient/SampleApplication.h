/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <B2BITS_V12.h>
#include <B2BITS_LogCategory.h>
#include <B2BITS_LogSystem.h>

#include "B2BITS_CME/Params.h"
#include "B2BITS_CME/CMEApplication.h"
#include "B2BITS_CME/SessionCollection.h"

#include "CustomCMEApplication.h"


#pragma once

#if _MSC_VER >= 1500
#   pragma warning(push)
    // A keyword was used that is not in the C++ standard, for example, 
    // one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
#endif

class SampleApplication: public ISem
{
public:
	explicit SampleApplication();
	virtual ~SampleApplication(void);


	/// Establishes connection using Begin Week Logon
	void beginWeekLogon(const std::string &marketSegmentId);

	/// Establishes connection using Mid Week Logon
	void midWeekLogon(const std::string &marketSegmentId);

	/// Resets sequence numbers after the client has logged on using In-session logon
	void inSessionLogon(const std::string &marketSegmentId);

    /// Shutdowns connection.
    void disconnect(const std::string &marketSegmentId, bool keepSeqNum = false, int startSequenceNumber = 0, bool switchBackupToPrimary = false);

	/// Reset incoming seq num
    void setInSeqNum(const std::string &marketSegmentId, int seqNum);

    /// Reset outgiong seq num
    void setOutSeqNum(const std::string &marketSegmentId, int seqNum);
		
	/// Switch Backup Host Ip to Primary Host Ip for Market Data Segment
	void switchBackupToPrimary(const std::string &marketSegmentId);

	/// Reconnect Session with a specific inSeqNum, keeping outSeqNum
	void midWeekReconnect(const std::string &marketSegmentId, int startSequenceNumber, bool switchBackupToPrimary = false);


	void sendMessage(const std::string &marketSegmentId, const std::string& message);

	/// Send test request message
    void sendTestRequest(const std::string &marketSegmentId, const std::string& text);

	b2bitscme::Params* getParams() {
		return params_.get();
	}


	virtual void post() B2B_OVERRIDE
	{
		logSem_.post();
	}

	virtual void wait() B2B_OVERRIDE
	{
		logSem_.wait();
	}


private:

	/// Create Session
	void createSession(const std::string &marketSegmentId, int inSeqNum, int outSeqNum, bool switchBackupToPrimary = false);

private:

	Utils::Log::LogCategory* logger_;
	std::auto_ptr<b2bitscme::Params> params_;
	std::auto_ptr<b2bitscme::CMEApplication> cmeApp_;

	b2bitscme::SessionCollection sessions_;

	System::Semaphore logSem_;
};

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif
