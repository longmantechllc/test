// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#ifndef B2BITS_SYSTEM_THREAD_H
#define B2BITS_SYSTEM_THREAD_H

#if HAVE_PRAGMA_ONCE
#   pragma once
#endif

#include <cstddef>
#include <string>

#include "B2BITS_IntDefines.h"
#include "B2BITS_Mutex.h"
#include "B2BITS_ManualEvent.h"
#include "B2BITS_V12_Defines.h"

#ifndef _WIN32
#   include <pthread.h>
#endif // _WIN32

#if defined(_MSC_VER)
#   pragma warning(push)
#   pragma warning(disable: 4251)
#endif // defined(_MSC_VER)

//extern "C"{ void onceInitRoutine(); }
extern "C" {
    void exitRoutine( void* ) ;
}

namespace System
{
#ifndef _WIN32
    typedef pthread_t ThreadId;
#else // _WIN32
    typedef unsigned int ThreadId;
#endif // _WIN32

    /**
    * A thread of execution in a program.
    */
    class V12_API Thread
    {
    public: // types
        typedef void ( *ThreadListener ) () /*throw()*/;

        /// Thread priority
        enum Priority 
        { 
            DefaultPriority,    /// not set, use OS default 
            IdlePriority,       /// Idle thread priority
            LowestPriority,     /// Lower than LowPriority
            LowPriority,        /// Lower than NormalPriority
            NormalPriority,     /// Normal thread priority
            HighPriority,       /// Higher than NormalPriority
            HighestPriority,    /// Higher than HighPriority
            RealtimePriority    /// Realtime thread priority
        };

    private: // static fields
        static ThreadListener threadStartListener_;
        static ThreadListener threadStopListener_;

    private: // instance fields
        std::string name_;
        Mutex lock_;
        ThreadId id_;
        u64 affinity_;
        Priority priority_;
        bool destroyOnExit_;

#ifdef _WIN32
        HANDLE handle_;
        bool setTranslator_;
#endif // _WIN32

#ifdef _SOLARIS
        ManualEvent joinEvent_;
#endif
        
    public:
        /**
        * Constructor.
        * When a new thread is created, it does not begin immediate execution.
        * @param setExTranslator - valid for the Windows platform only. When true SEH
        * exceptions will be translated into the Utils::Exception.
        *
        * @throw SystemException
        *
        * @see start()
        */
        explicit Thread( bool setExTranslator = true );

        /**
        * Constructor.
        * When a new thread is created, it does not begin immediate execution.
        * @param pName - Thread name
		* @param setExTranslator - valid for the Windows platform only. When true SEH
        * exceptions will be translated into the Utils::Exception.
        *
        * @throw SystemException
        *
        * @see start()
        */
        explicit Thread( const char* pName, bool setExTranslator = true );
        explicit Thread( std::string aName, bool setExTranslator = true );

        /**
        * Destructor.
        */
        virtual ~Thread();

        /**
        * Causes this thread to begin execution.
        *
        * @throw SystemException
        */
        virtual void start();

        /**
        * Returns the thread's name.
        */
        std::string const& getName() const {
            return name_;
        }

        /**
        * Update thread name before starting.
        * @param name - new name to be assigned to the thread
        */
        void updateName(std::string const& name) {
            name_ = name;
        }

        /**
        * Waits for this thread to terminate.
        *
        * @throw SystemException
        */
        void join() ;

        /**
        * Waits for this thread to terminate.
        * @param milliseconds - timeout in milliseconds
        * @throw SystemException
        */
        bool join( unsigned int milliseconds ) ;

        /**
        * Causes the currently executing thread object to temporarily pause
        * and allow other threads to execute.
        *
        * @warning This function can cause 100% CPU usage. If it your case, you can replace it with 
        * System::Thread::sleep(1).
        *
        * @throw SystemException
        */
        static void yield() throw();

        /** 
         * Calculates uSleepDelay value based on given microsecond time span.
         * Value should be passed to Thread::uSleep method. 
         * This method was intended to minimize overhead of Thread::uSleep.
         *
         * @param delayUs Delay. Microseconds. Can be zero.
         */
        static u64 prepareUSleep( unsigned int delayUs ) throw();

        /** 
         * Performs busy wait using Thread::yield method.
         * Methods blocks execution on given uSleepDelay time, which
         * is calculated by Thread::prepareUSleep method.
         * 
         * @param uSleepDelay Delay value which is calculated by Thread::prepareUSleep method.
         */
        static void uSleep( u64 uSleepDelay ) throw();

        /**
        * Returns the thread ID of the calling thread.
        */
        static ThreadId self();

        /**
        * @return True if thread IDs equals, false otherwise.
        */
        bool equals( ThreadId thrId );

        /**
        * Requests that thread to be canceled.
        *
        * Cancellation is asynchronous. Use join() to wait for termination of
        * thread if necessary.
        * You should never use cancellation unless you really want the target thread
        * to go away. This is a termination mechanism.
        *
        * @throw SystemException
        *
        * @see "cancellation (3THR)"
        */
        virtual void cancel();

        /**
        * Causes the currently executing thread to sleep for the specified number
        * of milliseconds.
        *
        * It is a cancellation point.
        *
        * @return true if the method was interrupted by a signal, false otherwise.
        *
        * @throw SystemException
        */
        static bool sleep( unsigned int aMillis );

        /**
        * Returns thread's id.
        */
        ThreadId getId() const {
            return id_;
        }

        /**
        * Gives name to the current thread
        * @param name New name
        */
        static void setName( char const* name );

        /**
        * get name of the current thread
        */
        static void getSelfName( std::string & res );

        /** Sets CPU affinity. 
         * There is no waranty for this method.
         */
        void setAffinity( u64 mask ) throw();

        /** Sets CPU affinity for current thread.
        * @param mask Affinity mask for calling thread.
        */
        static void setCurrentThreadAffinity( u64 mask );

        /** Sets thread priority.
        * There is no waranty for this method.
        */
        void setPriority( Priority p ) throw();

        /** Sets priority for current thread.
        * @param p priority for calling thread.
        */
        static void setCurrentThreadPriority( Priority p );

        /** Set to true to destroy thread on exit */
        void destroyOnExit( bool value ) throw() {
            destroyOnExit_ = value;
        }

#ifdef _WIN32
        HANDLE win32Handle() {
            return handle_;
        }
#endif // #ifndef _WIN32


        static void setThreadStartListener( ThreadListener listener ) throw();
        static ThreadListener getThreadStartListener() throw();
        static void setThreadStopListener( ThreadListener listener ) throw();
        static ThreadListener getThreadStopListener() throw();

        static inline void callThreadStartListener() throw() {
            ThreadListener listener = getThreadStartListener();
            if( NULL != listener ) {
                ( *listener )();
            }
        }

        static inline void callThreadStopListener() throw() {
            ThreadListener listener = getThreadStopListener();
            if( NULL != listener ) {
                ( *listener )();
            }
        }

    protected:
        // cppcheck-suppress functionStatic
        void beforeRun();
        // cppcheck-suppress functionStatic
        void afterRun();
        
        /**
        * Thread's start function.
        * Subclasses of Thread should override this method.
        */
        virtual void run() = 0;

        typedef enum {DISABLED, DEFERRED, ASYNCHRONOUS} CancellationType;

        /**
        * Sets thread's cancellation mode.
        *
        * @throw SystemException
        */
        void setCancelMode( CancellationType aMode );

    private:
        /** *
        * @throw SystemException
        */
        void init( bool setExTranslator );
        void cleanup();

        Thread( const Thread& aT );
        Thread& operator=( const Thread& aRhs );

        static void launcher( Thread* pThr );
        static void launcherSEH( Thread* pThr );
        static void exitRoutine( void* );

#ifndef _WIN32
        static void onceInitRoutine();
        static void* startHook( void* apArg );
        static void setAffinityLinux( ThreadId tid, u64 mask ) throw();
#else // _WIN32
        static unsigned int __stdcall startHook( void* apArg );
#endif // _WIN32  
    };

#ifdef _WIN32
    /** Attach SEH translator function to translate them into Utils::Exception */
    extern V12_API void attachExceptionTranslator();
#endif

}

#if defined(_MSC_VER)
#   pragma warning( pop )
#endif // defined(_MSC_VER)

#endif // B2BITS_SYSTEM_THREAD_H

