#include "starkware/crypto/ffi/pedersen_hash.h"
#include "starkware/crypto/pedersen_hash.h"

#include <array>



namespace starkware {

int Hash(
    const gsl::byte in1[kElementSize], const gsl::byte in2[kElementSize],
    gsl::byte out[kOutBufferSize]) {
  try {
    auto hash = PedersenHash(
        PrimeFieldElement::FromBigInt(Deserialize(gsl::make_span(in1, kElementSize))),
        PrimeFieldElement::FromBigInt(Deserialize(gsl::make_span(in2, kElementSize))));
    Serialize(hash.ToStandardForm(), gsl::make_span(out, kElementSize));
  } catch (const std::exception& e) {
    return HandleError(e.what(), gsl::make_span(out, kOutBufferSize));
  } catch (...) {
    return HandleError("Unknown c++ exception.", gsl::make_span(out, kOutBufferSize));
  }
  return 0;
}

}  // namespace starkware
