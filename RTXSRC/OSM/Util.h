#pragma once
#include "Order.h"
#include "ExecutionReport.h"
#include "OSMOrder.h"

namespace OSM {
    //std::string execInst2str(const std::vector<std::string>& eInst);
    void printER(const char* msg, const ExecutionReport& er );
    void printOrder(const char* msg, const Order& ord);
    void printOrder(const char* msg, const OSMOrder& ord);
}
