//
// Created by schen on 10/6/2020.
//
#ifndef RTXROUTER_TARGETING_H
#define RTXROUTER_TARGETING_H

class ExecutionReport;
class RTXOrder;

class Targeting : public IOrderLogicBase
{
public:
    Targeting() =default;
    explicit Targeting(RTXOrder* order);
    ~Targeting() override;
    bool InitOrder() override;

    bool ProcessOrder() override;
    virtual bool SendChildOrder(std::shared_ptr<Order> order) override;
    bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child) override;
    bool ProcessMktData() override;

private:
    bool IsRouteEligible(const std::string& routeName);
    bool IsEligible();
    bool IsRouteMarketable(const std::string& routeName);
    bool IsMarketable();

    std::unordered_map<std::string, std::string> m_mmidRouteMapping;
    std::vector<std::string> m_mmidToRemoveList;
    std::pair<std::string, int> m_mmidQtyToConsumed;
};


#endif //RTXROUTER_TARGETING_H
