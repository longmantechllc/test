#pragma once
#include <string>

//std::time_t acquisitionDate = (std::time_t)res->getInt("acquisitionDate");
// https://en.cppreference.com/w/cpp/chrono/c/time_t
using datetime2 = std::string;
using datetime = uint64_t;
struct tblState {
    int parentorderid;
    std::string childorderid;
    std::string execid;
    int refpoid;
    std::string instancename;
    datetime timeStamp;
    int state;
};

struct tblChildOrder {
    int parentorderid;
    std::string childorderid;
    std::string gatewayid;
    datetime senddatetime;
    std::string instancename;
    int size;
    int displaysize;
    uint16_t type;
    double limitprice;
    double discretion;
    uint16_t tif;
    std::string venue;
    datetime2 expirytime;
    std::string execinstrution;
    int cminqty;
    std::string message;
};

struct tblExecution {
    int parentorderid;
    std::string childorderid;
    std::string execid;
    std::string instancename;
    datetime recdatetime;
    double limitprice;
    std::string venue;
    uint16_t tif;
    int fillsize;
    double fillprice;
    int cumqty;
    double avgprice ;
    std::string message;
    std::string transacttime;
};

struct tblParentOrder {
    int parentorderid;
    datetime tradedatetime;
    std::string instancename;
    int refpoid;
    std::string cltordid;
    std::string symbol;
    uint16_t side;
    int size;
    int displaysize;
    uint16_t type;
    double limitprice;
    double discretion;
    uint16_t tif;
    datetime2 expirationtime;
    std::string execinst;
    int minqty;
    std::string desk;
    std::string user;
    std::string message;
};