// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_PubEngineDefines.h
/// Contains basic FIX Engine declarations.

#ifndef __B2BITS_PubEngineDefines_h__
#define __B2BITS_PubEngineDefines_h__

#include <string>
#include <vector>
#include <map>
#include <set>

/* For V12_API macro */
#include <B2BITS_V12_Defines.h>
/* For B2B_DEPRECATED macro */
#include <B2BITS_CompilerDefines.h>
#include <B2BITS_Emptily.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_IPAddr.h>

#if defined(_MSC_VER)
#   pragma warning(push)
#   pragma warning(disable: 4251)
#endif // defined(_MSC_VER)

namespace Engine
{
    struct SessionExtraParameters;

    /** FIX protocol version. */
    enum FIXVersion {
        NA            = 0, ///< Unknown version
        FIX40         = 1, ///< FIX 4.0
        FIX41         = 2, ///< FIX 4.1
        FIX42         = 3, ///< FIX 4.2
        ESFIX         = 4, ///< EasyScreen version of 4.2
        FIX43         = 5, ///< FIX 4.3
        FIX44         = 6, ///< FIX 4.4
        FIX50         = 7, ///< FIX 5.0
        FIX50SP1      = 8, ///< FIX 5.0 SP1
        FIX50SP2      = 9, ///< FIX 5.0 SP2
        FIXT11        = 10,///< FIXT 1.1
        FIX50SP1CME   = 11,///< FIX 5.0 SP1 CME
        FAST_FIX44    = 12,
        FIXVersion_Last    /// Represents number of FIX protocols
    };

    /** Encryption method. */
    enum EncryptMethod {
        NONE        = 0,  ///< None / other
        PKCS        = 1,  ///< proprietary
        DES         = 2,  ///< EBC mode
        PKCS_DES    = 3,  ///< proprietary
        PGP_DES     = 4,  ///< defunct
        PGP_DES_MD5 = 5,  ///< see app note on FIX home page
        PEM_DES_MD5 = 6,  ///< see app note on FIX home page
        EncryptMethod_NA = 7, ///< Non-initialized
        EncryptMethod_Last
    };

	/** Encryption method. */
   enum CustomSessionType {
		GENERIC = 0, ///< generic session
		LME_SELECT = 1, ///< encrypt password tag each time
        CME_SECURE_LOGON = 2, /// < CME Secure logon support
		TOTAL_SSN_TYPES /// Represents number of sessions types
	};


    /** Force SeqNum Reset mode */
    enum ForceSeqNumResetMode {
        /** Default value; take the value from properties file */
        FSNR_NA,
        /** Use tag 141 at first Logon to reset sequence number */
        FSNR_ON,
        /** Reset sequence number at every Logon */
        FSNR_ALWAYS,
        /** Do not use tag 141 at Logon to reset sequence number */
        FSNR_OFF
    };


    /** Strategy for out of sequence messages */
    enum OutOfSequenceMessagesStrategyEnum {
        /** Default value; for engine parameter used OSMS_REQUEST_ALWAYS
            for session parameter used value of engine parameter*/
        OSMS_DEFAULT         = 0x00000000,
        /** send resend request on any out of sequence message */
        OSMS_REQUEST_ALWAYS  = 0x00000001,
        /** Send resend request on gap. Suppress new resend request if gap not closed */
        OSMS_REQUEST_ONCE    = 0x00000002,
        /** Use out of sequence queue for storing out of sequence messages and processing them after closing gap */
        OSMS_QUEUE           = 0x00000003, 
        /** Send out of sequence request on gap and processing out of sequence messages */
        OSMS_IGNORE_GAP      = 0x00000004, 

        OSMS_TOTAL           = 0x00000005
    };

    /* This is done to avoid enum leakage into global namespace and
     * to allow value resolution in compile time. */
    namespace BuiltinMessageStorage {
        enum T {
            Persistent,
            Transient,
            PersistentMM,
            SplitPersistent,
            Null
        };
    }

    /**
     * \brief Object representing FA's message storage.
     *
     * \warning Don't initialize instances of this class during static
     * initialization or you will encounter an undefined behavior.
     *
     * \warning This class is deprecated. Use StorageMgr instead.
     *
     * \code
       Engine::MessageStorageType
       postgre_storage = Engine::MessageStorageType::named("PostgreSQL");

       int storage_count = Engine::MessageStorageType::total();
       \endcode
     */
    class V12_API
    MessageStorageType {
        enum Kind {
            Kind_Invalid,
            Kind_Builtin,
            Kind_Custom,
            Kind_Default,
            Kind_Named,
            Kind_Total
        };

        Kind kind_;
        BuiltinMessageStorage::T builtinMST_;
        std::string name_;
        std::size_t namedIndex_;

    public:
        /**
         * \brief Creates invalid storage type by default.
         */
        MessageStorageType() : kind_(Kind_Invalid),
            builtinMST_(BuiltinMessageStorage::Null),
            name_("invalid"),
            namedIndex_(0) {}

        /**
         * \brief Create the instance of classic builtin storage type.
         *
         * \details Instantiates new object representing builtin
         * FIX Antenna's storage type. This is a special function. Use
         * predefined constant object representing storage types
         * instead.
         *
         * \throw Utils::Exception When unsupported storage type specified.
         *
         * \param [in] bmst Engine::BuiltinMessageStorage::BuiltinMessageStorageType
         * enumerated value to instantiate new object of
         * MessageStorageType.
         *
         * \return New instance of MessageStorageType representing
         * built-in message storage type of FIX Antenna.
         */
        static MessageStorageType
        builtin(BuiltinMessageStorage::T bmst);

        /**
         * \brief Create the instance of dynamic new storage type.
         *
         * \warning The precondition for this action is initialized
         * FIX Antenna's engine. Any kind of static initialization
         * will lead to undefined behavior.
         *
         * \throw Utils::Exception in case the specified storage name
         * is not registered.
         *
         * \param [in] name name of the built-in storage or name of
         * dynamically loaded storage registered via StorageMgr.
         *
         * \return New instance of MessageStorageType representing
         * dynamically registered FIX Antenna's storage.
         */
        static MessageStorageType
        named(std::string const & name);

        /**
         * \brief Get the copy of invalid_storageType value.
         *
         * \return New instance of MessageStorageType representing
         * special value - incorrectly specified storage.
         */
        static MessageStorageType
        invalid();

        /**
         * \brief Get the copy of default_storageType value.
         *
         * \return New instance of MessageStorageType representing
         * built-in message storage type of FIX Antenna
         */
        static MessageStorageType
        default_();

        /**
         * \brief Get the copy of custom_storageType value.
         *
         * \return New instance of MessageStorageType representing
         * built-in message storage type of FIX Antenna
         */
        static MessageStorageType
        custom();

        /**
         * \brief Get the copy of total_storageType value.
         *
         * \return New instance of MessageStorageType representing
         * special value - total number of storage identifiers.
         */
        static MessageStorageType
        total();

        /**
         * \brief Get the string representation of storage type.
         *
         * \return Case-sensitive string identifier of current object
         * representing storage.
         */
        std::string name() const;

        /**
         * \brief Get the total number of registered storages.
         *
         * \details Use this function to get the real number of storages
         * registered in StorageMgr.
         *
         * \warning This function shall be used after FIXAntenna engine initialization.
         *
         * \return Number of registered storages.
         */
        static std::size_t totalCount();

        /**
         * \brief Implicit int conversion
         *
         * \details In case we work with total_storageType we suppose
         * implicit int conversion is performed and return the total
         * number of builtin storages (statically compiled into FIX
         * Antenna). To get the access to all available storages (named
         * storages included) you have to use total() function. This is
         * done to avoid problems with static initialization.
         *
         * \return Index number of the storage in storage list.
         */
        operator std::size_t () const {
            switch (kind_) {
            case Kind_Total:
                return totalCount();
            case Kind_Named:
            case Kind_Builtin:
            case Kind_Invalid:
            case Kind_Custom:
            case Kind_Default:
            default:
                return namedIndex_;
            }
        }

        friend V12_API void
        swap(MessageStorageType & lhs, MessageStorageType & rhs);

        friend V12_API bool
        operator == (MessageStorageType const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator == (MessageStorageType const & lhs,
            int const & rhs);

        friend V12_API bool
        operator == (int const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator != (MessageStorageType const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator != (MessageStorageType const & lhs,
            int const & rhs);

        friend V12_API bool
        operator != (int const & lhs,
            MessageStorageType const & rhs);

        /**
         * \brief Pre-increment operator.
         *
         * \throw Utils::Exception When the resulting value will be
         * greater than total();
         */
        friend V12_API MessageStorageType &
        operator ++ (MessageStorageType & rhs);

        /**
         * \brief Pre-decrement operator.
         *
         * \throw Utils::Exception When the resulting value will be
         * lesser than invalid();
         */
        friend V12_API MessageStorageType &
        operator -- (MessageStorageType & rhs);

        /**
         * \brief Post-increment operator.
         *
         * \throw Utils::Exception When the resulting value will be
         * greater than total();
         */
        friend V12_API MessageStorageType
        operator ++ (MessageStorageType & lhs, int);

        /**
         * \brief Post-decrement operator.
         *
         * \throw Utils::Exception When the resulting value will be
         * lesser than invalid();
         */
        friend V12_API MessageStorageType
        operator -- (MessageStorageType & lhs, int);

        friend V12_API bool
        operator < (MessageStorageType const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator > (MessageStorageType const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator <= (MessageStorageType const & lhs,
            MessageStorageType const & rhs);

        friend V12_API bool
        operator >= (MessageStorageType const  & lhs,
            MessageStorageType const & rhs);
    };

	V12_API std::ostream& operator<<(std::ostream& ostm, const MessageStorageType& type);

    /**
     * \brief swap() as a free function with ADL and non-throwing
     * guarantee.
     */
	V12_API void
	swap(MessageStorageType & lhs, MessageStorageType & rhs);

	V12_API bool
    operator < (MessageStorageType const & lhs,
        MessageStorageType const & rhs);

	V12_API bool
    operator > (MessageStorageType const & lhs,
        MessageStorageType const & rhs);

	V12_API bool
    operator <= (MessageStorageType const & lhs,
        MessageStorageType const & rhs);

	V12_API bool
    operator >= (MessageStorageType const & lhs,
        MessageStorageType const & rhs);

    /**
     * \brief Special instance representing incorrect storage type
     */
    const MessageStorageType
    invalid_storageType = MessageStorageType::invalid();

    /**
     * File storage - slow and safe
     */
    const MessageStorageType
    persistent_storageType = MessageStorageType::builtin(BuiltinMessageStorage::Persistent);

    /**
     * \brief Memory storage - fast but dangerous
     */
    const MessageStorageType
    transient_storageType = MessageStorageType::builtin(BuiltinMessageStorage::Transient);

    /**
     * \brief Memory mapped file storage - fast and safe
     */
    const MessageStorageType
    persistentMM_storageType = MessageStorageType::builtin(BuiltinMessageStorage::PersistentMM);

    /**
     * \brief Sliced file storage - fast and safe.
     *
     * \details SplitStorage stores messages to the file of fixed size.
     * If size of the file has reached the limit, storage closes file
     * and creates new one. It allows to allocate storage on the RAM
     * drive and move chunks of the storage to HDD in parallel. RAM
     * drive should be connected to power supply to avoid data loss.
     */
    const MessageStorageType
    splitPersistent_storageType = MessageStorageType::builtin(BuiltinMessageStorage::SplitPersistent);

    /**
     * \brief Simple storage, that does not store anything - fast
     */
    const MessageStorageType
    null_storageType = MessageStorageType::builtin(BuiltinMessageStorage::Null);

    /**
     * \brief Configurable from engine.properties
     */
    const MessageStorageType
    default_storageType = MessageStorageType::default_();

    /**
     * \warning This instance is provided for backward compatibility and
     * is deprecated.
     */
    const MessageStorageType
    custom_storageType = MessageStorageType::custom();

    /**
     * \brief Special instance representing total number of storages
     * for iteration.
     *
     * \warning This instance provided for backward compatibility and
     * is deprecated. Iterate over StorageMgr registered instances
     * instead.
     */
    const MessageStorageType
    total_storageType = MessageStorageType::total();

    /** Enumeration of the recovery storage strategy */
    enum StorageRecoveryStrategyEnum
    {
        SRS_RECOVERY_NONE                    = 0x00000000,
        SRS_CREATE_NEW                       = 0x00000001,//create new storage 
        SRS_DEFAULT                          = SRS_RECOVERY_NONE, // default value
        SRS_TOTAL                            = 0x00000002,
        SRS_FORCE_DWORD                      = 0xFFFFFFFF
    };


    /** The priorities of the messages send receive operations*/
    enum SocketOpPriority {
        NA_SOCKET_OP_PRIORITY                           = 0, ///< Not initialized SocketOpPriority
        AGGRESSIVE_SEND_SOCKET_OP_PRIORITY              = 1, ///< Use dedicated thread to send messages
        AGGRESSIVE_RECEIVE_SOCKET_OP_PRIORITY           = 2, ///< Use dedicated thread to receive messages
        AGGRESSIVE_SEND_AND_RECEIVE_SOCKET_OP_PRIORITY  = 3, ///< Use dedicated thread to send and receive messages
        EVEN_SOCKET_OP_PRIORITY                         = 4, ///< Use thread pool to send/receive messages
        DIRECT_SEND_SOCKET_OP_PRIORITY                  = 8  ///< Try sending from the calling thread, use a thread pool if that would block
    };

    /** Set of the supported underlying protocols. */
    enum UnderlyingProtocol {
        FIX_TCP, ///< simple TCP socket with same application and session protocols.
                 ///< The FIXT.1.1 session protocol will be used for the FIX50 application protocol.
        FIXT11_TCP, ///< simple TCP socket with FIXT.1.1 session protocol
        FIXT11_FAST_TCP ///< FIX over FIXT.1.1 over FAST
    };

    /** Set of transports */
    enum Transport {
        SOCKETS,        ///< Kernel sockets
        MYRICOM_DBL,    ///< Myricom TA
        Transport_Last  
    };

    /** Meyer's case-insensitive comparision functor. */
    struct V12_API StrCaseInsComp : public std::binary_function< const char*, const char*, bool > {
        bool operator()( const char* lhs, const char* rhs ) const;
    };

    /** Map of pairs (key, value) for properties */
    typedef std::map< const char*, const char*, StrCaseInsComp > StringProperties;

    /** Set of FIX message types. */
    typedef std::set<std::string> MsgTypes;

    typedef std::vector<std::string> MultipleString;

    typedef std::vector<char> MultipleChar;

    /** Defines role of session */
    enum SessionRole {
        NA_SESSION_ROLE = 0,        ///< NA
        INITIATOR_SESSION_ROLE = 1, ///< Session is acceptor
        ACCEPTOR_SESSION_ROLE =2  ///< Session is initiator
    }; // enum SessionRole

    /** Collection of ip addresess */
    typedef std::vector<std::string> Addresses;

    typedef Utils::Emptily<bool> TriBool;
    typedef Utils::Emptily<int>  TriInt;
    typedef Utils::Emptily<System::u64>  TriUInt64;
    typedef Utils::Emptily<System::affinity64_t> TriAffinity64;
    typedef Utils::Emptily<Engine::MessageStorageType> TriMessageStorageType;
    typedef Utils::Emptily<StorageRecoveryStrategyEnum> TriStorageRecoveryStrategy;

    /** Unique protocol identifier
    * @deprecated Use Engine::ParserID instead.
    */
    typedef unsigned int ProtocolID;

    /** Unique parser identifier */
    typedef unsigned int ParserID;

    /** Unique dictionary identifier 
    * @deprecated Use ParserID instead.
    */
    typedef std::string DictionaryID;

    /** Represents parser name */
    typedef std::string ParserName;

    /** Invalid ParserID */
    static ParserID const INVALID_PARSER_ID = 0;

    /** Invalid ProtocolID
     * @deprecated Use INVALID_PARSER_ID instead.
     */
    static ProtocolID const INVALID_PROTOCOL_ID = INVALID_PARSER_ID;

    /** Last valid FIX parser identifier */
    static ParserID const TOTAL_PARSER_ID_COUNT = 100;

    /** Last valid FIX protocol identifier
     * @deprecated Use TOTAL_PARSER_ID_COUNT instead
     */
    static ProtocolID const TOTAL_PROTOCOL_ID_COUNT = TOTAL_PARSER_ID_COUNT;

    /** Assigns FIXVersion and id of the FIX dictionary */
    typedef std::map<FIXVersion, DictionaryID> FixVersionToProtocolName;

    typedef std::map< ParserID, ParserName > ParserIDToParserName;

    /** Index of the field in the PreparedMessage */
    typedef unsigned int PreparedFieldIndex;

    /** Defines engine port roles. */
    enum PortRole {
        NA_PORT_ROLE            = 0, ///< Not initialized PortRole
        COMMON_LISTEN_PORT      = 1, ///< port accepts common FIX sessions
        ADMIN_LISTEN_PORT       = 2, ///< port accepts admin FIX sessions
        SECURED_LISTEN_PORT     = 4  ///< port uses SSL encryption
    };

    class ListenPort
    {
        int port_;  ///< port value
        int roles_; ///< port roles
    public:
        /** Returns port value */
        int port() const { return port_; }

        /** Returns true if port accepts common FIX sessions */
        bool isCommon()  const { return !!(roles_ & COMMON_LISTEN_PORT); }

        /** Returns true if port accepts admin FIX sessions */
        bool isAdmin()   const { return !!(roles_ & ADMIN_LISTEN_PORT); }

        /** Returns true if port uses SSL encryption */
        bool isSecured() const { return !!(roles_ & SECURED_LISTEN_PORT); }

        /** Constructor */
        ListenPort(): port_(0), roles_(NA_PORT_ROLE) {}
        /** Constructor */
        ListenPort(int portNumber, int roles): port_(portNumber), roles_(roles) {}
    };

    typedef std::vector<ListenPort> ListenPorts;

    struct ParserDescription
    {
        ParserName parserName_;
        std::string scpProtocolName_;

        typedef std::vector<std::string> AppProtocolsNames;
        AppProtocolsNames appProtocolsNames_;

        ParserDescription() {}
        explicit ParserDescription( const std::string& templateStr );
        std::string toTemplateStr()const;
        void fillEmptyFieldsFrom( const ParserDescription& other );
    };

    /*
    * Interface class used to implement SSLContextConfigurator conversion to/from string and pass it to the engine.
    */
    class V12_API SSLContextConfiguratorConverter : public virtual Utils::ReferenceCounter
    {
    public:
        /*
        * Serialize configurator to string. Used when persisting FIX session. If configurator instance can't be serialized whatever reason
        * the routine have to return empty string. Empty string returned forces the engine to try serialize the configurator itself.
        *
        * @param configurator - the configurator to serialize.
        *
        * @param params - session parameters the configurator belongs to.
        *
        * @return String containing serialized configurator or empty string. The characters set allowed is the same as for properties/xml files.
        */
        virtual std::string toString( const System::SSLContextConfiguratorPtr& configurator, const SessionExtraParameters& params ) = 0;

        /*
        * De-serialize configurator from string. Used when restoring FIX session. If configurator can't be de-serialized whatever reason
        * the routine have to return emtpy SSLContextConfiguratorPtr object. It will force the engine to try de-serialize the configurator itself
        * or configure SSL from scratch using internal engine.properties based configurator on de-serialization failure.
        *
        * @param serializedConfigurator - serialized configurator.
        *
        * @param params - session parameters the configurator is deserialized for.
        *
        * @return New configurator instance restored or empty SSLContextConfiguratorPtr.
        */
        virtual System::SSLContextConfiguratorPtr fromString( const std::string& serializedConfigurator, const SessionExtraParameters& params ) = 0;
    };

    typedef Utils::ReferenceCounterSharedPtr<SSLContextConfiguratorConverter> SSLContextConfiguratorConverterPtr;

    /*
    * SSL configuration related properties holder for the engine.
    */
    struct SSLCustomContextProperties
    {
        SSLCustomContextProperties()
            : serverSocketConfigurator_()
            , converter_()
        {
        }
        /*
        * SSL context configurator for SSL server socket if any.
        */
        System::SSLContextConfiguratorPtr serverSocketConfigurator_;

        /*
        * Converter instance if any.
        */
        SSLContextConfiguratorConverterPtr converter_;
    };

    
    /*
    * Converts utc timestamp to local and vice versa.
    */
    class TimezoneConverter : public Utils::ReferenceCounter
    {
    public:
        /*
        * Converts UTC timestamp to local time.
        *
        * @param timestamp - UTC timestamp.
        *
        * @param tz - target timezone. Empty string means UTC.
        *
        * @return struct tm structure holding timestamp in target timezone.
        */
        virtual struct tm getLocalfromUTC( const time_t& timestamp, const std::string& tz ) const = 0;

        /*
        * Converts local time to UTC timestamp.
        *
        * @param timeTm - struct tm holding timestamp in source timezone.
        *
        * @param tz - source timezone. Empty string means UTC.
        *
        * @return UTC timestamp.
        */
        virtual time_t getUTCfromLocal( const struct tm& timeTm, const std::string& tz ) const = 0;

        /*
        * Validates timezone name.
        *
        * @param tz - timezone name to validate. Please note that empty string represents UTC timezone that is always valid.
        *
        * @return true if given timezone represents valid timezone or false otherwise.
        */
        virtual bool isValidTimezone( const std::string& tz ) const = 0;
    };
    typedef Utils::ReferenceCounterSharedPtr<TimezoneConverter> TimezoneConverterPtr;

    class PreparedMessage;
    class FIXGroup;
    class TagValue;
    class FixFieldsContainer;
    class Session;
    struct FIXFieldValue;
} // namespace Engine

#if defined(_MSC_VER)
#   pragma warning( pop )
#endif // defined(_MSC_VER)

#endif // _B2BITS_PubEngineDefines__h_

