/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
* $Revision: 1.6 $
*/

#include <iostream>
#include <stdio.h>
#include <time.h>
#include <stdexcept>

#ifndef _WIN32
#   include <signal.h>
#   include <sys/types.h>
#   include <unistd.h>
#   include <sys/stat.h>
#   include <syslog.h>
#endif // _WIN32

#include <B2BITS_V12.h>

#include "MySessionsManager.h"

using namespace Engine;
using namespace std;

void log( const char* apMsg );
void logError( const char* apMsg );

bool g_isDaemon = false;

const char* DAEMON_PROPERTY = "IS_DAEMON";

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    string aPropertiesFileName = "";

    if( argc < 2 ) {
        clog << "\nUsage: " << argv[0] << " CONFIG_FILE [ IS_DAEMON ] \n" << endl;
        aPropertiesFileName = "engine.properties";
    }

    if( argc > 1 ) {
        aPropertiesFileName = argv[1];
    }

    if( argc > 2 ) {
        g_isDaemon = true;
    }

    try {
        // Initialize FE
        FixEngine::init( aPropertiesFileName );

        // create and register SessionsManager
        MySessionsManager* mySM = new MySessionsManager();
        FixEngine::singleton()->registerSessionsManager( mySM );

        // create session-acceptor
        string sid = mySM->createAcceptor( "ESVR", "CTG", FIX44 );
        // create FIX message of type "Order"
        FIXMessage* pMessage = FIXMsgFactory::singleton()->newSkel( FIX44, "D" );

        // Makes unique identifier.
        char uniqueId[20];
#if 1400 != _MSC_VER
        sprintf( uniqueId, "%ld", time( NULL ) );
#else
        sprintf_s( uniqueId, 19, "%d", time( NULL ) );
#endif

        // Sets up values for message.
        pMessage->set( FIXField::ClOrdID, uniqueId );
        pMessage->set( FIXField::HandlInst,    "2" );
        pMessage->set( FIXField::Symbol,     "IBM" );
        pMessage->set( FIXField::OrderQty,   "200" );
        pMessage->set( FIXField::Side,         "1" );
        pMessage->set( FIXField::OrdType,      "5" );
        pMessage->set( FIXField::TimeInForce,  "0" );
        pMessage->set( FIXField::TransactTime,  "20030101-00:00:00" );
        mySM->put( sid, pMessage );


        log( "TrEchoServer was started." );

        if( ! g_isDaemon ) {
            cin.tie( &cout );
            string cmd;
            // show and process EchoServer operational menu
            while( "4" != cmd ) {
                cout << "Choose command:" << endl
                     << "1 - show TrEchoServer's statistics" << endl
                     << "2 - show FIX Engine's statistics" << endl
                     << "3 - close all active sessions" << endl
                     << "4 - stop TrEchoServer" << endl
                     << "5 - send new message" << endl
                     << "6 - switch session storage type" << endl
                     << ">";
                getline( cin, cmd );
                try {
                    if( "1" == cmd ) {
                        mySM->showStat();
                    } else if( "2" == cmd ) {
                        mySM->showEngineStat();
                    } else if( "3" == cmd ) {
                        mySM->closeSessions();
                    } else if( "5" == cmd ) {
#if 1400 != _MSC_VER
                        sprintf( uniqueId, "%ld", time( NULL ) );
#else
                        sprintf_s( uniqueId, 19, "%d", time( NULL ) );
#endif
                        pMessage->set( FIXField::ClOrdID, uniqueId );
                        mySM->put( sid, pMessage );
                    } else if( "6" == cmd ) {
                        string type = "persistent";

                        if( persistent_storageType ==
                            FixEngine::singleton()->getUnregisteredAcceptorSessionStorageType() ) {
                            FixEngine::singleton()->setUnregisteredAcceptorSessionStorageType( transient_storageType );
                            type = "transient";
                        } else {
                            FixEngine::singleton()->setUnregisteredAcceptorSessionStorageType( persistent_storageType );
                        }

                        cout << "Session storage switched to " << type << "." << endl;
                    }

                } catch( const std::exception& ex ) {
                    logError( ex.what() );
                }
            }
            cout << "Please wait..." << endl;
        } else {
#ifndef _WIN32
            pause();
#endif
        }

        // destroy SessionsManager
        FixEngine::singleton()->registerSessionsManager( NULL );
        mySM->closeSessions();
        delete mySM;

        // destruct FE
        FixEngine::destroy();

        log( "TrEchoServer was stopped." );
    } catch( const std::exception& ex ) {
        logError( ex.what() );
        return 1;
    }
    return 0;
}

void log( const char* apMsg )
{
    if( ! g_isDaemon ) {
        clog << apMsg << endl;
    } else {
#ifndef _WIN32
        syslog( LOG_NOTICE, "%s", apMsg );
#endif
    }
}

void logError( const char* apMsg )
{
    if( ! g_isDaemon ) {
        cerr << apMsg << endl;
    } else {
#ifndef _WIN32
        syslog( LOG_ERR, "%s", apMsg );
#endif
    }
}

