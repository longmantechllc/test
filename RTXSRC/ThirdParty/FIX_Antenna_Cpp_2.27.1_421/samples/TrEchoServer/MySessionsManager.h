/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
 * $Revision: 1.1 $
 */

#ifndef __MySessionsManager__h_
#define __MySessionsManager__h_

#include <B2BITS_V12.h>
#include <B2BITS_PubEngineDefines.h>

#include <iostream>
#include <map>

#include "MyApp.h"

class MySessionsManager : public Engine::SessionsManager
{
public:
    /*
     * Constructor
     * @param storageType - session's storage type
     */
    MySessionsManager();

    /*
     * Reimplemented from Engine::SessionsManager.
     * Processes unregistered acceptor event.
     */
    virtual bool onUnregisteredAcceptor( Engine::Session* apSn, const Engine::FIXMessage& logonMsg );

    /*
     * Closes all applications and releases resources.
     */
    void closeSessions();

    /*
     * Outputs applications statistic to stdout.
     */
    void showStat();

    /*
     * Outputs engine statistic to stdout.
     */
    void showEngineStat();

    /**
     * Creates new session
     * @return session ID
     */
    std::string createAcceptor( const std::string& sender, const std::string& target, Engine::FIXVersion ver );

    /** Sends message to a session */
    void put( const std::string& sid, Engine::FIXMessage* pMessage );

protected:

    /** Applications list */
    typedef std::map< std::string, MyApp* > MyApps;
    MyApps apps_;

    /** Mutex to make application list operations thread-safe */
    System::Mutex lock_;

};

#endif // #ifndef __MySessionsManager__h_
