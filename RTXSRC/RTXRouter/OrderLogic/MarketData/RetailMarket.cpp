//
// Created by schen on 4/23/2021.
//v

//#include <string>
#include <memory>
//#include <unordered_map>
//#include <fstream>
#include <vector>
#include <iostream>
#include <string.h>     //strpy
#include <chrono>

#include "../include/App.h"
//#include "../include/Price.h"

#include "RTXUtils.h"
#include "Config/ConfigGlobalBox.h"
#include "Config/Config.h"
#include "Logger.h"

#include "RetailMarket.h"


RetailMarketBox::RetailMarketBox(App* app)
        :m_config(app->m_config),
         m_logger(app->m_loggerSys)
{
}

RetailMarketBox::~RetailMarketBox()
{
    for(auto& bData : m_retailMarketList_B)
        bData.second.clear();
    for(auto& sData : m_retailMarketList_S)
        sData.second.clear();
}

void RetailMarketBox::InitRetailMarketBox()
{
    m_path = m_config->GlobalConfig()->retailRouterContraQuotefilePath;
    LoadRetailMarketBox();
}

void RetailMarketBox::InitRetailMarketBoxForTest(const std::string& file)
{
    m_path = file;
    LoadRetailMarketBox();
}

void RetailMarketBox::LoadRetailMarketBox()
{
    uint64_t start = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    m_logger->logInfo("RetailMarket file path: %s", m_path.c_str());
    m_stream.open(m_path.c_str(), std::ios_base::in);
    if (!m_stream.is_open())
    {
        std::string msg = "Error opening RetailMarket file " + m_path;
        std::cout << msg << std::endl;
        m_logger->logErr("%s", msg.c_str());
        return;
    }
    uint64_t start2 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();

    int cnt = 0;
    bool init = true;
    std::string lineBuf;
    while(m_stream.good())
    {
        getline(m_stream, lineBuf);
        if(lineBuf.empty())
            continue;
        if(init) {
            if(!ProcessHead(lineBuf)) {
                return;
            }
            init = false;
        }
        else {
            if (ProcessLine(lineBuf))
                ++cnt;
        }
    }
    m_stream.close();

    auto end2 = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    auto microseconds2 = end2 - start2;
    std::cout << "LoadRetailMarketBox, in: microseconds: " << microseconds2 << endl;

    m_logger->logInfo("RetailMarket: %d line loaded.", cnt);

    auto end = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::high_resolution_clock::now().time_since_epoch()).count();
    auto microseconds = end - start;
    std::cout << "LoadRetailMarketBox, out: microseconds: " << microseconds << endl << endl;

//    std::cout << std::endl << "m_retailMarketList_B:" << std::endl;
//    for(auto itr : m_retailMarketList_B)
//    {
//        printf("symbol:%s, \n",  itr.first.c_str());
//        for(auto rtl : itr.second)
//        {
//            printf("mmid:%s, pfof:%0.4f, pi:%0.4f, size:%d, timeStamp:%ld\n", rtl.mmid.c_str(), rtl.pfof.ToDouble(), rtl.pi.ToDouble(), rtl.size, rtl.timeStamp);
//        }
//        std::cout << std::endl;
//    }
//    std::cout << std::endl << "m_retailMarketList_S:" << std::endl;
//    for(auto itr : m_retailMarketList_S)
//    {
//        printf("symbol:%s, \n",  itr.first.c_str());
//        for(auto rtl : itr.second)
//        {
//            printf("mmid:%s, pfof:%0.4f, pi:%0.4f, size:%d, timeStamp:%ld\n", rtl.mmid.c_str(), rtl.pfof.ToDouble(), rtl.pi.ToDouble(), rtl.size, rtl.timeStamp);
//        }
//        std::cout << std::endl;
//    }

}

bool RetailMarketBox::ProcessHead(const std::string& line)
{
    char list[512];
    strcpy(list, line.c_str());

    int cnt = 0;
    std::string mmid;

    char *token = strtok(list, "|");
    while (token != nullptr)
    {
        ++cnt;
        std::string tk = token;
        RTXUtils::trimString(tk);
        mmid = tk;
        if(!mmid.empty())
            mmidList.emplace_back(mmid);

//        std::cout << "mmid:" << mmid  << ", cnt:" << cnt <<std::endl;
        token = strtok(nullptr, "|");
    }
    if(mmidList.empty()) {
        m_logger->logInfo("RetailMarketBox::ProcessHead fail to load RetailMarket file: %s", m_path.c_str());
        return false;
    }
//    std::cout << "Head: " ;
//    for(auto i : mmidList )
//        std::cout << i  << ", ";
//    std::cout << std::endl << std::endl;

    return true;
}

bool RetailMarketBox::ProcessLine(const std::string& line)
{
    char list[512];
    strcpy(list, line.c_str());

//    std::cout << "xxxProcessLine:" << std::endl;
    int cnt = 0;
    std::string symbolSection;
    std::string symbol;
    std::string side;
    std::vector<std::string> segmentList;
    char *token = strtok(list, "|");
    while (token != nullptr)
    {
        std::string tk = token;
        RTXUtils::trimString(tk);
        if(cnt > 0)
            segmentList.push_back(tk);
        else
            symbolSection = tk;

//        std::cout << "1. symbolSection:" << symbolSection   << ", segment:" << tk  << ", --- cnt:" << cnt <<std::endl;
        ++cnt;
//        std::cout << "2. symbol:" << symbol   << ", segment:" << segment  << ", --- cnt:" << cnt <<std::endl;
        token = strtok(nullptr, "|");
    }
    ProcessSide(symbolSection, symbol, side);
    if(symbol.size() <= 0 || side.size() > 1)
        return false;

    //    std::cout << "ProcessLine side, any Side?  " << side << std::endl;
    if(side.empty())
    {
//        std::cout << "ProcessLine side empty:" << std::endl;
        auto itrB = m_retailMarketList_B.find(symbol);
        auto itrS = m_retailMarketList_S.find(symbol);
        if(itrB != m_retailMarketList_B.end() && itrS != m_retailMarketList_S.end())
            return false;
        else if(itrB != m_retailMarketList_B.end())
            side = "S";
        else if(itrS != m_retailMarketList_S.end())
            side = "B";
//        std::cout << "ProcessLine side empty, set side:" << side << std::endl;
    }
    else if(side == "B")
    {
//        std::cout << "ProcessLine side 'B'':" << std::endl;
        auto itrB = m_retailMarketList_B.find(symbol);
        if(itrB != m_retailMarketList_B.end())
        {
//            std::cout << "ProcessLine side 'B', dup" << std::endl;
            return false;
        }
    }
    else
    {
//        std::cout << "ProcessLine side 'S'':" << std::endl;
        auto itrB = m_retailMarketList_S.find(symbol);
        if(itrB != m_retailMarketList_S.end())
        {
//            std::cout << "ProcessLine side 'S', dup" << std::endl;
            return false;
        }
    }

    for(int i=0; i < segmentList.size(); ++i)
    {
        ProcessData(segmentList[i], symbol, side, i);
    }

//    std::cout << "m_retailMarketList_B" << std::endl;
//    for(auto data : m_retailMarketList_B)
//    {
//        std::cout << "Symbol:" << data.first<< std::endl;
//        for(auto dt : data.second)
//        {
//            std::cout << "mmid:" << dt.mmid << ", pi:" << dt.pi.ToDouble() << ", pfof:" << dt.pfof.ToDouble()
//                      << ", size:" << dt.size << ", timeStamp:" << dt.timeStamp << std::endl;
//        }
//    }
//    std::cout << std::endl << "m_retailMarketList_S" << std::endl;
//    for(auto data : m_retailMarketList_S)
//    {
//        std::cout << "Symbol:" << data.first<< std::endl;
//        for(auto dt : data.second)
//        {
//            std::cout << "mmid:" << dt.mmid << ", pi:" << dt.pi.ToDouble() << ", pfof:" << dt.pfof.ToDouble()
//                      << ", size:" << dt.size << ", timeStamp:" << dt.timeStamp << std::endl;
//        }
//    }
//    std::cout << std::endl << std::endl;

    return true;
}

bool RetailMarketBox::ProcessSide(const std::string& symbolSection, std::string& symbol, std::string& side)
{
//    std::cout << "ProcessSide" << std::endl;
    char list[256];
    strcpy(list, symbolSection.c_str());

    int cnt = 0;
    char *token = strtok(list, ",");
    while (token != nullptr)
    {
        std::string tk = token;
        RTXUtils::trimString(tk);
        if (!tk.empty()) {
            ++cnt;
            if (cnt == 1) {
                symbol = tk;
            } else if (cnt == 2) {
                side = tk;
            }
        }
        token = strtok(nullptr, ",");
    }
//    std::cout << "ProcessSide:: symbol:" << symbol << ", side:" << side << std::endl;
}

bool RetailMarketBox::ProcessData(const std::string& segment, const std::string& symbol, const std::string& side, const int count)
{
//    std::cout << "ProcessData" << std::endl;
    char list[256];
    strcpy(list, segment.c_str());

    int cnt = 0;
    Price pfof = Price(0.0);
    Price pi = Price(0.0);
    int size = 0;
    long timeStamp = 0;

    char *token = strtok(list, ",");
    while (token != nullptr) {
        std::string tk = token;
        RTXUtils::trimString(tk);
//        std::cout << tk << std::endl;
        if(!std::isdigit(tk[0])) {
            m_logger->logInfo("None digit: %s", tk.c_str());
            return false;
        }

        if (!tk.empty()) {
            ++cnt;
            if (cnt == 1) {
                pi = RTXUtils::StrToPrice(tk, 0);
            }
            else if (cnt == 2) {
                pfof = RTXUtils::StrToPrice(tk, 0);
            } else if (cnt == 3) {
                size = RTXUtils::StrToInt(tk, 0);
            } else {
                timeStamp = RTXUtils::StrToLint(tk, 0);
            }
//            std::cout << "tk:" << tk << ", cnt:" << cnt << std::endl;
        }
        token = strtok(nullptr, ",");
    }
//    if(size == 0 || timeStamp == 0)
//    {
//        pfof = Price(0.0);
//        pi = Price(0.0);
//        size = 0;
//        timeStamp = 0;
//    }

//    std::cout << "ProcessData:: symbol:" << symbol << ", count:" << count << ", mmidList[count]:" << mmidList[count] << std::endl;
//    auto data = std::make_shared<IOIMarketData>();
//    data->mmid = mmidList[count]; //, pfof, pi, size, 0, timeStamp);
//    data->pi = pi;
//    data->pfof = pfof;
//    data->size = size;
//    data->timeStamp = timeStamp;

    auto data = IOIMarketData();
    data.mmid = mmidList[count]; //, pfof, pi, size, 0, timeStamp);
    data.pi = pi;
    data.pfof = pfof;
    data.size = size;
    data.timeStamp = timeStamp;

    if(side.empty())
    {
        ProcessSideData(m_retailMarketList_B, symbol, data);
        ProcessSideData(m_retailMarketList_S, symbol, data);
    }
    else if(side == "B")
    {
        ProcessSideData(m_retailMarketList_B, symbol, data);
//        std::cout << "m_retailMarketList_B.size():" << m_retailMarketList_B.size() << std::endl;
    }
    else
    {
        ProcessSideData(m_retailMarketList_S, symbol, data);
//        std::cout << "m_retailMarketList_S.size():" << m_retailMarketList_S.size() << std::endl;
    }
    return true;
}

//bool RetailMarketBox::ProcessSideData(std::unordered_map<std::string /*symbol*/, std::vector<std::shared_ptr<IOIMarketData>>>& list, const std::string& symbol,  const std::shared_ptr<IOIMarketData>& data)
bool RetailMarketBox::ProcessSideData(std::unordered_map<std::string /*symbol*/, std::vector<IOIMarketData>>& list, const std::string& symbol,  const IOIMarketData& data)
{
//    std::cout << "ProcessSideData:" << symbol << std::endl;
    auto itr = list.find(symbol);
    if(itr == list.end())
    {
//        std::vector<std::shared_ptr<IOIMarketData>> vct;
        std::vector<IOIMarketData> vct;
        vct.emplace_back(data);
        auto pr = std::make_pair(symbol, vct);
        list.insert(pr);
    }
    else
    {
        itr->second.emplace_back(data);
    }
}
