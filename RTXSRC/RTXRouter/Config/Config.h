//
// Created by schen on 10/22/2020.
//

#ifndef RTXROUTER_CONFIG_H
#define RTXROUTER_CONFIG_H

class ConfigGlobalBox;
class ConfigRouterBox;
class ConfigSecIdfnsBox;
class ConfigUserBox;

class Config
{
public:
    Config();
    virtual ~Config();
    void InitConfigBox();

    ConfigGlobalBox* GlobalConfig() { return m_globalConfig; }
    ConfigRouterBox* RouterConfig() { return m_routerConfig; }
    ConfigSecIdfnsBox* SecIdfnsConfig() { return m_secIdfnsConfig; }
    ConfigUserBox* UserConfig() { return m_userUserConfig; }

private:
    ConfigGlobalBox* m_globalConfig;
    ConfigRouterBox* m_routerConfig;
    ConfigSecIdfnsBox* m_secIdfnsConfig;
    ConfigUserBox* m_userUserConfig;
};

#endif //RTXROUTER_CONFIG_H
