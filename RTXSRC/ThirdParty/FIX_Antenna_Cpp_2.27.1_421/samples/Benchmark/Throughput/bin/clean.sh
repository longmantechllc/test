#!/bin/sh

rm -f *.log
rm -f _log

if [ "$1" == "sender" ]
then
    rm -f logs.sender/*.in logs.sender/*.out logs.sender/*.log logs.sender/*.conf logs/_log
else
    rm -f logs.receiver/*.in logs.receiver/*.out logs.receiver/*.log logs.receiver/*.conf logs/_log
fi

