// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_UTCDateOnly.h
/// Contains Engine::UTCDateOnly class definition

#ifndef B2BITS_FIX_TYPES_UTCDateOnly_H
#define B2BITS_FIX_TYPES_UTCDateOnly_H

#include <string>
#include <iosfwd>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_String.h>


namespace Engine
{

    /// \brief Encapsulates UTCDateOnly FIX type
    class V12_API UTCDateOnly
    {
    public:

        /// Represents storage type for date-time value.
        typedef System::u32 value_type;

        /// Maximum buffer size required to store value
        static unsigned int const ValueSize = sizeof( "YYYYMMDD" ) - 1;

        /// Creates zero object.
        UTCDateOnly() throw();

        /// Constructor. Constructs object from year, month and day values.
        ///
        /// @param year Year
        /// @param month Month in range [1, 12]
        /// @param day Day in range [1, 31]
        UTCDateOnly( int year, int month, int day ) throw();

        /// Equal operator
        bool operator == ( const UTCDateOnly& v ) const throw();

        /// Not equal operator
        bool operator != ( const UTCDateOnly& v ) const throw();

        /// Greater operator
        bool operator > ( const UTCDateOnly& v ) const throw();

        /// Greater or equal operator
        bool operator >= ( const UTCDateOnly& v ) const throw();

        /// Less operator
        bool operator < ( const UTCDateOnly& v ) const throw();

        /// Less or equal operator
        bool operator <= ( const UTCDateOnly& v ) const throw();

        /// Returns year part of value.
        int year() const throw();

        /// Returns month part of value.
        int month() const throw();

        /// Returns day part of value.
        int day() const throw();

        /// Constructs object from FIX string.
        static UTCDateOnly fromFixString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMMDD).
        static UTCDateOnly fromDecimal( System::u32 val ) throw();

        /// Constructs object from decimal date format (YYYYMMDD).
        static UTCDateOnly fromDecimalString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMMDD).
        static UTCDateOnly fromString( AsciiString val ) throw();

        /// Converts value stored in object to FIX string 'YYYYMMDD'.
        std::string& toFixString( std::string* dest ) const;

        /// Converts stored value to FIX string 'YYYYMMDD'.
        ///
        /// @param buf Memory buffer enough to store string like YYYYMMDD
        /// @return pointer to the first character of the result. Last character of result is last character of the 'buf' parameter.
        char* toFixString( char* buf, char* end ) const throw();

        /// Returns value in decimal format (YYYYMMDD)
        System::u32 toDecimal() const throw();

        /// Returns UTCTimestamp with current date
        static UTCDateOnly now() throw();

    private:
        UTCDateOnly( value_type v ) throw() : v_( v ) {}

    private:
        value_type v_;
    };

    inline bool UTCDateOnly::operator == ( const UTCDateOnly& v ) const throw()
    {
        return v_ == v.v_;
    }

    inline bool UTCDateOnly::operator != ( const UTCDateOnly& v ) const throw()
    {
        return v_ != v.v_;
    }

    inline bool UTCDateOnly::operator < ( const UTCDateOnly& v ) const throw()
    {
        return v_ < v.v_;
    }

    inline bool UTCDateOnly::operator <= ( const UTCDateOnly& v ) const throw()
    {
        return v_ <= v.v_;
    }

    inline bool UTCDateOnly::operator > ( const UTCDateOnly& v ) const throw()
    {
        return v_ > v.v_;
    }

    inline bool UTCDateOnly::operator >= ( const UTCDateOnly& v ) const throw()
    {
        return v_ >= v.v_;
    }

    /// \brief Writes a UTCDateOnly into the output stream.
    extern V12_API std::ostream& operator << ( std::ostream& s, const UTCDateOnly& v );
}

#endif
