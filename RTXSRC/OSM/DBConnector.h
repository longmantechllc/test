#pragma once

#include <mutex>
#include <string>
#include "mysqlx/xdevapi.h"
#include <iostream>
#include <atomic>
#include <unordered_map>
#include <tuple>
#include "DBRecord.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "LockFreeQueue.h"
#include "OSMOrder.h"


class DBConnector {
public:
    DBConnector(std::string hostName, unsigned port, std::string user, std::string pwd,
                LockFree::LockFreeQueue<tblState>& stateQ,
                LockFree::LockFreeQueue<tblChildOrder>& childQ,
                LockFree::LockFreeQueue<tblParentOrder>& parentQ,
                LockFree::LockFreeQueue<tblExecution>& exeQ,
                bool logFlag):
            m_hostName{hostName}, m_user{user}, m_pwd{pwd}, m_portNum{port},
            m_stateQ(stateQ), m_childQ(childQ), m_parentQ(parentQ), m_exeQ(exeQ),
            m_logFlag(logFlag)
            { }
    bool init();
    template<typename T>
    bool addRecord(const T& rec) {
        try {
            add(rec);
        }
        catch (const mysqlx::Error &err) {
            spdlog::error("add record failed {}", err.what());
            return false;
        }
        catch(...) {
            spdlog::error("add record failed");
            return false;
        }
        return true;
    }
    bool running() const { return m_canRun;}
    void setRunning(bool run) { m_canRun = run;}
    void processingMessage();
    std::tuple<unsigned, unsigned, unsigned> getMaxIds(const std::string& instanceName);
    void getOpenOrders(const std::string& instanceName, std::unordered_map<int, OSMRecoveredOrder>& recoveryPOrders,
                       std::unordered_map<std::string, RecoveredGWOrder>& recoveryCOrderMap);

private:
    void add(const tblState& rec);
    void add(const tblChildOrder& rec);
    void add(const tblExecution& rec);
    void add(const tblParentOrder& rec);

    std::string m_hostName, m_user, m_pwd;
    unsigned m_portNum;

    std::unique_ptr<mysqlx::Session> m_sess;
    std::unique_ptr<mysqlx::Schema> m_db;
    std::unique_ptr<mysqlx::Table> m_state, m_parent, m_child, m_execution, m_recovery, m_maxIds;

    std::atomic_bool m_canRun {true};
    LockFree::LockFreeQueue<tblState>& m_stateQ;
    LockFree::LockFreeQueue<tblChildOrder>& m_childQ;
    LockFree::LockFreeQueue<tblParentOrder>& m_parentQ;
    LockFree::LockFreeQueue<tblExecution>& m_exeQ;
    std::array<uint32_t, 4> m_tblRecordNum = {0,0,0,0}; // no need for atomic here
    bool m_logFlag {false};
    uint64_t m_lastTime {0};
};