#!/bin/bash

source settings.sh

export ListenPort=65535
export Role=client

if [ ! -f /logs.client  ]; then
    mkdir logs.client
fi

if [ ! -f /logs.client/backup  ]; then
    mkdir logs.client/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh client
	
	if [ "$OutputFile" = "" ]
	then 
		./Client $ServerAdress $ServerPort conf engine.optimized.hard.spin.properties
	else
		./Client $ServerAdress $ServerPort conf engine.optimized.hard.spin.properties >> $OutputFile
	fi
	sleep 10
done
