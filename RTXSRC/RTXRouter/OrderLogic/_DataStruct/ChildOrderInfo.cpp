//
// Created by schen on 12/4/2020.
//

#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <vector>
#include <string>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "ChildOrderInfo.h"

void ChildOrderInfo::insert( const std::string& coid, std::shared_ptr< ChildOrder > childOrder )
{
    auto it = m_childOrderList.find( coid );
    if ( it != m_childOrderList.end() )
        m_childOrderList.erase(it);
    auto pr = std::make_pair(coid, childOrder);
    m_childOrderList.insert(pr);
}

std::shared_ptr< ChildOrder > ChildOrderInfo::GetChildByCoid( const std::string& coid )
{
    auto it = m_childOrderList.find( coid );
    if ( it != m_childOrderList.end() )
        return (*it).second;
    else
        return m_tpChild;
}

void ChildOrderInfo::RemoveChildByCoid(const std::string& coid)
{
    auto it = m_childOrderList.find( coid );
    if ( it != m_childOrderList.end() )
        m_childOrderList.erase(it);
}