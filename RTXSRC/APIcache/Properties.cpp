
#include "Properties.h"

#include <iostream>
#include <utility>
#include <cstring>
#include <cstdlib>

using namespace std;
//using namespace Utils;
using namespace Aux;


const char* const Aux::Properties::INITIATOR_CFG = "FixSessions";
const char* const Aux::Properties::PORT = "Port";
const char* const Aux::Properties::PRELOAD_FILE = "PreloadFile";
const char* const Aux::Properties::MMID = "MMids";
const char* const Aux::Properties::RLP_IDS = "RLPids";
const char* const Aux::Properties::CLIENT_NAME = "ClientName";
const char* const Aux::Properties::SYMBOL_FILE = "SymbolFile";
const char* const Aux::Properties::LOGGING = "Logging";

Properties::Properties( std::istream* apIStream )
{
    apIStream->seekg( 0, ios::beg );

    string s;
    while( getline( *apIStream, s ) ) {
        // skip comments
        if( 0 == s.size() || '#' == s[0] || '!' == s[0] ) {
            continue;
        }

        // skip empty lines
        if( string::npos == s.find_first_not_of( " \t\r" ) ) {
            continue;
        }

        const char separators[] = "=:, \t";
        bool done = false;
        string::size_type pos = 0;
        string::size_type endOfKey = 0;
        do {
            endOfKey = s.find_first_of( separators, pos );
            if( string::npos != endOfKey ) {
                if( ( endOfKey > 0 ) && ( '\\' == s.at( endOfKey - 1 ) ) ) {
                    pos = endOfKey + 1;
                    continue;
                }
            }
            done = true;
        } while( ! done );

        if( string::npos != endOfKey ) {
            string key = s.substr( 0, endOfKey );

            string::size_type slashPos = key.find( "\\" );
            while( string::npos != slashPos ) {
                key.erase( slashPos, strlen( "\\" ) );
                slashPos = key.find( "\\" );
            }

            string::size_type beginOfWord = s.find_first_not_of( separators, endOfKey );
            string value( "" );

            if( string::npos != beginOfWord ) {
                value = s.substr( beginOfWord, s.size() - beginOfWord );
                if( '\r' == value.at( value.size() - 1 ) ) {
                    value.resize( value.size() - 1 );
                }
            }

            slashPos = key.find( "\\" );
            while( string::npos != slashPos ) {
                if( ( slashPos == ( value.size() - 1 ) ) || ( '\\' != ( value.at( slashPos + 1 ) ) ) ) {
                    value.erase( slashPos, strlen( "\\" ) );
                    slashPos = value.find( "\\" );
                } else {
                    slashPos++;
                }
            }

            pair<MapType::iterator, bool> ret = m_pairs.insert( make_pair( key, value ) );
            if( ! ret.second ) {
                throw string( "Invalid property's line: \"") + s + "\", this key has been defined earlier." ;
            }
        } else {
            throw string( "Invalid property's line: \"") + s + "\", can't find separator." ;
        }
    }
}

std::string Properties::getString( const std::string& key ) const
{
    MapType::const_iterator it = m_pairs.find( key );

    if( m_pairs.end() != it ) {
        return it->second;
    } else {
        throw string( "Cannot find the '") + key + "' property" ;
    }
}

bool Properties::isPropertyExists( const std::string& key )const
{
    return m_pairs.end() != m_pairs.find( key );
}

int Properties::getInteger( const std::string& key ) const
{
    return atoi( getString( key ).c_str() );
}

bool Properties::getBool( const std::string& key ) const
{
    string val = getString( key );
    if( "true" == val ) {
        return true;
    } else if( "false" == val ) {
        return false;
    } else {
        throw string("Unknown value (") + val + ") of the '" + key + "' parameter" ;
    }
}
