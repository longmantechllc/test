#include <iostream>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included
#include "APIcacheSimulator.h"

using namespace ::std;

void APIcacheSimulator::loadDataFile() {
    ifstream in(params_.symbol_file.c_str());
    if (!in) {
        spdlog::error("Cannot open preload symbol file {}", params_.symbol_file);
        return;
    }

    std::string line;
    while (std::getline(in, line)) {
        if (line.size()) {
            if (line[0] == '#')
                continue;
            boost::algorithm::trim(line);
            if (auto p = line.find(','); p != string::npos) {
                auto symbol = line.substr(0, p);
                double prc = stod(line.substr(p+1));
                spdlog::info("symbol {}, avgPrc {}", symbol, prc);
                m_symbols.emplace_back(make_pair(symbol, prc));
            }
        }
    }
    spdlog::info("read in {} symbols", m_symbols.size());
}

APIcacheSimulator::APIcacheSimulator(const ApplicationProperties& params)
    : params_(params)
{
    try {
        spdlog::info("initiator_cfg: ", params.initiator_cfg);
        FIX::SessionSettings settings( params.initiator_cfg );
        FIX::FileStoreFactory storeFactory( settings );
        FIX::FileLogFactory logFactory( settings );
        m_initiator = make_unique<FIX::SocketInitiator>( *this, storeFactory, settings, logFactory);
        m_initiator->start();
        spdlog::info("connecting to APIcache ...");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}",e.what());
        return;
    }

    // loading test data file
    if (!params_.symbol_file.empty()) {
        loadDataFile();
        m_randomizerThread = std::thread([this]{
            spdlog::info("start randomizer thread");
            std::this_thread::sleep_for(std::chrono::seconds(3));

            const auto N = m_symbols.size();
            srand((unsigned) time(0));

            while (!m_randomizerStop ) {
                if (m_sessions.size()) {
                    int idx = rand() % m_sessions.size();
                    randomIoI(rand() % N, idx);
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(params_.interval));
            }
        });
    }

    m_init = true;
}

APIcacheSimulator::~APIcacheSimulator()
{
    m_randomizerStop = true;
    m_initiator->stop();
    spdlog::info("stop simulator sessions");
    std::this_thread::sleep_for(std::chrono::seconds(2));

    if (m_init) {
        if (!params_.symbol_file.empty())
            m_randomizerThread.join();
        spdlog::info("all threads are shut down");
    }
}

void APIcacheSimulator::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void APIcacheSimulator::onLogon( const FIX::SessionID& sessionID )
{
    spdlog::info("Logon - {}", sessionID);
    m_sessions.push_back(sessionID);
    m_ioiIds.push_back({});
}

void APIcacheSimulator::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("Logout - {}", sessionID);
    if (auto itor = find(m_sessions.begin(), m_sessions.end(), sessionID); itor != m_sessions.end())
        m_sessions.erase(itor);
}

void APIcacheSimulator::toAdmin( FIX::Message& message, const FIX::SessionID& )
{
    spdlog::info("Admin OUT: {}", message);
}

void APIcacheSimulator::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("Admin IN: {}", message);
}

void APIcacheSimulator::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("IN: {}", message);
    crack( message, sessionID );
}

void APIcacheSimulator::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("OUT: {}", message);
}
namespace {
    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}

void APIcacheSimulator::randomIoI(int i, int j) {
    static bool buy{};
    static uint64_t next_id {};

    FIX44::IOI msg;
    auto symbol = m_symbols[i].first;
    auto& sessionId = m_sessions[j];
    spdlog::info("simulating @symbol {} on session {}", symbol, sessionId.getSenderCompID().getValue());
    FIX::IOITransType ioiTransType;
    try  {
        auto itor = m_ioiIds[j].find(make_pair(symbol, buy));
        if (itor == m_ioiIds[j].end()) {
            ioiTransType = FIX::IOITransType_NEW;
            spdlog::info("New API Msg");
        }
        else {
            int i = rand() % 6;
            ioiTransType = (i < 5? FIX::IOITransType_REPLACE:FIX::IOITransType_CANCEL);
            msg.set(FIX::IOIRefID(to_string(itor->second)));
            spdlog::info("{} API Msg", (i < 5? "Replace" : "Cancel"));
        }

        msg.set(ioiTransType);
        msg.set(FIX::IOIID(to_string(++next_id)));
        msg.set(FIX::Symbol(symbol));
        msg.set(FIX::Side(buy? FIX::Side_BUY : FIX::Side_SELL));

        msg.setField(FIX::FIELD::IoiPi, to_string(((rand() % 10 +1) * m_symbols[i].second) / 10000.0));
        msg.setField(FIX::FIELD::IoiPfof, to_string(((rand() % 91 +10) * m_symbols[i].second) / 10000.0));
        if (ioiTransType ==  FIX::IOITransType_CANCEL) {
            msg.set(FIX::IOIQty("0"));
            m_ioiIds[j].erase(itor);
        }
        else {
            msg.set(FIX::IOIQty(to_string((rand() % 6 +1) * 100)));
            m_ioiIds[j][make_pair(symbol, buy)] = next_id;
        }
        FIX::Session::sendToTarget(msg, sessionId);
        buy = !buy;

    } catch (std::exception& ex) {
        spdlog::error("Caught exception: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught unknown exception");
    }
}
