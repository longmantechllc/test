//
// Created by schen on 10/26/2020.
//

#ifndef RTXROUTER_CONFIGGLOBALLOADER_H
#define RTXROUTER_CONFIGGLOBALLOADER_H

class Logger;

class ConfigGlobalBox;
class ConfigLoader;

class ConfigGlobalLoader : public LoaderBase
{
public:
    ConfigGlobalLoader();
    ConfigGlobalLoader(Logger* logger);
    virtual ~ConfigGlobalLoader();
    void InitGlobalConfigLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node) override;

    void LoadTradingHalts(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadSecurityMaster(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadArgos(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadQuoteFilters(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadMarketDataFailureRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadCustomTifs(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadSecIdfnIdentifier(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadSyntheticSecIdfnIdentifier(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadOrderParam(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadRetailRouter(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadRetailRouterMmid(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadRetailRouterMMIDpIpfoF(ConfigLoader* loader, xercesc_3_2::DOMNode* node);
    void LoadLog(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void PrintGlobal(ConfigLoader* loader);

    std::unordered_map <std::string /*Name*/, std::function<void(ConfigLoader* loader, xercesc_3_2::DOMNode* node) > > globalFuncList;
};


#endif //RTXROUTER_CONFIGGLOBALLOADER_H
