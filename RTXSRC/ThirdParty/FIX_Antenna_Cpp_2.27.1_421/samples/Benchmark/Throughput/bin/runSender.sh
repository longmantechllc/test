#!/bin/bash


LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

if [ ! -f /logs.sender  ]; then
    mkdir logs.sender
fi

if [ ! -f /logs.sender/backup  ]; then
    mkdir logs.sender/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh sender
	
	if [ "$OutputFile" = "" ]
	then 
		./Sender engine.sender.properties benchmark.properties
	else
		./Sender engine.sender.properties benchmark.properties >> $OutputFile
	fi
	sleep 10
done
