/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "MessageDispatcher.h"
#include "B2BITS_V12.h"
#include "B2BITS_FIXEnums.h"
#include <algorithm>

using namespace b2bitscme;

void MessageQueue::queueMessage (const Engine::FIXMessage& message) {
	int size;
    char const* raw = message.toRaw( &size );
	std::string stringMsg (raw, size);
	int seqNum = message.getSeqNum();

	Utils::Guard<System::Mutex> lock(lock_);
	messages_[seqNum] = stringMsg;
}



void MessageQueue::extractMessages(const int oldValue, const int newValue, FIXMessageVector& vec) {
	Utils::Guard<System::Mutex> lock(lock_);
	
	if (newValue <= oldValue) {
		// this may occur if setInSeqNum or resetSeqNum are called. 
		messages_.clear();
		return;
	}

	// erase all messages with seqnums lower than new session seqnum. Probably these messages are already sent, or counterparty uses gap fill mode.
	FIXMessageCollection::iterator iter = messages_.lower_bound(newValue + 1);
	messages_.erase(messages_.begin(), iter);
		
	std::vector<int> seqnums;

	int continuosValue = newValue + 1;

	for (FIXMessageCollection::iterator it = messages_.begin(); it!= messages_.end(); ++ it) 	{
			
		if (continuosValue != it->first)
			break;

		seqnums.push_back(it->first);
		vec.push_back(it->second);

		++continuosValue;
	}
		
	// erase all queued items that  are  about to be passed to process method
	for (std::vector<int>::iterator it = seqnums.begin(); it != seqnums.end(); ++it) 	{
		messages_.erase(*it);
	}
	
}



MessageDispatcher::MessageDispatcher(IMessageProcessor* msgProcessor, bool processOutOfOrder)
	: processMessageOutOfOrder_ (processOutOfOrder)
	,msgProcessor_ (msgProcessor){
}


MessageDispatcher::~MessageDispatcher(void) {
}


void MessageDispatcher::dispatchMessage (const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
	if (processMessageOutOfOrder_ || !isOutOfOrder(fixMsg, aSn)){
		process(fixMsg, aSn, false);
	}
	else {
		queueMessage(fixMsg, aSn);
	}
}

void  MessageDispatcher::process(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn, bool isFromQueue) {
	msgProcessor_->process(fixMsg, aSn, isFromQueue);
}


bool MessageDispatcher::isOutOfOrder(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
	int msgSeqNum = fixMsg.getSeqNum();
	int snSeqNum = aSn.getInSeqNum();

	return msgSeqNum > snSeqNum;
}


void MessageDispatcher::queueMessage(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
	const std::string* snId = aSn.getId();

	MessageQueuePtr queue;

	{
		Utils::Guard<System::Mutex> lock(lock_);

		QueuesCollection::iterator iter = queues_.find(*snId);

		if (iter != queues_.end()) 	{
			queue = iter->second;
		}
		else {
			queue.reset(new MessageQueue(), false);
			queues_[*snId] = queue;
		}
	}
	
	queue->queueMessage(fixMsg);
}

void MessageDispatcher::onInSeqNumChanged(int oldValue, int newValue, const  Engine::Session& sn) {
	const std::string* snId = sn.getId();

	MessageQueuePtr queue;
	
	{
		Utils::Guard<System::Mutex> lock(lock_);

		QueuesCollection::iterator iter = queues_.find(*snId);
		
		if (iter == queues_.end())
			return;

		queue = iter->second;
	}

	FIXMessageVector vec;

	queue->extractMessages(oldValue, newValue, vec);

	for (FIXMessageVector::iterator msgIt = vec.begin(); msgIt != vec.end(); ++msgIt) {
		std::string message = *msgIt;

		std::auto_ptr<Engine::FIXMessage> FIXmsg ( Engine::FIXMsgProcessor::singleton()->parse(sn.parserID(), message));

		process(*FIXmsg.get(), sn, true);
	}

}