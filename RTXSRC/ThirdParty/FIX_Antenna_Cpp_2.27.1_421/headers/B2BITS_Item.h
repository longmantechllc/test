// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Item.h
/// Contains FixDictionary2::Item class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_Item_H
#define H_B2BITS_FixDictionary2_Item_H

#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_DictionaryDefines.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class Item
    *   @brief Represents Base interface for Dictionary element.
    */
    class V12_API Item : public Utils::ReferenceCounter
    {
    public:
        /** Dictionary Item Names */
        enum ItemName {
            ITEM_FIELD,           ///< Field item name
            ITEM_FIELDTYPE,       ///< Field type item name
            ITEM_FIELDREF,        ///< Field ref item name
            ITEM_REPEATINGGROUP,  ///< Repeating group item name
            ITEM_BLOCK,           ///< Block item name
            ITEM_BLOCKREF,        ///< BlockRef item name
            ITEM_VALBLOCK,        ///< Values block
            ITEM_MESSAGE,         ///< Message item name
            ITEM_PROTOCOL,        ///< Protocol item name
            ITEM_DICTIONARY       ///< Dictionary item name
        };

    public:
        /** Make copy of object */
        virtual ItemT::Ptr clone() const = 0;

        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() = 0;

        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        ItemName getItemName() const throw() {
            return itemName();
        }

        /** Try downcast to Protocol class
        *
        * @return downcast to Protocol class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual ProtocolT::Ptr toProtocol();

        /** Try downcast to Message class
        *
        * @return downcast to Message class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual MessageT::Ptr toMessage();

        /** Try downcast to Field class
        *
        * @return downcast to Field class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldT::Ptr toField();

        /** Try downcast to MessageItem class
        *
        * @return downcast to MessageItem class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual MessageItemT::Ptr toMessageItem();

        /** Try downcast to FieldType class
        *
        * @return downcast to FieldType class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldTypeT::Ptr toFieldType();

        /** Try downcast to RepeatingGroup class
        *
        * @return downcast to RepeatingGroup class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual RepeatingGroupT::Ptr toRepeatingGroup();

        /** Try downcast to FieldRef class
        *
        * @return downcast to FieldRef class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldRefT::Ptr toFieldRef();

        /** Try downcast to Block class
        *
        * @return downcast to Block class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual BlockT::Ptr toBlock();

        /** Try downcast to BlockRef class
        *
        * @return downcast to BlockRef class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual BlockRefT::Ptr toBlockRef();

        /** Try downcast to Protocol class
        *
        * @return downcast to Protocol class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual ProtocolT::CPtr toProtocol() const;

        /** Try downcast to Message class
        *
        * @return downcast to Message class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual MessageT::CPtr toMessage() const;

        /** Try downcast to MessageItem class
        *
        * @return downcast to MessageItem class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual MessageItemT::CPtr toMessageItem() const;

        /** Try downcast to Field class
        *
        * @return downcast to Field class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldT::CPtr toField() const;

        /** Try downcast to FieldType class
        *
        * @return downcast to FieldType class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldTypeT::CPtr toFieldType() const;

        /** Try downcast to RepeatingGroup class
        *
        * @return downcast to RepeatingGroup class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual RepeatingGroupT::CPtr toRepeatingGroup() const;

        /** Try downcast to FieldRef class
        *
        * @return downcast to FieldRef class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual FieldRefT::CPtr toFieldRef() const;

        /** Try downcast to Block class
        *
        * @return downcast to Block class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual BlockT::CPtr toBlock() const;

        /** Try downcast to BlockRef class
        *
        * @return downcast to BlockRef class
        *
        * @throw Utils::Exception if the conversion fails
        */
        virtual BlockRefT::CPtr toBlockRef() const;

    public:
        /** Default constructor */
        Item();

        /** Copy constructor */
        Item( Item const& obj );

        /** Assign operator */
        Item& operator=( Item const& obj );
    }; // class Item

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_Item_H
