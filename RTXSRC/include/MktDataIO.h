//
// Created by schen on 11/19/2020.
//

#ifndef RTXROUTER_MKTDATAIO_H
#define RTXROUTER_MKTDATAIO_H


#include <vector>
#include <optional>
#include <atomic>
#include <mutex>
#include <thread>

using namespace std;

class TopOfBook;
class Bbo;
class OpenTrade;

class Order;
class ExecutionReport;

class App;
class RTXOrder;
class RTXOrderIO;

class MktDataProcess;
class MDSubscriber;

#include "MDInterface.h"

class MktDataIO : public MDSubscriber
{
public:
    MktDataIO();
    MktDataIO(App*);
    virtual ~MktDataIO();

//Market data
    std::vector <std::shared_ptr<TopOfBook>> SubscribeTOB(const std::string& symbol, int oid, RTXOrder* rtxOrder);  //SOR call data server tob Function
    Bbo SubscribeBbo(const std::string& symbol, int oid);                       //SOR call data server bbo Function
    OpenTrade RequestOpeningTrade(const std::string& symbol, int oid);          //SOR call data server opening trade Function

    void OnTopOfBookUpdate( const BatchTOB& tobV);                              //data server call this function to send batch tob data to SOR
//    void OnTopOfBookUpdate( const TopOfBook& tob);                              //data server call this function to send tob data to SOR
    void UpdateBbo(const Bbo& bbo);                                             //data server call this function to send bbo data to SOR
    void UpdateOpeningTrade(const OpenTrade& openTrade);                        //data server call this function to send opening trade data to SOR

    void RemoveTOBSubscription(const std::string& symbol, int oid);             //SOR call data server Function to remove tob subscription
    void RemoveBboSubscription(const std::string& symbol);                      //SOR call data server Function to remove bbo subscription
    void CancelOpeningTradeRequest(const std::string& symbol);                  //SOR call data server Function to remove opening trade subscription ?

    App* m_app=nullptr;
    Logger* m_logger;                                                           //for system log
    std::mutex m_mutex;
    //    std::unordered_map<int, std::shared_ptr<RTXOrder>> m_orderList;

    MDInterface* m_mktDataProcess;
    void SetMktDataIO(MDInterface* mktDataProcess) { m_mktDataProcess = mktDataProcess; } //call from market data server ??

private:
    void AddRTXOrder(std::string symbol, int oid, RTXOrder* rtxOrder);          //internal function call
    void RemoveRTXOrder(std::string symbol, int oid);
    std::vector< RTXOrder* > GetReceivers(const std::string& symbol);

    std::unordered_map< std::string, std::unordered_map<int, RTXOrder*> > symbolOrderDict;
    std::unordered_map< std::string, atomic<int>> m_tobSymbolUserCount;
    std::unordered_map< std::string, atomic<int>> m_bboSymbolUserCount;
    std::unordered_map< std::string, atomic<int>> m_openTradeSymbolUserCount;

    std::vector<std::thread> mThreads;
    void PullMktData();
    void StartMktDataThread();
    void CloseMktDataThread();
    bool running_ = false;

//    std::unordered_map< std::string, std::unordered_set <int> > symbolOrderDict;
//    int m_tobCount;
//    int m_bboCount;
//    int m_openTradeCount;
};
#endif //RTXROUTER_MKTDATAIO_H
