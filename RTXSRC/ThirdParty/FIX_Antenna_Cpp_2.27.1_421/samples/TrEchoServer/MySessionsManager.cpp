/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
 * $Revision: 1.3 $
 */

#include "MySessionsManager.h"

#include <memory>
#include <algorithm>
#include <stdexcept>

#include "B2BITS_Guard.h"

using namespace std;
using namespace Engine;
using namespace System;
using namespace Utils;

typedef Guard<System::Mutex> MGuard;

MySessionsManager::MySessionsManager():
    apps_()
{
}

bool MySessionsManager::onUnregisteredAcceptor( Engine::Session* apSn, const FIXMessage& /*logonMsg*/ )
{
    apSn->addRef();
    const string* pSID = apSn->getId();
    // register application to session
    MyApp* pApp = new MyApp();
    apSn->registerApplication( pApp );
    // register session to application
    pApp->setSession( apSn );
    // put application to the internal map
    MGuard l( lock_ );
    MyApps::iterator it = apps_.find( *pSID );
    if( apps_.end() != it ) {
        delete it->second;
        it->second = pApp;
    } else {
        apps_.insert( MyApps::value_type( *pSID, pApp ) );
    }
    // accept the incoming connection
    return true;
}

void MySessionsManager::showStat()
{
    MGuard l( lock_ );
    int appNum( 1 );
    long sumAppMsgNum( 0 );
    cout << endl;
    for( MyApps::iterator i = apps_.begin(); i != apps_.end(); ++i ) {
        if( i->second->isSesClosed() ) {
            continue;
        }
        long curAppMsgNum = i->second->getNMsg();


        cout << "Session <" << *i->second->getSession()->getSender() << ","
             << *i->second->getSession()->getTarget() << ">: " << curAppMsgNum
             << " messages were sent" << endl;

        sumAppMsgNum += curAppMsgNum;

        ++appNum;
    }
    cout << "Sum: " << sumAppMsgNum << " messages" << endl << endl;
}

void MySessionsManager::showEngineStat()
{
    Statistics stat;
    FixEngine::singleton()->getStatistics( &stat );
    cout << endl << "Number of sessions:" << endl
         << "acceptors: " << stat.m_nAcceptors << endl
         <<  "active: " << stat.m_nActive << endl
         << "initiators: " << stat.m_nInitiators << endl
         << "correctly terminated: " << stat.m_nCorrectlyTerminated << endl
         << "non-gracefully terminated: " << stat.m_nNonGracefullyTerminated << endl << endl;
}

void MySessionsManager::closeSessions()
{
    MGuard l( lock_ );
    for( MyApps::iterator i = apps_.begin(); i != apps_.end(); ++i ) {
        i->second->close();
        i->second->waitUntilSesClosed();
        delete i->second;
    }
    apps_.clear();
}

std::string MySessionsManager::createAcceptor( const std::string& sender, const std::string& target, Engine::FIXVersion ver )
{
    // create application
    std::auto_ptr<MyApp> pApp( new MyApp() );
    // create session; throw exception if session cannot be created.
    SessionExtraParameters params;
    params.storageType_ = transient_storageType;
    Session* pSn = FixEngine::singleton()->createSession( pApp.get(), sender, target, ver, &params );
    const string* pSID = pSn->getId();
    assert( NULL != pSID );
    string sid = *pSID;
    // register session to the application
    pApp->setSession( pSn );
    // add Application to the internal list; List controls stored
    // instances of Application.
    MGuard l( lock_ );
    {
        apps_.insert( MyApps::value_type( sid, pApp.release() ) );
    }
    // connect session as acceptor
    pSn->connect();
    // return session id
    return sid;
}

void MySessionsManager::put( const string& sid, FIXMessage* pMessage )
{
    MGuard l( lock_ );
    MyApps::iterator it = apps_.find( sid );
    if( apps_.end() == it ) {
        throw std::logic_error( "Session with ID '" + sid + "' does not exists." );
    }
    it->second->put( pMessage );
}
