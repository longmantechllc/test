#!/bin/sh

# Cleanup logs
sh clean.sh

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

if [ ! -f /logs.receiver  ]; then
    mkdir logs.receiver
fi

if [ ! -f /logs.receiver/backup  ]; then
    mkdir logs.receiver/backup
fi

./Receiver_D engine.receiver.properties benchmark.properties

