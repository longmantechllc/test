//
// Created by schen on 4/23/2021.
//

#ifndef RTXROUTER_RETAILMARKET_H
#define RTXROUTER_RETAILMARKET_H

#include <string>
#include <fstream>
#include <unordered_map>
//#include <memory>
//#include <vector>

#include "../include/Price.h"
#include "../include/IOIInterface.h"

class App;
class Price;
class Config;
class Logger;

using namespace std;

//struct RetailMarketData
//{
//    RetailMarketData(){}
//    RetailMarketData(std::string mmidIn, Price pfofIn, Price piIn, int sizeIn, int priorityIn, long timeStampIn)
//            :mmid(mmidIn),
//             pfof(pfofIn),
//             pi(piIn),
//             size(sizeIn),
//             priority(priorityIn),
//             timeStamp(timeStampIn)
//    {
//    }
//    std::string mmid;
//    Price pi;
//    Price pfof;
//    int size;
//    int priority;
//    long timeStamp;
//};

class RetailMarketBox
{
public:
    RetailMarketBox(){}
    RetailMarketBox(App* app);

    ~RetailMarketBox();
    void InitRetailMarketBox();
    void LoadRetailMarketBox();
    bool ProcessHead(const std::string& line);
    bool ProcessLine(const std::string& line);
    bool ProcessSide(const std::string& symbolSection, std::string& symbol, std::string& side);
    bool ProcessData(const std::string& segment, const std::string& symbol, const std::string& side, const int count);
//    bool ProcessSideData(std::unordered_map<std::string /*symbol*/, std::vector<std::shared_ptr<IOIMarketData>>>& list, const std::string& symbol,  const std::shared_ptr<IOIMarketData>& data);
    bool ProcessSideData(std::unordered_map<std::string /*symbol*/, std::vector<IOIMarketData>>& list, const std::string& symbol,  const IOIMarketData& data);
    std::unordered_map<std::string /*symbol*/, std::vector<IOIMarketData>>& GetRetailMarketList(bool isBuy)
    {
        if(isBuy)
            return m_retailMarketList_S;
        else
            return m_retailMarketList_B;
    }
    void InitRetailMarketBoxForTest(const std::string& file);

private:
    Config* m_config;
    std::string m_path;
    std::ifstream m_stream;
    Logger* m_logger = nullptr;

    std::vector<std::string> mmidList;

//    std::unordered_map<std::string /*symbol*/, std::vector<std::shared_ptr<IOIMarketData>>> m_retailMarketList_B;
//    std::unordered_map<std::string /*symbol*/, std::vector<std::shared_ptr<IOIMarketData>>> m_retailMarketList_S;
    std::unordered_map<std::string /*symbol*/, std::vector<IOIMarketData>> m_retailMarketList_B;
    std::unordered_map<std::string /*symbol*/, std::vector<IOIMarketData>> m_retailMarketList_S;
};
#endif //RTXROUTER_RETAILMARKET_H
