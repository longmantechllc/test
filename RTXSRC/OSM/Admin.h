#pragma once

#include <string>
#include <memory>
#include <unordered_set>
#include <vector>
#include <string_view>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

class RTX_OSM;

class con_handler : public std::enable_shared_from_this<con_handler>
{
public:
    typedef std::shared_ptr<con_handler> pointer;
    // creating the pointer
    static pointer create(boost::asio::io_service& io_service, RTX_OSM& osm)
    {
        return pointer(new con_handler(io_service, osm));
    }
    //socket creation
    auto& socket()
    {
        return m_socket;
    }

    void start();
    void shutdown() {
        boost::system::error_code ec;
        m_socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, ec);
        m_socket.close();
    }
private:
    con_handler(boost::asio::io_service& io_service, RTX_OSM& osm): m_osm{osm}, m_socket (io_service)
    {}
    void handle_read(const boost::system::error_code& err, size_t bytes_transferred);
    void handle_write(const boost::system::error_code& err, size_t bytes_transferred);
    void processCommand() noexcept;
    std::string sendToOsm(const string_view& cmd, int idx, const string_view& param);
    boost::asio::streambuf buffer;
    static constexpr int max_length = 1024;
    std::string line;
    std::string message;
    RTX_OSM& m_osm;
    boost::asio::ip::tcp::socket m_socket;
    bool m_done{false};
};
// https://stackoverflow.com/questions/47913054/boostasioasync-write-ensure-only-one-outstanding-call#:~:text=boost%3A%3Aasio%3A%3Aasync_write%20-%20ensure%20only%20one%20outstanding%20call.%20According,operations%20that%20perform%20writes%29%20until%20this%20operation%20completes.%22
// https://www.codeproject.com/Articles/1264257/Socket-Programming-in-Cplusplus-using-boost-asio-T
class Admin {
public:
    Admin(int port, RTX_OSM *osm)
        : m_io{}
          , m_acceptor(m_io, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
          , m_osm(*osm)
    {
        //start_accept();
    }
    ~Admin() { stop(); }
    Admin(const Admin &) = delete;
    Admin(Admin &&) = delete;
    Admin & operator = (const Admin &) = delete;
    Admin & operator = (Admin &&) = delete;
    void OnConnectionClosed(con_handler::pointer connection);
    void start();
    void stop();
private:

    void start_accept();
    void handle_accept(con_handler::pointer connection, const boost::system::error_code& err);
    boost::asio::io_service m_io;
    boost::asio::ip::tcp::acceptor m_acceptor;
    std::unordered_set<con_handler::pointer> active_connections_;
    RTX_OSM& m_osm;
};

