// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SessionEvent.h
/// Contains Engine::SessionEvent classes declaration.

#ifndef _B2BITS_Session_Event__h_
#define _B2BITS_Session_Event__h_

#include <string>

#include "B2BITS_Session.h"
#include "B2BITS_CompilerDefines.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
    // A keyword was used that is not in the C++ standard, for example, one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
#endif

namespace Engine
{
    class FIXMessage;

    /**
    * A generic session-level event.
    */
    class V12_API SessionEvent
    {
    public:
        /**
        * Describes the event.
        */
        virtual const std::string* what() const = 0;

    protected:
        /** Destructor. */
        virtual ~SessionEvent();
    };

    /**
    * The Logon message has been received from the counterparty.
    */
    class V12_API LogonEvent : public SessionEvent
    {
    public:
        /** Defines list of possible logon response actions */
        enum LogonAction {
            AcceptWithConfirmLogon, ///< Accepting incoming logon with outgoing Logon message
            DisconnectWithLogout,   ///< Rejecting incoming logon with outgoing Logout message
            Disconnect              ///< Rejecting incoming logon
        };

        /**
         * Constructor.
         */
        LogonEvent( FIXMessage const* pLogonMsg,
                    FIXMessage* logonMessage,
                    FIXMessage* logoutMessage,
                    const std::string& reason );

        /**
         * Reimplemented from SessionEvent.
         *
         * @see SessionEvent
         */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
         * The received Logon message.
         */
        const FIXMessage* const m_pLogonMsg;

        /**
         * Logon action
         */
        mutable LogonAction logonAction_;

        /**
         * Outgoing Logon message. Used when logonAction_ equals to Engine::LogonEvent::AcceptWithConfirmLogon.
         * Field is available only for acceptor. In case of initiator it equals to NULL.
         */
        FIXMessage* const outgoingLogonMessage_;

        /**
         * Outgoing Logout message. Used when logonAction_ equals to Engine::LogonEvent::DisconnectWithLogout.
         * Field is available only for acceptor. In case of initiator it equals to NULL.
         */
        FIXMessage* const outgoingLogoutMessage_;

    private:
        /**
         * Copy Constructor, performs "deep copy".
         */
        LogonEvent( const LogonEvent& orig );
        LogonEvent& operator=( const LogonEvent& orig );

    private:
        std::string reason_;
    };

    /**
    * The session was closed.
    */
    class V12_API LogoutEvent : public SessionEvent
    {
    public:
        /** Constructor. */
        LogoutEvent( FIXMessage const* pLogoutMsg, const std::string& reason );

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
        * The received Logout message.
        *
        * @warning not NULL only if the session was closed by the remote side.
        */
        const FIXMessage* m_pLogoutMsg;

        /** 
         * Should be set to true to re-connect the session.
         * Moving will be performed in the parallel thread.
         *
         * @warning User should not set this flag to true at the application stop. 
         * Otherwise it will cause infinite loop.
         */
        mutable bool reconnectFlag_;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        LogoutEvent( const LogoutEvent& orig );
        LogoutEvent& operator=( const LogoutEvent& orig );

    private:
        std::string reason_;
    };

    /**
    * Incoming message sequence number is too low.
    * If message is processed on the application level, set InMsgSeqNumTooLowEvent::processed_ to true.
    */
    class V12_API InMsgSeqNumTooLowEvent : public SessionEvent
    {
    public:
        /** Constructor. */
        explicit InMsgSeqNumTooLowEvent( FIXMessage const* msg );

        /** Destructor */
        virtual ~InMsgSeqNumTooLowEvent();

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
        * The received Logout message.
        *
        * @warning not NULL only if the session was closed by the remote side.
        */
        const FIXMessage* const message_;

        /** 
         * If false [default], session will stoped; otherwise message will be ignored.
         * Output parameter.
         */
        mutable bool processed_;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        InMsgSeqNumTooLowEvent( InMsgSeqNumTooLowEvent const& );
        InMsgSeqNumTooLowEvent& operator=( InMsgSeqNumTooLowEvent const& orig );
    };

    /**
    * The unexpected message was received.
    */
    class V12_API UnexpectedMessageEvent : public SessionEvent
    {
    public:
        /** Constructor. */
        UnexpectedMessageEvent( FIXMessage const* pMsg, const std::string& reason );

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
        * The received message.
        */
        const FIXMessage* m_pUnexpMsg;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        UnexpectedMessageEvent( const UnexpectedMessageEvent& orig );
        UnexpectedMessageEvent& operator=( const UnexpectedMessageEvent& orig );

    private:
        std::string reason_;
    };


    /**
    * A SessionReject message was received.
    */
    class V12_API SessionLevelRejectEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        explicit SessionLevelRejectEvent( FIXMessage const* pRejectMsg );

        /**
         * The received SessionReject message
         */
        const FIXMessage* m_pRejectMsg;

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        SessionLevelRejectEvent( const SessionLevelRejectEvent& orig );
        SessionLevelRejectEvent& operator=( const SessionLevelRejectEvent& orig );

    private:
        static const std::string s_pReason;
    };

    /**
      * Encapsulates rejected application message.
      */
    class V12_API MsgRejectEvent : public SessionEvent
    {
    public:
        /** Constructor. Object doesn't own rejected message, it have to be cloned
          * @param pMsg2Reject - rejected message
          */
        MsgRejectEvent( FIXMessage const* pMsg2Reject );

        /** Reimplemented from SessionEvent. */
        virtual const std::string* what() const B2B_OVERRIDE;

        /** The rejected message. */
        const FIXMessage* m_pMsgReject;

    private:
        /** Copy Constructor, performs "deep copy". */
        MsgRejectEvent( const MsgRejectEvent& orig );
        MsgRejectEvent& operator=( const MsgRejectEvent& orig );

    private:
        static const std::string s_pReason;
    };

    /**
    * A Heartbeat message (MsgType = 0) with the TestReqID field (tag = 112) is received.
    *
     * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    class V12_API HeartbeatWithTestReqIDEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        explicit HeartbeatWithTestReqIDEvent( const FIXMessage* heartbeatMsg );


        /// Destructor.
        virtual ~HeartbeatWithTestReqIDEvent();

        /**
         * The Heartbeat message.
         */
        const FIXMessage* heartbeatMsg_;

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /// Returns the TestReqId field (tag = 112).
        const std::string& getTestReqID() const;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        HeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& orig );
        HeartbeatWithTestReqIDEvent& operator=( const HeartbeatWithTestReqIDEvent& orig );

    private:
        struct Impl;

        /// Implementation details.
        Impl* impl_;
    };

    /**
     * A Resend Request (MsgType = 2) has been received.
     */
    class V12_API ResendRequestEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        ResendRequestEvent( const FIXMessage* resendRequestMsg, int beginSeqNum, int endSeqNum, const Session& sn );

        /// Destructor.
        virtual ~ResendRequestEvent();

        /**
         * The Resend Request message.
         */
        const FIXMessage* resendRequestMsg_;

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent.
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
         * Returns the message sequence number of first record in range to be resent
         * (the BeginSeqNo field, tag = 7).
         */
        int getBeginSeqNo() const;

        /**
         * Returns the message sequence number of last record in range to be resent
         * (the EndSeqNo field, tag = 16).
         */
        int getEndSeqNo() const;

        /**
         * If true resend request will be processed according to the standard.
         * This is the default behaviour. If false resend request will be ignored
         * i.e. neither messages will be resent nor gap fill will be sent.
         * Output parameter. Default is true.
         * @warning Setting this parameter to false results in non-standard behaviour.
         * Please do not change this parameter unless you are absolutely sure you need
         * to change this behavior.
         */
        mutable bool processResendRequest_;

        /**
         * Overrides BeginSeqNo field in the request.
         * @warning Changing BeginSeqNo results in non-standard behaviour.
         * Please do not change it unless you are absolutely sure you need
         * to change this behavior. For onResendRequestEvent only.
         */
        void setBeginSeqNo(int newBeginSeqNo) const;

        /**
         * Overrides EndSeqNo field from the request.
         * @warning Changing EndSeqNo results in non-standard behaviour.
         * Please do not change it unless you are absolutely sure you need
         * to change this behavior. For onResendRequestEvent only.
         */
        void setEndSeqNo(int newEndSeqNo) const;

        /**
         * Defines sub range [BeginSeqNo..gapEndSeqNo]
         * that is filled with gap fill in the request.
         * @note Set gapEndSeqNo equal to EndSeqNo to reply whole range with gap fill. For onResendRequestEvent only.
         */
        void setGapEndSeqNo(int gapEndSeqNo) const;

        /**
         * Returns gapEndSeqNo defined by setGapEndSeqNo;
         */
        int getGapEndSeqNo() const;

        /**
         * Returns number of requested messages.
         */
        int getRequestSize() const;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        ResendRequestEvent( const ResendRequestEvent& orig );
        ResendRequestEvent& operator=( const ResendRequestEvent& orig );

    private:
        struct Impl;

        /// Implementation details.
        Impl* impl_;
    };


    /**
     * The session has changed its state.
     */
    class V12_API NewStateEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        NewStateEvent( const Session::State& oldState, const Session::State& newState );

        /// Destructor.
        virtual ~NewStateEvent();

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent.
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
         * Returns the previous session's state.
         */
        Session::State getOldState() const;

        /**
         * Returns the new session's state.
         */
        Session::State getNewState() const;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        NewStateEvent( const NewStateEvent& orig );
        NewStateEvent& operator=( const NewStateEvent& orig );

    private:
        struct Impl;

        /// Implementation details.
        Impl* impl_;
    };

    /**
     * The routing session wasn't found.
     */
    class V12_API UnableToRouteMessageEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        UnableToRouteMessageEvent( const FIXMessage* routingMsg, const std::string& deliverTo );

        /// Destructor.
        virtual ~UnableToRouteMessageEvent();

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent.
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
         * Returns message that couldn't be routed.
         */
        const FIXMessage* getRoutingMsg() const;

        /**
         * Returns session name where message have to be routed.
         */
        const std::string* getDeliverTo() const;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        UnableToRouteMessageEvent( const UnableToRouteMessageEvent& orig );
        UnableToRouteMessageEvent& operator=( const UnableToRouteMessageEvent& orig );

    private:
        const FIXMessage* routingMsg_;
        std::string deliverTo_;
        std::string what_;
    };

    /**
    * The sequence gap was detected in incoming messages.
    */
    class V12_API SequenceGapEvent : public SessionEvent
    {
    public:
        /**
         * Constructor.
         */
        SequenceGapEvent( FIXMessage const* pMsg, int expectedSeqNum );

        /** Expected MsgSeqNum */
        int m_expectedSeqNum;

        /** MsgSeqNum of the received message. */
        int m_receivedSeqNum;

        /** The received message */
        const FIXMessage* m_pMsg;

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /** 
        * If true, session will request lost message automatically by sending ResendRequest message; 
        * if false, it is user's responsibility to recover lost messages. 
        * If case of false, incoming message sequence number will be updated automatically.
        *
        * Default value is true.
        */
        bool mutable requestLostMessagesFlag_;

    private:
        /**
         * Copy Constructor, performs "deep copy".
         */
        SequenceGapEvent( const SequenceGapEvent& orig );
        SequenceGapEvent& operator=( const SequenceGapEvent& orig );

    private:
        std::string reason_;
    };

    /**
    * The session has changed its state.
    */
    class V12_API SwitchConnectionEvent : public SessionEvent
    {
    public:
        /**
        * Constructor.
        */
        SwitchConnectionEvent( Session::ActiveConnection oldState, Session::ActiveConnection newState );

        /// Destructor.
        virtual ~SwitchConnectionEvent();

        /**
        * Reimplemented from SessionEvent.
        *
        * @see SessionEvent.
        */
        virtual const std::string* what() const B2B_OVERRIDE;

        /**
        * Returns the previous session's state.
        */
        Session::ActiveConnection getPrev() const;

        /**
        * Returns the new session's state.
        */
        Session::ActiveConnection getNew() const;

    private:
        /**
        * Copy Constructor, performs "deep copy".
        */
        SwitchConnectionEvent( const SwitchConnectionEvent& orig );
        SwitchConnectionEvent& operator=( const SwitchConnectionEvent& orig );

    private:
        struct Impl;

        /// Implementation details.
        Impl* impl_;
    };


} // namespace Engine{

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // #ifndef _Session_Event__h_

