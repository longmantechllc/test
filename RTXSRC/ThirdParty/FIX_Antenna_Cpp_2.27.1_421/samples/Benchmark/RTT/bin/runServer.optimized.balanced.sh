#!/bin/bash

source settings.sh

export Role=server

if [ ! -f /logs.server  ]; then
    mkdir logs.server
fi

if [ ! -f /logs.server/backup  ]; then
    mkdir logs.server/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh server
	if [ "$OutputFile" = "" ]
	then 
		onload --profile=latency ./Server conf engine.optimized.balanced.onload.properties
	else
		onload --profile=latency ./Server conf engine.optimized.balanced.onload.properties >> $OutputFile
	fi
	sleep 5
done

