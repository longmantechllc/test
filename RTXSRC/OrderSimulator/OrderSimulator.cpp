#include <iostream>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included
#include "OrderSimulator.h"

using namespace ::std;

void OrderSimulator::loadDataFile() {
    ifstream in(params_.symbol_file.c_str());
    if (!in) {
        spdlog::error("Cannot open preload symbol file {}", params_.symbol_file);
        return;
    }

    std::string line;
    while (std::getline(in, line)) {
        if (line.size()) {
            if (line[0] == '#')
                continue;
            boost::algorithm::trim(line);
            /*
            if (auto p = line.find(','); p != string::npos) {
                auto symbol = line.substr(0, p);
                double prc = stod(line.substr(p+1));
                spdlog::info("symbol {}, avgPrc {}", symbol, prc);
                m_symbols.emplace_back(make_pair(symbol, prc));
            }
            */
            if (!line.empty())
                m_symbols.emplace_back(line);
        }
    }
    spdlog::info("read in {} symbols", m_symbols.size());
}

OrderSimulator::OrderSimulator(const ApplicationProperties& params)
    : params_(params)
{
    try {
        spdlog::info("initiator_cfg: ", params.initiator_cfg);
        FIX::SessionSettings settings( params.initiator_cfg );
        FIX::FileStoreFactory storeFactory( settings );
        FIX::FileLogFactory logFactory( settings );
        m_initiator = make_unique<FIX::SocketInitiator>( *this, storeFactory, settings, logFactory);
        m_initiator->start();
        spdlog::info("connecting to OSM ...");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}",e.what());
        return;
    }

    // loading test data file
    if (!params_.symbol_file.empty()) {
        loadDataFile();
        m_randomizerThread = std::thread([this]{
            spdlog::info("start randomizer thread");
            std::this_thread::sleep_for(std::chrono::seconds(3));

            const auto N = m_symbols.size();
            srand((unsigned) time(0));
            int count = 0;
            while (!m_randomizerStop ) {
                if (m_isLogon) {
                    // int idx = rand() % m_sessions.size();
                    // randomIoI(rand() % N, idx);
                    randomOrder(rand() % N);
                }
                std::this_thread::sleep_for(std::chrono::milliseconds(params_.interval));
                ++count;
                if (params_.burstAfter > 0 && count > params_.burstAfter) {
                    for(count = 0; count < N*params_.burstTimes; ++count) {
                        if (m_isLogon) {
                            randomOrder(rand() % N);
                            if (params_.burstInterval > 0) // sleep between the burst
                                std::this_thread::sleep_for(std::chrono::milliseconds(params_.burstInterval));
                        }
                    }
                    count = 0;
                }

            }
        });
    }

    m_init = true;
}

OrderSimulator::~OrderSimulator()
{
    m_randomizerStop = true;
    m_initiator->stop();
    spdlog::info("stop simulator sessions");
    std::this_thread::sleep_for(std::chrono::seconds(2));

    if (m_init) {
        if (!params_.symbol_file.empty())
            m_randomizerThread.join();
        spdlog::info("all threads are shut down");
    }
}

void OrderSimulator::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void OrderSimulator::onLogon( const FIX::SessionID& sessionID )
{
    spdlog::info("Logon - {}", sessionID);
    session_ = sessionID;
    m_isLogon = true;
    //m_sessions.push_back(sessionID);
    //m_ioiIds.push_back({});
}

void OrderSimulator::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("Logout - {}", sessionID);
    m_isLogon = false;
    //if (auto itor = find(m_sessions.begin(), m_sessions.end(), sessionID); itor != m_sessions.end())
    //    m_sessions.erase(itor);
}

void OrderSimulator::toAdmin( FIX::Message& message, const FIX::SessionID& )
{
    spdlog::info("Admin OUT: {}", message);
}

void OrderSimulator::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("Admin IN: {}", message);
}

void OrderSimulator::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("IN: {}", message);
    crack( message, sessionID );
}

void OrderSimulator::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("OUT: {}", message);
}

namespace {
    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}

void OrderSimulator::randomOrder(int i) {
    static uint64_t next_id {};

    try {
        FIX44::NewOrderSingle msg;
        msg.set(FIX::ClOrdID(to_string(++next_id)));
        msg.set(FIX::HandlInst(FIX::HandlInst_AUTOMATED_EXECUTION_ORDER_PRIVATE_NO_BROKER_INTERVENTION));
        msg.set(FIX::Symbol(m_symbols[i]));

        msg.set(FIX::TimeInForce(FIX::TimeInForce_DAY));
        msg.set(FIX::Side(rand() %2? FIX::Side_BUY:FIX::Side_SELL));
        msg.set(FIX::OrdType(rand() %10 == 9? FIX::OrdType_MARKET:FIX::OrdType_LIMIT));
        msg.set(FIX::OrderQty((rand() % params_.maxOrderSize + params_.minOrderSize) * 100));
        msg.set(FIX::Price(rand() %2? 1:5000));
        msg.set(FIX::TransactTime());
        // msg.set(FIX::MaxFloor(ord.displaySize_));
        // msg.set(FIX::MinQty(ord.minQty_));
        // msg.set(FIX::ExecInst( ord.execInst_));
        // msg.set(FIX::ExpireTime(ord.expireTime_));
        // msg.set(FIX::ExDestination(ord.exchange_)); 
        // msg.set(FIX::DiscretionOffsetValue(ord.discretion_.value()));
        // getFieldValueIf<FIX::Account>(msg, ord.account);
        if (params_.desks.size() && params_.logins.size()) {
            if (rand() %2)
                msg.getHeader().set(FIX::SenderSubID(params_.desks[rand()%params_.desks.size()]));
            else
                msg.getHeader().set(FIX::OnBehalfOfSubID(params_.logins[rand()%params_.logins.size()]));
        }
        else if (params_.desks.size()) {
            msg.getHeader().set(FIX::SenderSubID(params_.desks[rand()%params_.desks.size()]));
        } else if (params_.logins.size()) {
            msg.getHeader().set(FIX::OnBehalfOfSubID(params_.logins[rand()%params_.logins.size()]));
        }

        FIX::Session::sendToTarget(msg, session_);
    } catch (std::exception& ex) {
        spdlog::error("Caught exception: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught unknown exception");
    }
}


void OrderSimulator::onMessage( const FIX44::ExecutionReport& msg, const FIX::SessionID& ){
    spdlog::info("got ExecutionReport");
}