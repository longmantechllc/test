
#include <iostream>
#include <ctime>
#include <string_view>
#include "Utils/Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
// #include "Properties.h"
// #include "ApplicationProperties.h"
#include "MDCache.h"
#include "Version.h"
using namespace ::std;

/// Application entry point.
int main(int argc, char* argv[])
{

    try
    {
        // Create basic file logger (not rotated)
        auto my_logger = spdlog::basic_logger_mt("B", "logs/ftx_driver.log");

        // create a file rotating logger with 5mb size max and 3 rotated files
        //auto file_logger = spdlog::rotating_logger_mt("file_logger", "myfilename", 1024 * 1024 * 5, 3);
        spdlog::set_default_logger(my_logger);
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
        return -1;
    }

    spdlog::info("ftx_driver release version {} coming up...", ftx_version);

    // command line parameters
    std::string gw_properties_file = "ftx_driver.cfg";

    Helper::writeLineToConsole("ftx_driver © 2022\n");

    if (2 == argc) {
        gw_properties_file = argv[1];
    } else {
        Helper::writeLineToConsole("Usage: ftx_driver <properties file> \n");
        return 1;
    }

    try {
        spdlog::info("Init engine...");

        ifstream in(gw_properties_file.c_str());

        if (!in) {
            throw string("Cannot open file ").append(gw_properties_file);
        }

        Aux::Properties p(&in);
        ApplicationProperties appParams;
        Helper::parseCommandLine(p, &appParams);

        MDCache application(appParams);
        if (!application.isInited()) {
            spdlog::error("failed to initiate ftx_driver");
            return 0;
        }
        spdlog::info("ftx_driver starts");
        application.run();
        
        string cmd;
        while ("exit" != cmd) {
            cout << "Type 'exit' to exit > " << endl;
            getline(cin, cmd);
        }
        std::this_thread::sleep_for(std::chrono::seconds(1));
        cout << "application shutdown..." << endl;

    } catch (const std::exception& ex) {
        spdlog::error("ERROR: {}", ex.what());
        return -1;
    }
    catch(...) {
        return -1;
    }

    return 0;
}
