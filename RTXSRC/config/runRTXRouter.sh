## This scripts will execute RTX OSM. It will backup OSM log in backup folder. 

cd /home/hjoshi/RTX_DEMO/RTX_SOR_DEMO_QF/logs/backup
mkdir "$(date +"%Y%m%d_%H%M")"
cd "$(date +"%Y%m%d_%H%M")"
cp  ~/RTX_DEMO/RTX_SOR_DEMO_QF/logs/*.log .
#cp  ~/RTX_DEMO/RTX_SOR_DEMO_QF/logs/_log .
cd /home/hjoshi/RTX_DEMO/RTX_SOR_DEMO_QF/logs
##rm *
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_SOR_DEMO_QF/store
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_SOR_DEMO_QF/
#mv core.* crash/.
#if [ -f "core.*" ]; then mv core.* crash/.; fi
if ls core.* 1> /dev/null 2>&1; then
	mv core.* crash/.; fi
## set core dump
ulimit -S -c unlimited > /dev/null 2>&1 

#set path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# Run OSM
nohup ./RTX_Intelligent_Router RTX_Router.cfg </dev/null &>/dev/null &

