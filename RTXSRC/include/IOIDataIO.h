
#include"IOIInterface.h"

using namespace std;

class App;
class RTXOrder;
class App;
class Logger;

class IOIDataIO: public IOISubscriber
{
public:
    IOIDataIO();
    IOIDataIO(App*);
    virtual ~IOIDataIO();

    std::vector<IOIMarketData> SubscribeIOI(std::string symbol, bool isBuy);    //SOR call data server tob Function
    //void OnIOIUpdate(const IOIMarketData& ioiData);                             //OSM call this function to sendioi data to SOR

    App* m_app=nullptr;
    Logger* m_logger;                                                           //for system log

    void SetIOIInterface(IOIInterface* ioiInterface) { m_ioiInterface = ioiInterface; }    //call from OSM

};