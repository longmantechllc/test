// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Guard.h
/// Contains Utils::Guard class declaration.

#ifndef _B2BITS_GUARD__H
#define _B2BITS_GUARD__H


/// FIX Antenna Utils framework
namespace Utils
{

    /**
     * This template class employs a C++ idiom (described by Bjarne Stroustrup)
     * that uses the constructor to acquire a resource automatically
     * when an object of the class is created and uses the destructor
     * to release the resource automatically when it goes out
     * of scope. Since Guard is parameterized by the type of
     * lock (such as Mutex), this class can be used with a fam-ily
     * of synchronization wrappers that conform to a uniform
     * interface.
     * E.g.:
     * Guard<Mutex> guard (lock_); // guard constructor acquires m_lock.
     */
    template<class T>
    class Guard
    {
    public:
        /**Constructor. Acquires the given resource.
         * @param lock Resource monitor implementation
         */
        Guard( T& lock ) 
            : lock_( lock )
            , isActive_( true ) 
        {
            lock_.lock();
        };

        /**
         * Destructor. Releases the given resource.
         */
        ~Guard() {
            release();
        };

        /**
         * Releases the resource.
         */
        void release() {
            if( isActive_ ) {
                lock_.unlock();
                isActive_ = false;
            }
        };

    private:
        /** Copy constructor */
        Guard( const Guard& );

        /** Assignment operator */
        Guard& operator=( const Guard& );

    private:
        T& lock_;
        bool isActive_;
    };

    /**
     * Used to temporary unlock mutex
     * Unlocker<Mutex> unlocker (lock_); // guard constructor releases m_lock.
     */
    template<class T>
    class Unlocker
    {
    public:
        /**Constructor. Acquires the given resource.
         * @param lock Resource monitor implementation
         */
        Unlocker( T& lock ) : lock_( lock ) {
            lock_.unlock();
        }

        /**
         * Destructor. Releases the given resource.
         */
        ~Unlocker() {
            lock_.lock();
        };

    private:

        /** Copy constructor */
        Unlocker( const Unlocker& );

        /** Assignment operator */
        Unlocker& operator=( const Unlocker& );

    private:
        T& lock_;
    };


}
#endif // _B2BITS_GUARD__H

