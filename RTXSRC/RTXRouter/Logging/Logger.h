//
// Created by schen on 10/4/2020.
//
#ifndef RTXROUTER_LOGGER_H
#define RTXROUTER_LOGGER_H

#include <stdarg.h>
#include <fstream>
#include "AppConstants.h"

using namespace std;

using namespace AppConstants;

class Logger {
public:
    Logger();
    Logger(std::string& filePath, std::string& fileName, bool printHead=true, bool printTime=true)
    :m_filePath(filePath),
     m_fileName(fileName),
     m_printHead(printHead),
    m_printTime(printTime)
    {

    }
    virtual ~Logger();

    void OpenFile();
    void log(int level, const char* fmt, va_list& args);
    void logDirect(const char* fmt, ...);

    void logDebug(const char* fmt, ...);
    void logInfo(const char* fmt, ...);
    void logWarn(const char* fmt, ...);
    void logErr(const char* fmt, ...);
    void logFatal(const char* fmt, ...);


    std::string& GetFilePath() { return m_filePath; }
    std::string& GetFileName()  { return m_fileName; }
    bool GetSync()  { return m_flushAlways; }
    void FlushFile(){ m_stream.flush();}
    unsigned int GetStreamPosition() { return m_stream.tellp(); }

    void setMaximumFileSize( unsigned long maxFileSize );

    void Threshold(int level)
    {
        m_threshold = level;
    }

    int Threshold(void)
    {
        return m_threshold;
    }

    unsigned long m_fileSize = 0;
protected:
private:
    unsigned int formatLogLine(const char* message, char* timeBuffer, int maxBufferSize=AppConstants::LOG_BUFSIZE);
    void moveToNextLogFile();

    std::ofstream m_stream;
    std::string m_filePath;
    std::string m_fileName;
    bool m_printHead=true;
    bool m_printTime=true;
    unsigned long m_fileMaxSize = 500000000;
    int m_threshold = 1;
    bool m_flushAlways=true;
};

#endif //RTXROUTER_LOGGER_H
