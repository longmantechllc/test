

#pragma once

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <exception>

#include "spdlog/spdlog.h"
#include <libwebsockets.h>
#include <rapidjson/document.h>


using namespace std;

struct Book;
class MDCache;

class CryptoWebSocket {
public:
	CryptoWebSocket(const std::string& host, uint16_t port,
		uint16_t idx, const std::string& symbol,
		Book& book, MDCache * mdCache)
		: m_host{host}, m_port{port}, m_idx{idx}, m_symbol{symbol},
		  m_book{book}, m_cache{mdCache} {

	}
	CryptoWebSocket( CryptoWebSocket&& rhs) = default;

	~CryptoWebSocket() {
		spdlog::error( "destroy websocket for idx {}, symbol {}",
			m_idx, m_symbol); 
		lws_context_destroy( context );
	}
	void connect_endpoint(const char* path);
	void subscribe() {

		auto subs = this->build_subscription_msg();
        for(auto& req : subs) {
            this->send_message(req);
        }
	}
	std::vector<std::string> build_subscription_msg();
	void init();
	void service();
private:
	static struct lws_context *context;
	static int event_cb( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len );
	
	static constexpr struct lws_protocols protocols[] =
	{
		{
			"default-protocol",
			event_cb,
			0,
			65536,
			0,
            NULL,
            0
		},
		{ NULL, NULL, 0, 0 } /* terminator */
	};
	std::string m_host;
	uint16_t m_port, m_idx;
	std::string m_symbol;
	Book& m_book;
	MDCache* m_cache;
	struct lws* conn;

	rapidjson::Document document_;
	void cb(std::string_view data);
	bool send_message(const std::string &message);

};
