// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>
#include <fstream>

#include <B2BITS_FastCodec.h>
#include <B2BITS_SystemDefines.h>

namespace
{
    const size_t SWXESSS_PREAMBLE_SIZE = 8;

    typedef std::vector<char> Buffer;
}

void usage( std::ostream& out )
{
    out << "Usage: program <options>" <<
        std::endl << "\t-h,  --help       display this message" <<
        std::endl << "\t-i,  --input      input file; default - the standard input stream" <<
        std::endl << "\t-o,  --output     output file; default - the standard output stream" <<
        std::endl <<
        std::endl;
}

void process(
    Engine::FastDecoder& fastDecoder,
    std::istream& input,
    std::ostream& output )
{
    struct Timestamp {
        System::i32    tv_sec;         /* seconds */
        System::i32    tv_usec;        /* and microseconds */
    };

    Buffer buffer;
    while ( !input.eof() ) {
        Timestamp timeval = {0, 0};
        int dataSize( 0 );

        //Read timestamp
        input.read( ( char* )&timeval, sizeof( timeval ) );
        int bytesReadActual = input.gcount();
        if(0 == bytesReadActual){
            break;
        }
        if(sizeof( timeval ) != bytesReadActual){
            output << "unexpected end of file.";
            break;
        }

        //Read size
        input.read( ( char* )&dataSize, sizeof( dataSize ) );
        bytesReadActual = input.gcount();
        if(sizeof( dataSize ) != bytesReadActual){
            output << "unexpected end of file.";
            break;
        }

        //read data
        if(0 == dataSize) {
            continue;
        }
        buffer.resize( dataSize );
        input.read( &*buffer.begin(), buffer.size() );
        bytesReadActual = input.gcount();

        if(dataSize != bytesReadActual){
            output << "unexpected end of file.";
            break;
        }
        buffer.resize( input.gcount() );

        //decode data to FIX messages
        const Buffer::const_iterator begin = buffer.begin();
        const Buffer::const_iterator end = buffer.end();

        Buffer::const_iterator cur = begin + SWXESSS_PREAMBLE_SIZE;

        std::size_t bytesToSkip( 0 );

        // determine the size of the Frame size block
        fastDecoder.decode_u32( &*cur, end - cur, &bytesToSkip );

        // skip the Frame size block
        cur += bytesToSkip;

        // decode messages in the frame
        while ( cur < end ) {
            std::size_t decoded( 0 );
            //decode the next message in the frame
            std::auto_ptr<Engine::FIXMessage> fixMessage = fastDecoder.decode( &*cur, end - cur, &decoded );

            // print the decoded FIX message
            output << *fixMessage->toString( '|' ) << std::endl;

            cur += decoded;
        }
        // reset the dictionary before going to the next frame
        fastDecoder.resetDictionary();
    }
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        char const* inputFile( NULL );
        char const* outputFile( NULL );

        int argi( 1 );

        // Parse command line parameters
        for ( ; argi < argc; ++argi ) {
            if ( argc == 1 && argi == 1 &&
                 ( strcmp( argv[argi], "--help" ) == 0 || strcmp( argv[argi], "-h" ) == 0 ) ) {
                usage( std::cout );
                return 0;
            } else if ( argi + 1 == argc ) {
                usage( std::cerr );
                return 1;
            } else if ( ( strcmp( argv[argi], "--input" ) == 0 ) || ( strcmp( argv[argi], "-i" ) == 0 ) ) {
                inputFile = argv[++argi];
            } else if ( ( strcmp( argv[argi], "--output" ) == 0 ) || ( strcmp( argv[argi], "-o" ) == 0 ) ) {
                outputFile = argv[++argi];
            } else {
                usage( std::cerr );
                return 1;
            }
        }

        // Initialize FastCodec
        Engine::FastCodec fastCodec( "../../../../engine.license" );

        Engine::FastMappingOptions mappingOptions;
        mappingOptions.boolNtoString_ = "N";
        mappingOptions.boolYtoString_ = "Y";

        std::auto_ptr<Engine::FastDecoder> fastDecoder( fastCodec.createFastDecoder(
                    "fixdict11_SWX_old.xml|fixdic50_SWX_old.xml",
                    "SWXESS.xml",
                    Engine::FIXT11, Engine::FIX50,
                    mappingOptions) );

        usage( std::cout );

        std::ifstream input( inputFile ? inputFile : "", std::ios::binary );
        std::ofstream output( outputFile ? outputFile : "", std::ios::binary );

        std::clog << "Decoding was started..." << std::endl;
        process( *fastDecoder, inputFile ? input : std::cin, outputFile ? output : std::cout );
        std::clog << "All messages were successfully decoded." << std::endl;
    } catch ( Utils::Exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 2;
    } catch ( std::exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 3;
    } catch ( ... ) {
        std::cerr << "Unknown error." << std::endl;
        return 4;
    }

    return 0;
}
////////////////////////////////////////////////////////////////////////////////////////////////
