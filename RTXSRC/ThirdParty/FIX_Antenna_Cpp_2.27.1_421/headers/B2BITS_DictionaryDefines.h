// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_DictionaryDefines.h
/// Contains FixDictionary2 forward declarations.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_DictionaryDefines_H
#define H_B2BITS_FixDictionary2_DictionaryDefines_H

#include <B2BITS_V12_Defines.h>
#include "B2BITS_CSRefCounterPtr.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

namespace FixDictionary2
{

    /** Generates helper types */
    template< typename T >
    class TypeTraits
    {
    public:
        typedef T Type;
        typedef Type* Ptr;
        typedef Type const* CPtr;
        typedef CSRefCounterPtr<Type const> RefC;
        typedef CSRefCounterPtr<Type> Ref;
        typedef CSRefCounterPtr<Type> const CRef;
        typedef Ref* RefArray;
        typedef CRef* CRefArray;
        typedef CPtr const* CPtrArray;
    };

    /** Protocol forward declaration. */
    class Protocol;

    /** Types for Protocol class */
    typedef TypeTraits<Protocol> ProtocolT;

    /** Message forward declaration. */
    class Message;

    /** Types for Message class */
    typedef TypeTraits<Message> MessageT;

    /** Field forward declaration. */
    class Field;

    /** Types for Field class */
    typedef TypeTraits<Field> FieldT;

    /** FieldType forward declaration. */
    class FieldType;

    /** Types for FieldType class */
    typedef TypeTraits<FieldType> FieldTypeT;

    /** RepeatingGroup forward declaration. */
    class RepeatingGroup;

    /** Types for RepeatingGroup class */
    typedef TypeTraits<RepeatingGroup> RepeatingGroupT;

    /** FieldRef forward declaration. */
    class FieldRef;

    /** Types for FieldRef class */
    typedef TypeTraits<FieldRef> FieldRefT;

    /** Block forward declaration. */
    class Block;

    /** Types for Block class */
    typedef TypeTraits<Block> BlockT;

    /** BlockRef forward declaration. */
    class BlockRef;

    /** Types for BlockRef class */
    typedef TypeTraits<BlockRef> BlockRefT;

    /** Item forward declaration. */
    class Item;

    /** Types for Item class */
    typedef TypeTraits<Item> ItemT;

    /** Item forward declaration. */
    class ValBlock;

    /** Types for Item class */
    typedef TypeTraits<ValBlock> ValBlockT;

    /** MessageItem forward declaration. */
    class MessageItem;

    /** Types for MessageItem class */
    typedef TypeTraits<MessageItem> MessageItemT;

    /** Dictionary forward declaration. */
    class Dictionary;

    /** Types for Dictionary class */
    typedef TypeTraits<Dictionary> DictionaryT;

    class MessageItemContainer;
    struct MessageItemContainerT {
        typedef MessageItemContainer* Ptr;
        typedef MessageItemContainer const* CPtr;
    };
} // namespace Engine


#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // H_B2BITS_FixDictionary2_DictionaryDefines_H
