//
// Created by schen on 10/9/2020.
//
#include <stdlib.h>
#include <iostream>
#include <sys/stat.h>
////#include <filesystem>
#include <sys/types.h>
#include <chrono>
#include <boost/filesystem.hpp>

#include "AppConstants.h"
#include "Logger.h"
#include "LogToFile.h"

#define _MAX_PATH 512

using namespace std;
//using namespace std::filesystem;
using namespace boost::filesystem;
using std::chrono::system_clock;

LogToFile::LogToFile()
{
}

LogToFile::~LogToFile()
{
    delete m_logger;
    m_logger = nullptr;
}

Logger* LogToFile::InitLogger(AppConstants::LOG_TO_FILE dest)
{
    std::string fileName;
    bool printHead=true;
    bool printTime=true;

    if(dest == AppConstants::LOG_TO_FILE::EXCEPTION)
    {
        std::string timeStr = CreatTimeString();
        fileName = "Exception_" + timeStr + ".log";
    }
    else if(dest == AppConstants::LOG_TO_FILE::SYSTEM)
    {
        fileName = "System.log";
    }
    else if(dest == AppConstants::LOG_TO_FILE::ORDER )
    {
        fileName = "Orders.log";
    }
    m_filePath = CreatDirectories(dest);
    m_logger = new Logger(m_filePath, fileName, printHead, printTime);
    m_logger->OpenFile();
    logHeader();

    return m_logger;
}

void LogToFile::CreatDirectory(std::string& filePath)
{
    std::string msg;
    struct stat info;

//    std::filesystem::path p = filePath.c_str();
    boost::filesystem::path p = filePath.c_str();
    try
    {
        if(stat(filePath.c_str(), &info) != 0)
        {
//            bool ret = std::filesystem::create_directory(p);
            bool ret = boost::filesystem::create_directory(p);
            if (!ret)
                msg = "Created dir: " + filePath + '\n';
        }
        else
        {
            msg = "The dir:" + filePath + " already exist!" + '\n';
        }
    }
    catch(std::exception& e)
    {
        msg = filePath + "..." + e.what() + '\n';
        std::cout << "Error: " << msg << std::endl;
    }
}

std::string LogToFile::CreatDirectories(AppConstants::LOG_TO_FILE dest)
{
    std::string msg;
    std::string filePath;
    std::string timeStr;

    if(dest == AppConstants::LOG_TO_FILE::ORDER || dest == AppConstants::LOG_TO_FILE::SYSTEM)
    {
        filePath = "./_LogInfo";
        CreatDirectory(filePath);

        timeStr = CreatTimeString();
        filePath = filePath + '/' + timeStr;
        CreatDirectory(filePath);
    }
    else if(dest == AppConstants::LOG_TO_FILE::EXCEPTION)
    {
        filePath = "./_LogException";
        CreatDirectory(filePath);
    }
    return filePath;
}

std::string LogToFile::CreatTimeString()
{
    time_t tt;
    char buffer[80];

    auto currentTime = std::chrono::system_clock::now();
    tt = system_clock::to_time_t ( currentTime );
    auto timeinfo = localtime (&tt);
    strftime (buffer,80,"%Y%m%d_%H%M",timeinfo);

    return std::string(buffer);
}

void LogToFile::logHeader()
{
    char buffer[ _MAX_PATH + 1 ];

    m_logger->logDirect("******************************************************\n");
    m_logger->logDirect("* File Name  : %s\n", m_logger->GetFileName().c_str() );
    m_logger->logDirect("* File Path  : %s\n", m_logger->GetFilePath().c_str() );

    time_t now = time( NULL );
    struct tm* tm = localtime( &now );
    strftime( buffer, 80, "%Y-%m-%d %H:%M:%S", tm );
    m_logger->logDirect("* Log opened : %s\n" , buffer);

    m_logger->logDirect("* File Mode  : %s\n", ((m_logger->GetSync()) ? "Synchronous" : "Asynchronous"));
    //m_logger->logDirect("* Working Dir :  %s\n", std::filesystem::current_path().c_str());
    m_logger->logDirect("******************************************************\n");
    m_logger->FlushFile();

    m_logger->m_fileSize = (unsigned long) m_logger->GetStreamPosition();
}


