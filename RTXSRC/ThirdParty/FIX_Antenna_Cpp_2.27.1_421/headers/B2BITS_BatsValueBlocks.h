// <copyright> 
// 
// $Revision: 1.3 $ 
//  
// (c) B2BITS EPAM Systems Company 2000-2017. 
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS). 
//  
// This software is for the use of the paying client of B2BITS (which may be 
// a corporation, business area, business unit or single user) to whom it was 
// delivered (the "Licensee"). The use of this software is subject to 
// license terms. 
// 
// The Licensee acknowledges and agrees that the Software and Documentation 
// (the "Confidential Information") is confidential and proprietary to 
// the Licensor and the Licensee hereby agrees to use the Confidential 
// Information only as permitted by the full license  agreement between 
// the two parties, to maintain the confidentiality of the Confidential 
// Information and not to disclose the confidential information, or any part 
// thereof, to any other person, firm or corporation. The Licensee 
// acknowledges that disclosure of the Confidential Information may give rise 
// to an irreparable injury to the Licensor in-adequately compensable in 
// damages. Accordingly the Licensor may seek (without the posting of any 
// bond or other security) injunctive relief against the breach of the forgoing 
// undertaking of confidentiality and non-disclosure, in addition to any other 
// legal remedies which may be available, and the licensee consents to the 
// obtaining of such injunctive relief. All of the undertakings and 
// obligations relating to confidentiality and non-disclosure, whether 
// contained in this section or elsewhere in this agreement, shall survive 
// the termination or expiration of this agreement for a period of five (5) 
// years. 
// 
// The Licensor agrees that any information or data received from the Licensee 
// in connection with the performance of the support agreement relating to this 
// software shall be confidential, will be used only in connection with the 
// performance of the Licensor's obligations hereunder, and will not be 
// disclosed to third parties, including contractors, without the Licensor's 
// express permission in writing. 
// 
// Information regarding the software may be provided to the Licensee's outside 
// auditors and attorneys only to the extent required by their respective 
// functions. 
// 
// </copyright> 


#ifndef B2BITS_BATS_VALUEBLOCKS_H
#define B2BITS_BATS_VALUEBLOCKS_H

/// @cond skipit

namespace Bats{ 

// ValueBlock: AuctionTypeCodes

   	static const char AuctionTypeCodes[] = { 'C', 'H', 'I', 'O' };

   	static const char AuctionTypeCodes_CLOSING= 'C';
   	static const char AuctionTypeCodes_CLOSING_DESC[] = "Closing Auction.";
   	static const char AuctionTypeCodes_HALT= 'H';
   	static const char AuctionTypeCodes_HALT_DESC[] = "Halt Auction.";
   	static const char AuctionTypeCodes_IPO= 'I';
   	static const char AuctionTypeCodes_IPO_DESC[] = "IPO Auction.";
   	static const char AuctionTypeCodes_OPENING= 'O';
   	static const char AuctionTypeCodes_OPENING_DESC[] = "Opening Auction.";

// ValueBlock: GRPLoginStatusCodes

   	static const char GRPLoginStatusCodes[] = { 'A', 'B', 'N', 'S' };

   	static const char GRPLoginStatusCodes_LOGIN_ACCEPTED= 'A';
   	static const char GRPLoginStatusCodes_LOGIN_ACCEPTED_DESC[] = "Login accepted.";
   	static const char GRPLoginStatusCodes_SESSION_IN_USE= 'B';
   	static const char GRPLoginStatusCodes_SESSION_IN_USE_DESC[] = "Session in use.";
   	static const char GRPLoginStatusCodes_NOT_AUTHORIZED= 'N';
   	static const char GRPLoginStatusCodes_NOT_AUTHORIZED_DESC[] = "Not authorised (Invalid Login/Password)";
   	static const char GRPLoginStatusCodes_INVALID_SESSION= 'S';
   	static const char GRPLoginStatusCodes_INVALID_SESSION_DESC[] = "Invalid session.";

// ValueBlock: GapResponseStatus

   	static const char GapResponseStatus[] = { 'A', 'C', 'D', 'I', 'M', 'O', 'S', 'U' };

   	static const char GapResponseStatus_ACCEPTED= 'A';
   	static const char GapResponseStatus_ACCEPTED_DESC[] = "Accepted.";
   	static const char GapResponseStatus_COUNT_REQUEST_LIMIT= 'C';
   	static const char GapResponseStatus_COUNT_REQUEST_LIMIT_DESC[] = " Count request limit for one gap exceeded.";
   	static const char GapResponseStatus_DAILY_GAP_REQUEST_ALLOCATION_EXHAUSTED= 'D';
   	static const char GapResponseStatus_DAILY_GAP_REQUEST_ALLOCATION_EXHAUSTED_DESC[] = "Daily gap request allocation exhausted.";
   	static const char GapResponseStatus_INVALID_UNIT= 'I';
   	static const char GapResponseStatus_INVALID_UNIT_DESC[] = "Invalid unit specified in request.";
   	static const char GapResponseStatus_MINUTE_GAP_REQUEST_ALLOCATION_EXHAUSTED= 'M';
   	static const char GapResponseStatus_MINUTE_GAP_REQUEST_ALLOCATION_EXHAUSTED_DESC[] = "Minute gap request allocation exhausted.";
   	static const char GapResponseStatus_OUT_OF_RANGE= 'O';
   	static const char GapResponseStatus_OUT_OF_RANGE_DESC[] = "Out of range (ahead of sequence or too far behind).";
   	static const char GapResponseStatus_SECOND_GAP_REQUEST_ALLOCATION_EXHAUSTED= 'S';
   	static const char GapResponseStatus_SECOND_GAP_REQUEST_ALLOCATION_EXHAUSTED_DESC[] = "Second gap request allocation exhausted.";
   	static const char GapResponseStatus_UNIT_UNAVAILABLE= 'U';
   	static const char GapResponseStatus_UNIT_UNAVAILABLE_DESC[] = "Unit is currently unavailable.";

// ValueBlock: HaltStatusCodes

   	static const char HaltStatusCodes[] = { 'H', 'Q', 'T' };

   	static const char HaltStatusCodes_HALTED= 'H';
   	static const char HaltStatusCodes_HALTED_DESC[] = "Halted.";
   	static const char HaltStatusCodes_QUOTE_ONLY= 'Q';
   	static const char HaltStatusCodes_QUOTE_ONLY_DESC[] = "Quote-Only (BATS Listings)";
   	static const char HaltStatusCodes_TRADING= 'T';
   	static const char HaltStatusCodes_TRADING_DESC[] = "Trading";

// ValueBlock: RegSHOActionCodes

   	static const char RegSHOActionCodes[] = { '0', '1' };

   	static const char RegSHOActionCodes_NO_PRICE_TEST= '0';
   	static const char RegSHOActionCodes_NO_PRICE_TEST_DESC[] = "No price test in effect.";
   	static const char RegSHOActionCodes_REG_SHO_PRICE_TEST= '1';
   	static const char RegSHOActionCodes_REG_SHO_PRICE_TEST_DESC[] = "Reg SHO price test restriction in effect.";

// ValueBlock: RetailPriceImprovementCodes

   	static const char RetailPriceImprovementCodes[] = { 'A', 'B', 'N', 'S' };

   	static const char RetailPriceImprovementCodes_BUY_SELL_RPI= 'A';
   	static const char RetailPriceImprovementCodes_BUY_SELL_RPI_DESC[] = "Buy and Sell RPI.";
   	static const char RetailPriceImprovementCodes_BUY_RPI= 'B';
   	static const char RetailPriceImprovementCodes_BUY_RPI_DESC[] = "Buy side RPI.";
   	static const char RetailPriceImprovementCodes_NO_RPI= 'N';
   	static const char RetailPriceImprovementCodes_NO_RPI_DESC[] = "No RPI.";
   	static const char RetailPriceImprovementCodes_SELL_RPI= 'S';
   	static const char RetailPriceImprovementCodes_SELL_RPI_DESC[] = "Sell side RPI.";

// ValueBlock: SideIndicatorCodes

   	static const char SideIndicatorCodes[] = { 'B', 'S' };

   	static const char SideIndicatorCodes_BUY= 'B';
   	static const char SideIndicatorCodes_BUY_DESC[] = "Buy Order.";
   	static const char SideIndicatorCodes_SELL= 'S';
   	static const char SideIndicatorCodes_SELL_DESC[] = "Sell Order.";

// ValueBlock: SpinResponseStatusCodes

   	static const char SpinResponseStatusCodes[] = { 'A', 'O', 'S' };

   	static const char SpinResponseStatusCodes_ACCEPTED= 'A';
   	static const char SpinResponseStatusCodes_ACCEPTED_DESC[] = "Accepted.";
   	static const char SpinResponseStatusCodes_OUT_OF_RANGE= 'O';
   	static const char SpinResponseStatusCodes_OUT_OF_RANGE_DESC[] = "Out of range (spin is no longer available).";
   	static const char SpinResponseStatusCodes_ALREADY_IN_PROGRESS= 'S';
   	static const char SpinResponseStatusCodes_ALREADY_IN_PROGRESS_DESC[] = "Spin already in progress (only one spin can be running at a time).";

// ValueBlock: SymbolConditionCodes

    static const char SymbolConditionCodes[] = { 'C', 'N' };

    static const char SymbolConditionCodes_CLOSING= 'C';
    static const char SymbolConditionCodes_CLOSING_DESC[] = "Closing";
    static const char SymbolConditionCodes_NORMAL= 'N';
    static const char SymbolConditionCodes_NORMAL_DESC[] = "Normal";
}//namespace Bats

/// @endcond

#endif // B2BITS_BATS_VALUEBLOCKS_H
