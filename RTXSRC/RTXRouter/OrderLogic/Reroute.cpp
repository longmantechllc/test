//
// Created by schen on 4/1/2021.
//

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "RTXUtils.h"
#include "Logger.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"
#include "CPU.h"

#include "RTXOrder.h"
#include "IOrderLogicBase.h"
#include "OrderLogic/_DataStruct/ChildOrderInfo.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"

#include "Reroute.h"


Reroute::Reroute(RTXOrder* order)
        : IOrderLogicBase { order }
{
}

Reroute::~Reroute()
{
}

bool Reroute::InitOrder()
{
    return true;
}

bool Reroute::ProcessOrder()
{
    if(!IsEligible())
        return false;
    if(m_order->m_rerouteDestinationList.empty())
    {
        m_order->CancelParent("Reroute no venue available.");
        return true;
    }
    std::string venue;
    auto itr = m_order->m_rerouteDestinationList.begin();
    while (itr != m_order->m_rerouteDestinationList.end())
    {
        if(IsVenueEligible(*itr))
        {
            venue = *itr;
            itr = m_order->m_rerouteDestinationList.erase(itr);
            break;
        }
        else
        {
            itr = m_order->m_rerouteDestinationList.erase(itr);
        }
    }
    if(venue.empty())
    {
        m_order->CancelParent("Reroute no valid venue available.");
        return true;
    }
    auto cOrder = m_order->CreateChildOrder();
    cOrder->size_ = m_order->QtyUncommited();
    cOrder->exchange_ = venue;
    SendChildOrder(cOrder);
    return true;
}

bool Reroute::SendChildOrder(std::shared_ptr<Order> order)
{
    m_order->SendChildOrder(order, ChildOrderType::CHILD_ORDER_TYPE_REROUTE);
    return false;
}

bool Reroute::ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if (child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_REROUTE)
        return false;
}

bool Reroute::ProcessMktData()
{
    return true;
}

bool Reroute::IsVenueEligible(const std::string& venue)
{
    if(m_order->ExcludedVenue(venue) == true)
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(venue))
        return false;

    return true;
}

bool Reroute::IsEligible()
{
    if (m_order->QtyUncommited() <= 0)
        return false;
    return true;
}