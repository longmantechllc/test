// <copyright> 
// 
// $Revision: 1.3 $ 
//  
// (c) B2BITS EPAM Systems Company 2000-2017. 
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS). 
//  
// This software is for the use of the paying client of B2BITS (which may be 
// a corporation, business area, business unit or single user) to whom it was 
// delivered (the "Licensee"). The use of this software is subject to 
// license terms. 
// 
// The Licensee acknowledges and agrees that the Software and Documentation 
// (the "Confidential Information") is confidential and proprietary to 
// the Licensor and the Licensee hereby agrees to use the Confidential 
// Information only as permitted by the full license  agreement between 
// the two parties, to maintain the confidentiality of the Confidential 
// Information and not to disclose the confidential information, or any part 
// thereof, to any other person, firm or corporation. The Licensee 
// acknowledges that disclosure of the Confidential Information may give rise 
// to an irreparable injury to the Licensor in-adequately compensable in 
// damages. Accordingly the Licensor may seek (without the posting of any 
// bond or other security) injunctive relief against the breach of the forgoing 
// undertaking of confidentiality and non-disclosure, in addition to any other 
// legal remedies which may be available, and the licensee consents to the 
// obtaining of such injunctive relief. All of the undertakings and 
// obligations relating to confidentiality and non-disclosure, whether 
// contained in this section or elsewhere in this agreement, shall survive 
// the termination or expiration of this agreement for a period of five (5) 
// years. 
// 
// The Licensor agrees that any information or data received from the Licensee 
// in connection with the performance of the support agreement relating to this 
// software shall be confidential, will be used only in connection with the 
// performance of the Licensor's obligations hereunder, and will not be 
// disclosed to third parties, including contractors, without the Licensor's 
// express permission in writing. 
// 
// Information regarding the software may be provided to the Licensee's outside 
// auditors and attorneys only to the extent required by their respective 
// functions. 
// 
// </copyright> 

#ifndef B2BITS_BATS_UNIT_SERVICE_H
#define B2BITS_BATS_UNIT_SERVICE_H

#include <B2BITS_BatsMessages.h>

namespace Bats
{
    class Application;
    class ServiceListener;
    class SymbolMappingListener;
    class InstrumentListener;
    struct RuntimeParameters;
    struct IUdpMock;

/** @addtogroup bats_mda_unit_service
*  @{ */

    /// @ingroup bats_mda_gs
    /// Represents single BATS unit
    class UnitService
    {
    public:

        /// Returns @Application
        /**
        * @return Bats::Application that owns this service
        * @throw no
        */
        virtual Application* getApplication() const = 0;

        /// Returns channel id
        /**
        * @return Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @throw no
        */
        virtual const std::string& getChannelID() const = 0;

       /// Returns unit id
        /**
        * @return Id of the unit specified as _unit\@id_ attribute in XML configuration file
        * @throw no
        */
        virtual System::u8 getUnitID() const = 0;

        /// Returns parameters
        /**
        * @return Parameters used by service
        * @throw no
        */
        virtual const RuntimeParameters& getParams() const = 0;

        /// Sets SPIN/GRP credentials 
        /**
        * @param userName must not exceed 4 characters
        * @param password must not exceed 10 characters
        * @param Session subId, must not exceed 4 characters
        *
        * @throw Utils::Exception if parameters are incorrect
        */
        virtual void setCredentials(const std::string& userName, const std::string& password, 
                                    const std::string& subId) = 0;

        /// Connects the service to the mutlicast group
        /** 
        * Starts all threads required by the service.
        * serviceListener should stay alive until service is disconnected.
        * Service becomes connected if this method succeds
        * @param serviceListener Optional listener to receive notification from the unit
        *
        * @throw Utils::Exception if error eccurs
        * @sa isConnected(), disconnect()
        */
        virtual void connect(ServiceListener* serviceListener) = 0;

        /// Disconnects the service from the mutlicast group.
        /**
        * This method returns returns immediately, service becomes disconnected only 
        * after ServiceListener::Notification::Disconnected notification. Does nothing if service 
        * is already disconnected.
        * @throw no
        * @sa isConnected(), connect()
        */
        virtual void disconnect() = 0;

        /// Returns true if service is in connected state
        /**
        * @return `true` if service is connected
        * @throw no
        * @sa connect(), disconnect()
        */
        virtual bool isConnected() = 0;

        /// Subscribe listener to symbol mapping messages
        /**
        * @param listener Observer to receive mapping messages from unit, cannot be null
        * cannot be used twice
        * @throw Utils::Exception parameter is incorrect or if error occurs
        */
        virtual void subscribeToSymbolMapping(SymbolMappingListener* listener) = 0;

        /// Unsubscribe symbol mapping listener 
        /**
        * @param listener Listener to unsubscribe
        * @throw Utils::Exception if error occurs
        */
        virtual void unsubscribeFromSymbolMapping(SymbolMappingListener* listener) = 0;

        /// Checks if service has instrument subscriptions
        virtual bool hasInstrumentSubscriptions() const = 0;

        /// Subscribe listener to updates for particular symbol
        /**
        * @param symbol Name of BATS symbol, must not exceed 8 characters
        * @param depth Depth of order book, if set to 0 incremental updates will not be delivered
        * @param listener Required observer to receive messages and updates
        * @param dispatchMessages When set to `true` service delivers raw PITCH messages

        * @throw Utils::Exception if error occurs
        * @note It is allowed to subscribe after service has been connected to the feed and 
        * received some data. Since it is not possible to restore actual state for a single 
        * BATS symbol, subscribing in this case leads to whole unit reset and recovery from 
        * Spin server, unless natural refresh is used. In case of natural refresh, just 
        * subscribed listener would be notified via @ref InstrumentListener::onReset "onReset" 
        * with ResetReason::rrNaturalRefresh
        */
        virtual void subscribeToInstrument(const std::string& symbol, System::u16 depth, InstrumentListener* listener, bool dispatchMessages = true) = 0;

        /// Unsubscribe symbol listener 
        /**
        * @param listener Listener to unsubscribe
        * @throw Utils::Exception if error occurs
        */
        virtual void unsubscribeFromInstrument(const std::string& symbol) = 0;

        virtual IUdpMock* getMock() = 0;
    };

    /// Handler' auxiliary structure for measuring processing latency
    /** 
    * Right after UDP package is received Timestamp::time0 is set to current time. 
    * On Windows platform this is value returned by `QueryPerformanceCounter` function, 
    * on Linux platform this is value in nanoseconds returned by `clock_gettime(CLOCK_MONOTONIC)`. 
    * Reference to Timestamp is passed along to each method in processing pipeline.
    */
    struct Timestamp
    {
        System::u64 time0;
        System::u64 time1;
    };

    /// Reasons why service resets its state
    /**
    * When reset condition occurs, service generates ServiceListener::onReset and InstrumentListener::onReset
    * events. Clears the cache and order books.
    */
    struct ResetReason {
        enum Type {
            /// Received unexpected packet with sequence number = 1
            rrSequenceNumber,
            /// Recovery from Spin server is required
            rrSpin,             
            /// Bats::UnitClearMsg message is received
            rrUnitClear,
            /// Recovery is required, but service is configured to use natural refresh
            /// @sa RuntimeParameters::useNaturalRefresh
			rrNaturalRefresh
        };
    };

    /// Unit service observer
    /**
    * Provides interface to receive service level events. 
    */
    class ServiceListener
    {
    public:
        /// Service notifications
        struct Notification {
            enum Type {
                /// Network activity has been restored, generated only if Notification::LinkDown notification has been generated
                LinkUp,
                /// UnitService does not forward heartbeat messages to user, in case when only heartbeats
                /// are being disseminated user does not receive any updates. This notification is generated
                /// when no messages, including heartbeats, has been received during RuntimeParameters::linkDownTimeoutS 
                /// period, which might indicate a network problems
                LinkDown,
                /// Service has been connected
                Connected,
                /// Service has been disconnected
                Disconnected,
                /// Recovery from Spin server has been started
                RecoveryStarted, 
                /// Recovery from Spin server has been finished successfully
                RecoverySucceeded,
                /// Recovery from Spin server failed
                RecoveryFailed,
                /// Replay from GRP server has been started
                ReplayStarted,
                /// Replay requested successfully
                ReplayRequested,
                /// Failed to request replay 
                ReplayFailed,
                /// Sequence has been restored (messages has been replayed)
                SequenceRestored 
            };
        };

        /// Passed when it is possible to utilize current thread and proceed reading multicast feed
        /// from other threads in pool
        struct ReadingCtx {
            /// Reading of UDP multicast will be continued in other thread after this call
            /// @note Sequence lock is released when this method returns
            virtual void proceedReading() = 0;
        };

        /// Called on service reset
        /**
        * @param service Service that generated notification
        * @param resetReason reason why reset has been generated
        */
        virtual void onReset(UnitService* service, ResetReason::Type resetReason) = 0;

        /// Called on PITCH message.
        /**
        * This method is called when PITCH message is received, messages are guaranteed to
        * be properly ordered
        * @param service Service that generated notification
        * @param msg Message received
        * @param seconds Last BATS Unit timestamp, sent via Bats::TimeMsg message
        */
        virtual void onMessage(UnitService* service, const PitchMsg* msg, System::u32 seconds, Timestamp& ts) = 0;

        /// Called when whole sequenced unit has been processed
        /**
        * @param service Service that generated notification
        * @param ctx If not NULL, can be used to utilize current thread and continue reading multicast feed
        */
        virtual void onUnitProcessed(ReadingCtx* ctx, Timestamp& ts) {(void)&ctx;(void)&ts;}

        /// Called on service notifications
        /**
        * @param service Service that generated notification
        * @param type Type of notification
        */
        virtual void onNotification(UnitService* service, Notification::Type type) {
            (void)&service;(void)&type;
        }
    };

/** @addtogroup bats_mda_stm
* @{ */

     /// Symbol mappings receiver
    class SymbolMappingListener
    {
    public:
        /// Called on option symbol mapping message, 
        /// @return `true` to proceed listening or `false` to unsibscribe
        virtual bool onSymbolMappingMsg(const SymbolMappingMsg* msg) = 0;
        /// Called when listener is unsubscribed. 
        /** 
        * Listener is not used by service after this call, 
        * you may call `delete this` from it. When service is being destroyed it automatically
        * unsubscribes all listeners. 
        */
        virtual void onUnsubscribed() = 0;
    };

/** @} */    

/** @addtogroup bats_mda_sti
* @{ */

    /// @nosubgrouping
    /// Single instrument observer
    class InstrumentListener
    {
    public:
        /// Represents current state of instrument
        /**
            Structure represents actual state of instrument. All properties have default value = 0
        */
        struct InstrumentCtx
        {
            static const unsigned MAX_SYMBOL_LEN = 8;

            InstrumentCtx() : haltStatus_(0), regSHOAction_(0), lastQty_(0), lastPrice_(0), 
                              batsTimestampS_(0), batsTimestampNS_(0)
			{
				memset( symbol_, 0, sizeof( symbol_ ) );
			}

            /// Last Halt status received in TradingStatusMsg
            char haltStatus_;
            /// Last Reg SHO action received in TradingStatusMsg
            char regSHOAction_;
            /// Last executed quantity
            System::u32 lastQty_;
            /// Last executed price
            System::u64 lastPrice_;
            /// Last BATS Unit timestamp in seconds, sent via Bats::TimeMsg message
            System::u32 batsTimestampS_;
            /// Last message time in nanoseconds (TimedPitchMsg::getTimeOffset)
            System::u32 batsTimestampNS_;
            /// Null-terminated symbol name without trailing spaces
            char symbol_[MAX_SYMBOL_LEN + 1];
        };

        /// Represents current state of a particular ordrer
        struct OrderCtx
        {
            /// 'B' or 'S'
            char side_;
            System::u32 qty_;
            System::u64 price_;
        };

        /** @name Service notifications
        * @{ */

        /// Called on reset, might be either single symbol or whole unit reset
        virtual void onReset(ResetReason::Type rr) = 0;
        /// Called when recovery from Spin server is finished
        virtual void onSpinFinished() {}

        /// Called when listener is unsubscribed.
        /** 
        * Listener is not used by service after this call, 
        * you may call `delete this` from it. When service is being destroyed it automatically
        * unsubscribes all listeners.
        */
        virtual void onUnsubscribed() {}
        /** @} */

        /** @name Auction updates
        * Auction update messages, these messages are always delivered
        * @{ */
        virtual void onMessage(const AuctionUpdateMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const AuctionSummaryMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        /** @} */

        /** @name Raw PITCH messages
        * The following raw PITCH messages are delivered only if UnitService::subscribeToInstrument has been
        * called with `dispatchMessages` = `true`
        * @param msg Raw PITCH message
        * @param seconds Last BATS Unit timestamp, sent via Bats::TimeMsg message
        * @param ctx This value represents current state of this order, before message `msg` is applied
        * @{ */
        virtual void onMessage(const AddOrderLongMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const AddOrderShortMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const AddOrderExpandedMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const DeleteMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const OrderExecutedMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const OrderExecutedAtPriceSizeMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const ReduceSizeLongMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const ReduceSizeShortMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const ModifyLongMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const ModifyShortMsg* msg, System::u32 seconds, Timestamp& ts, const OrderCtx& ctx) { (void)msg;(void)&seconds;(void)&ts;(void)&ctx; }
        virtual void onMessage(const TradingStatusMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const TradeExpandedMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const TradeLongMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        virtual void onMessage(const TradeShortMsg* msg, System::u32 seconds, Timestamp& ts) { (void)msg;(void)&seconds;(void)&ts; }
        /** @} */

        /** @name Order book incremental updates
        * The following updates are delivered only if UnitService::subscribeToInstrument 
        * has been called with `depth` > `0`
        * @param iCtx Current instrument context
        * @param ctx This value represents aggregate state on this price level
        * @param orders Number of orders on this price level
        * @param level Price level > 0, top level = 1
        * @param side 'B' or 'S' side
        * @{ */
        virtual void addBookItem(const InstrumentCtx& iCtx, const OrderCtx& ctx, System::u32 orders, System::u16 level, Timestamp& ts) = 0;
        virtual void changeBookItem(const InstrumentCtx& iCtx, const OrderCtx& ctx, System::u32 orders, System::u16 level, Timestamp& ts) = 0;
        virtual void removeBookItem(const InstrumentCtx& iCtx, char side, System::u16 level, Timestamp& ts) = 0;
        virtual void onTrade(const InstrumentCtx& iCtx, Timestamp& ts) { (void)&iCtx;(void)&ts; }
        virtual void onTradingStatus(const InstrumentCtx& iCtx, Timestamp& ts) { (void)&iCtx;(void)&ts; }
        /** @} */
    };
/**@}*/    
/**@}*/
}

#endif  //  B2BITS_BATS_UNIT_SERVICE_H
