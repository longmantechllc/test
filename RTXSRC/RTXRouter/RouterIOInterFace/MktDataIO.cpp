//
// Created by schen on 11/19/2020.
//
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>
#include <optional>
#include <iostream>

#include "../include/TopOfBook.h"
#include "../include/Bbo.h"
#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"
#include "Logger.h"

#include "CPU.h"
#include "../include/App.h"
#include "OrderLogic/RTXOrder.h"
#include "../include/RTXOrderIO.h"
//#include "../TestRouter/MktDataProcess.h"       //for test

#include "../include/MktDataIO.h"


MktDataIO::MktDataIO()
{
}

MktDataIO::MktDataIO(App* app)
        :m_app(app),
         m_logger(app->m_loggerSys)
{
    m_queue = nullptr;
    StartMktDataThread();
}

MktDataIO::~MktDataIO()
{
    CloseMktDataThread();
}

std::vector <std::shared_ptr<TopOfBook>> MktDataIO::SubscribeTOB(const std::string& symbol, int oid, RTXOrder* rtxOrder)
{
//    m_logger->logInfo("SubscribeTOB-1: symbol: %s, oid: %d", symbol.c_str(), oid);
    AddRTXOrder(symbol, oid, rtxOrder);
    ++m_tobSymbolUserCount[symbol];
//    m_logger->logInfo("SubscribeTOB-2: symbol: %s, oid: %d", symbol.c_str(), oid);

//    m_mktDataProcess->subscribeTOB(symbol);
    auto cachedTobList = m_mktDataProcess->subscribeTOB(symbol);
//    if(!cachedTobList.empty())
//        return cachedTobList;
//    m_logger->logInfo("SubscribeTOB-3: symbol: %s, oid: %d", symbol.c_str(), oid);

    return std::vector <std::shared_ptr<TopOfBook>>();
}

Bbo MktDataIO::SubscribeBbo(const std::string& symbol, int oid)
{
//    m_logger->logInfo("SubscribeBbo: symbol: %s, oid: %d", symbol.c_str(), oid);

    AddRTXOrder(symbol, oid, nullptr);
    ++m_bboSymbolUserCount[symbol];
//    return m_mktDataProcess->subscribeBbo(symbol);
}

OpenTrade MktDataIO::RequestOpeningTrade(const std::string& symbol, int oid)
{
    ++m_openTradeSymbolUserCount[symbol];
    OpenTrade trade;
    trade = m_mktDataProcess->requestOpeningTrade(symbol, oid);
    return trade;
}

void MktDataIO::OnTopOfBookUpdate( const BatchTOB& tobV)
{
    if (!tobV.empty())
    {
        auto list = GetReceivers(tobV[0].m_symbol);
        for (auto rtxOrder : list)
            rtxOrder->OnTob(tobV, true);
    }
//
////    m_logger->logInfo("OnTopOfBookUpdate 1: size: %d", tobV.size());
//    if (!tobV.empty())
//    {
////        m_logger->logInfo("OnTopOfBookUpdate 2: size: %d", tobV.size());
//
//        auto orderList = symbolOrderDict.find(tobV[0].m_symbol);
//        if (orderList != symbolOrderDict.end())
//        {
////            m_logger->logInfo("OnTopOfBookUpdate 3: symbol: %s, oid : %d", orderList->first.c_str(), orderList->second.size());
//            for (auto rtxOrderIO : orderList->second)
//            {
////                m_logger->logInfo("OnTopOfBookUpdate 4: try oid : %d, orderList size : %d", rtxOrderIO, m_app->m_RTXOrderIO->GetOrderList().size());
//                auto itr = m_app->m_RTXOrderIO->GetOrderList().find(rtxOrderIO);
//                if(itr != m_app->m_RTXOrderIO->GetOrderList().end())
//                {
////                    m_logger->logInfo("OnTopOfBookUpdate 5: found oid :%d", rtxOrderIO);
//                    itr->second->OnTob(tobV, true);
////                    m_logger->logInfo("OnTopOfBookUpdate 6: after onTob oid :%d", rtxOrderIO);
//                }
//            }
//        }
//    }
}

void MktDataIO::UpdateBbo(const Bbo& bbo)
{
    auto list = GetReceivers(bbo.m_symbol);
    for (auto rtxOrder : list)
        rtxOrder->OnBbo(bbo);
//
////    m_logger->logInfo("UpdateBbo-1: symbol: %s, size: %d", bbo.m_symbol.c_str(), symbolOrderDict.size());
//    auto orderList = symbolOrderDict.find(bbo.m_symbol);
//    if(orderList != symbolOrderDict.end())
//    {
////        m_logger->logInfo("UpdateBbo-2: orderList size: %d", orderList->second.size());
//        for(auto rtxOrderIO : orderList->second)
//        {
////            m_logger->logInfo("UpdateBbo-3: oid: %d, RTXOrderIO list size: %d", rtxOrderIO, m_app->m_RTXOrderIO->GetOrderList().size());
//            auto itr = m_app->m_RTXOrderIO->GetOrderList().find(rtxOrderIO);
//            if(itr != m_app->m_RTXOrderIO->GetOrderList().end())
//            {
////                m_logger->logInfo("UpdateBbo-4: Found oid: %d", rtxOrderIO);
//                itr->second->OnBbo(bbo);
//            }
//        }
//    }
}

void MktDataIO::UpdateOpeningTrade(const OpenTrade& openTrade)
{
    auto list = GetReceivers(openTrade.m_symbol);
    for (auto rtxOrder : list)
        rtxOrder->OnOpeningTrade(openTrade);
//
//    auto orderList = symbolOrderDict.find(openTrade.m_symbol);
//    if(orderList != symbolOrderDict.end())
//    {
//        for(auto rtxOrder : orderList->second)
//        {
//            rtxOrder.second->OnOpeningTrade(openTrade);
//        }
//    }
}

void MktDataIO::RemoveTOBSubscription(const std::string& symbol, int oid)
{
    /////////
    --m_app->m_cpu->m_oId;

    if(m_tobSymbolUserCount[symbol] == 0)
        return;
//    m_logger->logInfo("RemoveTOBSubscription-1: symbol: %s, oid: %d", symbol.c_str(), oid);
    --m_tobSymbolUserCount[symbol];
    if(m_tobSymbolUserCount[symbol] == 0) {
//        m_logger->logInfo("True RemoveTOBSubscription-2: symbol: %s, oid: %d", symbol.c_str(), oid);
        m_mktDataProcess->removeTOBSubscription(symbol);
    }
    RemoveRTXOrder(symbol, oid);
//    auto list = symbolOrderDict.find(symbol);
//    if(list != symbolOrderDict.end())
//    {
//        auto order = list->second.find(oid);
//        if(order != list->second.end()) {
//            m_logger->logInfo("RemoveTOBSubscription 2: oid: %d", oid);
//            list->second.erase(order);
//        }
//    }
//    m_logger->logInfo("RemoveTOBSubscription-3: oid: %d", oid);
}

void MktDataIO::RemoveBboSubscription(const std::string& symbol)
{
    if(m_bboSymbolUserCount[symbol] == 0)
        return;
//    m_logger->logInfo("RemoveBboSubscription 1: symbol: %s", symbol.c_str());
    --m_bboSymbolUserCount[symbol];
//    if(m_bboSymbolUserCount[symbol] == 0)
//        m_mktDataProcess->removeBboSubscription(symbol);
}

void MktDataIO::CancelOpeningTradeRequest(const std::string& symbol)
{
    if(m_openTradeSymbolUserCount[symbol] == 0)
        return;
//    m_logger->logInfo("CancelOpeningTradeRequest 1: symbol: %s", symbol.c_str());
    --m_openTradeSymbolUserCount[symbol];
    if(m_openTradeSymbolUserCount[symbol] == 0)
        m_mktDataProcess->cancelOpeningTradeRequest(symbol);
}

std::vector< RTXOrder* > MktDataIO::GetReceivers(const std::string& symbol)
{
    std::lock_guard<std::mutex> lk(m_mutex);
    std::vector< RTXOrder* > list;
    auto orderList = symbolOrderDict.find(symbol);
    if (orderList != symbolOrderDict.end()) {
        for (auto rtxOrder : orderList->second)
            list.emplace_back(rtxOrder.second);
    }
    return list;
}

void MktDataIO::RemoveRTXOrder(std::string symbol, int oid)
{
//    m_logger->logInfo("RemoveRTXOrder-1: symbol: %s, oid: %d", symbol.c_str(), oid);

    std::lock_guard<std::mutex> lk(m_mutex);
    auto list = symbolOrderDict.find(symbol);
    if(list != symbolOrderDict.end())
    {
        auto order = list->second.find(oid);
        if(order != list->second.end()) {
//            m_logger->logInfo("RemoveRTXOrder-2: symbol: %s, oid: %d", symbol.c_str(), oid);
            list->second.erase(order);
//            m_logger->logInfo("RemoveRTXOrder-3: symbol: %s, oid: %d", symbol.c_str(), oid);
        }
    }
}

void MktDataIO::AddRTXOrder(std::string symbol, int oid, RTXOrder* rtxOrder)
{
    std::lock_guard<std::mutex> lk(m_mutex);
    if(m_tobSymbolUserCount.find(symbol) == m_tobSymbolUserCount.end())
    {
        auto pr = std::make_pair(symbol, 0);
        m_tobSymbolUserCount.insert(pr);
        m_bboSymbolUserCount.insert(pr);
        m_openTradeSymbolUserCount.insert(pr);
    }
    auto list = symbolOrderDict.find(symbol);
    if(list == symbolOrderDict.end())
    {
        auto pr = std::make_pair(oid, rtxOrder);
//        std::unordered_map<int, std::shared_ptr<RTXOrder> > list2;
        std::unordered_map<int, RTXOrder*> list2;
        list2.insert(pr);

        auto pr2 = std::make_pair(symbol, list2);
        symbolOrderDict.insert(pr2);
    }
    else
    {
        if(list->second.find(oid) == list->second.end())
        {
            auto pr = std::make_pair(oid, rtxOrder);
            list->second.insert(pr);
        }
    }

//    if(list == symbolOrderDict.end())
//    {
//        std::unordered_set <int> list;
//        list.insert(oid);
//        auto pr =  std::make_pair(symbol, list);
//        symbolOrderDict.insert(pr);
//    }
//    else
//    {
//        if(list->second.find(oid) == list->second.end())
//        {
//            list->second.insert(oid);
//        }
//    }
}

void MktDataIO::PullMktData()
{
    while (running_)
    {
        if(!m_queue) {
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            continue;
        }
        MDMsgPtr msg;
        while(m_queue->try_dequeue(msg))
        {
            if(msg->msgtype() == MDType::TOB)
                OnTopOfBookUpdate(*msg->getTOB());
            else
                UpdateBbo(*msg->getBBO());
        }
        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        //std::this_thread::yield();
    }
}

void MktDataIO::StartMktDataThread()
{
    std::cout << "Start market data thread." << endl;
    m_logger->logInfo("Start market data thread.");

    int numThreads = 1;
    running_ = true;
    for (auto i = 0; i < numThreads; ++i) {
        mThreads.emplace_back([&]() {
            PullMktData();
        });

    }
}

void MktDataIO::CloseMktDataThread()
{
    std::cout << "Close MktDataThread ." << endl;
    running_ = false;
    for (auto& thread : mThreads)
        thread.join();
}
