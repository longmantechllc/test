/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "SampleApplication.h"
#include "B2BITS_CME/Params.h"
#include "CustomCMEApplication.h"
#include "B2BITS_Thread.h"
#include "B2BITS_ReferenceableManualEvent.h"
#include "B2BITS_Session.h"

SampleApplication::SampleApplication(void)
	:logger_(Utils::Log::LogSystem::singleton()->createCategory("CME Initiator"))
	,params_(NULL)
{
	
	// construct params Engine properties
	params_.reset(new b2bitscme::Params(Engine::FixEngine::singleton()->getProperties(), logger_));

	cmeApp_.reset(new CustomCMEApplication(params_.get(), logger_, this));

	// Create session for each Market Data Segment
	for (b2bitscme::Params::MarketSegmentByIdT::iterator it = params_->marketSegments_.begin(); it != params_->marketSegments_.end(); ++it) {
		std::string marketSegmentId = it->first;
			
		createSession(marketSegmentId, 0, 0);
	}
}


SampleApplication::~SampleApplication(void)
{
	cmeApp_.reset();
}


void SampleApplication::beginWeekLogon(const std::string &marketSegmentId)
{
	
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
		
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::beginWeekLogon] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		logSem_.post();
		return;
	}

	cmeApp_->beginWeekLogon(session, params_->marketSegments_[marketSegmentId].primaryHost_);

	// Wait for the Logon or Logout message
	logSem_.wait();
}

void SampleApplication::midWeekLogon(const std::string &marketSegmentId)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
		
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::midWeekLogon] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		logSem_.post();
		return;
	}
	cmeApp_->midWeekLogon(session, params_->marketSegments_[marketSegmentId].primaryHost_);

	// Wait for the Logon or Logout message
	logSem_.wait();
}

void SampleApplication::inSessionLogon(const std::string &marketSegmentId)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
		
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::inSessionLogon] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		logSem_.post();
		return;
	}

	if (session->getState() != Engine::Session::ESTABLISHED){
		if (logger_)
			logger_->note("Session should be established in order to apply in-session logon");
		
		return;
	}

	cmeApp_->inSessionLogon(session);
}


void SampleApplication::disconnect(const std::string &marketSegmentId, bool keepSeqNum, int startSequenceNumber, bool switchBackupToPrimary)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
		
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::disconnect] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		return;
	}	

	int inSeqNum = 0;
	int outSeqNum = 0;

	// Store In and Out seq nums before disconnecting
	if (keepSeqNum) {
		// if startSequenceNumber was specified use it instead of stored inSeqNum
		inSeqNum = startSequenceNumber > 0 ? startSequenceNumber : session->getInSeqNum();
		outSeqNum = session->getOutSeqNum();
	}

    if (Engine::Session::INITIAL!= session->getState() && Engine::Session::CORRECTLY_TERMINATED != session->getState() && 
        Engine::Session::NON_GRACEFULLY_TERMINATED != session->getState())
    {
        // Disconnect session NonGracefully
        session->disconnect();

        // Wait for Logout message
        logSem_.wait();
    }

	// Release session resources
	sessions_.removeSession(marketSegmentId);
	
	b2bitscme::RefCounter<System::ReferenceableManualEvent> sessionDestroyedEvent(new System::ReferenceableManualEvent());
	session->setSessionDestructionEvent(sessionDestroyedEvent.get());
	
	session.reset(NULL);

	// wait till the session is terminated and unregistered from the engine's session manager before creating the new one
	sessionDestroyedEvent->wait();

	// Re-create Session
	createSession(marketSegmentId, inSeqNum, outSeqNum, switchBackupToPrimary);
		
}

void SampleApplication::setInSeqNum(const std::string &marketSegmentId, int seqNum)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::setInSeqNum] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		return;
	}

	session->setInSeqNum(seqNum);
}

void SampleApplication::setOutSeqNum(const std::string &marketSegmentId, int seqNum)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::setOutSeqNum] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		return;
	}

	session->setOutSeqNum(seqNum);
}
		
void SampleApplication::switchBackupToPrimary(const std::string &marketSegmentId)
{
	// Perform mid week reconnect, keeping session seq nums and swapping hosts
	midWeekReconnect(marketSegmentId, 0, true);
}

void SampleApplication::midWeekReconnect(const std::string &marketSegmentId, int startSequenceNumber, bool switchBackupToPrimary)
{
	// Disconnect and re-create session keeping seq nums
	disconnect(marketSegmentId, true, startSequenceNumber, switchBackupToPrimary);

	System::Thread::sleep(1000);

	// Initiate Mid-Week Logon
	midWeekLogon(marketSegmentId);
}

void SampleApplication::sendMessage(const std::string &marketSegmentId, const std::string& message)
{
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);
	cmeApp_->sendMessage(session, message, true);
}

void SampleApplication::sendTestRequest(const std::string &marketSegmentId, const std::string& text) {
	b2bitscme::SessionPtr session = sessions_.getSession(marketSegmentId);

	if (session.get() == NULL) {
		if (logger_)
			logger_->note("[SampleApplication::sendTestRequest] Session with Market Segment Id [" + marketSegmentId + "] was not found");
		return;
	}	

	session->ping(text);
}

void SampleApplication::createSession(const std::string &marketSegmentId, int inSeqNum, int outSeqNum, bool switchBackupToPrimary) {
	if (switchBackupToPrimary) {
		// Swap backup and primary hosts for this marketSegmentId
		std::swap(params_->marketSegments_[marketSegmentId].backupHost_, params_->marketSegments_[marketSegmentId].primaryHost_);
		if (logger_)
			logger_->note("[SampleApplication::createSession] Backup and Primary hosts were swapped: Id = [" + marketSegmentId + "], "
				  + "Primary Host = [" + params_->marketSegments_[marketSegmentId].primaryHost_ + "], " 
				  + "Backup Host = [" + params_->marketSegments_[marketSegmentId].backupHost_ + "]");
	}

	b2bitscme::SessionPtr session = sessions_.createSession(marketSegmentId, cmeApp_.get(), *params_);

	if (session.get() == NULL){
		if (logger_)
			logger_->note("[SampleApplication::createSession] Unable to create session for the Market Segment Id [" + marketSegmentId + "]");
		return;
	}

	// Set In and Out Seq Nums if needed
	if (0 < inSeqNum) {
		session->setInSeqNum(inSeqNum);
	}

	if (0 < outSeqNum) {
		session->setOutSeqNum(outSeqNum);
	}
}


