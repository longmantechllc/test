// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include "MarketDataStorage.h"

#include <memory>
#include <iostream>

void MarketDataStorage::reset()
{
    book_.reset();
    impliedBook_.reset();
    tradeInformation_.reset();
    status_ = 0;
}

inline void MarketDataStorage::Book::reset()
{
    bid_.reset();
    ask_.reset();
}

const std::vector<MarketDataStorage::Book::Side::Action>& MarketDataStorage::Book::actions() const
{
    if( isAskAction_ ) {
        return ask_.actions_;
    }

    return bid_.actions_;
}

void MarketDataStorage::Book::Side::reserveActions( size_t size )
{
    actions_.clear();
    actions_.reserve( size * 2 );
}

void MarketDataStorage::Book::reserveActions( size_t size )
{
    bid_.actions_.clear();
    bid_.actions_.reserve( size * 2 );

    ask_.actions_.clear();
    ask_.actions_.reserve( size * 2 );
}

void MarketDataStorage::Book::Side::action( char actionCode, PriceSize price, size_t index )
{
    Action addAction( actionCode, price, index );
    actions_.push_back( addAction );

    std::cout <<  addAction.action_ << "\t" <<  addAction.level_ << "\t" <<  addAction.price_.price_ << ":" << addAction.price_.size_ << std::endl;

    if( addAction.action_ == MDUpdateAction::Delete
        && MarketDataStorage::Book::deltixMaxDepth - 1 < bookSize_
        && index < MarketDataStorage::Book::deltixMaxDepth ) {
        Action insertAction( MDUpdateAction::Insert,
                             prices_[MarketDataStorage::Book::deltixMaxDepth - 1],
                             MarketDataStorage::Book::deltixMaxDepth - 1 );

        actions_.push_back( insertAction );
        std::cout <<  insertAction.action_ << "\t" <<  insertAction.level_ << "\t" <<  insertAction.price_.price_ << ":" << insertAction.price_.size_ << std::endl;
    }

}

inline void MarketDataStorage::Book::Side::reset()
{
    //memset( this, 0, sizeof(*this) );
    bookSize_ = 0;
}

inline void MarketDataStorage::TradeInformation::reset()
{
    memset( this, 0, sizeof( *this ) );
}


void MarketDataStorage::Book::Side::insert( double price, double size, size_t index )
{
    PriceSize p;
    p.price_ = price;
    p.size_ = size;
    insert( p, index );
}

void MarketDataStorage::Book::Side::insert( PriceSize price, size_t index )
{
    assert( bookSize_ + 1 < MAX_LEVEL_COUNT );

    memmove( &prices_[ index + 1 ], &prices_[ index ], ( bookSize_ - index ) * sizeof( prices_[0] ) );
    prices_[ index ] = price;
    ++bookSize_;

    action( MDUpdateAction::Insert, price, index );
}

void MarketDataStorage::Book::Side::remove( size_t index )
{
    assert( bookSize_ > 0 );

    PriceSize del = prices_[ index ];
    memmove( &prices_[ index ], &prices_[ index + 1 ], ( bookSize_ - ( index - 1 ) ) * sizeof( prices_[0] ) );
    --bookSize_;

    // only after apply action
    action( MDUpdateAction::Delete, del, index );

    //memset( &prices_[ bookSize_ - 1 ], 0, sizeof(prices_[0]) );
}

void MarketDataStorage::Book::Side::update( PriceSize price, size_t index, bool isIncrement )
{
    update( price.price_, price.size_, index, isIncrement );
}

void MarketDataStorage::Book::Side::update( double price, double size, size_t index, bool isIncrement )
{
    assert( index < MAX_LEVEL_COUNT );

    prices_[ index ].price_ = price;
    prices_[ index ].size_ = size;

    if( index >= bookSize_ ) { // index + 1 > bookSize
        bookSize_ = index + 1;
    }

    if( isIncrement ) {
        action( MDUpdateAction::Update, prices_[ index ], index );
    }
}
