// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Session.h
/// Contains Engine::Session class declaration.

#ifndef __B2BITS_Session_h__
#define __B2BITS_Session_h__

#include <set>
#include <vector>
#include <string>
#include <memory>
#include <list>

#include <stdexcept>

#include <B2BITS_CompilerDefines.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_IPAddr.h>
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_SessionParameters.h"
#include "B2BITS_FixFieldsContainer.h"
#include "B2BITS_UTCTimestamp.h"
#include "B2BITS_IntDefines.h"

namespace System
{
    class InetAddress;
    class ReferenceableManualEvent;
}


namespace Engine
{
    class SessionStateObserver;
    class PreparedMessage;
    class Application;
    class SessionEx;
    struct MsgFilter;
    struct SessionId;

    /**
     * Options to Session::put method
     */
    struct PutMessageOptions {
        /**
         * Default constructor
         */
        PutMessageOptions()
            : overrideSenderCompID_( true )
            , overrideTargetCompID_( true )
            , overrideSendingTime_( true ) {
        }

        /**
         * Specifies either Session should override SenderCompID field of the passed FIXMessage or not.
         * true to override; false to pass as is. By default property equals to true.
         */
        bool overrideSenderCompID_;

        /**
         * Specifies either Session should override TargetCompID field of the passed FIXMessage or not.
         * true to override; false to pass as is. By default property equals to true.
         */
        bool overrideTargetCompID_;

        /**
         * Specifies either Session should override SendingTime field of the passed FIXMessage or not.
         * true to override; false to pass as is. By default property equals to true.
         */
        bool overrideSendingTime_;
    };

    /** Lightweight FIX Session statistics with small performance overhead  */
    struct V12_API FixSessionStatisticsLite {
        /** A number of recieved messages*/
        System::u64 numMsgsIn_;
        /** A number of messages in outgouing queue */
        size_t outgoingQueueNumMsgs_; 

        /**
        * Default constructor
        */
        FixSessionStatisticsLite()
            : numMsgsIn_(0)
            , outgoingQueueNumMsgs_(0) 
        {}
    };

    /**
    * FIX %Session.
    * Provides interface to establish connection, send and receive messages.
    * Instance of the session can be created by Engine::FixEngine class.
    *
    * Example:
    * \code
    * // Create FIX Session
    * SessionEventListener listener;
    * Engine::Session* session = Engine::FixEngine::singleton()->createSession( &listener, "sender", "target", Engine::FIX42 );
    *
    * // Establish connection with acceptor
    * session->connect( 30, "127.0.0.1", 9999 );
    *
    * // Create FIX Message
    * Engine::FIXMessage* msg = session->newSkel( "D" );
    *
    * // Fill fields of the message
    * // ... 
    * msg->set( FIXFields::ClOrdID, "1234" );
    *
    * // Send message
    * session->put( msg );
    * 
    * // After Engine::Session::put message can be destroyed
    * Engine::FIXMessage::release( msg );
    *
    * // Close connection
    * session->disconnect();
    * session->waitForTerminated();
    *
    * // Destroy session
    * session->release();
    * \endcode
    */
    class V12_API Session: public Utils::ReferenceCounter
    {
    public:
        /** Session role */
        typedef SessionRole Role;

        /**
        * Active connection to use.
        */
        enum ActiveConnection {
            PRIMARY_CONNECTION = 0, ///< The session connects via primary connection
            BACKUP_CONNECTION,      ///< The session connects via backup connection
            RESTORE_CONNECTION      ///< The session connects to the previous active connection
        };

        /** Sequence numbers reset strategy */
        enum SeqNumResetStrategy {
            RESET_SEQNUM_STRATEGY,                  /*! Should be used to reset sequence numbers on local side.
                                                    * Strategy covers case when sequence numbers on acceptor and initiator sides
                                                    * are reset at specific time on both sides. It should be used in
                                                    * INITIAL, NON_GRACEFULLY_TERMINATED and CORRECTLY_TERMINATED state.
                                                    */
            RESET_SEQNUM_AND_SEND_LOGON_STRATEGY    /*! Strategy to reset sequence using ResetSeqNumFlag flag that indicates
                                                    * both sides of a FIX session should reset sequence numbers.
                                                    * @warning cannot be used with FIX40.
                                                    */
        };

        /**
        * Possible states of Session.
        *
        * @see Session::state2string(State state).
        */
        enum State {
            ESTABLISHED, ///< The session is fully established
            INITIAL,     ///< The session has been created, but has not been connected yet
            NON_GRACEFULLY_TERMINATED, ///< The session has been non-gracefully terminated
            RECONNECT,                 ///< The session-initiator has detected the telecommunication link error and is trying to re-establish the link
            CORRECTLY_TERMINATED,      ///< The session has been correctly terminated
            WAIT_FOR_CONFIRM_LOGON,    ///< The session has been connected as an Initiator, the first Logon message has been sent and it is waiting for the conforming Logon message
            WAIT_FOR_CONFIRM_LOGOUT,   ///< Waiting for confirm logout state
            WAIT_FOR_FIRST_LOGON,      ///< The session has been connected as an Acceptor and is waiting for the first Logon message
            SWITCH_CONNECTION,         ///< The session switch to the another connection (backup or primary)
            WAIT_FOR_FIRST_HELLO,      ///< The session has been connected as an Acceptor and is waiting for the first Hello message
            WAIT_FOR_CONFIRM_HELLO,    ///< The session has been connected as an Initiator, the first FAST Hello message has been sent and it is waiting for the conforming Hello message
            WAIT_FOR_CONNECT           ///< The session-initiator is waiting fo async connect to complete
        };
    public:
        /**
        * Establishes the FIX session.
        * @param sessionRole - session role to connect as. Default  - NA_SESSION_ROLE means no role override use pre-configured via session extra parameters or engne.properties role.
        */
        virtual void connect(SessionRole sessionRole = NA_SESSION_ROLE ) = 0;

        /**
        * Establishes the FIX session as Initiator.
        *
        * @param HBI Heartbeat interval (seconds)
        * @param host remote FIX engine's host name or address.
        * @param port remote FIX engine's port.
        * @param encryption  Encrypt method.
        * @param backupConn - parameters of the backup connection
        * @param activeConn - specifies which connection will be used, by default primary connection
        */
        virtual void connect( int HBI,
                              const std::string& host,
                              int port,
                              const SessionBackupParameters* backupConn = NULL,
                              ActiveConnection activeConn = PRIMARY_CONNECTION ) = 0;

        /**
        * Establishes the FIX session as Initiator using the custom Logon message.
        *
        * @param HBI Heartbeat interval (in seconds)
        * @param customLogonMsg the custom Logon message.
        * @param host remote FIX engine's host name or address.
        * @param port remote FIX engine's port.
        * @param encryption  Encrypt method.
        * @param backupConn - parameters of the backup connection
        * @param activeConn - specifies which connection will be used, by default primary connection
        */
        virtual void connect( int HBI,
                              const FIXMessage& customLogonMsg,
                              const std::string& host,
                              int port,
                              const SessionBackupParameters* backupConn = NULL,
                              ActiveConnection activeConn = PRIMARY_CONNECTION ) = 0;

        /**
        * Establishes the FIX session over FIX_TCP underlying protocol.
        * @param protocolConfig - set of the FIX_TCP protocol parameters.
        * @param backupConn - parameters of the backup connection
        * @param activeConn - specifies which connection will be used, by default primary connection */
        virtual void connect( const FixTcpParameters& protocolConfig, 
                              const FixTcpBackupParameters* backupConn = NULL,
                              ActiveConnection activeConn = PRIMARY_CONNECTION ) = 0;

        /**
        * Establishes the FIX session over FIXT11_TCP underlying protocol.
        * @param protocolConfig - set of the FIXT11_TCP protocol parameters.
        * @param backupConn - parameters of the backup connection 
        * @param activeConn - specifies which connection will be used, by default primary connection */
        virtual void connect( const FixT11TcpParameters& protocolConfig, 
                              const FixT11TcpBackupParameters* backupConn = NULL,
                              ActiveConnection activeConn = PRIMARY_CONNECTION ) = 0;

        /** Returns type of the underlying protocol */
        virtual UnderlyingProtocol getUnderlyingProtocol() const = 0;

        /**
        * Initiates disconnect of the session.
        *
        * @param forcefullyMarkAsTerminated if true then the session will be marked as correctly terminated and persistent
        *                                   information about it will not be used during the subsequent connections.
        *
        * @note the forcefullyMarkAsTerminated parameter is used only if the IntradayLogoutTolerance configuration
        * option is "false".
        */
        virtual void disconnect( bool forcefullyMarkAsTerminated = false ) = 0;

        /**
        * Initiates disconnect of the session with the given logout text.
        *
        * @param forcefullyMarkAsTerminated if true then the session will be marked as correctly terminated and persistent
        *                                    information about it will not be used during the subsequent connections.
        * @param logoutText Reason of disconnecting
        *
        * @note the forcefullyMarkAsTerminated parameter is used only if the IntradayLogoutTolerance configuration
        * option is "false".
        */
        virtual void disconnect( const std::string& logoutText, bool forcefullyMarkAsTerminated = false ) = 0;

        /**
        * Initiates synchronous disconnect of the session.
        *
        * @param forcefullyMarkAsTerminated if true then the session will be marked as correctly terminated and persistant
        * information about it will not be used during the subsequent connections.
        *
        * @note the forcefullyMarkAsTerminated parameter is used only if the IntradayLogoutTolerance configuration
        * option is "false".
        */
        virtual void disconnectSync( bool forcefullyMarkAsTerminated = false );

        /**
        * Initiates synchronous disconnect of the session with the given logout text.
        *
        * @param forcefullyMarkAsTerminated if true then the session will be marked as correctly terminated and persistant
        * @param logoutText Reason of disconnecting
        * information about it will not be used during the subsequent connections.
        *
        * @note the forcefullyMarkAsTerminated parameter is used only if the IntradayLogoutTolerance configuration
        * option is "false".
        */
        virtual void disconnectSync( const std::string& logoutText, bool forcefullyMarkAsTerminated = false );

        /**
        * Closes the telecommunication link and switches the session into the NON_GRACEFULLY_TERMINATED state.
        */
        virtual void disconnectNonGracefully() = 0;

        /** Switch session to the another connection. When current is a primary - switch to the backup.
        *  When current is a backup - switch to the primary.
        */
        virtual void switchConnection() = 0;

        /**
        * Resets sequence numbers according to chosen strategy.
        * @see Engine::Session::SeqNumResetStrategy
        */
        virtual void resetSeqNum( SeqNumResetStrategy strategy = RESET_SEQNUM_AND_SEND_LOGON_STRATEGY )  = 0;

        /**
        * Registers Application. When any application callback executed - new Application coudn't be registered immediately.
        * Session will tries to register new application during delay*triesAmount period.
        *
        * @param pNewApplication Instance of class derived from Application that handles session's events.
        * @param delay - the time delay in microseconds when session tries to registrate new application
        * @param maxTries - the amount of attempts during session tries to registrate new application.
        *                '-1' means INFINITE attempt.
        * @warning It is vital to call 'registerApplication(NULL)' before calling the 'release' method.
        *
        * @return a pointer to the old application.
        */
        virtual Application* registerApplication( Application* pNewApplication, unsigned int delay = 300, int maxTries = -1 ) = 0;

        /**
        * Sends the given message to the remote FIX engine.
        * After Session::put is return, message can be destroyed by user.
        * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
        */
        virtual void put( FIXMessage* pMsg, PutMessageOptions const* options = NULL ) = 0;

        /**
        * Sets static session level fields in the message and reserves space for volatile session level fields.
        * @param[out] pMsg Pointer to the FIXMessage to store field values
        * @param[in] msgSeqNumSize Number of characters for MsgSeqNum field to reserve.
        * @param[in] options Extra parameters. @see Engine::PutMessageOptions.
        */
        virtual void inflate( FIXMessage* pMsg, int msgSeqNumSize, PutMessageOptions const* options = NULL ) const = 0;

        /**
        * Sends the given message to the remote FIX engine.
        * After Session::put is return, message can be destroyed by user.
        * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
        */
        virtual void put( PreparedMessage* pMsg ) = 0;

        /**
        * Sends the list of given messages to the remote FIX engine in one packet.
        * After Session::put is return, message can be destroyed by user.
        * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
        */
        virtual void put( const std::list<PreparedMessage*> &lMsgs )
        {
			//assert(false);
			(void)lMsgs;
			throw std::logic_error("Not implemented");
		}

        /**
        * Sends the given message to the remote FIX engine.
        *
        * @param msgHeader the message header in the native FIX format.
        * @param msgBody the message body in the native FIX format.
        *
        * @warning msgHeader and msgBody must be terminated by the FIX delimiter character (0x001).
        * @warning msgHeader must contain at least the 'BeginString' and 'MsgType' fields.
        *
        * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
        */
        virtual void put( const std::string& msgHeader, const std::string& msgBody ) = 0;

        /**
        * Returns session's state;
        *
        * @see Session::state2string(State).
        */
        virtual State getState() const = 0;

        /**
        * Returns current session's state as std::string;
        *
        * @see Session::getState()
        */
        virtual const char* getStateAsString() const = 0;

        /**
        * Returns the string representation of the given state value.
        */
        static const char* state2string( State state );

        /**
        * Converts MessageStorageType to string.
        */
        static const char* messageStorageTypeToString( MessageStorageType const & value );

        /**
        * Returns the string representation of the given Role value.
        */
        static const char* role2string( SessionRole role );

        /**
        * Converts string to MessageStorageType.
        */
        static MessageStorageType stringToMessageStorageType( char const* value );

        /**
        * Converts string to MessageStorageType. Case insensitive.
        */
        static MessageStorageType stringToMessageStorageTypeCaseI( char const* value );

        /**
        * Returns the std::string representation of the given connection state value.
        */
        static const char* connState2string( ActiveConnection aState );

        /**
        * Forcedly sets the value of the incoming sequence number (the expected MsgSeqNum of the next incoming message).
        */
        virtual void setInSeqNum( int seqNum ) = 0;

        /**
        * Forcedly sets the value of the outgoing sequence number (MsgSeqNum of the next outgoing message).
        */
        virtual void setOutSeqNum( int seqNum ) = 0;

        /**
        * Forcedly sets the value of the Sender Location ID.
        */
        virtual void setSenderLocationID( const std::string& id ) = 0;
        /**
        * Forcedly sets the value of the Target Location ID.
        */
        virtual void setTargetLocationID( const std::string& id ) = 0;

        /**
        * Forcedly sets the value of the Sender Sub ID.
        */
        virtual void setSenderSubID( const std::string& id ) = 0;
        /**
        * Forcedly sets the value of the Target Sub ID.
        */
        virtual void setTargetSubID( const std::string& id ) = 0;

        /**
        * Forcedly sets the value of the Enable Message Rejecting.
        */
        virtual void setEnableMessageRejecting( bool enable ) = 0;

        /**
        * Forcedly sets the value of the Intraday Logout Tolerance Mode.
        */
        virtual void setIntradayLogoutToleranceMode( bool enable ) = 0;

        /**
        * Forcedly sets the value of the Forced Reconnect.
        */
        virtual void setForcedReconnect( bool enable ) = 0;

        /**
        * Returns SenderCompID.
        */
        virtual const std::string* getSender() const = 0;

        /**
        * Returns TargetCompID.
        */
        virtual const std::string* getTarget() const = 0;

        /**
        * Returns reference to SessionId.
        */
        virtual const SessionId& getSessionId() const = 0;

        /**
        * Returns Heartbeat interval (HeartBtInt) in seconds.
        */
        virtual int getHBI() const = 0;

        /**
        * Returns unique parser identifier of the session.
        ** @deprecated Use Engine::Session::parserID().
        */
        B2B_DEPRECATED( "Use Engine::Session::parserID()." )
        ParserID protocolID() const;

        /**
        * Returns unique parser identifier of the session.
        */
        virtual ParserID parserID() const = 0;

        /**
        * Returns the FIX protocol version.
        */
        virtual FIXVersion getVer() const = 0;

        /**
        * Returns the FIX application protocol version.
        */
        virtual FIXVersion getAppVer() const = 0;

        /**
        * Returns the next expected incoming sequence numbers for this FIX session
        */
        virtual int getInSeqNum() const = 0;

        /**
        * Returns the outgoing sequence numbers for this FIX session
        * (MsgSeqNum of the next outgoing message).
        */
        virtual int getOutSeqNum() const = 0;

        /**
        * Returns a session storage type
        */
        virtual MessageStorageType getStorageType() const = 0;

        /**
        * Sends Test Request message.
        *
        * @param testReqID The TestReqID field (tag = 112) - Identifier included in
        * Test Request message to be returned in resulting Heartbeat message.
        * If empty then a timestamp std::string will be used as the value.
        */
        virtual void ping( const std::string& testReqID = "" ) = 0;

        /**
        * Returns the remote FIX Engine's port number.
        */
        virtual int getRemotePort() const = 0;

        /**
        * Returns the remote FIX Engine's host name.
        *
        * @throw Utils::Exception if no data is available
        */
        virtual const std::string* getRemoteHost() const = 0;


        /**
        * Returns the remote FIX Engine's IP Address.
        *
        * @throw Utils::Exception if no data is available
        */
        virtual System::InetAddress getRemoteAddress() const = 0;

        /**
        * Returns the remote FIX Engine's IP Address.
        *
        * @throw Utils::Exception if no data is available
        */
        virtual System::IPAddr getRemoteHostIP() const;

        /**
        * Returns session's role.
        */
        virtual Role getRole() const = 0;

        /**
         * Returns current connection type
         */
        virtual ActiveConnection getActiveConnectionType() const = 0;

        /**
        * Returns the session's ID in the format 'SenderCompID 0x01 TargetCompId' or .
        * in format 'SenderCompID 0x01 TargetCompId 0x01 qualifier' if session qualifier is available.
        */
        virtual const std::string* getId() const = 0;

        /// Returns the encryption method.
        virtual Engine::EncryptMethod getEncryptionMethod() const = 0;

        /**
        * Enables or disables TCP buffer
        * @param enable - when true, TCP buffer will be enabled.
        * @return true, if TCP buffer was enabled before.
        */
        virtual bool setTCPBuffer( bool enable ) = 0;

        virtual bool isTCPBuffer()const = 0;

        /**
        * Retrieves messages received by the session.
        * @param pMessages List of messages.
        * @param msgFilter specifies selection criteria.
        *
        * @warning It's caller's responsibility to release returned messages.
        */
        virtual void getReceivedMessages( std::vector<FIXMessage*>* pMessages, const MsgFilter& msgFilter ) const = 0;

        /**
        * Returns message storage base file path. Base file path is path to the file set without extension.
        * @return Returns message storage base file path
        */
        virtual const std::string getMsgStorageBaseFilePath() const = 0;

        /** Extends session interface */
        virtual SessionEx* getSessionEx();

        /**
         * Searches for a message by sequence number in the outgoing storage.
         * @return Pointer to FIXMessage is message was found; NULL otherwise.
         */
        virtual FIXMessage* locateSentMessage( int seqNum )const = 0;

        /**
         * Creates instance of the FIX message.
         */
        virtual FIXMessage* newSkel( const char* msgType, Engine::FIXVersion app_ver = Engine::NA ) const = 0;

        /**
         * Creates instance of the FIX message.
         */
        virtual FIXMessage* parse( std::string const& raw ) const = 0;

        /**
        * Returns std::string representation of primary session parameters
        * @return std::string representation of primary session parameters or NULL if parameters not exist
        */
        virtual std::auto_ptr<SessionExtraParameters> getPrimaryParameters() const = 0;

        /**
        * Returns std::string representation of backup session parameters
        * @return std::string representation of backup session parameters or NULL if parameters not exist
        */
        virtual std::auto_ptr<SessionBackupParameters> getBackupParameters() const = 0;

        /**
        * Returns std::string representation of active session parameters
        * @return std::string representation of active session parameters or NULL if parameters not exist
        */
        virtual std::auto_ptr<SessionExtraParameters> getActiveParameters() const = 0;

        /**
         * Close message storage.
         * Method forcedly closes storage if session is in the NGT/CT state.
         */
        virtual void closeMessageStorage() = 0;

        /**
         * Returns size of the outgoing message queue.
         * @return size of the outgoing message queue.
         */
        virtual std::size_t outgoingQueueSize() const = 0;

        /**
         * Reset messages storage, so nothing will be send on ResendRequest
         * @deprecated Use Engine::Session::resetSeqNum method
         */
        B2B_DEPRECATED("Session::resetMessageStorage is deprecated. Use Session::resetSeqNum instead.")
        virtual void resetMessageStorage() = 0;

        /**
         * Stores given message to outgoing storage and sends to socket.
         * Method does not fill session level fields (MsgSeqNum, SenderCompID, etc)
         * After Session::putAsIsNoLock is return, message can be destroyed by user.
         *
         * @param msg message to send
         *
         * @param seqNum Given message sequence number. Session does not update
         *               message's sequence number.
         * @param updateOutSeqNum If true, session will update internal outgoing sequence number.
         *
         * @note Session::lock must be called to use this method.
         *
         * @warning It is user's responsibility to set all session level fields like
         *          SendingTime, SenderCompID, TargetCompID, etc.
         *
         * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
         */
        virtual void putAsIsNoLock( FIXMessage* msg, int seqNum, bool updateOutSeqNum ) = 0;

        /**
         * Stores given message to outgoing storage and sends to socket.
         * Method does not fill session level fields (MsgSeqNum, SenderCompID, etc)
         * After Session::putAsIsNoLock is return, message can be destroyed by user.
         *
         * @param msg message to send
         *
         * @param seqNum Given message sequence number. Session does not update
         *               message's sequence number.
         * @param updateOutSeqNum If true, session will update internal outgoing sequence number.
         *
         * @note Session::lock must be called to use this method.
         *
         * @warning It is user's responsibility to set all session level fields like
         *          SendingTime, SenderCompID, TargetCompID, etc.
         *
         * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
         */
        virtual void putAsIsNoLock( PreparedMessage* msg, int seqNum, bool updateOutSeqNum ) = 0;

        /**
         * Stores given message to outgoing storage and sends to socket.
         * Method does not fill session level fields (MsgSeqNum, SenderCompID, etc)
         * After Session::putAsIs returns, message can be destroyed by user.
         *
         * @param msg message to send
         *
         * @param seqNum Given message sequence number. Session does not update
         *               message's sequence number.
         * @param updateOutSeqNum If true, session will update internal outgoing sequence number.
         *
         * @warning It is user's responsibility to set all session level fields like
         *          SendingTime, SenderCompID, TargetCompID, etc.
         *
         * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
         */
        virtual void putAsIs( FIXMessage* msg, int seqNum, bool updateOutSeqNum ) = 0;

        /**
         * Stores given message to outgoing storage and sends to socket.
         * Method does not fill session level fields (MsgSeqNum, SenderCompID, etc)
         * After Session::putAsIs returns, message can be destroyed by user.
         *
         * @param msg message to send
         *
         * @param seqNum Given message sequence number. Session does not update
         *               message's sequence number.
         * @param updateOutSeqNum If true, session will update internal outgoing sequence number.
         *
         * @warning It is user's responsibility to set all session level fields like
         *          SendingTime, SenderCompID, TargetCompID, etc.
         *
         * @throw Method throws Utils::Exception in case message is incorrect or session is closed.
         */
        virtual void putAsIs( PreparedMessage* msg, int seqNum, bool updateOutSeqNum ) = 0;

        /**
         * Locks session's mutex.
         */
        virtual void lock() = 0;

        /**
         * UnLocks session.
         */
        virtual void unlock() = 0;

        /**
         * Sets the value of the outgoing sequence number (MsgSeqNum of the next outgoing message).
         * @note Session::lock must be called to use this method.
         */
        virtual void setOutSeqNumNoLock( int seqNum ) = 0;

        /**
         * Returns the outgoing sequence numbers for this FIX session
         * (MsgSeqNum of the next outgoing message).
         * @note Session::lock must be called to use this method.
         */
        virtual int getOutSeqNumNoLock() const = 0;

        /** Attaches session state observer */
        virtual void attachStateObserver( SessionStateObserver* observer ) = 0;

        /** Detaches session state observer */
        virtual void detachStateObserver() = 0;

        /**
         * Assigns Session with Event object to notify user about session destruction.
         */
        virtual void setSessionDestructionEvent( System::ReferenceableManualEvent* event ) throw() = 0;

        /**
         * Blocks execution until all threads accittiated with Session are stopped.
         */
        virtual void waitForTerminated() throw() = 0;

        /** 
        * Returns storage creation time as number of milliseconds since 1970.01.01
        * If no storage is assigned with Session, method returns 0
        */
        virtual System::u64 getStorageCreationTime() const = 0;

		/**
        * Returns current internal state for this FIX session
        */
		virtual std::auto_ptr<InternalState> getInternalState() const = 0;

		/**
        * Forcedly sets the internal state
        */
		virtual void setInternalState( InternalState* state ) = 0;

        /** 
        * Returns Session's Lite statistics.
        */
        virtual void getStatistics( FixSessionStatisticsLite& statistics) const = 0;

    protected:
        /** Constructor. */
        Session() {}

        /**
        * Destructor.
        *
        * @note It is necessary to call the 'release()' method to free the resources used by this session.
        */
        virtual ~Session() {}

    private:
        /** Copy constructor is prohibited. */
        Session( const Session& );

        /** Assignment operator is prohibited. */
        Session& operator = ( const Session& );
    };

} // namespace Engine

#endif


