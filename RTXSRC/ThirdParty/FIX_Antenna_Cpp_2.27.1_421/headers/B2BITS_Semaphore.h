// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Semaphore.h
/// Contains System::Semaphore class declaration.

#ifndef _B2BITS_SEMAPHORE__H
#define _B2BITS_SEMAPHORE__H

#include <B2BITS_V12_Defines.h>
#include "B2BITS_SystemDefines.h"

#ifndef _WIN32
#   include <semaphore.h>
#endif

/**
 * Platform specific classes.
 */
namespace System
{

    /**
     * Semaphore - a general synchronization mechanism.
     */
    class V12_API Semaphore B2B_SEALED
    {
    public:
        /**
         * Constructor. Sets the semaphore's value to 0.
         */
        Semaphore();

        /**
         * Constructor. Sets the semaphore to the given value.
         * @param aValue Value to initialize semaphore with.
         */
        Semaphore( int aValue );

        /**
         * Destructor.
         */
        ~Semaphore();

        /**
         * Unlocks the semaphore.
         *
         * If the semaphore value resulting from this operation  is
         * positive, then no threads were blocked waiting for the
         * semaphore to become unlocked; the semaphore value is simply
         * incremented.
         *
         * If the value of the semaphore resulting from this operation
         * is 0, then one of the threads blocked waiting for the semaphore
         * will be allowed to return successfully from its call to wait().
         *
         * This method is reentrant with respect to signals
         * and may be invoked from a signal-catching function.
         */
        void post();

        /**
         * Locks the semaphore.
         *
         * If the semaphore value is currently zero, then the calling thread
         * will not return from this method until it either locks
         * the semaphore or the call is interrupted by a signal.
         */
        void wait();

    private:
        Semaphore( const Semaphore& );
        Semaphore& operator = ( const Semaphore& );

#ifndef _WIN32
        sem_t m_sem;
#else // _WIN32
        HANDLE m_sem;
#endif
    };

}
#endif
