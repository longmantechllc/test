/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/


#ifndef __Properties_h__
#define __Properties_h__

#include <string>
#include <map>
#include <iosfwd>

namespace Aux
{

    class Properties
    {

    public:
        static const char* const MSGFILE;
        static const char* const VERSION;
        /// SenderCompID key
        static const char* const SENDER;
        /// TargetCompID key
        static const char* const TARGET;
        static const char* const HOST;
        static const char* const PORT;
        static const char* const INSEQNUM;
        static const char* const OUTSEQNUM;
        static const char* const SESSIONPROTOCOL;
        /// SenderSubID key
        static const char* const SENDERSUB;
        /// TargetSubID key
        static const char* const TARGETSUB;
        /// SenderLocationID key
        static const char* const SENDERLOC;
        /// TargetLocationID key
        static const char* const TARGETLOC;
        static const char* const INTERACTIVERESEND;
        static const char* const HBI;
        static const char* const LOGOUTTOLERANCE;
        static const char* const FORCERECONNECT;
        static const char* const IGNORESEQNUMTOOLOW;
        static const char* const REJECTWHILENOCONN;
        static const char* const KEEPSTATE;
        static const char* const FORCESEQNUMRESET;
        static const char* const TCPBUFFERDISABLE;
        static const char* const MAXMESSAGESINBUNCH;
        static const char* const AGGRESSIVESEND;
        static const char* const USERNAME;
        static const char* const PASSWORD;
        static const char* const HIDDENLOGONCREDENTIALS;

        static const char* const ACTIVE_CONNECTION;
        static const char* const AUTO_SWITCH_BACKUP;
        static const char* const CYCLIC_SWITCH_BACKUP;
        static const char* const CUSTOM_LOGON;

        static const char* const BACKUP_HOST;
        static const char* const BACKUP_PORT;
        /// SenderSubID key
        static const char* const BACKUP_SENDERSUB;
        /// TargetSubID key
        static const char* const BACKUP_TARGETSUB;
        /// SenderLocationID key
        static const char* const BACKUP_SENDERLOC;
        /// TargetLocationID key
        static const char* const BACKUP_TARGETLOC;
        static const char* const BACKUP_HBI;
        static const char* const BACKUP_LOGOUTTOLERANCE;
        static const char* const BACKUP_FORCERECONNECT;
        static const char* const BACKUP_IGNORESEQNUMTOOLOW;
        static const char* const BACKUP_REJECTWHILENOCONN;
        static const char* const BACKUP_AUTO_SWITCH_BACKUP;
        static const char* const BACKUP_CYCLIC_SWITCH_BACKUP;

        /**
         * Constructor.
         * Reads a property list (key and element pairs) from the input stream.
         *
         * A line that contains only whitespace or whose first non-whitespace
         * character is an ASCII # or ! is ignored
         * (thus, # or ! indicate comment lines).
         *
         * Note: If required property is not found the default value is used.
         */
        Properties( std::istream* apIStream );

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter
         *
         * @throw Exception if such key does not exist.
         */
        std::string getString( const std::string& key ) const;

        bool isPropertyExists( const std::string& key )const;

        /**
         * Searches for the property with the specified key in this property list.
         * @param key out parameter; if property holds incorrect value will be
         *  set to 0.
         *
         * @throw Exception if such key does not exist.
         */
        int getInteger( const std::string& key ) const;

        bool getBool( const std::string& key ) const;

    private:
        typedef std::map<std::string, std::string> MapType;
        MapType m_pairs;
    };

}

#endif // __Properties_h__


