// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_NullableValue.h
/// Contains NullableValue template class definition

#ifndef B2BITS_Utils_NullableValue_h
#define B2BITS_Utils_NullableValue_h

#include <cassert>
#include <ostream>

#include "B2BITS_V12_Defines.h"
#include "B2BITS_Exception.h"
#include "B2BITS_Empty.h"

namespace std {
    
}

/// FIX Antenna Utils framework
namespace Utils
{

    // Forward declaration
    template <typename T> class Emptily;

    /// Enumerates state of the NullableValue
    enum ValueState { vsUndefined, vsEmpty, vsAssigned };

    /// Represents entity with three states - undefined, empty and assigned
    template <typename T>
    class NullableValue
    {
    public:
        /// Type of the stored value
        typedef T value_type;

        /// Self type
        typedef NullableValue<value_type> _Myt;

        /// Conversion constructor
        NullableValue( value_type value )
            : state_( vsAssigned ),
              value_( value )
        {}

        /// Conversion constructor
        NullableValue( const Emptily<value_type>& value ) {
            copy( value );
        }

        /// Conversion constructor
        NullableValue( EmptyValue )
            : state_( vsEmpty ) {
        }

        /// Conversion constructor
        NullableValue( UndefinedValue )
            : state_( vsUndefined ) {
        }

        /// Generic conversion constructor
        template< typename Other >
        NullableValue( const Other& value )
            : state_( vsAssigned ),
              value_( value )
        {}

        /// Generic copy constructor
        template< typename Other >
        NullableValue( const Emptily<Other>& value )
            : state_( value.isEmpty() ? vsEmpty : vsAssigned ),
              value_( vsAssigned == state_ ?  *value : value_type() )
        {}

        /// Generic copy constructor
        template< typename Other >
        NullableValue( const NullableValue<Other>& value )
            : state_( value.state() ),
              value_( vsAssigned == state_ ? *value : value_type() )
        {}

        /// Default constructor
        NullableValue()
            : state_( vsUndefined )
        {}

        /// Returns true if value assigned with empty value
        bool isEmpty() const {
            return vsEmpty == state_;
        }

        /// Returns true if value assigned
        bool isDefined() const {
            return vsUndefined != state_;
        }

        /// Returns true if value assigned and is not empty
        bool hasValue() const {
            return vsAssigned == state_;
        }

        /// Returns state of the object
        ValueState state() const {
            return state_;
        }

        /// Assign operator
        _Myt& operator=( const value_type& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        _Myt& operator=( const _Myt& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        _Myt& operator=( const Emptily<value_type>& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        template< typename Other >
        _Myt& operator=( const Other& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        _Myt& operator=( UndefinedValue ) {
            state_ = vsUndefined;
            return *this;
        }

        /// Assign operator
        _Myt& operator=( EmptyValue ) {
            state_ = vsEmpty;
            return *this;
        }

        /// Assign operator
        template< typename Other >
        _Myt& operator=( const Emptily<Other>& value ) {
            copy( value );
            return *this;
        }

        /// Assign operator
        template< typename Other >
        _Myt& operator=( const NullableValue<Other>& value ) {
            copy( value );
            return *this;
        }

        /// Resets object to undefined state
        void reset() {
            state_ = vsUndefined;
            //value_ = value_type();
        }

        /// Compares two objects
        bool equals( const _Myt& rv ) const {
            if( this == &rv ) {
                return true;
            }

            return state_ == rv.state_ && ( state_ != vsAssigned || value_ == rv.value_ );
        }

        /// Returns stored value
        /// If value is undefined or empty, std::exception is raised
        value_type* get() {
            if( state_ != vsAssigned ) {
                throw Utils::Exception( "Cannot return value. Value is not defined" );
            }

            return &value_;
        }

        /// Returns stored value
        /// If value is undefined or empty, std::exception is raised
        const value_type* get() const {
            if( state_ != vsAssigned ) {
                throw Utils::Exception( "Cannot return value. Value is not defined" );
            }

            return &value_;
        }

        /// Returns stored value
        /// If value is undefined or empty, std::exception is raised
        value_type& operator*() {
            return *get();
        }
        /// Returns stored value
        /// If value is undefined or empty, std::exception is raised
        const value_type& operator*() const {
            return *get();
        }

        /// Selector operator
        value_type* operator->() {
            return get();
        }

        /// Selector operator
        const value_type* operator->() const {
            return get();
        }

        /// Compare operator
        bool operator != ( const _Myt& rv ) const {
            return !equals( rv );
        }

        /// Compare operator
        bool operator == ( const _Myt& rv ) const {
            return equals( rv );
        }

    private:
        void copy( const Emptily<value_type>& value ) {
            if( value.isEmpty() ) {
                state_ = vsEmpty;
                //value_ = value_type();
            } else {
                value_ = *value;
                state_ = vsAssigned;
            }
        }

        void copy( const value_type& value ) {
            value_ = value;
            state_ = vsAssigned;
        }

        void copy( const _Myt& value ) {
            state_ = value.state_;
            value_ = value.value_;
        }

        template< typename Other >
        void copy( const Other& value ) {
            value_ = value;
            state_ = vsAssigned;
        }

        template< typename Other >
        void copy( const NullableValue<Other>& value ) {
            state_ = value.state();
            if ( vsAssigned == value.state() ) {
                value_ =  *value;
            }
        }

        template< typename Other >
        void copy( const Emptily<Other>& value ) {
            if( value.isEmpty() ) {
                state_ = vsEmpty;
                //value_ = value_type();
            } else {
                value_ = *value;
                state_ = vsAssigned;
            }
        }

        NullableValue( ValueState s ): state_( s ) {}

        ValueState state_;
        value_type value_;
    };

    template<class Elem, class Traits, typename T >
    inline std::basic_ostream<Elem, Traits>& operator << ( std::basic_ostream<Elem, Traits>& s, const NullableValue<T>& rv )
    {
        if( !rv.isDefined() ) {
            return s << "(undefined)";
        } else if( rv.isEmpty() ) {
            return s << "(empty)";
        } else {
            return s << *rv;
        }
    }
}
#endif // B2BITS_Utils_NullableValue_h

