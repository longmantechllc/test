#pragma once

#include <string>
#include <memory>
#include <unordered_set>
#include <vector>
#include <atomic>
#include <zmq.hpp>
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included

class DAMDCache;

// https://stackoverflow.com/questions/47913054/boostasioasync-write-ensure-only-one-outstanding-call#:~:text=boost%3A%3Aasio%3A%3Aasync_write%20-%20ensure%20only%20one%20outstanding%20call.%20According,operations%20that%20perform%20writes%29%20until%20this%20operation%20completes.%22
// https://www.codeproject.com/Articles/1264257/Socket-Programming-in-Cplusplus-using-boost-asio-T
class Acceptor {
public:
    Acceptor(int port, DAMDCache *DAMDcache)
        : m_context(1), m_socket(m_context, ZMQ_REP)
            //m_io{}, m_acceptor(m_io, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port))
          , m_DAMDcache(*DAMDcache)
    {
        //start_accept();
        spdlog::info("Listening for incoming connection...\n");
        std::string add("tcp://*:");
        add.append(std::to_string(port));
        m_socket.bind(add);
        m_run = true;
    }
    ~Acceptor() { stop();  /*google::protobuf::ShutdownProtobufLibrary();*/}
    Acceptor(const Acceptor &) = delete;
    Acceptor(Acceptor &&) = delete;
    Acceptor & operator = (const Acceptor &) = delete;
    Acceptor & operator = (Acceptor &&) = delete;
    //static void run(int port, DAMDCache *DAMDcache);
    //void OnConnectionClosed(con_handler::pointer connection);
    void start();
    void stop();
private:

    //void start_accept();
    //void handle_accept(con_handler::pointer connection, const boost::system::error_code& err);

    zmq::context_t m_context;
    zmq::socket_t m_socket;
    //boost::asio::io_service m_io;
    //boost::asio::ip::tcp::acceptor m_acceptor;
    //std::unordered_set<con_handler::pointer> active_connections_;
    DAMDCache& m_DAMDcache;
    std::atomic<bool> m_run {false};
};

