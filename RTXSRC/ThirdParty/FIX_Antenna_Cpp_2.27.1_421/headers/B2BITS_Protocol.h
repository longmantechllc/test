// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Protocol.h
/// Contains FixDictionary2::Protocol class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_Protocol_H
#define H_B2BITS_FixDictionary2_Protocol_H

#include <map>
#include <string>
#include <list>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class Protocol
    *   @brief Represents Protocol interface.
    */
    class V12_API Protocol B2B_SEALED
        : public Item
    {
    public:
        /** Messages by type map */
        typedef std::map<std::string, MessageT::Ref> MessagesByTypeMap;
        typedef std::map<std::string, BlockT::Ref> BlocksByIDMap;

    public:
        /** Constructor. */
        Protocol( std::string const& name, std::string const& version, std::string const& title );

        /** Makes shallow copy of object */
        ProtocolT::Ptr shallowClone( std::string const& name, std::string const& title ) const;

    public: // Public methods
        /**
        * Stores given protocol into XML file.
        * Created file can be loaded later by Dictionary.
        */
        void save( std::string const& filePath ) const;

        /** Returns name of the Protocol, unique identifier.
        * @note "name" is "id" in the FIX XML
        * @return name of the Protocol
        */
        std::string const& name() const throw();

        /** Returns name of the Protocol, unique identifier.
        * @note "name" is "id" in the FIX XML
        * @return name of the Protocol
        */
        std::string const& getName() const throw() {
            return name();
        }

        /** Returns name of the Protocol, unique identifier.
        * @note "name" is "id" in the FIX XML
        * @return name of the Protocol
        */
        std::string const& id() const throw() {
            return name();
        }

        /** Returns name of the Protocol, unique identifier.
        * @note "name" is "id" in the FIX XML
        * @return name of the Protocol
        */
        std::string const& getId() const throw() {
            return id();
        }

        /** Returns name of Protocol
        *
        * @return name of Protocol
        */
        std::string const& title() const throw();

        /** Returns name of Protocol
        *
        * @return name of Protocol
        */
        std::string const& getTitle() const throw() {
            return title();
        }

        /** Returns base type of Protocol
        *
        * @return base type of Protocol
        */
        std::string const& version() const throw();

        /** Returns base type of Protocol
        *
        * @return base type of Protocol
        */
        std::string const& getVersion() const throw() {
            return version();
        }

        /** Finds FieldType in protocol by name
        *
        * @param name of FieldType to find
        *
        * @return found FieldType; NULL if field type is not found.
        */
        FieldTypeT::CPtr getFieldType( std::string const& name ) const throw();

        /** Finds ValBlock in protocol by id
        *
        * @param id of the ValBlock to find
        *
        * @return found ValBlock; null otherwise
        */
        ValBlockT::CPtr getValBlock( std::string const& id ) const throw();

        /** Find field in protocol by tag
        *
        * @param tag of field to find
        *
        * @return found tag; null otherwise
        */
        FieldT::CPtr getField( int tag ) const throw();

        /** Find field in protocol by name
        *
        * @param name Name of the field
        *
        * @return found tag; null otherwise
        */
        FieldT::CPtr getField( std::string const& name ) const throw();

        /** Find block in protocol by id
        *
        * @param id of block to find
        *
        * @return found block; null otherwise.
        */
        BlockT::CPtr getBlock( std::string const& id ) const throw();

        /** Find message in protocol by message type
        *
        * @param type of message to find
        *
        * @return found message; null otherwise.
        */
        MessageT::CPtr getMessage( std::string const& msgType ) const throw();

        /** Returns field's types registered in protocol
        *
        * @return field's types registered in protocol
        */
        std::map<std::string, FieldTypeT::Ref> const* getFieldTypes() const throw();

        /** Returns block's set used in protocol
        *
        * @return block's set used in protocol
        */
        BlocksByIDMap const* getBlocks() const throw();

        /** Returns fields registered in protocol
        *
        * @return fields registered in protocol
        */
        std::size_t getFields( FieldT::CRefArray* fields ) const throw();

        /** Returns messages registered in protocol
        *
        * @return messages registered in protocol
        */
        MessagesByTypeMap const* getMessages() const throw();

        /** Returns messages registered in protocol
        *
        * @return messages registered in protocol
        */
        std::map<std::string, ValBlockT::Ref> const* getValBocks() const throw();

        /** Returns description of the protocol */
        std::string const& getDescription() const throw();

        /** Assigns description of the protocol */
        void setDescription(std::string const& value);

        /** Returns description of the protocol */
        std::string const& description() const throw() {
            return getDescription();
        }

    public: // Item contract
        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

        /** Try downcast to Protocol class
        *
        * @return downcast to Protocol class
        */
        virtual ProtocolT::Ptr toProtocol() B2B_OVERRIDE;

        /** Try downcast to Protocol class
        *
        * @return downcast to Protocol class
        */
        virtual ProtocolT::CPtr toProtocol() const B2B_OVERRIDE;

        /** Make copy of object */
        ProtocolT::Ptr clone() const;

    public: // mutators
        /** Adds field type to the protocol */
        void addFieldType( FieldTypeT::Ptr type );

        /** Adds field types to the protocol */
        void addFieldTypes( std::list<FieldTypeT::Ref>* types );

        /** Adds field types to the protocol */
        void addFieldTypes( FieldTypeT::RefArray types, std::size_t size );

        /** Adds value block to the protocol */
        void addValBlock( ValBlockT::Ptr valBlock );

        /** Adds field to the protocol */
        void addField( FieldT::Ptr field );

        /** Returns field type by its name.
        * If field is not found, method returns null.
         */
        FieldTypeT::Ptr getFieldType( std::string const& name ) throw();

        /** Adds block to the protocol */
        void addBlock( BlockT::Ptr block );

        /** Adds message to the protocol */
        void addMessage( MessageT::Ptr msg );

        /** Replaces field type with given */
        void updateFieldType( FieldTypeT::Ptr type );

        /** Replaces value block with given */
        void updateValBlock( ValBlockT::Ptr valBlock );

        /** Replaces field with given */
        void updateField( FieldT::Ptr field );

        /** Removes field from the protocol */
        void removeField( int tag );

        /** Replaces block with given */
        void updateBlock( BlockT::Ptr block );

        /** Updates given message */
        void updateMessage( MessageT::Ptr msg );

        /** Removes given message */
        void removeMessage( std::string const & msgType );

    protected:
        /** Destructor */
        virtual ~Protocol() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;
    private:
        Protocol( std::string const& name, std::string const& version, std::string const& title, Impl* impl );

    }; // class Protocol

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_Protocol_H
