// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_AutoPtr.h
/// Contains Utils::AutoPtr class declaration.

#ifndef __FIX_Utils_B2BITS_AutoPtr_h__
#define __FIX_Utils_B2BITS_AutoPtr_h__

#include <cassert>
#include <cstddef>

#include <B2BITS_CompilerDefines.h>

/// FIX Antenna Utils framework
namespace Utils
{
    /// Returns pointer to the mutable method of the class
    template<typename T>
    struct ConstMethodResolver {
        typedef void ( T::*MethodType )();
    };

    /// Returns pointer to the const method of the class
    template< typename T >
    struct ConstMethodResolver< const T> {
        typedef void ( T::*MethodType )() const;
    };

    /// std::auto_ptr analogue with ability to delete object by object's method.
    /// @deprecated This class is not safe. Please use Utils::AutoPtr2 instead.
    template <typename T>
    class AutoPtr
    {
    public: // types
        /// Declares ReleaseMethod handler
        typedef typename ConstMethodResolver<T>::MethodType ReleaseMethod;

    public: // methods
        /// Default constructor.
        B2B_DEPRECATED( "This class is not safe. Please use Utils::AutoPtr2 instead." )
        AutoPtr() throw()
            : releaseMethod_( 0 )
            , ptr_( NULL ) {
            ( void )sizeof( T ); // type should be defined
        }

        /// Constructor.
        /// @param ptr Pointer to the object to be owned.
        /// @param releaseMethod Object's release method.
        B2B_DEPRECATED( "This class is not safe. Please use Utils::AutoPtr2 instead." )
        AutoPtr( T* ptr, ReleaseMethod releaseMethod ) throw()
            : releaseMethod_( releaseMethod )
            , ptr_( ptr ) {
            ( void )sizeof( T ); // type should be defined
        }

        /// Copy constructor.
        B2B_DEPRECATED( "This class is not safe. Please use Utils::AutoPtr2 instead." )
        AutoPtr( AutoPtr<T>& ptr ) throw()
            : releaseMethod_( 0 )
            , ptr_( NULL ) {
            ( void )sizeof( T ); // type should be defined

            reset( ptr.ptr_, ptr.releaseMethod_ );

            ptr.ptr_ = NULL;
            ptr.releaseMethod_ = 0;
        }

        /// Copy operator.
        AutoPtr& operator=( AutoPtr<T>& ptr ) throw() {
            ( void )sizeof( T ); // type should be defined

            reset( ptr.ptr_, ptr.releaseMethod_ );

            ptr.ptr_ = NULL;
            ptr.releaseMethod_ = 0;

            return *this;
        }

        /// Destructor.
        ~AutoPtr() throw() {
            ( void )sizeof( T ); // type should be defined

            if ( ptr_ != NULL ) {
                ( ptr_->*releaseMethod_ )();
            }
        }

        /// Release underlying pointer and return it.
        /// @return underlying pointer.
        T* release() throw() {
            ( void )sizeof( T ); // type should be defined

            releaseMethod_ = 0;

            T* ptr = ptr_;
            ptr_ = NULL;

            return ptr;
        }

        /// Deletes object by underlying pointer and set it to new value.
        /// @param ptr New value to set.
        /// @param releaseMethod Object's release method.
        void reset( T* ptr, ReleaseMethod releaseMethod ) throw() {
            ( void )sizeof( T ); // type should be defined

            if ( ptr_ != NULL ) {
                ( ptr_->*releaseMethod_ )();
            }

            ptr_ = ptr;
            releaseMethod_ = releaseMethod;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T const* get() const throw() {
            ( void )sizeof( T ); // type should be defined

            return ptr_;
        }

        /// Returns a pointer.
        /// @return Pointer.
        T* get() throw() {
            ( void )sizeof( T ); // type should be defined

            return ptr_;
        }

        /// Overloaded operator*.
        T& operator*() throw() {
            ( void )sizeof( T ); // type should be defined

            return *ptr_;
        }

        /// Overloaded operator*.
        T const& operator*() const throw() {
            ( void )sizeof( T ); // type should be defined

            return *ptr_;
        }

        /// Overloaded operator&.
        T** operator&() {
            ( void )sizeof( T ); // type should be defined

            return &ptr_;
        }

        /// Overloaded operator->.
        T* operator->() throw() {
            ( void )sizeof( T ); // type should be defined

            return get();
        }

        /// Overloaded operator->.
        T const* operator->() const throw() {
            ( void )sizeof( T ); // type should be defined

            return get();
        }

    private:
        /// Object's release method.
        ReleaseMethod releaseMethod_;

        /// Pointer to the owned object.
        T* ptr_;
    };
}

#endif
