#include <string>
#include <string_view>
#include <iostream>

#include <json_dto/pub.hpp>

#include <rapidjson/document.h>

#include <Utils/numeric.h>

const std::string_view json_simple_in {
    R"JSON(
    {
        "name":"Dennis",
        "age":"1234567890987654321",
        "salary":"12345.67"
    })JSON"
};

struct people_t
{
	std::string name = "";
	std::int64_t age = 0;
	double salary = 0.0;
};

namespace json_dto
{

template<typename Json_Io>
void json_io(Json_Io & io, people_t & m)
{
    std::string_view age_str = "";
    std::string_view salary_str = "";
	io & json_dto::mandatory("name", m.name)
		& json_dto::mandatory("age", age_str)
		& json_dto::mandatory("salary", salary_str);

    auto age = crypto::utils::numeric::cov_sv_to_int64(age_str);
    if (age) {
        m.age = *age;
    }
    auto salary = crypto::utils::numeric::cov_sv_to_double(salary_str);
    if (salary) {
        m.salary = *salary;
    }
}

} // namespace json_dto
// const std::string_view json_complex_in { 
//     R"JSON(
//     {
//         "type":"orderbookdepth",
//         "content":{
//             "list":[
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64191000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64239000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64108000","quantity":"0.1558","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64142000","quantity":"1.11","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64179000","quantity":"0.0809","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"ask","price":"64289000","quantity":"0.003","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"64045000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63976000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63937000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63892000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63639000","quantity":"0","total":"0"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"64062000","quantity":"0.9537","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63965000","quantity":"0.009","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63893000","quantity":"0.2589","total":"2"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63872000","quantity":"0.312","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"63810000","quantity":"0.1521","total":"1"},
//                 {"symbol":"BTC_KRW","orderType":"bid","price":"55973000","quantity":"0.0089","total":"1"}
//             ],
//             "datetime":"1615905342373345"
//         }
//     } )JSON" 
// };

int main(int argc, char** argv) {
    rapidjson::Document document;
    try {
        document.SetNull();
        document.GetAllocator().Clear();
        document.Parse<rapidjson::kParseDefaultFlags>(
            json_simple_in.data(), json_simple_in.size());
        json_dto::check_document_parse_status(document);
    }
    catch (const std::exception &e) {
        std::cout << "error parsing " << e.what()
                  << " on " << json_simple_in << std::endl;
        return -1;
    }
    try {
        people_t man;
        json_dto::from_json<people_t>(document, man);

        std::cout << "parsed: name= " << man.name
                  << ",age=" << man.age 
                  << ",salary=" << man.salary
                  << std::endl;
    } catch (const std::exception &e) {
        std::cout << "json_dto error: e= " << e.what()
                  << std::endl;
        return -2;
    }
    return 0;
}