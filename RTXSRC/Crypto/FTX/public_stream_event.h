#pragma once

#include "constants.h"
#include <json_dto/pub.hpp>

namespace Crypto::ftx {

struct public_stream_event_t {
    WS::ResponseType response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
    WS::ChannelType event_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
    std::string_view market = INVALID_STRING_VIEW;

    void reset() {
        response_type = WS::ResponseType::INVALID_RESPONSE_TYPE;
        event_type = WS::ChannelType::INVALID_CHANNEL_TYPE;
        market = "N/A";
    }
};


}  // namespace Crypto::ftx


namespace json_dto {

template<typename Json_Io>
void json_io(Json_Io &io, Crypto::ftx::public_stream_event_t &m) {
    io
    & json_dto::mandatory("type", m.response_type);
    
    if (m.response_type != Crypto::ftx::WS::ResponseType::partial &&
        m.response_type != Crypto::ftx::WS::ResponseType::update)
        throw std::runtime_error("unable to parse as public_stream_event_t");
    
    io
    & json_dto::mandatory("channel", m.event_type)
    & json_dto::mandatory("market", m.market);
}


}  // namespace json_dto
