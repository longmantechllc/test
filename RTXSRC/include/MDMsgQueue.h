#include <memory>
#include "Bbo.h"
#include "TopOfBook.h"
#include "LockFreeQueue.h"

enum class MDType { TOB, BBO};

struct MDData {
    virtual MDType msgtype() = 0;
    virtual BatchTOB* getTOB() {return nullptr;}
    virtual Bbo* getBBO() {return nullptr;}
};
struct MDBatchTOB : public MDData {
    MDType msgtype() override { return MDType::TOB;}
    BatchTOB* getTOB() override { return &data; }
    BatchTOB data;
};
struct MDBBO : public MDData {
    MDType msgtype() override { return MDType::BBO;}
    Bbo* getBBO() {return &data;}
    Bbo data;
};

template<typename T>
class MsgQueue {
public:
    bool
    try_push(const T &v) noexcept(std::is_nothrow_copy_constructible<T>::value) {
        static_assert(std::is_copy_constructible<T>::value,
                      "T must be copy constructible");
        return m_queue.try_push(v);
    }

    bool try_push(T &&v) noexcept(std::is_nothrow_constructible<T &&>::value) {
        return m_queue.try_push(std::forward<T>(v));
    }

    bool try_dequeue(T& msg) {
        if (!m_queue.empty()) {
            msg = *m_queue.front();
            m_queue.pop();
            return true;
        }
        return false;
    }
    size_t size() const noexcept {
        return m_queue.size();
    }

private:
    LockFree::LockFreeQueue<T> m_queue{1024};
};

using MDMsgPtr = std::shared_ptr<MDData>;
using MDMsgQueue = MsgQueue<MDMsgPtr>;
