/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
* Benchmark Receiver.
* - creates session as an acceptor;
* - calculates performance as a number of received messages divided
*   by the period between receiving of the first and last message;
*/

#ifdef _WIN32
#pragma warning(disable: 4786)
#endif // _WIN32

#include <iostream>
#include <fstream>
#include <stdio.h>

#include <B2BITS_V12.h>
#include <B2BITS_Thread.h>

#include "BenchmarkDefines.h"
#include "Properties.h"
#include "Exception.h"
#include "Auxiliary.h"

using namespace std;
using namespace System;
using namespace Engine;
using namespace Benchmark;

namespace BenchmarkReceiver
{

    /**
    * Receives messages. Sends the Logout message after the receiving of the given number of messages.
    */
    class Application : public Engine::Application
    {
    public:
        /**
        * Constructor.
        *
        * @param pSem Is posted when the Logout message is received.
        */
        Application( int expectedNumberOfMessages , System::Semaphore* pSem, int delay )
            : expectedNumberOfMessages_( expectedNumberOfMessages )
            , nMsgs_ ( 0 )
            , isFirstMsgReceived_( false )
            , startTime_(0)
            , finishTime_(0)
            , pLogoutSem_( pSem )
            , delay_( delay )
        {}

        /** A call-back method to process incoming messages. */
        virtual bool process( const Engine::FIXMessage& fixmsg, const Engine::Session& sn ) {
            if( ! isFirstMsgReceived_ ) {
                startTime_ = Benchmark::getCurrentTime();
                isFirstMsgReceived_ = true;
            }

            if( ++nMsgs_  >= expectedNumberOfMessages_ ) {
                finishTime_ = Benchmark::getCurrentTime();
                double duration = ( finishTime_ - startTime_ ) / 1000.0;
                if ( 0 == duration ) {
                    duration = 1;
                }
                unsigned int  performance = (unsigned int)(expectedNumberOfMessages_ / duration);

                char msg[1024];
#if 1400 != _MSC_VER
                sprintf( msg, "Session '%s_%s' result: \n\n\t%u msg/sec, %u messages.", sn.getSender()->c_str(), sn.getTarget()->c_str(), performance, expectedNumberOfMessages_ );
#else
                sprintf_s( msg, 1023, "Session '%s_%s' result: \n\n\t%u msg/sec, %d messages.", sn.getSender()->c_str(), sn.getTarget()->c_str(), performance, expectedNumberOfMessages_ );
#endif
                Benchmark::Auxiliary::notification( msg );

                Engine::FIXMessage* fixMsg = FIXMsgProcessor::singleton()->clone( fixmsg );
                const_cast<Engine::Session&>( sn ).put( fixMsg );
            }

            if( delay_ != 0 ) {
                System::Thread::sleep( delay_ );
            }

            return true;
        }

        /**
        * This call-back method is called to notify about Logon event.
        */
        virtual void onLogonEvent( const Engine::LogonEvent* , const Engine::Session& sn ) {
            Benchmark::Auxiliary::notification( string( "Logon received for the session '" ) + *sn.getSender() +
                                                "_" + *sn.getTarget() + "'. Receiving the measuring messages..." );
        }

        virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) {}

        /** This call-back method is called to notify about Logout event. */
        virtual void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {
            pLogoutSem_->post();
        }

        /** This call-back method is called to notify about SequenceGap event.  */
        virtual void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {
            ;
        }

        /** * This call-back method is called to notify about SessionLevelReject event. */
        virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* , const Engine::Session& ) {
            ;
        }

        /**
        * This call-back method is called when a Heartbeat message (MsgType = 0)
        * with the TestReqID field (tag = 112) has been received.
        *
        * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
        */
        virtual void onHeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& /*event*/, const Session& /*sn*/ ) {
            ;
        }

        virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ ) {
            cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
        }

        /**
        * This call-back method is called when a Resend Request (MsgType = 2) has been received.
        */
        virtual void onResendRequestEvent( const ResendRequestEvent& /*event*/, const Session& /*sn*/ ) {
            ;
        }

        /**
        * This call-back method is called when the session has changed its state.
        */
        virtual void onNewStateEvent( const NewStateEvent& /*event*/, const Session& /*sn*/ ) {
            ;
        }

        /**
        * This call-back method is called when an outgoing message is about to be resent
        * as a reply to the incoming ResendRequest message.
        *
        * If the method returns 'true' then the Engine resends the message to counterpart,
        * otherwise it sends the SequenceReset Gap Fill message.
        *
        * @param msg Outgoing message.
        * @param sn FIX session.
        *
        @return true if the message should be resent, otherwise false.
        */
        virtual bool onResend( const FIXMessage& /*msg*/, const Session& /*sn*/ ) {
            return false;
        }

        unsigned int getStartTime() {
            return startTime_;
        };
        unsigned int getFinishTime() {
            return finishTime_;
        };
        int getMessageProceed() {
            return nMsgs_;
        };


    private:
        /// Expected number of incoming applicatin-level messages.
        int expectedNumberOfMessages_;

        /// Number of received applicatin-level messages.
        int nMsgs_ ;

        /// Whether first message was received or not.
        bool isFirstMsgReceived_;

        /// Time of the first received message.
        unsigned int startTime_;

        /// Time of the last received message.
        unsigned int finishTime_;

        /// Is posted when the Logout message is received.
        System::Semaphore* pLogoutSem_;

        /// Prcessing delay
        int delay_;
    };


    class ReceiverThread: public System::Thread
    {
    private:
        int volume_;

        string sender_;
        string target_;
        string fixVersion_;
        string storageType_;

        /// Time of the first received message.
        unsigned int startTime_;

        /// Time of the last received message.
        unsigned int finishTime_;

        /// Number of received application-level messages.
        int nMsgs_ ;

        /// Message processing delay
        int delay_;

        bool aggressiveSendReceive_;

    public:
        ReceiverThread( const int volume, const string& sender,
                        const string& target, const string& fixVersion, const string& storageType, bool aggressiveSendReceive, int delay )
            : Thread()
            , volume_( volume )
            , sender_( sender )
            , target_( target )
            , fixVersion_( fixVersion )
            , storageType_( storageType )
            , startTime_(0)
            , finishTime_(0)
            , nMsgs_(0)
            , delay_( delay )
            , aggressiveSendReceive_( aggressiveSendReceive )
        {}

        ~ReceiverThread() {}

        void run() {
            try {
                Semaphore logoutSem;
                BenchmarkReceiver::Application app( volume_, &logoutSem, delay_ ); // Application

                SessionExtraParameters params;
                if( aggressiveSendReceive_ ) {
                    params.socketPriority_ = Engine::AGGRESSIVE_SEND_AND_RECEIVE_SOCKET_OP_PRIORITY;
                }

                Auxiliary::notification( "Going to create session" );
                // Create a session.
                FIXVersion ver = Auxiliary::string2version( fixVersion_ );

                params.storageType_ = Session::stringToMessageStorageType( storageType_.c_str() );

                Session* s = FixEngine::singleton()->createSession( &app,
                             sender_,
                             target_,
                             ver,
                             &params );
                s->connect(); // Connect as acceptor.

                logoutSem.wait();

                startTime_ = app.getStartTime();
                finishTime_ = app.getFinishTime();
                nMsgs_ = app.getMessageProceed();
                // Free resources.
                s->registerApplication( NULL );
                s->release();
            } catch( const Utils::Exception& ex ) {
                Benchmark::Auxiliary::error( ex.what() );
            } catch( const Benchmark::Exception& ex ) {
                Benchmark::Auxiliary::error( ex.what() );
            }
        }

        unsigned int getStartTime() const {
            return startTime_;
        };
        unsigned int getFinishTime() const {
            return finishTime_;
        };
        int getMessageProceed() const {
            return nMsgs_;
        };

    };

}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // command line parameters
    std::string engineConfig = "engine.receiver.properties",
                reciverConfig = "benchmark.properties";
    if ( argc == 3 ) {
        engineConfig = argv[1];
        reciverConfig = argv[2];
    } else if( argc > 1 ) {
        cout << "Usage:\n\t" << argv[0] << " <engine properties file> <receiver properties file>" << endl;
        return 1;
    }

    try {
        ifstream in( reciverConfig.c_str() );
        if( ! in ) {
            Auxiliary::error( string( "Can not open file '" ) + reciverConfig + "'" );
            return 1;
        }
        // read settings
        Properties p( &in );
        in.close();

        int volume = p.getInteger( Properties::VOLUME );
        string sender = p.getString( Properties::TARGET );
        string target = p.getString( Properties::SENDER );
        string fixVersion = p.getString( Properties::FIXVERSION );
        string storageType = p.getString( Properties::STORAGE_TYPE );
        int sessionCount = p.getInteger( Properties::SESSIONCOUNT );
        int delay = p.getInteger( Properties::DELAY );
        bool aggressiveSendReceive = "true" == p.getString( Benchmark::Properties::AGGRESSIVE_SEND );

        // create receivers
        Auxiliary::notification( "Creating receivers." );
        vector<BenchmarkReceiver::ReceiverThread*> receivers;
        receivers.reserve( volume );
        for( int i = 0; i < sessionCount; ++i ) {
            receivers.push_back( new BenchmarkReceiver::ReceiverThread( volume,
                                 sender/*(1 == volume)?(sender):(sender + i2str(i))*/,
                                 ( 1 == volume ) ? ( target ) : ( target + i2str( i ) ),
                                 fixVersion,
                                 storageType,
                                 aggressiveSendReceive,
                                 delay ) );
        }

        // Initialize FE
        FixEngine::init( engineConfig );

        // Start all receivers
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != receivers.at( i ) );
            receivers.at( i )->start();
        }

        unsigned int startTime = static_cast<unsigned int>( -1 );
        unsigned int finishTime = 0;
        unsigned int nMsgs = 0;
        Auxiliary::notification( "Acceptors created. Please start the Sender..." );

        // Wait until senders finish
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != receivers.at( i ) );
            receivers.at( i )->join();
            if( startTime > receivers.at( i )->getStartTime() ) {
                startTime = receivers.at( i )->getStartTime();
            }
            if( finishTime < receivers.at( i )->getFinishTime() ) {
                finishTime = receivers.at( i )->getFinishTime();
            }
            nMsgs += receivers.at( i )->getMessageProceed();
        }

        // calculate total performance
        {
            double duration = ( finishTime - startTime ) / 1000.0;
            if( 0.0 == duration ) {
                duration = 1.0;
            }
            unsigned int performance = static_cast<unsigned int>( nMsgs / duration );

            char msg[1024];
#if 1400 != _MSC_VER
            sprintf( msg, "Average result for all sessions: \n\n\t%u msg/sec, %u messages.", performance, nMsgs );
#else
            sprintf_s( msg, 1023, "Average result for all sessions: \n\n\t%u msg/sec, %u messages.", performance, nMsgs );
#endif
            Benchmark::Auxiliary::notification( msg );
        }

        // destroy senders
        for( int i = 0; i < sessionCount; ++i ) {
            assert( NULL != receivers.at( i ) );
            delete receivers.at( i );
        }

        FixEngine::destroy();
    } catch( const Utils::Exception& ex ) {
        Auxiliary::error( ex.what() );
        return 2;
    } catch( const Benchmark::Exception& ex ) {
        Auxiliary::error( ex.what() );
        return 3;
    }

    return 0;
}
