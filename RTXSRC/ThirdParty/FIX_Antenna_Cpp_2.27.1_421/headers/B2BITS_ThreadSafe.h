// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_ThreadSafe.h
/// Contains implementation of the thread safety checking classes
/// @author Dmytro Ovdiienko [dmytro_ovdiienko@epam.com]
/// @see http://drdobbs.com/cpp/184403766

#ifndef H_B2BITS_System_ThreadSafe_H
#define H_B2BITS_System_ThreadSafe_H

#include <cassert>
#include <cstddef>

#include <B2BITS_Mutex.h>

#if !defined(B2B_ENABLE_THREAD_SAFETY_CHECK) && defined(_DEBUG)
#   define B2B_ENABLE_THREAD_SAFETY_CHECK 1
#endif //_DEBUG

#if B2B_ENABLE_THREAD_SAFETY_CHECK
#   define B2B_THREAD_SAFE volatile
#   define B2B_SHARED volatile
#else // B2B_ENABLE_THREAD_SAFETY_CHECK
#   define B2B_THREAD_SAFE
#   define B2B_SHARED
#endif // B2B_ENABLE_THREAD_SAFETY_CHECK

namespace System
{
    /// Contains implementation of the thread safety checking classes
    /// @see http://drdobbs.com/cpp/184403766
    namespace ThreadSafe
    {

        template< typename T >
        class MutexGuard
        {
        private:
            T* obj_;
            Mutex* mutex_;

        public:
            explicit MutexGuard( T B2B_SHARED* obj, Mutex* mutex )
                : obj_( const_cast<T*>( obj ) )
                , mutex_( mutex ) {
                assert( NULL != obj );
                assert( NULL != mutex );

                mutex_->lock();
            }

            ~MutexGuard() {
                mutex_->unlock();
            }

        public:
            T* get() const throw() {
                return obj_;
            }

            T* operator->() const throw() {
                return obj_;
            }

            T& operator*() const throw() {
                return *obj_;
            }

        private:
            MutexGuard( MutexGuard const& );
            MutexGuard& operator=( MutexGuard const& );
        };

        template< typename T >
        class LockFreeGuard
        {
        private:
            T* obj_;

        public:
            explicit LockFreeGuard( T B2B_SHARED* obj ) throw()
                : obj_( const_cast<T*>( obj ) ) {
                assert( NULL != obj );
            }

        public:
            T* get() const throw() {
                return obj_;
            }

            T& operator*() const throw() {
                return *obj_;
            }

            T* operator->() const throw() {
                return get();
            }
        private:
            LockFreeGuard( LockFreeGuard const& );
            LockFreeGuard& operator=( LockFreeGuard const& );
        };

        template< typename T >
        inline void releaseObject( T const* obj )
        {
            if( NULL != obj ) {
                obj->release();
            }
        }

        template< typename T >
        inline void deleteObject( T const* obj )
        {
            delete obj;
        }

        template< typename T >
        class VolatileSensitivePointer
        {
        private:
            T* obj_;

        public:
            explicit VolatileSensitivePointer( T* obj ) throw()
                : obj_( obj ) {

            }

        public:
#if B2B_ENABLE_THREAD_SAFETY_CHECK
            T B2B_SHARED& operator*() B2B_THREAD_SAFE throw() {
                assert( NULL != obj_ );
                return *obj_;
            }

            T B2B_SHARED* operator->() B2B_SHARED throw() {
                return obj_;
            }
#endif // B2B_ENABLE_THREAD_SAFETY_CHECK

            T* operator->() throw() {
                return obj_;
            }

            T& operator*() throw() {
                assert( NULL != obj_ );
                return *obj_;
            }

        private:
            VolatileSensitivePointer( VolatileSensitivePointer const& );
            VolatileSensitivePointer& operator=( VolatileSensitivePointer const& );
        };
    }
} // namespace System::ThreadSafe {

#endif // H_B2BITS_System_ThreadSafe_H
