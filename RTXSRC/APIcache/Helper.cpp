#include "Helper.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>
#include <cstring>

using namespace ::std;

using namespace ::Aux;

// Writes message to console.
void Helper::writeLineToConsole(const string& message)
{
    //outputLock_.lock();
    clog << message << endl;
    //outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine(const string& message)
{
    cerr << message << endl;
}

// Loads file content into std::string.
string Helper::loadString(const string& fileName)
{

    struct stat statistics;

    // Retrieves file size.
    if (-1 == stat(fileName.c_str(), &statistics)) {
        throw strerror(errno);
    }

    // Opens file.
    ifstream ifs(fileName.c_str(), ios::binary);

    if (!ifs.good()) {
        throw strerror(errno);
    }

    string str;

    string tmp_ln;
    getline(ifs, tmp_ln);
    if (!tmp_ln.empty()) {
        string::size_type beg = tmp_ln.find("\x01" "9=");
        if (beg == string::npos) {
            throw string("Must be size (tag 9) in message");
        }
        string::size_type end = tmp_ln.find("\x01", beg + 3);
        if (end == string::npos) {
            throw string("Must be size (tag 9) in message");
        }
        string msg_size = tmp_ln.substr(beg + 3, end - beg - 3);
        char* errPos = NULL;
        size_t size = strtol(msg_size.c_str(), &errPos, 10);
        while (size > tmp_ln.size()) {
            if (ifs.eof()) {
                throw string("Unexpected end of file");
            }
            string tmp_ln2;
            char buf[10 * 1024];
            buf[0] = 10;
            ifs.read(&buf[1], size - tmp_ln.size());
            tmp_ln += string(&buf[0], size - tmp_ln.size() + 1);
            tmp_ln2.clear();
            getline(ifs, tmp_ln2);
            tmp_ln += tmp_ln2;
        }
        if (*tmp_ln.rbegin() == '\r') {
            tmp_ln.erase(tmp_ln.size() - 1);
        }
        str = tmp_ln;
    }

    ifs.close();

    return str;
}

void Helper::parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params)
{

    params->initiator_cfg = prop.getString(Aux::Properties::INITIATOR_CFG);

    params->port = prop.getInteger(Aux::Properties::PORT);
    if (prop.isPropertyExists(Aux::Properties::PRELOAD_FILE)) {
        params->preload_file = prop.getString(Aux::Properties::PRELOAD_FILE);
    }

    params->mmids = prop.getString(Aux::Properties::MMID);

    if (prop.isPropertyExists(Aux::Properties::RLP_IDS)) {
        params->rlp_ids = prop.getString(Aux::Properties::RLP_IDS);
        params->client_name = prop.getString(Aux::Properties::CLIENT_NAME);
        params->symbol_file = prop.getString(Aux::Properties::SYMBOL_FILE);
    }

    if (prop.isPropertyExists(Aux::Properties::LOGGING)) {
        params->logging = prop.getBool(Aux::Properties::LOGGING);
    }
}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo(const string& applicationName)
{
    Helper::writeErrorLine();
    Helper::writeErrorLine("Usage: " + applicationName + " <property file>");
    Helper::writeErrorLine();
}

