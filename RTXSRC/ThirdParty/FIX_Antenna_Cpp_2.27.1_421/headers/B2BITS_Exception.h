// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Exception.h
/// Contains Utils::Exception classes declaration.

#ifndef _B2BITS_EXCEPTION__H
#define _B2BITS_EXCEPTION__H

#include <string>
#include <exception>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_CompilerDefines.h>

#if _MSC_VER >= 1900
#   pragma warning(push)
// non � DLL-interface classkey 'identifier' used as base for DLL-interface classkey 'identifier'
#   pragma warning(disable: 4275)
#endif

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// FIX Antenna Utils framework
namespace Utils
{

    /**
    * Generic application exception.
    */
    class V12_API Exception : public std::exception
    {
    public:
        /** Constructor. */
        Exception();

        /** Constructor. */
        Exception( const std::string& aReason );

        /** Destructor. */
        virtual ~Exception() throw ();

        /**
        * Returns the reason for this exception.
        */
        virtual const char* what()  const throw() B2B_OVERRIDE;

        /**
        * Returns the reason for this exception.
        */
        std::string const& reason() const throw();

    protected:
        /// std::string that describes raised exception
#if _MSC_VER >= 1400
#   pragma warning(push)
#   pragma warning(disable: 4251)
#endif
        std::string m_reason;
#if _MSC_VER >= 1400
#   pragma warning(pop)
#endif
    };

    /**
    * Invalid parameter Exception.
    */
    class V12_API InvalidParameterException : public Exception
    {
    public:
        /// Constructor
        /// @param aMsg Message that explains exception.
        InvalidParameterException( const std::string& aMsg ) : Exception( aMsg ) {}
    };
}

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#if _MSC_VER >= 1900
#   pragma warning(pop)
#endif

#endif // _B2BITS_EXCEPTION__H
