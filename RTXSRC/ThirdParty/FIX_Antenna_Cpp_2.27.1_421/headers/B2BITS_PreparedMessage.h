// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_PreparedMessage.h
/// Contains Engine::PreparedMessage and GenericPreparedMessage classes declaration.

#ifndef H_B2BITS_Engine_PreparedMessage_H
#define H_B2BITS_Engine_PreparedMessage_H

#include <cassert>
#include <memory>
#include <cstddef>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_Decimal.h>
#include <B2BITS_UTCTimestamp.h>
#include <B2BITS_TZTimeHelper.h>
#include <B2BITS_String.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
    // A keyword was used that is not in the C++ standard, for example, one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
#endif // _MSC_VER >= 1500

/** Raw FIX message parser tools */
namespace Parser
{
    /** Internal parser helpers */
    struct ParserUtils;
}

namespace Engine
{

    class FIXMessage;

    /**
    * Alternative for FIXMessage class.
    * Improves latency of the Session::put method if user sends same message 
    * many times and size of the volatile fields do not change. Volatile fields are fields
    * which user changes between Session::put calls (e.g. OrderQty, Price, TransactTime, etc).
    *
    * Sample of usage:
    * \code
    * Engine::Session* initiator;
    * // ... create session
    *
    * // Replace this string with correct raw FIX message 
    * std::string const RAW_MSG = "8= ... NEW ORDER SINGLE RAW FIX MESSAGE HERE... ";
    *
    * // Create New Order Single FIXMessage skeleton from raw fix message
    * Engine::FIXMessage* msg = Engine::FIXMsgProcessor::singleton()->parse( RAW_MSG );
    *
    * //
    * // Reserve space for volatile fields. 
    * //
    * msg->reserve( FIXFields::ClOrdID,       0, 10 ); // Assign index 0 for ClOrdID
    * msg->reserve( FIXFields::Price,         1, 7 );  // Assign index 1 for Price
    * msg->reserve( FIXFields::OrderQty,      2, 2 );  // Assign index 2 for OrderQty, etc.
    * msg->reserve( FIXFields::TransactTime,  3, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
    * msg->reserve( FIXFields::SendingTime,   4, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
    * 
    * //
    * // Reserve space for Session Level Fields like SenderCompID, TargetCompID, etc.
    * //
    * size_t const MsgSeqNumFieldSize = 7; // 7 digits for MsgSeqNum. Max value would be 9,999,999
    * Engine::PutMessageOptions putOptions;
    * putOptions.overrideSendingTime_ = false;
    * initiator_->inflate( msg, MsgSeqNumFieldSize, &putOptions );
    * 
    * // Create Prepared Message
    * Engine::GenericPreparedMessage preparedMessage( msg );
    * 
    * //
    * // Main sending loop
    * //
    * size_t sz;
    * size_t const TEST_COUNT = 1000000;
    * for( size_t i = 0; i < TEST_COUNT; ++i ) {
    * 
    *     //
    *     // Update volatile fields which were reserved later
    *     //
    *     preparedMessage.set( 0, static_cast<int>( i ) ); // ClOrdID
    *     preparedMessage.set( 1, Engine::Decimal( 100023, -2 ) ); // Price
    *     preparedMessage.set( 2, 10 ); // OrderQty
    *     preparedMessage.set( 3, Engine::UTCTimestamp::now() ); // TransactTime
    *     preparedMessage.set( 4, preparedMessage.get( 3, &sz ), Engine::UTCTimestamp::ValueSizeWithMilliseconds ); // TransactTime
    * 
    *     // Put message to session
    *     initiator_->put( &preparedMessage );
    * }
    * \endcode
    */
    class PreparedMessage
    {
        friend class FIXMessage;
        friend struct Parser::ParserUtils;
    public:
        /**
        * Enumerates indexes for fields
        */
        enum {
            First = 1000,             ///< First field index
            MsgSeqNum = First,        ///< MsgSeqNum field
            CheckSum,                 ///< CheckSum field
            SendingTime,              ///< SendingTime field
            LastMsgSeqNumProcessed,   ///< LastMsgSeqNumProcessed field
            Last                      ///< Last field index + 1
        };

    public:
        /**
        *  Updates MsgSeqNum value
        * @return 0 if value was written successfully.
        */
        int setMsgSeqNum( int value ) throw();

        /** 
         * Returns pointer to buffer of the MsgSeqNum field
         */
        char* getMsgSeqNum( size_t* size ) throw();

        /**
        * Updates SendingTime value
        */
        void setSendingTime( char const* value, std::size_t size ) throw();

        /**
        * Updates SendingTime value
        */
        void setSendingTime( UTCTimestamp value ) throw();

        /**
        * Updates SendingTime value
        */
        void setSendingTime( const TZTimeHelper::UTCTimestamp& value ) throw();

        /**
        * Updates LastMsgSeqNumProcessed value
        */
        void setLastMsgSeqNumProcessed( int value ) throw();

        /**
        * Updates CheckSum value
        * @param value Buffer of three bytes
        */
        void setCheckSum( char const* value ) throw();

        /**
         * Returns pointer to buffer of the CheckSum field
         */
        char* getCheckSum( size_t* size ) throw();

        /** Returns pointer to the RAW FIX message */
        char const* rawMsg() const throw();

        /** Returns size of the RAW FIX message */
        std::size_t rawMsgSize() const throw();

        /** Returns true, if LastMsgSeqNumProcessed is initalized. */
        bool isLastMsgSeqNumProcessedReserved() const throw();

        /** Returns true, if SendingTime is initalized. */
        bool isSendingTimeReserved() const throw();

        /** Returns size reserved for SendingTime. */
        size_t sizeSendingTimeReserved() const throw();

    public:
        /** Recalculates checksum */
        void V12_API updateCheckSum() throw();

    public:
        V12_API PreparedMessage() throw();

        /**
        * Polymorphous destructor
        */
        virtual V12_API ~PreparedMessage() throw();

    protected:
        /**
        * Prepared message field
        */
        struct Field {
            /** Pointer to the field value in the buffer */
            char* value_;

            /** Value size in the buffer */
            std::size_t size_;

            /**
            * Initalizes field with pointer to value in the buffer and its size.
            * @param[in] value Pointer to the value in the buffer
            * @param[in] size Size of the value
            */
            void init( char* value, std::size_t size ) throw() {
                value_ = value;
                size_ = size;
            }
        }; // struct Field

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, char const* value, std::size_t size ) throw();

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, char value ) throw();

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, bool value ) throw();

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, UTCTimestamp value ) throw();

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, const TZTimeHelper::UTCTimestamp & value ) throw();

        /**
        * Copies passed value to the message
        * @return true if value was written completely.
        */
        static bool set( Field const& field, System::u32 value ) throw();

        /**
        * Copies passed value to the message
        * @return true if value was written completely.
        */
        static bool set( Field const& field, System::u64 value ) throw();

        /**
        * Copies passed value to the message
        * @return true if value was written completely.
        */
        static bool set( Field const& field, System::i32 value ) throw();

        /**
        * Copies passed value to the message
        * @return true if value was written completely.
        */
        static bool set( Field const& field, System::i64 value ) throw();

        /**
        * Copies passed value to the message
        */
        static void set( Field const& field, Decimal const& value ) throw();

        /**
        * Returns pointer to the buffer where field value is stored
        * @param[in] index Index of the field
        * @param[out] size Size of the field value
        * @throw Utils::Exception if index is out of range
        */
        V12_API char* getChecked( PreparedFieldIndex index, std::size_t* size );

        /**
        * Initialize PreparedField.
        * Used by FA in the FIXMessage::prepare.
        */
        virtual void V12_API initField( PreparedFieldIndex index, char* start, std::size_t size );

    protected:
        /**
        * Acquires passed buffer and its size.
        * Used by FA in the FIXMessage::prepare
        */
        void setBuffer( char* buf, char const* msgStart, std::size_t msgSize ) throw();

        /**
        * Updates pointer to the RAW FIX message
        */
        void setMsgStart( char const* msgStart ) throw();

        /**
        * Updates size of the RAW FIX message
        */
        void setMsgSize( std::size_t msgSize ) throw();

    private:
        char* buffer_;
        char const* msgStart_;
        std::size_t msgSize_;
        Field fields_[ Last - First ];

    private:
        PreparedMessage( PreparedMessage const& );
        PreparedMessage& operator=( PreparedMessage const& );
    }; // class PreparedMessage

    inline char* PreparedMessage::getMsgSeqNum( size_t* size ) throw()
    {
        assert( NULL != size );

        *size = fields_[ MsgSeqNum - First ].size_;
        return fields_[ MsgSeqNum - First ].value_;
    }

    inline int PreparedMessage::setMsgSeqNum( int value ) throw()
    {
        return PreparedMessage::set( fields_[ MsgSeqNum - First ], value );
    }

    inline void PreparedMessage::setSendingTime( char const* value, std::size_t size ) throw()
    {
        PreparedMessage::set( fields_[ SendingTime - First ], value, size );
    }

    inline void PreparedMessage::setSendingTime( UTCTimestamp value ) throw()
    {
        PreparedMessage::set( fields_[ SendingTime - First ], value );
    }

    inline void PreparedMessage::setSendingTime( const TZTimeHelper::UTCTimestamp & value ) throw()
    {
        PreparedMessage::set( fields_[ SendingTime - First ], value );
    }

    inline void PreparedMessage::setLastMsgSeqNumProcessed( int value ) throw()
    {
        PreparedMessage::set( fields_[ LastMsgSeqNumProcessed - First ], value );
    }

    inline char* PreparedMessage::getCheckSum( size_t* size ) throw()
    {
        assert( NULL != size );

        *size = fields_[ CheckSum - First ].size_;
        return fields_[ CheckSum - First ].value_;
    }

    inline void PreparedMessage::setCheckSum( char const* value ) throw()
    {
        PreparedMessage::set( fields_[ CheckSum - First ], value, 3 );
    }

    inline bool PreparedMessage::isLastMsgSeqNumProcessedReserved() const throw()
    {
        return fields_[ LastMsgSeqNumProcessed - First ].size_ > 0;
    }

    inline bool PreparedMessage::isSendingTimeReserved() const throw()
    {
        return fields_[ SendingTime - First ].size_ > 0;
    }

    inline size_t PreparedMessage::sizeSendingTimeReserved() const throw()
    {
        return fields_[ SendingTime - First ].size_;
    }

    inline char const* PreparedMessage::rawMsg() const throw()
    {
        return msgStart_;
    }

    inline std::size_t PreparedMessage::rawMsgSize() const throw()
    {
        return msgSize_;
    }

    inline void PreparedMessage::setBuffer( char* buf, char const* msgStart, std::size_t msgSize ) throw()
    {
        std::free( buffer_ );

        buffer_ = buf;
        msgStart_ = msgStart;
        msgSize_ = msgSize;
    }

    inline void PreparedMessage::setMsgStart( char const* msgStart ) throw()
    {
        msgStart_ = msgStart;
    }

    inline void PreparedMessage::setMsgSize( std::size_t msgSize ) throw()
    {
        msgSize_ = msgSize;
    }

    inline void PreparedMessage::set( Field const& field, char const* value, std::size_t size ) throw()
    {
        assert( NULL != field.value_ );
        assert( size == field.size_ );

        std::memcpy( field.value_, value, size );
    }

    inline void PreparedMessage::set( Field const& field, char value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 1 == field.size_ );

        field.value_[0] = value;
    }

    inline void PreparedMessage::set( Field const& field, bool value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 1 == field.size_ );

        field.value_[0] = value ? 'Y' : 'N';
    }

    inline void PreparedMessage::set( Field const& field, UTCTimestamp value ) throw()
    {
        assert( NULL != field.value_ );
        assert( UTCTimestamp::ValueSizeWithMilliseconds <= field.size_ ||
                UTCTimestamp::ValueSizeNoMilliseconds == field.size_ );
        size_t size = field.size_;
        char * end = field.value_ + size;
        if ( size > UTCTimestamp::ValueSizeWithMilliseconds ){
            char * end_need = field.value_ + UTCTimestamp::ValueSizeWithMilliseconds;
            while( end_need != end ){
                --end;
                *end = '0';
            }
        };
        char const* result = value.toFixString( field.value_, end, UTCTimestamp::ValueSizeWithMilliseconds <= field.size_ );
        assert( result == field.value_ );
        ( void )result;
    }

    inline void PreparedMessage::set( Field const& field, const TZTimeHelper::UTCTimestamp & value ) throw()
    {
        assert( NULL != field.value_ );
        assert( TZTimeHelper::ValueSizeUTCTimestampPico == field.size_ ||
                TZTimeHelper::ValueSizeUTCTimestampNano == field.size_ ||
                TZTimeHelper::ValueSizeUTCTimestampMicro == field.size_ ||
                TZTimeHelper::ValueSizeUTCTimestampMilli == field.size_ ||
                TZTimeHelper::ValueSizeUTCTimestampSec == field.size_ 
                );
        switch( field.size_ )
        {
            case TZTimeHelper::ValueSizeUTCTimestampPico:
                TZTimeHelper::utcTimestampToString( field.value_ , TZTimeHelper::ValueSizeBufferTZTimestamp , value , TZTimeHelper::Picoseconds );
            break;
            case TZTimeHelper::ValueSizeUTCTimestampNano:
                TZTimeHelper::utcTimestampToString( field.value_ , TZTimeHelper::ValueSizeBufferTZTimestamp , value , TZTimeHelper::Nanoseconds );
            break;
            case TZTimeHelper::ValueSizeUTCTimestampMicro:
                TZTimeHelper::utcTimestampToString( field.value_ , TZTimeHelper::ValueSizeBufferTZTimestamp , value , TZTimeHelper::Microseconds );
            break;
            case TZTimeHelper::ValueSizeUTCTimestampMilli:
                TZTimeHelper::utcTimestampToString( field.value_ , TZTimeHelper::ValueSizeBufferTZTimestamp , value , TZTimeHelper::Milliseconds );
            break;
            case TZTimeHelper::ValueSizeUTCTimestampSec:
                TZTimeHelper::utcTimestampToString( field.value_ , TZTimeHelper::ValueSizeBufferTZTimestamp , value , TZTimeHelper::Seconds );
            break;
        }
    }

    inline bool PreparedMessage::set( Field const& field, System::i32 value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 0 != field.size_ );

        char* last = field.value_;
        char* begin = field.value_ + field.size_ - 1;

        if( value < 0 ) {
            value = -value;
            *last++ = '-';
        }

        while( begin >= last ) {
            *begin-- = static_cast<char>( '0' + value % 10 );
            value /= 10;
        }

        assert( 0 == value );
        return value > 0;
    }

    inline bool PreparedMessage::set( Field const& field, System::u32 value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 0 != field.size_ );

        char* last = field.value_;
        char* begin = field.value_ + field.size_ - 1;

        while( begin >= last ) {
            *begin-- = static_cast<char>( '0' + value % 10 );
            value /= 10;
        }

        assert( 0 == value );
        return value > 0;
    }

    inline bool PreparedMessage::set( Field const& field, System::i64 value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 0 != field.size_ );

        char* last = field.value_;
        char* begin = field.value_ + field.size_ - 1;

        if( value < 0 ) {
            value = -value;
            *last++ = '-';
        }

        while( begin >= last ) {
            *begin-- = static_cast<char>( '0' + value % 10 );
            value /= 10;
        }

        assert( 0 == value );
        return value > 0;
    }

    inline bool PreparedMessage::set( Field const& field, System::u64 value ) throw()
    {
        assert( NULL != field.value_ );
        assert( 0 != field.size_ );

        char* last = field.value_;
        char* begin = field.value_ + field.size_ - 1;

        while( begin >= last ) {
            *begin-- = static_cast<char>( '0' + value % 10 );
            value /= 10;
        }

        assert( 0 == value );
        return value > 0;
    }

    inline void PreparedMessage::set( Field const& field, Decimal const& value ) throw()
    {
        value.toString( field.value_, field.size_ );
    }

    /**
    * Provides minimal functionality to work with prepared fields. Implements PreparedMessage.
    */
    class GenericPreparedMessage B2B_SEALED : public PreparedMessage
    {
    public:
        static int const FieldCount = 20;

    public:
        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, char const* value, std::size_t size ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, AsciiString value ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, Decimal value ) throw();

        /**
        * Copies passed value to the message
        * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
        */
        bool set( PreparedFieldIndex index, System::u32 value ) throw();

        /**
        * Copies passed value to the message
        * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
        */
        bool set( PreparedFieldIndex index, System::i32 value ) throw();

        /**
        * Copies passed value to the message
        * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
+        */
        bool set( PreparedFieldIndex index, System::u64 value ) throw();

        /**
        * Copies passed value to the message
        * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
        */
        bool set( PreparedFieldIndex index, System::i64 value ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, char value ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, bool value ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, UTCTimestamp value ) throw();

        /**
        * Copies passed value to the message
        */
        void set( PreparedFieldIndex index, const TZTimeHelper::UTCTimestamp &value ) throw();

        /**
        * Returns pointer to the buffer where field value is stored
        * @param[in] index Index of the field
        * @param[out] size Size of the field value
        * @note Please use this method carefully. User takes on responsibility to field value format.
        */
        char* get( PreparedFieldIndex index, std::size_t* size ) throw();

        /**
         * Returns true, if field by gived index was reserved and user can write value; otherwise false.
         * @return true, if field by gived index was reserved  and user can write value; otherwise false.
         */
        bool isFieldReserved( PreparedFieldIndex index ) const;

    public:
        /** Default constructor */
        V12_API GenericPreparedMessage() throw();

        /** Conversion constructor */
        explicit V12_API GenericPreparedMessage( FIXMessage const* msg );

        /**
        * Polymorphous destructor
        */
        virtual V12_API ~GenericPreparedMessage() throw();

    private:
        virtual void V12_API initField( PreparedFieldIndex index, char* start, std::size_t size ) B2B_OVERRIDE;

    private:
        Field fields_[ FieldCount ];

    private:
        GenericPreparedMessage( GenericPreparedMessage const& );
        GenericPreparedMessage& operator=( GenericPreparedMessage const& );
    }; // class GenericPreparedMessage

    /**
     * Returns true, if field by gived index was reserved and user can write value; otherwise false.
     * @return true, if field by gived index was reserved and user can write value; otherwise false.
     */
    inline bool GenericPreparedMessage::isFieldReserved( PreparedFieldIndex index ) const
    {
        return index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) &&
               0 != fields_[ index ].size_;
    }

    /**
    * Returns pointer to the buffer where field value is stored
    * @param[in] index Index of the field
    * @param[out] size Size of the field value
    * @note Please use this method carefully. User takes on responsibility to field value format.
    */
    inline char* GenericPreparedMessage::get( PreparedFieldIndex index, std::size_t* size ) throw()
    {
        assert( NULL != size );

        *size = fields_[ index ].size_;
        return fields_[ index ].value_;
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, char const* value, std::size_t size ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value, size );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, AsciiString value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value.data(), value.size() );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, Decimal value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
    */
    inline bool GenericPreparedMessage::set( PreparedFieldIndex index, System::u32 value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        return PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
    */
    inline bool GenericPreparedMessage::set( PreparedFieldIndex index, System::i32 value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        return PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
    */
    inline bool GenericPreparedMessage::set( PreparedFieldIndex index, System::u64 value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        return PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    * @return 0 if value was written successfully; non-zero if stored buffer is too small to keep passed value.
    */
    inline bool GenericPreparedMessage::set( PreparedFieldIndex index, System::i64 value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        return PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, UTCTimestamp value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, const TZTimeHelper::UTCTimestamp& value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, char value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value );
    }

    /**
    * Copies passed value to the message
    */
    inline void GenericPreparedMessage::set( PreparedFieldIndex index, bool value ) throw()
    {
        assert( index < static_cast<PreparedFieldIndex>( GenericPreparedMessage::FieldCount ) );
        PreparedMessage::set( fields_[ index ], value );
    }
} // namespace Engine

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif  // _MSC_VER >= 1500

#endif // H_B2BITS_Engine_PreparedMessage_H


