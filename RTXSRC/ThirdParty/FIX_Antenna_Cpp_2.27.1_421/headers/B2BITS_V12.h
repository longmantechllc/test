// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_V12.h
/// Gathers together all public headers

#ifndef _B2BITS_V12__H_
#define _B2BITS_V12__H_

#include "B2BITS_V12_Defines.h"
#include "B2BITS_Application.h"
#include "B2BITS_AutoPtr.h"
#include "B2BITS_Condition.h"
#include "B2BITS_Decimal.h"
#include "B2BITS_Emptily.h"
#include "B2BITS_Empty.h"
#include "B2BITS_Event.h"
#include "B2BITS_Exception.h"
#include "B2BITS_FAProperties.h"
#include "B2BITS_FixEngine.h"
#include "B2BITS_FIXFields.h"
#include "B2BITS_FIXFieldValue.h"
#include "B2BITS_FIXGroup.h"
#include "B2BITS_FIXMessage.h"
#include "B2BITS_FIXMsgFactory.h"
#include "B2BITS_FIXMsgProcessor.h"
#include "B2BITS_FastDecoder.h"
#include "B2BITS_FastCoder.h"
#include "B2BITS_Guard.h"
#include "B2BITS_IntDefines.h"
#include "B2BITS_LocalMktDate.h"
#include "B2BITS_MDApplication.h"
#include "B2BITS_MDApplicationListeners.h"
#include "B2BITS_MDTypes.h"
#include "B2BITS_MonthYear.h"
#include "B2BITS_Mutex.h"
#include "B2BITS_NullableValue.h"
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_ReferenceCounter.h"
#include "B2BITS_Semaphore.h"
#include "B2BITS_SessionEvent.h"
#include "B2BITS_Session.h"
#include "B2BITS_SessionId.h"
#include "B2BITS_SessionParameters.h"
#include "B2BITS_SessionsManager.h"
#include "B2BITS_Statistics.h"
#include "B2BITS_String.h"
#include "B2BITS_StringUtils.h"
#include "B2BITS_SystemDefines.h"
#include "B2BITS_SystemException.h"
#include "B2BITS_TagValue.h"
#include "B2BITS_Types.h"
#include "B2BITS_UTCDateOnly.h"
#include "B2BITS_UTCTimeOnly.h"
#include "B2BITS_UTCTimestamp.h"
#include "B2BITS_PreparedMessage.h"
#include "B2BITS_MsgFilter.h"
#include "B2BITS_AutoEvent.h"
#include "B2BITS_ManualEvent.h"

#ifdef HAVE_XERCES_C
#   include "B2BITS_AdminApplication.h"
#endif // HAVE_XERCES_C

#endif

