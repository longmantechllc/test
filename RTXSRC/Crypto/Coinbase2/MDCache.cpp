#include <iostream>
#include <fstream>
#include <boost/algorithm/string/trim.hpp>

// #include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included
#include "MDCache.h"

using namespace ::std;

void MDCache::loadDataFile() {
    ifstream in(params_.symbol_file.c_str());
    if (!in) {
        spdlog::error("Cannot open preload symbol file {}", params_.symbol_file);
        return;
    }

    std::string line;
    while (std::getline(in, line)) {
        if (line.size()) {
            if (line[0] == '#')
                continue;
            boost::algorithm::trim(line);
            if (auto p = line.find('|'); p != string::npos) {
                auto symbol = line.substr(0, p);
                auto exch_symbol = line.substr(p+1); //stod(
                spdlog::info("internal symbol {}, exchange symbol {}", symbol, exch_symbol);
                m_symbols.emplace_back(make_pair(symbol, exch_symbol));
            }
        }
    }
    spdlog::info("read in {} symbols", m_symbols.size());
}

MDCache::MDCache(const ApplicationProperties& params)
    : params_(params)
{
    
    try {
        spdlog::info("initiator_cfg: ", params.initiator_cfg);
        FIX::SessionSettings settings( params.initiator_cfg );
        FIX::FileStoreFactory storeFactory( settings );
        FIX::FileLogFactory logFactory( settings );
        m_initiator = make_unique<FIX::SocketInitiator>( *this, storeFactory, settings, logFactory);
        m_initiator->start();
        spdlog::info("connecting to APIcache ...");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}",e.what());
        return;
    }

    // loading test data file
    m_fhs.reserve(10); // so no fhs destructor is called.
    if (!params_.symbol_file.empty()) {
        loadDataFile();
        subscribeMarketData();
    }

    m_init = true;
}

MDCache::~MDCache()
{
    m_initiator->stop();
}

void MDCache::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void MDCache::onLogon( const FIX::SessionID& sessionID )
{
    spdlog::info("Logon - {}", sessionID);
    session_ = sessionID;
    m_connected = true;
    //m_sessions.push_back(sessionID);
    m_ioiIds.push_back({});
}

void MDCache::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("Logout - {}", sessionID);
    // if (auto itor = find(m_sessions.begin(), m_sessions.end(), sessionID); itor != m_sessions.end())
    //     m_sessions.erase(itor);
    m_connected = false;
}

void MDCache::toAdmin( FIX::Message& message, const FIX::SessionID& )
{
    spdlog::info("Admin OUT: {}", message);
}

void MDCache::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("Admin IN: {}", message);
}

void MDCache::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("IN: {}", message);
    crack( message, sessionID );
}

void MDCache::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("OUT: {}", message);
}
namespace {
    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}

void MDCache::subscribeMarketData() {
    int idx = 0;

    for (const auto& symbol: m_symbols) {
        spdlog::info("subscrbing symbol {}", symbol.first);
        auto& book = m_ioiCache[symbol.first];
        book.symbol = symbol.first;
        // better to generate subscribe message from above
        // but for now, let FH generate it.
        
        m_fhs.emplace_back("coinbase.us", 443, idx++,symbol.second, book, this);
        auto& fh = m_fhs.back();
        fh.init();
        fh.connect_endpoint("/ws");
    }

    run();
}

void MDCache::run() {

    while(true) {
        for (auto& fh: m_fhs) {
            fh.service();
        }
    }
}
void MDCache::publish(bool buy, const Book& book) {
    static uint64_t next_id {};

    if (!m_connected) {
        spdlog::warn("Didn't connect to DAMD");
        return;
    }

    FIX44::IOI msg;

    // auto& sessionId = m_sessions[j];
    auto& sessionId = session_;
    int j = 0; // potentially support many

    FIX::IOITransType ioiTransType;
    
    try  {
        ioiTransType = FIX::IOITransType_NEW;
        msg.set(ioiTransType);
        msg.set(FIX::IOIID(to_string(++next_id)));
        msg.set(FIX::Symbol(book.symbol));
        msg.set(FIX::Side(buy? FIX::Side_BUY : FIX::Side_SELL));

        auto& [price, size] = buy? *book.bids.rbegin() : *book.asks.begin();
        msg.setField(FIX::FIELD::IoiPi, std::to_string(price));
        msg.setField(FIX::FIELD::IoiPfof, "0.0");
        msg.set(FIX::IOIQty(std::to_string(size)));
        spdlog::info("publish symbol {} side {} on session {}, pi {}, qty {}",
            book.symbol, buy? "BID":"ASK", sessionId.getSenderCompID().getValue(),
            price, size);
    
        /*
        auto itor = m_ioiIds[j].find(make_pair(book.symbol, buy));
        if (itor == m_ioiIds[j].end()) {
            ioiTransType = FIX::IOITransType_NEW;
            spdlog::info("New API Msg");
        }
        else {
            // int i = rand() % 6;
            // ioiTransType = (i < 5? FIX::IOITransType_REPLACE:FIX::IOITransType_CANCEL);
            // msg.set(FIX::IOIRefID(to_string(itor->second)));
            // spdlog::info("{} API Msg", (i < 5? "Replace" : "Cancel"));
            ioiTransType = FIX::IOITransType_REPLACE;
            msg.set(FIX::IOIRefID(to_string(itor->second)));
            spdlog::info("Replace API Msg");           
        }

        msg.set(ioiTransType);
        msg.set(FIX::IOIID(to_string(++next_id)));
        msg.set(FIX::Symbol(book.symbol));
        msg.set(FIX::Side(buy? FIX::Side_BUY : FIX::Side_SELL));

        auto& [price, size] = buy? *book.bids.rbegin() : *book.asks.begin();
        msg.setField(FIX::FIELD::IoiPi, std::to_string(price));
        msg.setField(FIX::FIELD::IoiPfof, "0.0");
        msg.set(FIX::IOIQty(std::to_string(size)));
    
        // if (ioiTransType ==  FIX::IOITransType_CANCEL) {
        //     msg.set(FIX::IOIQty("0"));
        //     m_ioiIds[j].erase(itor);
        // }
        // else {
        //     msg.set(FIX::IOIQty(to_string((rand() % 6 +1) * 100)));
        //     m_ioiIds[j][make_pair(symbol, buy)] = next_id;
        // }
        */
        FIX::Session::sendToTarget(msg, sessionId);

    } catch (std::exception& ex) {
        spdlog::error("Caught exception: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught unknown exception");
    }
}
