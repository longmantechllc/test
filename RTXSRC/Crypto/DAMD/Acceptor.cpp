#include <iostream>
#include <unordered_map>
#include <functional>
#include <regex>
//#include <charconv>  // only in /opt/rh/devtoolset-8/root/usr/include/c++/8/charconv
#include "DAMDCache.h"
#include "Acceptor.h"
#include "IOIMessage.pb.h"
#include <google/protobuf/text_format.h>

using namespace std;

namespace {

    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }

    void test() {
        cout << "DAMDcache messages test" << endl;
        RTX::IOIMessages msgs;
        auto *msg = msgs.add_ioidata();
        msg->set_pfof(1.1);
        msg->set_pi(2.2);
        msg->set_size(63);
        msg->set_timestamp(getNow());
        msg->set_mmid("exch");
        std::string msg_str;
        if (!msgs.SerializeToString(&msg_str)) {
            cout << "Failed to serialize DAMDcache data." << endl;
            return;
        }
        cout << "msg_str.size() = " << msg_str.size() << endl;

        for(int i=0; i < msg_str.size(); i++)
            printf(" %02X", (unsigned char)msg_str[i]);
        cout << endl;
        std::string msg_str2 = msg_str;
        RTX::IOIMessages verify;
        using namespace google::protobuf;
        const Descriptor* descriptor = verify.GetDescriptor();
        assert(descriptor != nullptr);
        const FieldDescriptor* numbers_field = descriptor->
                FindFieldByName("ioiData");
        assert(numbers_field != nullptr);
        assert(numbers_field->label() == FieldDescriptor::LABEL_REPEATED);

        try {
            if (!verify.ParseFromString(msg_str2)) {
                cout << "can't parse protobuf reply" << endl;
            }
            for(int i = 0; i < verify.ioidata_size(); ++i) {
                auto& ioi = msgs.ioidata(i);
                cout << "mmid=" << ioi.mmid() << ", pi=" << ioi.pi()
                     << ", pfof=" << ioi.pfof() << ", size=" << ioi.size()
                     << ", ts=" << ioi.timestamp() << endl;
            }
        }
        catch(...) {
            cout << "exception in ParseFromString" << endl;
        }
    }
}

void Acceptor::start() {
    //::test();
    spdlog::info("Acceptor::start()");
    while (m_run) {
        zmq::message_t request;

        //  Wait for next request from client
        try {
            m_socket.recv(request, zmq::recv_flags::none); // zmq::recv_flags::dontwait
        }
        catch(zmq::error_t& e) {
            spdlog::error("Acceptor got exception, stop");
            return;
        }

        string line = static_cast<const char *>(request.data());
        //logger << "Received " <<  line << endl;

        std::regex newlines_re("[ \t\n\r]+");
        RTX::IOIMessages msgs;
        auto symbol = std::regex_replace(line, newlines_re, "");
        //logger << "take out whitespace line: [" << symbol << "]" << endl;
        spdlog::info("subscribe DAMDcache for [{}]", symbol);

        if (!symbol.empty()) {
            auto &cache = m_DAMDcache.getIoiCache();
    spdlog::info("subscribe cache snapshot ============");
        for(const auto& sym: cache) {
            for(const auto& e: sym.second)
            spdlog::info("{}: symbol {}, exch {}, size {}, pi {}",
                (void*)&e.second, sym.first, e.first, e.second.size, e.second.pi);
        }
    spdlog::info("====================================");    

            auto itor = cache.find(symbol);
            if (itor != cache.end()) {
                const auto &vals = itor->second;
                {
                    std::lock_guard lk(m_DAMDcache.getMutex());
                    for (const auto&[exch, val] : vals) {
    spdlog::info("add ioi {} data on symbol={}, exchange={}, size={}, ts={}, pi={}, pfof={}",
            (void *)&val, symbol, exch, val.size, val.timeStamp, val.pi, val.pfof);
                        auto *msg = msgs.add_ioidata();
                        msg->set_pfof(val.pfof);
                        msg->set_pi(val.pi);
                        msg->set_size(val.size);
                        msg->set_timestamp(val.timeStamp);
                        msg->set_mmid(exch);
                    }
                }
                spdlog::info("DAMDcache vector size from FIX line: {}", vals.size());
            }
            else
                spdlog::warn("can't find DAMDcache data from cache for {}", symbol);
            // // get rlp from redline
            // auto rlps = m_DAMDcache.getRLP(symbol);
            // for(auto& [exch, val] : rlps) {
            //     auto *msg = msgs.add_ioidata();
            //     msg->set_pfof(val.pfof);
            //     msg->set_pi(val.pi);
            //     msg->set_size(val.size);
            //     msg->set_timestamp(val.timeStamp);
            //     msg->set_mmid(exch);
            // }
            // spdlog::info("{} has RLP data size ", symbol, rlps.size());
        }
        else
            continue;

        std::string msg_str;
        if (!msgs.SerializeToString(&msg_str)) {
            spdlog::error("Failed to serialize DAMDcache data.");
            return;
        }

        //  Send reply back to client
        std::string text_str;
        google::protobuf::TextFormat::PrintToString(msgs, &text_str);
        spdlog::info("protobuf msg for :{}\n{}", symbol, text_str);

        zmq::message_t reply(msg_str.size());
        memcpy((void *)reply.data(),msg_str.c_str(), msg_str.size());
        m_socket.send(reply, zmq::send_flags::none);
        //auto rc = zmq_send (m_socket, &reply, 0);
        //assert (rc == 0);
    }
    spdlog::info("Acceptor::start() done");
}

void Acceptor::stop() {
    spdlog::info("Stop Acceptor...\n");
    m_run = false;
    m_socket.close();
    m_context.close();
}
