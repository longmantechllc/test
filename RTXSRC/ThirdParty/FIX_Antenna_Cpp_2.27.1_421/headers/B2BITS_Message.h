// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_Message.h
/// Contains FixDictionary2::Message class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_Message_H
#define H_B2BITS_FixDictionary2_Message_H

#include <string>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_MessageItemContainer.h"
#include "B2BITS_Item.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /** @class Message
    *   @brief Represents Message interface.
    */
    class V12_API Message B2B_SEALED
        : public Item
        , public MessageItemContainer
    {
    public:
        /** Copy Constructor. */
        Message( Message const& message );

        /** Constructor.
        *
        * @param type of message
        *
        * @param name of message
        */
        Message( std::string const& type, std::string const& name );

        /** Make copy of object */
        MessageT::Ptr clone() const {
            return new Message( *this );
        }

        /** Returns name of Message
        *
        * @return name of Message
        */
        std::string const& name() const throw();

        /** Returns name of Message
        *
        * @return name of Message
        */
        std::string const& getName() const throw() {
            return name();
        }

        /** Returns type of Message
        *
        * @return type of Message
        */
        std::string const& type() const throw();

        /** Returns type of Message
        *
        * @return type of Message
        */
        std::string const& getType() const throw() {
            return type();
        }

        /**
        * Assigns message name
        */
        void setName( std::string const& name );

    public: // Item contract
        /** Returns the ItemName of object.
        *
        * @return ItemName of Object
        */
        virtual ItemName itemName() const throw() B2B_OVERRIDE;

        /** Try downcast to Message class
        *
        * @return downcast to Message class
        */
        virtual MessageT::Ptr toMessage() B2B_OVERRIDE;

        /** Try downcast to Message class
        *
        * @return downcast to Message class
        */
        virtual MessageT::CPtr toMessage() const B2B_OVERRIDE;

    protected:
        /** Destructor */
        virtual ~Message() throw();

    private:
        /** Implementation details */
        struct Impl;
        Impl* impl_;

        Message& operator= (Message const&) B2B_DELETE_FUNCTION;
    };

} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_Message_H
