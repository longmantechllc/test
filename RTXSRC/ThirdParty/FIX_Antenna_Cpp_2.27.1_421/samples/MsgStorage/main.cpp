// <copyright>
//
// $Revision$
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>
#include <fstream>
#include <string>
#include <limits>

#include <B2BITS_V12.h>
#include <B2BITS_StorageMgr.h>
#include <B2BITS_MsgStorage.h>

#include "FixStorableMsgAdaptor.h"

// Raw FIX message for writing in the storage
static char const RAW_MSG[] = "8=FIX.4.4""\001""9=1""\001""35=D""\001""49=BLP""\001""56=SCHB""\001""34=01""\001""50=30737""\001""97=Y""\001""52=20000809-20:20:50""\001""11=90001008""\001""1=10030003""\001""21=2""\001""55=TESTA""\001""54=1""\001""38=4000""\001""40=2""\001""59=0""\001""44=30""\001""47=I""\001""60=20000809-18:20:32""\001""10=000""\001";

void testStorage( Engine::MessageStorageType storageType, const std::string& storageId );
void testWrite( Engine::MsgStorage* storage, Engine::MessageStorageType storageType, Engine::FIXMessage* message );
void testRead( int minSeqNum, int maxSeqNum, Engine::MsgStorage* storage, Engine::MessageStorageType storageType );

void runSample()
{
    testStorage( Engine::persistentMM_storageType, "persistentMM" );
    testStorage( Engine::persistent_storageType, "persistent" );
    testStorage( Engine::transient_storageType, "transient" );
    testStorage( Engine::null_storageType, "null" );
}

void testStorage( Engine::MessageStorageType storageType, const std::string& storageId )
{
    std::auto_ptr<Engine::FIXMessage> msg( Engine::FIXMsgProcessor::singleton()->parse( RAW_MSG, sizeof( RAW_MSG ) - 1, 0, false, false, Engine::NA ) );
    std::string sender = msg->getSenderCompID().toStdString();
    std::string target = msg->getTargetCompID().toStdString();
    std::auto_ptr<Engine::MsgStorage> storage( Engine::StorageMgr::singleton( storageType )->create( 
        sender, 
        target, 
        msg->getSessionVersion(), 
        10, 
        Engine::FixEngine::singleton()->getProperties()->getLogDir(), 
        Engine::FixEngine::singleton()->getProperties()->getLogDir() + storageId ) );

    int minSeqNum = msg->getSeqNum();
    testWrite( storage.get(), storageType, msg.get() );
    int maxSeqNum = msg->getSeqNum();
    testRead( minSeqNum, maxSeqNum, storage.get(), storageType );
}

void testWrite( Engine::MsgStorage* storage, Engine::MessageStorageType storageType, Engine::FIXMessage* message )
{
    FixStorableMsgAdaptor adaptor( message );

    int messagesWritten = 0;

    int size;
    const char* raw = message->toRaw( &size );
    bool result = storage->logLocal( &adaptor, message->getSeqNum(), raw, size );
    if ( result ) {
        ++messagesWritten;
    }

    message->setSeqNum( message->getSeqNum() + 1 );
    FixStorableMsgAdaptor adaptor2( message );
    raw = message->toRaw( &size );
    result = storage->logLocal( &adaptor, message->getSeqNum(), raw, size );
    if ( result ) {
        ++messagesWritten;
    }

    std::cout << messagesWritten << " messages has been written to " << Engine::Session::messageStorageTypeToString( storageType ) << " storage." << std::endl;
}

void testRead( int minSeqNum, int maxSeqNum, Engine::MsgStorage* storage, Engine::MessageStorageType storageType )
{
    std::auto_ptr<Engine::CacheHolderBase> cacheHolderBase( storage->prepareLocalRetriving( minSeqNum, maxSeqNum, std::numeric_limits<int>::max() ) );
    std::vector<Engine::FIXMessage*> messages;
    storage->retrieveLocal( cacheHolderBase.get(), Engine::FixEngine::singleton()->getParserID( "FIX44" ),
                            &messages, minSeqNum, maxSeqNum );

    std::cout << messages.size() << " messages has been read from " << Engine::Session::messageStorageTypeToString( storageType ) << " storage." << std::endl;
}

int main()
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        Engine::FixEngine::init();
        runSample();
        Engine::FixEngine::destroy();
    } catch( Utils::Exception const& e ) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

}
