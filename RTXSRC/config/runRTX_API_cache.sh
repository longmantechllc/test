## This scripts will execute RTX Normalizer. 

cd /home/hjoshi/RTX_DEMO/RTX_API/store
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_API/logs
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_API

## set core dump
ulimit -S -c unlimited > /dev/null 2>&1 

#set path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# Run OSM
nohup ./RTX_IOINormalizer IOINormalizer.cfg </dev/null &>/dev/null &