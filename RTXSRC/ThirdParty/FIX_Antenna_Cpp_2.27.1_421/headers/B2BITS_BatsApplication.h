// <copyright> 
// 
// $Revision: 1.3 $ 
//  
// (c) B2BITS EPAM Systems Company 2000-2017. 
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS). 
//  
// This software is for the use of the paying client of B2BITS (which may be 
// a corporation, business area, business unit or single user) to whom it was 
// delivered (the "Licensee"). The use of this software is subject to 
// license terms. 
// 
// The Licensee acknowledges and agrees that the Software and Documentation 
// (the "Confidential Information") is confidential and proprietary to 
// the Licensor and the Licensee hereby agrees to use the Confidential 
// Information only as permitted by the full license  agreement between 
// the two parties, to maintain the confidentiality of the Confidential 
// Information and not to disclose the confidential information, or any part 
// thereof, to any other person, firm or corporation. The Licensee 
// acknowledges that disclosure of the Confidential Information may give rise 
// to an irreparable injury to the Licensor in-adequately compensable in 
// damages. Accordingly the Licensor may seek (without the posting of any 
// bond or other security) injunctive relief against the breach of the forgoing 
// undertaking of confidentiality and non-disclosure, in addition to any other 
// legal remedies which may be available, and the licensee consents to the 
// obtaining of such injunctive relief. All of the undertakings and 
// obligations relating to confidentiality and non-disclosure, whether 
// contained in this section or elsewhere in this agreement, shall survive 
// the termination or expiration of this agreement for a period of five (5) 
// years. 
// 
// The Licensor agrees that any information or data received from the Licensee 
// in connection with the performance of the support agreement relating to this 
// software shall be confidential, will be used only in connection with the 
// performance of the Licensor's obligations hereunder, and will not be 
// disclosed to third parties, including contractors, without the Licensor's 
// express permission in writing. 
// 
// Information regarding the software may be provided to the Licensee's outside 
// auditors and attorneys only to the extent required by their respective 
// functions. 
// 
// </copyright> 

#ifndef B2BITS_BATS_APPLICATION_H
#define B2BITS_BATS_APPLICATION_H

#include <deque>

#include <B2BITS_BatsUnitService.h>

/** @addtogroup bats_mda
* @{
*/

/// BATS namespace
namespace Bats
{
/** @addtogroup bats_mda_rtc
* @{ */

    /// Thread types
    struct ThreadType {
        enum Type {
            /// Threads of this type are only used for reading UDP multicast feeds
            IncrementalThread = 0,
            /// Threads of this type are used for TCP communications with GRP/Spin servers, 
            /// internal timers, etc...
            WorkerThread
        };
    };

    /// Types of increment readers, represent ways of reading data from UDP socket
    struct IncrementReaderType {
        enum Type {
            /// Incremental threads are organized in pool (even for 1 thread).
            /// Thread pools can be shared between unit services. Behaviour depends on UDPParams::recvTimeoutUS
            ThreadPool = 0, 
            /// A separate thread is created for each unit service.
            /// Behaviour depends on UDPParams::recvTimeoutUS
            DedicatedThread,
            /// [Myricom DBL](https://www.myricom.com/support/downloads/dbl.html) thread pool is used. 
            /// Multiple threads reading from one DBL interface.
            /// Synchronous, non blocking read. 100% CPU usage for each thread.
            /// Can be shared between unit services, which use the same network interface
            MyricomDBL } ;
    };

    /// Parameters used for TCP sockets (replay and recovery)
    struct TCPParams
    {
        /// Connect timeout, msec\n
        /// Default value: 0 - no timeout, use OS default
        System::u32 connectTimeoutMS;
        /// Send timeout, msec\n
        /// Default value: 0 - no timeout, use OS default
        System::u32 sendTimeoutMS;
        /// Receive timeout, msec\n
        /// Default value: 0 - no timeout, use OS default
        System::u32 recvTimeoutMS;

        /// Send socket buffer size\n
        /// Default value: -1 - use OS default
        int sendBufferSize;
        /// Receive socket buffer size\n
        /// Default value: -1 - use OS default
        int recvBufferSize;

        TCPParams() : connectTimeoutMS(0), sendTimeoutMS(0), recvTimeoutMS(0)
                          , sendBufferSize(-1), recvBufferSize(-1) {}
    };

    /// Parameters used for UDP sockets
    struct UDPParams
    {
        /// Receive timeout, usec\n
        /// Default value: 0
        ///
        /// This parameter is used when reading from IncrementReaderType::DedicatedThread
        /// or IncrementReaderType::ThreadPool. Possible values are:
        /// - 0 - no timeout, use non blocking read consuming 100% CPU (each thread in case of pool)
        /// to achieve the lowest possible latency
        /// - >0 - use blocking read with timeout
        /// - <1 - use blocking read with use OS default timeout
        int recvTimeoutUS;

        /// Receive socket buffer size\n
        /// Default value: -1 - use OS default
        int recvBufferSize;

        UDPParams() : recvTimeoutUS(0), recvBufferSize(-1) {}
    };

    /// @ingroup bats_mda_rnr
    /// Sequence processing parameters
    struct SequenceParams
    {
        /// Maximum number of incoming messages allowed to be cached.
        /// Normally, when out-of-order UDP packet is received, GRP replay is not started immediately.
        /// Unit service caches incoming packet in memory and proceeds receiving data (see @ref replayDelaySize).
        /// If all missed packets are received, service will line them up in correct order and continue 
        /// normal processing.
        /// This parameter specifies size of the cache.
        /// When number of cached messages exceeds the limit, service clean out the cache
        /// and initiates recovery process (from Spin server)\n
        /// Default value: 1000
        System::u32 cacheSize;

        /// Maximum number of incoming UDP packets allowed to be stored during recovery process.
        /// While TCP session with Spin server is in progress, unit service caches all incoming 
        /// multicast packets in memory, to process them later when TCP session is done.
        /// This parameter specifies the size of the cache. 
        /// When limit is reached, recovery is considered as failed and started from the beginning.\n
        /// Default value: 50000
        System::u32 spinCacheSize;

        /// Replay delay (messages).
        /// This parameter specifies the number of messages service should wait before initiating 
        /// replay from GRP server. This is difference between last received sequence number and
        /// last processed(ordered) sequence number.\n
        /// Default value: 10
        System::u32 replayDelaySize;

        /// Time to replay(msec).
        /// How many msecs to wait after replay has been initiated, before service is allowed to
        /// initiate another replay.\n
        /// Default value: 50
        System::u32 replayTimeoutMS;

        /// Maximum number of messages that can be requested at once from GRP server.\n
        /// Default value: 100
        System::u16 gapRequestLimit;

        SequenceParams() : cacheSize(1000), spinCacheSize(50000), replayDelaySize(10)
            , replayTimeoutMS(50), gapRequestLimit(100) {}
    };


    /// Bats::Application runtime parameters.
    /// Parameters can be global, or specified at channel or unit levels
    /// See @ref bats_mda_configuration for details
    struct RuntimeParameters
    {
        /// Type of increments reader.
        /// Default value: IncrementReaderType::ThreadPool
        IncrementReaderType::Type readerType;

        /// Path of the directory where the log files will be created.
        /// Default value: "." - current directory
        std::string logDirectory;

        /// Use 'natural refresh' instead of GRP/Spin.
        /// Setting this option to `true` allows to recover from gaps without using GRP 
        /// or Spin server. Instead of initiating replay or recovery, service generates
        /// `onReset` event with ResetReason::rrNaturalRefresh, clears 
        /// cache, all books and process current packet as properly ordered.\n
        /// Default value: `false`
        /// @note Please note that in case of 'natural refresh' state of book is not restored
        /// immediately, and there is now way to find out when the book contains consistent data,
        /// but due to short lifetime of BATS orders, 'natural refresh' could be useful. Set this 
        /// property to `true` at your own risk
        bool useNaturalRefresh;

        /// Log incoming messages as text.
        /// All incoming UDP messages will logged as text to the log file.\n
        /// Default value: `false`
        /// @note Setting this value to true, will impact upon the performance. 
        bool logIncomingMessages;

        /// Log incoming messages to binary files.
        /// All incoming UDP and TCP messages will logged to the files in binary format.\n
        /// Default value: `false`
        /// @note Setting this value to true, will impact upon the performance. 
        bool logIncomingBinaryMessages;

        /// Use worker pool for increments.
        /// When this parameter is set to `true`, thread pool of worker threads is used
        /// for reading UDP multicast messages. Incremental threads are not created.
        /// RuntimeParameters::readerType parameter is ignored.\n
        /// Default value: `false`
        bool useWorkersForIncrements;

        /// Number of seconds before ServiceListener::Notification::LinkDown notification is generated.
        /// If this property is set to 0 - LinkXXX notifications are not generated.\n
        /// Default value = 0
        System::u16 linkDownTimeoutS;

        /// Number of incremental threads.
        /// If set to 0 - no threads will be created.\n Default value: 2
        System::u16 incrementalThreads;

        /// Number of worker threads.
        /// If set to 0 - no threads will be created.\n Default value: 2
        System::u16 workerThreads;

        /// Minimum acceptable number of shares.
        /// Orders with quantity less than this value will be ignored.\n
        /// Default value: 1 - all orders are accepted
        System::u16 minSharesValue;

        /// TCP parameters
        TCPParams tcpParams;
        /// UDP parameters
        UDPParams udpParams;

        /// Sequence parameters
        SequenceParams sequenceParams;


        RuntimeParameters() : readerType(IncrementReaderType::ThreadPool), logDirectory("."), useNaturalRefresh(false), logIncomingMessages(false)
                            , logIncomingBinaryMessages(false), useWorkersForIncrements(false), linkDownTimeoutS(0)
                            , incrementalThreads(2), workerThreads(2), minSharesValue(1) {}
    };


    /// Options to pass to Engine::createBatsApplication
    struct ApplicationOptions
    {
        /// Application global runtime parameters
        RuntimeParameters params;
        /// Name of the file with BATS XML configuration
        std::string xmlConfig;
    };

/**@}*/

    class ApplicationListener;
    class UnitService;

/** @addtogroup bats_mda_application
* @{
*/
    /// @ingroup bats_mda_gs
    /// Represents BATS application
    class Application
    {
    public:
        /// Returns application parameters
        /**
        * @return const reference to RuntimeParameters passed to Engine::createBatsApplication
        * @throw no
        */
        virtual const RuntimeParameters& getParams() const = 0;

        /// Returns channel options
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @return const reference to RuntimeParameters specified for particular channel
        * @throw Utils::Exception if error occurs
        */
        virtual const RuntimeParameters& getChannelParams(const std::string& channelId) const = 0;

        /// Overrides application options for channel
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param params parameters for particular channel
        * @throw Utils::Exception if error occurs
        */
        virtual void setChannelParams(const std::string& channelId, const RuntimeParameters& params) = 0;

        /// Destroys application
        /**
        * Disconnects and destroys all running services and threads. 
        * @note Method is synchronous and 
        * waits for threads to quit. Calling this method from one of incremental or worker 
        * threads will lead to deadlock.
        * @throw no
        */
        virtual void release() const = 0;

        /// Returns the number of created services.
        /**
        * You can get a signle service instance using getService(unsigned index) const method
        * @throw no
        */
        virtual unsigned getServicesCount() const = 0;


        /// Creates service by channel and unit id
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        * @param iface Address of a local network interface to use when subscribing to multicast feed
        * @param params Runtime parameters, when specified, override current channel's parameters
        *
        * @return pointer to created Bats::UnitService
        *
        * @throw Utils::Exception if error occurs, usually if service has been misconfigured
        *
        * @note If service has already been created this method is equal to getService(const std::string& channelId, System::u8 unitId) const
        */
        virtual UnitService* createService(const std::string& channelId, System::u8 unitId, const std::string& iface, const RuntimeParameters* params = NULL) = 0;

        /// Creates service by channel, and unit params
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param symbol Symbol to subscribe to. Proper unit will be found using _unit\@coderange_ attribute in XML configuration file
        * @param expiration _unit\@expiration_ attribute in XML configuration file
        * @param putOrCall _unit\@putorcall_ attribute in XML configuration file
        * @param iface Address of a local network interface to use when subscribing to multicast feed
        * @param params Runtime parameters, when specified, override current channel's parameters
        *
        * @return pointer to created Bats::UnitService
        *
        * @throw Utils::Exception if error occurs, usually if service has been misconfigured
        *
        * @note If service has already been created this method is equal to getService(const std::string& channelId, const std::string& symbol, const std::string& expiration, const std::string& putOrCall) const
        */
        virtual UnitService* createService(const std::string& channelId, const std::string& symbol, 
                                           const std::string& expiration, const std::string& putOrCall, const std::string& iface, const RuntimeParameters* params = NULL) = 0;

        /// Creates all services specified in configuration file
        /**
        * Logs error message if some of the service have not been created
        * @param iface Address of a local network interface to use when subscribing to multicast feed
        * @param params Runtime parameters, when specified, override current channel's parameters
        *
        * @throw no
        */
        virtual void createServices(const std::string& iface, const RuntimeParameters* params = NULL) = 0;

        /// Returns service by index
        /**
        * Returns service by zero based index
        * @param index Zero-based index of the service, must be < getServicesCount()
        *
        * @return pointer to Bats::UnitService or NULL if index is invalid
        * @throw no
        */
        virtual UnitService* getService(unsigned index) const = 0;

        /// Returns service by channel and unit id
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        *
        * @return pointer to Bats::UnitService or NULL if service has not been created yet
        * @throw no
        */
        virtual UnitService* getService(const std::string& channelId, System::u8 unitId) const = 0;

        /// Returns service by channel, and unit params
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param symbol Symbol to subscribe to. Proper unit will be found using _unit\@coderange_ attribute in XML configuration file
        * @param expiration _unit\@expiration_ attribute in XML configuration file
        * @param putOrCall _unit\@putorcall_ attribute in XML configuration file
        *
        * @return pointer to Bats::UnitService or NULL if service has not been yet created
        * @throw no
        */
        virtual UnitService* getService(const std::string& channelId, const std::string& symbol, 
                                        const std::string& expiration, const std::string& putOrCall) const = 0;

        /// Disconnects and destroys a service from the application
        /**
        * @note Method is asynchronous, it returns immediately. Exact moment when service is removed can 
        * be handled with ApplicationListener::onServiceRemoved() notification. It is allowed to call this 
        * method from any thread.
        *
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        * @throw Utils::Exception if error occurs
        */
        virtual void removeService(const std::string& channelId, System::u8 unitId) = 0;

        /// Disconnects and destroys all services from the application
        /**
        * @note Method is synchronous and waits for threads to quit. Calling this method from one of 
        * incremental or worker threads will lead to deadlock
        * @throw no
        */
        virtual void removeServices() = 0;

        /// Returns unit Id or throws if unit has not been configured.
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param symbol Symbol to subscribe to. Proper unit will be found using _unit\@coderange_ attribute in XML configuration file
        * @param expiration _unit\@expiration_ attribute in XML configuration file
        * @param putOrCall _unit\@putorcall_ attribute in XML configuration file
        *
        * @return unit id
        * @throw Utils::Exception if unit has not been configured
        */
        virtual System::u8 findUnitId(const std::string& channelId, const std::string& symbol, 
                                        const std::string& expiration, const std::string& putOrCall) = 0;
    };

    /// @Application observer
    /**
    * Provides interface to receive application level events. Unit level notifications 
    * are also available via ApplicationListener::onNotification callback. Notifications are 
    * forwarded from each service application creates
    */
    class ApplicationListener
    {
    public:
        /// Called from each thread on start
        /**
        * @param type Type of the thread
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        * @param index Zero-based index of the thread within its pool
        * @param count Total number of threads in the pool
        */
        virtual void onStartThread(ThreadType::Type type, const std::string& channelId, System::u8 unitId, unsigned index, unsigned count) {
            (void)&type;(void)&channelId;(void)&unitId;(void)&index;(void)&count;
        }

        /// Called from each thread at exit
        /**
        * @param type Type of the thread
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        * @param index Zero-based index of the thread within its pool
        * @param count Total number of threads in the pool
        */
        virtual void onStopThread(ThreadType::Type type, const std::string& channelId, System::u8 unitId, unsigned index, unsigned count) {
            (void)&type;(void)&channelId;(void)&unitId;(void)&index;(void)&count;
        }

        /// Called after a service is created and added to the application
        /**
        * @param service Service that has just been created
        */
        virtual void onServiceAdded(UnitService* service) { (void)&service; }

        /// Called right before a service is removed from the application and destroyed
        /**
        * @param service Service to be destroyed
        */
        virtual void onServiceRemoving(UnitService* service) { (void)&service; }

        /// Called after a service is removed from the application and destroyed
        /**
        * @param channelId Id of the channel, specified as _channel\@id_ attribute in XML configuration file
        * @param unitId Id of the unit specified as _unit\@id_ attribute in XML configuration file
        */
        virtual void onServiceRemoved(const std::string& channelId, System::u8 unitId) { (void)&channelId;(void)&unitId; }

        /// Forwarded ServiceListener::onNotification events
        /**
        * @param service Service that generated notification
        * @param type Notification type
        */
        virtual void onNotification(UnitService* service, ServiceListener::Notification::Type type) { (void)&service;(void)&type; }
    };

/**@}*/
}

/**@}*/
#endif  //  B2BITS_BATS_APPLICATION_H
