
#include "RTX_OSM.h"

#include <iostream>
#include <fstream>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>
#include <regex>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "spdlog/fmt/ostr.h" // must be included to log object
#include "Admin.h"
#include "IOIMessage.pb.h"
#include <google/protobuf/text_format.h>

using namespace ::std;
namespace bip = boost::interprocess;

namespace {
    bool isOpen(const OSMOrder& order) {
        switch (order.status) {
            case ERSTATUS::FILLED:
            case ERSTATUS::CANCELED:
            case ERSTATUS::REJECTED:
                return false;
            default:
                return true;
        }
    }
}
namespace {
    void test() {
        cout << "API messages test" << endl;
        RTX::IOIMessages msgs;
        auto *msg = msgs.add_ioidata();
        msg->set_mmid("exch");
        msg->set_pfof(1.1);
        msg->set_pi(2.2);
        msg->set_size(63);
        msg->set_timestamp(127);

        std::string msg_str;
        if (!msgs.SerializeToString(&msg_str)) {
            cout << "Failed to serialize API data." << endl;
            return;
        }
        cout << "msg_str.size() = " << msg_str.size() << endl;

        for(int i=0; i < msg_str.size(); i++)
            printf(" %02X", (unsigned char)msg_str[i]);
        cout << endl;

        std::string msg_str2 = msg_str;
        RTX::IOIMessages verify;
        using namespace google::protobuf;
        const Descriptor* descriptor = verify.GetDescriptor();
        assert(descriptor != nullptr);
        const FieldDescriptor* numbers_field = descriptor->
                FindFieldByName("ioiData");
        assert(numbers_field != nullptr);
        assert(numbers_field->label() == FieldDescriptor::LABEL_REPEATED);

        try {
            if (!verify.ParseFromString(msg_str2)) {
                cout << "can't parse protobuf reply" << endl;
            }
            for(int i = 0; i < verify.ioidata_size(); ++i) {
                auto& ioi = msgs.ioidata(i);
                cout << "mmid=" << ioi.mmid() << ", pi=" << ioi.pi()
                     << ", pfof=" << ioi.pfof() << ", size=" << ioi.size()
                     << ", ts=" << ioi.timestamp() << endl;
            }
        }
        catch(...) {
            cout << "exception in ParseFromString" << endl;
        }
    }
}

RTX_OSM::RTX_OSM(const ApplicationProperties& params)
    : params_(params), m_context(5)
{
    //test();

    try {

        FIX::SessionSettings settings(params.acceptor_cfg);

        //Application application;
        FIX::FileStoreFactory storeFactory(settings);
        FIX::FileLogFactory logFactory(settings);
        m_acceptor = make_unique<FIX::SocketAcceptor>(/*application*/*this, storeFactory, settings, logFactory);
        m_acceptor->start();
        spdlog::info("Listening for incoming connection...");

        FIX::SessionSettings settings2( params.initiator_cfg );

        //Application application;
        FIX::FileStoreFactory storeFactory2( settings2 );
        FIX::FileLogFactory logFactory2( settings2 );
        m_initiator = make_unique<FIX::SocketInitiator>( *this, storeFactory2, settings2, logFactory2);

        m_initiator->start();
        spdlog::info("connecting to gateway...");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}", e.what());
        return;
    }

    // loading virtual destination file
    if (!params_.virtualDestFile.empty()) {
        ifstream in(params_.virtualDestFile.c_str());
        if (!in) {
            spdlog::warn("Cannot open virtual destination file {}", params_.virtualDestFile);
        }
        std::string line;
        //using boost::algorithm::trim;
        while (std::getline(in, line)) {
            if (line.size()) {
                if (line[0] == '#')
                    continue;
                // Delimiters are spaces tab :
                regex re("[:]+");
                sregex_token_iterator it(line.begin(), line.end(), re, -1);
                sregex_token_iterator reg_end;
                string dest, tags;
                VirtualDest vdest;
                for (int i = 0; it != reg_end; ++it, ++i) {
                    if (i == 0) dest = it->str();
                    else if (i == 1) vdest.dest = it->str();
                    else if (i == 2) tags = it->str();
                    else break;
                }
                if (!tags.empty()) {
                    re = "[|]+";
                    sregex_token_iterator it2(tags.begin(), tags.end(), re, -1);
                    for (int i = 0; it2 != reg_end; ++it2) {
                        int tag;
                        string value;
                        auto pos = it2->str().find('=');
                        if (pos != string::npos) {
                            try {
                                vdest.vec.push_back({stoi(it2->str().substr(0, pos - 1)), it2->str().substr(pos + 1)});
                            }
                            catch(...) {
                                spdlog::info("wrong format at line: {}", line);
                                break;
                            }
                        }
                    }
                }
                m_virtualDestMap[dest] = vdest;
                spdlog::info("virtual dest from {} to {}, with extra tags size = {}",
                             dest, vdest.dest, vdest.vec.size());
            }
        }
    }

    // start SOR
    if (!sor.InitializeApp(".")) {
        spdlog::error("InitializeApp failed. Shutting down");
        return;
    }

    RTXOrderIO_ = sor.GetOrderIO();
    RTXOrderIO_->SetOrderIO(this); // pass OSM to RTX for it to call these callback functions.

    // m_md.setClientName(params_.mdClientName);
    // m_md.setCheckingTime(params_.mdCheckingTime);
    // if (!m_md.isInit()) {
    //     spdlog::error("Cannot connect to Redline MD server. Shutting down");
    //     return;
    // }
    MDMsgQueue* mdMsgQueuePtr {nullptr};
    if (params_.simMD) {
        
        auto rmd = new SimMD(params_.symbolFile, params_.exchs);
        spdlog::info("using SimMD");
        m_md  = rmd;
        mdMsgQueuePtr = rmd->getMDMsgQueuePtr();
    }
    else {
        auto rmd = new RedlineMD();
        spdlog::info("using RedlineMD");
        m_md  = rmd;
        rmd->setClientName(params_.mdClientName);
        rmd->setCheckingTime(params_.mdCheckingTime);
        if (!rmd->isInit()) {
            spdlog::error("Cannot connect to Redline MD server. Shutting down");
            return;
        }
        mdMsgQueuePtr = rmd->getMDMsgQueuePtr();
    }

    RTXMarketIO_ = sor.GetMktDataIO();
    spdlog::info("SetMktDataIO with OSM m_md");
    // RTXMarketIO_->SetMktDataIO(&m_md); // pass MD to RTX for it to call these callback functions.
    RTXMarketIO_->SetMktDataIO(m_md);
    m_md->addSubscriber(RTXMarketIO_.get());
    // RTXMarketIO_->SetMsgQueue(m_md->getMDMsgQueuePtr());
    RTXMarketIO_->SetMsgQueue(mdMsgQueuePtr);

    // DB
    m_db = make_unique<DBConnector> (params_.dbHost, params_.dbPort, params_.dbUser, params_.dbPassword,
        m_stateQ, m_childQ, m_parentQ, m_exeQ, params_.dbLogging);
    m_db->init();
    m_dbThread = std::thread([&] {
        while (m_db->running()) {
            m_db->processingMessage();
        }
    });

    m_toKeep = new ToKeep{0, 0, 0};
    try {
        auto [parentOrdId, gwId, execId] = m_db->getMaxIds(params_.routerName);
        m_toKeep->parentOrdId = parentOrdId;
        m_toKeep->gwId = gwId;
        m_toKeep->execId = execId;
        spdlog::info("the max parentOrdId = {}, gatewayid = {}, execId = {}",
                     m_toKeep->parentOrdId, m_toKeep->gwId, m_toKeep->execId);

        if (params_.recoveryMode) {
            spdlog::debug("recovery data from DB views");
            m_db->getOpenOrders(params_.routerName, m_recoveryPOrderMap, m_recoveryCOrderMap);
            spdlog::info("recovery parent order# {}, child order# {}",
                         m_recoveryPOrderMap.size(), m_recoveryCOrderMap.size());
        }
    }
    catch (const mysqlx::Error &err) {
        spdlog::error("recovery failed: {}", err.what());
        return;
    }
    catch(...) {
        spdlog::error("unknown exception");
        return;
    }

    m_admin = make_unique<Admin>(params_.adminPort, this);
    m_adminThread = std::thread([this]{
        m_admin->start();
    });
    // admin thread
    spdlog::info("extraLogging = {}", params_.extraLogging);
    if (params_.extraLogging) {
        m_monitorThread = std::thread([&, this] {
            while (m_monitor_running) {
                int pnum = 0, cnum = 0;
                {
                    std::shared_lock guard(m_pmutex);
                    for (const auto &o: m_parentOrderMap) {
                        if (isOpen(o.second))
                            ++pnum;
                    }
                }
                {
                    std::shared_lock guard(m_mutex);
                    for (const auto &o: m_childOrderMap) {
                        if (isOpen(o.second))
                            ++cnum;
                    }
                }
                spdlog::info("TotalOID: {}, OpenOID: {}, OpenChild: {}",
                             m_parentOrderMap.size(), pnum, cnum);
                if (params_.dbLogging)
                    spdlog::info("Total to DB queue: parent: {}, child: {}, state: {}, execution: {}",
                             m_tblRecordNum[0], m_tblRecordNum[1], m_tblRecordNum[2], m_tblRecordNum[3]);
                std::this_thread::sleep_for(std::chrono::seconds(1));
            }
        });
    }

    if (!params_.ioiServer.empty()) {
        connectNormalizer();
    }
    RTXIoiIO_ = sor.GetIOIDataIO();
    RTXIoiIO_->SetIOIInterface(this); // pass MD to RTX for it to call these callback functions.
    //markExchanges(false);
    markExchanges(true);
    m_init = true;
}
void RTX_OSM::markExchanges(bool status) {
    //std::unique_lock guard(mutex_);
    for(const auto& dest: m_virtualDestMap) {
        spdlog::info("virtual dest {}  is {}", dest.first, status? "up" : "down");
        RTXOrderIO_->UpdateDestinationAvailability(dest.first, status);
        m_exchStatus[dest.first] =  status;
    }
}
std::string RTX_OSM::connectNormalizer() {
    std::string result;

    spdlog::info("connecting to APIcache ...");
    string add("tcp://");
    add.append(params_.ioiServer).append(":").append(to_string(params_.ioiPort));
    
    m_socket = zmq::socket_t(m_context, ZMQ_REQ);
    int timeOut = 10;
    int rc = zmq_setsockopt(m_socket.handle(), ZMQ_SNDTIMEO, &timeOut, sizeof(timeOut));
    if (rc != 0) {
        spdlog::error("can't set socket ZMQ_SNDTIMEO");
    }
    // sndtimeo, rcvtimeo
    rc = zmq_setsockopt(m_socket.handle(), ZMQ_RCVTIMEO, &timeOut, sizeof(timeOut));
    if (rc != 0) {
        spdlog::error("can't set socket ZMQ_RCVTIMEO");
    }
    /*timeOut = 0; // Discard pending messages in buf on close
    rc = zmq_setsockopt(m_socket, ZMQ_LINGER, &timeOut, sizeof(timeOut));
    if (rc != 0) {
        logger << "can't set socket ZMQ_LINGER" << endl;
    }
     */
    // https://learning-0mq-with-pyzmq.readthedocs.io/en/latest/pyzmq/patterns/client_server.html
    // seems like zmq client can connect multiple servers for load balancing.
    rc = zmq_connect(m_socket.handle(), add.data());
    if (rc != 0) {
        switch(errno) {
            case EINVAL:
                result = "The endpoint supplied is invalid.";
                break;
            case EPROTONOSUPPORT:
                result = "The requested transport protocol is not supported.";
                break;
            case ENOCOMPATPROTO:
                result = "The requested transport protocol is not compatible with the socket type.";
                break;
            case ETERM:
                result = "The ØMQ context associated with the specified socket was terminated.";
                break;
            case ENOTSOCK:
                result = "The provided socket was invalid.";
                break;
            case EMTHREAD:
                result = "No I/O thread is available to accomplish the task.";
                break;
            default:
                result =  "unknown error.";
                break;

        }
        result = string("can't connect to APIcache: ") + result +"\n";
        m_sendingIOI = false;
    }
    else {
        m_sendingIOI = true;
        result = "connected to APIcache\n";
    }

    spdlog::info(result);
    return result;
}

void RTX_OSM::stopAdmin() {
    m_admin->stop();
}

RTX_OSM::~RTX_OSM()
{
    m_acceptor->stop();
    m_initiator->stop();
    spdlog::info("stop both client & gateway sessions");
    if (m_init) {
        m_dbThread.join(); // delete thread here
        m_adminThread.join();
        if (params_.extraLogging) {
            m_monitorThread.join();
        }
        spdlog::info("all threads are shut down");
    }
    zmq_close(m_socket);
    zmq_ctx_destroy(&m_context);
}

std::string RTX_OSM::getNewGWId() {
    return std::to_string(++ m_toKeep->gwId);
}

std::string RTX_OSM::getNewExecId() {
    return std::to_string(++ m_toKeep->execId);
}
void RTX_OSM::cancelLogin(const string& str) {
    std::shared_lock guard(m_pmutex);
    for(auto& o: m_parentOrderMap) {
        if (o.second.logInId == str && isOpen(o.second))
            RTXOrderIO_->CancelParentOrder(o.first);
    }
}

void RTX_OSM::cancelSubAccount(const string& str) {
    std::shared_lock guard(m_pmutex);
    for(auto& o: m_parentOrderMap) {
        if (o.second.subAccount == str && isOpen(o.second))
            RTXOrderIO_->CancelParentOrder(o.first);
    }
}

void RTX_OSM::cancelVenue(const string& str) {
    std::shared_lock guard(m_mutex);
    for(auto& o: m_childOrderMap) {
        if (o.second.exchange == str && isOpen(o.second))
            cancelChild(o.first);
    }
}

void RTX_OSM::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void RTX_OSM::onLogon( const FIX::SessionID& sessionID )
{
    bool isClient = false;

    if (m_acceptor->getSession(sessionID)) {
        cliSession_ = sessionID;
        isClient = true;
    }
    else {
        session_ = sessionID;
        //markExchanges(true);
    }
    spdlog::info("Logon - {} {}", (isClient? "Client" : "Gateway"), sessionID);
}

void RTX_OSM::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("Logout - {}", sessionID);
    if (sessionID == session_) {
        //markExchanges(false);
    }
}

void RTX_OSM::toAdmin( FIX::Message& message, const FIX::SessionID& )
{
    spdlog::info("ADMIN OUT: {}", message);
}

void RTX_OSM::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("ADMIN IN: {}", message);
}

void RTX_OSM::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("App IN: {}", message);
    crack( message, sessionID );
}

void RTX_OSM::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("App OUT: {}", message);
}

std::vector<IOIMarketData> RTX_OSM::SubscribeIOI(std::string symbol, bool isBuy) {
    symbol += (isBuy? ",B\n": ",S\n");
    spdlog::info("Subscribe APIcache on symbol {}", symbol);

    std::unique_lock guard(mutex_); // add mutex to sync multi-thread call of this function

    zmq::message_t request (symbol.size()+1);
    memcpy (request.data (), symbol.c_str(), symbol.size()+1);

    zmq::message_t reply;
    int times = 0;
again:
    try {
        auto rc = m_socket.send(request, zmq::send_flags::none);

        if (rc)
            spdlog::info("send return {}", *rc);
        else {
            spdlog::info("sending buffer full, try again");
            std::this_thread::sleep_for(1ms);
            goto again;
        }

again2:
        rc = m_socket.recv(reply, zmq::recv_flags::none);
        if (rc)
            spdlog::info("recv return {}", *rc);
        else {
            spdlog::info("recv interrupted, try again");
            std::this_thread::sleep_for(1ms);
            goto again2;
        }
    }
    catch(...) {
        auto res = connectNormalizer();
        spdlog::warn("got exception, connecting to APIcache again: {}", res);
        if (times < 3) {
            ++times;
            spdlog::info("try APIcache again, {} times", times);
            goto again;
        }
        else
            return {};
    }

    std::string msg_str(static_cast<char*>(reply.data()), reply.size());

    RTX::IOIMessages msgs;

    try {
        if (!msgs.ParseFromString(msg_str)) {
            spdlog::error("can't parse protobuf reply");
            return {};
        }
    }
    catch(...) {
        spdlog::error("exception in ParseFromString");
        return {};
    }

    std::string text_str;
    google::protobuf::TextFormat::PrintToString(msgs, &text_str);

    std::vector<IOIMarketData> result;
    result.reserve(msgs.ioidata_size());
    for(int i = 0; i < msgs.ioidata_size(); ++i) {
        IOIMarketData ioi;
        ioi.mmid = msgs.ioidata(i).mmid();
        ioi.pi = msgs.ioidata(i).pi();
        ioi.pfof = msgs.ioidata(i).pfof();
        ioi.size = msgs.ioidata(i).size();
        //int priority;
        ioi.timeStamp = msgs.ioidata(i).timestamp();
        result.push_back(ioi);
        spdlog::info("{},{},{},{},{}",
                     ioi.mmid, ioi.pi.ToDouble(),
                     ioi.pfof.ToDouble(), ioi.size, ioi.timeStamp);
    }

    //logger << "get APIcache data for " << symbol << ", side " << (isBuy? "Buy" : "Sell")
    //        << ", vector size " << result.size() << endl;
    return result;
}