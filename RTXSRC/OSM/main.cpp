
#include <iostream>
#include <ctime>
#include <cstdio>
#include <memory>

//#include <B2BITS_V12.h>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "Properties.h"
#include "ApplicationProperties.h"
#include "RTX_OSM.h"
#include "Version.h"
#include "DBConnector.h"

using namespace ::std;
using namespace ::Aux;

/// Application entry point.
int main(int argc, char* argv[])
{
    try
    {
        // Create basic file logger (not rotated)
        auto my_logger = spdlog::basic_logger_mt("OSM", "logs/OSM.log");

        // create a file rotating logger with 5mb size max and 3 rotated files
        //auto file_logger = spdlog::rotating_logger_mt("file_logger", "myfilename", 1024 * 1024 * 5, 3);
        spdlog::set_default_logger(my_logger);
    }
    catch (const spdlog::spdlog_ex& ex)
    {
        std::cout << "Log initialization failed: " << ex.what() << std::endl;
        return -1;
    }

    spdlog::info("RTX release version {} coming up...", osm_version);

    string applicationName(argv[0]);

    // command line parameters
    std::string gw_properties_file = "RTX_Router.cfg";

    Helper::writeLineToConsole("RTX_Intelligent_Router © 2021\n");

    if (2 == argc) {
        gw_properties_file = argv[1];
    } else {
        Helper::writeLineToConsole("Usage: RTX_OSM <RTX_OSM properties file> \n");
        return 1;
    }

    try {
        spdlog::info("Init engine...");

        ifstream in(gw_properties_file.c_str());

        if (!in) {
            throw string("Cannot open file ").append(gw_properties_file);
        }

        Aux::Properties p(&in);
        ApplicationProperties appParams;
        Helper::parseCommandLine(p, &appParams);
        
        RTX_OSM application(appParams);
        if (!application.isInited())
            return 0;

        string cmd;
        while ("exit" != cmd) {
            cout << "Type 'exit' to exit > " << endl;
            getline(cin, cmd);
        }
        application.stopDB();
        application.stopAdmin();
        application.stopMonitor();

    } catch (const std::exception& ex) {
        spdlog::error("ERROR: {}", ex.what());
        return -1;
    }

    return 0;
}
