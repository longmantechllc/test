#pragma once

#include <memory>
#include <chrono>
#include <atomic>
#include <queue>
#include <vector>
#include <thread>

#include <libwebsockets.h>
#include <core/defs.h>
#include <comm/channel.h>

#include "event_queue.h"
#include "ws_engine.h"
#include "ws_connection.h"
#include "l2book_controller.h"

namespace Crypto {

class WsConnectionHandler {
protected:
    enum State {
        INIT = -1,
        CONNECTING,
        CONNECTED,
        AUTHENTICATING,
        SUBSCRIBING,
        ACTIVE,
    };

    enum Action {
        BEGIN,
        CONNECT_SUCCESS,
        AUTHENTICATE,
        AUTHENTICATE_SUCCESS,
        AUTHENTICATE_TIMEOUT,
        SUBSCRIBE,
        SUBSCRIBE_SUCCESS,
        SUBSCRIBE_TIMEOUT,
        RECV_TIMEOUT,
        RECV_ERROR,
    };
    
    std::queue<Action> input_queue;

    data_controller::on_update_t on_message_cb;
    data_controller::on_ready_t on_ready_cb;
    data_controller::on_error_t on_error_cb;
    data_controller::build_ping_msg_t build_ping_msg;
    data_controller::build_pong_msg_t build_pong_msg;
    data_controller::build_auth_msg_t build_auth_msg;
    data_controller::build_subscription_msg_t build_subscription_msg;

    std::shared_ptr<WsEngine> engine;
    std::shared_ptr<WsConnection> connection;
    std::vector<std::shared_ptr<WsConnection>> old_connections_;
    std::string host;
    std::int64_t port;
    std::string path;
    int interface;
    uint32_t ws_index_ = 0;

    milliseconds subscribe_timeout;
    milliseconds connect_timeout;
    milliseconds connect_retry_delay;
    milliseconds receive_timeout_ = 0ms;
    milliseconds ping_interval_ = 0ms;

    crypto::comm::timer* ping_timer_ptr_ = nullptr;

    uint64_t last_receive_data_ts_ = 0;
    uint64_t recv_count_ = 0;
    uint64_t last_recv_count_ = 0;
    crypto::exchanges::utils::l2book_controller* book_ctrl_ = nullptr;
    
    uint32_t connect_retry_count;    

    bool need_connect;
    bool need_subscribe;
    bool need_authenticate;
    bool no_authenticate_reply;
    bool no_subscribe_reply;

    State state = INIT;
    std::pair<milliseconds, time_point<system_clock>> backoff_time;

    void on_timeout(
        State current_state, milliseconds timeout, Action new_action
    );

    void on_ready();

    void update_state_machine(
        Action action, time_point<system_clock> tp = system_clock::now());

    void state_machine();

    void refresh();

    void update_backoff_time();

    void on_message(std::string_view);

    void on_ws_pong();

    void on_connect();

    void on_disconnect();

    void subscribe();

    void authenticate();

public:
    WsConnectionHandler(
        data_controller::on_update_t on_message_cb,
        data_controller::on_ready_t on_ready_cb,
        data_controller::on_error_t on_error_cb,
        data_controller::build_ping_msg_t build_ping_msg,
        data_controller::build_pong_msg_t build_pong_msg,
        data_controller::build_auth_msg_t build_auth_msg,
        data_controller::build_subscription_msg_t build_subscription_msg,
        crypto::exchanges::EventQueue &event_queue,
        std::shared_ptr<WsEngine> engine,
        std::string path,
        std::string host,
        int port,
        int interface,
        int ws_index,
        milliseconds subscribe_timeout = 1000ms,
        milliseconds connect_timeout = 100ms,
        milliseconds connect_retry_delay = 100ms,
        milliseconds receive_timeout = 10000ms,  // 10 seconds
        milliseconds ping_interval = 0ms,  // no auto ping by default
        int connect_retry_count = 3,
        bool need_connect = true,
        bool need_subscribe = true,
        bool need_authenticate = true,
        bool no_authenticate_reply = false,
        bool no_subscribe_reply = false);

    ~WsConnectionHandler();

    void send_message(const std::string &message);
    void disconnect();
    void reconnect();

    bool receive_data_timeout(uint64_t now_ts);
    void set_book_ctrl(crypto::exchanges::utils::l2book_controller* book_ctrl);
    void set_book_ctrl_v3(crypto::exchanges::utils::l2book_controller_v3* book_ctrl);
    uint64_t get_recv_count() { return recv_count_; }
    uint64_t get_last_recv_count() { return last_recv_count_; }
    bool is_active() { return this->state == State::ACTIVE; } 
    bool is_connected() { return this->state > State::CONNECTED; }
};

inline bool WsConnectionHandler::receive_data_timeout(uint64_t now_ts) {
    if (UNLIKELY(receive_timeout_.count() > 0 && last_receive_data_ts_ > 0 
        && now_ts > last_receive_data_ts_ + receive_timeout_.count() * 1000000)) {
        spdlog::error("<receive_timeout> [{}] detect receive_timeout", ws_index_);
        return true;
    }
    last_recv_count_ = recv_count_;
    return false;
}

}  // namespace 