//
// Created by schen on 10/26/2020.
//

#include <string>
#include <memory>
#include <iostream>
#include <unordered_map>
#include <functional>
#include <ctime>
#include <vector>

#include <xercesc/dom/DOM.hpp>

#include "auto_xerces_ptr.h"
#include "RTXUtils.h"
#include "Logger.h"

#include "LoaderBase.h"
#include "ConfigGlobalBox.h"
#include "ConfigGlobalLoader.h"
#include "ConfigRouterLoader.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigUserLoader.h"

#include "Config.h"
#include "ConfigLoader.h"

ConfigGlobalLoader::ConfigGlobalLoader()
{
}

ConfigGlobalLoader::ConfigGlobalLoader(Logger* logger)
    :LoaderBase(logger)
{
}

ConfigGlobalLoader::~ConfigGlobalLoader()
{
    globalFuncList.clear();
}

void ConfigGlobalLoader::InitGlobalConfigLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    auto  fptr = std::make_pair( "tradingHalts", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadTradingHalts(loader, node); } );
    globalFuncList.insert(fptr);

    auto  fptr1 = std::make_pair( "SecurityMaster", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadSecurityMaster(loader, node); } );
    globalFuncList.insert(fptr1);

    auto  fptr2 = std::make_pair( "Argos", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadArgos(loader, node); } );
    globalFuncList.insert(fptr2);

    auto  fptr3 = std::make_pair( "quoteFilters", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadQuoteFilters(loader, node); } );
    globalFuncList.insert(fptr3);

    auto  fptr4 = std::make_pair( "MarketDataFailureRoute", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadMarketDataFailureRoutes(loader, node); } );
    globalFuncList.insert(fptr4);

    auto  fptr5 = std::make_pair( "CODS", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadCustomTifs(loader, node); } );
    globalFuncList.insert(fptr5);

    auto  fptr6 = std::make_pair( "SecIdfnIdentifier", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadSecIdfnIdentifier(loader, node); } );
    globalFuncList.insert(fptr6);

    auto  fptr7 = std::make_pair( "orderParam", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadOrderParam(loader, node); } );
    globalFuncList.insert(fptr7);

    auto  fptr8 = std::make_pair( "SyntheticSecIdfnIdentifier", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadSyntheticSecIdfnIdentifier(loader, node); } );
    globalFuncList.insert(fptr8);

    auto  fptr9 = std::make_pair( "RetailRouter", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadRetailRouter(loader, node); } );
    globalFuncList.insert(fptr9);

    auto  fptr10 = std::make_pair( "Log", [&](ConfigLoader* loader, xercesc_3_2::DOMNode* node){ LoadLog(loader, node); } );
    globalFuncList.insert(fptr10);
}

void ConfigGlobalLoader::ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    InitGlobalConfigLoader(loader, node);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(j) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
         if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
         {
             xercesc_3_2::XMLString::release(&nodeName) ;
             continue;
         }
        char*  nodeText = xercesc_3_2::XMLString::transcode(subNode->getTextContent()) ;
        if(loader->m_config->GlobalConfig()->FuncList.find(nodeName) != loader->m_config->GlobalConfig()->FuncList.end() )
        {
            loader->m_config->GlobalConfig()->FuncList[nodeName](nodeText);
        }
        else if(globalFuncList.find(nodeName) != globalFuncList.end())
        {
            globalFuncList[nodeName](loader,subNode);
        }
        xercesc_3_2::XMLString::release(&nodeText) ;
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
    std::cout << "current time (on server): " << RTXUtils::NowSsm() << std::endl;
    std::string timeZone = loader->m_config->GlobalConfig()->TimeZone();
    if(!timeZone.empty())
    {
        tzset();
        setenv("TZ", loader->m_config->GlobalConfig()->TimeZone().c_str(), 1);
        std::cout << "current time (local): " << RTXUtils::NowSsm() << std::endl;
    }
//    PrintGlobal(loader);      //for debug
}

void ConfigGlobalLoader::LoadTradingHalts(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string enabled = GetElement(node, "enabled") ;
    loader->m_config->GlobalConfig()->tradingHalts->enabled = RTXUtils::StrToBool(enabled, false);
}

void ConfigGlobalLoader::LoadSecurityMaster(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string path = GetElement(node, "path") ;
    loader->m_config->GlobalConfig()->SecurityMaster()->path = path;
}

void ConfigGlobalLoader::LoadArgos(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;
        char* nodeText = xercesc_3_2::XMLString::transcode(subNode->getTextContent()) ;
        std::string txt = nodeText;
        loader->m_config->GlobalConfig()->AddArgo(nodeName, txt, false);

        xercesc_3_2::XMLString::release(&nodeText) ;
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
//    loader->m_config->GlobalConfig()->SetArgo();
}

void ConfigGlobalLoader::LoadQuoteFilters(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        auto obj = std::make_shared<QuoteFilter>();
        std::string mdid = GetElement(subNode, "mdid") ;
        std::string premarket = GetElement(subNode, "premarket") ;
        obj->premarket = RTXUtils::StrToBool(premarket, true);
        std::string postmarket = GetElement(subNode, "postmarket") ;
        obj->postmarket = RTXUtils::StrToBool(postmarket, true);

        loader->m_config->GlobalConfig()->AddQuoteFilter(mdid, obj);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadMarketDataFailureRoutes(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        std::string name = GetElement(subNode, "name") ;
        loader->m_config->GlobalConfig()->AddMarketDataFailureRoute(name);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadCustomTifs(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        auto obj = std::make_shared<COD>();
        std::string value = GetElement(subNode, "value") ;
        std::string cxlTimer = GetElement(subNode, "cxlTimer") ;

        obj->cxlTimer = RTXUtils::StrToInt(cxlTimer, false);
        loader->m_config->GlobalConfig()->AddCustomTif(value, obj);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadSecIdfnIdentifier(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;
        std::string value = GetElement(subNode, "value") ;
        std::string SecIdfn = GetElement(subNode, "SecIdfn") ;

        loader->m_config->GlobalConfig()->AddSecIdfnIdentifier(value, SecIdfn);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadSyntheticSecIdfnIdentifier(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    loader->m_config->GlobalConfig()->minQtySecIdfnIdentifier = "";
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;
        std::string value = GetElement(subNode, "value") ;
        std::string SecIdfn = GetElement(subNode, "SecIdfn") ;

        loader->m_config->GlobalConfig()->AddSyntheticSecIdfnIdentifier(nodeName, value, SecIdfn);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadOrderParam(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        std::string value = GetElement(subNode, "value") ;
        std::string action = GetElement(subNode, "action") ;
        std::string venue = GetElement(subNode, "venue") ;

        loader->m_config->GlobalConfig()->AddOrderParam(nodeName, value, action, venue);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadRetailRouter(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(i) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
            continue;

        if(strcmp(nodeName, "ContraQuotefile") == 0)
        {
            std::string path = GetElement(subNode, "path") ;
            std::string ioiNormalizerCacheStr = GetElement(subNode, "IoiNormalizerCache") ;
            loader->m_config->GlobalConfig()->retailRouterContraQuotefilePath = path;
            loader->m_config->GlobalConfig()->retailRouterIoiNormalizerCache = RTXUtils::StrToBool(ioiNormalizerCacheStr, false);
        }
        else if(strcmp(nodeName, "MMIDmapping") == 0)
        {
            LoadRetailRouterMmid(loader, subNode);
        }
        else
        {
            LoadRetailRouterMMIDpIpfoF(loader, subNode);
        }
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}
void ConfigGlobalLoader::LoadRetailRouterMmid(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(i);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
            continue;

        std::string mmid = GetElement(subNode, "mmid") ;
        std::string route = GetElement(subNode, "route") ;
        auto pr = std::make_pair(mmid, route);
        loader->m_config->GlobalConfig()->retailRouterMMIDmap.insert(pr);

        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadRetailRouterMMIDpIpfoF(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();

    for(int i=0; i!=nodeList1->getLength(); i++)
    {
        xercesc_3_2::DOMNode *subNode = nodeList1->item(i);
        char *nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName());
        if (strcmp(nodeName, "#text") == 0 || strcmp(nodeName, "#comment") == 0)
            continue;

        std::string mmid = GetElement(subNode, "mmid");
        std::string pIStr = GetElement(subNode, "pI");
        std::string pfoFStr = GetElement(subNode, "pfoF");
        std::string priorityStr = GetElement(subNode, "priority");
        Price pi = RTXUtils::StrToPrice(pIStr, 0);
        Price pfoF = RTXUtils::StrToPrice(pfoFStr, 0);
        int priority = RTXUtils::StrToInt(priorityStr, 0);

        auto str = std::make_shared<RetailRouterMMIDpIpfoF>(mmid, pi, pfoF, priority);
        auto pr = std::make_pair(mmid, str);
        loader->m_config->GlobalConfig()->retailRouterMMIDpIpfoFmap.insert(pr);

        xercesc_3_2::XMLString::release(&nodeName) ;
    }
}

void ConfigGlobalLoader::LoadLog(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    std::string asyncStr = GetElement(node, "async") ;
    loader->m_config->GlobalConfig()->logAsync = RTXUtils::StrToBool(asyncStr, true);
}

void ConfigGlobalLoader::PrintGlobal(ConfigLoader* loader)
{
    std::cout << std::endl << "Print Global" << std::endl;

    std::cout << "RouteName: -- " <<  loader->m_config->GlobalConfig()->RouteName() << std::endl;
    std::cout << "TimeZone: -- " <<  loader->m_config->GlobalConfig()->TimeZone() << std::endl;
    std::cout << "DefaultSecIdfn: -- " <<  loader->m_config->GlobalConfig()->DefaultSecIdfn() << std::endl;
    std::cout << "RetryOnCxlRej: -- " <<  loader->m_config->GlobalConfig()->RetryOnCxlRej() << std::endl;
    std::cout << "MarketDataFailureTimeOutSwp: -- " <<  loader->m_config->GlobalConfig()->MarketDataFailureTimeOutSwp() << std::endl;
    std::cout << "MarketDataFailureTimeOutTarg: -- " <<  loader->m_config->GlobalConfig()->MarketDataFailureTimeOutTarg() << std::endl;
    std::cout << "ShortSellTargetingReTries: -- " <<  loader->m_config->GlobalConfig()->ShortSellTargetingReTries() << std::endl;
    std::cout << "CheckChildOrdLimitPriceViolation: -- "<< boolalpha  <<  loader->m_config->GlobalConfig()->CheckChildOrdLimitPriceViolation() << std::endl;

    std::cout << "MarketOpen: -- "<< loader->m_config->GlobalConfig()->MarketOpenStr() << ", MarketOpenTime:" << loader->m_config->GlobalConfig()->MarketOpenTime() << std::endl;
    std::cout << "MarketClose: -- "<< loader->m_config->GlobalConfig()->MarketCloseStr() << ", MarketCloseTime:" << loader->m_config->GlobalConfig()->MarketCloseTime() << std::endl;
    std::cout << "PostMarketClose: -- "<< loader->m_config->GlobalConfig()->PostMarketCloseStr() << ", PostMarketCloseTime:" << loader->m_config->GlobalConfig()->PostMarketCloseTime() << std::endl;
    std::cout << "RetailSWPStartTimeStr: -- "<< loader->m_config->GlobalConfig()->RetailSWPStartTimeStr() << ", SWPStartTime:" << loader->m_config->GlobalConfig()->RetailSWPStartTime() << std::endl;
    std::cout << "RetailSWPEndTimeStr: -- "<< loader->m_config->GlobalConfig()->RetailSWPEndTimeStr() << ", SWPEndTime:" << loader->m_config->GlobalConfig()->RetailSWPEndTime() << std::endl;
    std::cout << "RetailRoutingStartTime: -- "<< loader->m_config->GlobalConfig()->RetailRoutingStartTimeStr() << ", RetailRoutingStartTime:" << loader->m_config->GlobalConfig()->RetailRoutingStartTime() << std::endl;
    std::cout << "RetailRoutingEndTime: -- "<< loader->m_config->GlobalConfig()->RetailRoutingEndTimeStr() << ", RetailRoutingEndTime:" << loader->m_config->GlobalConfig()->RetailRoutingEndTime() << std::endl;
    std::cout << "SWPStartTimeStr: -- "<< loader->m_config->GlobalConfig()->SWPStartTimeStr() << ", SWPStartTime:" << loader->m_config->GlobalConfig()->SWPStartTime() << std::endl;
    std::cout << "SWPEndTimeStr: -- "<< loader->m_config->GlobalConfig()->SWPEndTimeStr() << ", SWPEndTime:" << loader->m_config->GlobalConfig()->SWPEndTime() << std::endl;

    std::cout << "ExtraLogging: -- " << boolalpha << loader->m_config->GlobalConfig()->ExtraLogging() << std::endl;

    std::cout << "TradingHalts: -- " << boolalpha << loader->m_config->GlobalConfig()->TradingHalts()->enabled << std::endl;
    std::cout << "SecurityMaster: -- " << loader->m_config->GlobalConfig()->SecurityMaster()->path << std::endl;
    for(auto ar : loader->m_config->GlobalConfig()->Argos() )
        std::cout << "Argos:name: " << ar.first << ", val:" << ar.second << std::endl;
    for(auto ar : loader->m_config->GlobalConfig()->argos)
        std::cout << "Valid argos:name: " << ar->name << ", val:" << boolalpha << ar->enabled << std::endl;
    for(auto qf : loader->m_config->GlobalConfig()->QuoteFilters() )
        std::cout << "QuoteFilters:name: " << qf.first << ", pre:" << qf.second->premarket << ", post:" << qf.second->postmarket << std::endl;
    for(auto mr : loader->m_config->GlobalConfig()->MarketDataFailureRoutes() )
        std::cout << "MarketDataFailureRoute:name : " << mr << std::endl;
    for(auto ob : loader->m_config->GlobalConfig()->CustomTif() )
        std::cout << "CustomTif:value: " << ob.first << ", cxlTimer:" << ob.second->cxlTimer << std::endl;
    for(auto id : loader->m_config->GlobalConfig()->SecIdfnIdentifier() )
        std::cout << "SecIdfnIdentifier:value: " << id.first << ", exchange:" << id.second << std::endl;
    for(auto id : loader->m_config->GlobalConfig()->symbolSecIdfnIdentifier )
        std::cout << "symbolSecIdfnIdentifier:symbol: " << id.first << ", exchange:" << id.second << std::endl;
    for(auto id : loader->m_config->GlobalConfig()->userSecIdfnIdentifier )
        std::cout << "userSecIdfnIdentifier:user: " << id.first << ", exchange:" << id.second << std::endl;
    for(auto id : loader->m_config->GlobalConfig()->accountSecIdfnIdentifier )
        std::cout << "accountSecIdfnIdentifier:account: " << id.first << ", exchange:" << id.second << std::endl;
    std::cout << "minQtySecIdfnIdentifier, exchange:" << loader->m_config->GlobalConfig()->minQtySecIdfnIdentifier << std::endl;

    for(auto& tp : loader->m_config->GlobalConfig()->orderTypeActionList ) {
        if(tp.second->action == "reject")
            std::cout << "orderParam: orderType, value: " << tp.first << ", action:" << tp.second->action << std::endl;
        else {
            for (auto& dest : tp.second->destinationList)
                std::cout << "orderParam: orderType, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
    }
    for(auto& tp : loader->m_config->GlobalConfig()->orderSymbolActionList ) {
        if(tp.second->action == "reject")
            std::cout << "orderParam: symbol, value: " << tp.first << ", action:" << tp.second->action << std::endl;
        else {
            for (auto& dest : tp.second->destinationList)
                std::cout << "orderParam: symbol, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
    }
    for(auto& tp : loader->m_config->GlobalConfig()->orderTifActionList ) {
        if(tp.second->action == "reject")
            std::cout << "orderParam: Tif, value: " << tp.first << ", action:" << tp.second->action << std::endl;
        else {
            for (auto& dest : tp.second->destinationList)
                std::cout << "orderParam: Tif, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
    }
    for(auto& tp : loader->m_config->GlobalConfig()->userActionList ) {
        if(tp.second->action == "reject")
            std::cout << "orderParam: user, value: " << tp.first << ", action:" << tp.second->action << std::endl;
        else {
            for (auto& dest : tp.second->destinationList)
                std::cout << "orderParam: user, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
    }
    for(auto& tp : loader->m_config->GlobalConfig()->accountActionList ) {
        if(tp.second->action == "reject")
            std::cout << "orderParam: account, value: " << tp.first << ", action:" << tp.second->action << std::endl;
        else {
            for (auto& dest : tp.second->destinationList)
                std::cout << "orderParam: account, value: " << tp.first << ", action:" << tp.second->action
                          << ", destination:" << dest << std::endl;
        }
    }
    std::cout << "retailRouterContraQuotefile Path: " <<  loader->m_config->GlobalConfig()->retailRouterContraQuotefilePath << std::endl;
    std::cout << "retailRouterIoiNormalizerCache: " << boolalpha << loader->m_config->GlobalConfig()->retailRouterIoiNormalizerCache << std::endl;
    for(auto& id : loader->m_config->GlobalConfig()->retailRouterMMIDmap )
        std::cout << "retailRouterMDIDmap: mmid:" << id.first << ", route:" << id.second << std::endl;
    for(auto& id : loader->m_config->GlobalConfig()->retailRouterMMIDpIpfoFmap )
        std::cout << "retailRouterMMIDpIpfoFmap: mmid:" << id.first << ", pI:" << id.second->pI.ToDouble() << ", pfoF:" << id.second->pfoF.ToDouble() << ", priority:" << id.second->priority << std::endl;

    std::cout << "Log Async: " << boolalpha <<  loader->m_config->GlobalConfig()->logAsync << std::endl;

//    for(auto tp : loader->m_config->GlobalConfig()->OrderParam() )
//        for(auto act : tp.second )
//            for(auto dest : act.second->destinationList)
//                std::cout << "Type: " << tp.first  << ", Type:value: " << act.first << ", action:" << act.second->action
//                      << ", destination:" << dest << std::endl;
    std::cout << std::endl;
}