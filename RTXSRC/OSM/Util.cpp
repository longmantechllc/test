#include "spdlog/spdlog.h"
#include "Util.h"

namespace OSM {

    void printER(const char* msg, const ExecutionReport &er) {
        spdlog::info("{} OID={}, COID={}, execID={}, Account={}, subAccount={}, Login={}, "
                     "Exch={}, execType={}, Status={}, Sym={}, Side={}, Size={}, Type={}, "
                     "TIF={}, FillPrice={}, FillSize={}, Leave={}, AvgPrice={}, "
                     "CumQty={}, transactTime={}",
                     msg, er.orderId_, er.childOrderId_, er.execId_, er.accountId_,
                     er.subAccount_, er.logInId_, er.exchange_, er.execType_,
                     er.status_, er.symbol_,  er.side_, er.size_, er.type_, er.TIF_,
                     er.fillPrice_.ToDouble(),  er.fillSize_, er.leaveQty, er.avgPrice_.ToDouble(),
                     er.cumulativeFillQuantity_, er.transactTime_);
    }


    void printOrder(const char* msg, const Order& ord)
    {
        spdlog::info("{} OID={}, COID={}, CltOID={}, Sym={}, Side={}, Size={}, Type={}, TIF={}, "
                     "Disp={}, Exch={}, Discretion={}, ExpTime={}, ExecInst={}, MinQty={}",
                     msg, ord.orderID_, ord.childOrderId_, ord.cltOrderId_, ord.symbol_,
                     ord.side_, ord.size_, ord.type_, ord.TIF_, ord.displaySize_, ord.exchange_,
                     ord.discretion_.ToDouble(), ord.expireTime_, ord.execInst_, ord.minQty_);
    }

    void printOrder(const char* msg, const OSMOrder& ord)
    {
        spdlog::info("{} OID={}, refOrderId={}, childOrderId={}, COID={}, exOrderId={}, "
                     "Sym={}, Side={}, Qty={}, Type={}, Status={}, TIF={}, transactTime={}",
                     msg, ord.pOrderId, ord.refOrderId, ord.childOrderId, ord.clOrdId,
                     ord.exOrderId, ord.symbol, ord.side , ord.qty, ord.type,
                     ord.status, ord.tif, ord.transactTime);
    }
}

