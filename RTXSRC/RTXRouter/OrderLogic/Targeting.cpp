//
// Created by schen on 10/6/2020.
//
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "RTXUtils.h"
#include "Logger.h"
#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "CPU.h"

#include "_DataStruct/TOBQuote.h"
#include "RTXOrder.h"
#include "IOrderLogicBase.h"

#include "Targeting.h"


Targeting::Targeting(RTXOrder* order)
        : IOrderLogicBase { order }
{
}

Targeting::~Targeting()
{
}

bool Targeting::InitOrder()
{
    if(m_isInited == true)
        return true;

    m_mmidRouteMapping = m_order->m_exchange->targeting->MDIDmapping;
    m_isInited = true;
    return true;
}

bool Targeting::ProcessOrder()
{
    if (IsEligible() == false)
        return false;
    if (IsMarketable() == false) {
        if(m_order->m_clientOrder->TIF_ == TimeInForce::TIF_IOC)
            m_order->CancelParent("IOC order cannot find inside quote.");
        return false;
    }

    while(true) {
        int qtyRemain = m_order->QtyUncommited();
        std::pair<std::string, int> mmidForConsumedQty;
        std::vector<std::string> toRemovedQuoteMmid;
        std::vector<std::shared_ptr<Order>> targetOrderList;
        auto targetQuoteList = m_order->m_tobList.GetBestPriceQuoteList();
        Price bestPx = m_order->m_tobList.GetBestQuotePrice();
        if(targetQuoteList.empty() || bestPx < Price(0.0)
        || (m_order->m_clientOrder->type_ == OrderType::TYPE_LIMIT && !m_order->IsMarketable(m_order->EffectivePrice(), bestPx)))
        {
            m_order->m_isMarketable = false;
            return false;
        }
        m_order->PrintTob(targetQuoteList);

        auto quote = targetQuoteList.begin();
        while (quote != targetQuoteList.end()) {
            std::string routeName;
            std::string mpid = (*quote)->m_mpid;
            auto mmidRoute = m_mmidRouteMapping.find(mpid);
            if (mmidRoute != m_mmidRouteMapping.end()) {
                routeName = mmidRoute->second;
            }

            if (!IsRouteEligible(routeName)) {
                quote = targetQuoteList.erase(quote);
                m_order->m_logger->logInfo("oid:%d, Route:%s is not eligible at this time.", m_order->m_clientOrder->orderID_, routeName.c_str());
                m_mmidToRemoveList.push_back(mpid);
                continue;
            }
            if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
            {
                int sz = m_order->m_tobList.GetTobList().size();
                if(sz == 1 && mpid == m_order->m_exchange->listingExchange)
                {
                    m_order->m_isMarketable = false;
                    return false;
                }
            }
            int qQty = 0;
            if (m_order->m_exchange->targeting->sequentialTargeting == true)
                qQty = qtyRemain;
            else
                qQty = (*quote)->m_afterConsumedQty;
            if (qQty >= qtyRemain) {
                qQty = qtyRemain;
                m_mmidQtyToConsumed.second = qQty;
                m_mmidQtyToConsumed.first = mpid;
                qtyRemain = 0;
            }
            if (qtyRemain > 0)
                m_mmidToRemoveList.push_back(mpid);

            auto route = m_order->m_config->RouterConfig()->Route(routeName);
            auto cOrder = m_order->CreateChildOrder();
            cOrder->size_ = qQty;
            cOrder->type_ = OrderType::TYPE_LIMIT;
            cOrder->TIF_ = route->tif;
            cOrder->price_ = bestPx;
            Price offset = route->aggressiveOffset;
            if(offset > Price(0.0)) {
                if (m_order->IsBuyOrder()) {
                    cOrder->price_ = bestPx + offset;
                    if ((cOrder->price_).isGE(m_order->EffectivePrice()))
                        cOrder->price_ = m_order->EffectivePrice();
                } else {
                    cOrder->price_ = bestPx - offset;
                    if ((cOrder->price_).isLE(m_order->EffectivePrice()))
                        cOrder->price_ = m_order->EffectivePrice();
                }
            }
            cOrder->exchange_ = route->venue;
            cOrder->routeName_ = routeName;
            targetOrderList.push_back(cOrder);

            if (qtyRemain > 0)
                qtyRemain -= qQty;
            if (qtyRemain <= 0)
                break;

            ++quote;
        }
        if(targetOrderList.empty())
        {
            m_order->m_tobList.RemoveQuoteByMmidList(m_mmidToRemoveList);

            Price bestPx = m_order->m_tobList.GetBestQuotePrice();
            if(bestPx < Price(0.0) || (m_order->m_clientOrder->type_ == OrderType::TYPE_LIMIT && !m_order->IsMarketable(m_order->EffectivePrice(), bestPx)))
            {
                m_order->m_isMarketable = false;
                return false;
            }
            continue;
        }
        int cnt = 0;
        int qtyRemainTp = qtyRemain;
        int cOrderSize = targetOrderList.size();
        auto orderItr = targetOrderList.begin();
        if (qtyRemain > 0) {
            while (orderItr != targetOrderList.end()) {
                ++cnt;
                int qty = qtyRemain / cOrderSize;
                qty = RTXUtils::RoundToLotSize(qty, m_order->m_lotsize);
                if (qty >= qtyRemainTp || cnt == cOrderSize) {
                    qty = qtyRemainTp;
                    qtyRemainTp = 0;
                }
                (*orderItr)->size_ += qty;
                qtyRemainTp -= qty;
                if (qtyRemainTp <= 0)
                    break;
                ++orderItr;
            }
        }
        orderItr = targetOrderList.begin();
        while (orderItr != targetOrderList.end()) {
            SendChildOrder(*orderItr);
            ++orderItr;
        }
        m_order->m_tobList.RemoveQuoteByMmidList(m_mmidToRemoveList);
        if (m_mmidQtyToConsumed.second > 0) {
            m_order->m_tobList.SetQuoteConsumedByMmid(m_mmidQtyToConsumed.first, m_mmidQtyToConsumed.second);
            m_mmidQtyToConsumed.second = 0;
        }
        break;
    }
    return false;
}

bool Targeting::SendChildOrder(std::shared_ptr<Order> order)
{
    m_order->SendChildOrder(order, ChildOrderType::CHILD_ORDER_TYPE_TARGETING);
    return false;
}

bool Targeting::ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if(child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_TARGETING)
        return false;

    return false;
}


bool Targeting::ProcessMktData()
{
    return true;
}

bool Targeting::IsRouteEligible(const std::string& routeName)
{
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!route)
        return false;
    if(m_order->ExcludedRoute(routeName) == true)
        return false;
    if(route->InOperationTime(RTXUtils::NowSsm()) == false)
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(route->venue))
        return false;

    return true;
}

bool Targeting::IsEligible()
{
    if(m_order->m_isCleaning || m_order->m_orderIsCancelling || m_order->m_orderIsDone)
        return false;
    if(m_order->QtyUncommited() <= 0)
        return false;
    if(m_order->m_isRetailRouting == true)
        return false;
    if(m_order->m_isSWPDone == false)
        return false;

    return true;
}

bool Targeting::IsRouteMarketable(const std::string& routeName)
{
    return true;
}

bool Targeting::IsMarketable()
{

    if(m_order->m_tobList.GetTobList().empty())
    {
        m_order->m_isMarketable = false;
        return false;
    }
    if(m_order->m_clientOrder->type_ == OrderType::TYPE_LIMIT )
    {
        Price px = m_order->m_tobList.GetBestQuotePrice();
        if(!m_order->IsMarketable(m_order->EffectivePrice(), px))
        {
            m_order->m_isMarketable = false;
            return false;
        }
    }
    else if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
    {
        int now = RTXUtils::NowSsm();
//        if(m_order->m_validTob == false || m_order->m_validTrade == false || (m_order->m_cpu->GetMarketSession() == MarketSession::Pre_Market))
        if(m_order->m_validTob == false || now < m_order->m_marketStartTimeSsm)
        {
            m_order->m_isMarketable = false;
            return false;
        }
    }
    m_order->m_isMarketable = true;
    return true;
}
