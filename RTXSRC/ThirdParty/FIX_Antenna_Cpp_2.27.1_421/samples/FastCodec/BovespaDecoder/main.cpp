// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <limits>
#include <sys/timeb.h>
#include <stdexcept>

#ifdef _LINUX
#	include <arpa/inet.h>
#endif

#include <B2BITS_FastCodec.h>
#include <B2BITS_Exception.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_SystemDefines.h>


class ChunkSet // accumulates chunks to construct full fast message
{
public:

    ChunkSet()
        : lastUdp_(0), chunksCount_(0)
    {}

    // chunksCount - number of chunks to construct full fast message
    // lastUdp - number of last udp packet that contains chunk. Used like timpstamp for removing obsolete chunkSets
    explicit ChunkSet(System::u16 chunksCount, System::u64 lastUdp=0)
        :  chunksCount_(chunksCount),lastUdp_(lastUdp)
    {}

    void setLastUdp(System::u64 lastUdp)
    {
        lastUdp_= lastUdp;
    }

    System::u64 getLastUdp()
    {
        return lastUdp_;
    }

    void addChunk(const char* start, System::u16 length, System::u16 chunkNumber )
    {
        if(chunkNumber<1 ||chunksCount_<chunkNumber)
            throw std::runtime_error("Can't create chunk: out of range");

        if(chunks_.count(chunkNumber) == 1)
            throw std::runtime_error("Chunks overwrite is not allowed");
            
        chunks_[chunkNumber].assign(start, length);
    }

    void clear()
    {
        chunks_.clear();
        data_.clear();
    }

    bool empty()
    {
        return chunks_.empty();
    }

    bool ready()
    {
        return chunks_.size() == chunksCount_;
    }

    const char* data()
    {
        prepareData();
        return &data_[0];
    }

    size_t size()
    {
        prepareData();
        return data_.size();
    }

private:
    void prepareData()
    {
        if(data_.empty())
        {
            if(!ready())
            {
                throw std::logic_error("Unable to complete message from read chunks");
            }

            std::string tmp;
            for(std::map <int, std::string>::iterator it=chunks_.begin(); it!=chunks_.end(); ++it)
            {
                tmp += it->second;
            }
            data_.assign( tmp.begin(), tmp.end() );
        }
    }

    System::u64 lastUdp_;
    System::u16 chunksCount_;

    std::map <int, std::string> chunks_;
    std::vector<char> data_;
};


int main( int argc, char const* const argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if ( argc != 2 ) {
        std::cerr << "Usage: <bin_file>" << std::endl;
        return 1;
    }

    try {
        // Initialize FastCodec
        Engine::FastCodec fastCodec( "../../../../engine.license" );

        // Create FastDecoder
        std::auto_ptr<Engine::FastDecoder> fastDecoder(
            fastCodec.createFastDecoder(
                "../../../../data/fixdict11.xml|../../../../data/fixdic50sp2.xml|fixdic50sp2bvmf_puma.xml",
                "templates-PUMA.xml",
                "FIXT11",
                "FIX50SP2BVMF",
                Engine::FastMappingOptions::getBovespaOptions()) );

        // Open the binary file to read messages
        std::ifstream file( argv[1], std::ios::binary );

        if ( !file ) {
            throw Utils::Exception( "cannot open file: " + std::string( argv[1] ) );
        }

        // Get the file size
        file.seekg( 0, std::ios::end );
        std::ifstream::pos_type size = file.tellg();
        file.seekg( 0, std::ios::beg );

        // Read the file to the vector of char
        std::vector<char> data( ( unsigned int )size );
        std::vector<char>::iterator dataStart = data.begin();
        const std::vector<char>::const_iterator dataEnd = data.end();

        file.read( &*dataStart, size );

        System::u64 nUdp=0;
        const int maxGap = 10; // Number of upd packets before cleaning obsolete chunks
        std::map <System::u32, ChunkSet> chunksMap; //Chache of uncompleted ChunksSets
        std::map <System::u64, System::u32> udpToSeqNum; // Index to chunksMap ordered by last udp

        while ( dataStart != dataEnd ) {
            nUdp++;

            // Udp preambula (10 bytes)
            if(dataStart + 10 >= dataEnd)
                throw std::runtime_error("Not enough length");

            dataStart+=8;
            System::u16 blockSize = htons(*reinterpret_cast<System::u16 *>(&*dataStart));
            dataStart +=sizeof(blockSize);

            std::vector<char>::iterator dataStop = dataStart + blockSize;
            
            if(dataStop > dataEnd)
                throw std::runtime_error("Not enough data!");

            while(dataStart != dataStop)
            {
                //extract data from Bovespa Technical Message Header
                System::u32 seqNum = htonl(*reinterpret_cast<System::u32 *>(&*dataStart));
                dataStart +=sizeof(seqNum);
                System::u16 chunkCounts = htons(*reinterpret_cast<System::u16 *>(&*dataStart));
                dataStart +=sizeof(chunkCounts);
                System::u16 chunkNumber = htons(*reinterpret_cast<System::u16 *>(&*dataStart));
                dataStart +=sizeof(chunkNumber);
                System::u16 chunkLength = htons(*reinterpret_cast<System::u16 *>(&*dataStart));
                dataStart +=sizeof(chunkLength);

                // Get or create Chunkset. Update index.
                ChunkSet& fastmsg=chunksMap[seqNum];
                if(fastmsg.empty())
                    fastmsg = ChunkSet(chunkCounts, nUdp);
                else
                {
                    udpToSeqNum.erase( fastmsg.getLastUdp() );
                    fastmsg.setLastUdp(nUdp);
                }
                udpToSeqNum[ fastmsg.getLastUdp() ] = seqNum;

                // Add chunk to ChunckSet
                if( (dataStart+chunkLength) <= dataStop)
                    fastmsg.addChunk(&*dataStart, chunkLength, chunkNumber);
                else
                    throw std::runtime_error("Not enough data!");

                // Create and decode fast message if there is enough data.
                if(fastmsg.ready())
                {
                    // Reset the dictionary before decoding
                    fastDecoder->resetDictionary();
                    std::size_t decoded( 0 );
                    try {
                        std::auto_ptr<Engine::FIXMessage> msg( fastDecoder->decode( fastmsg.data(), fastmsg.size(), &decoded ) );
                        std::cout << *msg->toString( '|' ) << std::endl;
                    } catch (Utils::Exception& e) {
                        std::cerr << "Error: " << e.what() << std::endl;
                    }
                    // Remove processed ChunkSet from map
                    udpToSeqNum.erase( fastmsg.getLastUdp() );
                    chunksMap.erase(seqNum);
                }

                // Remove obsolete ChunkSets
                if(!udpToSeqNum.empty())
                {
                    if(nUdp - udpToSeqNum.begin()->first > maxGap)
                    {
                        chunksMap.erase(udpToSeqNum.begin()->second);
                        udpToSeqNum.erase(udpToSeqNum.begin());
                    }
                }

                dataStart += chunkLength;
            }

        }
    } catch ( Utils::Exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 1;
    } catch ( std::exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 2;
    } catch ( ... ) {
        std::cerr << "Unknown error." << std::endl;
        return 3;
    }

    return 0;
}
