// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MDApplication.h
/// Contains Globex::MDApplication class definition

#ifndef B2BITS_GLOBEX_Application_h__
#define B2BITS_GLOBEX_Application_h__

#include <vector>

#include <B2BITS_MDTypes.h>
#include <B2BITS_V12_Defines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_MDThreadManager.h>
#include <B2BITS_CompilerDefines.h>

namespace Engine
{
    class FIXMessage;
    class TagValue;
}

/// CME FAST/FIX Globex namespace
namespace Globex
{
    class MDApplication;
    class SecurityDefinitionListener;
    class InstrumentListener;

    /// Represents CME channel connection
    struct ChannelConnection {
        ChannelConnection( std::string const& channel_id, std::string const& connection_id )
            : channelID_( channel_id )
            , connectionID_( connection_id ) {
        }

        explicit ChannelConnection( std::string const& connection_id )
            : channelID_()
            , connectionID_( connection_id ) {
        }

        ChannelConnection() {
        }

        /// ID of the channel
        std::string channelID_;

        /// ID of the connection
        ///@note can't be empty
        std::string connectionID_;
    };

    /// Represents TCPReplay params
    struct TCPReplayParams {
        typedef std::vector<std::string> ChannelIdList;

        TCPReplayParams()
            : channels_()
            , username_()
            , password_() {
        }

        /// List of channel id, which will be using the TCPReplay recovery model
        ChannelIdList channels_;

        /// Username for authorization on TCPReplay connection
        std::string username_;

        /// Password for authorization on TCPReplay connection
        std::string password_;
    };

    /// Startup parameters
    struct MDApplicationParams {
        /// Type of transport layer
        enum Transport {
            Sockets,    ///< standard IP sockets
            MyricomDBL,  ///< Myricom datagram kernel bypass Layer
            MyricomSNF  ///< Myricom sniffer kernel bypass Layer
        };

        MDApplicationParams()
            : templatesFn_()
            , configXml_()
            , config20Xml_()
            , numberOfWorkers_( 4 )
            , incrementQueueSize_( 50 )
            , checkUDPSender_( false )
            , listenInterfaceIP_()
            , activeConnections_()
            , activeConnections20_()
            , incomingUdpBufferSize_( 0 )
            , logIncomingMessages_( false )
            , logIncomingUdpMessages_( false )
            , transportType_( Sockets )
            , detectionHolePackDelay_( 1 )
            , useMsgSeqNumForGapDetection_( false )
            , incrementalProcessorWorkerCount_( 0 )
            , incrementalProcessorInterfaceIP_()
            , incrementalProcessorTransportType_( Sockets )
            , experimentalLogic_( false )
            , feedAListenInterfaceIP_()
            , feedBListenInterfaceIP_()
#ifdef _WIN32
            , bindToInterface_( true )
#else
            , bindToInterface_( false )
#endif
        {
        }

        /// Path to the CME FAST templates file.
        /// @note Should be downloaded from ftp://ftp.cmegroup.com
        std::string templatesFn_;

        /// Path to the CME configuration file
        /// @note Should be downloaded from ftp://ftp.cmegroup.com
        std::string configXml_;

        /// Path to the CME FIX/FAST 2.0 configuration file
        /// @note Should be downloaded from ftp://ftp.cmegroup.com
        std::string config20Xml_;

        /// Number of threads to decode incoming data
        /// Default value is 4
        size_t numberOfWorkers_;

        /// Maximum number of messages could be stored in recovery mode for the particular instrument.
        /// Default value is 50
        size_t incrementQueueSize_;

        /// Pass true to check the UDP packet sender's IP address.
        /// Default value is true
        bool checkUDPSender_;

        /// IP of network interface to listen on; nullptr or empty std::string means all interfaces.
        /// Default value is null (all interfaces)
        std::string listenInterfaceIP_;

        typedef std::vector<ChannelConnection> ActiveConnections;
        /// List of channel connection to use; empty means all connections are allowed.
        /// Default value is empty (all connections from configXml are used)
        ActiveConnections activeConnections_;

        /// List of channel connection to use; empty means all connections are allowed.
        /// Default value is empty (all connections from configXml are used)
        ActiveConnections activeConnections20_;

        /// UDP incoming buffer size.
        /// Should be tuned in case of UDP message miss
        size_t incomingUdpBufferSize_;

        /// Pass true to write out to the log file incoming FIX messages
        /// Default value is false
        bool logIncomingMessages_;

        /// Pass true to write out to the binary log file incoming FAST messages
        /// Default value is false
        bool logIncomingUdpMessages_;

        /// Transport layer for data reading
        /// Default value is Transport::Sockets
        Transport transportType_;

        /// Number of incoming messages with seq num out of order to skip before start recovery.
        /// Default value is 1
        size_t detectionHolePackDelay_;

        /// Pass true to use tag 34 for detect gaps
        /// Default value is false
        bool useMsgSeqNumForGapDetection_;

        /// Number of separate threads to decode incoming "X" messages. (0 - disable separate processing of "X"
        /// Default value is 0
        size_t incrementalProcessorWorkerCount_;

        /// IP of network interface to listen on for separate "X" processing; nullptr or empty std::string means all interfaces.
        /// Default value is null (all interfaces)
        std::string incrementalProcessorInterfaceIP_;

        /// Transport layer for data reading by separated "X" threads
        /// Default value is Transport::Sockets
        Transport incrementalProcessorTransportType_;

        /// Params for TCPReplay recovery mode
        TCPReplayParams tcpReplayParams_;

        /// enable exerimental logic
        /// Default value is false
        bool experimentalLogic_;

        /// IP of network interface to listen for messages for Feed A; nullptr or empty std::string means all interfaces.
        /// Default value is null (all interfaces)
        /// @note This parameter cannot be used with parameters listenInterfaceIP_ or incrementalProcessorInterfaceIP_.
        /// @note This parameter is supported Sockets transport only.
        std::string feedAListenInterfaceIP_;

        /// IP of network interface to listen for messages for Feed B; nullptr or empty std::string means all interfaces.
        /// Default value is null (all interfaces)
        /// @note This parameter cannot be used with parameters listenInterfaceIP_ or incrementalProcessorInterfaceIP_.
        /// @note This parameter is supported Sockets transport only.
        std::string feedBListenInterfaceIP_;

        bool bindToInterface_;
    };

    /// Represents CME Globex application listener
    /// @note Objects of this class do not put to the std::auto_ptr or other smart pointers
    /// (except specialized, example Utils::RefCounterPtr)
    class MDApplicationListener
        : virtual public Utils::ReferenceCounter
        , virtual public MDThreadManager
    {
    protected:
        virtual ~MDApplicationListener() {}

    public:

        /// Called on errors in MDApplication
        /// @note This function can be called from different thread, so used should make it thread-safe in implementation
        /// @param error error message
        virtual void onError( std::string const& error ) = 0;


        /// Called on non X, d and W messages
        /// for enable calls on X messages, use MDApplication::startListeningXMessagesByApplication
        /// @note This function can be called from different thread, so used should make it thread-safe in implementation
        /// @param msg message
        virtual void process( Engine::FIXMessage const& msg, std::string const& channel_id )
        {
           (void)&channel_id;
           return process(msg);
        }

        /// Called after reader was connected to feed
        /// @note This function can be called from different thread, so used should make it thread-safe in implementation
        /// @param feedId - feed ID (%channel_id%:%connection_id%)
        virtual void onConnectToFeed( std::string const& feedId ) {
            ( void ) &feedId;
        }

        /// Called after reader was disconnected from feed
        /// @note This function can be called from different thread, so used should make it thread-safe in implementation
        /// @param feedId - feed ID (%channel_id%:%connection_id%)
        virtual void onDisconnectFromFeed( std::string const& feedId ) {
            ( void )&feedId;
        }

        /// Called after any message is processed
        virtual void onCompleteMessageProcessing() { }
    private:
        /// @Deprecated
        /// please use "process( Engine::FIXMessage const& msg,std::string const& channel_id )"
        /// Called on non X, d and W messages
        /// for enable calls on X messages, use MDApplication::startListeningXMessagesByApplication
        /// @note This function can be called from different thread, so used should make it thread-safe in implementation
        /// @param msg message
        /// @deprecated Please use "process( Engine::FIXMessage const& msg,std::string const& channel_id )"
        virtual void process( Engine::FIXMessage const& msg ) {
            ( void )&msg;
        }
    };

    /// Represents CME Globex application
    class V12_API MDApplication
    {
    protected:
        virtual ~MDApplication( void ) {}

    public:
        /// Stops application. Method does not stop application immediately.
        /// Use join method to wait until will stop.
        virtual void stop() = 0;

        /// Waits until application is stopped.
        virtual void join() = 0;

        /// Waits until application is stopped or timeout accured.
        /// return true, if application was stopped
        virtual bool join( unsigned int timeout ) = 0;

        /// Subscribes to market data for instrument by security description
        /// @deprecated This method is deprecated, use  Globex::MDApplication::subscribeBySecurityDesc instead
        B2B_DEPRECATED( "This method is deprecated, use Globex::MDApplication::subscribeBySecurityDesc instead" )
        virtual void subscribe( SecurityDesc const& secDesc, InstrumentListener* listener,
                                RecoveryOptions recovery = RO_USE_MARKET_RECOVERY ) = 0;

        /// Unsubscribes from instrument by security description
        /// @deprecated This method is deprecated, use Globex::MDApplication::unsubscribeBySecurityDesc instead
        B2B_DEPRECATED( "This method is deprecated, use Globex::MDApplication::unsubscribeBySecurityDesc instead" )
        virtual void unsubscribe( SecurityDesc const& symbol ) = 0;

        /// Subscribes to market data for instrument by security description
        virtual void subscribeBySecurityDesc( SecurityDesc const& secDesc, InstrumentListener* listener,
                                              RecoveryOptions recovery = RO_USE_MARKET_RECOVERY ) = 0;

        /// Unsubscribes from instrument by security description
        virtual void unsubscribeBySecurityDesc( SecurityDesc const& secDesc ) = 0;

        /// Unsubscribes from instrument by security ID
        virtual void unsubscribeBySecurityID( SecurityID const& secID ) = 0;

        /// Subscribes to market data for instrument by security ID and channel info
        virtual bool subscribeBySecurityID( SecurityID const& secID, const Globex::SecurityDesc& secDesc,
                                            std::string const& channel_id, InstrumentListener* listener,
                                            RecoveryOptions recovery = RO_USE_MARKET_RECOVERY ) = 0;

        /// Subscribes to instrument replay by symbol
        virtual void resolveInstrumentBySymbol( Symbol const& symbol, SecurityDefinitionListener* listener ) = 0;

        /// Subscribes to instrument replay by symbol in channel defined by channel_id
        virtual void resolveInstrumentBySymbol( Symbol const& symbol,
                                                SecurityDefinitionListener* listener,
                                                std::string const& channel_id ) = 0;

        /// Subscribes to instrument replay by security description
        virtual void resolveInstrumentBySecDesc( SecurityDesc const& secDesc, SecurityDefinitionListener* listener ) = 0;

        /// Subscribes to instrument replay by security ID
        virtual void resolveInstrumentBySecID( SecurityID const& secID, SecurityDefinitionListener* listener ) = 0;

        /// Subscribes to instrument replay by security ID in channel defined by channel_id
        virtual void resolveInstrumentBySecID( SecurityID const& secID,
                                               SecurityDefinitionListener* listener,
                                               std::string const& channel_id ) = 0;

        /// Subscribes to instrument replay by security group
        virtual void resolveInstrumentBySecurityGroup( std::string const& secGroup, SecurityDefinitionListener* listener ) = 0;

        /// Subscribes to instrument replay channel by channel ID
        virtual bool resolveInstrumentsByChannelID( std::string const& channel_id, SecurityDefinitionListener* listener ) = 0;

        /// Unsubscribe from instrument replay by symbol
        virtual void stopSymbolListener( Symbol const& symbol, SecurityDefinitionListener* listener ) = 0;

        /// Unsubscribe from instrument replay by security description
        virtual void stopSecDescListener( SecurityDesc const& secDesc, SecurityDefinitionListener* listener ) = 0;

        /// Unsubscribe from instrument replay by security ID
        virtual void stopSecIDListener( SecurityID const& secID, SecurityDefinitionListener* listener ) = 0;

        /// Unsubscribe from instrument replay by security group
        virtual void stopSecGroupListener( std::string const& secGroup, SecurityDefinitionListener* listener ) = 0;

        /// Unsubscribe ftom instrument replay by channel id
        virtual void stopChannelListener( std::string const& channel_id, SecurityDefinitionListener* listener ) = 0;

        /// Subscribes to the market data for all instruments on market
        virtual void subscribeAll( RecoveryOptions recovery = RO_USE_MARKET_RECOVERY ) = 0;

        /// Unsubscribe from all instruments
        virtual void unsubscribeAll() = 0;

        /// Returns listener assigned to application
        virtual MDApplicationListener* getListener() const = 0;

        /// Releases resources assigned to application
        virtual void release() = 0;

        ///start sending any "X" messages into ApplicationListener::process callback
        /// param channelId -id of channel or empty std::string for all channels
        virtual void startListeningXMessagesByApplication( std::string const& channelId ) = 0;

        /// param channelId -id of channel or empty std::string for all channels
        virtual void stopListeningXMessagesByApplication( std::string const& channelId ) = 0;

        /// parce string representation of FIX message into Engine::FIXMessage class
        /// param fixMessage -string representation of FIX message
        virtual Engine::FIXMessage* parseFixMessage( std::string const& fixMessage ) = 0;
    };
}

#endif // B2BITS_GLOBEX_Application_h__
