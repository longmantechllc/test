//
// Created by schen on 10/30/2020.
//

#ifndef RTXROUTER_CONFIGROUTERLOADER_H
#define RTXROUTER_CONFIGROUTERLOADER_H

class Logger;
class ConfigLoader;

class ConfigRouterLoader : public LoaderBase
{
public:
    ConfigRouterLoader();
    ConfigRouterLoader(Logger* logger);

    virtual ~ConfigRouterLoader();
    void InitConfigRouterLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node);

    void ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node) override;

    void PrintRoutess(ConfigLoader* loader);
};


#endif //RTXROUTER_CONFIGROUTERLOADER_H
