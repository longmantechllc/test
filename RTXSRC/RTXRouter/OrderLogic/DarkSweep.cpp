//
// Created by schen on 10/6/2020.
//
#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "RTXUtils.h"
#include "Logger.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"
#include "CPU.h"

#include "RTXOrder.h"
#include "IOrderLogicBase.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"

#include "DarkSweep.h"

DarkSweep::DarkSweep(RTXOrder* order)
        : IOrderLogicBase { order }
{
}

DarkSweep::~DarkSweep()
{
}

bool DarkSweep::InitOrder()
{
    if(m_isInited == true)
        return true;
    if(m_isRetailSwp)
    {
        m_sWPSequentialRoutes = m_order->m_exchange->retailSWP->sWPSequentialRoutes;
        m_sWPParallelRoutes = m_order->m_exchange->retailSWP->sWPParallelRoutes;
        m_isPMP = !m_order->m_exchange->retailSWP->sWPPriceCheckEnbaled;
//        cout << "retailSWP: sWPPriceCheckEnbaled:" << m_order->m_exchange->retailSWP->sWPPriceCheckEnbaled << ", m_isPMP:" << m_isPMP << endl;
        m_order->m_logger->logInfo("%s, m_isRetailSwpPMP=%s", "m_isRetailSwp", m_isPMP ? "true":"false");
    }
    else
    {
        m_sWPSequentialRoutes = m_order->m_exchange->sWP->sWPSequentialRoutes;
        m_sWPParallelRoutes = m_order->m_exchange->sWP->sWPParallelRoutes;
        m_isPMP = !m_order->m_exchange->sWP->sWPPriceCheckEnbaled;
//        cout << "sWP: sWPPriceCheckEnbaled:" << m_order->m_exchange->retailSWP->sWPPriceCheckEnbaled << ", m_isPMP:" << m_isPMP << endl;
        m_order->m_logger->logInfo("%s, m_isSwpPMP=%s", "isSwp", m_isPMP ? "true":"false");
    }
    m_isInited = true;
    return true;
}

bool DarkSweep::ProcessOrder()
{
    if(IsEligible() == false)
        return false;
    if(IsMarketable() == false)
        return false;

    if(SequentialSweep() == true)
        return true;

    if(ParallelSweep() == true)
        return true;

    return false;
}

bool DarkSweep::SequentialSweep()
{
    if(m_sWPSequentialRoutes.empty())
        return false;

    std::string routeName;
    auto itr = m_sWPSequentialRoutes.begin();
    while (itr != m_sWPSequentialRoutes.end())
    {
        if(IsRouteMarketable(*itr))
        {
            routeName = *itr;
            auto route = m_order->m_config->RouterConfig()->Route(routeName);
            if((route->oddLot == false && m_order->QtyUncommited() < m_order->m_lotsize)
               || m_order->QtyUncommited() < route->swpMinimumSize)
            {
                ++itr;
                continue;
            }
            else
            {
                itr = m_sWPSequentialRoutes.erase(itr);
                break;
            }
        }
        else
        {
            itr = m_sWPSequentialRoutes.erase(itr);
        }
    }

    if(routeName.empty())
    {
        if(m_sWPSequentialRoutes.empty() && m_sWPParallelRoutes.empty()) {
//            if (m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
            if(TestForCancel())
            {
                std::string msg;
                if(m_isRetailSwp)
                    msg = "no available route, no RetailSWP.(S)";
                else
                    msg = "no available route, no SWP.(S)";
                m_order->CancelParent(msg);
            }
        }
        return false;
    }

    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    auto cOrder = m_order->CreateChildOrder();
    cOrder->size_ = m_order->QtyUncommited();
    cOrder->TIF_ = route->tif;
    if(!m_isPMP)
    {
        cOrder->type_ = OrderType::TYPE_LIMIT;
        cOrder->price_ = CalculatePrice(route);
     }
    cOrder->exchange_ = route->venue;
    cOrder->routeName_ = routeName;

    if(!m_isPMP)
        m_order->m_logger->logInfo("oid:%d, Limit px: %.2f, NBBO %.2f, %sSWP", m_order->m_clientOrder->orderID_, m_order->EffectivePrice().ToDouble(), m_order->NBBO().ToDouble(), m_isRetailSwp ? "Retail" : "");
    SendChildOrder(cOrder);
    return true;
}

bool DarkSweep::ParallelSweep()
{
    if(m_sWPParallelRoutes.empty())
    {
//        m_order->m_logger->logInfo("m_sWPParallelRoutes.empty()");
        if(m_sWPSequentialRoutes.empty() && m_order->QtyPending() <= 0)
        {
            m_isDone = true;
            if(m_isRetailSwp)
            {
                m_order->m_isRetailSWPDone = true;
                m_order->m_isRetailSWPPMP = false;
                m_order->m_logger->logInfo("oid:%d, RetailSWP is Done", m_order->m_clientOrder->orderID_);
            }
            else
            {
                m_order->m_isSWPDone = true;
                m_order->m_isPMP = false;
//                m_order->m_logger->logInfo("oid:%d, SWP is Done", m_order->m_clientOrder->orderID_);
            }
//            if (m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
            if(TestForCancel())
            {
                std::string msg;
                if(m_isRetailSwp)
                    msg = "no available route, no RetailSWP.(P)";
                else
                    msg = "no available route, no SWP.(P)";
                m_order->CancelParent(msg);
            }
        }
        return false;
    }

    double totalPct = 0;
    auto itr = m_sWPParallelRoutes.begin();
    while (itr != m_sWPParallelRoutes.end())
    {
        if(IsRouteMarketable(itr->get()->name))
        {
            totalPct += itr->get()->allocationPercent;
            ++itr;
        }
        else
        {
            m_order->m_logger->logInfo("oid:%d, Route:%s is not eligible at this time.", m_order->m_clientOrder->orderID_, (*itr)->name.c_str());
            itr = m_sWPParallelRoutes.erase(itr);
        }
    }
    if(RTXUtils::DoubleEqual(totalPct, 0.0) || m_sWPParallelRoutes.empty())
    {
        m_isDone = true;
        if(m_isRetailSwp)
        {
            m_order->m_isRetailSWPDone = true;
            m_order->m_isRetailSWPPMP = false;
        }
        else
        {
            m_order->m_isSWPDone = true;
            m_order->m_isPMP = false;
        }
        if(m_sWPSequentialRoutes.empty() && m_sWPParallelRoutes.empty()) {
//            if (m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
            if(TestForCancel())
            {
                std::string msg;
                if(m_isRetailSwp)
                    msg = "no available route, no RetailSWP.(P)";
                else
                    msg = "no available route, no SWP.(P)";
                m_order->CancelParent(msg);
            }
         }
        return false;
    }
    if(!m_isPMP)
        m_order->m_logger->logInfo("oid:%d, Limit px: %.2f, NBBO %.2f, %sSWP", m_order->m_clientOrder->orderID_, m_order->EffectivePrice().ToDouble(), m_order->NBBO().ToDouble(), m_isRetailSwp ? "Retail" : "");

    std::string routeName;
    int qtyRemain = m_order->QtyUncommited();
    int qtyRemainTp = qtyRemain;
    int cnt = 0;
    int qSize = m_sWPParallelRoutes.size();

    itr = m_sWPParallelRoutes.begin();
    while (itr != m_sWPParallelRoutes.end())
    {
        ++cnt;
        routeName = itr->get()->name;
        auto route = m_order->m_config->RouterConfig()->Route(routeName);
        double Pct = itr->get()->allocationPercent;
        int qty = qtyRemain * (Pct / totalPct);
        qty = RTXUtils::RoundToLotSize(qty, m_order->m_lotsize);
        if(qty >= qtyRemainTp || cnt == qSize)
        {
            qty = qtyRemainTp;
            if((route->oddLot == false && qty < m_order->m_lotsize))
            {
                ++itr;
                continue;
            }
            qtyRemainTp = 0;
        }
        if(qty < route->swpMinimumSize)
        {
            ++itr;
            continue;
        }
        itr = m_sWPParallelRoutes.erase(itr);

        auto cOrder = m_order->CreateChildOrder();
        cOrder->size_ = qty;
        cOrder->TIF_ = route->tif;
        if(!m_isPMP)
        {
            cOrder->type_ = OrderType::TYPE_LIMIT;
            cOrder->price_ = CalculatePrice(route);
        }
        std::string venue = route->venue;
        cOrder->exchange_ = venue;
        cOrder->routeName_ = routeName;
        m_isParallel = true;

        SendChildOrder(cOrder);
        qtyRemainTp -= qty;
        auto routeInf = std::make_shared<SWPRouteInfo>(SWPRouteInfo(routeName, route->multiPounce, route->maxNumberOfTry));
        m_routeList.insert(std::make_pair(venue, routeInf));
        if(qtyRemainTp == 0)
            break;
    }
    return true;
}

Price DarkSweep::CalculatePrice(const std::shared_ptr< RouteStruct>& route)
{
    Price bbo = m_order->NBBO();
    Price px = bbo;
    Price offset = route->aggressiveOffset;
    if(offset > Price(0.0)) {
        if (m_order->IsBuyOrder())
        {
            px = bbo + offset;
            if ((m_order->m_clientOrder->type_ != OrderType::TYPE_MARKET) && px.isGE(m_order->EffectivePrice()))
//            if (px.isGE(m_order->EffectivePrice()))
                px = m_order->EffectivePrice();
        }
        else
        {
            if(offset.isLE(bbo))
                px = bbo - offset;
            if ((m_order->m_clientOrder->type_ != OrderType::TYPE_MARKET) && px.isLE(m_order->EffectivePrice()))
//            if (px.isLE(m_order->EffectivePrice()))
                px = m_order->EffectivePrice();
        }
    }
    return px;
}

bool DarkSweep::SendChildOrder(std::shared_ptr<Order> order)
{
    ChildOrderType tp = ChildOrderType::CHILD_ORDER_TYPE_SWP;
    if(m_isRetailSwp)
        tp = ChildOrderType::CHILD_ORDER_TYPE_RETAIL_SWP;
    m_order->SendChildOrder(order, tp);
    return false;
}

bool DarkSweep::ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if(m_isDone == true)
        return false;
    if(child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_SWP || child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_RETAIL_SWP)
        return false;

    auto routeInf = m_routeList.find(er->exchange_);
    if(routeInf != m_routeList.end())
    {
        if(m_isParallel && er->status_ == ERSTATUS::FILLED && er->size_ == er->cumulativeFillQuantity_ && m_order->QtyRemain() > 0)     //child fulfilled
        {
            ++routeInf->second->multiPounceCnt;
            if(routeInf->second->multiPounce == true && routeInf->second->multiPounceCnt < routeInf->second->maxNumberOfTry)
            {
                m_sWPSequentialRoutes.insert(m_sWPSequentialRoutes.begin(), routeInf->second->routeName);
            }
        }
    }
    if(m_sWPSequentialRoutes.empty() && m_sWPParallelRoutes.empty())
    {
        if(m_order->QtyPending() <= 0)
        {
            m_isDone = true;
            if(m_isRetailSwp)
            {
                m_order->m_isRetailSWPDone = true;
                m_order->m_isRetailSWPPMP = false;
            }
            else
            {
                m_order->m_isSWPDone = true;
                m_order->m_isPMP = false;
            }
//            if(m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
            if(TestForCancel())
            {
                std::string msg;
                if(m_isRetailSwp)
                    msg = "RetailSWP finished.";
                else
                    msg = "SWP finished.";
                m_order->CancelParent(msg);
            }
            return false;
        }
        return true;
    }
    return false;
}


bool DarkSweep::ProcessMktData()
{
    return true;
}

bool DarkSweep::IsEligible()
{
    if(m_isDone == true)
        return false;
    if(m_order->m_isCleaning || m_order->m_orderIsCancelling || m_order->m_orderIsDone)
        return false;

    if(!m_isRetailSwp)
    {
        if (m_order->m_isRetailSWPDone == false)
            return false;
        if (m_order->m_isRetailRouting == true)
            return false;
    }

    if(m_order->QtyUncommited() <= 0)
        return false;

    return true;
}

bool DarkSweep::IsRouteEligible(const std::string& routeName)
{
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!route)
        return false;
    if(m_order->QtyRemain() < route->swpMinimumSize )
        return false;
    if(route->oddLot == false && m_order->QtyRemain() < m_order->m_lotsize )
        return false;
    if(route->InOperationTime(RTXUtils::NowSsm()) == false)       //may used in the future
        return false;
    if(m_order->ExcludedRoute(routeName) == true)
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(route->venue))
        return false;

    return true;
}

bool  DarkSweep::IsRouteMarketable(const std::string& routeName)
{
    if(!IsRouteEligible(routeName))
        return false;
    if(m_isPMP)         ////? for market order?
        return true;
    if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
        return true;
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!m_order->IsMarketable(m_order->EffectivePrice(), m_order->NBBO()))
    {
        if (route->midpointEligibility && m_order->IsMarketable(m_order->EffectivePrice(), m_order->NbboMidpoint()))
             return true;
        return false;
    }
    return true;
}

bool DarkSweep::IsMarketable()
{
    if(m_isDone)
        return false;

////    if(!(m_order->m_cpu->GetMarketSession() == MarketSession::In_Market))
//    auto now = RTXUtils::NowMssm();
//    if(now < m_order->m_config->GlobalConfig()->SWPStartTime() || now >= m_order->m_config->GlobalConfig()->SWPEndTime())
//    {
//        m_isDone = true;
//    if(m_isRetailSwp)
//    {
//        m_order->m_isRetailSWPDone = true;
//        m_order->m_isRetailSWPPMP = false;
//    }
//    else {
//        m_order->m_isSWPDone = true;
//        m_order->m_isPMP = false;
//    }
//        return false;
//    }
    if(m_isPMP)         ////? for market order?
        return true;
    if(m_order->m_marketDataFailureTimeOutPost == true)
    {
        m_isDone = true;
        if(m_isRetailSwp)
        {
            m_order->m_isRetailSWPDone = true;
            m_order->m_isRetailSWPPMP = false;
        }
        else
        {
            m_order->m_isSWPDone = true;
            m_order->m_isPMP = false;
        }
        return false;
    }
    if(!m_order->m_validBbo)
    {
//        if(m_order->m_validTob)
//        {
//            Price bestPx = m_order->m_tobList.GetBestQuotePrice();
//            if(m_order->IsMarketable(m_order->EffectivePrice(), bestPx))
//            {
//                if(m_order->IsBuyOrder())
//                    m_order->m_nbboAsk = bestPx;
//                else
//                    m_order->m_nbboBid = bestPx;
//                m_order->m_validTob = true;
//                m_order->m_logger->logInfo("oid:%d, SWP timer expired, Use valid tob as nbbo for %sSWP", (m_order->m_clientOrder->orderID_), m_isRetailSwp? "Retail":"");
//            }
//        }
//        else
//        {
        std::string who="";
        m_isDone = true;
        if(m_isRetailSwp)
        {
            m_order->m_isRetailSWPDone = true;
            m_order->m_isRetailSWPPMP = false;
            who = "Retail ";
        }
        else
        {
            m_order->m_isSWPDone = true;
            m_order->m_isPMP = false;
        }

        m_order->m_logger->logInfo("oid:%d, %s%sSWP PriceCheck enabled, but no bbo received. No %sSWP", m_order->m_clientOrder->orderID_, who.c_str(), m_isRetailSwp ? "Retail" : "", m_isRetailSwp ? "Retail" : "");
//        if (m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
        if(TestForCancel())
        {
            std::string msg;
            if(m_isRetailSwp)
                msg = "No bbo received, no RetailSWP.(Mkt)";
            else
                msg = "No bbo received, no SWP.(Mkt)";
            m_order->CancelParent(msg);
        }
        return false;
//        }
    }

//
//    if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
//    {
//        if(m_order->m_validTob == false || m_order->m_validTrade == false)
//        {
//            m_isDone = true;
//          if(m_isRetailSwp)
//          {
//              m_order->m_isRetailSWPDone = true;
//              m_order->m_isRetailSWPPMP = false;
//          }
//          else {
//            m_order->m_isSWPDone = true;
//            m_order->m_isPMP = false;
//    }
//            return false;
//        }
//    }
    return true;
}

bool DarkSweep::TestForCancel()
{
    if ((m_order->m_argoPtr.size() == 1)
        || (m_order->m_argoPtr.size() == 2 && ( //(m_isRetailSwp && (!m_order->m_isSWPDone || m_order->m_isRetailRouting)) ||
                                              (!m_isRetailSwp && ( m_order->m_isRetailSWPInit ||  m_order->m_isRetailRoutingInit))))
        || (m_order->m_argoPtr.size() == 3 && (!m_isRetailSwp && (m_order->m_isRetailSWPInit && m_order->m_isRetailRoutingInit)) ))

//    if (m_order->m_argoPtr.size() == 1 || (m_order->m_argoPtr.size() == 2 && m_order->m_isRetailRoutingInit))
        return true;

    return false;
}