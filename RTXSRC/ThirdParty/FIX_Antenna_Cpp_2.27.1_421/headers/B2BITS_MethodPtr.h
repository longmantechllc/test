// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MethodPtr.h
/// Defines Utils::MethodPtrX classes

#if defined( HAVE_PRAGMA_ONCE )
#   pragma once
#endif // HAVE_PRAGMA_ONCE

#ifndef H_B2BITS_UTILS_METHODPTR_H
#define H_B2BITS_UTILS_METHODPTR_H

#include <cstddef>

namespace Utils
{
    template< typename return_t >
    class MethodPtr0
    {
    private:
        typedef return_t ( *func_type )( void* );

    private:
        func_type func_;
        void* ptr_;

    public:
        MethodPtr0() throw()
            : func_( NULL )
            , ptr_( NULL ) {
        }

        template< typename T, typename PtrT >
        MethodPtr0( return_t ( *func )( T* ), PtrT* ptr ) throw()
        {
            init<T>( func, ptr );
        }

        template< typename T >
        static MethodPtr0 create( return_t ( *func )( T* ), T* ptr ) throw()
        {
            return MethodPtr0( func, ptr );
        }

        return_t operator() () const {
            return ( *func_ )( ptr_ );
        }

        operator bool() const throw() {
            return NULL != func_;
        }

    private:
        template< typename T >
        void init( return_t ( *func )( T* ), T* ptr ) throw()
        {
            func_ = reinterpret_cast<func_type>( func );
            ptr_ = const_cast<void*>( static_cast<void const*>( ptr ) );
        }
    }; // // class MethodPtr0

    template< typename return_t, typename arg_t >
    class MethodPtr1
    {
    private:
        typedef return_t ( *func_type )( void*, arg_t );

    private:
        func_type func_;
        void* ptr_;

    public:
        MethodPtr1() throw()
            : func_( NULL )
            , ptr_( NULL ) {
        }

        template< typename T, typename PtrT >
        MethodPtr1( return_t ( *func )( T*, arg_t ), PtrT* ptr ) throw()
        {
            init<T>( func, ptr );
        }

        template< typename T >
        static MethodPtr1 create( return_t ( *func )( T*, arg_t ), T* ptr ) throw()
        {
            return MethodPtr1( func, ptr );
        }

        return_t operator() ( arg_t arg1 ) const {
            return ( *func_ )( ptr_, arg1 );
        }

        operator bool() const throw() {
            return NULL != func_;
        }

    private:
        template< typename T >
        void init( return_t ( *func )( T*, arg_t ), T* ptr ) throw()
        {
            func_ = reinterpret_cast<func_type>( func );
            ptr_ = const_cast<void*>( static_cast<void const*>( ptr ) );
        }

    }; // class MethodPtr1

    template< typename return_t, typename arg1_t, typename arg2_t >
    class MethodPtr2
    {
    private:
        typedef return_t ( *func_type )( void*, arg1_t, arg2_t );

    private:
        func_type func_;
        void* ptr_;

    public:
        MethodPtr2() throw()
            : func_( NULL )
            , ptr_( NULL ) {
        }

        template< typename T, typename PtrT >
        MethodPtr2( return_t ( *func )( T*, arg1_t, arg2_t ), PtrT* ptr ) throw()
        {
            init<T>( func, ptr );
        }

        template< typename T >
        static MethodPtr2 create( return_t ( *func )( T*, arg1_t, arg2_t ), T* ptr ) throw()
        {
            return MethodPtr2( func, ptr );
        }

        return_t operator() ( arg1_t arg1, arg2_t arg2 ) const {
            return ( *func_ )( ptr_, arg1, arg2 );
        }

        operator bool() const throw() {
            return NULL != func_;
        }
    private:
        template< typename T >
        void init( return_t ( *func )( T*, arg1_t, arg2_t ), T* ptr ) throw()
        {
            func_ = reinterpret_cast<func_type>( func );
            ptr_ = const_cast<void*>( static_cast<void const*>( ptr ) );
        }

    }; // class MethodPtr2

    template< typename return_t, typename arg1_t, typename arg2_t, typename arg3_t >
    class MethodPtr3
    {
    private:
        typedef return_t ( *func_type )( void*, arg1_t, arg2_t, arg3_t );

    private:
        func_type func_;
        void* ptr_;

    public:
        MethodPtr3() throw()
            : func_( NULL )
            , ptr_( NULL ) {
        }

        template< typename T, typename PtrT >
        MethodPtr3( return_t ( *func )( T*, arg1_t, arg2_t, arg3_t ), PtrT* ptr ) throw()
        {
            init<T>( func, ptr );
        }

        template< typename T >
        static MethodPtr3 create( return_t ( *func )( T*, arg1_t, arg2_t, arg3_t ), T* ptr ) throw()
        {
            return MethodPtr3( func, ptr );
        }

        return_t operator() ( arg1_t arg1, arg2_t arg2, arg3_t arg3 ) const {
            return ( *func_ )( ptr_, arg1, arg2, arg3 );
        }

        operator bool() const throw() {
            return NULL != func_;
        }

    private:
        template< typename T >
        void init( return_t ( *func )( T*, arg1_t, arg2_t, arg3_t ), T* ptr ) throw()
        {
            func_ = reinterpret_cast<func_type>( func );
            ptr_ = const_cast<void*>( static_cast<void const*>( ptr ) );
        }
    }; // class MethodPtr3

    template <typename T>
    void swap ( T& a, T& b )
    {
        T temp( a );
        a = b;
        b = temp;
    }
}
#endif // H_B2BITS_UTILS_METHODPTR_H
