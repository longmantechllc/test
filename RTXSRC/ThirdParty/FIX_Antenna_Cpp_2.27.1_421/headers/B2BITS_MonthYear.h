// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MonthYear.h
/// Contains Engine::MonthYear class definition

#ifndef B2BITS_FIX_TYPES_MonthYear_H
#define B2BITS_FIX_TYPES_MonthYear_H

#include <string>
#include <iosfwd>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_String.h>


namespace Engine
{

    /// \brief Encapsulates MonthYear FIX type
    class V12_API MonthYear
    {
    public:
        /// Maximum buffer size required to store value
        static const unsigned int ValueSizeWithWeek = sizeof( "YYYYMMwD" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSizeWithDay = sizeof( "YYYYMMDD" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSizeNoDay = sizeof( "YYYYMM" ) - 1;

        /// Maximum buffer size required to store value
        static const unsigned int ValueSize = ValueSizeWithWeek;

        enum Week {
            NA,
            w1,
            w2,
            w3,
            w4,
            w5
        };

        /// \brief Constructor
        MonthYear() throw();

        /// \brief Creates object from year and month
        MonthYear( int year, int month ) throw();

        /// \brief Creates object from year, month and day
        MonthYear( int year, int month, int day ) throw();

        /// \brief Creates object from year, month and week
        MonthYear( int year, int month, Week week ) throw();

        /// \brief Returns year
        int year() const throw();
        /// \brief Returns month
        int month() const throw();
        /// \brief Returns day
        int day() const throw();
        /// \brief Returns week
        Week week() const throw();

        /// Constructs object from FIX string.
        static MonthYear fromFixString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMMDDW).
        static MonthYear fromDecimal( System::u32 val ) throw();

        /// Constructs object from decimal date format (YYYYMMDDW).
        static MonthYear fromDecimalString( AsciiString val ) throw();

        /// Constructs object from string.
        static MonthYear fromString( AsciiString val ) throw();

        /// Constructs object from decimal date format (YYYYMM).
        static MonthYear fromYYYYMM( System::u32 val ) throw();

        /// Constructs object from decimal date format (YYYYMMDD).
        static MonthYear fromYYYYMMDD( System::u32 val ) throw();

        /// Converts value stored in object to FIX string.
        /// and stores value to dest
        std::string& toFixString( std::string* dest ) const;

        /// Converts value stored in object to FIX string.
        /// and stores value to dest
        char* toFixString( char* buf, char* end ) const throw();

        /// Returns value in decimal format (YYYYMMDDW)
        System::u32 toDecimal() const throw();

        /// Returns value in decimal format
        System::u32 toYYYYMMDD() const throw();

        /// Returns value in decimal format
        System::u32 toYYYYMM() const throw();

        /// \brief Compares two MonthYear
        bool operator == ( const MonthYear& v ) const throw();
        /// \brief Compares two MonthYear
        bool operator != ( const MonthYear& v ) const throw();

    private:
        MonthYear( System::u32 v ) throw() : value_( v ) {}

    private:
        System::u32 value_;
    };

    inline bool MonthYear::operator == ( const MonthYear& v ) const throw()
    {
        return value_ == v.value_;
    }

    inline bool MonthYear::operator != ( const MonthYear& v ) const throw()
    {
        return value_ != v.value_;
    }

    /// \brief Writes a MonthYear into the output stream.
    extern V12_API std::ostream& operator << ( std::ostream& s, const MonthYear& v );
}

#endif
