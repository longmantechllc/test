#include <magic_enum.hpp>
#include <comm/channel.h>
#include "../../../include/exchanges/utils/l2book_controller.h"
#include "../../../include/exchanges/utils/ws_connection_handler.h"

namespace crypto::exchanges::connection {
static crypto::utils::PerfCounter cb_onmsg_counter = crypto::utils::PerfCounter("cb_onmsg");

WsConnectionHandler::WsConnectionHandler(
    data_controller::on_update_t on_message_cb,
    data_controller::on_ready_t on_ready_cb,
    data_controller::on_error_t on_error_cb,
    data_controller::build_ping_msg_t build_ping_msg,
    data_controller::build_pong_msg_t build_pong_msg,
    data_controller::build_auth_msg_t build_auth_msg,
    data_controller::build_subscription_msg_t build_subscription_msg,
    crypto::exchanges::EventQueue &event_queue,
    std::shared_ptr<WsEngine> engine,
    std::string path,
    std::string host,
    int port,
    int interface,
    int ws_index,
    milliseconds subscribe_timeout,
    milliseconds connect_timeout,
    milliseconds connect_retry_delay,
    milliseconds receive_timeout,
    milliseconds ping_interval,
    int connect_retry_count,
    bool need_connect,
    bool need_subscribe,
    bool need_authenticate,
    bool no_authenticate_reply,
    bool no_subscribe_reply)
: on_message_cb(std::move(on_message_cb))
, on_ready_cb(std::move(on_ready_cb))
, on_error_cb(std::move(on_error_cb))
, build_ping_msg(std::move(build_ping_msg))
, build_pong_msg(std::move(build_pong_msg))
, build_auth_msg(std::move(build_auth_msg))
, build_subscription_msg(std::move(build_subscription_msg))
, event_queue(event_queue)
, engine(std::move(engine))
, path(std::move(path))
, host(std::move(host))
, port(port)
, interface(interface)
, ws_index_(ws_index)
, subscribe_timeout(subscribe_timeout)
, connect_timeout(connect_timeout)
, connect_retry_delay(connect_retry_delay)
, receive_timeout_(receive_timeout)
, ping_interval_(ping_interval)
, connect_retry_count(connect_retry_count)
, need_connect(need_connect)
, need_subscribe(need_subscribe)
, need_authenticate(need_authenticate)
, no_authenticate_reply(no_authenticate_reply)
, no_subscribe_reply(no_subscribe_reply) {
    if (this->need_connect) {
        this->connection = std::make_shared<WsConnection>(
            [&](std::string_view in) { this->on_message(in); },
            [&]() { this->on_ws_pong(); },
            [&]() { this->on_connect(); },
            [&]() { this->on_disconnect(); },
            this->event_queue,
            this->engine,
            this->path,
            this->host,
            this->port,
            this->interface,
            this->ws_index_,
            this->connect_timeout,
            this->connect_retry_delay,
            this->connect_retry_count);
    }        
    this->update_state_machine(BEGIN);
    crypto::influx::RegisterCounter(&cb_onmsg_counter);
}

void WsConnectionHandler::on_message(std::string_view in) {
    cb_onmsg_counter.IncrementCounter((int64_t)in.length());
    auto data = this->on_message_cb(in);
    switch (data.first) {
        case crypto::exchanges::common::exchange_message_t::PING: {
            spdlog::info("<WsConnectionHandler::on_message> : idx={} PING {}", 
                ws_index_, std::get<2>(data.second));
            this->send_message(this->build_pong_msg(std::get<2>(data.second)));
            break;
        }
        case crypto::exchanges::common::exchange_message_t::AUTHENTICATE_SUCCESS: {
            spdlog::info(
                "<WsConnectionHandler::on_message> : idx={} AUTHENTICATE_SUCCESS",
                ws_index_);
            this->update_state_machine(AUTHENTICATE_SUCCESS);
            break;
        }
        case crypto::exchanges::common::exchange_message_t::SUBSCRIBE_SUCCESS: {
            spdlog::info(
                "<WsConnectionHandler::on_message> : idx={} SUBSCRIBE_SUCCESS",
                ws_index_);
            this->update_state_machine(SUBSCRIBE_SUCCESS);
            break;
        }
        default: {
            break;
        }
    }
    recv_count_++;
    last_receive_data_ts_ = crypto::comm::selector::cycle_now();
}

void WsConnectionHandler::on_ws_pong() {
    last_receive_data_ts_ = crypto::comm::selector::cycle_now();
}

void WsConnectionHandler::on_connect() {
    this->update_state_machine(CONNECT_SUCCESS);
    // Start ping message timer here
    if (ping_interval_.count() > 0 && !ping_timer_ptr_) {
        ping_timer_ptr_ = crypto::comm::selector::add_timer(
            [&, inteval_in_nano = ping_interval_.count() * 1000000] {
                this->send_message(this->build_ping_msg());
                // repeat fire
                return uint64_t(inteval_in_nano);
            }, 
            // first fire
            uint64_t(0)
        );
    }
}

void WsConnectionHandler::subscribe() {
    auto subs = this->build_subscription_msg();
    for(auto& req : subs) {
        this->send_message(req);
    }
}

void WsConnectionHandler::authenticate() {
    this->send_message(this->build_auth_msg());
}

void WsConnectionHandler::send_message(const std::string &message) {
    this->connection->send_message(message);
}

void WsConnectionHandler::on_disconnect() {
    this->update_state_machine(RECV_ERROR);
    // Stop ping message timer here
    if (ping_timer_ptr_) {
        crypto::comm::selector::del_timer(ping_timer_ptr_);
        ping_timer_ptr_ = nullptr;
    }

    this->on_error_cb();
}

void WsConnectionHandler::refresh() {
    this->connection->reconnect();
}

void WsConnectionHandler::update_backoff_time() {
    auto current_time = std::chrono::system_clock::now();
    if (current_time - this->backoff_time.second > 10s) {
        this->backoff_time.first = 20ms;
    } else {
        this->backoff_time.first = std::min(
            duration_cast<milliseconds>(this->backoff_time.first * 1.25), 
            2000ms);
    }
    this->backoff_time.second = current_time;
}

void WsConnectionHandler::on_timeout(
    State current_state, milliseconds timeout, Action new_action) {

    crypto::comm::selector::add_timer(
        [&, current_state, timeout, new_action] {
            if (this->state == current_state) {
                this->update_state_machine(new_action);
            }
            // one time
            return uint64_t(0);
        }, 
        // first delay
        uint64_t(timeout.count() * 1000000)
    );
}

void WsConnectionHandler::on_ready() {
    if (this->on_ready_cb) {
        this->on_ready_cb();
    }
}

void WsConnectionHandler::update_state_machine(
        Action action, time_point<system_clock> tp) {
    spdlog::info("<WsConnectionHandler::update_state_machine> : idx={} {}", 
        ws_index_, magic_enum::enum_name(action));
    this->input_queue.push(action);
    this->event_queue.push(tp, [&]() { this->state_machine(); });
}

void WsConnectionHandler::state_machine() {
    WsConnectionHandler::Action action = this->input_queue.front();
    this->input_queue.pop();
    spdlog::info(
        "<WsConnectionHandler::state_machine> : index: {} | state: {} | action: {}",
        ws_index_, magic_enum::enum_name(this->state), 
        magic_enum::enum_name(action));
    switch (this->state) {
        case INIT: {
            switch (action) {
                case BEGIN: {
                    if (this->connection->is_connected) {
                        this->state = CONNECTED;
                    } else {
                        this->state = CONNECTING;
                    }
                    break;
                }
                case CONNECT_SUCCESS: {
                    this->state = CONNECTED;
                    break;
                }
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case CONNECTING: {
            switch (action) {
                case CONNECT_SUCCESS: {
                    if (this->need_authenticate) {
                        this->update_backoff_time();
                        this->update_state_machine(
                            AUTHENTICATE, 
                            system_clock::now() + this->backoff_time.first);
                        this->state = CONNECTED;
                    } else if (this->need_subscribe) {
                        this->update_backoff_time();
                        this->update_state_machine(
                            SUBSCRIBE, 
                            system_clock::now() + this->backoff_time.first);
                        this->state = CONNECTED;
                    } else {
                        this->state = ACTIVE;
                        this->on_ready();
                    }
                    break;
                }
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case CONNECTED: {
            switch (action) {
                case CONNECT_SUCCESS: {
                    if (this->need_authenticate) {
                        this->update_backoff_time();
                        this->update_state_machine(
                            AUTHENTICATE, 
                            system_clock::now() + this->backoff_time.first);
                        this->state = CONNECTED;
                    } else if (this->need_subscribe) {
                        this->update_backoff_time();
                        this->update_state_machine(
                            SUBSCRIBE, 
                            system_clock::now() + this->backoff_time.first);
                        this->state = CONNECTED;
                    } else {
                        this->state = ACTIVE;
                        this->on_ready();
                    }
                    break;
                }

                case AUTHENTICATE: {
                    this->state = AUTHENTICATING;
                    this->authenticate();
                    this->on_timeout(this->state, this->subscribe_timeout, 
                        AUTHENTICATE_TIMEOUT);
                    break;
                }
                case SUBSCRIBE: {
                    this->state = SUBSCRIBING;
                    this->subscribe();
                    this->on_timeout(this->state, this->subscribe_timeout, 
                        SUBSCRIBE_TIMEOUT);
                    break;
                }
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case AUTHENTICATING: {
            switch (action) {
                case AUTHENTICATE_SUCCESS: {
                    if (this->need_subscribe) {
                        this->state = SUBSCRIBING;
                        this->subscribe();
                        this->on_timeout(this->state, this->subscribe_timeout, 
                            SUBSCRIBE_TIMEOUT);
                    } else {
                        this->state = ACTIVE;
                        this->on_ready();
                    }
                    break;
                }
                case AUTHENTICATE_TIMEOUT:
                    if (this->no_authenticate_reply) {
                        this->update_state_machine(AUTHENTICATE_SUCCESS);
                        break;
                    }
                    [[fallthrough]];
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case SUBSCRIBING: {
            switch (action) {
                case SUBSCRIBE_SUCCESS: {
                    this->state = ACTIVE;
                    this->on_ready();
                    if (book_ctrl_ && book_ctrl_->need_snapshot()) {
                        book_ctrl_->send_snapshot(
                            crypto::exchanges::utils::l2book_controller::ON_SUBSCRIBE_SUCCESS);
                    }
                    break;
                }
                case SUBSCRIBE_TIMEOUT:
                    if (this->no_subscribe_reply) {
                        this->update_state_machine(SUBSCRIBE_SUCCESS);
                    }
                    else {
                        this->subscribe();
                        this->on_timeout(this->state, this->subscribe_timeout, 
                            SUBSCRIBE_TIMEOUT);
                    }
                    break;
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        case ACTIVE: {
            switch (action) {
                case RECV_ERROR: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                case RECV_TIMEOUT: {
                    this->update_state_machine(BEGIN);
                    this->state = INIT;
                    break;
                }
                default:
                    break;
            }
            break;
        }
        default:
            break;
    }
}

WsConnectionHandler::~WsConnectionHandler() {
    disconnect();
}

void WsConnectionHandler::disconnect() {
    if (connection && connection->is_connected) {
        spdlog::info(
            "<WsConnectionHandler::disconnect> [{}] disconnecting", ws_index_);
        connection->disconnect();
    }

    // Stop ping message timer here
    if (ping_timer_ptr_) {
        crypto::comm::selector::del_timer(ping_timer_ptr_);
        ping_timer_ptr_ = nullptr;
    }
}

void WsConnectionHandler::reconnect() {
    if (this->need_connect && last_receive_data_ts_ > 0) {
        this->disconnect();
        old_connections_.push_back(this->connection);
        last_receive_data_ts_ = 0;
        spdlog::info(
            "<check_receive_timeout> [{}] reconnecting", ws_index_);
        this->connection = std::make_shared<WsConnection>(
            [&](std::string_view in) { this->on_message(in); },
            [&]() { this->on_ws_pong(); },
            [&]() { this->on_connect(); },
            [&]() { this->on_disconnect(); },
            this->event_queue,
            this->engine,
            this->path,
            this->host,
            this->port,
            this->interface,
            this->ws_index_,
            this->connect_timeout,
            this->connect_retry_delay,
            this->connect_retry_count);
        spdlog::info("<check_receive_timeout> [{}], created a new ws connection",
            ws_index_);

        this->update_state_machine(BEGIN);
    }
}

void WsConnectionHandler::set_book_ctrl(
    crypto::exchanges::utils::l2book_controller* book_ctrl) {
    book_ctrl_ = book_ctrl;
}

void WsConnectionHandler::set_book_ctrl_v3(
    crypto::exchanges::utils::l2book_controller_v3* book_ctrl) {
    book_ctrl_v3_ = book_ctrl;
}

}  // namespace crypto::exchanges::connection