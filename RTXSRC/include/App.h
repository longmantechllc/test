//
// Created by schen on 10/2/2020.
//

#ifndef RTXROUTER_APP_H
#define RTXROUTER_APP_H

using namespace std;

class Logger;
class LogToFile;
class ConfigLoader;
class Config;

class RTXOrderIO;
class MktDataIO;
class IOIDataIO;
class CPU;
class App {
public:
    App();
    virtual ~App();
    bool InitializeApp(const std::string &rootDir = "");
    bool InitLogging();
    bool InitConfig();
    bool InitCPU();

    std::shared_ptr<RTXOrderIO>& GetOrderIO() { return m_RTXOrderIO; }
    std::shared_ptr<MktDataIO>& GetMktDataIO() { return m_MktDataIO; }
    std::shared_ptr<IOIDataIO>& GetIOIDataIO() { return m_IOIDataIO; }

    Logger* m_logger = nullptr;           //for orders
    Logger* m_loggerSys = nullptr;        //for system
    Logger* m_loggerException = nullptr;
    ConfigLoader* m_configLoader = nullptr;
    Config* m_config = nullptr;
    CPU* m_cpu = nullptr;

    std::shared_ptr<RTXOrderIO> m_RTXOrderIO;
    std::shared_ptr<MktDataIO> m_MktDataIO;
    std::shared_ptr<IOIDataIO> m_IOIDataIO;
protected:
private:
    LogToFile* m_logToFileEx;
    LogToFile* m_logToFileSys;
    LogToFile* m_logToFile;       //for orders

} ;

#endif //RTXROUTER_APP_H
