/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "Properties.h"
#include "Exception.h"
#include "Auxiliary.h"

#include <iostream>
#include <utility>

using namespace std;
using namespace Benchmark;

const char* const Properties::SENDER             = "sender";
const char* const Properties::TARGET             = "target";
const char* const Properties::AGGRESSIVE_SEND    = "enableAggressiveSend";
const char* const Properties::FIXVERSION         = "fix.version";
const char* const Properties::VOLUME             = "volume";
const char* const Properties::HBI                = "hbi";
const char* const Properties::HOST               = "host";
const char* const Properties::PORT               = "port";
const char* const Properties::MESSAGE            = "message";
const char* const Properties::SESSIONCOUNT       = "sessionCount";
const char* const Properties::TCPBUFFERENABLED   = "tcpBufferEnabled";
const char* const Properties::MAXMSGAMOUNTBUNCH  = "maxMessageAmountInBunch";
const char* const Properties::DELAY              = "delay";
const char* const Properties::STORAGE_TYPE       = "storageType";
const char* const Properties::USESSL             = "useSSL";

namespace Benchmark
{
    typedef std::pair<std::string, std::string> StringPair;
    static const StringPair PARAMS[] = {
        StringPair( Properties::SENDER,             "Receiver" ),
        StringPair( Properties::TARGET,             "Sender" ),
        StringPair( Properties::AGGRESSIVE_SEND,    "false" ),
        StringPair( Properties::FIXVERSION,         "4.2" ),
        StringPair( Properties::VOLUME,             "10000" ),
        StringPair( Properties::HOST,               "localhost" ),
        StringPair( Properties::PORT,               "9106" ),
        StringPair( Properties::HBI,                "30" ),
        StringPair( Properties::MESSAGE,            "order.msg" ),
        StringPair( Properties::SESSIONCOUNT,       "1" ),
        StringPair( Properties::TCPBUFFERENABLED,   "true" ),
        StringPair( Properties::MAXMSGAMOUNTBUNCH,  "0" ),
        StringPair( Properties::DELAY,              "0" ),
        StringPair( Properties::STORAGE_TYPE,       "null" ),
        StringPair( Properties::USESSL,             "false" )
    };
}

Properties::Properties( std::istream* apIStream )
{
    apIStream->seekg( 0, ios::beg );

    string s;
    while( getline( *apIStream, s ) ) {
        // skip comments
        if( 0 == s.size() || '#' == s[0] || '!' == s[0] ) {
            continue;
        }

        // skip empty lines
        if( string::npos == s.find_first_not_of( " \t\r" ) ) {
            continue;
        }

        const char separators[] = "=:, \t";
        bool done = false;
        string::size_type pos = 0;
        string::size_type endOfKey = 0;
        do {
            endOfKey = s.find_first_of( separators, pos );
            if( string::npos != endOfKey ) {
                if( ( endOfKey > 0 ) && ( '\\' == s.at( endOfKey - 1 ) ) ) {
                    pos = endOfKey + 1;
                    continue;
                }
            }
            done = true;
        } while( ! done );

        if( string::npos != endOfKey ) {
            string key = s.substr( 0, endOfKey );

            string::size_type slashPos = key.find( "\\" );
            while( string::npos != slashPos ) {
                key.erase( slashPos, strlen( "\\" ) );
                slashPos = key.find( "\\" );
            }

            string::size_type beginOfWord = s.find_first_not_of( separators, endOfKey );
            string value( "" );

            if( string::npos != beginOfWord ) {
                value = s.substr( beginOfWord, s.size() - beginOfWord );
                if( '\r' == value.at( value.size() - 1 ) ) {
                    value.resize( value.size() - 1 );
                }
            }

            slashPos = key.find( "\\" );
            while( string::npos != slashPos ) {
                if( ( slashPos == ( value.size() - 1 ) ) || ( '\\' != ( value.at( slashPos + 1 ) ) ) ) {
                    value.erase( slashPos, strlen( "\\" ) );
                    slashPos = value.find( "\\" );
                } else {
                    ++slashPos;
                }
            }

            pair<MapType::iterator, bool> ret = m_pairs.insert( make_pair( key, value ) );
            if( ! ret.second ) {
                throw Benchmark::Exception( "Invalid property's line: \"" + s + "\", this key has been defined earlier." );
            }
        } else {
            throw Benchmark::Exception( "Invalid property's line: \"" + s + "\", can't find separator." );
        }
    }
}

std::string Properties::getString( const std::string& key ) const
{
    MapType::const_iterator it = m_pairs.find( key );
    if( m_pairs.end() != it ) {
        return it->second;
    } else {
        throw Exception( "Cannot find the '" + key + "' property" );
    }
}

int Properties::getInteger( const std::string& key ) const
{
    return atoi( getString( key ).c_str() );
}
