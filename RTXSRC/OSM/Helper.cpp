#include "Helper.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>
#include <cstring>

using namespace ::std;
//using namespace ::Utils;
//using namespace ::System;
using namespace ::Aux;

////////////////////////////////////////////////////////////////////////////////
// Constants.
/// Application required arguments quantity.

// Writes message to console.
void Helper::writeLineToConsole(const string& message)
{
    clog << message << endl;
}

// Writes line to stderr.
void Helper::writeErrorLine(const string& message)
{
    cerr << message << endl;
}

// Loads file content into std::string.
string Helper::loadString(const string& fileName)
{

    struct stat statistics;

    // Retrieves file size.
    if (-1 == stat(fileName.c_str(), &statistics)) {
        throw strerror(errno);
    }

    // Opens file.
    ifstream ifs(fileName.c_str(), ios::binary);

    if (!ifs.good()) {
        throw strerror(errno);
    }

    string str;

    string tmp_ln;
    getline(ifs, tmp_ln);
    if (!tmp_ln.empty()) {
        string::size_type beg = tmp_ln.find("\x01" "9=");
        if (beg == string::npos) {
            throw string("Must be size (tag 9) in message");
        }
        string::size_type end = tmp_ln.find("\x01", beg + 3);
        if (end == string::npos) {
            throw string("Must be size (tag 9) in message");
        }
        string msg_size = tmp_ln.substr(beg + 3, end - beg - 3);
        char* errPos = NULL;
        size_t size = strtol(msg_size.c_str(), &errPos, 10);
        while (size > tmp_ln.size()) {
            if (ifs.eof()) {
                throw string("Unexpected end of file");
            }
            string tmp_ln2;
            char buf[10 * 1024];
            buf[0] = 10;
            ifs.read(&buf[1], size - tmp_ln.size());
            tmp_ln += string(&buf[0], size - tmp_ln.size() + 1);
            tmp_ln2.clear();
            getline(ifs, tmp_ln2);
            tmp_ln += tmp_ln2;
        }
        if (*tmp_ln.rbegin() == '\r') {
            tmp_ln.erase(tmp_ln.size() - 1);
        }
        str = tmp_ln;
    }

    ifs.close();

    return str;
}

void Helper::parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params)
{
    params->acceptor_cfg = prop.getString(Aux::Properties::ACCEPTOR_CFG);
    params->initiator_cfg = prop.getString(Aux::Properties::INITIATOR_CFG);
    // whether or not sending pending cancel
    if (prop.isPropertyExists(Aux::Properties::PENDING_CANCEL)) {
        params->sendPendingCancel = prop.getBool(Aux::Properties::PENDING_CANCEL);
    }
    // whether or not sending pending cancel replace
    if (prop.isPropertyExists(Aux::Properties::PENDING_CANCEL_REPLACE)) {
        params->sendPendingCancelReplace = prop.getBool(Aux::Properties::PENDING_CANCEL_REPLACE);
    }
    if (prop.isPropertyExists(Aux::Properties::ROUTER_INSTANCE_NAME)) {
        params->routerName = prop.getString(Aux::Properties::ROUTER_INSTANCE_NAME);
    }
    //virtual destination file
    if (prop.isPropertyExists(Aux::Properties::VIRTUAL_DEST_FILE)) {
        params->virtualDestFile = prop.getString(Aux::Properties::VIRTUAL_DEST_FILE);
    }

    if (prop.isPropertyExists(Aux::Properties::MD_CHECKING_TIME)) {
        params->mdCheckingTime = prop.getInteger(Aux::Properties::MD_CHECKING_TIME);
    }

    if (prop.isPropertyExists(Aux::Properties::ADMIN_PORT)) {
        params->adminPort = prop.getInteger(Aux::Properties::ADMIN_PORT);
    }

    if (prop.isPropertyExists(Aux::Properties::EXTRA_LOGGING))
        params->extraLogging = prop.getBool(Aux::Properties::EXTRA_LOGGING);

    if (prop.isPropertyExists(Aux::Properties::MD_CLIENT_NAME)) {
        params->mdClientName = prop.getString(Aux::Properties::MD_CLIENT_NAME);
    }

    if (prop.isPropertyExists(Aux::Properties::RECOVERY_MODE))
        params->recoveryMode = prop.getBool(Aux::Properties::RECOVERY_MODE);

    if (prop.isPropertyExists(Aux::Properties::IOI_SERVER))
        params->ioiServer = prop.getString(Aux::Properties::IOI_SERVER);
    if (prop.isPropertyExists(Aux::Properties::IOI_PORT)) {
        params->ioiPort = prop.getInteger(Aux::Properties::IOI_PORT);
    }
    // DB parameters
    params->dbHost = prop.getString(Aux::Properties::DB_HOST);
    params->dbPort = prop.getInteger(Aux::Properties::DB_PORT);
    params->dbUser = prop.getString(Aux::Properties::DB_USER);
    params->dbPassword = prop.getString(Aux::Properties::DB_PWD);
    if (prop.isPropertyExists(Aux::Properties::DB_LOGGING))
        params->dbLogging = prop.getBool(Aux::Properties::DB_LOGGING);

    // redline sim
    if (prop.isPropertyExists(Aux::Properties::SIM_MD))
        params->simMD = prop.getBool(Aux::Properties::SIM_MD);

    if (params->simMD) {
        if (prop.isPropertyExists(Aux::Properties::SYMBOL_FILE)) {
            params->symbolFile = prop.getString(Aux::Properties::SYMBOL_FILE);
        }

        if (prop.isPropertyExists(Aux::Properties::SIM_EXCH)) {
            params->exchs = prop.getString(Aux::Properties::SIM_EXCH);
        }
    }

}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo(const string& applicationName)
{
    Helper::writeErrorLine();
    Helper::writeErrorLine("Usage: " + applicationName + " <property file>");
    Helper::writeErrorLine();
}

