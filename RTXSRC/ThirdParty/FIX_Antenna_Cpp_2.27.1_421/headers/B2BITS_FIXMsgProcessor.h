// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXMsgProcessor.h
/// Contains Engine::FIXMsgProcessor class declaration.

#ifndef _B2BITS_FIXMsgProcessor__h_
#define _B2BITS_FIXMsgProcessor__h_

#include "B2BITS_V12_Defines.h"
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_FIXMessage.h"


namespace Engine
{

    /* Forward declaration. */
    class SecurityAttributes;

    /**
    * Processes raw messages and provides
    * message verification capabilities.
    */
    class V12_API FIXMsgProcessor
    {

    public:
        /** List of options available to control message parsing flow. */
        enum Option {
            PROHIBIT_TAGS_WITHOUT_VALUE,    /**< Message is treated as invalid if tag without value appeared. */
            VERIFY_DATA_TAGS_SEQUENCE,      /**< Controls order and length of raw data fields pair verification. */
            VERIFY_TAGS_VALUES,             /**< Enables/disables the verification of tag values  (like date-time). */
            PROHIBIT_UNKNOWN_TAGS,          /**< Controls collecting of unknown tags while message parsing. */
            PROHIBIT_DUPLICATED_TAGS,       /**< Message is treated as invalid if any tag appears more than once. */
            VERIFY_REPERATING_GROUP_BOUNDS, /**< Message is treated as invalid if the quantity of repeating group
                                                 entries is less than specified in the leading field. Message with
                                                 exceeding quantity of repeating group entries is allways treated
                                                 as invalid. */
            CHECK_REQUIRED_TAGS,            /**< Verifies the presence of required and conditionally required
                                                 tags of the message. */
            CHECK_REQUIRED_GROUP_FIELDS,    /**< Verifies the presence of required fields in repeating groups . */
            ALLOW_ZERO_NUMINGROUP,          /**< When true, parser will ignore group tags with 0 values. Exception fired otherwise.*/
            IGNORE_UNKNOWN_FIELDS           /**< When true, parser will ignore unknown fields. */
        };

        /** Option state. */
        enum OptionState {

            ON, /**< Turns option on.  */
            OFF /**< Turns option off. */
        };

        /**
        * Returns the instance of the class.
        */
        static FIXMsgProcessor* singleton();

        /**
        * Duplicates the given message.
        *
        * @param fixMessage message to duplicate
        * @return duplicated FIXMessage
        */
        virtual FIXMessage* clone( const FIXMessage& fixMessage ) const = 0;

        /**
        * Duplicates the given TagValue instance.
        *
        * @param instance TagValue instance to duplicate
        * @return copy of the passed TagValue instance
        */
        virtual TagValue* clone( const TagValue& instance ) const = 0;

        /**
        * Creates corresponding reject for the source message.
        *
        * @param fixMessage message to be rejected
        * @param appReject when true application reject will be created, session when false
        * @return reject FIXMessage
        */
        virtual FIXMessage* createReject( const FIXMessage& fixMessage, bool appReject = true ) const = 0;

        /**
        * Parses the input std::string and creates corresponding structured message.
        *
        * @param rawMessage message to parse.
        * @param rawMessageSize Size of the message to parse.
        * @param pSA pointer to SecurityAttributes class to decrypt input message.
        * This parameter may be NULL if input message wasn't encrypted.
        * @param verifyBodyLength if true then conformance of the input message length
        * to the 'BodyLength' field (tag 9) value will be checked.
        * @param verifyCheckSum if true then conformance of the input message
        * checksum to the 'CheckSum' field (tag 10) value will be checked.
        * @param preferredVersion forces to use specified FIX version instead
        * of using 'BeginString' field (tag 8) value.
        *
        * @warning the user is responsible for the deletion.
        * @return Instance of FIXMessage class.
        */
        virtual FIXMessage* parse( char const* rawMessage,
                                   std::size_t rawMessageSize,
                                   SecurityAttributes* pSA = NULL,
                                   bool verifyBodyLength = false,
                                   bool verifyCheckSum = false,
                                   FIXVersion preferredVersion = NA ) const = 0;

        /**
        * Parses the input std::string and creates corresponding structured message.
        *
        * @param parserID Unique FIX parser identifier of the passed message.
        * @param rawMessage Message to parse.
        * @param rawMessageSize Message size.
        * @param pSA pointer to SecurityAttributes class to decrypt input message.
        * This parameter may be NULL if input message wasn't encrypted.
        * @param verifyBodyLength if true then conformance of the input message length
        * to the 'BodyLength' field (tag 9) value will be checked.
        * @param verifyCheckSum if true then conformance of the input message
        * checksum to the 'CheckSum' field (tag 10) value will be checked.
        * @param preferredVersion forces to use specified FIX version instead
        * of using 'BeginString' field (tag 8) value.
        *
        * @warning the user is responsible for the deletion.
        * @return Instance of FIXMessage class.
        */
        virtual FIXMessage* parse( ParserID parserID,
                                   const char* rawMessage,
                                   std::size_t rawMessageSize,
                                   SecurityAttributes* pSA = NULL,
                                   bool verifyBodyLength = false,
                                   bool verifyCheckSum = false,
                                   FIXVersion preferredVersion = NA ) const = 0;


        /**
        * Parses the input std::string and creates corresponding structured message.
        *
        * @param rawMessage message to parse.
        * @param pSA pointer to SecurityAttributes class to decrypt input message.
        * This parameter may be NULL if input message wasn't encrypted.
        * @param verifyBodyLength if true then conformance of the input message length
        * to the 'BodyLength' field (tag 9) value will be checked.
        * @param verifyCheckSum if true then conformance of the input message
        * checksum to the 'CheckSum' field (tag 10) value will be checked.
        * @param preferredVersion forces to use specified FIX version instead
        * of using 'BeginString' field (tag 8) value.
        *
        * @warning the user is responsible for the deletion.
        * @return Instance of FIXMessage class.
        */
        FIXMessage* parse( const std::string& rawMessage,
                           SecurityAttributes* pSA = NULL,
                           bool verifyBodyLength = false,
                           bool verifyCheckSum = false,
                           FIXVersion preferredVersion = NA ) const {
            return parse( rawMessage.c_str(),
                          rawMessage.size(),
                          pSA,
                          verifyBodyLength,
                          verifyCheckSum,
                          preferredVersion );
        }

        /**
        * Parses the input std::string and creates corresponding structured message.
        *
        * @param parserID Unique FIX parser identifier of the passed message.
        * @param rawMessage message to parse.
        * @param pSA pointer to SecurityAttributes class to decrypt input message.
        * This parameter may be NULL if input message wasn't encrypted.
        * @param verifyBodyLength if true then conformance of the input message length
        * to the 'BodyLength' field (tag 9) value will be checked.
        * @param verifyCheckSum if true then conformance of the input message
        * checksum to the 'CheckSum' field (tag 10) value will be checked.
        * @param preferredVersion forces to use specified FIX version instead
        * of using 'BeginString' field (tag 8) value.
        *
        * @warning the user is responsible for the deletion.
        * @return Instance of FIXMessage class.
        */
        FIXMessage* parse( ParserID parserID,
                           const std::string& rawMessage,
                           SecurityAttributes* pSA = NULL,
                           bool verifyBodyLength = false,
                           bool verifyCheckSum = false,
                           FIXVersion preferredVersion = NA ) const {
            return parse( parserID,
                          rawMessage.c_str(),
                          rawMessage.size(),
                          pSA,
                          verifyBodyLength,
                          verifyCheckSum,
                          preferredVersion );
        }

        /**
         * Checks that the message contains all required and conditionally required fields.
         * @param fixMessage FIXMessage to check.
         * @throw Utils::Exception if any required and conditionally required field is absent.
         */
        virtual void check( const FIXMessage& fixMessage ) const = 0;

        /**
        * Checks that the message contains valid values of fields
        * @param fixMessage FIXMessage to check.
        * @throw Utils::Exception if any field contains invalid value
        */
        virtual void checkFields( const FIXMessage& fixMessage ) const = 0;

        /** Changes state of particular parsing option.
         * @param option Option to change
         * @param state New state
         */
        virtual void switchOption( Option option, OptionState state ) = 0;

        /** Returns the state of particular option.
         * @param option Option to ckeck
         * @return the state of particular option.
         */
        virtual OptionState optionState( Option option ) const = 0;

        /** Fills a message with required fields, blocks and repeating groups.
         * @param fixMessage FIXMessage to fill.
         * @warning Method throws Utils::Exception in case of message or protocol is not defined.
         */
        virtual void fillRequired( FIXMessage& fixMessage ) const = 0;

    protected:
        /// Constructor
        FIXMsgProcessor() {};

        /// Destructor
        virtual ~FIXMsgProcessor();
    };

} // namespace Engine

#endif // #ifndef _FIXMsgProcessor__h_

