// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include <iostream>

#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXGroup.h>
#include <B2BITS_FastCodec.h>
#include <B2BITS_SystemDefines.h>

namespace
{
    // template id, used for encoding fix messages
    const int TEMPLATE_ID = 4;

    // count of fix messages to be encoded/decoded in this example
    const int COUNT_FIX_MESSAGES = 3;
}

void runSample( Engine::FastCodec& fastCodec )
{
    // Create FastCoder
    std::auto_ptr<Engine::FastCoder> fastCoder(
        fastCodec.createFastCoder(
            "../../../../data/fixdict11.xml|../../../../data/fixdic50sp2.xml|additional.xml",
            "templates.xml",
            Engine::FIXT11,
            Engine::FIX50SP2 ) );

    // Create FastDecoder
    std::auto_ptr<Engine::FastDecoder> fastDecoder(
        fastCodec.createFastDecoder(
            "../../../../data/fixdict11.xml|../../../../data/fixdic50sp2.xml|additional.xml",
            "templates.xml",
            Engine::FIXT11,
            Engine::FIX50SP2 ) );

    std::auto_ptr<Engine::FIXMessage> fixMessages[COUNT_FIX_MESSAGES];

    // Create several FIX messages
    for ( int i = 0; i < COUNT_FIX_MESSAGES; ++i ) {
        fixMessages[i] = fastCoder->newSkel( "X" );
        fixMessages[i]->set( FIXFields::ApplVerID, "9" );
        fixMessages[i]->set( FIXFields::SenderCompID, "MICEX" );
        fixMessages[i]->set( FIXFields::MsgSeqNum, i );
        fixMessages[i]->set( FIXFields::SendingTime, Engine::UTCTimestamp::now() );
        fixMessages[i]->set( FIXFields::NoMDEntries, 1 );

        Engine::FIXGroup* grp = fixMessages[i]->getGroup( FIXFields::NoMDEntries );
        Engine::TagValue* entry = grp->getEntry( 0 );
        {
            entry->set( FIXFields::MDEntryType, "0" );
            entry->set( FIXFields::MDEntryTime, Engine::UTCTimeOnly::now() );
            entry->set( FIXFields::MDUpdateAction, '0' );

            char mdEntryId[2] = {0};
            mdEntryId[0] = ( char ) i + '0';
            entry->set( FIXFields::MDEntryID, mdEntryId );

            char symbol[] = "symbol_";
            symbol[sizeof( symbol ) - 2] = ( char ) i + '0';
            entry->set( FIXFields::Symbol, symbol );

            entry->set( FIXFields::RptSeq, i + 10 );
            entry->set( FIXFields::MDEntryPx, Engine::Decimal( i * 10, i % 4 ) );

            entry->set( FIXFields::MDEntrySize, i * 12 );
        }
    }

    Engine::FastCoder::Buffer buffer;
    for ( int i = 0; i < COUNT_FIX_MESSAGES; ++i ) {
        // Print message
        int size( 0 );
        char const* raw = fixMessages[i]->toRaw( &size );

        std::cout << "Sources message: " << Engine::AsciiString( raw, size ) << std::endl;

        try {
            // Check if the specified message can be encoded without errors using the specified template.
            // This method silently returns a control if no errors detected; otherwise throws an exception with detailed information.
            // Usually used for debugging purpose.
            fastCoder->tryEncode( fixMessages[i].get(), TEMPLATE_ID );
        } catch ( Utils::Exception const& error ) {
            std::cerr << "TryEncode Error: " << error.what() << std::endl;
        }

        // Reset the fast coder dictionary (optional)
        fastCoder->resetDictionary();

        // Encode message
        // Node: the specified buffer will be reused.
        fastCoder->encode( fixMessages[i].get(), TEMPLATE_ID, &buffer );

        // Reset the fast decoder dictionary (optional)
        fastDecoder->resetDictionary();

        // Decode the fast message back to fix message
        std::auto_ptr<Engine::FIXMessage> decodedFixMessage( fastDecoder->decode( buffer.getPtr(), buffer.getSize() ) );

        // Print message
        raw = decodedFixMessage->toRaw( &size );
        std::cout << "Decoded message: " << Engine::AsciiString( raw, size ) << std::endl;
    }
}

int main()
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        // Initialize the FastCodec
        Engine::FastCodec fastCodec( "../../../../engine.license" );

        // Rus the sample
        runSample( fastCodec );
    } catch ( Utils::Exception const& error ) {
        std::cerr << "Error: " << error.what() << std::endl;
        return 1;
    }

    return 0;
}
