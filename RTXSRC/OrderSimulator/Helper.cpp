#include "Helper.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>
#include <cstring>
#include <regex>

using namespace ::std;
using namespace ::Aux;

// Writes message to console.
void Helper::writeLineToConsole(const string& message)
{
    //outputLock_.lock();
    clog << message << endl;
    //outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine(const string& message)
{
    cerr << message << endl;
}

void Helper::parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params)
{
    params->initiator_cfg = prop.getString(Aux::Properties::INITIATOR_CFG);
    params->symbol_file = prop.getString(Aux::Properties::SYMBOL_FILE);
    if (prop.isPropertyExists(Aux::Properties::INTERVAL)) {
        params->interval = prop.getInteger(Aux::Properties::INTERVAL);
    }
    if (prop.isPropertyExists(Aux::Properties::MIN_ORDER_SIZE)) {
        params->minOrderSize = prop.getInteger(Aux::Properties::MIN_ORDER_SIZE);
    }
    if (prop.isPropertyExists(Aux::Properties::MIN_ORDER_SIZE)) {
        params->maxOrderSize = prop.getInteger(Aux::Properties::MIN_ORDER_SIZE);
    }
    params->minOrderSize /= 100;
    params->maxOrderSize /= 100;
    params->maxOrderSize -= params->minOrderSize;
    ++params->maxOrderSize;

    if (prop.isPropertyExists(Aux::Properties::BURST_AFTER)) {
        params->burstAfter = prop.getInteger(Aux::Properties::BURST_AFTER);
    }
    
    if (prop.isPropertyExists(Aux::Properties::BURST_TIMES)) {
        params->burstTimes = prop.getInteger(Aux::Properties::BURST_TIMES);
    }
    
    if (prop.isPropertyExists(Aux::Properties::BURST_INTERVAL)) {
        params->burstInterval = prop.getInteger(Aux::Properties::BURST_INTERVAL);
    }

    if (prop.isPropertyExists(Aux::Properties::DESK)) {
        auto values = prop.getString(Aux::Properties::DESK);
        regex re("[,]");
        for(sregex_token_iterator it(values.begin(), values.end(), re, -1),  reg_end;
            it != reg_end; ++it) {
            params->desks.push_back(it->str());
        }
        clog << "Desk has " << params->desks.size() << " desks" << endl;
    }

    if (prop.isPropertyExists(Aux::Properties::LOGIN)) {
        auto values = prop.getString(Aux::Properties::LOGIN);
        regex re("[,]");
        for(sregex_token_iterator it(values.begin(), values.end(), re, -1),  reg_end;
            it != reg_end; ++it) {
            params->logins.push_back(it->str());
        }
        clog << "Login has " << params->logins.size() << " logins" << endl;    }

}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo(const string& applicationName)
{
    Helper::writeErrorLine();
    Helper::writeErrorLine("Usage: " + applicationName + " <property file>");
    Helper::writeErrorLine();
}

