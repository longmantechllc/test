//
// Created by schen on 2/8/2021.
//

#include <iostream>
#include <memory>
#include <thread>
#include <vector>
#include <queue>
#include <map>
#include <chrono>
#include <mutex>
#include <functional>
#include <condition_variable>

//#include <pthread.h>
#include "Logger.h"

#include "xTimer.h"
#include "TimerScheduler.h"

TimerScheduler::TimerScheduler(int numThreads, Logger* log)
   :m_loggerSys(log)
{
    StartTimer(numThreads);
}


TimerScheduler::~TimerScheduler()
{
    close();
    std::cout << "Scheduler std::thread joined successfully \n";
}

void TimerScheduler::StartTimer(int numThreads)
{
    m_loggerSys->logInfo("Start Timer Scheduler, %d threads", numThreads);

    trigger_ready_ = false;
    running_ = true;
    for (auto i = 0; i < numThreads; ++i)
    {
        mThreads.emplace_back([&]()
                              {
                                  run();
                              });
    }
}

void TimerScheduler::run()
{
    std::this_thread::sleep_for(std::chrono::microseconds(100));
    auto beging = std::chrono::system_clock::now();

    while (running_)
    {
        std::shared_ptr<xTimer> tmr = nullptr;
        {
            std::unique_lock<std::mutex> lock(lock_);
            signal_.wait(lock, [&]() {return trigger_ready_ || !running_; });
            {
                if (!running_)
                    return;

                auto itr = mTimerQ.begin();
                if (itr != mTimerQ.end()) {
                    auto now = std::chrono::system_clock::now();
                    auto diff = std::chrono::duration_cast<std::chrono::milliseconds>(itr->first - now).count();
                    if (diff > 2000)
                        sleepTime = 100;
                    else if (diff < 500 && diff > 0)
                        sleepTime = 1;
                    else
                        sleepTime = 10;

                    if (now >= itr->first) {
                        tmr = itr->second;
                        itr = mTimerQ.erase(itr);
                    }
                } else {
                    sleepTime = 100;
                    trigger_ready_ = false;
                }

//                if (tmr != nullptr) {
//                    tmr->m_task(*tmr);
//                    tmr.reset();
//                    tmr = nullptr;
//                    std::this_thread::sleep_for(std::chrono::milliseconds(1));
////                std::this_thread::yield();
//                }
            }
        }
        if (tmr != nullptr)
        {
            tmr->m_task(*tmr);
            tmr.reset();
            tmr = nullptr;
            std::this_thread::sleep_for(std::chrono::milliseconds(1));
//            std::this_thread::yield();
        }
        else
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(sleepTime));
        }
    }
}

void TimerScheduler::insert(std::shared_ptr <xTimer> timer)
{
    std::unique_lock<std::mutex> lock(lock_);

    auto pr = std::make_pair(timer->m_timerId, timer);
    mTimerQ.insert(pr);

    trigger_ready_ = true;
    signal_.notify_all();
}

bool TimerScheduler::isTimerActive(time_point tp)
{
    std::unique_lock<std::mutex> lock(lock_);
    auto itr = mTimerQ.find(tp);
    if (itr != mTimerQ.end())
        return true;
    return false;
}

bool TimerScheduler::remove(time_point tp)
{
    std::unique_lock<std::mutex> lock(lock_);
    auto itr = mTimerQ.find(tp);
    if (itr != mTimerQ.end()) {
        mTimerQ.erase(itr);
        return true;
    }
    return false;
}

void TimerScheduler::close()
{
    running_ = false;
    signal_.notify_all();

    for (auto& thread : mThreads)
        thread.join();
}
