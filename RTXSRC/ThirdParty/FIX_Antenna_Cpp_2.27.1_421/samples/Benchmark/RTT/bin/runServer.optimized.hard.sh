#!/bin/bash

source settings.sh

export Role=server

export EF_UL_EPOLL=1
export EF_EPOLL_MT_SAFE=1
export EF_POLL_USEC=600000
export EF_STACK_PER_THREAD=1
export EF_TCP_FASTSTART_INIT=1
export EF_TCP_FASTSTART_IDLE=1
export EF_DELACK_THRESH=0
export EF_DYNAMIC_ACK_THRESH=0


if [ ! -f /logs.server  ]; then
    mkdir logs.server
fi

if [ ! -f /logs.server/backup  ]; then
    mkdir logs.server/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh server
	if [ "$OutputFile" = "" ]
	then 
		taskset -c $TasksetServerCores numactl -m 1 onload --profile=$OnloadServerProfile ./Server conf engine.optimized.hard.properties
	else
		taskset -c $TasksetServerCores numactl -m 1 onload --profile=$OnloadServerProfile ./Server conf engine.optimized.hard.properties >> $OutputFile
	fi
	sleep 5
done

