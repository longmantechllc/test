/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <cstddef>
#include <cassert>
#include <cstdlib>
#include <fstream>
#include <algorithm>

#include "CMEApplication.h"

#include <B2BITS_FIXEnums.h>

using namespace b2bitscme;

namespace CmeFixFields {
    using namespace FIXFields;

    static const int ApplicationSystemName = 1603;
    static const int TradingSystemVersion = 1604;
    static const int ApplicationSystemVendor = 1605;

	static const int ManualOrderIndicator = 1028;
}

CMEApplication::CMEApplication(Params* params, Utils::Log::LogCategory* logger) 
	: params_(params), logger_(logger) {
		
		if (params_->senderCompID_.empty())
			throw std::runtime_error ("SenderCompId is empty");		

		if (params_->marketSegments_.empty())
			throw std::runtime_error("No market segments are specified");

}

CMEApplication::~CMEApplication() {
	
	dispatcher_.reset();
}

void CMEApplication::attachMessageProcessor(IMessageProcessor* handler) {
	dispatcher_.reset(new MessageDispatcher(handler, params_->processMessagesOutOfOrder_));
}


std::auto_ptr<Engine::FIXMessage> createLogonMessage(const Params& params, SessionPtr session)
{
	if (params.senderCompID_.empty())
		throw std::runtime_error ("SenderCompId is empty");

	if (params.applicationSystemName_.empty())
		throw std::runtime_error ("ApplicationSystemName is empty");

	if (params.tradingSystemVersion_.empty())
		throw std::runtime_error ("TradingSystemVersion is empty");

	if (params.applicationSystemVendor_.empty())
		throw std::runtime_error ("ApplicationSystemVendor is empty");

	if (params.senderLocationID_.empty())
		throw std::runtime_error ("SenderLocationId is empty");
		
	if (params.senderSubID_.empty())
		throw std::runtime_error ("SenderSubId is empty");

	if (params.keysFilePath_.empty())
		throw std::runtime_error("KeysFilePath is empty");
	
	std::auto_ptr<Engine::FIXMessage> msg(session->newSkel("A"));

	msg->set(FIXFields::SenderCompID, params.senderCompID_);

	msg->set(CmeFixFields::ApplicationSystemName, params.applicationSystemName_);
	
	msg->set(CmeFixFields::TradingSystemVersion, params.tradingSystemVersion_);
	
	msg->set(CmeFixFields::ApplicationSystemVendor, params.applicationSystemVendor_);

    msg->set( CmeFixFields::SenderLocationID, params.senderLocationID_ );
    
	msg->set( CmeFixFields::SenderSubID, params.senderSubID_ );
    
	return msg;
}


void CMEApplication::beginWeekLogon(SessionPtr session, const std::string& host) {
	// This method performs Begin Week Logon
	// It is the first Logon the client sends for the week
	// Client systems must set their inbound and outbound sequence numbers to '1' prior to the Beginning of Week Logon for a successful logon.
	// More information can be found here: 
	// http://www.cmegroup.com/confluence/display/EPICSANDBOX/Drop+Copy+Session+Layer+-+Logon

	// Setting inbound and outbound sequence numbers to '1' for a successful logon
	session->resetSeqNum(Engine::Session::RESET_SEQNUM_STRATEGY);

	std::auto_ptr<Engine::FIXMessage> msg = createLogonMessage(*params_, session);

	msg->set(FIXFields::ResetSeqNumFlag, "N");
	
	session->connect(
            30,																				 // Heartbeat interval
            *msg,																			 // custom Logon message
			host,																			 // host
            params_->port_																	 // port
    );

	
}

void CMEApplication::midWeekLogon(SessionPtr session, const std::string& host) {
	// This method performs Mid-Week Logon
	// It is used for any subsequent logons, after the beginning of the week.
	// Following mid-week log off, the client system logs in mid-week with the next sequential outbound message sequence number.
	// Mid-Week Logon provides handling for undelivered messages which were sent while the client system was logged out.
	// More information can be found here: 
	// http://www.cmegroup.com/confluence/display/EPICSANDBOX/Drop+Copy+Session+Layer+-+Logon


	std::auto_ptr<Engine::FIXMessage> msg = createLogonMessage(*params_, session);
		
	msg->set(FIXFields::ResetSeqNumFlag, "N");
		
	session->connect(
            30,																				 // Heartbeat interval
            *msg,																			 // custom Logon message
			host,																			 // host
            params_->port_																	 // port
    );

	
}

void CMEApplication::inSessionLogon(SessionPtr session) {
	// This method performs In-Session Logon
	// It is used to reset sequence numbers and to recover from catastrophic failure.
	// More information can be found here: 
	// http://www.cmegroup.com/confluence/display/EPICSANDBOX/Drop+Copy+Session+Layer+-+Logon

	// NOTE: The client system must send a Test Request (tag 35-MsgType=1) message before sending an In-Session Logon (tag 35-MsgType=A) message. 
	// If not sent in that order, the client system may lose messages that cannot be requested again as the sequence number may be reset to '1' for both parties, client and Drop Copy.
		
	session->resetSeqNum(Engine::Session::RESET_SEQNUM_AND_SEND_LOGON_STRATEGY);
}

bool CMEApplication::process(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
   
	if (NULL != dispatcher_.get()) {
		dispatcher_->dispatchMessage(fixMsg, aSn);
	}
			
    // Return 'true' if the application can process the given message, otherwise 'false'.
    // If the application can not process the given message,
    // the FIX Engine will try to deliver it later according to "Delayed Processing Algorithm".
    return true;

}

void CMEApplication::onLogonEvent(const Engine::LogonEvent* /*apEvent*/, const Engine::Session& aSn) {
    // This call-back method is called to notify that the Logon message has been received from the counterpart.
    if (logger_)
        logger_->note( "Logon received for the session " + *aSn.getId());

}

void CMEApplication::onLogoutEvent(const Engine::LogoutEvent* apEvent, const Engine::Session& aSn) {
    // This call-back method is called to notify that the Logout message has been received from the counterpart or the session was disconnected.
    if (logger_)
        logger_->note("Logout received for the session " + *aSn.getId());
	
	if (!apEvent->m_pLogoutMsg)
		return;
	
	handleLogout(*apEvent->m_pLogoutMsg, aSn);	

}

void CMEApplication::sendMessage(SessionPtr session, const std::string& message, bool isManual){

	std::auto_ptr<Engine::FIXMessage> msg;
	
    try {
		 msg.reset( Engine::FIXMsgProcessor::singleton()->parse(session->parserID(), message));
    } catch( std::exception const& e ) {
		if (logger_)
			logger_->note("  ERROR: Cannot parse message: " + std::string(e.what()));
        return;
    }

	if (msg->isSupported(CmeFixFields::ManualOrderIndicator ))
	{

		if( !msg->hasValue( CmeFixFields::ManualOrderIndicator ) ) 
		{
			if (isManual)
				msg->set( CmeFixFields::ManualOrderIndicator, "Y" );
			else
				msg->set(CmeFixFields::ManualOrderIndicator, "N");
		}
	} 

    session->put( msg.get());
    
	if (logger_)
		logger_->note("  Message is successfully scheduled for sending.");

}



void CMEApplication::onInSeqNumChanged(int oldValue, int newValue, const  Engine::Session& sn)
{
	if (NULL != dispatcher_.get()) {
		dispatcher_->onInSeqNumChanged(oldValue, newValue, sn);
	}
}

   
void CMEApplication::onAfterMessageIsParsed(Engine::FIXMessage& msg, const Engine::Session& sn)
{
	if (msg.type() == "5"){
		handleLogout(msg, sn);	
	}
}

void CMEApplication::handleLogout(const Engine::FIXMessage& msg, const Engine::Session& sn)
{
	B2B_USE_ARG(sn);

	Engine::FIXFieldValue val;
	// Get the value of the NextExpectedMsgSeqNum tag and display it for user
	if (msg.get(FIXField::NextExpectedMsgSeqNum, &val)) 
	{
		if (logger_)	
			logger_->note("The value of the NextExpectedMsgSeqNum tag is " + std::string(val.data_, val.length_));
	}

}