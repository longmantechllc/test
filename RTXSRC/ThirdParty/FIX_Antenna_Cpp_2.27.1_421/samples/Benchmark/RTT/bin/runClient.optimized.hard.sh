#!/bin/bash

source settings.sh

export ListenPort=65535
export Role=client

export EF_UL_EPOLL=1
export EF_EPOLL_MT_SAFE=1
export EF_POLL_USEC=600000
export EF_STACK_PER_THREAD=1
export EF_TCP_FASTSTART_INIT=1
export EF_TCP_FASTSTART_IDLE=1
export EF_DELACK_THRESH=0
export EF_DYNAMIC_ACK_THRESH=0


if [ ! -f /logs.client  ]; then
    mkdir logs.client
fi

if [ ! -f /logs.client/backup  ]; then
    mkdir logs.client/backup
fi

RunningsCount=$1
OutputFile=$2

if [ "$RunningsCount" = "" ]; then
    RunningsCount=1
fi

for i in `seq 1 $RunningsCount`
do
	# Cleanup logs
	sh clean.sh client
	
	if [ "$OutputFile" = "" ]
	then 
		taskset -c $TasksetClientCores numactl -m 1 onload --profile=$OnloadClientProfile ./Client $ServerAdress $ServerPort conf engine.optimized.hard.properties
	else
		taskset -c $TasksetClientCores numactl -m 1 onload --profile=$OnloadClientProfile ./Client $ServerAdress $ServerPort conf engine.optimized.hard.properties >> $OutputFile
	fi
	sleep 10
done
