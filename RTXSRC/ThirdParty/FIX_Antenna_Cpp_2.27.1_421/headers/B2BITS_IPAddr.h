// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_IPAddr.h
/// Contains System::IPAddr class definition

#ifdef HAVE_PRAGMA_ONCE
#   pragma once
#endif // HAVE_PRAGMA_ONCE

#ifndef H_B2BITS_SYSTEM_IPADDR_H
#define H_B2BITS_SYSTEM_IPADDR_H

#include <ostream>
#include <vector>
#include <map>
#include "B2BITS_IntDefines.h"
#include <B2BITS_ReferenceCounter.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
// Unreferenced parameter
#   pragma warning(disable: 4100)
#endif

namespace System
{
    class Mutex;
    /**
     * Represents IP address
     */
    struct IPAddr {
        /** Part of the IP address.
         * @note Field order is important. Please do not change it.
         */
        System::u8 a1, a2, a3, a4;

        /** Compares two IPAddr instances. Returns true, if instances are equal; false otherwise. */
        bool operator==( IPAddr const& rv ) const {
            return
                this->a1 == rv.a1 &&
                this->a2 == rv.a2 &&
                this->a3 == rv.a3 &&
                this->a4 == rv.a4;
        }

        /** Compares two IPAddr instances. Returns true, if instances are NOT equal; false otherwise. */
        bool operator !=( IPAddr const& rv ) const {
            return !( *this == rv );
        }

        /** Constructs IPAddr instance using IP address parts. */
        static IPAddr create( System::u8 a1, System::u8 a2, System::u8 a3, System::u8 a4 ) throw() {
            IPAddr result = { a1, a2, a3, a4 };
            return result;
        }

        /** Constructs IPAddr instance using IPv4 address. */
        static IPAddr create( System::u32 addr ) throw() {
            union {
                IPAddr addr;
                System::u32 u32;
            } result;

            result.u32 = addr;

            return result.addr;
        }
    }; // struct IPAddr

    inline std::ostream& operator << ( std::ostream& s, const IPAddr& v )
    {
        return s << (int)v.a1 << '.' << (int)v.a2 << '.' << (int)v.a3 << '.' << (int)v.a4;
    }

    /**
    * Defines encoding type of the certificate contained inside SSLCertificateDataRecord
    */
    enum SSLCertificateEncoding
    {
        CERT_ENCODING_NA = 0,
        CERT_ENCODING_PEM = 1,
        CERT_ENCODING_ASN1 = 2,
        CERT_ENCODING_PFX = 3
    };
    
    /**
    * SSLContext configurator class. SSLContext uses this class instance to setup SSL configuration (certificates, private keys, cipchers and so on).
    */
    class V12_API SSLContextConfigurator : public virtual Utils::ReferenceCounter
    {
    public:
        typedef std::vector<char> ConfigData;
        /**
        * Certificate data record contiaining certificate data along with encoding.
        */
        struct SSLCertificateDataRecord
        {
            SSLCertificateDataRecord();
            SSLCertificateEncoding encoding;
            ConfigData certificateData;
        };
        /**
        * Array of certificates descriptions represented as name/value pair. So each array(vector) item(map) is a certificate description made with name/value pairs provided.
        * See https://www.openssl.org/docs/man1.0.2/crypto/X509_NAME_add_entry_by_txt.html for details
        * This structure is used along with getClientCAsList() callback in order to populate acceptable certificates list passed to client if peer validation is enabled.
        */
        typedef std::vector<std::map<std::string, std::string> > CANames;

    public:
        /*
        * Object constructor
        */
        SSLContextConfigurator();

        /*
        * Object destructor
        */
        virtual ~SSLContextConfigurator();

        /**
        * Resets configurator internal state if any.
        * This routine is called right before any other routines are called to give configurator a chance to reset its internal state if any from previous configuration rounds
        * and prepare to new one since configurator instance can be reused accross different SSL contexts.
        */
        virtual void reset() {};

        /**
        * Returns protocols value to use. See Protocol_* constants. 
        * If 0 is returned value passed directly to SSLContext constructor is used.
        */
        virtual int getProtocols() { return 0; };

        /**
         * Provides location where to search for CA certificates.
         * It is called by both Client and Server contexts. It is used to search for root CA certificates during peer certificate validation.
         * @param[out] caFile - keeps file path containing CA certificates on routine return.
         * @param[out] caPath - keeps CA certificates path on routine return. See https://www.openssl.org/docs/man1.0.2/ssl/SSL_CTX_load_verify_locations.html for details.
         * @param[out] useCAFileAsAcceptedCertificatesList - when true certificates from caFile will be used to build list of acceptable CA certificates sent to client. Applicable to server context only.
         * Returns true if at least one(caFile or caPath) is provided, false otherwise
         */
        virtual bool getVerifyLocations( std::string& caFile, std::string& caPath, bool& useCAFileAsAcceptedCertificatesList ) { return false; };       

        /**
        * Provides next certificate to set.
        * It is called by both Client and Server contexts.
        * Certificate matching against private key is performed for every certificate provided and exception is thrown if mismatch is detected.
        * @param[in,out] index - tracking index, 0 is passed at first call after reset() to help trivial implementations track current certificate,
        *                configurator is free to use this index whatever it wants. It helps configurator to track what certificate is next.
        *                Index value on return is passed into password callback routine for returned certificate/key pair as is.
        * @param[out] certificate - certificate record decribing the certificate to set.
        * @param[out] certificateChain - additional certificate chain to set for the certificate. See https://www.openssl.org/docs/man1.0.2/ssl/SSL_CTX_add1_chain_cert.html for details.
        * @param[out] privateKeyData - private key corresponding to the certificate provided.
        * Returns true if next certificate data is provided, false otherwise.
        */
        virtual bool getNextCertificatePrivateKeyPair( int& index, SSLCertificateDataRecord& certificate, std::vector<SSLCertificateDataRecord>& certificateChain, SSLCertificateDataRecord& privateKeyData ) { return false; };
        
        /**
        * Returns true if peer certificate has to be validated, false otherwise.
        */
        virtual bool isValidatePeerCertificate() { return false; };

        /**
        * Returns ciphers configuration string. See https://www.openssl.org/docs/man1.0.2/apps/ciphers.html for details.
        * If empty string is returned cipher's list remains untouched and OpenSSL default list is used.
        */
        virtual std::string getCiphersList() { return ""; };
        
        /**
        * This routing is called when password for private key or certificate is required.
        * Certificate matching against private key set before is performed for every certificate provided.
        * @param[in] index - tracking index of certificate/key pair or -1 if password for CA certificate from getClientCAsList is requested. 
        * @param[in] arrayIndex - index of password requested - -1 for private key, 0 for certificate, 1,2,3,... for every certificate from certificate chain array if password is required.
        * @param[in] maxPasswordSize - maximal password length accepted. Return password should not be bigger the this value. All characted above this value will be truncated!
        * Returns password requested or empty string if no password is provided.
        */
        virtual std::string passwordCallback( int index, int arrayIndex, int maxPasswordSize ) { return ""; };
        
        // Server side specific
        /**
        * Provides data to build acceptable CAs list sent to client.
        * The same list is set to all CAs from caFile if 'getVerifyLocations' sets 'useCAFileAsAcceptedCertificatesList' to true;
        * @param[out] caNames - array of CA names to add.
        * @param[out] accaptableCACertificates - array of CA cartificates that names are extracted and than add. 
        * Returns true if acceptable CAs list has to be updated, false otherwise.
        */
        virtual bool getClientCAsList( CANames& caNames, std::vector<SSLCertificateDataRecord>& accaptableCACertificates ) { return false; };

        /*
        * This routine gets the lock over the object and have to be called when exclusive access to the configurator instance is required.
        * SSLContext contstructor calls this routine right before reset() routine is called to get exclusive access and avoid concurent access the the same instance.
        * All configuration calls are made inside SSLContex constructor. So lock time is short. No furter calls (outside constructor) are made.
        * The reference to the configurator object is kept for further serialization when needed.
        */
        void lock();

        /*
        * This routine releases the lock over the object and have to be called when no further exclusive access to the configurator instance is required.
        * SSLContext contstructor calls this routine right after it finishes configuration process.
        */
        void unlock();

    private:
        /*
        * Mutex guard object pointer. This pointer is accessed by lock()/unlock() routines only and so made private.
        */
        System::Mutex* pLockMutex_;
    };

    typedef Utils::ReferenceCounterSharedPtr<SSLContextConfigurator> SSLContextConfiguratorPtr;

    /**
     * Contains SSL specific information that is used while establishing connection.
     */
    class V12_API SSLContext
    {
    public:
        static const int Protocol_SSLv2 = 1;
        static const int Protocol_SSLv3 = 2;
        static const int Protocol_TLSv1 = 4;
        static const int Protocol_TLSv1_1 = 8;
        static const int Protocol_TLSv1_2 = 16;
        static const int Protocol_ALL_SSL = Protocol_SSLv2 | Protocol_SSLv3;
        static const int Protocol_ALL_TLS = Protocol_TLSv1 | Protocol_TLSv1_1 | Protocol_TLSv1_2;
        static const int Protocol_ALL = Protocol_ALL_SSL | Protocol_ALL_TLS;

        /** Type of the context contained. */
        enum ContextType
        {
            ContextType_Undefined,
            ContextType_Server,
            ContextType_Client
        };
    protected:
        void* sslCtx_;
        ContextType ctxType_;
        SSLContextConfiguratorPtr configurator_;
    public:
        SSLContext();
        virtual ~SSLContext();

        SSLContext( SSLContext const& src);
        SSLContext& operator=( SSLContext const& src);

        void *get() const;
        ContextType getContextType() const;
        template<typename T> T* getTyped() const
        {
            return reinterpret_cast<T *>(get());
        }
        
        /** Returns true if context contains server side information. */
        bool inline isServerContext() const
        {
            return ctxType_ == ContextType_Server;
        }

        /** Returns true if context contains client side information. */
        bool inline isClientContext() const
        {
            return ctxType_ == ContextType_Client;
        }

        /** 
         * Returns true if context contains client or server side information.
         * True means that connection created with this context must and will be secured.
         */
        bool inline isSecuredContext() const
        {
            return ctxType_ != ContextType_Undefined;
        }

        /**
        * Returns SSLContextConfigurator pointer if any.
        */
        const SSLContextConfiguratorPtr getConfigurator() const;
    protected:
        SSLContext(ContextType ctxType, int protocols = Protocol_ALL, const SSLContextConfiguratorPtr& configurator = SSLContextConfiguratorPtr() );

    public:
        // info fields
        int protocols_;
        std::string ciphersList_;
        std::string getStringProtocols() const;
    };

    /**
     * Client specific SSL context. This context type is used to establish secured connection from client side.
     */
    class V12_API SSLClientContext: public SSLContext
    {
    public:
        /** 
         * Creates client specific context.
         * @param protocols - defines which protocols are allowed to use.
         * @param configurator - custom SSL context configurator.
         */
        SSLClientContext(int protocols = Protocol_ALL, const SSLContextConfiguratorPtr& configurator = SSLContextConfiguratorPtr() );
    };

    /**
     * Server specific SSL context. This context type is used to establish secured connection from server side.
     * This class can't be created by user. Use FAProperties::getSSLServerContext() to get the instance.
     * FAProperties::getSSLServerContext() returns the instance populated with the same data as used with SSL server socket by the engine.
     */
    class V12_API SSLServerContext: public SSLContext
    {
    protected:
        SSLServerContext(int protocols = Protocol_ALL, const SSLContextConfiguratorPtr& configurator = SSLContextConfiguratorPtr() );
    };

} // namespace System {

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // H_B2BITS_SYSTEM_IPADDR_H

