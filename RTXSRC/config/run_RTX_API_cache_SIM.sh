## This scripts will run RTX IOI Simulator. 

cd /home/hjoshi/RTX_DEMO/RTX_API_SIMULATOR/store
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_API_SIMULATOR/logs
rm * 2>&1 | grep -v 'cannot remove .*: Is a directory'

cd /home/hjoshi/RTX_DEMO/RTX_API_SIMULATOR/

## set core dump
ulimit -S -c unlimited > /dev/null 2>&1 

#set path
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib

# Run RTX IOI Simulator
./RTX_APIcacheSimulator APIcacheSimulator.cfg
