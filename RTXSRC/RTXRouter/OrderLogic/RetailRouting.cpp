//
// Created by schen on 4/27/2021.
//

#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "RTXUtils.h"
#include "Logger.h"
#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"
#include "../include/IOIDataIO.h"
#include "../include/App.h"
#include "CPU.h"

#include "_DataStruct/TOBQuote.h"
#include "MarketData/RetailMarket.h"
#include "RTXOrder.h"
//#include "IOrderLogicBase.h"

#include "RetailRouting.h"


RetailRouting::RetailRouting(RTXOrder* order)
        : IOrderLogicBase { order }
{
    if(m_order->m_userConfig->retailUseOrderLimitPrice >= 0) {
        if(m_order->m_userConfig->retailUseOrderLimitPrice == 1)
            m_useOrderLimitPrice = true;
        else
            m_useOrderLimitPrice = false;
    }
    else if(m_order->m_accountConfig->retailUseOrderLimitPrice >= 0) {
        if(m_order->m_accountConfig->retailUseOrderLimitPrice == 1)
            m_useOrderLimitPrice = true;
        else
            m_useOrderLimitPrice = false;
    }
}

RetailRouting::~RetailRouting()
{
}

bool RetailRouting::InitOrder()
{
    if(m_isInited)
        return true;
    if(m_isDone || !m_order->m_isRetailSWPDone)
        return false;
    if(!m_order->m_validBbo && !m_useOrderLimitPrice)
    {
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        m_order->m_logger->logInfo("oid:%d, No nbbo received. No Retail Routing", m_order->m_clientOrder->orderID_);
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No nbbo received. No Retail Routing.");

        return true;
    }
    if(m_order->m_userConfig->retailBDCM)
        m_bdcm = m_order->m_userConfig->retailBDCM;
    else if(m_order->m_accountConfig->retailBDCM)
        m_bdcm = m_order->m_accountConfig->retailBDCM;
    else
        m_bdcm = 1;
//    m_order->m_logger->logInfo("Retail Route oid:%d, m_bdcm:%d.", m_order->m_clientOrder->orderID_, m_bdcm);

    m_mmidRouteMap = m_order->m_config->GlobalConfig()->retailRouterMMIDmap;
    m_isInited = true;

    if(!ProcessMktData())
        return false;

    return true;
}

bool RetailRouting::ProcessOrder()
{
    if(!IsMarketable())
    {
        if(m_useOrderLimitPrice && m_isFirstTimeRetailRouting)
        {
            m_isFirstTimeRetailRouting = false;
            return true;
        }
        return false;
    }

    std::string routeName;
    std::string mmid;
    auto itr = m_contraQuotes.begin();
    while (itr != m_contraQuotes.end()) {
        auto rName = m_mmidRouteMap[(*itr).mmid];
        mmid = (*itr).mmid;
//        cout << "mmid:" << (*itr)->mmid << ", rName:" << rName << endl;
        itr = m_contraQuotes.erase(itr);
        if (!IsRouteMarketable(rName)) {
            m_order->m_logger->logInfo("iod:%d. Retail Route:%s (mmid:%s) is not eligible at this time.",
                                       m_order->m_clientOrder->orderID_, rName.c_str(), mmid.c_str());
            continue;
        }
        else {
            routeName = rName;
            break;
        }
    }
//    std::cout << "m_contraQuotes.size:" <<  m_contraQuotes.size()<< std::endl;
    if(routeName.empty()) {
        m_order->m_logger->logInfo("iod:%d, No eligible contra quote available, skip Retail Routing.", m_order->m_clientOrder->orderID_);
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        if(m_isFirstTimeRetailRouting)
        {
            if(m_useOrderLimitPrice) {
                m_isFirstTimeRetailRouting = false;
                return true;
            }
            if(m_order->m_isSWPDone)
            {
                if(m_order->m_tobList.GetTobList().empty())
                    m_order->StartMarketDataFailureTimeOutTimerTarg(20);
                return true;
            }
        }
        return false;
    }
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    auto cOrder = m_order->CreateChildOrder();
    cOrder->size_ =  m_order->QtyUncommited();
    cOrder->TIF_ = route->tif;
    cOrder->minQty_ = 0;
    cOrder->exchange_ = route->venue;
    cOrder->routeName_ = routeName;
    if(!m_useOrderLimitPrice)
    {
        cOrder->type_ = OrderType::TYPE_LIMIT;
        auto px = m_order->NBBO();
        cOrder->price_ = px;
        Price offset = route->aggressiveOffset;
        if (offset > Price(0.0)) {
            if (m_order->IsBuyOrder()) {
                cOrder->price_ = px + offset;
                if ((cOrder->price_).isGE(m_order->EffectivePrice()))
                    cOrder->price_ = m_order->EffectivePrice();
            } else {
                cOrder->price_ = px - offset;
                if ((cOrder->price_).isLE(m_order->EffectivePrice()))
                    cOrder->price_ = m_order->EffectivePrice();
            }
        }
        m_order->m_logger->logInfo("oid:%d, Limit px: %.2f, NBBO %.2f, RetailRouting", m_order->m_clientOrder->orderID_, m_order->EffectivePrice().ToDouble(), m_order->NBBO().ToDouble());
    }
    SendChildOrder(cOrder);
    m_isFirstTimeRetailRouting = false;
}

bool RetailRouting::SendChildOrder(std::shared_ptr <Order> order)
{
    m_order->SendChildOrder(order, ChildOrderType::CHILD_ORDER_TYPE_RETAIL);
    return false;
}

bool RetailRouting::ProcessChildOrderER(const ExecutionReport *er, std::shared_ptr <ChildOrder> &child) {
//    if (m_isDone == true)
//        return false;
    if (child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_RETAIL)
        return false;

//    std::cout << "ProcessChildOrderER, m_contraQuotes.size:" <<  m_contraQuotes.size() << std::endl;
    if(m_contraQuotes.empty())
    {
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No more contraQuote. No Retail Routing.");
        return false;
    }
    return false;
}

bool RetailRouting::ProcessMktData()
{
    if(m_order->m_config->GlobalConfig()->retailRouterIoiNormalizerCache)
    {
        char lineBuffer[256];
        std::string msg;
        m_contraQuotes = m_order->m_app->GetIOIDataIO()->SubscribeIOI(m_order->m_clientOrder->symbol_, !m_order->IsBuyOrder());

        sprintf(lineBuffer,"oid:%d, Received API cache snapshot, %s: ", m_order->m_clientOrder->orderID_, m_order->m_clientOrder->symbol_.c_str());
        msg += lineBuffer;
//      m_order->m_logger->logInfo("oid:%d, Received ioi normalizer cache, %s:", m_order->m_clientOrder->orderID_, m_order->m_clientOrder->symbol_.c_str());

        for(auto& data : m_contraQuotes)
        {
            sprintf(lineBuffer, "%s,%.04f,%.04f,%d,%ld| ", data.mmid.c_str(), data.pi.ToDouble(), data.pfof.ToDouble(), data.size, data.timeStamp);
            msg += lineBuffer;
//            m_order->m_logger->logInfo("oid:%d, %s,%.04f,%.04f,%d,%ld", m_order->m_clientOrder->orderID_, data.mmid.c_str(), data.pi.ToDouble(), data.pfof.ToDouble(), data.size, data.timeStamp);
        }
        m_order->m_logger->logInfo("%s", msg.c_str());
    }
    else
    {
        auto data = m_order->m_cpu->m_retailMarketBox->GetRetailMarketList(m_order->IsBuyOrder()).find(
                m_order->m_clientOrder->symbol_);
        if (data != m_order->m_cpu->m_retailMarketBox->GetRetailMarketList(m_order->IsBuyOrder()).end())
            m_contraQuotes = data->second;
    }
    auto contraQuotesConfig = m_order->m_config->GlobalConfig()->retailRouterMMIDpIpfoFmap;
    for(auto& temp : contraQuotesConfig)
    {
        auto mmid = temp.second->mmid;
        auto itr = std::find_if(m_contraQuotes.begin(), m_contraQuotes.end(), [&](auto& q){return (q.mmid == mmid);});
        if(itr == m_contraQuotes.end())
        {
//            auto str = std::make_shared<IOIMarketData>(mmid, temp.second->pfoF, temp.second->pI, 0, temp.second->priority, 0);
            auto data = IOIMarketData();
            data.mmid = mmid;
            data.pi = temp.second->pI;
            data.pfof = temp.second->pfoF;
            data.size = 0;
            data.priority = temp.second->priority;
            data.timeStamp = 0;
            m_contraQuotes.emplace_back(data);
        }
    }
    
    std::string msg = "Remove contra quote reason: ";
    std::string mmid;
    bool gotData = false;
    auto q = m_contraQuotes.begin();
    while(q != m_contraQuotes.end())
    {
        mmid = (*q).mmid;
        msg += mmid;
//        if((*q).size == 0 && (*q).priority == 0)
//        {
//            q = m_contraQuotes.erase(q);
//            msg += " Size=0; ";
//            gotData = true;
//            continue;
//        }
        if((*q).timeStamp == 0 && (*q).priority == 0)
        {
            q = m_contraQuotes.erase(q);
            msg += " TimeStamp=0; ";
            gotData = true;
            continue;
        }
        if(m_order->m_userConfig->excludeRetailMmidList.find(mmid) != m_order->m_userConfig->excludeRetailMmidList.end()
           || m_order->m_accountConfig->excludeRetailMmidList.find(mmid) != m_order->m_accountConfig->excludeRetailMmidList.end())
        {
            q = m_contraQuotes.erase(q);
            msg += " excludeMmid; ";
            gotData = true;
            continue;
        }
        if((*q).pi < m_order->m_userConfig->retailMPI || (*q).pi < m_order->m_accountConfig->retailMPI)
        {
            q = m_contraQuotes.erase(q);
            msg += " MPI; ";
            gotData = true;
            continue;
        }
        if((*q).pfof < m_order->m_userConfig->retailMPFOF || (*q).pfof < m_order->m_accountConfig->retailMPFOF)
        {
            q = m_contraQuotes.erase(q);
            msg += " MPFOF; ";
            gotData = true;
            continue;
        }
        msg += " -; ";
        ++q;
    }
    auto BDCM1Cmp = [&](auto &q1, auto &q2) {
        if (q1.pi + q1.pfof > q2.pi + q2.pfof) return true;
        else if (q1.pi + q1.pfof < q2.pi + q2.pfof) return false;
        if (q1.priority > q2.priority) return true;
        else if (q1.priority < q2.priority) return false;

        if (q1.timeStamp < q2.timeStamp) return true;
        else return false;
    };
    auto BDCM2Cmp = [&](auto &q1, auto &q2) {
        if (q1.pi > q2.pi) return true;
        else if (q1.pi < q2.pi) return false;
        if (q1.priority > q2.priority) return true;
        else if (q1.priority < q2.priority) return false;
        if (q1.timeStamp < q2.timeStamp) return true;
        else return false;
    };
    auto BDCM3Cmp = [&](auto &q1, auto &q2) {
        if (q1.pfof > q2.pfof) return true;
        else if (q1.pfof < q2.pfof) return false;
        if (q1.priority > q2.priority) return true;
        else if (q1.priority < q2.priority) return false;
        if (q1.timeStamp < q2.timeStamp) return true;
        else return false;
    };
    int piWeight = 1;
    int pfofWeight = 1;
    if(m_bdcm == 4) {
        if (m_order->m_userConfig->retailpIWeight != 1)
            piWeight = m_order->m_userConfig->retailpIWeight;
        else if (m_order->m_accountConfig->retailpIWeight != 1)
            piWeight = m_order->m_accountConfig->retailpIWeight;
        if (m_order->m_userConfig->retailpfoFWeight != 1)
            pfofWeight = m_order->m_userConfig->retailpfoFWeight;
        else if (m_order->m_accountConfig->retailpfoFWeight != 1)
            pfofWeight = m_order->m_accountConfig->retailpfoFWeight;
    }
    auto BDCM4Cmp = [&](auto &q1, auto &q2) {
        if (Price((q1.pi.value() * piWeight) + (q1.pfof.value() * pfofWeight)) > Price((q2.pi.value() * piWeight) + (q2.pfof.value() * pfofWeight))) return true;
        else if (Price((q1.pi.value() * piWeight) + (q1.pfof.value() * pfofWeight)) < Price((q2.pi.value() * piWeight) + (q2.pfof.value() * pfofWeight))) return false;
        if (q1.priority > q2.priority) return true;
        else if (q1.priority < q2.priority) return false;

        if (q1.timeStamp < q2.timeStamp) return true;
        else return false;
    };

    switch (m_bdcm)
    {
        case 1:
            std::sort(m_contraQuotes.begin(), m_contraQuotes.end(), BDCM1Cmp);
            break;
        case 2:
            std::sort(m_contraQuotes.begin(), m_contraQuotes.end(), BDCM2Cmp);
            break;
        case 3:
            std::sort(m_contraQuotes.begin(), m_contraQuotes.end(), BDCM3Cmp);
            break;
        case 4:
            std::sort(m_contraQuotes.begin(), m_contraQuotes.end(), BDCM4Cmp);
            break;
        default:
            std::sort(m_contraQuotes.begin(), m_contraQuotes.end(), BDCM1Cmp);
            break;
    }
    if(gotData)
    {
        m_order->m_logger->logInfo("oid:%d, RetailRouter m_bdcm:%d, %s contraQuotes.size:%d.", m_order->m_clientOrder->orderID_, m_bdcm, msg.c_str(), m_contraQuotes.size());
    }

////// Print ioi data
//    std::cout << "Msg:" << msg << ", m_contraQuotes.size:" << m_contraQuotes.size() <<std::endl;
//    std::cout << "retailBDCM:" << m_bdcm << std::endl;
//    std::cout << "piWeight:" << piWeight  << ", pfofWeight:" << pfofWeight << std::endl;
//    for(auto& q : m_contraQuotes)
//    {
//        std::cout << "mmid:" << q.mmid << ", (1)pi+pfof:" << q.pi.ToDouble()+q.pfof.ToDouble() << ", (2)pi:" << q.pi.ToDouble()
//                << ", (3)pfof:" << q.pfof.ToDouble() << ", (4)(pi* piWeight)+(pfof * pfofWeight):" << (q.pi.ToDouble() * piWeight) + (q.pfof.ToDouble() * pfofWeight)
//                << ", priority:" << q.priority << ", size:" << q.size << ", timeStamp:" << q.timeStamp << std::endl;
//    }
//////
    if(m_contraQuotes.empty())
    {
        m_order->m_logger->logInfo("RetailRouter oid:%d, no contra quote for symbol:%s .", m_order->m_clientOrder->orderID_, m_order->m_clientOrder->symbol_.c_str());
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No eligible contraQuote. No Retail Routing.");
        if(m_order->m_isSWPDone)
        {
            if(m_order->m_tobList.GetTobList().empty())
                m_order->StartMarketDataFailureTimeOutTimerTarg(20);
            return false;
        }
    }
    return true;
}

bool RetailRouting::IsEligible()
{
//    if(m_order->m_orderIsCancelling)
//        return false;
//    if(m_isDone || !m_order->m_isRetailSWPDone)
    if(m_isDone)
        return false;
    if(m_order->QtyUncommited() <= 0)
        return false;
    if(m_contraQuotes.empty())
    {
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No contraQuote. No Retail Routing.");
        return false;
    }
    return true;
}

bool RetailRouting::IsMarketable()
{
    if(!IsEligible())
        return false;
    if(m_useOrderLimitPrice)
        return true;

//    if(m_order->m_marketDataFailureTimeOutPost)
//    {
//        m_isDone = true;
//        m_order->m_isRetailRouting = false;
//        return false;
//    }
    if(!m_order->m_validBbo)
    {
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        m_order->m_logger->logInfo("oid:%d, No nbbo received. No Retail Routing", m_order->m_clientOrder->orderID_);
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No nbbo received. No Retail Routing.");

        return false;
    }
    if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
        return true;
    if(!m_order->IsMarketable(m_order->EffectivePrice(), m_order->NBBO()))
    {
        m_isDone = true;
        m_order->m_isRetailRouting = false;
        m_order->m_logger->logInfo("oid:%d, EffectivePrice:%0.4f, nbbo:%0.4f, No Retail Routing", m_order->m_clientOrder->orderID_, m_order->EffectivePrice().ToDouble(), m_order->NBBO().ToDouble());
        if(m_order->m_argoPtr.size() == 1)
            m_order->CancelParent("No marketable nbbo received. No Retail Routing.");
        return false;
    }
    return true;
}

bool RetailRouting::IsRouteEligible(const std::string& routeName)
{
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!route)
        return false;
//    if(m_order->QtyRemain() < route->swpMinimumSize )
//        return false;
    if(!route->oddLot && m_order->QtyRemain() < m_order->m_lotsize )
        return false;
    if(!route->InOperationTime(RTXUtils::NowSsm()))
        return false;
    if(m_order->ExcludedRoute(routeName))
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(route->venue))
        return false;

    return true;
}

bool  RetailRouting::IsRouteMarketable(const std::string& routeName)
{
    if(!IsRouteEligible(routeName))
        return false;
    if(m_order->m_clientOrder->type_ == OrderType::TYPE_MARKET)
        return true;

    return true;
}
