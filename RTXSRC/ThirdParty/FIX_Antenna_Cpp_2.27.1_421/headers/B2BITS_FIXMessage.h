// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXMessage.h
/// Contains Engine::FIXMessage class declaration.

#ifndef __B2BITS_Engine_V12_FIXMessage_h__
#define __B2BITS_Engine_V12_FIXMessage_h__

#include <string>
#include <vector>
#include <set>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_V12_Defines.h"
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_TagValue.h"
#include "B2BITS_TZTimeHelper.h"


namespace Engine
{
    /** Extended interface of FIXMessage */
    class FIXMessageEx;

    /** SecurityAttributes forward declaration. */
    class SecurityAttributes;

    /** Basic FIX message. 
    * To create FIXMessage instance use FIXMsgFactory::newSkel method. 
    * To parse raw FIX message use FIXMsgProcessor::parse method.
    * To clone FIXMessage use FIXMsgProcessor::clone method.
    * @see FIXMsgFactory::newSkel
    * @see FIXMsgProcessor::parse
    * @see FIXMsgProcessor::clone
    */
    class V12_API FIXMessage: public virtual TagValue
    {
    public:
        /** Constructor. */
        FIXMessage();

        /** Destructor */
        virtual ~FIXMessage();

        /** Returns the SenderCompID field.
         *
         * @warning This method is not thread safe and has side effect.
         * @return Value of the SenderCompID field.
         * @deprecated Use Engine::FIXMessage::getSenderCompID instead.
         */
        B2B_DEPRECATED( "Use Engine::FIXMessage::getSenderCompID instead" )
        virtual const std::string* getSender() const = 0;

        /** Sets the SenderCompID field.
         *
         * @warning This method is not thread safe and has side effect.
         * @param senderCompID New SenderCompID value.
         * @deprecated Use Engine::FIXMessage::setSenderCompID instead.
         */
        B2B_DEPRECATED( "Use Engine::FIXMessage::setSenderCompID instead" )
        void setSender( const std::string& senderCompID ) {
            setSenderCompID( senderCompID );
        }

        /** Returns the SenderCompID field.
         *
         * @return Value of the SenderCompID field.
         */
        virtual AsciiString getSenderCompID() const = 0;

        /** Sets the SenderCompID field.
         *
         * @param senderCompID New SenderCompID value.
         */
        virtual void setSenderCompID( AsciiString senderCompID ) = 0;

        /** Returns the TargetCompID field.
         *
         * @warning This method is not thread safe and has side effect.
         * @return Value of the TargetCompID field.
         * @deprecated Use Engine::FIXMessage::getTargetCompID instead.
         */
        B2B_DEPRECATED( "Use Engine::FIXMessage::getTargetCompID instead" )
        virtual const std::string* getTarget() const = 0;

        /** Sets the TargetCompID field.
         *
         * @warning This method is not thread safe and has side effect.
         * @param targetCompID New TargetCompID value.
         * @deprecated Use Engine::FIXMessage::setTargetCompID instead.
         */
        B2B_DEPRECATED( "Use Engine::FIXMessage::setTargetCompID instead" )
        void setTarget( const std::string& targetCompID ) {
            setTargetCompID( targetCompID );
        }

        /** Returns the TargetCompID field.
         *
         * @return Value of the TargetCompID field.
         */
        virtual AsciiString getTargetCompID() const = 0;

        /** Sets the TargetCompID field.
         *
         * @param targetCompID New TargetCompID value.
         */
        virtual void setTargetCompID( AsciiString targetCompID ) = 0;

        /** Returns the MsgSeqNum field.
         *
         * @return Value of the MsgSeqNum field.
         */
        virtual int getSeqNum() const = 0;

        /** Returns the MsgType field.
         *
         * @warning This method is not thread safe and has side effect.
         * @return Value of the MsgType field.
         * @deprecated Use Engine::FIXMessage::type method instead.
         */
        B2B_DEPRECATED( "Use Engine::FIXMessage::type() instead" )
        virtual const std::string* getType() const = 0;

        /** Returns the MsgType field.
         *
         * @return Value of the MsgType field.
         */
        virtual AsciiString type() const throw() = 0;

        /** Updates the MsgSeqNum field.
         *
         * @param msgSeqNum New MsgSeqNum value.
         *
         * @return true field value was updated, false otherwise.
         */
        virtual bool setSeqNum( int msgSeqNum ) = 0;

        /** Returns Heartbeat interval (HeartBtInt) in seconds.
         *
         * @return Heartbeat interval (HeartBtInt) in seconds.
         */
        virtual int getHeartBeatInterval() const = 0;

        /**
         * Updates HeartBtInt field value.
         *
         * @param hbi New heartbeat value.
         * @return allways returns true.
         */
        virtual bool setHeartBeatInterval( int hbi ) = 0;

        /** Checks the message's originality.
         *
         * @return Returns "true" if the message is original, i.e.
         * PossDupFlag field value is empty or equals to 'N'.
         * Otherwise returns "false".
         */
        virtual bool isOriginal() const = 0;

        /** Checks whether the message is a Business Level Reject message.
         *
         * @return "true" if the message is a Business Level Reject message. "false" otherwise.
        */
        virtual bool isBusinessMsgRejectMsg() const throw() = 0;

        /** Checks whether the message is a SessionLevelReject message.
         *
         * @return "true" if the message is a Session Level Reject message. "false" otherwise.
        */
        virtual bool isSessionLevelRejectMsg() const throw() = 0;

        /** Checks whether the message is a session level message.
         *
         * @return "true" if the message is a Session Level message. "false" otherwise.
        */
        virtual bool isAdministrativeMsg() const throw() = 0;

        /**
         * Returns the formatted representation of the message.
         * @param substChar character to use instead of unreadable symbols (less than 0x20 and greater than 0x7F).
         * @warning This method is not thread safe 
         * @warning Intended for debugging only.
         */
        virtual const std::string* toString( char substChar = ' ' ) const = 0;

        /** Builds raw (native) representation of the message.
         *
         * @param[out] size will contain size of returned memory buffer.
         *
         * @return Pointer to buffer contained raw form.
         * @note This method should be used instead of getBuffer.
         * @note This method is not thread safe
         * @warning Returned buffer does not have null-terminator.
         */
        virtual const char* toRaw( int* size ) const = 0;

        /**
        * Returns a pointer to the internal message buffer that contains
        * raw form (native FIX format) of the message.
        *
        * @param size the receiving field for the buffer's size.
        * @param sa Session's security attributes. Always set 0 for this parameter.
        * @note This method is not thread safe
        *
        * @deprecated Use Engine::FIXMessage::toRaw(int* size) const instead.
        */
        B2B_DEPRECATED( "Use Engine::FIXMessage::toRaw(int* size) const instead" )
        virtual const char* getBuffer( int* size,
                                       SecurityAttributes* sa = NULL ) const = 0;

        /** Returns unique parser identifier of the message. 
        * @deprecated Use Engine::FIXMessage::parserID() instead.
        */
        B2B_DEPRECATED( "Use Engine::FIXMessage::parserID() instead." )
        ParserID protocolID() const throw();

        /** Returns unique parser identifier of the message. */
        virtual ParserID parserID() const throw() = 0;

        /** Returns the version of the FIX protocol.
          * @deprecated Use either Engine::FIXMessage::getApplicationVersion or Engine::FIXMessage::getSessionVersion instead.
          */
        B2B_DEPRECATED( "Use either FIXMessage::getApplicationVersion or FIXMessage::getSessionVersion instead" )
        virtual FIXVersion getVer() const throw() = 0;

        /** Returns the version of the FIX protocol.
          * For the message received from FIX50 session returns version of the application protocol.
          */
        virtual FIXVersion getApplicationVersion() const throw() = 0;

        /** For the session message received from FIX50 session returns version of the session protocol.
          * For the application message received from FIX50 session returns NA.
          * Otherwise returns same result as getApplicationVersion() method. */
        virtual FIXVersion getSessionVersion() const throw() = 0;

        /** Overloaded new operator. */
        void* operator new( std::size_t size );

        /** Overloaded delete operator */
        void operator delete( void* obj );

        /** Returns extended interface of FIXMessage*/
        virtual FIXMessageEx* extend() throw() = 0;

        /** Returns extended interface of FIXMessage*/
        virtual FIXMessageEx const* extend() const throw() = 0;

        /**
        *  Generates template of the message and stores it to the PreparedMessage
        * @param[out] msg PreparedMessage instance to store message template.
        */
        virtual void prepare( PreparedMessage* msg ) const = 0;

        /** Releases FIX message instance.
         * @see release(FIXMessage* msg)
         */
        virtual void release() const = 0;

        /** Releases FIX message instance.
         * @see release(FIXMessage* msg)
         */
        virtual void release() = 0;

        /** Releases FIX message instance.
         *
         * @param msg Instance of FIXMessage class.
         *
         * @warning This method releases all resources assigned with message. So, don't
         * call FIXGroup::release() for repeating group entries of this message
         * after you call this method.
         *
         */
        static void release( FIXMessage const* msg );
    }; // class FIXMessage

    /** Removes custom fields (tag > 6000) from message.
     * @param msg Instance of FIXMessage
     */
    void V12_API removeCustomFields( FIXMessage* msg );

    namespace TZTimeHelper
    {
        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param timestamp New time UTC for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagUTCTimestamp( FIXMessage *msg , int tag , const UTCTimestamp &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferUTCTimestamp ];
            return msg->set( tag , Engine::AsciiString( buf , utcTimestampToString( buf , ValueSizeBufferUTCTimestamp , timestamp , flags ) ) );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param timestamp New time for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagTZTimestamp( FIXMessage *msg , int tag , const TZTimestamp &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferTZTimestamp ];
            return msg->set( tag , Engine::AsciiString( buf , tzTimestampToString( buf , ValueSizeBufferTZTimestamp , timestamp , flags ) ) );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param timestamp New date and time UTC for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagUTCTimeOnly( FIXMessage *msg , int tag , const UTCTimeOnly &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferUTCTimeOnly ];
            return msg->set( tag , Engine::AsciiString( buf , utcTimeOnlyToString( buf , ValueSizeBufferUTCTimeOnly , timestamp , flags ) ) );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param timestamp New  date and time for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagTZTimeOnly( FIXMessage *msg , int tag , const TZTimeOnly &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferTZTimeOnly ];
            return msg->set( tag , Engine::AsciiString( buf , tzTimeOnlyToString( buf , ValueSizeBufferTZTimeOnly , timestamp , flags ) ) );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param timestamp New date for field.
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagDateOnly( FIXMessage *msg , int tag , const UTCDateOnly &timestamp )
        {
            char buf[ ValueSizeBufferUTCDateOnly ];
            return msg->set( tag , Engine::AsciiString( buf , utcDateOnlyToString( buf , ValueSizeBufferUTCDateOnly , timestamp ) ) );
        }

        /**
         * Returns field value as UTCTimestamp.
         * @param[in] msg Message.
         * @param[in] tag Tag of the field.
         */
        UTCTimestamp V12_API getAsUTCTimestamp( const FIXMessage *msg , int tag );

        /**
         * Returns field value as TZTimestamp.
         * @param[in] msg Message.
         * @param[in] tag Tag of the field.
         */
        TZTimestamp V12_API getAsTZTimestamp( const FIXMessage *msg , int tag );

        /**
         * Returns field value as UTCTimeOnly.
         * @param[in] msg Message.
         * @param[in] tag Tag of the field.
         */
        UTCTimeOnly V12_API getAsUTCTimeOnly( const FIXMessage *msg , int tag );

        /**
         * Returns field value as TZTimeOnly.
         * @param[in] msg Message.
         * @param[in] tag Tag of the field.
         */
        TZTimeOnly V12_API getAsTZTimeOnly( const FIXMessage *msg , int tag );

        /**
         * Returns field value as UTCDateOnly.
         * @param[in] msg Message.
         * @param[in] tag Tag of the field.
         */
        UTCDateOnly V12_API getAsUTCDateOnly( const FIXMessage *msg , int tag );
    }

} // namespace Engine

#endif

