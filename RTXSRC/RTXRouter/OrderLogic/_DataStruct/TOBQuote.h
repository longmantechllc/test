//
// Created by schen on 12/28/2020.
//

#ifndef RTXROUTER_TOBQUOTE_H
#define RTXROUTER_TOBQUOTE_H

#include <memory>
#include <vector>
#include <string>
#include <unordered_map>

#include "../include/Price.h"
#include "../include/TopOfBook.h"

using namespace std;
class TopOfBook;
class Price;

struct Tob
{
    Tob(){}
    Tob(const std::string& mpid, const Price px, const int qty, const int pri=0)
    :m_mpid(mpid),
    m_px(px),
    m_qty(qty),
    m_pri(pri)
    {}
    std::string m_mpid;
    Price m_px;
    int   m_qty;
    int   m_pri;
};

class TobQuote
{
public:
    TobQuote(){}
    TobQuote(const TopOfBook& tob, Price px, int qt, int pri=0)
    :m_mpid(tob.m_mpid),
     m_symbol(tob.m_symbol),
     m_px(px),
     m_qty(qt),
     m_pri(pri),
     m_afterConsumedQty(qt)
    {}
    virtual ~TobQuote(){}
    inline TobQuote& operator =( TobQuote q)
    {
        m_mpid = q.m_mpid;
        m_symbol = q.m_symbol;
        m_px = q.m_px;
        m_qty = q.m_qty;
        m_pri = q.m_pri;
        m_afterConsumedQty = m_qty;

        return *this;
    }
    void SetTob(Price px, const int qty, const int pri);
    void AddToQuote(const TopOfBook& tob, Price px, const int qt, const int pri=0, bool pass=false);

    std::string m_mpid;
    std::string m_symbol;
    Price m_px;
    int   m_qty = 0;
    int   m_pri = 0;
    int   m_afterConsumedQty = 0;

    std::unordered_map <std::string, std::shared_ptr<Tob>> m_subQuoteList;       //for pass through quotes
};


#endif //RTXROUTER_TOBQUOTE_H
