/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "AcceptorApp.h"

#include <cstdio>

#include <B2BITS_Session.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_LogSystem.h>

using namespace std;

AcceptorApp::AcceptorApp( void )
    : execId_( 0 )
    , orderId_( 0 )
    , logger_(Utils::Log::LogSystem::singleton()->createCategory( "Acceptor Application" ))
{
}

AcceptorApp::~AcceptorApp( void )
{
}

bool AcceptorApp::process( const Engine::FIXMessage& fixMsg, const Engine::Session& aSn )
{
    // WARNING: This method should complete as quickly as possible.
    // Do not perform time-consuming tasks inside it.

    // Just print the message to console here.
    if (logger_)
    {
        logger_->note(string("Message in session ") + *aSn.getId());
        logger_->note(*fixMsg.toString( '|' ));
    }

    // Process New Order Single and send Execution Reports
    if( "D" == fixMsg.type() ) {
        char order_id_str[128];
#ifdef _MSC_VER
        sprintf_s( order_id_str, sizeof(order_id_str), "%d", ++orderId_ );
#else
        sprintf( order_id_str, "%d", ++orderId_ );
#endif

        char buf[128];
        Engine::Session* volatile_session = const_cast<Engine::Session*>( &aSn );

        // Send Execution Report - New
        std::auto_ptr<Engine::FIXMessage> er_new( volatile_session->newSkel( "8" ) );

        fixMsg.copyTo( er_new.get(), FIXFields::ClOrdID, FIXFields::ClOrdID );
        fixMsg.copyTo( er_new.get(), FIXFields::Symbol, FIXFields::Symbol );
        fixMsg.copyTo( er_new.get(), FIXFields::Side, FIXFields::Side );
        fixMsg.copyTo( er_new.get(), FIXFields::OrderQty, FIXFields::OrderQty );

#ifdef _MSC_VER
        sprintf_s( buf, sizeof(buf), "%d", ++execId_ );
#else
        sprintf( buf, "%d", ++execId_ );
#endif
        er_new->set( FIXFields::OrderID, order_id_str );
        er_new->set( FIXFields::ExecID, buf );
        er_new->set( FIXFields::OrdStatus, '0' ); // New
        er_new->set( FIXFields::ExecType, '0' );  // New
        fixMsg.copyTo( er_new.get(), FIXFields::LeavesQty, FIXFields::OrderQty );
        er_new->set( FIXFields::LastQty, 0 );
        er_new->set( FIXFields::LastPx, 0 );
        er_new->set( FIXFields::CumQty, 0 );
        er_new->set( FIXFields::AvgPx, 0 );
        er_new->set( FIXFields::TransactTime, Engine::UTCTimestamp::now() );

        volatile_session->put( er_new.get() );

        // Send Execution Report - Fill
        std::auto_ptr<Engine::FIXMessage> er_fill( volatile_session->newSkel( "8" ) );

        fixMsg.copyTo( er_fill.get(), FIXFields::ClOrdID, FIXFields::ClOrdID );
        fixMsg.copyTo( er_fill.get(), FIXFields::Symbol, FIXFields::Symbol );
        fixMsg.copyTo( er_fill.get(), FIXFields::Side, FIXFields::Side );
        fixMsg.copyTo( er_fill.get(), FIXFields::OrderQty, FIXFields::OrderQty );

#ifdef _MSC_VER
        sprintf_s( buf, sizeof(buf), "%d", ++execId_ );
#else
        sprintf( buf, "%d", ++execId_ );
#endif
        er_fill->set( FIXFields::OrderID, order_id_str );
        er_fill->set( FIXFields::ExecID, buf );
        er_fill->set( FIXFields::OrdStatus, '2' ); // New
        er_fill->set( FIXFields::ExecType, 'F' );  // Trade
        er_fill->set( FIXFields::LeavesQty, 0 );
        fixMsg.copyTo( er_fill.get(), FIXFields::LastQty, FIXFields::OrderQty );
        fixMsg.copyTo( er_fill.get(), FIXFields::LastPx, FIXFields::Price );
        fixMsg.copyTo( er_fill.get(), FIXFields::CumQty, FIXFields::OrderQty );
        fixMsg.copyTo( er_fill.get(), FIXFields::AvgPx, FIXFields::Price );
        er_fill->set( FIXFields::TransactTime, Engine::UTCTimestamp::now() );
        er_fill->set( 1056, 1 );

        volatile_session->put( er_fill.get() );
    }

    // Return 'true' if the application can process the given message, otherwise 'false'.
    // If the application can not process the given message,
    // the FIX Engine will try to deliver it later according to "Delayed Processing Algorithm".
    return true;

}

void AcceptorApp::onLogonEvent( const Engine::LogonEvent* /*apEvent*/, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logon message has been received from the counterpart.
    if (logger_)
        logger_->note(  "Logon received for the session " + *aSn.getId() );
}

void AcceptorApp::onLogoutEvent( const Engine::LogoutEvent* /*apEvent*/, const Engine::Session& aSn )
{
    // This call-back method is called to notify that the Logout message has been received from the counterpart or the session was disconnected.
    if (logger_)
        logger_->note("Logout received for the session " + *aSn.getId());
}

void AcceptorApp::onMsgRejectEvent( const Engine::MsgRejectEvent* /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when a message have to be rejected.
}

void AcceptorApp::onSequenceGapEvent( const Engine::SequenceGapEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a message gap is detected in incoming messages.
}

void AcceptorApp::onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* /*apEvent*/, const Engine::Session& /*aSn*/ )
{
    // This call-back method is called when a session-level Reject message (MsgType = 3) is received.
}

void AcceptorApp::onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ )
{
}

void AcceptorApp::onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when a Resend Request (MsgType = 2) has been received.
}

void AcceptorApp::onNewStateEvent( const Engine::NewStateEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when the session has changed its state.
}

void AcceptorApp::onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent& /*event*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when the message has to be routed to the session, which does not exist.
}

bool AcceptorApp::onResend( const Engine::FIXMessage& /*msg*/, const Engine::Session& /*sn*/ )
{
    // This call-back method is called when an outgoing message is about to be resent as a reply to the incoming ResendRequest message.

    // Return 'true' if the message should be resent, otherwise 'false'
    // If the method returns 'true' then the Engine resends the message to counterpart,
    // otherwise it sends the SequenceReset Gap Fill message.
    return true;
}
