

#pragma once

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <exception>

#include "spdlog/spdlog.h"
#include <libwebsockets.h>
#include <rapidjson/document.h>

// #define BINANCE_WS_HOST "stream.binance.com" tokyo global
#define BINANCE_WS_HOST "stream.binance.us"
#define BINANCE_WS_PORT 9443


using namespace std;


struct Book;
class MDCache;

class BinaCPP_websocket {
public:
	BinaCPP_websocket(int idx, const std::string& symbol,
		Book& book, MDCache * mdCache)
		: m_idx{idx}, m_symbol{symbol},
		  m_book{book}, m_cache{mdCache} {

	}
	BinaCPP_websocket( BinaCPP_websocket&& rhs) = default;

	~BinaCPP_websocket() {
		spdlog::error( "destroy websocket for idx {}, symbol {}",
			m_idx, m_symbol); 
		lws_context_destroy( context );
	}
	void connect_endpoint(
		const char* path
	);
	void init();
	void service();
private:
	static struct lws_context *context;
	static int  event_cb( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len );
	
	static constexpr struct lws_protocols protocols[] =
	{
		{
			"binance-protocol",
			event_cb,
			0,
			65536,
		},
		{ NULL, NULL, 0, 0 } /* terminator */
	};
	int m_idx;
	std::string m_symbol;
	Book& m_book;
	MDCache* m_cache;
	struct lws* conn;
	//int cb(Json::Value &json_value);
	rapidjson::Document document_;
	void cb(std::string_view data);
};
