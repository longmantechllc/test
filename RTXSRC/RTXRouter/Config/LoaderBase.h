//
// Created by schen on 10/26/2020.
//

#ifndef RTXROUTER_LOADERBASE_H
#define RTXROUTER_LOADERBASE_H

using namespace std;

class Logger;
class ConfigGlobalLoader;
class ConfigRouterLoader;
class ConfigLoader;

namespace xercesc_3_2
{
    class DOMElement;
    class DOMNode;
    class InputSource;
}

class LoaderBase
{
public:
    LoaderBase()=default;
    LoaderBase(Logger* logger);
    virtual ~LoaderBase()=default;

    virtual void ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node) =0;
    std::string GetElement(xercesc_3_2::DOMNode* node, const char* name);

    Logger* m_loggerSys;        //log to system.log file
};


#endif //RTXROUTER_LOADERBASE_H
