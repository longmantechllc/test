// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FastMappingOptions.h
/// Contains Engine::FastMappingOptions class declaration.

#ifndef B2BITS_Engine_FastMappingOptions_h
#define B2BITS_Engine_FastMappingOptions_h

#include <string>

namespace Engine
{
    struct FastMappingOptions
    {

        /** Method of conversion between FIX char and FAST int32.
          * if true char '0' will be 0 in int32 
          * if false char '0' will be ASCII code of '0' in int32
          */
        bool charToInt32AsAscii_;

        /** Method of conversion between FIX char and FAST uint32.
          * if true char '0' will be 0 in uint32 
          * if false char '0' will be ASCII code of '0' in uint32
          */
        bool charToUInt32AsAscii_;

        /** Representation as FAST string of 'true' value */
        std::string boolYtoString_;

        /** Representation as FAST string of 'false' value */
        std::string boolNtoString_;

        FastMappingOptions()
            :charToInt32AsAscii_(false)
            ,charToUInt32AsAscii_(true)
            ,boolYtoString_(1, '\x0001')
            ,boolNtoString_(1, '\x0000')
        {
        }

        static FastMappingOptions getBovespaOptions()
        {
            FastMappingOptions options;
            options.charToInt32AsAscii_ = true;
            options.charToUInt32AsAscii_ = true;
            options.boolYtoString_ = "Y";
            options.boolNtoString_ = "N";
            return options;
        }

        static FastMappingOptions getCqgOptions()
        {
            FastMappingOptions options;
            options.charToInt32AsAscii_ = false;
            options.charToUInt32AsAscii_ = true;
            options.boolYtoString_ = "Y";
            options.boolNtoString_ = "N";
            return options;
        }
    };
} // namespace Engine
#endif // B2BITS_Engine_FastMappingOptions_h
