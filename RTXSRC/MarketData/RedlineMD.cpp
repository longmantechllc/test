#include <cstring>
#include <unordered_map>
#include "RedlineMD.h"
#include "logger.h"
#include <iostream>

using namespace std;

RedlineMD::RedlineMD() {
    //m_init = connectRedline();

   m_fp = fopen("MD.log", "w");
    if (m_fp == NULL)
    {
        cout << "Couldn't open file MD.log" << endl;
    }
}

RedlineMD::~RedlineMD() {
    if (m_init) {
        fh_close( &m_clientHandle, FH_DEFAULT_FLAG );
    }
}
/*
namespace {
    template <typename T, typename ... Args>
    void reportError(FhRC_t rc, Args ... args) {
        if (rc != FH_SUCCESS ) {
            logger2 << ", reason: " << fh_get_error_string(rc) << args ...
                   << endl;
        }
    }
}
*/
#define FH_API( api_call )                                                                                             \
     ( {                                                                                                               \
          FhRC_t _rc = api_call;                                                                                       \
          if (_rc != FH_SUCCESS ) {      \
            logger2 <<  #api_call << "failed with error: " << fh_get_error_string( _rc ) << endl;              \
     }} )


static void onBookUpdateStatic(void* user_arg, const FhBookUpdate_t* book_update) {
    if (!user_arg) {
        logger2 << " no user_arg " << endl;
        return;
    }
    if (user_arg) {
        //logger2 << "user_arg " << user_arg << endl;
        //logger2 << "(RedlineMD*) user_arg->m_clientName: " << ((RedlineMD*) user_arg)->getClientName() << endl;
        ((RedlineMD*) user_arg)->onBookUpdate(book_update);
    }
}

bool RedlineMD::connectRedline() {
    FhRC_t rc = FH_SUCCESS;

    rc = fh_client_open( &m_clientHandle, m_clientName.c_str(), FH_DEFAULT_FLAG );
    if ( rc != FH_SUCCESS ) {
        logger2 << "fh_client_open() failed on clientName " << m_clientName
               << ", reason: " << fh_get_error_string(rc) << endl;
        return false;
    }
    /* Register Market data callback
      * Callback runs on the update handler
      * NOTE: the callback runs in the the update handler thread and blocks update processin
      *       while the callback is running  so the callback function
      *       should be execute quickly to not block the update handler thread */
    rc = fh_set_book_callback( m_clientHandle, ::onBookUpdateStatic, FH_DEFAULT_FLAG ) ;
    if ( rc != FH_SUCCESS ) {
        logger2 << "fh_set_book_callback() failed: " << fh_get_error_string( rc ) << endl;
        return false;
    }
    else
        logger2 << "fh_set_book_callback() OK with m_clientHandle: " << &m_clientHandle << endl;

    int32_t depth = 1;
    const char *feed_name = "FH_FEED_US_EQUITY_SIP";
    const char *pid_name = "FH_PID_NBBO";

    // lookup the feed ID and PID we want to subscribe to
    FhFeedId_t feed;
    FhPID_t pid;
    FH_API( fh_get_feed_id_from_name( m_clientHandle, feed_name, &feed ) );
    FH_API( fh_get_pid_from_name( m_clientHandle, pid_name, &pid ) );

    uint64_t subscription_flags = FH_DEFAULT_FLAG;

    FH_API( fh_create_subscription_template( m_clientHandle, feed, pid, depth,
                                             0, // Dynamically accomodate new subscriptions that use this template
                                             &official_nbbo_template, subscription_flags ) );

    FhFeedPID_t composite_builder;
    composite_builder.feed = feed;
    composite_builder.pid = FH_AUTO_DETECT_PID;

    FH_API( fh_create_composite_subscription_template( m_clientHandle, 1, &composite_builder, depth,
                                                       0, // Dynamically accomodate new subscriptions that use this template
                                                       &composite_template, subscription_flags ) );

    m_init = true;
    return true;
}

static const char* get_pid_name( FhClientHandle_t fh_handle, FhPID_t pid )
{
    if( !fh_handle ) {
        return "UNKNOWN";
    }
    const char* pid_name = NULL;
    fh_get_name_from_pid( fh_handle, pid, &pid_name );
    return pid_name;
}

// PID name shortener for the nonscroll book viewer
static const char* get_concise_pid_name( FhClientHandle_t fh_handle, FhPID_t pid )
{
    const char* pid_name = get_pid_name( fh_handle, pid );
    int idx = -1;
    for(int i = strlen(pid_name)-1; i>=0; --i)
    {
        if(pid_name[i] == '_')
        {
            idx = i;
            break;
        }
    }
    return pid_name+1+idx;
}

void RedlineMD::onBookUpdate( const FhBookUpdate_t *update ) {
    static std::chrono::milliseconds lastCheckingTime {0};
/*    if (!m_init || !m_TobSubscripts.count(update->symbol) && !m_BboSubscripts.count(update->symbol)) {
        logger2 << " no init or symbol subscribed " << endl;
        return;
    }
    */
    {
        std::shared_lock guard(m_mutex);
        if (!m_init || !m_subs.count(update->symbol) || m_unsubs.count(update->symbol))
            return;
    }

    //fh_print_book_update(m_fp,update,FH_PRINT_PARTICIPANT_BOOKS);
    //logger2 << "update on symbol: " << update->symbol << ", BBO? " << !(update->subscriptionFlag & FH_COMPOSITE)
    //<< "bid px = " << update->bidEntries[0].price.d << ", ask px = " << update->askEntries[0].price.d << endl;
    auto getMs = []() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        return std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        //auto now2 = std::chrono::duration_cast<std::chrono::milliseconds>(now.time_since_epoch());
        //return now2.count();
    };

    if (!(update->subscriptionFlag & FH_COMPOSITE)) {
        // This is an official NBBO update from the SIP
        MDBBO mdbbo;
        auto& bbo = mdbbo.data;
        bbo.m_bboBuy = update->bidEntries[0].price.d;
        bbo.m_bboSell = update->askEntries[0].price.d;
        bbo.m_symbol = update->symbol;
        //getSubscriber()->UpdateBbo(bbo);
        if (!m_queue.try_push(make_shared<MDBBO>(mdbbo))) {
            auto now = getMs();
            if ((now - lastCheckingTime).count() > m_checkingTime) {
                lastCheckingTime = now;
                logger2 << "failed to push NBBO, queue size = " << m_queue.size() << endl;
            }
        }
        //logger2 << "NBBO on " << bbo.m_symbol << ", buy " << bbo.m_bboBuy.ToDouble() << ", sell " << bbo.m_bboSell.ToDouble() << endl;
    } else {
        // This is an update to the composite view - meaning the best price changed, or the
        // size of the best price, on either side - among all venues.
        unordered_map <FhPID_t, TopOfBook> tobs;

        if (update->numBidEntries) // There is something on the bid side of the book right now
        {
            // This line accesses current composite best price - for SIP should match the
            // nbboBidPrice above
            double px = update->bidEntries[0].price.d;

            int numExchanges = update->bidEntries[0].numComponents;
            for (int exchangeIterator = 0; exchangeIterator < numExchanges; exchangeIterator++) {
                FhPID_t exchange = update->bidEntries[0].entryComponents[exchangeIterator].componentPID;
                uint32_t vol = update->bidEntries[0].entryComponents[exchangeIterator].volume;
                tobs.emplace(exchange, TopOfBook{get_concise_pid_name(getClientHandle(), exchange),
                                        update->symbol, Price(), 0, Price(px), vol});
            }
        }

        if (update->numAskEntries) {
            double px = update->askEntries[0].price.d;
            int numExchanges = update->askEntries[0].numComponents;
            for (int exchangeIterator = 0; exchangeIterator < numExchanges; exchangeIterator++) {
                FhPID_t exchange = update->askEntries[0].entryComponents[exchangeIterator].componentPID;
                uint32_t vol = update->askEntries[0].entryComponents[exchangeIterator].volume;
                if (tobs.count(exchange)) {
                    tobs[exchange].m_askPrice = px;
                    tobs[exchange].m_askSize = vol;
                } else
                    tobs.emplace(exchange, TopOfBook{get_concise_pid_name(getClientHandle(), exchange), update->symbol, Price(px), vol, Price(), 0});
            }
        }

        //m_symbols[update->symbol] = true; // this is used by unscribe model
        MDBatchTOB tob;
        auto& batch = tob.data;
        //logger2 << "batch update on " << update->symbol << " TOB: " << tobs.size() << endl;
        for (auto &e: tobs) {
            batch.push_back(e.second);
        /*
            logger2 << "mpid=" << e.second.m_mpid << ", symbol=" << e.second.m_symbol << ", askPrice="
                   << e.second.m_askPrice.ToDouble()
                   << ", askSize=" << e.second.m_askSize << ", bidPrice=" << e.second.m_bidPrice.ToDouble()
                   << ", bidSize=" << e.second.m_bidSize << endl;
        */
        }
        if (!m_queue.try_push(make_shared<MDBatchTOB>(tob))) {
            auto now = getMs();
            if ((now - lastCheckingTime).count() > m_checkingTime) {
                lastCheckingTime = now;
                logger2 << "failed to push BatchTOB, queue size = " << m_queue.size() << endl;
            }
        }
        //getSubscriber()->OnTopOfBookUpdate(batch);
    }
    /*
    {
        MDMsgPtr msg;
        while (m_queue.try_dequeue(msg)) {
            if (msg->msgtype() == MDType::TOB)
                logger2 << "top BatchTOB has TOBs: " << msg->getTOB()->size() << endl;
            else
                logger2 << "top BBO  symbol : " << msg->getBBO()->m_symbol << endl;
        }
    }
    */
}
#ifdef MD_UNSUB
std::vector <std::shared_ptr<TopOfBook>>  RedlineMD::subscribeTOB(const std::string& symbol ){
    logger2 << "subscribing TOB on symbol " << symbol << endl;

    if (!m_init) return {};

    if (m_TobSubscripts.find(symbol) != m_TobSubscripts.end()) {
        logger2 << symbol << " is subscribed on TOB before, refresh" << endl;
        FH_API( fh_refresh(m_clientHandle, m_TobSubscripts[symbol], FH_DEFAULT_FLAG) );
        return {};
    }

    FhSubscripHandle_t composite_subscription;
    FH_API( fh_subscribe_from_template(
            composite_template, symbol.c_str(), this,               // user_data to be passed with updates from this subscription
            NULL, &composite_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_TobSubscripts.emplace(symbol, composite_subscription);

     logger2 << "subscribeTOB OK on " << symbol << ", subHandle " << composite_subscription << ", this " << this << endl;
     m_symbols.emplace(symbol,false);
     return {};
}

Bbo RedlineMD::subscribeBbo(const std::string& symbol){
    logger2 << "subscribing Bbo on symbol " << symbol << endl;
    if (!m_init) return {};
    if (m_BboSubscripts.find(symbol) != m_BboSubscripts.end()) {
        logger2 << symbol << " is subscribed on NBBO before, refresh" << endl;
        FH_API( fh_refresh(m_clientHandle, m_BboSubscripts[symbol], FH_DEFAULT_FLAG) );
        return {};
    }

    FhSubscripHandle_t official_nbbo_subscription;
    FH_API( fh_subscribe_from_template(
            official_nbbo_template, symbol.c_str(), this,           // user_data to be passed with updates from this subscription
            NULL, &official_nbbo_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_BboSubscripts.emplace(symbol, official_nbbo_subscription);
    logger2 << "subscribe NBBO OK on " << symbol << ", subHandle " << official_nbbo_subscription << ", this " << this << endl;
    FH_API( fh_refresh(m_clientHandle, m_BboSubscripts[symbol], FH_DEFAULT_FLAG) );
    return {};
}

OpenTrade RedlineMD::requestOpeningTrade(const std::string& symbol, int oid){
    return {};
}

void RedlineMD::removeTOBSubscription(const std::string& symbol){
    //logger2 << "removeTOBSubscription called on " << symbol << endl;
    if (!m_init) return;
    if (m_TobSubscripts.find(symbol) == m_TobSubscripts.end()) {
        logger2 << symbol << " isn't subscribed on TOB. No action taken" << endl;
    }
    else {

        FH_API( fh_unsubscribe(m_clientHandle, &m_TobSubscripts[symbol], NULL, FH_DEFAULT_FLAG) );
        m_TobSubscripts.erase(symbol);
    }
    logger2 << "remove TOB subscription OK on " << symbol << endl;
    m_symbols.erase(symbol);
}

void RedlineMD::removeBboSubscription(const std::string& symbol){
    //logger2 << "removeBboSubscription called on " << symbol << endl;
    if (!m_init) return;
    if (m_BboSubscripts.find(symbol) == m_BboSubscripts.end()) {
        logger2 << symbol << " isn't subscribed on NBBO. No action taken" << endl;
    }
    else {
        //for(auto& i: m_BboSubscripts[symbol])
            FH_API( fh_unsubscribe(m_clientHandle, &m_BboSubscripts[symbol], NULL, FH_DEFAULT_FLAG) );
        m_BboSubscripts.erase(symbol);
    }
    logger2 << "remove NBBO subscription OK on " << symbol << endl;
}

void RedlineMD::cancelOpeningTradeRequest(const std::string& symbol){

}
#elif defined (MD_DISABLE) //disable version
std::vector <std::shared_ptr<TopOfBook>>  RedlineMD::subscribeTOB(const std::string& symbol ){
    subscribeBbo(symbol);
    logger2 << "subscribing TOB on symbol " << symbol << endl;
    if (!m_init) return {};
    auto itor = m_TobSubscripts.find(symbol);
    if (itor != m_TobSubscripts.end()) {
        if (itor->second.second) {
            logger2 << symbol << " is subscribed on TOB before, refresh" << endl;
            FH_API(fh_refresh(m_clientHandle, itor->second.first, FH_DEFAULT_FLAG));
        }
        else {
            logger2 << symbol << " is subscribed on TOB before, enable" << endl;
            FH_API(fh_enable_delivery(m_clientHandle, itor->second.first, FH_DEFAULT_FLAG));
        }
        return {};
    }

    FhSubscripHandle_t composite_subscription;
    FH_API( fh_subscribe_from_template(
            composite_template, symbol.c_str(), this,               // user_data to be passed with updates from this subscription
            NULL, &composite_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_TobSubscripts.emplace(symbol, make_pair(composite_subscription,true));

    logger2 << "subscribeTOB OK on " << symbol  << endl;
    //m_symbols.emplace(symbol,true); // this is used by unscribe model
    //removeTOBSubscription(symbol);
    //subscribeTOB(symbol);
    std::lock_guard<std::mutex> guard(m_mutex);
    m_unsubs.erase(symbol);
    return {};
}

Bbo RedlineMD::subscribeBbo(const std::string& symbol){
    logger2 << "subscribing Bbo on symbol " << symbol << endl;
    if (!m_init) return {};
    auto itor = m_BboSubscripts.find(symbol);
    if (itor != m_BboSubscripts.end()) {
        if (itor->second.second) {
            logger2 << symbol << " is subscribed on NBBO before, refresh" << endl;
            FH_API(fh_refresh(m_clientHandle, itor->second.first, FH_DEFAULT_FLAG));
        }
        else {
            logger2 << symbol << " is subscribed on NBBO before, enable" << endl;
            FH_API(fh_enable_delivery(m_clientHandle, itor->second.first, FH_DEFAULT_FLAG));
        }
        return {};
    }

    FhSubscripHandle_t official_nbbo_subscription;
    FH_API( fh_subscribe_from_template(
            official_nbbo_template, symbol.c_str(), this,           // user_data to be passed with updates from this subscription
            NULL, &official_nbbo_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_BboSubscripts.emplace(symbol, make_pair(official_nbbo_subscription,true));
    logger2 << "subscribe NBBO OK on " << symbol << endl;
    FH_API( fh_refresh(m_clientHandle, official_nbbo_subscription, FH_DEFAULT_FLAG) );
    return {};
}
#else
std::vector <std::shared_ptr<TopOfBook>>  RedlineMD::subscribeTOB(const std::string& symbol ){
    logger2 << "subscribing TOB on symbol " << symbol << endl;
    if (!m_init) return {};

    std::unique_lock guard(m_mutex);
    m_unsubs.erase(symbol);

    if (m_subs.count(symbol)) { // it's subscribed before
        logger2 << symbol << " is subscribed on before, refresh" << endl;
        FH_API(fh_refresh(m_clientHandle, m_BboSubscripts[symbol].first, FH_DEFAULT_FLAG));
        FH_API(fh_refresh(m_clientHandle, m_TobSubscripts[symbol].first, FH_DEFAULT_FLAG));
        return {};
    }

    FhSubscripHandle_t official_nbbo_subscription;
    FH_API( fh_subscribe_from_template(
            official_nbbo_template, symbol.c_str(), this,           // user_data to be passed with updates from this subscription
            NULL, &official_nbbo_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_BboSubscripts.emplace(symbol, make_pair(official_nbbo_subscription,true));
    logger2 << "subscribe NBBO OK on " << symbol << endl;
    //FH_API( fh_refresh(m_clientHandle, official_nbbo_subscription, FH_DEFAULT_FLAG) );

    FhSubscripHandle_t composite_subscription;
    FH_API( fh_subscribe_from_template(
            composite_template, symbol.c_str(), this,               // user_data to be passed with updates from this subscription
            NULL, &composite_subscription, FH_DELIVERY_CALLBACK // Flag to modify delivery method
    ) );
    m_TobSubscripts.emplace(symbol, make_pair(composite_subscription,true));
    logger2 << "subscribeTOB OK on " << symbol  << endl;
    m_subs.emplace(symbol);
    return {};
}

Bbo RedlineMD::subscribeBbo(const std::string& symbol){
    return {};
}

void RedlineMD::removeTOBSubscription(const std::string& symbol){
    //std::lock_guard<std::mutex> guard(m_mutex);
    unique_lock guard(m_mutex);
    m_unsubs.emplace(symbol);
    logger2 << "removeTOBSubscription OK on " << symbol  << endl;
}
/*
void RedlineMD::removeTOBSubscription(const std::string& symbol){
    removeBboSubscription(symbol);
    logger2 << "removeTOBSubscription called on " << symbol << ", m_init= " << m_init << endl;
    if (!m_init) return;
    if (m_TobSubscripts.find(symbol) == m_TobSubscripts.end()) {
        logger2 << symbol << " isn't subscribed on TOB. No action taken" << endl;
    }
    else {
        FH_API( fh_disable_delivery(m_clientHandle, m_TobSubscripts[symbol].first, FH_DEFAULT_FLAG) );
        //FH_API( fh_unsubscribe(m_clientHandle, &m_TobSubscripts[symbol], NULL, FH_DEFAULT_FLAG) );
        m_TobSubscripts[symbol].second = false;
        //m_TobSubscripts.erase(symbol);
    }
    logger2 << "remove TOB subscription OK on " << symbol << endl;
}

void RedlineMD::removeBboSubscription(const std::string& symbol){
    logger2 << "removeBboSubscription called on " << symbol << ", m_init= " << m_init << endl;
    if (!m_init) return;
    logger2 << "removeBboSubscription is it subscribed before? "
        << m_BboSubscripts.find(symbol) == m_BboSubscripts.end() << endl;
    if (m_BboSubscripts.find(symbol) == m_BboSubscripts.end()) {
        logger2 << symbol << " isn't subscribed on NBBO. No action taken" << endl;
    }
    else {
        FH_API( fh_disable_delivery(m_clientHandle, m_BboSubscripts[symbol].first, FH_DEFAULT_FLAG) );
        //FH_API( fh_unsubscribe(m_clientHandle, &m_BboSubscripts[symbol], NULL, FH_DEFAULT_FLAG) );
        m_BboSubscripts[symbol].second = false;
        //m_BboSubscripts.erase(symbol);
    }
    logger2 << "remove NBBO subscription OK on " << symbol << endl;
}
*/
#endif
OpenTrade RedlineMD::requestOpeningTrade(const std::string& symbol, int oid){
    return {};
}

void RedlineMD::cancelOpeningTradeRequest(const std::string& symbol){

}
