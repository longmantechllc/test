#include <string>
#include <string_view>
#include <iostream>

#include <json_dto/pub.hpp>
#include <rapidjson/document.h>
#include <magic_enum/magic_enum.hpp>
// #include <utils/numeric.h>

const std::string_view info_in {
    R"JSON({"event":"info","version":2,"serverId":"2601281d-039b-4eb8-a668-04a9bf5cc926","platform":{"status":1}}
    )JSON"
};
const std::string_view subscribed_in {
    R"JSON({"event":"subscribed","channel":"book","chanId":124,"symbol":"tBTCUSD","prec":"P0","freq":"F0","len":"25","pair":"BTCUSD"}
    )JSON"
};

const std::string_view json_orderbook_snap_in {
    R"JSON(
    [124,[[43802,1,0.175467],[43801,2,0.47560197],[43799,1,0.057077],[43798,2,0.01850798],[43797,1,0.63906491],[43795,1,0.114154],[43793,1,0.114157],[43792,3,0.5839915],[43791,1,0.00121739],[43788,2,0.213223],[43786,1,0.20028],[43784,1,0.1],[43783,2,0.40390327],[43780,1,0.00624202],[43778,2,1.434305],[43777,2,0.32127],[43775,3,0.55197998],[43774,1,0.3001],[43773,1,0.00181387],[43772,1,0.5426],[43771,2,1.211],[43770,3,1.925162],[43766,4,2.36487273],[43765,5,7.695006],[43764,1,0.4139],[43804,1,-0.12000001],[43806,2,-0.27998504],[43807,1,-0.00095027],[43811,1,-0.00025231],[43815,1,-0.2891],[43817,3,-1.85443083],[43818,1,-0.01350798],[43820,2,-1.132],[43821,2,-0.157075],[43824,4,-1.71891983],[43825,1,-0.42],[43826,1,-0.755],[43827,2,-1.248],[43829,1,-0.1],[43830,3,-5.76287441],[43831,1,-0.42],[43832,2,-0.53905469],[43837,4,-0.68657349],[43838,2,-0.13463323],[43839,2,-0.228446],[43840,5,-0.8299],[43842,2,-1.29150884],[43843,2,-0.570958],[43844,5,-0.63455441],[43846,3,-0.20127112]]]
    )JSON"
};

const std::string_view json_orderbook_udpate_in {
    R"JSON(
    [124,[43801,3,0.57560197]]
    )JSON"
};

const std::string_view book_channel {
    R"JSON(
{"event":"info","version":2,"serverId":"fbfdcfc5-c135-4e37-b986-994b5cfdd85b","platform":{"status":1}}
{"event":"subscribed","channel":"book","chanId":38,"symbol":"tBTCUSD","prec":"P0","freq":"F0","len":"25","pair":"BTCUSD"}
[38,[[43480,4,0.8248362],[43479,2,0.09776074],[43478,1,0.00058202],[43477,2,0.00174846],[43476,1,0.2326537],[43475,1,0.2884],[43474,1,0.05051078],[43473,2,1.56851411],[43472,2,3.52089849],[43470,1,0.115001],[43469,1,0.120764],[43464,1,0.1],[43463,3,2.7245],[43462,2,1.143901],[43461,1,0.08128],[43460,1,0.2861],[43459,2,0.82223815],[43458,1,0.7213],[43457,1,0.172518],[43456,1,0.172512],[43455,2,0.57304194],[43454,2,0.40792284],[43452,1,0.45998112],[43450,4,7.0368607],[43447,5,0.62457519],[43481,2,-0.95405193],[43485,1,-0.77881736],[43488,1,-0.00577521],[43489,2,-0.10624326],[43490,1,-0.115003],[43491,1,-0.00046949],[43492,6,-1.07037725],[43493,5,-0.0005],[43494,2,-0.0002],[43495,6,-1.65641212],[43496,7,-0.7384],[43497,4,-0.0004],[43498,4,-0.31702788],[43499,6,-0.28790466],[43500,7,-0.23812151],[43501,5,-0.172899],[43502,3,-0.0003],[43503,6,-2.4494],[43504,6,-0.345429],[43505,5,-0.97313043],[43506,7,-0.2385773],[43507,7,-1.17148067],[43508,6,-0.6],[43509,8,-1.043938],[43510,5,-0.51963341]]]
[38,[43461,0,1]]
[38,[43485,0,-1]]
[38,[43480,3,0.44899473]]
[38,[43477,3,0.41331136]]
    )JSON"
};

struct level_t
{
	std::uint64_t price = 0;
    std::uint32_t order_num = 0;
	double size = 0.0;
    level_t(std::uint64_t px, std::uint32_t num, double qty)
        : price{px}, order_num{num}, size{qty} {}
};
struct snap_t {
    std::uint32_t channel_id = 0;
    std::vector<level_t> levels;
};

namespace test::WS {
enum class EventType : std::uint8_t {
        INVALID_EVENT_TYPE = 0,
        l2snapshot,
        l2update,
        bbo,
        trades,
        hb, // heartbeat
        on, // new order
        on_req, // new order request
        ou, // order update
        oc, // order cancel
        oc_req, // order cancel request
        oc_multi_req, // multiple orders cancel request
        te, // trade executed
        enum_with_space
    };
}
// Сustom definitions of names for enum.
// Specialization of `enum_name` must be injected in `namespace magic_enum::customize`.
template <>
constexpr std::string_view magic_enum::customize::enum_name<
test::WS::EventType>(test::WS::EventType value) noexcept {
    using test::WS::EventType;
    switch (value) {
        case EventType::on_req:
            return "on-req";
        case EventType::oc_multi_req:
            return "oc_multi-req";
        case EventType::enum_with_space:
            return "Enum with space";
        default:
            return {}; // Empty string for default value.
    }
    return {}; // Empty string for unknow value.
}

struct JsonObj {
    test::WS::EventType event;
};

namespace json_dto
{
#define read_json_for_enum(enum_type) \
template<> \
void read_json_value(enum_type &v, const rapidjson::Value &object)

#define write_json_for_enum(enum_type) \
template<> \
void write_json_value(const enum_type &v, \
                        rapidjson::Value &object, \
                        rapidjson::MemoryPoolAllocator<> &allocator)

read_json_for_enum(test::WS::EventType);
write_json_for_enum(test::WS::EventType);
#undef read_json_for_enum
#undef write_json_for_enum

/////////////////////
#define read_json_for_enum(enum_type) \
template<> \
void read_json_value( \
    enum_type &v, \
    const rapidjson::Value &object) { \
    try { \
        std::string_view representation; \
        read_json_value(representation, object); \
        v = magic_enum::enum_cast<enum_type>( \
            representation).value(); \
    } \
    catch (const std::exception &ex) { \
        throw std::runtime_error{std::string{"unable to read : "} + ex.what()}; \
    } \
}

#define write_json_for_enum(enum_type) \
template<> \
void write_json_value(const enum_type &v, \
                        rapidjson::Value &object, \
                        rapidjson::MemoryPoolAllocator<> &allocator) { \
    std::string_view representation{magic_enum::enum_name(v)}; \
    write_json_value(representation, object, allocator); \
}
read_json_for_enum(test::WS::EventType);
write_json_for_enum(test::WS::EventType);

#undef read_json_for_enum
#undef write_json_for_enum

template<typename Json_Io>
void json_io(Json_Io & io, JsonObj & m)
{
	io & json_dto::mandatory("event", m.event);

}

} // namespace json_dto

void test_enum() {
    const std::vector<std::string_view> enums {
        R"JSON({"event": "l2snapshot"})JSON",
        R"JSON({"event": "on-req"})JSON",
        R"JSON({"event": "Enum with space"})JSON",
        R"JSON({"event": "oc_multi-req"})JSON",
        R"JSON({"event": "garbage"})JSON"
    };

    std::cout << "========= test_enum =========" << std::endl;
    constexpr auto names = magic_enum::enum_names<test::WS::EventType>();
    for(const auto& i: names) {
        std::cout << "WS::EventType name "<<  i << std::endl;
    }

    for (auto& in : enums) {
        rapidjson::Document document;
        try {
            document.SetNull();
            document.GetAllocator().Clear();
            document.Parse<rapidjson::kParseDefaultFlags>(
            //document.Parse<rapidjson::kParseNumbersAsStringsFlag>(
                in.data(), in.size());
            json_dto::check_document_parse_status(document);
        }
        catch (const std::exception &e) {
            std::cout << "error parsing " << e.what()
                    << " on " << in << std::endl;
            return;
        }
        try {
            JsonObj obj;
            json_dto::from_json<JsonObj>(document, obj);
            std::cout << "EventType " << (int)obj.event << " = " << magic_enum::enum_name(obj.event) << std::endl;

        }
        catch (const std::exception &e) {
            std::cout << "json_dto error: e= " << e.what() << " on " << in
                    << std::endl;
        }
    }
}


static const char* kTypeNames[] = 
    { "Null", "False", "True", "Object", "Array", "String", "Number" };

// namespace json_dto {

// template<typename Json_Io>
// void json_io(
//         Json_Io &io, 
//         crypto::exchanges::deribit::models::orderbook_result_t &m) {

//     io
//     & json_dto::mandatory("instrument_name", m.instrument_name)
//     & json_dto::mandatory("settlement_price", m.settlement_price)
//     & json_dto::mandatory("mark_price", m.mark_price)
//     & json_dto::mandatory("change_id", m.change_id)
//     & json_dto::mandatory("timestamp", m.timestamp)
//     & json_dto::mandatory("state", m.state)
//     & json_dto::mandatory("bids", m.bids)
//     & json_dto::mandatory("asks", m.asks);

// }
// } // namespace json_dto

void test_array_of_array() {
    const std::string_view currency_in {
        R"JSON(
        [["1INCH","AAA","AAVE","AAVEF0","ADA","ADAF0","ADD","AIX","ALBT","ALG","ALGF0","AMP","AMPF0","ANC","ANT","ATD","ATO","ATOF0","AVAX","AVAXC","AVAXF0","AXS","AXSF0","B21X","BAL","BAND","BAT","BBB","BCH","BCHABC","BCHN","BEST","BFT","BFX","BG1","BG2","BMI","BNT","BOBA","BOSON","BSV","BTC","BTCDOMF0","BTCF0","BTG","BTSE","BTT","CAD","CEL","CHEX","CHF","CHSB","CHZ","CLO","CNH","CNHT","COMP","COMPF0","CRV","CRVF0","CSTBCHABC","CSTBCHN","CTK","DAI","DAT","DCR","DGB","DOG","DOGE","DOGEF0","DORA","DOT","DOTF0","DRK","DSH","DUSK","DVF","EDO","EGLD","EGLDF0","ENJ","EOS","EOSDT","EOSF0","ESS","ETC","ETH","ETH2P","ETH2R","ETH2X","ETHF0","ETP","EUR","EURF0","EUROPE50IXF0","EUS","EUT","EUTF0","EXO","EXRD","FCL","FET","FIL","FILF0","FORTH","FTM","FTMF0","FTT","FUN","GBP","GBPF0","GERMANY30IXF0","GERMANY40IXF0","GNO","GNT","GOT","GRT","GTX","HEZ","HKD","HMT","ICE","ICP","IDX","IOT","IOTF0","IQX","JASMY","JPY","JPYF0","JST","KAN","KNC","KSM","LBT","LEO","LES","LET","LINK","LINKF0","LNX","LRC","LTC","LTCF0","LUNA","LUNAF0","LYM","MATIC","MATICF0","MIM","MIR","MKR","MLN","MNA","MOB","MTO","NEAR","NEC","NEO","NEOF0","NEXO","OCEAN","ODE","OMG","OMGF0","OMN","ORS","OXY","PANTEOS","PAS","PAX","PBALEOS","PBANDEOS","PBATEOS","PBTCEOS","PBTCETH","PCOMPEOS","PDAIEOS","PEOSETH","PETHEOS","PLANETS","PLINKEOS","PLRCEOS","PLTCEOS","PLTCETH","PLU","PMKREOS","PNK","POMGEOS","PPNKEOS","PPNTEOS","PREPEOS","PSNXEOS","PUNIEOS","PUOSEOS","PYFIEOS","PZRXEOS","QRDO","QSH","QTF","QTM","RBT","REEF","REP","REQ","RINGX","ROSE","RRB","RRT","SAN","SGD","SHIB","SHIBF0","SNG","SNT","SNX","SOL","SOLF0","SPELL","SRM","STJ","SUKU","SUN","SUSHI","SUSHIF0","TERRAUST","TESTBTC","TESTBTCF0","TESTUSD","TESTUSDT","TESTUSDTF0","THB","THETA","TLOS","TRX","TRXF0","TSD","TWD","UDC","UNI","UNIF0","UOP","UOS","USD","UST","USTF0","UTK","VEE","VELO","VET","VSY","WAVES","WAX","WBT","WILD","WNCG","XAGF0","XAUT","XAUTF0","XCH","XDC","XLM","XLMF0","XMR","XMRF0","XRA","XRD","XRP","XRPF0","XSN","XTZ","XTZF0","XVG","YFI","YGG","ZCN","ZEC","ZECF0","ZIL","ZMT","ZRX"]]
        )JSON"
    };
    rapidjson::Document document;
    try {
        document.SetNull();
        document.GetAllocator().Clear();
        document.Parse<rapidjson::kParseDefaultFlags>(
        //document.Parse<rapidjson::kParseNumbersAsStringsFlag>(
            currency_in.data(), currency_in.size());
        json_dto::check_document_parse_status(document);
    }
    catch (const std::exception &e) {
        std::cout << "error parsing " << e.what()
                  << " on " << currency_in << std::endl;
        return;
    }
    try {
        //assert(document.IsObject()); // throw
        assert(document.IsArray ());
        // const rapidjson::Value& root = document;
        // for (rapidjson::Value::ConstMemberIterator itr = root.MemberBegin(); itr != root.MemberEnd(); ++itr) 
        //     printf("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
        std::cout << "document is array with size = " << document.Size() << std::endl;
        using CURRENCIES = std::vector<std::vector<std::string>>;
        CURRENCIES currencies;
        json_dto::from_json<CURRENCIES>(document, currencies);
        if (currencies.size() > 0) {
            std::cout << "currencies size = " << currencies[0].size() << std::endl;
            for(size_t i = 0; i < currencies[0].size(); ++i) {
                std::cout << "idx " << i << " = " << currencies[0][i] << std::endl;
            }
        } else
            std::cout << "failed to parse currencies" << std::endl;

    }
    catch (const std::exception &e) {
        std::cout << "json_dto error: e= " << e.what()
                  << std::endl;
    }
}

int main(int argc, char** argv) {
    test_array_of_array();
    rapidjson::Document document;
    try {
        document.SetNull();
        document.GetAllocator().Clear();
        document.Parse<rapidjson::kParseDefaultFlags>(
        //document.Parse<rapidjson::kParseNumbersAsStringsFlag>(
            json_orderbook_snap_in.data(), json_orderbook_snap_in.size());
        json_dto::check_document_parse_status(document);
    }
    catch (const std::exception &e) {
        std::cout << "error parsing " << e.what()
                  << " on " << json_orderbook_snap_in << std::endl;
        return -1;
    }
    try {
        //assert(document.IsObject()); // throw
        assert(document.IsArray ());
        std::cout << "document is array with size = " << document.Size() << std::endl;

        const rapidjson::Value& root = document;
        assert(root.IsArray ());
        // const rapidjson::Value& root = document;
        // for (rapidjson::Value::ConstMemberIterator itr = root.MemberBegin(); itr != root.MemberEnd(); ++itr) 
        //     printf("Type of member %s is %s\n", itr->name.GetString(), kTypeNames[itr->value.GetType()]);
        std::cout << "root is array with size = " << root.Size() << std::endl;
       
        snap_t snap;
        for(rapidjson::SizeType i = 0; i < document.Size(); ++i) {
            std::cout << "type of idx " << i << " = " << kTypeNames[document[i].GetType()] << std::endl;
            if (i == 0) {
                std::cout << "idx 0 =" << document[i].GetInt() << std::endl;
                snap.channel_id = document[i].GetInt();
            } else if (i == 1){
                std::cout << "idx 1 is array? " << document[i].IsArray() << std::endl;
                if (document[i].IsArray()) {
                    const auto& arr =  document[i].GetArray();
                    std::cout << "array size =  " << arr.Size() << std::endl;
                    for(const auto&e: document[i].GetArray()) {
                        std::cout << "e is array? " << e.IsArray() << std::endl;
                        if (e.IsArray()) {
                            snap.levels.emplace_back(e[0].GetUint64(), e[1].GetUint(), e[2].GetDouble());
                        }
                    }
                }
            }
        }
        // for (auto &e: document.getArray()) {
        //     assert(e.IsObject());
        // }
        //std::cout << "document string=" << document.GetString() << std::endl;
        //json_dto::from_json<snap_t>(document, snap);

        std::cout << "parsed: channel_id= " << snap.channel_id
                  << ", levels =" << snap.levels.size() 
                  << std::endl;
        for(size_t i = 0; i < snap.levels.size(); ++i) {
            std::cout << "idx " << i << ": "
                << "price = " << snap.levels[i].price
                << " , num = " << snap.levels[i].order_num
	            << ", size = " << snap.levels[i].size
                << std::endl;
        }
    } catch (const std::exception &e) {
        std::cout << "json_dto error: e= " << e.what()
                  << std::endl;
        return -2;
    }

    test_enum();
    return 0;
}