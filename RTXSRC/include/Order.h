#pragma once

#include <string>
#include <memory>
#include "Price.h"
class ExecutionReport;

struct TimeInForce
{
    static const int TIF_Day            = 0;        //'0'	Day (or session)
    static const int TIF_GTC            = 1;        //'1'	Good Till Cancel (GTC)
    static const int TIF_OPG            = 2;        //'2'	At the Opening (OPG)
    static const int TIF_IOC            = 3;        //'3'	Immediate Or Cancel (IOC)
    static const int TIF_FOK            = 4;        //'4'	Fill Or Kill (FOK)
    static const int TIF_GTX            = 5;        //'5'	Good Till Crossing (GTX)
    static const int TIF_GTD            = 6;        //'6'	Good Till Date (GTD)
    static const int TIF_CLS            = 7;        //'7'	At the Close
};

struct SessionType
{
    static const int SESSION_TYPE_PRE_MARKET            = 0;
    static const int SESSION_TYPE_IN_MARKET             = 1;
    static const int SESSION_TYPE_POST_MARKET           = 2;
};

struct OrderType
{
    static const int TYPE_MARKET            = 1;
    static const int TYPE_LIMIT             = 2 ;
    static const int TYPE_STOP_MARKET       = 3;
    static const int TYPE_STOP_LIMIT        = 4;
    static const int TYPE_INVALID           = 0xFF ;
};

struct OrderSide
{
    static const int SIDE_BUY               = 1 ;
    static const int SIDE_SELL              = 2 ;
    static const int SIDE_SHORT             = 5 ;
};

class Order
{
public:
    Order(){}
    Order(const Order& odr);
    Order* Clone() const {return new Order(*this);}
    std::shared_ptr<ExecutionReport> CreatER();

    int         orderID_;       //parent order id
    std::string childOrderId_;  //maybe not for parent order
    std::string account_;       // tag 1
    std::string subAccount_;    // 50
    std::string logInId_;       // 116
    std::string cltOrderId_;

    std::string symbol_;
    int         side_;
    int         size_;
    int         displaySize_{0}; //maxFloor tag 111
    int         type_;           // limit, market, etc
    Price       price_;          //Price is double type, but easy to compare
    Price       discretion_;     // tag 389
    int         TIF_;            //TimeInForce
    long        expireTime_{0};	 // tag 126
    std::string exchange_;	     // tag 100, only used by child order, ignore for parent order

    std::string text_;
    int         securityType_;   //equity, option ...
    int         tradingSessionID_;
    mutable long transactTime_;

    //std::vector< std::string >  execInst_;      // tag 18
    std::string execInst_;       // tag 18
    int         minQty_{0};      //tag 110
    //// for internal use
    std::string routeName_;
};
