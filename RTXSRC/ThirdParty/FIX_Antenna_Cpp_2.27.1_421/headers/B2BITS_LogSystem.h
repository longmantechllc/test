// <copyright>
//
// $Revision: 42192 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#ifndef __B2BITS_Utils_Log__LogSystem_h__
#define __B2BITS_Utils_Log__LogSystem_h__

/// @file B2BITS_LogSystem.h
/// Contains Utils::Log::LogSystem class definition

#ifdef _WIN32
# pragma warning(push)
# pragma warning(disable: 4100)
#endif // _WIN32

#include <iosfwd>
#include <map>
#include <string>

#include <B2BITS_V12_Defines.h>

/** Utility namespace */
namespace Utils
{
    class Properties;

    namespace LogImpl
    {
        class LogSystemImpl;
    }

    /** Logging system namespace */
    namespace Log
    {
        struct DeviceObserver;
        class LogCategory;

        /**
        * Log system manager.
        * Acts as a factory and manager for the categories.
        * Either LogSystem::create() or FixEngine::init() method must be called before usage.
        */
        class V12_API LogSystem
        {
        public:
            typedef std::map<std::string, std::string> LogProperties;

            /**
            * Creates and initializes log system.
            * @param settings map of property names and values. Cannot be NULL.
            * @param pObserver device observer.
            *
            * @exception Utils::Exception if input parameter is incorrect.
            *
            * @note If you are using this method to access logger before FixEngine::init
            * call, FixEngine should be initialized with Engine::FixEngine::InitParameters::initUtilsLogger_ = false
            * 
            * @see Engine::FixEngine::InitParameters
            * @see Engine::FixEngine::init( Engine::FixEngine::InitParameters const& )
            */
            static void create( const LogProperties& properties, DeviceObserver* pObserver = NULL );

            /**
            * Creates and initializes log system.
            * @param prop Set of properties. Cannot be NULL.
            * @param pObserver device observer.
            *
            * @exception Utils::Exception if input parameter is incorrect.
            *
            * @note If you are using this method to access logger before FixEngine::init
            * call, FixEngine should be initialized with Engine::FixEngine::InitParameters::initUtilsLogger_ = false
            * 
            * @see Engine::FixEngine::InitParameters
            * @see Engine::FixEngine::init( Engine::FixEngine::InitParameters const& )
            */
            static void create( const Utils::Properties* prop, DeviceObserver* pObserver = NULL );

            /**
            * Creates and initializes log system.
            * @param prop Set of properties. Cannot be NULL.
            * @param pObserver device observer.
            *
            * @exception Utils::Exception if input parameter is incorrect.
            *
            * @note If you are using this method to access logger before FixEngine::init
            * call, FixEngine should be initialized with Engine::FixEngine::InitParameters::initUtilsLogger_ = false
            * 
            * @see Engine::FixEngine::InitParameters
            * @see Engine::FixEngine::init( Engine::FixEngine::InitParameters const& )
            */
            static void create( const Utils::Properties& prop, DeviceObserver* pObserver = NULL );

            /**
            * Creates and initializes log system.
            * @param settingsFileName file with properties. Cannot be NULL.
            * @param pObserver device observer.
            *
            * @exception Utils::Exception if input parameter is incorrect.
            *
            * @note If you are using this method to access logger before FixEngine::init
            * call, FixEngine should be initialized with Engine::FixEngine::InitParameters::initUtilsLogger_ = false
            * 
            * @see Engine::FixEngine::InitParameters
            * @see Engine::FixEngine::init( Engine::FixEngine::InitParameters const& )
            */
            static void create( const std::string& settingsFileName, DeviceObserver* pObserver = NULL );

            /**
            * Creates and initializes log system.
            * @param settings stream with properties. Cannot be NULL.
            * @param pObserver device observer.
            *
            * @exception Utils::Exception if input parameter is incorrect.
            *
            * @note If you are using this method to access logger before FixEngine::init
            * call, FixEngine should be initialized with Engine::FixEngine::InitParameters::initUtilsLogger_ = false
            * 
            * @see Engine::FixEngine::InitParameters
            * @see Engine::FixEngine::init( Engine::FixEngine::InitParameters const& )
            */
            static void create( std::istream& settings, DeviceObserver* pObserver = NULL );

            /**
            * Creates new category.
            * @param name Category name.
            * @exception Utils::Exception if category properties are incorrect
            * or none of the specified for this category devices are supported
            * by the used platform.
            */
            LogCategory* createCategory( const std::string& name );

            /**
            * Creates new fake category. All its methods are empty.
            */
            LogCategory* createFakeCategory();

            /** Appends passed text to special category */
            void logVersion( std::string const& text );

            /**
            * Attaches Device Observer to get report about critical errors with logging device.
            * After event processing Device can be detached from logging 
            * or continues to work (depends of the Device Observer's reaction).
            * @param pObserver - device observer.
            */
            void attach( DeviceObserver* pObserver );

            /** Detachs Device Observer. */
            void detach();

            /** Destroys LogSystem instance. */
            static void destroy();

            /** Returns singleton. */
            static LogSystem* singleton();

            /** Returns true, if log system is initizlized. */
            static bool isCreated();

        protected:
            /** Constructor */
            LogSystem();

            /** Destructor. */
            virtual ~LogSystem();

        private:
            /** Copy constructor is forbidden */
            LogSystem( const LogSystem& );

            /** Assignment operator is forbidden */
            LogSystem& operator =( const LogSystem& right );

            /** Creates LogSystem instance. */
            static void create();

        private:
            /** Implementation */
            ::Utils::LogImpl::LogSystemImpl* pImpl_;
            /** Singleton */
            static LogSystem* instance_;
        };  // class LogSystem
    } // namespace Log
} // namespace Utils

#ifdef _WIN32
# pragma warning(pop)
#endif // _WIN32

#endif // __B2BITS_Utils_Log__LogSystem_h__
