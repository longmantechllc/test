//
// Created by schen on 12/29/2020.
//

#include <string>
#include <memory>
#include <vector>
#include <set>
#include <algorithm>

#include "../include/Price.h"
#include "../include/Order.h"

#include "../include/TopOfBook.h"
#include "TOBQuote.h"
#include "Logger.h"

#include "TobList.h"

TobList::TobList()
{

}

TobList::~TobList()
{
    m_quoteList.clear();
}

void TobList::SetSide(int side)
{
    if(side == OrderSide::SIDE_BUY)
        m_isAsk = true;
    else
        m_isAsk = false;
}

void TobList::SetLog(Logger* log)
{
    m_logger = log;
}

void TobList::AddTob(const TopOfBook& tob, const int pri)
{
    auto mmid = tob.m_mpid;
    Price px;
    int qty;
    if(m_isAsk)
    {
        px = tob.m_askPrice;
        qty = tob.m_askSize;
    }
    else
    {
        px = tob.m_bidPrice;
        qty = tob.m_bidSize;
    }
    if(qty <= 0 || px.isLE(Price(0.0)))
        return;
    std::vector<std::shared_ptr< TobQuote >>::iterator itr2;
    bool isPas = (m_definedMmid.find(mmid) == m_definedMmid.end());

    auto askCmp = [&](auto& q1, auto& q2)
    {
        if (q1->m_px < q2->m_px) return true;
        else if (q1->m_px > q2->m_px) return false;

        if (q1->m_pri > q2->m_pri) return true;
        else return false;
    };
    auto bidCmp = [&](auto& q1, auto& q2)
    {
        if (q1->m_px > q2->m_px) return true;
        else if(q1->m_px < q2->m_px) return false;

        if(q1->m_pri > q2->m_pri) return true;
        else return false;
    };
    auto mmidFind = [&](auto& q){return (q->m_mpid == mmid);};

//    std::unique_lock<std::mutex> lock(lock_);
    {
        if (!isPas) {
//        auto itr = std::find_if(m_quoteList.begin(), m_quoteList.end(), [&](std::shared_ptr< TobQuote > q){return (q->m_mpid == mmid);});
            auto itr = std::find_if(m_quoteList.begin(), m_quoteList.end(), mmidFind);
            if (itr != m_quoteList.end()) {
                if ((*itr)->m_px == px) {
                    (*itr)->SetTob(px, qty, pri);
                    return;
                }
                m_quoteList.erase(itr);
            }
            auto qt = std::make_shared<TobQuote>(tob, px, qty, pri);
            if (m_isAsk) {
//            itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, [&](const std::shared_ptr< TobQuote >& q1, const std::shared_ptr< TobQuote >& q2)
                itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, askCmp);
            } else {
//            itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, [&](const std::shared_ptr< TobQuote >& q1, const std::shared_ptr< TobQuote >& q2)
                itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, bidCmp);
            }
            m_quoteList.insert(itr2, qt);
            return;
        }
        mmid = "XXXX";
        auto qt = std::make_shared<TobQuote>();
        auto itr = std::find_if(m_quoteList.begin(), m_quoteList.end(), mmidFind);
        if (itr == m_quoteList.end()) {
            qt->AddToQuote(tob, px, qty, pri, true);
            if (m_isAsk)
                itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, askCmp);
            else
                itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, bidCmp);

            m_quoteList.insert(itr2, qt);
        } else {
            if (m_isAsk) {
                if (px < itr->get()->m_px) { //better
                    itr = m_quoteList.erase(itr);
                    qt->AddToQuote(tob, px, qty, pri, true);
                    itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, askCmp);
                    m_quoteList.insert(itr2, qt);
                } else if (px == (*itr)->m_px) {  //equal
                    auto tpId = tob.m_mpid;
                    auto subItr = (*itr)->m_subQuoteList.find(tpId);
                    if (subItr != (*itr)->m_subQuoteList.end()) {
                        (*itr)->m_qty -= (*subItr).second->m_qty;
                        (*itr)->m_qty += qty;
                        (*itr)->m_afterConsumedQty -= (*subItr).second->m_qty;
                        (*itr)->m_afterConsumedQty += qty;
                        (*subItr).second->m_qty = qty;
                        if ((*itr)->m_afterConsumedQty <= 0)
                            itr = m_quoteList.erase(itr);
                    } else {
                        (*itr)->AddToQuote(tob, px, qty, pri, true);
                    }
                } else {   // worse
                    auto tpId = tob.m_mpid;
                    auto subItr = (*itr)->m_subQuoteList.find(tpId);
                    if (subItr != (*itr)->m_subQuoteList.end()) {
                        (*itr)->m_qty -= (*subItr).second->m_qty;
                        subItr = (*itr)->m_subQuoteList.erase(subItr);
                        if ((*itr)->m_afterConsumedQty <= 0)
                            itr = m_quoteList.erase(itr);
                    }
                }
            } else    //bid
            {
                if (px > itr->get()->m_px) { //better
                    itr = m_quoteList.erase(itr);
                    qt->AddToQuote(tob, px, qty, pri, true);
                    itr2 = std::lower_bound(m_quoteList.begin(), m_quoteList.end(), qt, bidCmp);
                    m_quoteList.insert(itr2, qt);
                } else if (px == (*itr)->m_px) { //equal
                    auto tpId = tob.m_mpid;
                    auto subItr = (*itr)->m_subQuoteList.find(tpId);
                    if (subItr != (*itr)->m_subQuoteList.end()) {
                        (*itr)->m_qty -= (*subItr).second->m_qty;
                        (*itr)->m_qty += qty;
                        (*itr)->m_afterConsumedQty -= (*subItr).second->m_qty;
                        (*itr)->m_afterConsumedQty += qty;
                        (*subItr).second->m_qty = qty;
                        if ((*itr)->m_afterConsumedQty <= 0)
                            itr = m_quoteList.erase(itr);
                    } else {
                        (*itr)->AddToQuote(tob, px, qty, pri, true);
                    }
                } else {   // worse
                    auto tpId = tob.m_mpid;
                    auto subItr = (*itr)->m_subQuoteList.find(tpId);
                    if (subItr != (*itr)->m_subQuoteList.end()) {
                        (*itr)->m_qty -= (*subItr).second->m_qty;
                        (*itr)->m_afterConsumedQty -= (*subItr).second->m_qty;
                        subItr = (*itr)->m_subQuoteList.erase(subItr);
                        if ((*itr)->m_afterConsumedQty <= 0)
                            itr = m_quoteList.erase(itr);
                    }
                }
            }
        }
    }
}

std::vector <std::shared_ptr< TobQuote >> TobList::GetTobList()
{
    std::vector <std::shared_ptr< TobQuote >> qList;
    for (auto q : m_quoteList) {
        if(q->m_afterConsumedQty <= 0)
            continue;
        else
            qList.push_back(q);
    }
    return qList;
}

std::vector <std::shared_ptr< TobQuote >> TobList::GetBestPriceQuoteList()
{
    std::vector <std::shared_ptr< TobQuote >> qList;
    bool isFirst = true;
    Price bestPrice;

//    std::unique_lock<std::mutex> lock(lock_);
    {
        for (auto q : m_quoteList) {
            if(q->m_afterConsumedQty <= 0)
                continue;
            if (isFirst == true) {
                bestPrice = q->m_px;
                isFirst = false;
            }
            if (q->m_px == bestPrice)
                qList.push_back(q);
            else
                break;
        }
    }
    return qList;
}

Price TobList::GetBestQuotePrice()
{
//    std::unique_lock<std::mutex> lock(lock_);
    {
        if (!m_quoteList.empty()) {
//            return m_quoteList[0]->m_px;
            for(auto q : m_quoteList)
            {
                if(q->m_afterConsumedQty > 0)
                    return q->m_px;
            }
        }
    }
    return Price(-9999.99);
}

void TobList::RemoveQuoteByMmid(const std::string& mmid)
{
    m_logger->logInfo("RemoveQuoteByMmid 1: mmid: %s, size: %d", mmid.c_str(), m_quoteList.size());

//    std::unique_lock<std::mutex> lock(lock_);
    auto itr = std::find_if(m_quoteList.begin(), m_quoteList.end(), [&](auto& q){if(q) return (q->m_mpid == mmid); return false;});
    if(itr != m_quoteList.end())
    {
        itr = m_quoteList.erase(itr);
//        SetQuoteConsumedByMmid(mmid, -1);
    }
}

void TobList::RemoveQuoteByMmidList(const std::vector<std::string>& mmidList)
{
//    m_logger->logInfo("RemoveQuoteByMmidList 1: size: %d", mmidList.size());
    for(auto mmid : mmidList)
    {
//        m_logger->logInfo("RemoveQuoteByMmidList 1: mmid: %s.", mmid.c_str());
//        RemoveQuoteByMmid(mmid);
        SetQuoteConsumedByMmid(mmid, -1);
    }
}

void TobList::SetQuoteConsumedByMmid(const std::string& mmid, int consumedQty)
{
//    m_logger->logInfo("SetQuoteConsumedByMmid 1: mmid: %s, consumedQty: %d, m_quoteList: %d", mmid.c_str(), consumedQty, m_quoteList.size());
//    std::unique_lock<std::mutex> lock(lock_);
    auto itr = std::find_if(m_quoteList.begin(), m_quoteList.end(), [&](auto& q){if(q) return (q->m_mpid == mmid); return false;});
//    m_logger->logInfo("SetQuoteConsumedByMmid 2: after std::find_if()");
    if(itr != m_quoteList.end())
    {
//        m_logger->logInfo("SetQuoteConsumedByMmid 3: itr->mmid: %s, itr->consumedQty: %d.", (*itr)->m_mpid.c_str(),  (*itr)->m_afterConsumedQty);

        if(consumedQty == -1)
            (*itr)->m_afterConsumedQty = 0;
        else
            (*itr)->m_afterConsumedQty -= consumedQty;
//        if((*itr)->m_afterConsumedQty <= 0)
//            itr = m_quoteList.erase(itr);
//        m_logger->logInfo("SetQuoteConsumedByMmid 4: itr->mmid: %s, itr->consumedQty: %d.", (*itr)->m_mpid.c_str(),  (*itr)->m_afterConsumedQty);
    }
}