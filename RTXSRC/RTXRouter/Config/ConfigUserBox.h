//
// Created by schen on 11/16/2020.
//

#ifndef RTXROUTER_CONFIGUSERBOX_H
#define RTXROUTER_CONFIGUSERBOX_H

using namespace std;
class ConfigUserBox;

struct User
{
    User();
    virtual ~User()
    {
        excludeVenueList.clear();
        excludeRouteList.clear();
    }
    void AddNameList(std::string val);
    unsigned int SetSessionId(const std::string& val);
    void SetStartTime(const std::string& val);

    void AddExcludeVenue(ConfigUserBox* prnt, std::string val);
    std::string AddExcludeVenue(std::string val);
    std::string RemoveExcludeVenue(std::string val);

    void AddExcludeRoute(ConfigUserBox* prnt, std::string val);
    void AddExcludeRetailMmid(ConfigUserBox* prnt, std::string val);
    void SetAcceptMarketOrder(bool value){acceptMarketOrder = value;}

    unsigned int type=-1;                            //0 - login, 1 - account
    bool acceptMarketOrder=true;
    std::string startTimeStr;
    std::time_t startTime=0;                        //9:30:05 -->some integer
    unsigned int sessionId=0;                       //1="1", 2="2", 3="3", 4="1,2", 5="2,3", 6="1,2,3"
    Price retailMPI = Price(0.0);
    Price retailMPFOF = Price(0.0);
    int retailpIWeight = 1;
    int retailpfoFWeight = 1;
    int retailBDCM = 0;
    int retailUseOrderLimitPrice = -1;

    std::unordered_set< std::string > userNameList;
    std::unordered_set< std::string > excludeVenueList;         //<excludeVenue>TEST, NYSE</excludeVenue>
    std::unordered_set< std::string > excludeRouteList;         //<excludeRoute>TEST-TEST, NYSE-NYSE</excludeRoute>
    std::unordered_set< std::string > excludeRetailMmidList;    //<excludeRetailMmid>CTDL,HDSN</excludeRetailMmid>

    std::unordered_map <std::string /*SWP*/, int> argosMp ;
    void InitArgo();
    void AddArgo(const std::string& val, std::string& strVal);
    std::unordered_map <std::string /*DarkSweep*/, int>& Argos() { return argosMp; }

};

class ConfigUserBox
{
public:
    ConfigUserBox();
    virtual ~ConfigUserBox();
    void InitConfigUserBox();

    void AddUser(const unordered_set<std::string>& name, const std::shared_ptr<User>& user);
    const std::shared_ptr<User>& GetUserConfig(const unsigned int userType, const std::string& name);

    void AddAcceptMarketOrder(const std::unordered_set< std::string >& users, std::string val);
    void AddStartTime(const std::unordered_set< std::string >& users, std::string val);
    void AddSessionId(const std::unordered_set< std::string >& users, std::string val, int deflt=0);
    void AddSessionId(const std::unordered_set< std::string >& users, unsigned int val, int deflt=0);

    std::unordered_map<std::string, bool> acceptMarketOrder;
    std::unordered_map<std::string, int> startTimeSsm;
    std::unordered_map<std::string, unsigned int> sessionId;

    std::unordered_map<std::string, std::unordered_set<std::string>> excludeVenueDict;
    std::unordered_map<std::string, std::unordered_set<std::string>> excludeRouteDict;

    std::unordered_map<std::string, std::shared_ptr<User>> userList;
    std::unordered_map<std::string, std::shared_ptr<User>> accountList;
    std::shared_ptr<User> m_EmptyUser;
};

#endif //RTXROUTER_CONFIGUSERBOX_H
