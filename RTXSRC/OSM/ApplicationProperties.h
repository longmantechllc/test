#pragma once

#include <string>
#include <vector>

/// Application parameters
struct ApplicationProperties
{
    std::string acceptor_cfg, initiator_cfg;
    bool sendPendingCancelReplace{true}, sendPendingCancel{true};
    std::string customLogon;
    std::string routerName{"RTX"};
    std::string virtualDestFile;
    uint32_t mdCheckingTime {1000}; // in milliseconds
    int adminPort {6636}; //
    bool extraLogging {false};
    bool recoveryMode {false};
    std::string mdClientName;
    std::string ioiServer;
    int ioiPort;
    std::string dbHost;
    int dbPort;
    std::string dbUser;
    std::string dbPassword;
    bool dbLogging {false};
    bool simMD {false};
    std::string symbolFile;
    std::string exchs;
};

