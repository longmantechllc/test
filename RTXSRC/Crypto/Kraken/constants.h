#pragma once

#include <string_view>
#include "json_dto/pub.hpp"

namespace Crypto::kraken {
static constexpr std::string_view INVALID_STRING_VIEW = "N/A";

namespace WS {

    enum class ResponseType : std::uint8_t {
        INVALID_TYPE = 0,
        error,
        subscriptions,
        snapshot,
        l2update,
        heartbeat,
        status,
        ticker,
        received,
        open,
        done,
        match,
        change,
        activate
    };
}

}  // namespace Crypto::kraken

namespace json_dto {

#define read_json_for_enum(enum_type) \
template<> \
void read_json_value(enum_type &v, const rapidjson::Value &object)

read_json_for_enum(Crypto::kraken::WS::ResponseType);

#undef read_json_for_enum
}  // namespace json_dto