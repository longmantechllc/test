#include "binacpp_websocket.h"
#include "MDCache.h"
#include "magic_enum/magic_enum.hpp"
#include "orderbook_response.h"

struct lws_context *BinaCPP_websocket::context = nullptr;
//--------------------------
int 
BinaCPP_websocket::event_cb( struct lws *wsi, enum lws_callback_reasons reason, void *user, void *in, size_t len )
{

	auto *connection = reinterpret_cast<BinaCPP_websocket *> (user);
#ifndef NDEBUG
	spdlog::debug("user {}, lws {}, reason {}: in len {}, data {}", 
                  user,  (void *)wsi, magic_enum::enum_name(reason),
				  len, len>0? (char*)in: "");
#endif
	switch( reason )
	{
		case LWS_CALLBACK_CLIENT_ESTABLISHED:
			lws_callback_on_writable( wsi );
			//connection->on_connect(wsi);
			break;

		case LWS_CALLBACK_CLIENT_RECEIVE:
			lws_validity_confirmed(wsi);
			/* Handle incomming messages here. */
			try {
				spdlog::info( "recevied data for idx {}, symbol {}, book {}",
					connection->m_idx, connection->m_symbol, (void *)&connection->m_book);

				connection->cb(std::string_view((char *)in, len));

			} catch ( exception &e ) {
		 		spdlog::error( "<BinaCPP_websocket::event_cb> Error ! {}", e.what() ); 
			}   	
			break;

		case LWS_CALLBACK_CLIENT_WRITEABLE:
		{
			//spdlog::info( "LWS_CALLBACK_CLIENT_WRITEABLE wsi {}", (void *)wsi);
			break;
		}

		case LWS_CALLBACK_CLOSED_CLIENT_HTTP: 
		case LWS_CALLBACK_CLOSED:
		case LWS_CALLBACK_CLIENT_CONNECTION_ERROR:
			//spdlog::info( "LWS_CALLBACK_CLIENT_CONNECTION_ERROR wsi {}", (void *)wsi);
			// connection->on_disconnect();
			break;

		default:
		 	// spdlog::info("<event_cb> [{}]: notice {}", 
            //         (void *)wsi, magic_enum::enum_name(reason));
			break;
	}

	return 0;
}


//-------------------
void 
BinaCPP_websocket::init( ) 
{
	if (context != nullptr)
		return;
	struct lws_context_creation_info info;
	memset( &info, 0, sizeof(info) );

	info.port = CONTEXT_PORT_NO_LISTEN;
	info.protocols = protocols;
	info.gid = -1;
	info.uid = -1;
	info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;

	context = lws_create_context( &info );
	spdlog::info("BinaCPP_websocket::init on exch symbol {}, lws_context {}",
		m_symbol, (void *)context); 
}


//----------------------------
// Register call backs
void
BinaCPP_websocket::connect_endpoint ( 
		const char *path
	) 
{
	//char ws_path[1024];
	//strcpy( ws_path, path );
	
	
	/* Connect if we are not connected to the server. */
	struct lws_client_connect_info ccinfo = {0};
	ccinfo.context 	= context;
	ccinfo.address 	= BINANCE_WS_HOST;
	ccinfo.port 	= BINANCE_WS_PORT;
	ccinfo.path 	= path; //ws_path;
	ccinfo.host 	= lws_canonical_hostname( context );
	ccinfo.origin 	= "origin";
	ccinfo.protocol = protocols[0].name;
	ccinfo.ssl_connection = LCCSCF_USE_SSL | LCCSCF_ALLOW_SELFSIGNED | LCCSCF_SKIP_SERVER_CERT_HOSTNAME_CHECK;
	ccinfo.userdata = (void *) this;

	/*struct lws* */conn = lws_client_connect_via_info(&ccinfo);
	spdlog::info("<connect_endpoint> : idx={}, lws={}, path {}",
			m_idx, (void *)conn, path);

}

namespace {
	std::map < string, map <double,double> >  depthCache;
	uint64_t  lastUpdateId;


	void print_depthCache() {

			map < string, map <double,double> >::iterator it_i;

			for ( it_i = depthCache.begin() ; it_i != depthCache.end() ; it_i++ ) {

					string bid_or_ask = (*it_i).first ;
					cout << bid_or_ask << endl ;
					cout << "Price             Qty" << endl ;

					map <double,double>::reverse_iterator it_j;

					for ( it_j = depthCache[bid_or_ask].rbegin() ; it_j != depthCache[bid_or_ask].rend() ; it_j++ ) {

							double price = (*it_j).first;
							double qty   = (*it_j).second;
							printf("%.08f          %.08f\n", price, qty );
					}
			}
	}
}

static std::optional<double> cov_sv_to_double(const std::string_view & input)
{
	try {
		char* p_end;
		double out = std::strtod(input.data(), &p_end);
		return out;
	} catch(const std::exception &e) {
		return std::nullopt;
	}
}

void BinaCPP_websocket::cb(std::string_view in) {
	try {
        this->document_.SetNull();
        this->document_.GetAllocator().Clear();
        this->document_.Parse<rapidjson::kParseDefaultFlags>(
            in.data(), in.size());
        json_dto::check_document_parse_status(this->document_);
    } catch (const std::exception &e) {
        spdlog::error("<BinaCPP_websocket::cb> : error parsing {} on {}", 
                     e.what(), in);
    }

    try {
        static Crypto::binance::orderbook_response_t book;
        book.reset();
        json_dto::from_json(this->document_, book);
		auto add_it = [&](auto& side, auto& input) {
			side.clear();
			for(const auto& price_point: input) {
				auto p = cov_sv_to_double(price_point[0]);
				auto q = cov_sv_to_double(price_point[1]);
				if (p && q)
					side.emplace(*p, *q);
			}
		};
		double o_bid_px{0.0},  o_bid_qty{0.0};
		double o_ask_px{0.0},  o_ask_qty{0.0};
		if (!m_book.bids.empty()) {
			const auto& it= *m_book.bids.rbegin();
		 	o_bid_px = it.first;
			o_bid_qty = it.second;
		}
		if (!m_book.asks.empty()) {
			const auto& it= *m_book.asks.begin();
		 	o_ask_px = it.first;
			o_ask_qty = it.second;
		}

		add_it(m_book.bids, book.bids);
		add_it(m_book.asks, book.asks);
		if (const auto itor = m_book.bids.rbegin(); o_bid_px != itor->first || o_bid_qty != itor->second)
			m_cache->publish(true, m_book);
		if (const auto itor = m_book.asks.begin(); o_ask_px != itor->first || o_ask_qty != itor->second)
			m_cache->publish(false, m_book);

    } catch (const std::exception &e) {
        spdlog::error("<WsApi::on_private_message> : error {} on {}", e.what(), in);
    }
}
//----------------------------
// Entering event loop
void 
BinaCPP_websocket::service() 
{
	try {	
		lws_service( context, -1 );
	} catch ( exception &e ) {
		spdlog::error( "service idx {}, symbol {} Error ! {}",
			m_idx, m_symbol, e.what() ); 
	}


}


