// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CqgMDApplication.h
/// Contains Cqg::MDApplication class definition

#ifndef H_B2BITS_CQG_MDApplication_H
#define H_B2BITS_CQG_MDApplication_H

#include <B2BITS_CqgDefines.h>

namespace Cqg
{
    class SecurityDefinitionListener;
    class InstrumentListener;

    /// Provides access to the CQG FIX/FAST Direct Market Data feed
    class MDApplication
    {
    public:
        /// Subscribes for market data for instrument given by SecurityID
        /// @param listener Instance of InstrumentListener to receive notifications
        /// @param securityDesc SecurityDescription(107) of instrument
        /// @recoveryMode Kind of recovery
        /// @note SecurityDescription is not unique identifier of instrument, subscription will failed if duplicates exists
        virtual void subscribeBySecurityDescription( InstrumentListener* listener, 
                                                     ASecurityDescription securityDesc,
                                                     RecoveryOptions recoveryMode = RO_USE_MARKET_RECOVERY) = 0;

        /// Subscribes for market data for instrument given by SecurityID
        /// @param listener Instance of InstrumentListener to receive notifications
        /// @param securityID SecurityID(48) of instrument
        /// @param feedID ID of the feed on which instrument is published
        /// @param subChannel Sub-channel identifier. Should be 0 at the moment,
        /// @recoveryMode Kind of recovery
        virtual void subscribeBySecurityID( InstrumentListener* listener, 
                                            ASecurityID securityID, 
                                            AFeedID feedID = FeedID(), 
                                            ASubChannelID subChannel = 0,
                                            RecoveryOptions recoveryMode = RO_USE_MARKET_RECOVERY) = 0;

        /// Stop processing data for specified instrument
        virtual void unsubscribeBySecurityDescription( ASecurityDescription securityDesc ) = 0;

        /// Stop processing data for specified instrument
        virtual void unsubscribeBySecurityID( ASecurityID securityID, AFeedID feedID = FeedID() ) = 0;

        /// Stop data processing for all instruments
        virtual void unsubscribeAll() = 0;

        /// Stops all threads assigned to instance.
        /// Method is non-blocking. Use MDApplication::join to complete the operation
        virtual void stop() = 0;

        /// Waits until all threads and asynchronous operations assigned to instance are completed.
        virtual void join() = 0;

        /// Destroys the object and cleanup all assigned resources.
        virtual void release() const = 0;

    protected:
        /// Virtual destructor.
        /// Use MDApplication::release to destroy the instance.
        virtual ~MDApplication() {}
    }; // class MDApplication {
} // namespace Cqg {

#endif // H_B2BITS_CQG_MDApplication_H

