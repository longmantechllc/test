// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CompilerDefines.h
/// Contains compiler macros

#ifndef _B2BITS_COMPILER_DEFINES__H
#define _B2BITS_COMPILER_DEFINES__H

/// Macro to fix warning about unused variable/parameter
#define B2B_USE_ARG(arg) (void)&arg

/// This macro converts any value to string
#define _B2B_MAKE_STRING(ARG) #ARG
/// This macro converts value of the macro to string
#define B2B_MAKE_STRING(ARG) _B2B_MAKE_STRING(ARG)
/// Convert two arguments to string and concatenates them
#define B2B_STRING_CONCAT(STR1,STR2) _B2B_MAKE_STRING(STR1 ## STR2)

#define _B2B_NO_DEPRECATED_TEXT ". Define B2B_NO_DEPRECATED to disable warning."

#if defined(_MSC_VER) 

#   define B2B_MSC_VER_14_1   1910    // MSVC++ 14.1 (Visual Studio 2017)
#   define B2B_MSC_VER_14_0   1900    // MSVC++ 14.0 (Visual Studio 2015)
#   define B2B_MSC_VER_12_0   1800    // MSVC++ 12.0 (Visual Studio 2013)
#   define B2B_MSC_VER_11_0   1700    // MSVC++ 11.0 (Visual Studio 2012)
#   define B2B_MSC_VER_10_0   1600    // MSVC++ 10.0 (Visual Studio 2010)
#   define B2B_MSC_VER_9_0    1500    // MSVC++ 9.0  (Visual Studio 2008)
#   define B2B_MSC_VER_8_0    1400    // MSVC++ 8.0  (Visual Studio 2005)
#   define B2B_MSC_VER_7_1    1310    // MSVC++ 7.1  (Visual Studio 2003)
#   define B2B_MSC_VER_7_0    1300    // MSVC++ 7.0  (Visual Studio 2002)
#   define B2B_MSC_VER_6_0    1200    
#   define B2B_MSC_VER_5_0    1100    

#   if _MSC_VER >= B2B_MSC_VER_8_0
#       define B2B_NORETURN     __declspec(noreturn)
#       define B2B_DEPRECATED(MSG)  __declspec(deprecated(MSG _B2B_NO_DEPRECATED_TEXT))
#   endif

#   if _MSC_VER >= B2B_MSC_VER_9_0
#       define B2B_OVERRIDE     override
#       define B2B_SEALED       sealed
#   endif // _MSC_VER >= B2B_MSC_VER_9_0

#   define B2B_FORCE_INLINE __forceinline

#   define B2B_BEGIN_PACK_1 __pragma(pack(push, 1))
#   define B2B_END_PACK  __pragma(pack(pop))

#   define B2B_CDECL __cdecl
#   define B2B_TLS __declspec(thread)

#   if _MSC_VER >= B2B_MSC_VER_12_0
#       define B2B_DELETE_FUNCTION = delete
#   endif

#elif defined( __GNUC__ )

#   define B2B_GCC_VER (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

#   if (__cplusplus >= 201103L) || defined(__GXX_EXPERIMENTAL_CXX0X__)
#       define B2B_DELETE_FUNCTION = delete
#   endif

#   if B2B_GCC_VER >= 40504
#       define B2B_DEPRECATED(STR) __attribute__ ((deprecated( STR _B2B_NO_DEPRECATED_TEXT) ))
#   else
#       define B2B_DEPRECATED(STR) __attribute__ ((deprecated))
#   endif

#   if B2B_GCC_VER >= 40600
#       define B2B_GCC_DIAG_DEPRECATED_OFF _Pragma("GCC diagnostic push")\
                                           _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")
#       define B2B_GCC_DIAG_DEPRECATED_ON _Pragma("GCC diagnostic pop")
#   elif B2B_GCC_VER >= 40200
#       define B2B_GCC_DIAG_DEPRECATED_OFF _Pragma("GCC diagnostic ignored \"-Wdeprecated-declarations\"")
#       define B2B_GCC_DIAG_DEPRECATED_ON _Pragma("GCC diagnostic warning \"-Wdeprecated-declarations\"")
#   else
#       define B2B_GCC_DIAG_DEPRECATED_OFF 
#       define B2B_GCC_DIAG_DEPRECATED_ON 
#   endif

#   if __cplusplus >= 201100
#       define B2B_OVERRIDE override
#       define B2B_SEALED final
#   endif

#   define B2B_FORCE_INLINE     __attribute__((always_inline))

#   define B2B_BEGIN_PACK_1    _Pragma("pack(1)")
#   define B2B_END_PACK         _Pragma("pack()")

#   define B2B_LIKELY(expr) (__builtin_expect((expr), 1))
#   define B2B_UNLIKELY(expr) (__builtin_expect((expr), 0))

#   define B2B_CDECL __attribute__((__cdecl__))
#   define B2B_TLS __thread

#elif defined(__SUNPRO_CC)

#   define B2B_BEGIN_PACK_1    _Pragma("pack(1)")
#   define B2B_END_PACK         _Pragma("pack()")

#   define B2B_CDECL __attribute__((__cdecl__))
#   define B2B_TLS __thread

#endif // _MSC_VER >= B2B_MSC_VER_8_0


#ifndef B2B_NORETURN
    /// This attribute tells the compiler that a function does not return. 
    /// As a consequence, the compiler knows that the code following a 
    /// call to a B2B_NORETURN function is unreachable. 
#   define B2B_NORETURN
#endif

#if !defined( B2B_DEPRECATED )
    /// The deprecated declaration lets you specify particular forms 
    /// of function overloads as deprecated, whereas the pragma form 
    /// applies to all overloaded forms of a function name.
#   define B2B_DEPRECATED(MSG)
#elif defined( B2B_NO_DEPRECATED )
#   undef B2B_DEPRECATED
#   define B2B_DEPRECATED(MSG)
#endif

#ifndef B2B_OVERRIDE
    /// The override context-sensitive keyword indicates that a member 
    /// of a type must override a base class or a base interface member. 
    /// If there is no member to override, the compiler generates an error.
#   define B2B_OVERRIDE
#endif

#ifndef B2B_SEALED
    /// B2B_SEALED is a context sensitive macro that indicates a virtual 
    /// member cannot be overridden, or a type cannot be used as a base type.
#   define B2B_SEALED
#endif

#ifndef B2B_DELETE_FUNCTION
    /// B2B_DELETE_FUNCTION is a macro that marks function as deleted.
#   define B2B_DELETE_FUNCTION
#endif // B2B_DELETE_FUNCTION

#ifndef B2B_FORCE_INLINE
    /// The B2B_FORCE_INLINE macro overrides the cost/benefit analysis and 
    /// relies on the judgment of the programmer instead.
#   define B2B_FORCE_INLINE inline
#endif

#ifndef B2B_LIKELY
    /// The B2B_LIKELY macro defines most possible hive of execution flow.
    /// \code
    /// if B2B_LIKELY ( someBoolExpression ) {
    ///    doSomething();
    /// } else {
    ///    doSomethingElse();
    /// }
    /// \endcode
#   define B2B_LIKELY(expr) (expr)
#endif // B2B_LIKELY

#ifndef B2B_UNLIKELY
    /// The B2B_LIKELY macro defines less possible hive of execution flow.
    /// \code
    /// if B2B_LIKELY ( someBoolExpression ) {
    ///    doSomething();
    /// } else {
    ///    doSomethingElse();
    /// }
    /// \endcode
#   define B2B_UNLIKELY(expr) (expr)
#endif // B2B_UNLIKELY

#ifndef B2B_TLS
#   define B2B_TLS THREAD_LOCAL_STORAGE_IS_NOT_IMPLEMENTED
#endif

//
// Sample of preprocessor command to disable warning in the VS about non-standard keyword usage
//

#if _MSC_VER >= 1500
#   pragma warning(push)
    // A keyword was used that is not in the C++ standard, for example, 
    // one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
#endif

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

//
// Sample of preprocessor command to disable warning about deprecated method usage
//

#if _MSC_VER >= 1500
#   pragma warning(push)
    // Depreacted function usage warning
#   pragma warning(disable: 4996)
#elif defined( __GNUC__ )
    B2B_GCC_DIAG_DEPRECATED_OFF
#endif

#if _MSC_VER >= 1500
#   pragma warning(pop)
#elif defined( __GNUC__ )
    B2B_GCC_DIAG_DEPRECATED_ON
#endif

#if defined( __SUNPRO_CC )
#   define FAKE_RETURN(value) return value
#   define B2B_THROW(ReturnValue, ThrowClause) ThrowClause; return ReturnValue
#else
#   define FAKE_RETURN(value) 
#   define B2B_THROW(ReturnValue, ThrowClause) ThrowClause
#endif

#endif // #ifndef _B2BITS_COMPILER_DEFINES__H
