#
# The FIX Engine configuration file.
#
# $Revision: 1.9 $
#

# For UNIX platforms, the filename-separator character is "/".
# For Win32 platforms, the filename-separator character is "\\" or "/".

# The top of the directory tree under which the engine's configuration,
# and log files are kept.
#
# Do NOT add a slash at the end of the directory path.
EngineRoot = .

# Engine's listen port. Must be > 0.
ListenPort = 9007

# Engine's local IP address to bind to. It can be used on a multi-homed host
# for a FIX Engine that will only accept connect requests to one of its addresses.
# If this parameter is commented, the engine will accept connections to any/all 
# local addresses.
# ListenAddress = localhost

# Engine's local IP address to send from. It can be used on a multi-homed host
# for a FIX Engine that will only send IP datagrams from one of its addresses.
# If this parameter is commented, the engine will send IP datagrams from any/all 
# local addresses.
# ConnectAddress = localhost

# Number of threads that serve FIX sessions.
# 
# Changing this value will impact upon the performance of FIX Engine. 
# The recommended value is 10. The value must be integer and greater than zero.
NumberOfWorkers = 10

# This property is the path of the directory in which the logs for all incoming 
# (if LogIncomingMessages is set to "true") and outgoing FIX messages are stored. 
# It is possible to specify a path related to the EngineRoot directory or an absolute path.
# For example if LogDirectory is set to \"logs\" then the real path is $(EngineRoot)/logs.
# The specified directory must exist.
LogDirectory = logs

# This property provides an option to log incoming FIX messages (those received) from 
# a counterparty FIX Engine. They will be stored in the directory specified by 
# the LogDirectory parameter in a file with extension "in".
LogIncomingMessages = false

# This parameter sets the time period after which a session is non-gracefully terminated 
# if a response is not received to a first "Logon" message (message type A). 
# The corresponding Logout message is sent to the counterparty.
# This value is in seconds. 
# The recommended value is 30 seconds for dedicated connections or private networks. 
# Trading connections via the internet will require calibration. 
# If it is set to "0", then the time period is unlimited. 
# The value must be integer and not negative.
LogonTimeFrame = 30

# This parameter sets the time period after which a session is automatically terminated 
# if a response is not received to a "Logout message" (message type 5). 
# This value is in seconds. 
# The recommended value is 10 seconds for dedicated connections or private networks. 
# Trading connections via the internet will require calibration. 
# The value must be integer and greater than 0.
LogoutTimeFrame = 10

# An option not to reset sequence numbers after Logout.
# Logout sender should initiate session recovery by sending Logon message
# with SeqNum = <last outgoing SeqNum> + 1;
# expecting reply Logon with SeqNum = <last incoming SeqNum> + 1.
# If a gap is detected, standard message recovery or gap filling process
# takes place.
IntradayLogoutTolerance = true

# This parameter controls existance of required tags in application level messages. 
# The possible values are "true" and "false". If set to "true" then all application 
# level messages are validated. If set to "false" then the responsibility for message validity 
# rests with the counterparty. Please note that session level messages are validated in 
# all cases. The recommended setting is "true".
MessageMustBeValidated = false

# This parameter specifies the delta (increment) to the Heartbeat interval between
# a TestRequest message being sent by FIX Engine and a Response Heartbeat being received. 
# The session attains a "telecommunication failed state" if no Response Heartbeat message 
# is received after the normal Heartbeat interval plus delta. For example if no message 
# (application or session level) is received during the Heartbeat interval then Engine sends 
# a TestRequest message. If the required Response Heartbeat message is not received during 
# Heartbeat interval plus Delta then the session moves to the state "Telecommunication link 
# failed". This parameter is specified in (Heartbeat Interval/100). The recommended value is 
# twenty percent.
ReasonableTransmissionTime = 20

# FIX Engine has inbuilt FIX message routing capability and  fully supports 
# the "Deliver To On Behalf Of" mechanism as specified by the FIX protocol.
# If this parameter is set to "True" then Engine will redirect FIX messages automatically 
# to other FIX sessions it maintains. 
# If this parameter is set to "False" Engine directs all messages received to the client 
# application.
ThirdPartyRoutingIsEnabled = false

# This parameter provides an option whereby FIX Engine will accept a FIX session for which it 
# has no registered application (an acceptor). If set to "true" Engine establishes a session, 
# all application level messages are rejected (Application Level Reject - type 3). When 
# an application is registered behaviour is as standard. If set to false then Logon messages 
# are ignored.
UnregisteredAcceptor.CreateSession = true 

# This parameter specifies the time interval between attempts to deliver an application level 
# message to a registered client application in the event the application does not confirm 
# receipt and operation upon the message. The value is specified in milliseconds. 
# The value must be integer and greater than 0.
# This parameter is required ony if the DelayedProcessing.MaxDeliveryTries
# parameter is specified.
DelayedProcessing.DeliveryTriesInterval = 500

# This parameter specifies the number of attempts that will be made to deliver an application 
# level message to the registered client application. If this value is exceeded then 
# the session will be closed with the logout reason "Application not available". The recommended value is 10.
# The value must be integer and not negative.
DelayedProcessing.MaxDeliveryTries = 2

# This parameter specifies the number of attempts to restore the session.
# The session is considered as restored if the telecommunication link was
# restored and the exchange of Logon messages was successful.
# If it is set to "-1", then the number of attempts is unlimited. 
# This value is integer. 
# This parameter is optional.
Reconnect.MaxTries = 3

# This parameter specifies the time interval between reconnection attempts in order to restore
# a communications link. This value is specified in milliseconds (seconds*10-3). 
# The recommended value is 1000 for dedicated connections and private networks. 
# Internet connections require calibration. The value must be integer and greater than 0.
Reconnect.Interval = 1000

# The license file name.
LicenseFile=../../../engine.license

# This parameter defines the upper limit to the number of outgoing messages that are resent 
# in the event of a Resend Request. The recommended value is 20000 if no data on mean activity is known.
# WARNING: Changing this value will impact upon the performance of FIX Engine. 
# Larg storage increases application working set.
OutgoingMessagesStorageSize = 1000

# This parameter is an option whereby the version of FIX protocol used for the outgoing message
# is validated against that of the established session. If set to "true" then the application 
# must use the same version of the protocol as the established session otherwise an error 
# occurs. If set to false then the application level message will be sent to the counterparty. 
# The recommended value is "true".
CheckVersionOfOutgoingMessages = false

# If this parameter is true than file streams are flushed after each I/O operation. 
ExtraSafeMode = true

# This parameter allow to automaticaly resolve sequence gap problem. 
# When parameter is true and outgoing SeqNum is 0, session use 141(ResetSeqNumFlag) 
# tag in sending/confirming Logon message to reset seqNum at the initiator or acceptor.
ForceSeqNumReset=2

# Default storage type of the created sessions. By default persistent storage 
# type used. Use "transient" value to use transient storage for the sessions.
#UnregisteredAcceptor.SessionStorageType = 

# This parameter contains name of the XML file with extensions of the FIX protocols.
DictionariesFilesList = ../../../data/fixdic40.xml;../../../data/fixdic41.xml;../../../data/fixdic42.xml;../../../data/fixdic43.xml;../../../data/fixdic44.xml;../../../data/fixdic50.xml;../../../data/fixdic50sp1.xml;../../../data/fixdic50sp2.xml;../../../data/fixdict11.xml

UnregisteredAcceptor.IgnoreSeqNumTooLowAtLogon = true

BackupDirectory = logs/backup

UnregisteredAcceptor.RejectMessageWhileNoConnection = false

ResetSeqNumAfter24hours = false

Validation.CheckRequiredGroupFields = true

MessageTimeToLive = 1000

AllowZeroNumInGroup = false

UnregisteredAcceptor.tcpBufferDisabled = true
UnregisteredAcceptor.maxMessagesAmountInBunch = 100

Log.Device = File
Log.DebugIsOn = true
Log.NoteIsOn = true
Log.WarnIsOn = true
Log.ErrorIsOn = true
Log.FatalIsOn = true
Log.Cycling = false
#Log.File.TimeZone = Local|UTC|EST| +/-0000
Log.File.RootDir = ./logs
Log.File.TimeZone = UTC
Log.File.Locked = false
Log.File.Rotate = false
Log.File.Backup.Time =
Log.File.Name = _log
Log.File.Recreate = true
Log.File.AutoFlush = true
Log.EventLog.EventSource = TestService

# Monitoring
Monitoring.Enable = true

Monitoring.AdminSessionNames = AdminClient
#
## User monitoring tool (TargetCompId = AdminClient)
Monitoring.AdminSession.AdminClient.TargetCompId = AdminClient
Monitoring.AdminSession.AdminClient.Version = FIX44
Monitoring.AdminSession.AdminClient.Username = user
Monitoring.AdminSession.AdminClient.Password = pass
Monitoring.AdminSession.AdminClient.IntradayLogoutToleranceMode = true
Monitoring.AdminSession.AdminClient.ForceSeqNumResetMode = true
Monitoring.AdminSession.AdminClient.IgnoreSeqNumTooLowAtLogon = true
Monitoring.AdminSession.AdminClient.DisableTCPBuffer = false
Monitoring.AdminSession.AdminClient.MaxMessagesAmountInBunch = 10
Monitoring.AdminSession.AdminClient.SocketOpPriority = AGGRESSIVE_SEND
