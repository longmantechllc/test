var searchData=
[
  ['ratesource',['RateSource',['../namespaceFIXFields.html#af9c06242ccbe0bb4ede98ec51b76b9cb',1,'FIXFields']]],
  ['ratesourcetype',['RateSourceType',['../namespaceFIXFields.html#aa006357ffe1159866df2564fff0f5a7f',1,'FIXFields']]],
  ['ratioqty',['RatioQty',['../namespaceFIXFields.html#af96ea98905ac0948568299aa1020f8b5',1,'FIXFields']]],
  ['rawdata',['RawData',['../namespaceFIXFields.html#a98e34a6a5440cdf211bf7b9f145b330d',1,'FIXFields']]],
  ['rawdatalength',['RawDataLength',['../namespaceFIXFields.html#a08a5af74f762401c53ea0d55aeb5a5c6',1,'FIXFields']]],
  ['rawdatatagprocessingstrategies_5f',['rawDataTagProcessingStrategies_',['../structEngine_1_1SessionExtraParameters.html#a93301e51b60935e063b7bbba49806d89',1,'Engine::SessionExtraParameters']]],
  ['readertype',['readerType',['../structBats_1_1RuntimeParameters.html#a766ec3fdb347c921af186af630f0f90f',1,'Bats::RuntimeParameters']]],
  ['reasonable_5ftransmission_5ftime_5fparam',['REASONABLE_TRANSMISSION_TIME_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#aea40900ce45b9470c82e07bc8ae672d3',1,'Engine::FIXPropertiesNames']]],
  ['receiveddeptid',['ReceivedDeptID',['../namespaceFIXFields.html#ad1a7933e0c3adcb05ccd098f44ce3bca',1,'FIXFields']]],
  ['reconnect_5finterval_5fparam',['RECONNECT_INTERVAL_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#aa657a409557412c27de3a233581b3922',1,'Engine::FIXPropertiesNames']]],
  ['reconnect_5fmax_5ftries_5fparam',['RECONNECT_MAX_TRIES_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#ab7dda5bd5ecc85c8a81feaed5d12ecff',1,'Engine::FIXPropertiesNames']]],
  ['reconnectattemptcount_5f',['reconnectAttemptCount_',['../classCqg_1_1MDApplicationParams.html#a38e009e3efcd300893f6a9901af06969',1,'Cqg::MDApplicationParams']]],
  ['reconnectflag_5f',['reconnectFlag_',['../classEngine_1_1LogoutEvent.html#af0b65c5ad765ffc187b5c655e13fc246',1,'Engine::LogoutEvent']]],
  ['reconnectinterval',['ReconnectInterval',['../namespaceEngine_1_1SessionParameters.html#a88e58e7468af33f915483ac6fac9cd1a',1,'Engine::SessionParameters']]],
  ['reconnectinterval_5f',['reconnectInterval_',['../structEngine_1_1SessionExtraParameters.html#a12298ac53de34c061c397494a5176e19',1,'Engine::SessionExtraParameters']]],
  ['reconnectmaxtries',['ReconnectMaxTries',['../namespaceEngine_1_1SessionParameters.html#ae7f5b6b97435d20db211a34b92bb9a83',1,'Engine::SessionParameters']]],
  ['reconnectmaxtries_5f',['reconnectMaxTries_',['../structEngine_1_1SessionExtraParameters.html#ac3a596df0f53406cad7a1aefb7eb8e85',1,'Engine::SessionExtraParameters']]],
  ['recvbuffersize',['recvBufferSize',['../structBats_1_1TCPParams.html#aa57f407e16429125eadea6ca5a218a39',1,'Bats::TCPParams::recvBufferSize()'],['../structBats_1_1UDPParams.html#a8bfa72448ba67cc26e26e1867f893efc',1,'Bats::UDPParams::recvBufferSize()']]],
  ['recvcpuaffinity',['RecvCpuAffinity',['../namespaceEngine_1_1SessionParameters.html#a90409d3e441beae4be35898ade1c6495',1,'Engine::SessionParameters']]],
  ['recvcpuaffinity_5f',['recvCpuAffinity_',['../structEngine_1_1SessionExtraParameters.html#a4de064c6fa2e1461a79922e0184441c7',1,'Engine::SessionExtraParameters']]],
  ['recvtimeoutms',['recvTimeoutMS',['../structBats_1_1TCPParams.html#a2e925c1876bbb324ec83825f19a45050',1,'Bats::TCPParams']]],
  ['recvtimeoutus',['recvTimeoutUS',['../structBats_1_1UDPParams.html#a70961bd164b3eb062c7a67bc2d882718',1,'Bats::UDPParams']]],
  ['redemptiondate',['RedemptionDate',['../namespaceFIXFields.html#a9b8640de79dbf98dd8821acd01988b7d',1,'FIXFields']]],
  ['redemptionprice',['RedemptionPrice',['../namespaceFIXFields.html#a8c028ab4cf06e836e2e2368bdce25a9f',1,'FIXFields']]],
  ['redemptionpricetype',['RedemptionPriceType',['../namespaceFIXFields.html#af0f76721a4d3c527b1edb2622aac4c45',1,'FIXFields']]],
  ['refallocid',['RefAllocID',['../namespaceFIXFields.html#a12fb6488e8460658843864d2f99c4383',1,'FIXFields']]],
  ['refapplextid',['RefApplExtID',['../namespaceFIXFields.html#aeccaef7cccf2b28ee761faf175c92fa7',1,'FIXFields']]],
  ['refapplid',['RefApplID',['../namespaceFIXFields.html#a8d1376c0d583eb994338718388ef7601',1,'FIXFields']]],
  ['refappllastseqnum',['RefApplLastSeqNum',['../namespaceFIXFields.html#a9c8c9d1d3368e5c91991c60a0b672d20',1,'FIXFields']]],
  ['refapplreqid',['RefApplReqID',['../namespaceFIXFields.html#a96ffb0c7d4fc94ad7c4b636661c4adcb',1,'FIXFields']]],
  ['refapplverid',['RefApplVerID',['../namespaceFIXFields.html#a664bb113e38adf1a458a3ef0b58a2a05',1,'FIXFields']]],
  ['refcompid',['RefCompID',['../namespaceFIXFields.html#a2fdfd64da6a7c3b9827b763f00737905',1,'FIXFields']]],
  ['refcstmapplverid',['RefCstmApplVerID',['../namespaceFIXFields.html#a54f254201a4ca0fd8017b62451731c77',1,'FIXFields']]],
  ['referencepage',['ReferencePage',['../namespaceFIXFields.html#aa6dc935df33e2c7d7a9c89a82016098a',1,'FIXFields']]],
  ['refmsgtype',['RefMsgType',['../namespaceFIXFields.html#a0bdc1d306d2488c1136d1089f18633db',1,'FIXFields']]],
  ['reforderid',['RefOrderID',['../namespaceFIXFields.html#a77de574791ebcdbecf23d89717439f95',1,'FIXFields']]],
  ['reforderidsource',['RefOrderIDSource',['../namespaceFIXFields.html#acd705c31838c3ddffb72e27f94c1ea8e',1,'FIXFields']]],
  ['refordidreason',['RefOrdIDReason',['../namespaceFIXFields.html#a5c381988f3dc60d34582f766f96b2bb3',1,'FIXFields']]],
  ['refreshindicator',['RefreshIndicator',['../namespaceFIXFields.html#aff3c19292928a81dd23339d3e3aab1d4',1,'FIXFields']]],
  ['refreshqty',['RefreshQty',['../namespaceFIXFields.html#acd5df8e382070d23dfe222c2ccd8809d',1,'FIXFields']]],
  ['refseqnum',['RefSeqNum',['../namespaceFIXFields.html#a99d732b476ca3f4e3a66addd6762df58',1,'FIXFields']]],
  ['refsubid',['RefSubID',['../namespaceFIXFields.html#a3839fe882712cf0babf3e9fb31aa9a78',1,'FIXFields']]],
  ['reftagid',['RefTagID',['../namespaceFIXFields.html#a693ea4b18214a86d05266d8a0e283a3f',1,'FIXFields']]],
  ['registaccttype',['RegistAcctType',['../namespaceFIXFields.html#a29a3df76fe53341b5387788119cbc17b',1,'FIXFields']]],
  ['registdetls',['RegistDetls',['../namespaceFIXFields.html#a38e0f49dd4283fb36ed44c549a5b1265',1,'FIXFields']]],
  ['registdtls',['RegistDtls',['../namespaceFIXFields.html#a9ce21041bec70deb5934afecb671432d',1,'FIXFields']]],
  ['registemail',['RegistEmail',['../namespaceFIXFields.html#a8d47ac2c26967b96557c0c6202b66da3',1,'FIXFields']]],
  ['registid',['RegistID',['../namespaceFIXFields.html#adbef206c7c08b8514703565f536f722f',1,'FIXFields']]],
  ['registrefid',['RegistRefID',['../namespaceFIXFields.html#a7821b50c36dfe9db8880c5ae123bddb0',1,'FIXFields']]],
  ['registrejreasoncode',['RegistRejReasonCode',['../namespaceFIXFields.html#ac67768e6fab6dadbeb2c2931ad465ca1',1,'FIXFields']]],
  ['registrejreasontext',['RegistRejReasonText',['../namespaceFIXFields.html#aeb9c12261d01c3e81dcf39834b9b7ea1',1,'FIXFields']]],
  ['registstatus',['RegistStatus',['../namespaceFIXFields.html#a12564cf1511d81b94e7b5c5f9966edf6',1,'FIXFields']]],
  ['registtranstype',['RegistTransType',['../namespaceFIXFields.html#a757e98c0dd9b52acb42717b8a93c41aa',1,'FIXFields']]],
  ['regshoaction_5f',['regSHOAction_',['../structBats_1_1InstrumentListener_1_1InstrumentCtx.html#a7d7a736484cc68e37107eaab0eeee1ed',1,'Bats::InstrumentListener::InstrumentCtx']]],
  ['rejectflagsupportrequired_5f',['rejectFlagSupportRequired_',['../structEngine_1_1MsgStorage_1_1RestoreOptions.html#a510eeff0835093335262ee81f1ac84f5',1,'Engine::MsgStorage::RestoreOptions']]],
  ['rejecttext',['RejectText',['../namespaceFIXFields.html#a2d69692083aa7ed63263143843230793',1,'FIXFields']]],
  ['relatdsym',['RelatdSym',['../namespaceFIXFields.html#a8c32b964da1709fa6788817c32fcb042',1,'FIXFields']]],
  ['relatedcontextpartyid',['RelatedContextPartyID',['../namespaceFIXFields.html#aad442757936a2c8b7012f1d662f3d8c5',1,'FIXFields']]],
  ['relatedcontextpartyidsource',['RelatedContextPartyIDSource',['../namespaceFIXFields.html#a26f77ccb0be9a1e87336cab0cd055ccc',1,'FIXFields']]],
  ['relatedcontextpartyrole',['RelatedContextPartyRole',['../namespaceFIXFields.html#a01780975c938311ed1312fc1cf66d585',1,'FIXFields']]],
  ['relatedcontextpartysubid',['RelatedContextPartySubID',['../namespaceFIXFields.html#a74dc407021ecb4af7db665f67fe85915',1,'FIXFields']]],
  ['relatedcontextpartysubidtype',['RelatedContextPartySubIDType',['../namespaceFIXFields.html#a575f0a3764fe271a26a0c86ac9dac09d',1,'FIXFields']]],
  ['relatedpartyaltid',['RelatedPartyAltID',['../namespaceFIXFields.html#a5a682591d7fe21e4eeea78f5b0d7326a',1,'FIXFields']]],
  ['relatedpartyaltidsource',['RelatedPartyAltIDSource',['../namespaceFIXFields.html#a9a2cd2655e6eda8210ee77ad81c9542d',1,'FIXFields']]],
  ['relatedpartyaltsubid',['RelatedPartyAltSubID',['../namespaceFIXFields.html#ab7c676da29e7e6bf775976d193dd3752',1,'FIXFields']]],
  ['relatedpartyaltsubidtype',['RelatedPartyAltSubIDType',['../namespaceFIXFields.html#ab6482a165072ff50608c5ef7c828a15f',1,'FIXFields']]],
  ['relatedpartyid',['RelatedPartyID',['../namespaceFIXFields.html#ab47f670242eb04a107113140b35062fe',1,'FIXFields']]],
  ['relatedpartyidsource',['RelatedPartyIDSource',['../namespaceFIXFields.html#ae924b9dfb5e0e61cc7eb638152dcc3a3',1,'FIXFields']]],
  ['relatedpartyrole',['RelatedPartyRole',['../namespaceFIXFields.html#a01db99ea1b28a925d60f16f4f333e4be',1,'FIXFields']]],
  ['relatedpartysubid',['RelatedPartySubID',['../namespaceFIXFields.html#a32623cab35625a813732c84f6d6bcb4e',1,'FIXFields']]],
  ['relatedpartysubidtype',['RelatedPartySubIDType',['../namespaceFIXFields.html#acb41d8db7d0885bdb7786780cac6174f',1,'FIXFields']]],
  ['relationshipriskcficode',['RelationshipRiskCFICode',['../namespaceFIXFields.html#a56e0a0da5a453ae1ede77ece56324326',1,'FIXFields']]],
  ['relationshipriskcouponrate',['RelationshipRiskCouponRate',['../namespaceFIXFields.html#a3b1223963a0d8282a68b3af7d898c875',1,'FIXFields']]],
  ['relationshipriskencodedsecuritydesc',['RelationshipRiskEncodedSecurityDesc',['../namespaceFIXFields.html#ae8018aa1d55062aa73e78a8d977f6cf6',1,'FIXFields']]],
  ['relationshipriskencodedsecuritydesclen',['RelationshipRiskEncodedSecurityDescLen',['../namespaceFIXFields.html#a349bd69299720677d52f3e975893461a',1,'FIXFields']]],
  ['relationshipriskflexibleindicator',['RelationshipRiskFlexibleIndicator',['../namespaceFIXFields.html#a04b00da31960927f67c7fa376d054322',1,'FIXFields']]],
  ['relationshipriskinstrumentmultiplier',['RelationshipRiskInstrumentMultiplier',['../namespaceFIXFields.html#a7a74b47e94ff03c9bea2082fd566014a',1,'FIXFields']]],
  ['relationshipriskinstrumentoperator',['RelationshipRiskInstrumentOperator',['../namespaceFIXFields.html#a78e1aba941dd4d8ef743a388d74c1c5c',1,'FIXFields']]],
  ['relationshipriskinstrumentsettltype',['RelationshipRiskInstrumentSettlType',['../namespaceFIXFields.html#a4f22a18dab7cd010efea259347b6aff0',1,'FIXFields']]],
  ['relationshiprisklimitamount',['RelationshipRiskLimitAmount',['../namespaceFIXFields.html#ad2cb2e79ee5116adb1914ee3a9c12d03',1,'FIXFields']]],
  ['relationshiprisklimitcurrency',['RelationshipRiskLimitCurrency',['../namespaceFIXFields.html#a804b925837ee8af4c947afea1cca8285',1,'FIXFields']]],
  ['relationshiprisklimitplatform',['RelationshipRiskLimitPlatform',['../namespaceFIXFields.html#ab4e97b76dfb03bf53ec8c5e6e91a91b5',1,'FIXFields']]],
  ['relationshiprisklimittype',['RelationshipRiskLimitType',['../namespaceFIXFields.html#a285791142c929d7407855829d09d9b13',1,'FIXFields']]],
  ['relationshipriskmaturitymonthyear',['RelationshipRiskMaturityMonthYear',['../namespaceFIXFields.html#a1e26be5d3fc40f7e2070d2408816c571',1,'FIXFields']]],
  ['relationshipriskmaturitytime',['RelationshipRiskMaturityTime',['../namespaceFIXFields.html#a483c4d71bd100ee73e279a9969015dbd',1,'FIXFields']]],
  ['relationshipriskproduct',['RelationshipRiskProduct',['../namespaceFIXFields.html#a650a8811136ba45289f46d9e0f106b6b',1,'FIXFields']]],
  ['relationshipriskproductcomplex',['RelationshipRiskProductComplex',['../namespaceFIXFields.html#a0df8377b2650c370d92a6569f4a6b4fd',1,'FIXFields']]],
  ['relationshipriskputorcall',['RelationshipRiskPutOrCall',['../namespaceFIXFields.html#a4d573830794a77d3daa5b0371915dbcf',1,'FIXFields']]],
  ['relationshipriskrestructuringtype',['RelationshipRiskRestructuringType',['../namespaceFIXFields.html#af30cf204455353037696f3a8ffd3c0ab',1,'FIXFields']]],
  ['relationshiprisksecurityaltid',['RelationshipRiskSecurityAltID',['../namespaceFIXFields.html#af28a3324b9706133751a3f478e173b9d',1,'FIXFields']]],
  ['relationshiprisksecurityaltidsource',['RelationshipRiskSecurityAltIDSource',['../namespaceFIXFields.html#a136b4bc0d84fffcf7396ee14bc9c4083',1,'FIXFields']]],
  ['relationshiprisksecuritydesc',['RelationshipRiskSecurityDesc',['../namespaceFIXFields.html#a1a931453315562014df2c16d70027779',1,'FIXFields']]],
  ['relationshiprisksecurityexchange',['RelationshipRiskSecurityExchange',['../namespaceFIXFields.html#a433bac89ae61e3c275f3fbe6d3a9c022',1,'FIXFields']]],
  ['relationshiprisksecuritygroup',['RelationshipRiskSecurityGroup',['../namespaceFIXFields.html#a7c3674b746af2f0c6a7d0787be10f7ab',1,'FIXFields']]],
  ['relationshiprisksecurityid',['RelationshipRiskSecurityID',['../namespaceFIXFields.html#a04573e8ff7dbb186dd21c399905caa19',1,'FIXFields']]],
  ['relationshiprisksecurityidsource',['RelationshipRiskSecurityIDSource',['../namespaceFIXFields.html#a219c1c52f0282eb9d88c8eea04f4f280',1,'FIXFields']]],
  ['relationshiprisksecuritysubtype',['RelationshipRiskSecuritySubType',['../namespaceFIXFields.html#a1dd5e603937ddd6d85e014dcb4c26ab2',1,'FIXFields']]],
  ['relationshiprisksecuritytype',['RelationshipRiskSecurityType',['../namespaceFIXFields.html#afca453e60fa7986e52c8ecc0e6db7b4d',1,'FIXFields']]],
  ['relationshipriskseniority',['RelationshipRiskSeniority',['../namespaceFIXFields.html#a90bdcca40c47a31abb988e2ec078ef87',1,'FIXFields']]],
  ['relationshiprisksymbol',['RelationshipRiskSymbol',['../namespaceFIXFields.html#a5c34a922c65f6d1f2ba1554bd5b08559',1,'FIXFields']]],
  ['relationshiprisksymbolsfx',['RelationshipRiskSymbolSfx',['../namespaceFIXFields.html#af040326bf0c558eb0428239f616da592',1,'FIXFields']]],
  ['relationshipriskwarninglevelname',['RelationshipRiskWarningLevelName',['../namespaceFIXFields.html#a52566a961f1a2a434a9b6d2482e11090',1,'FIXFields']]],
  ['relationshipriskwarninglevelpercent',['RelationshipRiskWarningLevelPercent',['../namespaceFIXFields.html#a55ef095956ebc6685efec02e6891ac27',1,'FIXFields']]],
  ['relsymtransacttime',['RelSymTransactTime',['../namespaceFIXFields.html#aabd7e98a46028acde9345c203daac17d',1,'FIXFields']]],
  ['replaychannel_5f',['replayChannel_',['../structCqg_1_1MdGroup.html#ad48c2432407abfea68fd4a467c6ed1b9',1,'Cqg::MdGroup']]],
  ['replaydelaysize',['replayDelaySize',['../structBats_1_1SequenceParams.html#a5b9068be50aa14355c9b57cbe34f7d52',1,'Bats::SequenceParams']]],
  ['replaytimeoutms',['replayTimeoutMS',['../structBats_1_1SequenceParams.html#a0c7a811486ce68759b6474c7df0d9447',1,'Bats::SequenceParams']]],
  ['repocollateralsecuritytype',['RepoCollateralSecurityType',['../namespaceFIXFields.html#aede07ced339db67730bb09ce78803b2c',1,'FIXFields']]],
  ['reportedpx',['ReportedPx',['../namespaceFIXFields.html#a6812126fa159b348df564952c96cc398',1,'FIXFields']]],
  ['reportedpxdiff',['ReportedPxDiff',['../namespaceFIXFields.html#a130008e160ccb4d94cf70d22d5f82aed',1,'FIXFields']]],
  ['reporttoexch',['ReportToExch',['../namespaceFIXFields.html#ab7184915fdac2181529f430f6b5e00b2',1,'FIXFields']]],
  ['repurchaserate',['RepurchaseRate',['../namespaceFIXFields.html#a5c156388507448e55604ebb9fc469524',1,'FIXFields']]],
  ['repurchaseterm',['RepurchaseTerm',['../namespaceFIXFields.html#aa4b21b9a05a3f32191322b5568fea9bb',1,'FIXFields']]],
  ['requestedpartyrole',['RequestedPartyRole',['../namespaceFIXFields.html#ad8e27a1532020c0a1a31d3a32c345807',1,'FIXFields']]],
  ['requestid_5f',['requestId_',['../structEngine_1_1SessionContext.html#a2661aaf4875b1120f42138ba57a70c80',1,'Engine::SessionContext::requestId_()'],['../structEngine_1_1AdminApplication_1_1Context.html#a7bbb9b4f7aa0d79c266f801cba69d832',1,'Engine::AdminApplication::Context::requestId_()']]],
  ['requestlostmessagesflag_5f',['requestLostMessagesFlag_',['../classEngine_1_1SequenceGapEvent.html#a1e38a67b7db10ab927685499579bd453',1,'Engine::SequenceGapEvent']]],
  ['resend_5fmessages_5fblock_5fsize_5fparam',['RESEND_MESSAGES_BLOCK_SIZE_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#a788e5ac430d766319987ff7255d12542',1,'Engine::FIXPropertiesNames']]],
  ['resendrequestblocksize',['ResendRequestBlockSize',['../namespaceEngine_1_1SessionParameters.html#a811c10e35aa7f2266cbbf7d879e86cbe',1,'Engine::SessionParameters']]],
  ['resendrequestblocksize_5f',['resendRequestBlockSize_',['../structEngine_1_1SessionExtraParameters.html#aa629a95b941f5c1b82ef95cfda4e411a',1,'Engine::SessionExtraParameters']]],
  ['resendrequestmsg_5f',['resendRequestMsg_',['../classEngine_1_1ResendRequestEvent.html#a2921e81269247c8eebbe5751fc8132f2',1,'Engine::ResendRequestEvent']]],
  ['reset_5fseqnum_5fafter_5f24_5fhours_5fparam',['RESET_SEQNUM_AFTER_24_HOURS_PARAM',['../namespaceEngine_1_1FIXPropertiesNames.html#a4d3eab72bb5fdf386dc64e78e3b70012',1,'Engine::FIXPropertiesNames']]],
  ['resetseqnumflag',['ResetSeqNumFlag',['../namespaceFIXFields.html#ac212d1f97d9cbf76ba56dedc5091c31c',1,'FIXFields']]],
  ['respondenttype',['RespondentType',['../namespaceFIXFields.html#a552a32f314373e88a20d560f89ea2ef7',1,'FIXFields']]],
  ['responsedestination',['ResponseDestination',['../namespaceFIXFields.html#a850f0735942dce21a4b384b766c02d43',1,'FIXFields']]],
  ['responsetransporttype',['ResponseTransportType',['../namespaceFIXFields.html#ac2da99f3c22e8ed6ae47f653fe3511b6',1,'FIXFields']]],
  ['restoreenabled_5f',['restoreEnabled_',['../structEngine_1_1MsgStorage_1_1RestoreOptions.html#a9b86641dedf26be03cee2a433c1ac87b',1,'Engine::MsgStorage::RestoreOptions']]],
  ['restructuringtype',['RestructuringType',['../namespaceFIXFields.html#a686e1749393bdb5a0b4d527c062a9624',1,'FIXFields']]],
  ['result_5fconditions_5flack',['RESULT_CONDITIONS_LACK',['../namespaceEngine.html#a10d46bc84c925c9a3c8eb29411b189d0',1,'Engine']]],
  ['result_5fengine_5fexception',['RESULT_ENGINE_EXCEPTION',['../namespaceEngine.html#a90f35f98d6bb318f9497a5991b0ead9c',1,'Engine']]],
  ['result_5fincorrect_5fvalue',['RESULT_INCORRECT_VALUE',['../namespaceEngine.html#af6ea7d125db77d63b0a238679c702df0',1,'Engine']]],
  ['result_5finvalidarg',['RESULT_INVALIDARG',['../namespaceEngine.html#a30e616c315242a7669640be972e11ed8',1,'Engine']]],
  ['result_5flogic_5ferror',['RESULT_LOGIC_ERROR',['../namespaceEngine.html#a2ed6dbeae6aed331501631cfa407f111',1,'Engine']]],
  ['result_5fnot_5fenough_5fparameters',['RESULT_NOT_ENOUGH_PARAMETERS',['../namespaceEngine.html#a4f51790cc485f691dd7f6242b839d4f1',1,'Engine']]],
  ['result_5fnot_5fimplemented',['RESULT_NOT_IMPLEMENTED',['../namespaceEngine.html#aef150fdb9fe512fa4485b1d3e690b251',1,'Engine']]],
  ['result_5foperation_5frejected',['RESULT_OPERATION_REJECTED',['../namespaceEngine.html#a0f4b410edb24f8b2a02745e81a71711e',1,'Engine']]],
  ['result_5fruntime_5ferror',['RESULT_RUNTIME_ERROR',['../namespaceEngine.html#a135b4084f78b6218793cc0a0b7b217c9',1,'Engine']]],
  ['result_5fsuccess',['RESULT_SUCCESS',['../namespaceEngine.html#a20c74aebd24bd5d3fb036e0260e53296',1,'Engine']]],
  ['result_5funknown_5ferror',['RESULT_UNKNOWN_ERROR',['../namespaceEngine.html#ae456730c0469a7a7f7d6e9964a59af59',1,'Engine']]],
  ['result_5funknown_5fsession',['RESULT_UNKNOWN_SESSION',['../namespaceEngine.html#ac096a5c207929ecf925bf801bcfb33fe',1,'Engine']]],
  ['reversalindicator',['ReversalIndicator',['../namespaceFIXFields.html#a7b54d1f638d27ed4dfe4fd0294a2251b',1,'FIXFields']]],
  ['rfqreqid',['RFQReqID',['../namespaceFIXFields.html#acddad8dc1e07e092fa5e932dad8463a6',1,'FIXFields']]],
  ['riskcficode',['RiskCFICode',['../namespaceFIXFields.html#a4c5cc33572f829e50e76d85f85df4748',1,'FIXFields']]],
  ['riskcouponrate',['RiskCouponRate',['../namespaceFIXFields.html#a773c8ec8833a8cc03ecf4e7f8df60d7a',1,'FIXFields']]],
  ['riskencodedsecuritydesc',['RiskEncodedSecurityDesc',['../namespaceFIXFields.html#a59a451699f08672d61b67b448170b40d',1,'FIXFields']]],
  ['riskencodedsecuritydesclen',['RiskEncodedSecurityDescLen',['../namespaceFIXFields.html#abf0616a35051d56ddf9308efcde225fb',1,'FIXFields']]],
  ['riskflexibleindicator',['RiskFlexibleIndicator',['../namespaceFIXFields.html#a49c491cc1feec7b9cab2af1c3b92ceaa',1,'FIXFields']]],
  ['riskfreerate',['RiskFreeRate',['../namespaceFIXFields.html#a030b92ec72826070d18f087901767192',1,'FIXFields']]],
  ['riskinstrumentmultiplier',['RiskInstrumentMultiplier',['../namespaceFIXFields.html#afc28911b30cb02b60fa03a3254c4d6c2',1,'FIXFields']]],
  ['riskinstrumentoperator',['RiskInstrumentOperator',['../namespaceFIXFields.html#aace196b1721c281a9486c0d5f8da84fd',1,'FIXFields']]],
  ['riskinstrumentsettltype',['RiskInstrumentSettlType',['../namespaceFIXFields.html#a03d4d528f61561a46cf3770cde346215',1,'FIXFields']]],
  ['risklimitamount',['RiskLimitAmount',['../namespaceFIXFields.html#ad41188e72830c94f30dab86d3b11fae1',1,'FIXFields']]],
  ['risklimitcurrency',['RiskLimitCurrency',['../namespaceFIXFields.html#a0a4d4e4d6e942a3d23ddfa0b61cfc5db',1,'FIXFields']]],
  ['risklimitplatform',['RiskLimitPlatform',['../namespaceFIXFields.html#a0a5b38f2930f423dc4af376b6b19ef70',1,'FIXFields']]],
  ['risklimittype',['RiskLimitType',['../namespaceFIXFields.html#afb36653eb4dbdd250d3828bad382b044',1,'FIXFields']]],
  ['riskmaturitymonthyear',['RiskMaturityMonthYear',['../namespaceFIXFields.html#aaec761ba3ec1631a86f558f68e972249',1,'FIXFields']]],
  ['riskmaturitytime',['RiskMaturityTime',['../namespaceFIXFields.html#a0ebeaff1c7d58ab8ad08c3c68b27c82c',1,'FIXFields']]],
  ['riskproduct',['RiskProduct',['../namespaceFIXFields.html#ad9bdaac232d6b4e4f12b5e55c2f73a39',1,'FIXFields']]],
  ['riskproductcomplex',['RiskProductComplex',['../namespaceFIXFields.html#a333564ee0a0489715da5e87be7280898',1,'FIXFields']]],
  ['riskputorcall',['RiskPutOrCall',['../namespaceFIXFields.html#abfe27cba38749faf1701705ee8ea16c9',1,'FIXFields']]],
  ['riskrestructuringtype',['RiskRestructuringType',['../namespaceFIXFields.html#a3478439cbf762342b2e336c6f2588b65',1,'FIXFields']]],
  ['risksecurityaltid',['RiskSecurityAltID',['../namespaceFIXFields.html#a97b9e677d8bd8a048181e276cc4f1688',1,'FIXFields']]],
  ['risksecurityaltidsource',['RiskSecurityAltIDSource',['../namespaceFIXFields.html#af4b934b463f48e9e816e55d1d4de6cdc',1,'FIXFields']]],
  ['risksecuritydesc',['RiskSecurityDesc',['../namespaceFIXFields.html#ac4b6f8e5e59f299c5a8ceb9671f40dd3',1,'FIXFields']]],
  ['risksecurityexchange',['RiskSecurityExchange',['../namespaceFIXFields.html#aefa68c4133eeef5f4ae44082df1258ba',1,'FIXFields']]],
  ['risksecuritygroup',['RiskSecurityGroup',['../namespaceFIXFields.html#aea0d4986ac4f2198245c78388de15897',1,'FIXFields']]],
  ['risksecurityid',['RiskSecurityID',['../namespaceFIXFields.html#ab8c133962d0b6810d34f4f7467988762',1,'FIXFields']]],
  ['risksecurityidsource',['RiskSecurityIDSource',['../namespaceFIXFields.html#aa1323f1642b77c957b1f8410229e4c04',1,'FIXFields']]],
  ['risksecuritysubtype',['RiskSecuritySubType',['../namespaceFIXFields.html#a08973319ee644d02f8f69d230ef31c29',1,'FIXFields']]],
  ['risksecuritytype',['RiskSecurityType',['../namespaceFIXFields.html#a7e4151afdd4bf14c0b96145a1cb474d4',1,'FIXFields']]],
  ['riskseniority',['RiskSeniority',['../namespaceFIXFields.html#a2b6eee80fc4b15072945c3b0a762477e',1,'FIXFields']]],
  ['risksymbol',['RiskSymbol',['../namespaceFIXFields.html#a880fc05b035f8a6a83de06db2c2e4bcd',1,'FIXFields']]],
  ['risksymbolsfx',['RiskSymbolSfx',['../namespaceFIXFields.html#a8435e4f2fc67f025ff9c43212f84c4ee',1,'FIXFields']]],
  ['riskwarninglevelname',['RiskWarningLevelName',['../namespaceFIXFields.html#a9adf2098040320d01d8f05d525e16cf3',1,'FIXFields']]],
  ['riskwarninglevelpercent',['RiskWarningLevelPercent',['../namespaceFIXFields.html#a26975234732471e34f44dbc90c81852e',1,'FIXFields']]],
  ['rndpx',['RndPx',['../namespaceFIXFields.html#a3f56be6e70b7fc5c490fd1d93db7a110',1,'FIXFields']]],
  ['role',['Role',['../namespaceEngine_1_1SessionParameters.html#ab8cc039d0bbd0371366d9f102f5b7cb2',1,'Engine::SessionParameters']]],
  ['role_5f',['role_',['../structEngine_1_1FixField.html#a7a3a9ef020255ff33c28a2f7ffcd9f4f',1,'Engine::FixField']]],
  ['role_5facceptor',['Role_Acceptor',['../namespaceEngine_1_1SessionParameters.html#a86e651045f8fc4f010241a53c2bdd8ae',1,'Engine::SessionParameters']]],
  ['role_5finitiator',['Role_Initiator',['../namespaceEngine_1_1SessionParameters.html#ab5b77e66b4f9c875c0789067621accd8',1,'Engine::SessionParameters']]],
  ['root',['Root',['../namespaceEngine_1_1SessionName.html#a6dde309ac96225a5e51f8416a2506a50',1,'Engine::SessionName::Root()'],['../namespaceEngine_1_1ScheduleName.html#a1058fa9691b1dee7f1545996e1a50f09',1,'Engine::ScheduleName::Root()']]],
  ['rootpartyid',['RootPartyID',['../namespaceFIXFields.html#a5d6ef44752e0e90576928c365585320c',1,'FIXFields']]],
  ['rootpartyidsource',['RootPartyIDSource',['../namespaceFIXFields.html#a6c523b214988d03520cae80477f31441',1,'FIXFields']]],
  ['rootpartyrole',['RootPartyRole',['../namespaceFIXFields.html#a6e6fc6ca81b5e34fde37981964337846',1,'FIXFields']]],
  ['rootpartysubid',['RootPartySubID',['../namespaceFIXFields.html#ae4abfad7a7c2ca238898941951c8bf4e',1,'FIXFields']]],
  ['rootpartysubidtype',['RootPartySubIDType',['../namespaceFIXFields.html#aacbee608a36f3808bd2327cb4cf30227',1,'FIXFields']]],
  ['roundingdirection',['RoundingDirection',['../namespaceFIXFields.html#acbc95b1c8a2965d94589035d0ce3780d',1,'FIXFields']]],
  ['roundingmodulus',['RoundingModulus',['../namespaceFIXFields.html#a5b73aa55535e146f46d40c58155106af',1,'FIXFields']]],
  ['roundlot',['RoundLot',['../namespaceFIXFields.html#a8cdf815008704f8fed40fe9259fdea55',1,'FIXFields']]],
  ['routingid',['RoutingID',['../namespaceFIXFields.html#a5ce34a11bf4c43fbbbf1d320f0419cb9',1,'FIXFields']]],
  ['routingtype',['RoutingType',['../namespaceFIXFields.html#a3aec71cdccf6bc3046458a1482b42efe',1,'FIXFields']]],
  ['rptseq',['RptSeq',['../namespaceFIXFields.html#a6e9f5aa5ae9dda1e6adee7c19c39a07e',1,'FIXFields']]],
  ['rptsys',['RptSys',['../namespaceFIXFields.html#a20fe5a3475150b1999fc0da6caaa12a5',1,'FIXFields']]],
  ['rule80a',['Rule80A',['../namespaceFIXFields.html#a01cad52a59722c51edb3548fa2ec482e',1,'FIXFields']]]
];
