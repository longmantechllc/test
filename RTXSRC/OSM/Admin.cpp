#include <iostream>
#include <unordered_map>
#include <functional>
//#include <charconv>  // only in /opt/rh/devtoolset-8/root/usr/include/c++/8/charconv
#include "spdlog/spdlog.h"
#include "spdlog/sinks/basic_file_sink.h" // support for basic file logging
#include "RTX_OSM.h"
#include "Admin.h"

using namespace boost::asio;
using namespace std;
using ip::tcp;

void con_handler::start()
{
    if (!m_done) {
        boost::asio::async_read_until(m_socket, buffer, '\n',
                                      boost::bind(&con_handler::handle_read,
                                                  shared_from_this(),
                                                  boost::asio::placeholders::error,
                                                  boost::asio::placeholders::bytes_transferred));
    } else {
        spdlog::info("DONE, close the socket");
        m_socket.close();
    }
}

void con_handler::handle_read(const boost::system::error_code& err, size_t bytes_transferred)
{
    std::ostringstream ss;
    ss<<&buffer;
    line = ss.str();

    if (!err) {
        processCommand();
        spdlog::info("send back message: {}", message);
        m_socket.async_write_some(
                boost::asio::buffer(message, max_length),
                boost::bind(&con_handler::handle_write,
                            shared_from_this(),
                            boost::asio::placeholders::error,
                            boost::asio::placeholders::bytes_transferred));
        start();
    } else {
        spdlog::error("read error: {}", err.message());
        m_socket.close();
    }
}

void con_handler::handle_write(const boost::system::error_code& err, size_t bytes_transferred)
{
    if (!err) {
        spdlog::info("Admin Server sent: {}", message);
        if (message == "Bye!\n") {
            m_socket.close();
        }
    } else {
        spdlog::error("write error: {}", err.message());
        m_socket.close();
    }
}

void con_handler::processCommand() noexcept {
    // check router name
    auto pos= line.find("receiver=");
    if (pos == string::npos) {
        message = "missing receiver=\n";
        return;
    }
    pos += 9;
    auto pos2 = line.find("|", pos);
    auto cmd = line.substr(pos, pos2-pos);
    if (cmd != m_osm.params_.routerName) {
        message = "sent to wrong receiver\n";
        return;
    }
    pos= line.find("command=");
    if (pos == string::npos) {
        message = "missing command=\n";
        return;
    }
    pos += 8;
    pos2 = line.find("|", pos);
    cmd = line.substr(pos, pos2-pos);
    if (cmd == "BYE") {
        m_done = true;
        message = "Bye!\n";
        return;
    }
    static unordered_map<string, vector<string>> fields = {
            {"VIRTUALSTATUS", {"venue="}},
            {"CANCELPARENT", {"pid=", "login=", "subaccount="}},
            {"CANCELCHILD", {"coid=", "venue="}},
            {"CONNECTNORM", {"sendtime="}},
            {"VIRTUALQRY", {"venue="}},
            {"TOROUTER", {} }
    };
    if (fields.count(cmd) == 0) {
        message = "unsupported command\n";
        return;
    }
    int num = 0, idx;
    for(int i = 0; i < fields[cmd].size(); ++i) {
        pos = line.find(fields[cmd][i]);
        if (pos != string::npos) {
            ++num;
            idx = i;
        }
    }
    string result;
    if (cmd != "TOROUTER") {
        if (num == 0) {
            message = "missing parameter for command ";
            message.append(cmd).append("\n");
            return;
        }
        else if (num > 1) {
            message = "too many parameters for command ";
            message.append(cmd).append("\n");
            return;
        }
        pos = line.find(fields[cmd][idx]);
        pos += fields[cmd][idx].size();
        pos2 = line.find("|", pos);
        auto param = line.substr(pos, pos2-pos);
        message  = sendToOsm(cmd, idx, param);
    }
    else {
        result = sendToOsm(cmd, 0, line);
        spdlog::info("TOROUTER get back: {}", result);
        if (result.empty()) {
            spdlog::info("Error: TOROUTER shouldn't return empty string. check router log on its status");
            message = cmd + " is processed, don't know it's successful or not\n";
        }
        else {
            cmd.append(": ").append(result).append("\n");
            message = cmd;
        }
    }
}

string con_handler::sendToOsm(const string_view& cmd, int idx, const string_view& param) {
    string result;
    if (cmd == "VIRTUALSTATUS") {
        auto pos= param.find(",");
        if (pos == string_view::npos) {
            return string(cmd).append(" has wrong parameter: ").append(param).append("\n");
        }
        auto exch2 = param.substr(0, pos);
        string exch (exch2.data(), exch2.size()); // UpdateDestinationAvailability can't take string_view
        auto status = param.substr(pos+1);
        cout << exch << " is " << status << endl;
        m_osm.RTXOrderIO_->UpdateDestinationAvailability(exch, status == "UP");
        m_osm.getExchStatus()[exch] = (status == "UP");
        result = "VIRTUALSTATUS is processed\n";
    }
    else if (cmd == "CANCELPARENT") {
        switch (idx ) {
            case 0: {
                int pid;
                auto cxlOrder = [&](auto str) {
                    string s(str.data(), str.size());
                    pid = atoi(s.c_str());
                    if (pid != 0) {
                        spdlog::info("CANCELPARENT {}", pid);
                        m_osm.RTXOrderIO_->CancelParentOrder(pid);
                    }
                };
                int starting = 0;
                auto pos = param.find(",", starting);
                for (; pos != string_view::npos; starting = pos + 1, pos = param.find(",", starting)) {
                    auto pids = param.substr(starting, pos - starting);
                    cxlOrder(pids);
                }
                auto pids = param.substr(starting);
                cxlOrder(pids);
                break;
            }
            case 1: {
                m_osm.cancelLogin(string(param.data(), param.size()));
                break;
            }
            case 2: {
                m_osm.cancelSubAccount(string(param.data(), param.size()));
                break;
            }
        }
        result = "CANCELPARENT is processed\n";
    }
    else if (cmd == "CANCELCHILD") {
        switch(idx)
        {
            case 0: {
                auto cxlOrder = [&](auto str) {
                    string s(str.data(), str.size());
                    spdlog::info("CANCELCHILD {}", s);
                    m_osm.cancelChild(s);
                };
                int starting = 0;
                auto pos = param.find(",", starting);
                for (; pos != string_view::npos; starting = pos + 1, pos = param.find(",", starting)) {
                    auto pids = param.substr(starting, pos - starting);
                    cxlOrder(pids);
                };
                auto pids = param.substr(starting);
                cxlOrder(pids);
                break;
            }
            case 1: {
                m_osm.cancelVenue(string(param.data(), param.size()));
                break;
            }
        }
        result = "CANCELCHILD is processed\n";
    }
    else if (cmd == "CONNECTNORM") {
        result = m_osm.connectNormalizer();
    }
    else if (cmd == "TOROUTER") {
        spdlog::info("TOROUTER: {}", param);
        result = m_osm.RTXOrderIO_->RouteAdminCmd(string(param)) +"\n";
    }
    else if (cmd == "VIRTUALQRY") {
        int starting = 0;
        if (param == "*") {  // wildcard support
            for(const auto& exch: m_osm.getExchStatus()) {
                result.append(exch.first).append(exch.second ? "=UP," : "=DOWN,");
            }
            result += '\n';
        }
        else {
            auto pos = param.find(",", starting);
            const auto& exchStatus = m_osm.getExchStatus();
            for (; pos != string_view::npos; starting = pos + 1, pos = param.find(",", starting)) {
                string exch{param.substr(starting, pos - starting)};
                if (exchStatus.count(exch))
                    result.append(exch).append(exchStatus.at(exch) ? "=UP," : "=DOWN,");
                else
                    result.append(exch).append("=UNKNOWN,");
            }
            string exch{param.substr(starting)};
            if (exchStatus.count(exch))
                result.append(exch).append(exchStatus.at(exch) ? "=UP" : "=DOWN");
            else
                result.append(exch).append("=UNKNOWN");
        }
    }
    return result;
}

void Admin::start() {
    start_accept();
    m_io.run();
    cout << "Admin::start() done" << endl;
}

void Admin::stop() {
    m_acceptor.close();
    {
        std::vector<con_handler::pointer> sessionsToClose;
        copy(active_connections_.begin(), active_connections_.end(), back_inserter(sessionsToClose));
        for (auto& s : sessionsToClose)
            s->shutdown();
    }
    active_connections_.clear();
    m_io.stop();
}

void Admin::start_accept()
{
    // socket
    con_handler::pointer connection = con_handler::create(m_io, m_osm);
    // asynchronous accept operation and wait for a new connection.
    m_acceptor.async_accept(connection->socket(),
                            boost::bind(&Admin::handle_accept, this, connection,
                                        boost::asio::placeholders::error));
}

void Admin::handle_accept(con_handler::pointer connection, const boost::system::error_code& err)
{
    if (!err) {
        active_connections_.insert(connection);
        connection->start();
    }
    else {
        cout << "get error on handle_accept" << endl;
    }
    start_accept();
}
