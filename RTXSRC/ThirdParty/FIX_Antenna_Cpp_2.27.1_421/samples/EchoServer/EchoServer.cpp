/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <sstream>
#include <cstdio>
#include <ctime>
#include <stdexcept>
#include <string>

#include <B2BITS_V12.h>

#include "MySessionsManager.h"
#include "B2BITS_Thread.h"

using namespace Engine;
using namespace std;

void runServer( std::string const& aPropertiesFileName );
std::string listenPorts2string(const Engine::ListenPorts& ports);

int main( int argc, char const* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if( argc > 3 || ( 2 == argc && ( 0 == strcmp( "-h", argv[1] ) || 0 == strcmp( "/?", argv[1] ) ) ) ) {
        clog << "Usage: " << argv[0] << " [<config_file>]" << endl;
        return 1;
    }

    string aPropertiesFileName;

    if( argc > 1 ) {
        aPropertiesFileName = argv[1];
    }


    try {
        runServer( aPropertiesFileName );
    } catch( const Utils::Exception& ex ) {
        cout << "[ERROR] " << ex.what();
        return 1;
    } catch( const std::exception& ex ) {
        cout << "[ERROR] " << ex.what();
        return 1;
    }

    return 0;
}
const std::string echoModeProp = "EchoMode";
const std::string EnableSessionAutoCreationProp = "EnableSessionAutoCreation";

void runServer( std::string const& aPropertiesFileName )
{
    // Initialize FE
    FixEngine::init( aPropertiesFileName );

    Utils::Log::LogCategory* logger = Utils::Log::LogSystem::singleton()->createCategory( "Engine" );

    bool isEchoMode(true);
    if ( FixEngine::singleton()->getProperties()->existsProperty( echoModeProp ) )
    {
        std::string echoMode = FixEngine::singleton()->getProperties()->getStringProperty( echoModeProp );
        if ( echoMode == "false" )
            isEchoMode = false;
        else if ( echoMode != "true" )
            logger->error( "EchoMode value '" + echoMode + "' is incorrect. Default value EchoMode=true is applied" );
        logger->note( std::string("EchoMode is ") + (isEchoMode ? "true" : "false") );
    }

    // create SessionsManager
    MySessionsManager* g_mySM = new MySessionsManager(isEchoMode);

    // register SessionsManager
    FixEngine::singleton()->registerSessionsManager( g_mySM );

    // create session-acceptor
    string sid = g_mySM->createAcceptor( "ESVR", "CTG", FIX44 );

    // create FIX message of type "Order"
    std::auto_ptr<FIXMessage> pMessage( FIXMsgFactory::singleton()->newSkel( FIX44, "D" ) );

    // Makes unique identifier.
    char uniqueId[20];
    sprintf( uniqueId, "%u", static_cast<unsigned int>( time( NULL ) ) );
    pMessage->set( FIXField::ClOrdID, uniqueId );

    // Sets up values for message.
    pMessage->set( FIXField::Symbol,       "IBM" );
    pMessage->set( FIXField::Side,         '2' ); // Sell
    pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );
    pMessage->set( FIXField::OrderQty,     200 );
    pMessage->set( FIXField::OrdType,      '2' ); // Limit
    pMessage->set( FIXField::Price,        Engine::Decimal( 125, -2 ) ); // Limit
    pMessage->set( FIXField::TimeInForce,  '1' ); // GTC

    g_mySM->put( sid, pMessage.get() );

    if ( FixEngine::singleton()->getProperties()->existsProperty( EnableSessionAutoCreationProp ) && FixEngine::singleton()->getProperties()->getStringProperty( EnableSessionAutoCreationProp ) == "true")
	{
		FAProperties::ConfiguredSessionsMap sessions = Engine::FixEngine::singleton()->getProperties()->getConfiguredSessions();
		for(FAProperties::ConfiguredSessionsMap::const_iterator it = sessions.begin(); it != sessions.end(); ++it)
		{
			if(!g_mySM->isSessionExist(it->first))
				g_mySM->createSession( it->first, it->second);
		}
	}

    cout << "listen ports: " << listenPorts2string(FixEngine::singleton()->getListenPorts()) << endl;
    cout << "EchoServer was started." << endl;
	
    cin.tie( &cout );
    string cmd;

    // show and process EchoServer operational menu
    while( "4" != cmd ) {
        cout << "Choose command:" << endl
                    << "1 - show EchoServer's statistics" << endl
                    << "2 - show FIX Engine's statistics" << endl
                    << "3 - close all active sessions" << endl
                    << "4 - stop EchoServer" << endl
                    << "5 - send new message" << endl
                    << ">" ;

        // if eof was received, clear the stream state and try to read again
        getline( cin, cmd );
        if (cin.eof())
        {
            cin.clear();
            System::Thread::sleep(1000);
            continue;
        }

        try {
            if( "1" == cmd ) {
                g_mySM->showStat();
            } else if( "2" == cmd ) {
                g_mySM->showEngineStat();
            } else if( "3" == cmd ) {
                g_mySM->closeSessions();
            } else if( "5" == cmd ) {
                sprintf( uniqueId, "%u", static_cast<unsigned int>( time( NULL ) ) );
                pMessage->set( FIXField::ClOrdID, uniqueId );
                g_mySM->put( sid, pMessage.get() );
            }
        } catch( const std::exception& ex ) {
            cout << ex.what()  << endl;
        }
    }

    cout <<  "Please wait..."  << endl;

    // destroy SessionsManager
    g_mySM->stop();
    g_mySM->closeSessions();

    FixEngine::singleton()->registerSessionsManager( NULL );

    FixEngine::destroy();

    // free memory allocated for SessionsManager
    delete g_mySM;

    cout <<  "EchoServer was stopped." << endl;
}

std::string listenPorts2string(const Engine::ListenPorts& ports)
{
    if (ports.empty())
        return "none";
    std::ostringstream oss;
    for (Engine::ListenPorts::const_iterator it = ports.begin(); it != ports.end(); ++it) {
        if (it != ports.begin()) 
            oss << ", ";
        oss << (it->port()) << (it->isSecured() ? " SSL" : "");
    }
    return oss.str();
}