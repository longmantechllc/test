// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_StorageMgr.h
/// Contains Engine::StorageMgr class definition

#ifndef B2BITS_STORAGEMGR_H_
#define B2BITS_STORAGEMGR_H_

#include <string>
#include <list>
#include <vector>
/* For size_t */
#include <cstddef>
#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_SessionId.h"
#include "B2BITS_Session.h"

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

namespace Engine
{
    class MsgStorage;
    /**
    * Manages MsgStorages.
    */
    class V12_API StorageMgr
    {
    public:
        /// Information about the persistent session.
        struct V12_API PersistentSessionInfo {
            /*
            Default constructor
            */
            PersistentSessionInfo();
            /**
            * Path to the storage without extention
            **/
            std::string baseFilePath_;
            /**
            * SessionId
            **/
            SessionId sessionId_;
            /**
            * Heartbeat interval
            **/
            int HBI_;
            /**
            * Incoming message sequence number
            **/
            int inSeqNum_;
            /**
            * Outgoing message sequence number
            **/
            int outSeqNum_;
            /**
            * Minimal value of output sequence number
            **/
            int minOutSeqNum_;
            /**
            * FIX protocol version
            **/
            FIXVersion version_;
            /**
            * True, if owner of storage is initiator; otherwise owner is acceptor.
            **/
            bool isInitiator_;
            /**
            * Host name
            **/
            std::string host_;
            /**
            * Port number
            **/
            int port_;
            /**
            * Active connection type
            **/
            std::string activeConnection_;
            /** Equals to true, if session is terminated. */
            bool terminated_;
            /**
            * Time creation
            **/
            System::u64 timeCreation_;
            /**
            * Parser description of the session. Contains Parser name, scp and app protocols describing the parser.
            **/
            ParserDescription parserDescription_;
        };

        /**
         * \brief Register built-in storage managers.
         *
         * \details FIX Antenna have the number of built-in storages
         * like: persistent, transient, persistentMM, splitPersistent
         * and null. This function performs initialization of storage
         * register and registers all compiled-in storage managers.
         *
         * \warning This function is not thread safe.
         */
        static void init();

        /**
         * \brief Destroys the instance.
         *
         * \details Deallocate resources taken by registered instances
         * of StorageMgr.
         *
         * \warning This function is not thread safe.
         */
        static void destroy();

        /**
        * \brief Returns the pointer to the instance of StorageMgr.
        *
        * \throw Utils::Exception When the specified storage name is
        * not registered yet.
        *
        * \warning This function is not thread safe.
        */
        static StorageMgr* singleton( const MessageStorageType type );

        /**
        * \brief Returns the pointer to the instance of StorageMgr.
        *
        * \throw Utils::Exception When the specified storage name is
        * not registered yet.
        *
        * \warning This function is not thread safe.
        */
        static StorageMgr*
        singleton( std::string const & type );

        /// Information about the persistent sessions that were non-gracefully terminated.
        typedef std::list<PersistentSessionInfo> IncompleteSessionsInfo;

        /**
        * Loads information about sessions that were terminated non-gracefully.
        *
        * @return the number of found sessions.
        */
        // virtual int load(IncompleteSessionsInfo* sessions) = 0;

        /**
        * Search for the session's storage and read it.
        * @param sender - session's sender name
        * @param target - session's target name
        *
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool existStorage( const std::string& sender, const std::string& target, bool* reuse ) = 0;

        /**
        * Search for the session's storage and read it.
        * @param sessionId - session identifier
        *
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool existStorage( const SessionId& sessionId, bool* reuse ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sender - session's sender name
        * @param target - session's target name
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const std::string& sender, const std::string& target, PersistentSessionInfo* info ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sessionId - session identifier
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const SessionId& sessionId, PersistentSessionInfo* info ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sender - session's sender name
        * @param target - session's target name
        * @param version - fix protocol version
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const std::string& sender, const std::string& target, FIXVersion version, PersistentSessionInfo* info ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sessionId - session identifier
        * @param version - fix protocol version
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const SessionId& sessionId, FIXVersion version, PersistentSessionInfo* info ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param baseFilePath Path to the storage without etension
        * @param info - information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const std::string& baseFilePath, PersistentSessionInfo* info ) = 0;

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sender - session's sender name
        * @param target - session's target name
        * @param version - fix protocol version
        * @param type - active connection type
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const std::string& sender, const std::string& target, FIXVersion version, Session::ActiveConnection type, PersistentSessionInfo* info );

        /**
        * Loads information about the session that was terminated non-gracefully.
        * @param sessionId - session identifier
        * @param version - fix protocol version
        * @param type - active connection type
        * @param info - Information about the persistent session
        * @return 'true' if the information was found, otherwise - false.
        */
        virtual bool load( const SessionId& sessionId, FIXVersion version, Session::ActiveConnection type, PersistentSessionInfo* info );

        /** Creates new MsgStorage object at subFolder
        * @param sender - session sender
        * @param target - session target
        * @param version - session's version
        * @param outgoingStorageSize - Specifies size of the transient message storage
        * @param location - path to the folder where files have to be created.
        *            When empty - files will be created in log folder.
        * @param baseFilePath Path to the storage without etension. 
        *        if empty, path will be calculated based on sender, target and engine log directory parameters.
        * @return new MsgStorage object
        */
        virtual MsgStorage* create( const std::string& sender,
                                    const std::string& target,
                                    FIXVersion version,
                                    int outgoingStorageSize,
                                    const std::string& location = "",
                                    const std::string& baseFilePath = "" ) = 0;

        /** Creates new MsgStorage object at subFolder
        * @param sessionId - session identifier
        * @param version - session's version
        * @param outgoingStorageSize - Specifies size of the transient message storage
        * @param location - path to the folder where files have to be created.
        *            When empty - files will be created in log folder.
        * @param baseFilePath Path to the storage without etension. 
        *        if empty, path will be calculated based on sender, target and engine log directory parameters.
        * @return new MsgStorage object
        */
        virtual MsgStorage* create( const SessionId& sessionId,
                                    FIXVersion version,
                                    int outgoingStorageSize,
                                    const std::string& location = "",
                                    const std::string& baseFilePath = "" ) = 0;

        /** Creates new MsgStorage object
        * @param info - information about the persistent session
        * @param outgoingStorageSize - Specifies size of the transient message storage
        * @return new MsgStorage object
        */

        virtual MsgStorage* create( PersistentSessionInfo* info, int outgoingStorageSize ) = 0;

        /** Return storage type
        */
        virtual MessageStorageType type() const = 0;

        /** 
        * Returns true if storage is persistent; false otherwise.
        */
        virtual bool isPersistent() const = 0;

        /**
         * \brief Register storage manager named "custom".
         *
         * \details This function acquires ownership on the pointer
         * to the storage manager.
         *
         * \throw Utils::Exception When the instance of custom storage
         * manager is already registered.
         *
         * \warning This function is not thread safe.
         *
         * \param [in] Pointer to the StorageMgr object.
         */
        static void
        registerCustomStorage(StorageMgr*);

        /**
         * \brief Add new storage manager.
         *
         * \details This function acquires ownership on the pointer
         * to the storage manager.
         *
         * \throw Utils::Exception When the manager with the specified
         * name is already registered.
         *
         * \warning This function is not thread safe.
         *
         * \param [in] sname String identifier of the storage.
         * \param [in] storageManager Pointer to the storage instance.
         */
        static void registerStorageManager(std::string const & sname,
            StorageMgr * storageManager);

        /**
         * \brief Get the number of registered storages.
         *
         * \warning This function can't be accessed during static
         * initialization. Otherwise you'll get an undefined behavior.
         * This function is not thread safe.
         *
         * \return std::size_t Number of actually registered torages.
         */
        static std::size_t totalStorageManagers();

        /**
         * \brief Check if the specified storage already registered.
         *
         * \warning This function is not thread safe.
         *
         * \param [in] name Storage identifier.
         *
         * \return true if the name of the storage is already registered
         * and false if the name is not registered.
         */
        static bool const
        isStorageManagerRegistered(std::string const & name);

        /**
         * \brief Get the list of the registered storages.
         *
         * \details The vector contains names of storages in the order
         * of their registration so it's possible to determine the
         * index of the storage in the registry.
         *
         * \warning This function is not thread safe.
         */
        static std::vector<std::string> const &
        storageManagersNames();

        /**
         * \brief This destructor is implemented by StorageMgr and
         * used by StorageMgr.
         *
         * \details This destructor of StorageMgr implementation is
         * used by "central" StorageMgr registry to deallocate resources
         * previously acquired.
         */
        virtual ~StorageMgr();
    protected:
        StorageMgr();
    };

}

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif
