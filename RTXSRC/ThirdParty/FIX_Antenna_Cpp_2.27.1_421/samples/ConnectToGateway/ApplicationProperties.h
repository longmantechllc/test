/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#ifndef H_B2BITS_Engine_Samples_CTGW_ApplicationProperties_H
#define H_B2BITS_Engine_Samples_CTGW_ApplicationProperties_H

#include <string>

#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_Session.h>

////////////////////////////////////////////////////////////////////////////////
// Constants.

/// Session heart beat interval.
const int HEART_BEAT_INTERVAL = 30;


/// Application parameters
class ApplicationProperties
{
public: // tyopes
    class SessionParameters
    {
    public: // fields
        /// Session's sender identifier.
        std::string senderCompId_;

        /// Session's target identifier.
        std::string targetCompId_;

        /// Host address.
        std::string host_;

        /// Port number.
        int port_;

        /// The expected MsgSeqNum of the next incoming message.
        int inSeqNumber_;

        int outSeqNumber_;

        std::string senderSubId_; // SenderSubID (tag = 50)
        std::string targetSubId_; // TargetSubID (tag = 57)
        std::string senderLocationId_;
        std::string targetLocationId_;

        /// Session version (default is the oldest).
        Engine::FIXVersion fixVersion_;

        int hbi_;

        bool intradayLogoutTolerance_;

        bool forceReconnect_;
        bool ignoreSeqNumTooLowAtLogon_;
        bool rejectMessageWhileNoConnection_;
        bool keepState_;
        Engine::ForceSeqNumResetMode forceSeqNumReset_;
        bool disableTCPBuffer_;
        bool aggressiveSend_;
        int maxMessageInBunch_;
        Engine::Session::ActiveConnection activeConnection_;
        bool autoSwitchConnection_;
        bool cyclicSwitchConnection_;

        std::string backupHost_;
        int backupPort_;
        std::string backupSenderSubId_;
        std::string backupTargetSubId_;
        std::string backupSenderLocId_;
        std::string backupTargetLocId_;
        std::string user_;
        std::string password_;
		/**
		 * Hide username and password in logs
		 */

		bool hiddenLogonCredentials_;

        int backupHBI_;
        bool backUpIntradayLogoutTolerance_;
        bool backupForceReconn_;
        bool backupIgnoreSeqNumTooLow_;
        bool backupRejectNoConn_;
        bool backupAutoSwitchConnection_;
        bool backupCyclicSwitchConnection_;
        bool isFIXTSsnProtocol_;

        bool interactiveResend_;
        std::string customLogon_;
    public: // constructors
        SessionParameters()
            : port_(0)
            , inSeqNumber_( -1 )
            , outSeqNumber_( -1 )
            , fixVersion_( Engine::FIX44 )
            , hbi_( HEART_BEAT_INTERVAL )
            , intradayLogoutTolerance_( false )
            , forceReconnect_( false )
            , ignoreSeqNumTooLowAtLogon_( false )
            , rejectMessageWhileNoConnection_( false )
            , keepState_( true )
            , forceSeqNumReset_( Engine::FSNR_OFF )
            , disableTCPBuffer_( false )
            , aggressiveSend_( false )
            , maxMessageInBunch_( 0 )
            , activeConnection_( Engine::Session::PRIMARY_CONNECTION )
            , autoSwitchConnection_( false )
            , cyclicSwitchConnection_( false )
            , backupPort_( -1 )
            , backupHBI_( -1 )
            , backUpIntradayLogoutTolerance_( false )
            , backupForceReconn_( false )
            , backupIgnoreSeqNumTooLow_( false )
            , backupRejectNoConn_( false )
            , backupAutoSwitchConnection_( false )
            , backupCyclicSwitchConnection_( false )
            , isFIXTSsnProtocol_( false )
            , interactiveResend_( false )
			, hiddenLogonCredentials_( false ) {
        }
    };

public: // fields
    /// Filename in which example message is stored.
    std::string messageFileName_;
    SessionParameters sessionParams_;
};

#endif // H_B2BITS_Engine_Samples_CTGW_ApplicationProperties_H
