#include "RTX_OSM.h"

#include <iostream>
#include <fstream>
#include <chrono>

#include "Helper.h"
#include "FIX_Convertor.h"
#include "spdlog/spdlog.h"
#include "Util.h"

using namespace std;
using namespace OSM;

namespace {
    void Sor2OsmOrder(const Order &ord, OSMOrder &osmOrder) {
        osmOrder.pOrderId = ord.orderID_;
        osmOrder.childOrderId = ord.childOrderId_;
        osmOrder.price = ord.price_;
        osmOrder.side = ord.side_;
        osmOrder.qty = ord.size_;
        osmOrder.tif = ord.TIF_;
        osmOrder.displaySize = ord.displaySize_;
        osmOrder.type = ord.type_;
        //osmOrder.status = ERSTATUS::OPEN; // -1 at starting to prevent cancel on new child order
        osmOrder.symbol = ord.symbol_;
        osmOrder.clOrdId = ord.cltOrderId_;
        osmOrder.oClOrdId = ord.cltOrderId_;
        osmOrder.exchange = ord.exchange_;
        osmOrder.account = ord.account_;
        osmOrder.subAccount = ord.subAccount_;
        osmOrder.logInId = ord.logInId_;
        osmOrder.discretion = ord.discretion_;
        osmOrder.expirationTime = ord.expireTime_;
        osmOrder.execInst = ord.execInst_;
        osmOrder.minQty = ord.minQty_;
        osmOrder.exchange = ord.exchange_;
    }

    void OsmOrder2Sor(const OSMOrder &osmOrder, Order &ord) {

        ord.orderID_ = osmOrder.pOrderId;
        ord.childOrderId_ = osmOrder.childOrderId;
        ord.price_ = osmOrder.price;
        ord.side_ = osmOrder.side;
        ord.size_ = osmOrder.qty;
        ord.TIF_ = osmOrder.tif;
        ord.displaySize_ = osmOrder.displaySize;
        ord.type_ = osmOrder.type;
        //ord.status_ = osmOrder.status;
        ord.symbol_ = osmOrder.symbol;
        ord.cltOrderId_ = osmOrder.clOrdId;
        ord.exchange_ = osmOrder.exchange;
        ord.account_ = osmOrder.account;
        ord.subAccount_ = osmOrder.subAccount;
        ord.logInId_ = osmOrder.logInId;
        ord.discretion_ = osmOrder.discretion;
        ord.expireTime_ = osmOrder.expirationTime;
        ord.execInst_ = osmOrder.execInst;
        ord.minQty_ = osmOrder.minQty;
        ord.exchange_ = osmOrder.exchange;
    }

    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}

// new order requests
void RTX_OSM::onMessage( const FIX44::NewOrderSingle& msg,
                const FIX::SessionID& sessionID ){
    // convert to RTX
    auto receiveTime = getNow();
    OSMOrder osmOrder;
    FIX_Convertor::toOSM(msg,osmOrder );
    osmOrder.oClOrdId = osmOrder.clOrdId;
    osmOrder.pOrderId = ++ m_toKeep->parentOrdId;
    Order ord;
    OsmOrder2Sor(osmOrder, ord);
    printOrder("New client order: ", ord);

    {
        std::unique_lock guard(m_pmutex);
        m_clOrdId2OrderId[osmOrder.clOrdId] = osmOrder.pOrderId;
        m_parentOrderMap.emplace(osmOrder.pOrderId, osmOrder);
    }

    // SOR API to initiate parent order
    auto success = RTXOrderIO_->StartParentOrder(&ord);
    spdlog::info("StartParentOrder() {}: OID={}", success? "OK":"FAILED", ord.orderID_);

    {
        std::unique_lock guard(m_queueProtection[0]);
        m_parentQ.push(tblParentOrder{ord.orderID_,  receiveTime, params_.routerName, 0,
                                  ord.cltOrderId_, ord.symbol_,ord.side_, ord.size_, ord.displaySize_,
                                  (uint16_t)ord.type_, ord.price_.ToDouble(), ord.discretion_.ToDouble(),
                                  ord.TIF_,to_string(osmOrder.expirationTime),
                                  osmOrder.execInst, osmOrder.minQty,
                                  osmOrder.subAccount, osmOrder.logInId, success? "New":"Rejected"});
    }
    ++m_tblRecordNum[0];
}

// cancel parent order
void RTX_OSM::onMessage( const FIX44::OrderCancelRequest& msg, const FIX::SessionID& )
{
    int orderId = 0;
    string oClOrdId =  FIX_Convertor::getFieldValue<FIX::OrigClOrdID>(msg);
    {
        std::shared_lock guard(m_pmutex);
        if (m_clOrdId2OrderId.count(oClOrdId))
            orderId = m_clOrdId2OrderId[oClOrdId];
    }
	if (!orderId) {
        spdlog::info("client order {} doesn't exist!", oClOrdId );
        return;
	}

    string clOrdId =  FIX_Convertor::getFieldValue<FIX::ClOrdID>(msg);
	auto& order = m_parentOrderMap[orderId];
    if (order.status == ERSTATUS::FILLED || order.status == ERSTATUS::CANCELED ||
        order.status == ERSTATUS::REJECTED || order.accumQty == order.qty || order.leaveQty == 0 ||
        order.status == ERSTATUS::PENDING_CANCEL) { // it's in the process of pending cancel already
        sendRejectCancel(order, clOrdId);
        return;
    }

    order.clOrdId = clOrdId;
    order.oClOrdId = oClOrdId;
    order.status = ERSTATUS::PENDING_CANCEL;
    if (params_.sendPendingCancel) {
        ackPendingCancel(orderId, msg, order.transactTime);
    }

    spdlog::info("Cancelling origClOrdId {}, parent order id {}, clOrdId {}, order status {}",
                 oClOrdId, orderId, clOrdId, order.status);

	RTXOrderIO_->CancelParentOrder(orderId);
    spdlog::info("processCancelOrder() OK");
}

// cancel replace parent order
void RTX_OSM::onMessage( const FIX44::OrderCancelReplaceRequest& msg, const FIX::SessionID& ) {
    int orderId = 0;
    string oClOrdId =  FIX_Convertor::getFieldValue<FIX::OrigClOrdID>(msg);
    {
        std::shared_lock guard(m_pmutex);
        if (m_clOrdId2OrderId.count(oClOrdId))
            orderId = m_clOrdId2OrderId[oClOrdId];
    }

    if (!orderId) {
        spdlog::info("client order {} doesn't exist!", oClOrdId);
        return;
    }

    auto& pOrder = m_parentOrderMap[orderId];
    int newQty = FIX_Convertor::getFieldValue<FIX::OrderQty>(msg);

    if (pOrder.accumQty >= newQty || pOrder.status == ERSTATUS::FILLED || pOrder.status == ERSTATUS::CANCELED ||
        pOrder.status == ERSTATUS::REJECTED || pOrder.leaveQty == 0 || pOrder.status == ERSTATUS::PENDING_CANCEL) {
        sendRejectCancelReplace(orderId, msg, pOrder.transactTime);
        return;
    }

    if (params_.sendPendingCancelReplace) {
        ackPendingCancelReplace(orderId, msg, pOrder.transactTime);
    }

    string clOrdId =  FIX_Convertor::getFieldValue<FIX::ClOrdID>(msg);
    pOrder.clOrdId = clOrdId;
    pOrder.oClOrdId = oClOrdId;
    auto old_status = pOrder.status;
    pOrder.status = ERSTATUS::PENDING_CANCELREPLACE;

    spdlog::info("Cancel Replace origClOrdId {}, parent order id {}, clOrdId {}",
                 oClOrdId, orderId, clOrdId);

    pOrder.newQty = newQty;
    if(pOrder.type != 1)  // none market order
        pOrder.newPx = Price(FIX_Convertor::getFieldValue<FIX::Price>(msg));;

    spdlog::info("cancelling old parent order {}", orderId);
    RTXOrderIO_->CancelParentOrder(orderId);
}

// SOR callback with child order
void RTX_OSM::sendChild(const Order& ord)
{
    printOrder("Child order from SOR: ", ord);
    OSMOrder osmOrder;
    Sor2OsmOrder(ord, osmOrder);

    // pass FIX 4.4 cancel Order Single using the Flat model
    FIX44::NewOrderSingle gwMsg;

    FIX_Convertor::fromRTX(ord, gwMsg, m_virtualDestMap);
    {
        std::unique_lock guard(m_mutex);
        auto clOrdId = getNewGWId();
        gwMsg.set(FIX::ClOrdID(clOrdId));
        osmOrder.clOrdId = clOrdId;
        m_childOrderMap.emplace(ord.childOrderId_, osmOrder);
        m_exch2ChildOrderMap.emplace(clOrdId, osmOrder.childOrderId);
    }
    {
        std::unique_lock guard(m_queueProtection[1]);
        m_childQ.push(tblChildOrder{osmOrder.pOrderId, osmOrder.childOrderId, osmOrder.clOrdId, getNow(), params_.routerName,
                                osmOrder.qty, osmOrder.displaySize, (uint16_t)osmOrder.type, osmOrder.price.ToDouble(),
                                osmOrder.discretion.ToDouble(),osmOrder.tif, osmOrder.exchange,
                                to_string(osmOrder.expirationTime), osmOrder.execInst, osmOrder.minQty, "Child"});
    }
     ++m_tblRecordNum[1];
     // Send order to GW
    send(session_, gwMsg);
    spdlog::info("sent the message to GW: {}", gwMsg.toString());
}

bool RTX_OSM::cancelChild(std::string childOrderId) {
    if (!m_childOrderMap.count(childOrderId)) {
        spdlog::info("cancelChild failed : childOrderId {} doesn't exist", childOrderId);
        return false;
    }

    auto& ord = m_childOrderMap[childOrderId];
    printOrder("Cancel child order: ", ord);
    if (ord.status == -1) { // new order, waiting for ack to get exch order id.
        spdlog::info("cancelChild failed : childOrderId {} hasn't ack'ed yet from exchange", childOrderId);
        return false;
    }

    FIX44::OrderCancelRequest gwMsg;
    FIX_Convertor::fromOSM(ord, gwMsg);
    // need to generate new ClOrdId;
    auto clOrdId = getNewGWId();
    gwMsg.set(FIX::ClOrdID(clOrdId));
    ord.oClOrdId = ord.clOrdId;
    ord.clOrdId = clOrdId;
    gwMsg.set(FIX::OrigClOrdID(ord.oClOrdId));
    // Send order to GW
    send(session_, gwMsg);
    spdlog::info("sent child order cancel message to GW: {}", gwMsg.toString());
    return true;
}

// execution reports from Gateway
void RTX_OSM::onMessage( const FIX44::ExecutionReport& msg, const FIX::SessionID& ){
    if (params_.recoveryMode)
        return onRecoveryMessage(msg);

    ExecutionReport er;

    std::string gwOrdId = FIX_Convertor::getFieldValue<FIX::ClOrdID>(msg);
    int refOrderId;
    {
        std::shared_lock guard(m_mutex);

        if (!m_exch2ChildOrderMap.count(gwOrdId) ||
            !m_childOrderMap.count(m_exch2ChildOrderMap[gwOrdId])) {
            spdlog::info("can't find GW order with ClOrdID {}", gwOrdId);
            return;
        }
        auto &it = m_childOrderMap[m_exch2ChildOrderMap[gwOrdId]];
        it.exOrderId = FIX_Convertor::getFieldValue<FIX::OrderID>(msg);

        FIX_Convertor::toRTX(msg, er);
        er.orderId_ = it.pOrderId;
        er.side_ = it.side;
        er.type_ = it.type;
        er.TIF_ = it.tif;
        er.size_ = it.qty;
        er.exchange_ = it.exchange;
        er.childOrderId_ = it.childOrderId;
        refOrderId = it.refOrderId;
        it.status = er.status_; // tracking child order statusa
        er.price_ = it.price;
    }
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{er.orderId_, er.childOrderId_, er.execId_, refOrderId, params_.routerName,
                           getNow(), er.status_});
    }
    ++m_tblRecordNum[2];
    //spdlog::info("update tblState with orderId {}, childOrderId {}, status {}", er.orderId_, er.childOrderId_, er.status_);
    if (er.status_ == ERSTATUS::FILLED || er.status_ == ERSTATUS::PARTIALY_FILLED) {
        FIX::TransactTime tt;
        string transTime;
        if (msg.isSetField(tt)) {
            msg.get(tt);
            transTime = to_string(tt.getValue().getTimeT());
        }
        {
            std::unique_lock guard(m_queueProtection[3]);
            m_exeQ.push(tblExecution{er.orderId_, er.childOrderId_, er.execId_, params_.routerName, getNow(),
                                 er.price_.ToDouble(), er.exchange_, er.TIF_, er.fillSize_, er.fillPrice_.ToDouble(),
                                 er.cumulativeFillQuantity_, er.avgPrice_.ToDouble(), "Child Execution",
                                 transTime}); // only child order needs trasactTime in DB if exchange sends it.
        }
        ++m_tblRecordNum[3];
    }

    if (er.status_ == ERSTATUS::CANCELED) {
        spdlog::info("cancelled GW child order {}", er.childOrderId_);
    }

    // forward to SOR
    try {
        printER("ER from GW: ", er);
        RTXOrderIO_->ReceiveExecutionReport(&er);
    }
    catch (...) {
        spdlog::error("ReceiveExecutionReport() failed");
        return;
    }
}

void RTX_OSM::onRecoveryMessage(const FIX44::ExecutionReport& msg) {

    std::string gwOrdId = FIX_Convertor::getFieldValue<FIX::ClOrdID>(msg);

    if (!m_recoveryCOrderMap.count(gwOrdId)) {
        spdlog::error("can't find GW order in recovery mode with ClOrdID {}", gwOrdId);
        return;
    }
    auto [pOrderId, childOrderId, venue] = m_recoveryCOrderMap[gwOrdId];
    OSMRecoveredOrder& pOrder = m_recoveryPOrderMap[pOrderId];
    std::string execId = FIX_Convertor::getFieldValue<FIX::ExecID>(msg);
    int status =  FIX_Convertor::getFieldValue<FIX::OrdStatus>(msg)-'0';
    m_stateQ.push(tblState{pOrderId, childOrderId, execId, pOrder.refOrderId, params_.routerName,
                           getNow(), status});
     ++m_tblRecordNum[2];
    if (status == ERSTATUS::FILLED || status == ERSTATUS::PARTIALY_FILLED) {
        int lastQty = FIX_Convertor::getFieldValue<FIX::LastQty>(msg);
        double lastPx = FIX_Convertor::getFieldValue<FIX::LastPx>(msg);
        int cumQty = FIX_Convertor::getFieldValue<FIX::CumQty>(msg);
        double avgPrice = FIX_Convertor::getFieldValue<FIX::AvgPx>(msg);
        m_exeQ.push(tblExecution{pOrderId, childOrderId, execId, params_.routerName, getNow(),
                                 pOrder.price, venue, pOrder.tif, lastQty, lastPx,
                                 cumQty, avgPrice, "Child Execution"});
         ++m_tblRecordNum[3];
        // parent order ER report
        FIX44::ExecutionReport erMsg;

        erMsg.set(FIX::ExecID(getNewExecId()));
        erMsg.set(FIX::OrderID(to_string(pOrder.pOrderId)));
        if (!pOrder.desk.empty())
            erMsg.getHeader().set(FIX::SenderSubID(pOrder.desk));
        if (!pOrder.user.empty())
            erMsg.getHeader().set(FIX::OnBehalfOfSubID(pOrder.user));

        erMsg.set(FIX::Symbol(pOrder.symbol));
        erMsg.set(FIX::TimeInForce(pOrder.tif + '0'));
        erMsg.set(FIX::Side(pOrder.side + '0'));
        erMsg.set(FIX::OrdType(pOrder.type + '0'));
        erMsg.set(FIX::ExecType(FIX::ExecType_TRADE));
        erMsg.set(FIX::LastQty(lastQty));
        erMsg.set(FIX::Price(pOrder.price));
        erMsg.set(FIX::LastPx(lastPx));

        if (!venue.empty())
            erMsg.set(FIX::LastMkt(venue)); // populate execution venue

        int accumQty = pOrder.accumQty;
        double notional = pOrder.avgPx * pOrder.accumQty;

        spdlog::info("existing accumQty={}, px={}, new fill qty={}, px={}",
                      accumQty, pOrder.avgPx , lastQty, lastPx);
        accumQty += lastQty;
        notional += (lastPx * lastQty);
        pOrder.accumQty = accumQty;
        pOrder.avgPx = notional / accumQty;

        erMsg.set(FIX::CumQty(pOrder.accumQty));
        int leaveQty = pOrder.qty - pOrder.accumQty;
        erMsg.set(FIX::LeavesQty(leaveQty));
        erMsg.set(FIX::AvgPx(pOrder.avgPx));
        if (leaveQty <= 0)
            pOrder.status = ERSTATUS::FILLED;
        else
            pOrder.status = ERSTATUS::PARTIALY_FILLED;

        erMsg.set(FIX::OrdStatus(pOrder.status+'0'));
        erMsg.set(FIX::TransactTime());

        m_exeQ.push(tblExecution{pOrder.pOrderId, "", std::to_string(m_toKeep->execId),
                                 params_.routerName, getNow(),
                                 pOrder.price, venue, pOrder.tif, lastQty,
                                 lastPx, pOrder.accumQty, pOrder.avgPx,
                                 "Parent Execution"});
        ++m_tblRecordNum[3];
        send(cliSession_, erMsg);
        spdlog::info("Sent ER to client: {}", erMsg.toString());
        m_stateQ.push(tblState{pOrder.pOrderId, "", std::to_string(m_toKeep->execId), pOrder.refOrderId,
                               params_.routerName, getNow(), pOrder.status});
        ++m_tblRecordNum[2];
    }

}

// callback from SOR
void RTX_OSM::sendER(const ExecutionReport& er)
{
    printER("ER from SOR: ", er);
    OSMOrder *orderPtr = nullptr;
    {
        std::shared_lock guard(m_pmutex);

        if (m_parentOrderMap.count(er.orderId_) == 0) {
            spdlog::error("can't find parent order using parent orderId {}", er.orderId_);
            return;
        }
        orderPtr = &m_parentOrderMap[er.orderId_];
    }
    // new cancel replace logic on parent order
    auto& pOrder = *orderPtr; //m_parentOrderMap[er.orderId_];

    pOrder.transactTime = FIX::UtcTimeStamp().getTimeT();
    if (er.status_ == ERSTATUS::CANCELED && pOrder.status == ERSTATUS::PENDING_CANCELREPLACE) {
        if (pOrder.accumQty >= pOrder.newQty) {
            sendRejectCancelReplace(pOrder);
            return;
        }
        else {
            if (pOrder.accumQty == 0)
                pOrder.status = ERSTATUS::OPEN; // there is no 5 in B2Bits dictionary
            else
                pOrder.status = ERSTATUS::PARTIALY_FILLED;

            // hack code to track back new parent order id and refOrderId before the change
            int old_id = pOrder.pOrderId, old_refOrderId = pOrder.refOrderId;
            pOrder.refOrderId = pOrder.pOrderId;
            pOrder.pOrderId = ++ m_toKeep->parentOrdId;
            sendCancelReplaceAck(pOrder);

            OSMOrder newOrder;
            newOrder.pOrderId  = pOrder.pOrderId;
            pOrder.pOrderId = old_id; // recover these two order ids
            pOrder.refOrderId = old_refOrderId;

            newOrder.refOrderId = pOrder.pOrderId;
            newOrder.qty = pOrder.newQty - pOrder.accumQty;
            newOrder.price = pOrder.newPx;
            newOrder.side = pOrder.side;
            newOrder.tif = pOrder.tif;
            newOrder.displaySize = pOrder.displaySize;
            newOrder.type = pOrder.type;
            newOrder.symbol = pOrder.symbol;
            newOrder.exchange = pOrder.exchange;
            newOrder.account = pOrder.account;
            newOrder.subAccount = pOrder.subAccount;
            newOrder.logInId = pOrder.logInId;
            newOrder.clOrdId = pOrder.clOrdId;
            newOrder.oClOrdId = pOrder.oClOrdId;

            newOrder.minQty = pOrder.minQty;
            newOrder.discretion = pOrder.discretion;
            newOrder.expirationTime = pOrder.expirationTime;
            newOrder.execInst = pOrder.execInst;
            //newOrder.fClOrdId = pOrder.fClOrdId;

            newOrder.cxlReplaced = true;
            newOrder.leaveQty = newOrder.qty;
            newOrder.accumQty = pOrder.accumQty;
            newOrder.avgPx = pOrder.avgPx;

            m_clOrdId2OrderId[newOrder.clOrdId] = newOrder.pOrderId; // the clOrdId is used to tracking this new order now

            Order ord;
            OsmOrder2Sor(newOrder, ord);
            printOrder("New parent order after cancel replace: ", ord);
            //m_clOrdId2OrderId[newOrder.clOrdId] = newOrder.pOrderId;
            m_clOrdId2OrderId[newOrder.oClOrdId] = newOrder.pOrderId; // hack for client simulator

            m_parentOrderMap.emplace(newOrder.pOrderId, newOrder);
            m_parentOrderMap.erase(newOrder.refOrderId);

            // SOR API to initiate parent order
            if (!RTXOrderIO_->StartParentOrder(&ord)) { // call sendChild for each child order
                spdlog::error("StartParentOrder for cancel replace failed");
                return;
            }
            spdlog::info("StartParentOrder for cancel replace  OK");
            newOrder.transactTime = FIX::UtcTimeStamp().getTimeT();
            {
                std::unique_lock guard(m_queueProtection[0]);
                m_parentQ.push(tblParentOrder{ord.orderID_, getNow(), params_.routerName,
                                          newOrder.refOrderId, ord.cltOrderId_, ord.symbol_,
                                          ord.side_, ord.size_, ord.displaySize_, (uint16_t)ord.type_,
                                          ord.price_.ToDouble(), ord.discretion_.ToDouble(),ord.TIF_,
                                          to_string(newOrder.expirationTime),
                                          newOrder.execInst, newOrder.minQty,
                                          newOrder.subAccount, newOrder.logInId, "CxlPlace"});
            }
            ++m_tblRecordNum[0];
        }
        return;
    }
    else {
        FIX44::ExecutionReport er_msg;
        FIX_Convertor::fromRTX(er, er_msg);
        er_msg.set(FIX::ExecID(getNewExecId()));

        std::string clOrdId = pOrder.clOrdId;
        er_msg.set(FIX::ClOrdID(clOrdId));
        if (pOrder.status != ERSTATUS::PENDING_CANCELREPLACE && pOrder.status != ERSTATUS::PENDING_CANCEL)
            pOrder.status = er.status_;

        if (er.execType_ == FIX::ExecType_REJECTED || er.execType_ == FIX::ExecType_CANCELLED) { // 8 is reject, 4 is canceled
            if (er.execType_ == FIX::ExecType_CANCELLED) {// cancelled parent order
                //logger << "OID=" << pOrder.pOrderId << " previous state is " << pOrder.status << endl;
                if (pOrder.status == ERSTATUS::PENDING_CANCEL)
                    er_msg.set(FIX::ClOrdID(pOrder.oClOrdId)); // user cancel
                else
                    er_msg.set(FIX::ClOrdID(pOrder.clOrdId)); // unsolicited cancel
                pOrder.status = ERSTATUS::CANCELED; // no longer needs the previous order status now.
            }
        } else if (er.execType_ == 'F') { // it's trade, update order
            pOrder.lastQty = er.fillSize_;
            pOrder.lastPx = er.fillPrice_;
            if (!er.exchange_.empty())
                er_msg.set(FIX::LastMkt(er.exchange_)); // populate execution venue

            if (!pOrder.cxlReplaced) {
                pOrder.accumQty = er.cumulativeFillQuantity_;
                pOrder.avgPx = er.avgPrice_;
            }
            else {
                int accumQty = pOrder.accumQty;
                double notional = pOrder.avgPx.value() * pOrder.accumQty;
                spdlog::info("existing accumQty={}, px={}, new fill qty={}, px={}",
                             accumQty, pOrder.avgPx.value(), pOrder.lastQty, pOrder.lastPx.value());
                accumQty += pOrder.lastQty;
                notional += (pOrder.lastPx.value() * pOrder.lastQty);
                pOrder.accumQty = accumQty;
                pOrder.avgPx = notional / accumQty;
            }
            {
                std::unique_lock guard(m_queueProtection[3]);
                m_exeQ.push(tblExecution{pOrder.pOrderId, "", std::to_string(m_toKeep->execId)/*er.execId_*/,
                                     params_.routerName, getNow(),
                                     pOrder.price.ToDouble(), er.exchange_, pOrder.tif, pOrder.lastQty,
                                     pOrder.lastPx.ToDouble(), pOrder.accumQty, pOrder.avgPx.ToDouble(),
                                     "Parent Execution"});
            }
            ++m_tblRecordNum[3];
        }

        pOrder.leaveQty = er.leaveQty;
        er_msg.set(FIX::CumQty(pOrder.accumQty));
        er_msg.set(FIX::AvgPx(pOrder.avgPx.value()));
        er_msg.set(FIX::LeavesQty(pOrder.leaveQty));
        send(cliSession_, er_msg);
        spdlog::info("Sent ER to client: {}", er_msg.toString());
        {
            std::unique_lock guard(m_queueProtection[2]);
            m_stateQ.push(tblState{pOrder.pOrderId, "", std::to_string(m_toKeep->execId), pOrder.refOrderId,
                               params_.routerName, getNow(), pOrder.status});
        }
        ++m_tblRecordNum[2];
    }
}

void RTX_OSM::sendNewACK(int orderId, const OSMOrder& order)
{
    // Send Execution Report - New
    FIX44::ExecutionReport er;

    er.set(FIX::ClOrdID(order.clOrdId));
    er.set(FIX::Symbol(order.symbol));
    er.set(FIX::Side(order.side+'0'));
    er.set(FIX::OrderQty(order.qty));
    er.set(FIX::OrderID(std::to_string(orderId)));
    er.set(FIX::ExecID(getNewExecId()));
    er.set(FIX::OrdStatus(FIX::OrdStatus_NEW));   // New
    er.set(FIX::ExecType(FIX::ExecType_NEW));    // New
    er.set(FIX::LeavesQty(order.leaveQty));
    er.set(FIX::LastQty(0));
    er.set(FIX::LastPx(0));
    er.set(FIX::CumQty(0));
    er.set(FIX::AvgPx(0));

    FIX::UtcTimeStamp t;
    order.transactTime = t.getTimeT();
    er.set(FIX::TransactTime(t));

    if (!order.account.empty())
        er.set(FIX::Account(order.account));
    auto& header = er.getHeader();
    if (!order.subAccount.empty())
        header.set(FIX::SenderSubID(order.subAccount));
    if (!order.logInId.empty())
        header.set(FIX::OnBehalfOfSubID(order.logInId));

    send(cliSession_, er);
    spdlog::info("Sent ACK to client: {}", er.toString());
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId), order.refOrderId,
                           params_.routerName, getNow(), ERSTATUS::OPEN});
    }
    ++m_tblRecordNum[2];
}

void RTX_OSM::sendACK(const ExecutionReport& er)
{
    //logger << "Ack to client on OID=" << er.orderId_ <<  endl;
    //mutex_.lock();
    OSMOrder *orderPtr = nullptr;
    {
        std::shared_lock guard(m_pmutex);
        if (m_parentOrderMap.count(er.orderId_) == 0) {
            spdlog::error("Can't find parent order id {}", er.orderId_);
            return;
        }
        orderPtr = &m_parentOrderMap[er.orderId_];
    }
    auto& ord = *orderPtr;
    if (ord.refOrderId) // don't ack it now, because it's cancel replacement on new order
        return;
    ord.leaveQty = ord.qty;
    sendNewACK(er.orderId_, ord);

}

void RTX_OSM::ackPendingCancel(int orderId, const FIX44::OrderCancelRequest& msg, time_t& ts) {
    // Send Execution Report - PendingCancel
    FIX44::ExecutionReport er;
    er.set(FIX_Convertor::getField<FIX::OrigClOrdID>(msg));
    er.set(FIX_Convertor::getField<FIX::ClOrdID>(msg));
    er.set(FIX_Convertor::getField<FIX::Symbol>(msg));
    er.set(FIX_Convertor::getField<FIX::Side>(msg));
    auto& order = m_parentOrderMap[orderId];
    er.set(FIX::OrderID(std::to_string(orderId)));
    er.set(FIX::ExecID(getNewExecId()));
    er.set(FIX::OrdStatus(FIX::OrdStatus_PENDING_CANCEL));   // Pending Cancel
    er.set(FIX::ExecType(FIX::ExecType_PENDING_CANCEL));    // New
    er.set(FIX::LeavesQty(order.leaveQty));
    er.set(FIX::CumQty(order.accumQty));
    er.set(FIX::AvgPx(order.avgPx.value()));
    auto t = FIX::UtcTimeStamp();
    ts = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId),
                           order.refOrderId, params_.routerName, getNow(), ERSTATUS::PENDING_CANCEL});
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent PendingCancel to client: {}", er.toString());
}

void RTX_OSM::ackPendingCancelReplace(int orderId, const FIX44::OrderCancelReplaceRequest &msg, time_t& ts) {
    // Send Execution Report - PendingCancelReplace
    FIX44::ExecutionReport er;

    er.set(FIX_Convertor::getField<FIX::ClOrdID>(msg));
    er.set(FIX_Convertor::getField<FIX::Symbol>(msg));
    er.set(FIX_Convertor::getField<FIX::Side>(msg));
    auto& ord = m_parentOrderMap[orderId];

    er.set(FIX_Convertor::getField<FIX::OrigClOrdID>(msg));

    er.set(FIX::OrderID(std::to_string(orderId)));
    er.set(FIX::ExecID(getNewExecId()));
    er.set(FIX::OrdStatus(FIX::OrdStatus_PENDING_CANCEL_REPLACE));   // Pending Cancel Replace
    er.set(FIX::ExecType(FIX::ExecType_PENDING_CANCEL_REPLACE));
    er.set(FIX::LeavesQty(ord.leaveQty));
    er.set(FIX::CumQty(ord.accumQty));
    er.set(FIX::AvgPx(ord.avgPx.value()));
    if (ord.type != 1) // non market order
        er.set(FIX::Price(ord.price.value()));
    auto t = FIX::UtcTimeStamp();
    ts = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{ord.pOrderId, "", std::to_string(m_toKeep->execId), ord.refOrderId,
                           params_.routerName, getNow(), ERSTATUS::PENDING_CANCELREPLACE});
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent PendingCancelReplace to client: {}", er.toString());
}

void RTX_OSM::sendRejectCancel(const OSMOrder& order, const string& clOrdId) {
    FIX44::OrderCancelReject er;
    er.set(FIX::OrigClOrdID(order.clOrdId));
    er.set(FIX::ClOrdID(clOrdId));
    er.set(FIX::OrderID(std::to_string(order.pOrderId)));
    er.set(FIX::OrdStatus(order.status+'0')); // or '8' for reject?
    er.set(FIX::CxlRejResponseTo(FIX::CxlRejResponseTo_ORDER_CANCEL_REQUEST));   // for cancel
    auto t = FIX::UtcTimeStamp();
    order.transactTime = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId), order.refOrderId,
                           params_.routerName, getNow(), order.status});
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent Order Cancel Reject to client: {}", er.toString());
}

void RTX_OSM::sendRejectCancelReplace(int orderId, const FIX44::OrderCancelReplaceRequest& msg, time_t& ts) {
    FIX44::OrderCancelReject er;

    er.set(FIX_Convertor::getField<FIX::ClOrdID>(msg));
    auto& order = m_parentOrderMap[orderId];
    er.set(FIX_Convertor::getField<FIX::OrigClOrdID>(msg));
    er.set(FIX::OrderID(std::to_string(orderId)));
    /*
    ord.status =  ERSTATUS::OPEN;
    if (ord.accumQty < ord.qty) {
        ord.status = ERSTATUS::PARTIALY_FILLED;
    }
    */
    er.set(FIX::OrdStatus(order.status+'0'));
    er.set(FIX::CxlRejResponseTo(FIX::CxlRejResponseTo_ORDER_CANCEL_REPLACE_REQUEST));   // for cancel replace
    auto t = FIX::UtcTimeStamp();
    ts = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId), order.refOrderId,
                           params_.routerName, getNow(), order.status});
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent Order Cancel Reject to client: {}", er.toString());
}

void RTX_OSM::sendRejectCancelReplace(OSMOrder& order) {
    FIX44::OrderCancelReject er;
    er.set(FIX::ClOrdID(order.clOrdId));
    er.set(FIX::OrigClOrdID(order.oClOrdId));
    er.set(FIX::OrderID(std::to_string(order.pOrderId)));
    order.status =  ERSTATUS::CANCELED;
    er.set(FIX::OrdStatus(order.status+'0'));
    er.set(FIX::CxlRejResponseTo(FIX::CxlRejResponseTo_ORDER_CANCEL_REPLACE_REQUEST));
    auto t = FIX::UtcTimeStamp();
    order.transactTime = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId), order.refOrderId,
                           params_.routerName, getNow(), order.status});
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent Order Cancel Reject to client: {}", er.toString());
}

void RTX_OSM::sendCancelReplaceAck(const OSMOrder& order) {
    // Send Execution Report - CancelReplaceAck
    FIX44::ExecutionReport er;
    er.set(FIX::OrigClOrdID(order.oClOrdId));
    er.set(FIX::ClOrdID(order.clOrdId));
    er.set(FIX::Symbol(order.symbol));
    er.set(FIX::Side(order.side+'0'));
    er.set(FIX::OrderQty(order.newQty));
    er.set(FIX::LeavesQty(order.newQty - order.accumQty));
    er.set(FIX::OrderID(std::to_string(order.pOrderId)));
    er.set(FIX::ExecID(getNewExecId()));
    if (order.accumQty == 0)
        er.set(FIX::OrdStatus(FIX::OrdStatus_NEW));   // order.status
    else
        er.set(FIX::OrdStatus(FIX::OrdStatus_PARTIALLY_FILLED));
    er.set(FIX::ExecType(FIX::ExecType_REPLACED)); // replaced
    er.set(FIX::CumQty(order.accumQty));
    er.set(FIX::AvgPx(order.avgPx.value()));
    auto t = FIX::UtcTimeStamp();
    order.transactTime = t.getTimeT();
    er.set(FIX::TransactTime(t));

    send(cliSession_, er);
    {
        std::unique_lock guard(m_queueProtection[2]);
        m_stateQ.push(tblState{order.pOrderId, "", std::to_string(m_toKeep->execId),
                           order.refOrderId, params_.routerName,
                           getNow(), 5}); // replaced
    }
    ++m_tblRecordNum[2];
    spdlog::info("Sent CancelReplaceAck to client: {}", er.toString());
}
