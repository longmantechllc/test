//
// Created by schen on 12/29/2020.
//

#ifndef RTXROUTER_TOBCONTAINER_H
#define RTXROUTER_TOBCONTAINER_H

#include <string>
#include <memory>
#include <vector>
#include <unordered_set>
#include <mutex>

using namespace std;
class TopOfBook;
class Price;
class Order;
class TobQuote;
class Logger;

class TobList
{
public:
    TobList();
    virtual ~TobList();
    void SetLog(Logger* log);
    void SetSide(int side);

    void AddTob(const TopOfBook& tob, const int pri);

    std::vector <std::shared_ptr< TobQuote >> GetTobList(); // {return m_quoteList;}
    void SetDefinedMmid(std::unordered_set< std::string > list){m_definedMmid = list;}

    std::vector <std::shared_ptr< TobQuote >> GetBestPriceQuoteList();
    Price GetBestQuotePrice();

    void RemoveQuoteByMmid(const std::string& mmid);
    void RemoveQuoteByMmidList(const std::vector<std::string>& mmidList);
    void SetQuoteConsumedByMmid(const std::string& mmid, int consumedQty);

private:
    std::vector < std::shared_ptr< TobQuote >> m_quoteList;
    std::unordered_set< std::string > m_definedMmid;
    bool m_isAsk = true;
    Logger* m_logger;                                                           //for system log

    std::mutex lock_;
};

#endif //RTXROUTER_TOBCONTAINER_H
