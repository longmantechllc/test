// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXFieldValue.h
/// Contains Engine::FIXFieldValue class declaration.

#ifndef __B2BITS_FIXFieldValue_h__
#define __B2BITS_FIXFieldValue_h__

#include <cstddef>

#include <B2BITS_V12_Defines.h>
#include <B2BITS_IntDefines.h>

namespace Engine
{

    /// It is used for storing the Contents information (data + length).
    struct V12_API FIXFieldValue {

        /// Pointer to byte array.
        const char* data_;

        /// Length of sequence.
        std::size_t length_;

        /// Constructs empty sequence.
        FIXFieldValue();

        /// Constructs object from null-terminated C-string.
        explicit FIXFieldValue( const char* str );

        /// Constructs object from char chain.
        FIXFieldValue( const char* data, std::size_t length );

        /// Resets internal storage
        void clear();

        /// Verifies is sequence empty.
        bool isEmpty() const;

        /// Returns signed numeric value of sequence.
        int toSigned() const;

        /// Returns signed 64bit numeric value of sequence.
        /// @throw Utils::Exception If value cannot be converted
        System::i64 toSigned64() const;

        /// Returns signed 64bit numeric value of sequence.
        /// @throw Utils::Exception If value cannot be converted
        System::u64 toUnsigned64() const;

        /// Returns unsigned numeric value of sequence.
        /// @throw Utils::Exception If value cannot be converted
        unsigned int toUnsigned() const;

        /// Compares two object. Based on compare() member.
        /// @param fieldValue Instance of FIXFieldValue to compare with.
        bool operator ==( const FIXFieldValue& fieldValue ) const;

        /// Compares with null-terminated char* std::string. Based on compare() member.
        /// @param val null-terminated std::string to compare with.
        bool operator ==( const char* val ) const;

        /// Compares two object. Based on compare() member.
        /// @param fieldValue Instance of FIXFieldValue to compare with.
        bool operator !=( const FIXFieldValue& fieldValue ) const;

        /// Compares two sequences in 'std::strcmp' style.
        /// @param firstValue lvalue
        /// @param secondValue rvalue
        static int compare( const FIXFieldValue& firstValue,
                            const FIXFieldValue& secondValue );


    };

    // Verifies is sequence empty.
    inline bool FIXFieldValue::isEmpty() const
    {
        return ( 0 == length_ );
    }

    // Constructs empty sequence.
    inline FIXFieldValue::FIXFieldValue()
        : data_( NULL ), length_( 0 )
    {
    }

    // Constructs sequence from raw buffer and its length.
    inline FIXFieldValue::FIXFieldValue( const char* data, std::size_t length )
        : data_( data )
        , length_( length )
    {
    }


    // Compares two objects.
    inline bool FIXFieldValue::operator ==( const FIXFieldValue& fieldValue ) const
    {
        return ( 0 == compare( *this, fieldValue ) );
    }

    inline bool FIXFieldValue::operator ==( const char* value ) const
    {
        return 0 == compare( *this, FIXFieldValue( value ) );
    }

    // Compares two objects.
    inline bool FIXFieldValue::operator !=( const FIXFieldValue& fieldValue ) const
    {
        return ( 0 != compare( *this, fieldValue ) );
    }
}

#endif // __B2BITS_FIXFieldValue_h__

