/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <string>
#include <memory>

#include <B2BITS_V12.h>
#include "FixApp.h"

using namespace std;

const char* LISTENER_IP = "127.0.0.1";
const int LISTENER_PORT = 9022;

int main()
{
    cout << "starting FIX Sender..." << endl;

    try {
        // Initializes engine.
        // If error appears during initialization (properties file is not found, required property is missing, etc.) then exception is thrown.
        Engine::FixEngine::init( "sender.engine.properties" );

        // Create Application instance. CFIXApp class derived from Engine::Application
        CFIXApp application;

        cout << "starting FIX Sender session..." << endl;

        Engine::SessionExtraParameters params;
        params.intradayLogoutToleranceMode_ = true;

        // Create FIX 4.3 session instance
        Engine::Session* pSIFIX43 =  Engine::FixEngine::singleton()->createSession( &application, "TargetCompID_FIX43", "SenderCompID_FIX43",  Engine::FIX43, &params );

        // Connect session as initiator
        pSIFIX43->connect( 30, LISTENER_IP, LISTENER_PORT );

        // create FIX 4.3 Order Cancel Request using the Flat model
        std::auto_ptr<Engine::FIXMessage> pFIX43Message( Engine::FIXMsgFactory::singleton()->newSkel( Engine::FIX43, "F" ) );

        // Fill required by FIX 4.3 fields
        std::string clrodid;
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pFIX43Message->set( FIXField::OrigClOrdID, clrodid );
        pFIX43Message->set( FIXField::ClOrdID, clrodid );
        pFIX43Message->set( FIXField::Symbol, "MSFT" );
        pFIX43Message->set( FIXField::Side, '1' ); // Buy
        pFIX43Message->set( FIXField::OrderQty, 400 );
        pFIX43Message->set( FIXField::OrdType, '2' ); // Limit
        pFIX43Message->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );

        // Fill custom fields
        pFIX43Message->set( 555, 1 );
        pFIX43Message->getGroup( 555 )->set( 600, "MSFS", 0 );
        pFIX43Message->getGroup( 555 )->set( 616, "XAMS", 0 );
        pFIX43Message->getGroup( 555 )->set( 624, '2', 0 );
        pFIX43Message->getGroup( 555 )->set( 600, "MSFT", 0 );
        pFIX43Message->getGroup( 555 )->set( 616, "XAMS", 0 );
        pFIX43Message->getGroup( 555 )->set( 624, '1', 0 );

        cout << "sending the message: " << *pFIX43Message->toString( '|' ) << endl << endl;

        // Send order to session initiator
        pSIFIX43->put( pFIX43Message.get() );

        // Wait for trade
        application.tradeReceived_.wait();

        // Closing the session
        pSIFIX43->disconnect();
        application.logoutReceived_.wait();

        // Releasing resources
        pSIFIX43->registerApplication( NULL );
        pSIFIX43->release();

        // Load new FIX protocol dialect from XML file and store ParserID
        Engine::FixEngine::singleton()->loadProtocolFile( "custom_fix44.xml" );
        Engine::ParserID pid = Engine::FixEngine::singleton()->createFixParser( "FIX44_MY" );
        // Create session using customized FIX44 protocol
        Engine::Session* pSIFIX44 = Engine::FixEngine::singleton()->createSession( &application, "TargetCompID_FIX44", "SenderCompID_FIX44", pid, &params );
        // Start session as initiator
        pSIFIX44->connect( 30, LISTENER_IP, LISTENER_PORT );

        // Create and fill default "X" message fields
        std::auto_ptr<Engine::FIXMessage> pFIX44Message( Engine::FIXMsgFactory::singleton()->newSkel( pid, "X" ) );
        pFIX44Message->set( FIXField::NoMDEntries, 1 );
        pFIX44Message->getGroup( FIXField::NoMDEntries )->set( FIXField::MDUpdateAction, '0', 0 );
        pFIX44Message->getGroup( FIXField::NoMDEntries )->set( FIXField::Symbol, "MSTS", 0 );
        pFIX44Message->getGroup( FIXField::NoMDEntries )->set( FIXField::NoUnderlyings, 1, 0 );
        pFIX44Message->getGroup( FIXField::NoMDEntries )->getGroup( FIXField::NoUnderlyings, 0 )->set( FIXField::UnderlyingSymbol, "MSTS", 0 );

        // Fill customized fields
        pFIX44Message->set( 5001, 1 );
        pFIX44Message->getGroup( 5001 )->set( 5002, "APL", 0 );

        cout << "sending the message: " << *pFIX44Message->toString( '|' ) << endl << endl;

        // Send message to session initiator
        pSIFIX44->put( pFIX44Message.get() );

        // Wait for response
        application.tradeReceived_.wait();

        // Closing the session
        pSIFIX44->disconnect();
        application.logoutReceived_.wait();

        // Releasing resources
        pSIFIX44->registerApplication( NULL );
        pSIFIX44->release();

        // Destroying the engine
        Engine::FixEngine::destroy();
    } catch( const std::exception& ex ) {
        cout << "Error: " << ex.what() << endl;

        // Destroying the engine
        Engine::FixEngine::destroy();
    } catch ( ... ) {
        cout << "Unknown error." << endl;

        // Destroying the engine
        Engine::FixEngine::destroy();
    }

    return 0;
}
