//
// Created by schen on 2/8/2021.
//


#include <iostream>
#include <memory>
#include <thread>
#include <vector>
//#include <queue>
#include <map>
#include <chrono>
#include <mutex>
#include <functional>
#include <condition_variable>

#include "TimerScheduler.h"
#include "xTimer.h"


xTimer::xTimer(TimerScheduler* timerScheduler)
        :m_timerScheduler(timerScheduler)
{

}

xTimer::~xTimer()
{

}

void xTimer::StartTimer(int durationMs, std::function<void(xTimer&)> task, void* data)
{
    m_timerId = std::chrono::system_clock::now() + std::chrono::milliseconds(durationMs);
    m_fireTime = m_timerId;
    m_task = task;
    m_data = data;

    auto sptr = std::make_shared<xTimer>(*this);
    m_timerScheduler->insert(sptr);
}
