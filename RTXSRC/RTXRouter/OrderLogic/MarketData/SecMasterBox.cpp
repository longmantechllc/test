//
// Created by schen on 2/19/2021.
//
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>
#include <fstream>
#include <functional>
#include <string.h>

#include "RTXUtils.h"
#include "Config/ConfigGlobalBox.h"
#include "Config/Config.h"
#include "../include/App.h"
#include "Logger.h"

#include "SecMasterBox.h"

SecMasterBox::SecMasterBox(App* app)
:m_config(app->m_config),
 m_logger(app->m_loggerSys)
{
}

SecMasterBox::~SecMasterBox()
{

}

void SecMasterBox::InitSecMasterBox()
{
    m_path = m_config->GlobalConfig()->SecurityMaster()->path;
    LoadSecMasterBox();
}

void SecMasterBox::LoadSecMasterBox()
{
    m_logger->logInfo("SecMaster file path: %s", m_path.c_str());

    m_stream.open(m_path.c_str(), std::ios_base::in);
    if (!m_stream.is_open())
    {
        std::string msg = "Error opening SecMaster file " + m_path;
        std::cout << msg << std::endl;
        m_logger->logErr("%s", msg.c_str());
        return;
    }
    int cnt = 0;
    string lineBuf;
    while(m_stream.good())
    {
        getline(m_stream, lineBuf);
        if(ProcessData(lineBuf))
            ++cnt;
    }
    m_stream.close();
    m_logger->logInfo("SecMaster: %d line loaded.", cnt);

//    for(auto itr = m_secMasterDataList.begin(); itr != m_secMasterDataList.end(); ++itr)
//        printf("%s: symbol:%s, secId:%s, lotSize:%d, ipo:%s\n", itr->first.c_str(), itr->second->symbol.c_str(), itr->second->exchangeId.c_str(), itr->second->lotSize, itr->second->isIPO.c_str());
}

bool SecMasterBox::ProcessData(const std::string& line)
{
    char list[512];
    strcpy(list, line.c_str());

    int cnt = 0;
    std::string symbl;
    std::string secId;
    int lotSize = 100;
    std::string isIPO;
    bool processFail = false;

    char *token = strtok(list, ",");
    while (token != nullptr)
    {
        ++cnt;
        std::string tk = token;
        RTXUtils::trimString(tk);
//        std::cout << tk << std::endl;
        if(cnt == 1)
            symbl = tk;
        else if(cnt == 2) {
            secId = tk;
            if(secId.size() != 1) {
                processFail = true;
                break;
            }
        }
        else  if(cnt == 3) {
            lotSize = RTXUtils::StrToInt(tk, 100);
        }
        else if(cnt==9)
        {
            isIPO = tk;
            if(isIPO != "Y" && isIPO != "y" )
                isIPO = "";
        }
//        std::cout << "tk:" << tk  << ", cnt:" << cnt <<std::endl;
        token = strtok(nullptr, ",");
    }
    if(processFail == true) {
        m_logger->logInfo("SecMaster fail to load line: %s", line.c_str());
        return false;
    }
    auto data = std::make_shared<SecMasterData>(symbl, secId, isIPO, lotSize);
    auto pr = std::make_pair(symbl, data);
    m_secMasterDataList.insert(pr);
//    std::cout << "m_secMasterDataList.size():" << m_secMasterDataList.size() << std::endl << std::endl;
    return true;
}

std::string SecMasterBox::GetExchangeName(const std::string& symbol)
{
    auto itr = m_secMasterDataList.find(symbol);
    if(itr != m_secMasterDataList.end())
    {
        std::string exchId = itr->second->exchangeId;
        auto secIds = m_config->GlobalConfig()->SecIdfnIdentifier();
        auto itr2 = secIds.find(exchId);
        if(itr2 != secIds.end()) {
            std::string exch = itr2->second;
            return exch;
        }
    }
    return "";
}

bool SecMasterBox::GetIPO(const std::string& symbol)
{
    bool isIpo = false;
    auto itr = m_secMasterDataList.find(symbol);
    if(itr != m_secMasterDataList.end()) {

        std::string ipoStr = itr->second->isIPO;
        if(ipoStr == "Y" || ipoStr == "y")
            isIpo = true;
    }
    return isIpo;
}
