//
// Created by schen on 10/6/2020.
//
#include <iostream>
#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>
#include <ctime>

#include "../include/Order.h"
#include "../include/ExecutionReport.h"
#include "Logger.h"
#include "RTXUtils.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "RTXOrder.h"
#include "IOrderLogicBase.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"

#include "PassivePosting.h"


PassivePosting::PassivePosting(RTXOrder* order)
        : IOrderLogicBase { order }
{
}

PassivePosting::~PassivePosting()
{
}

bool PassivePosting::InitOrder()
{
    if(m_isInited == true)
        return true;
    m_postRoutes = m_order->m_exchange->posting->postRoutes;
    m_isInited = true;
    return true;
}

bool PassivePosting::ProcessOrder()
{
//    if(CancelPostedChildOrder() == true)
//        return true;
    if(!IsEligible())
        return false;

    std::string routeName;
    if(m_order->m_marketDataFailureTimeOutPost == true) {
        if (m_marketDataFailureTimeOutPostRoutes.empty())
            m_marketDataFailureTimeOutPostRoutes = m_order->m_config->GlobalConfig()->MarketDataFailureRoutes();
        if(GetPostRoute(routeName, m_marketDataFailureTimeOutPostRoutes) == false)
            return false;
    }
    else {
        if(GetPostRoute(routeName, m_postRoutes)== false)
            return false;
    }
    if(routeName.empty())
    {
        m_order->CancelParent("No eligible posting route available. Cannot processing posting.");
//        m_order->Cleanup();
        return false;
    }

    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    auto cOrder = m_order->CreateChildOrder();
    cOrder->size_ = m_order->QtyUncommited();
    cOrder->TIF_ = route->tif;
    cOrder->exchange_ = route->venue;
    cOrder->routeName_ = routeName;

    char msg[256];
    if(m_order->m_tobList.GetTobList().empty())
    {
        if(m_order->m_firstTob)
            sprintf(msg, "Tob quote fail, post.");
        else
            sprintf(msg, "No Tob quote, post.");
    }
    else if(m_order->m_tobList.GetBestQuotePrice().isLE(Price(0.0)))
        sprintf(msg, "No targetable Tob quote, post.");
    else
        sprintf(msg, "limit px: %.2f, best Tob %.2f, Post", m_order->m_clientOrder->price_.ToDouble(), m_order->m_tobList.GetBestQuotePrice().ToDouble());

    m_order->m_logger->logInfo("oid:%d, %s", m_order->m_clientOrder->orderID_, msg);
    SendChildOrder(cOrder);
    m_postedChild.insert(cOrder->childOrderId_);
    return true;
}

bool PassivePosting::SendChildOrder(std::shared_ptr<Order> order)
{
    m_order->SendChildOrder(order, ChildOrderType::CHILD_ORDER_TYPE_POSTING);
    return false;
}

bool PassivePosting::ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child)
{
    if(child->m_childOrderType != ChildOrderType::CHILD_ORDER_TYPE_POSTING)
        return false;

    bool isRemoved = true;
    if(m_cancelPosting == true) {
        m_cancelPosting = false;
    }
    else {
        if (er->status_ == ERSTATUS::CANCELED || er->status_ == ERSTATUS::REJECTED)
        {
            std::string routeName = child->m_order->routeName_;
            if(m_order->m_marketDataFailureTimeOutPost == true)
                isRemoved = RemovePostRoute(routeName, m_marketDataFailureTimeOutPostRoutes);
            else
                isRemoved = RemovePostRoute(routeName, m_postRoutes);
        }
    }
    switch (er->status_)
    {
        case ERSTATUS::FILLED:
        case ERSTATUS::CANCELED:
        case ERSTATUS::REJECTED:
            m_postedChild.erase(er->childOrderId_);
            break;
        default:
            break;
    }
    if(isRemoved == false)
        return true;
    return false;
}

bool PassivePosting::ProcessMktData()
{
    return true;
}

bool PassivePosting::IsRouteEligible(const std::string& routeName)
{
    auto route = m_order->m_config->RouterConfig()->Route(routeName);
    if(!route)
        return false;
    if(m_order->ExcludedRoute(routeName) == true)
        return false;
    if(route->InOperationTime(RTXUtils::NowSsm()) == false)
        return false;
    if(!m_order->m_orderIO->IsDestinationAvailable(route->venue))
        return false;

    return true;
}

bool PassivePosting::IsEligible()
{
    if(m_order->m_orderIsCancelling)
        return false;
    if(!m_order->m_isRetailSWPDone)
        return false;
    if(m_order->m_isRetailRouting == true)
        return false;
    if(m_order->m_isSWPDone == false)
        return false;
    if(m_order->QtyUncommited() <= 0)
        return false;
    if(m_order->m_isMarketable == true)
        return false;
    ////waiting for all targeting child order came back
    if(m_order->m_isMarketable == false && m_order->QtyPending() > 0)
        return false;
    if(m_order->m_clientOrder->TIF_ == TimeInForce::TIF_IOC) {
        m_order->CancelParent("IOC order cannot find inside quote.");
        return false;
    }
    if(m_order->m_config->GlobalConfig()->UnsubscribeMDWhenPost()) {
        if(!m_order->m_marketDataUnsubscribed)
            m_order->m_logger->logInfo("oid:%d, Unsubscribe MD post.", m_order->m_clientOrder->orderID_);
        m_order->UnsubscribeMarketData();
    }
    return true;
}

bool PassivePosting::CancelPostedChildOrder()
{
    if(m_order->m_isMarketable == true && m_order->QtyPending() > 0 && !m_postedChild.empty())
    {
        m_cancelPosting = true;
        m_order->CancelAllChildOrders();
        return true;
    }
    return false;
}

bool PassivePosting::RemovePostRoute(const std::string& routeName, std::vector<std::string>& postRoutes)
{
    auto itr = postRoutes.begin();
    while (itr != postRoutes.end())
    {
        bool found = false;
        if(*itr == routeName)
        {
            itr = postRoutes.erase(itr);
            m_order->m_logger->logInfo("oid:%d, Posting route:%s is removed.", m_order->m_clientOrder->orderID_, routeName.c_str());
            found = true;
            break;
        }
        ++itr;
        if (found == true)
            break;
    }

    if (postRoutes.empty()) {
        m_order->CancelParent("No posting route available.");
        m_order->Cleanup();
        return false;
    }
    return true;
}

bool PassivePosting::GetPostRoute(std::string& routeName, std::vector<std::string>& postRoutes)
{
    if(postRoutes.empty())
    {
        m_order->CancelParent("No posting route available. Cannot processing posting.");
//        m_order->Cleanup();
        return false;
    }
    auto itr = postRoutes.begin();
    while (itr != postRoutes.end())
    {
        if(IsRouteEligible(*itr))
        {
            routeName = *itr;
            break;
        }
        else
        {
            m_order->m_logger->logInfo("oid:%d, Route:%s is not eligible at this time.", m_order->m_clientOrder->orderID_, (*itr).c_str());
            itr = postRoutes.erase(itr);
        }
    }
    return true;
}
