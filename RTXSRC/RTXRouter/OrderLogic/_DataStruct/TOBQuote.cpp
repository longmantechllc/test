//
// Created by schen on 12/28/2020.
//
#include <string>
#include <memory>
#include <vector>

#include "Price.h"

#include "TopOfBook.h"

#include "TOBQuote.h"

void TobQuote::AddToQuote(const TopOfBook& tob, Price px, const int qty, const int pri, bool pass)
{
    std::string subMmid = tob.m_mpid;
    m_px = px.value();
    if(pass == false)
    {
        m_mpid = tob.m_mpid;
        m_symbol = tob.m_symbol;
        m_qty = qty;
        m_afterConsumedQty = qty;
    }
    else
    {
        m_mpid = "XXXX";
        m_symbol = tob.m_symbol;
        auto qt = m_subQuoteList.find(subMmid);
        if(qt != m_subQuoteList.end())
        {
            m_qty -= qt->second->m_qty;
            m_subQuoteList.erase(qt);
        }
        m_qty += qty;
        m_afterConsumedQty = m_qty;
        auto tb = std::make_shared<Tob>(subMmid, px, qty, pri);
        auto pr = std::make_pair(subMmid, tb);
        m_subQuoteList.insert(pr);
    }
}

void TobQuote::SetTob(Price px, const int qty, const int pri)
{
    m_px = px;
    m_qty = qty;
    m_pri = pri;
    m_afterConsumedQty = m_qty;
}