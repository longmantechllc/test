#pragma once

#include <stdint.h>
#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include <cassert>
#include <memory>
#include <thread>
#include <atomic>

#include "quickfix/FileStore.h"
#include "quickfix/FileLog.h"
#include "quickfix/SocketAcceptor.h"
#include "quickfix/Log.h"
#include "quickfix/SessionSettings.h"
#include "quickfix/Session.h"
#include "quickfix/Application.h"
#include "quickfix/MessageCracker.h"
#include "quickfix/fix44/NewOrderSingle.h"
#include "quickfix/fix44/ExecutionReport.h"
#include "quickfix/fix44/OrderCancelRequest.h"
#include "quickfix/fix44/OrderCancelReplaceRequest.h"

#include "ApplicationProperties.h"

struct OSMOrder {
    FIX::Side side;
    FIX::OrderQty qty;
    FIX::TimeInForce tif;
    FIX::OrdType type;
    FIX::Price price;
    std::string symbol;
    std::string clOrdId;
};

/// Application implementation (processes the incoming messages).
class ExchangeSimulator : public FIX::Application, public FIX::MessageCracker
{
public:
    explicit ExchangeSimulator(const ApplicationProperties& params);
    ~ExchangeSimulator();

    // Application overloads
    void onCreate( const FIX::SessionID& ) override;
    void onLogon( const FIX::SessionID& sessionID ) override;
    void onLogout( const FIX::SessionID& sessionID ) override;
    void toAdmin( FIX::Message&, const FIX::SessionID& ) override;
    void toApp( FIX::Message&, const FIX::SessionID& )
        EXCEPT ( DoNotSend )  override;
    void fromAdmin( const FIX::Message&, const FIX::SessionID& )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon ) override;
    void fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType ) override;

    bool isInited() {return m_init;}
 
private: // Engine::Application contract implementation
    template<typename T>
    static auto getField(const auto& msg) {
        T field;
        msg.get(field);
        return field;
    };

    template<typename T>
    static auto getFieldValue(const auto& msg) {
        T field;
        msg.get(field);
        return field.getValue();
    };
    template<typename T>
    static auto getFieldValueIf(const auto& msg, auto& var) {
        T field;
        if (msg.isSetField(field)) {
            msg.get(field);
            var = field.getValue();
        }
    };

    void onMessage( const FIX44::NewOrderSingle& msg, const FIX::SessionID& ) override;
    void onMessage( const FIX44::OrderCancelRequest& msg, const FIX::SessionID& ) override;
    void onMessage( const FIX44::OrderCancelReplaceRequest& msg, const FIX::SessionID& ) override;

    const ApplicationProperties& params_;

    std::unique_ptr<FIX::SocketAcceptor> m_acceptor;
    FIX::SessionID session_;
    uint32_t m_orderId {}, m_execId{};
    std::unordered_map<std::string, OSMOrder> m_orders;
    bool m_init{};
};


