// <copyright>
//
// $Revision: 42192 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_LogDefines.h
/// Contains Utils::Log common types and variables

#ifdef _MSC_VER
#   pragma once
#endif // _MSC_VER

#ifndef __B2BITS_Utils_Log__LogDefines_h__
#define __B2BITS_Utils_Log__LogDefines_h__

#include <string>

/** Utility namespace */
namespace Utils
{

    /** Logging system namespace */
    namespace Log
    {

        static const std::string DEFAULT_CATEGORY( "" );

        /** Log levels */
        enum LEVELS {
            LS_NOTE     = 0,
            LS_DEBUG    = 1,
            LS_TRACE    = 2,
            LS_WARN     = 3,
            LS_ERROR    = 4,
            LS_FATAL    = 5,
            LS_CYCLE    = 6,
            LEVELS_LAST
        }; // enum LEVELS

        /** Levels */
        typedef int Levels;

        /** String, which will be used in logging for any level */
        static const std::string LEVELS_MARKS[ LEVELS_LAST ] = {
            "[INFO] ", //LS_NOTE
            "[DEBUG]",
            "[TRACE]",
            "[WARN] ",
            "[ERROR]",
            "[FATAL]",
            "[CYCLE]"
        };

        /** Managed devices names. WARNING: the order of names is important! */
        static const std::string DEVICES[] = {
            "Console",
            "File",
#ifdef _WIN32
            "EventLog",
            "WinDebug",
#else
            "Syslog",
#endif
            "Log4Cplus"
        };

        /** Name, which will be used for the device if it is not supported. */
        static std::string UNSUPPORTED_DEVICE( "Unsupported" );

    } // namespace Log

} // namespace Utils

#endif // __B2BITS_Utils_Log__LogDefines_h__
