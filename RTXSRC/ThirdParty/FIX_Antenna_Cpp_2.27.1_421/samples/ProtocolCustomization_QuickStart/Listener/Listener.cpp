/*
<copyright>

$Revision: 1.3 $

(c) B2BITS EPAM Systems Company 2000-2017.
"Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

This software is for the use of the paying client of B2BITS (which may be
a corporation, business area, business unit or single user) to whom it was
delivered (the "Licensee"). The use of this software is subject to
license terms.

The Licensee acknowledges and agrees that the Software and Documentation
(the "Confidential Information") is confidential and proprietary to
the Licensor and the Licensee hereby agrees to use the Confidential
Information only as permitted by the full license agreement between
the two parties, to maintain the confidentiality of the Confidential
Information and not to disclose the confidential information, or any part
thereof, to any other person, firm or corporation. The Licensee
acknowledges that disclosure of the Confidential Information may give rise
to an irreparable injury to the Licensor in-adequately compensable in
damages. Accordingly the Licensor may seek (without the posting of any
bond or other security) injunctive relief against the breach of the forgoing
undertaking of confidentiality and non-disclosure, in addition to any other
legal remedies which may be available, and the licensee consents to the
obtaining of such injunctive relief. All of the undertakings and
obligations relating to confidentiality and non-disclosure, whether
contained in this section or elsewhere in this agreement, shall survive
the termination or expiration of this agreement for a period of five (5)
years.

The Licensor agrees that any information or data received from the Licensee
in connection with the performance of the support agreement relating to this
software shall be confidential, will be used only in connection with the
performance of the Licensor's obligations hereunder, and will not be
disclosed to third parties, including contractors, without the Licensor's
express permission in writing.

Information regarding the software may be provided to the Licensee's outside
auditors and attorneys only to the extent required by their respective
functions.

</copyright>
*/

#include <iostream>
#include <cassert>
#include <cstddef>

#include <B2BITS_V12.h>

#include "FIXApp.h"
#include "SessionConnector.h"

using namespace std;

class AcceptorApplication: public FIXApp
{
private:
    SessionConnector* connector_;

public:
    AcceptorApplication( SessionConnector* connector )
        : connector_( connector ) {
        assert( NULL != connector );
    }
private:
    virtual void onLogoutEvent( const Engine::LogoutEvent* event, const Engine::Session& sn ) {
        FIXApp::onLogoutEvent( event, sn );

        // Schedule session to accept incoming connections
        connector_->connectSession( const_cast<Engine::Session*>( &sn ) );
    }
};



int main()
{
    cout << "starting FIX Listener..." << endl;

    try {
        // Initializes engine.
        // If error appears during initialization (properties file is not found, required property is missing, etc.) then exception is thrown.
        Engine::FixEngine::init( "listener.engine.properties" );

        SessionConnector connector;

        // Create Application instance
        AcceptorApplication application( &connector );

        cout << "starting FIX Listener session..." << endl;

        // Create FIX session instance
        Engine::SessionExtraParameters params;
        params.intradayLogoutToleranceMode_ = true;
        Engine::Session* pSaFIX43 =  Engine::FixEngine::singleton()->createSession( &application, "SenderCompID_FIX43", "TargetCompID_FIX43", Engine::FIX43, &params );
        // Connect session as acceptor
        pSaFIX43->connect();

        Engine::FixEngine::singleton()->loadProtocolFile( "custom_fix44.xml" );
        Engine::ParserID pid = Engine::FixEngine::singleton()->createFixParser( "FIX44_MY" );
        Engine::Session* pSaFIX44 = Engine::FixEngine::singleton()->createSession( &application, "SenderCompID_FIX44", "TargetCompID_FIX44", pid, &params );
        // Connect session as acceptor
        pSaFIX44->connect();

        cout << "Session has been created. Listening for incoming connection." << endl;

        string cmd;
        while( "exit" != cmd ) {
            cout << "Type 'exit' to exit > ";
            getline( cin, cmd );
        }
		
		pSaFIX44->disconnectNonGracefully();
		pSaFIX44->registerApplication(NULL);
		pSaFIX44->release();

		pSaFIX43->disconnectNonGracefully();
		pSaFIX43->registerApplication(NULL);
		pSaFIX43->release();

        connector.stop();
        connector.join();
    } catch( const std::exception& ex ) {
        cout << "Error: " << ex.what() << endl;
    } catch ( ... ) {
        cout << "Unknown error." << endl;
    }

    Engine::FixEngine::destroy();

    return 0;
}

