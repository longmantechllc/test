//
// Created by schen on 10/30/2020.
//
#include <string>
#include <memory>
#include <iostream>
#include <unordered_map>
#include <functional>
#include <ctime>
#include <unordered_set>
#include <vector>

#include <xercesc/dom/DOM.hpp>
#include "auto_xerces_ptr.h"

#include "RTXUtils.h"
#include "Logger.h"
#include "LoaderBase.h"
#include "ConfigRouterBox.h"
#include "ConfigRouterLoader.h"
#include "ConfigGlobalLoader.h"
#include "ConfigSecIdfnsLoader.h"
#include "ConfigUserLoader.h"

#include "Config.h"
#include "ConfigLoader.h"

ConfigRouterLoader::ConfigRouterLoader()
{
}

ConfigRouterLoader::ConfigRouterLoader(Logger* logger)
        :LoaderBase(logger)
{
}

ConfigRouterLoader::~ConfigRouterLoader()
{
}

void ConfigRouterLoader::InitConfigRouterLoader(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
}

void ConfigRouterLoader::ProcessLoading(ConfigLoader* loader, xercesc_3_2::DOMNode* node)
{
    InitConfigRouterLoader(loader, node);

    xercesc_3_2::DOMNodeList* nodeList1 = node->getChildNodes();
    for(int j=0; j!=nodeList1->getLength(); j++)
    {
        xercesc_3_2::DOMNode* subNode = nodeList1->item(j) ;
        char* nodeName = xercesc_3_2::XMLString::transcode(subNode->getNodeName()) ;
        if(strcmp(nodeName, "#text")==0 || strcmp(nodeName, "#comment")==0)
        {
            xercesc_3_2::XMLString::release(&nodeName) ;
            continue;
        }
        auto obj = std::make_shared<RouteStruct>();

        std::string name = GetElement(subNode, "name") ;
        obj->name = name;
        std::string venue = GetElement(subNode, "venue") ;
        obj->venue = venue;
        std::string tif = GetElement(subNode, "tif") ;
        obj->tif = RTXUtils::StrToInt(tif, 3);
        std::string swpMinimumSize = GetElement(subNode, "swpMinimumSize") ;
        obj->swpMinimumSize = RTXUtils::StrToInt(swpMinimumSize, 1);
        std::string midpointEligibility = GetElement(subNode, "midpointEligibility") ;
        obj->midpointEligibility = RTXUtils::StrToBool(midpointEligibility, false);
        std::string multiPounce = GetElement(subNode, "multiPounce") ;
        obj->multiPounce = RTXUtils::StrToBool(multiPounce, false);
        std::string maxNumberOfTry = GetElement(subNode, "maxNumberOfTry") ;
        obj->maxNumberOfTry = RTXUtils::StrToInt(maxNumberOfTry, 1);
        std::string oddLot = GetElement(subNode, "oddLot") ;
        obj->oddLot = RTXUtils::StrToBool(oddLot, true);
        std::string startTime = GetElement(subNode, "startTime") ;
        obj->startTimeStr = startTime;
        std::string endTime = GetElement(subNode, "endTime") ;
        obj->endTimeStr = endTime;
        std::string priority = GetElement(subNode, "priority") ;
        obj->priority = RTXUtils::StrToInt(priority, 1);
        std::string quoteConsumed = GetElement(subNode, "quoteConsumed") ;
        obj->quoteConsumed = RTXUtils::StrToBool(quoteConsumed, false);
        std::string aggressiveOffset = GetElement(subNode, "aggressiveOffset") ;
        obj->aggressiveOffset = RTXUtils::StrToDouble(aggressiveOffset, 0.0);

        loader->m_config->RouterConfig()->SetRoute(name, obj);
        loader->m_config->RouterConfig()->SetVenueToRoute(venue, name);
        xercesc_3_2::XMLString::release(&nodeName) ;
    }
//    PrintRoutess(loader);     //for debug
}

void ConfigRouterLoader::PrintRoutess(ConfigLoader* loader)
{
    std::cout << std::endl << "Print Routes" << std::endl;

    for(auto& id : loader->m_config->RouterConfig()->Routes() )
    {
        std::cout << "RouterConfig :value: " << id.first << "\n";
        std::cout << "\t-- venue:" << id.second->venue << std::endl;

        std::cout << "\t tif:" << id.second->tif << std::endl
                  << "\t swpMinimumSize:" << id.second->swpMinimumSize << std::endl
                  //<< "\t checkNBBO:" << boolalpha << id.second->checkNBBO << std::endl
                  << "\t midpointEligibility:" << boolalpha << id.second->midpointEligibility << std::endl
                  << "\t multiPounce:" << boolalpha << id.second->multiPounce << std::endl
                  << "\t maxNumberOfTry:" << id.second->maxNumberOfTry << std::endl
                  << "\t oddLot:" << boolalpha << id.second->oddLot << std::endl
                  << "\t startTimeStr:" << id.second->startTimeStr << std::endl
                  << "\t startTime:" << id.second->startTimeSsm << std::endl
                  << "\t endTimeStr:" << id.second->endTimeStr << std::endl
                  << "\t endTime:" << id.second->endTimeSsm << std::endl
                  << "\t priority:" << id.second->priority << std::endl
                  << "\t quoteConsumed:" << boolalpha << id.second->quoteConsumed << std::endl
                  << "\t aggressiveOffset:" << id.second->aggressiveOffset.ToDouble() << std::endl << std::endl;
    }
    std::cout << "Venue --> Routes" << std::endl;
    for(auto& id : loader->m_config->RouterConfig()->VenueToRouteList() )
    {
        std::cout << "\t-- venue:" << id.first << " -- route: ";
        for(auto& rt :  loader->m_config->RouterConfig()->FindRoutesFromVenue(id.first) )
            std::cout << " " << rt;
        std::cout << std::endl;
    }
    if(loader->m_config->RouterConfig()->FindRoutesFromVenue("XYZ").empty())
        std::cout << "venue 'XYZ' empty!" << std::endl;

    std::cout << std::endl;
}