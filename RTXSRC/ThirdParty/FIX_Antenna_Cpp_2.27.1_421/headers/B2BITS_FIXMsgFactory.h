// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXMsgFactory.h
/// Contains Engine::FIXMsgFactory class declaration.

#ifndef _B2BITS_FIXMsgFactory__h_
#define _B2BITS_FIXMsgFactory__h_

#include "B2BITS_V12_Defines.h"
#include "B2BITS_FIXMessage.h"


namespace Engine
{
    class FixMsg;

    /**
     * Creates FIXMessages.
     */
    class V12_API FIXMsgFactory
    {
    public:

        /**
         * Returns the instance of the class.
         */
        static FIXMsgFactory* singleton();

        /**
         * Creates the new skeleton of the application message and returns it.
         *
         * @param ver Version of FIX protocol to use.
         * @param msgType Null-terminated std::string. Specifies type of new message.
         *
         * @warning the user is responsible for the deletion.
         * @warning Method throws Utils::Exception in case message or protocol is not defined.
         */
        virtual FIXMessage* newSkel( FIXVersion ver, const char* msgType, FIXVersion ssnver = NA ) const = 0;

        /**
         * Creates the new skeleton of the application message and returns it.
         *
         * @param parserID Unique identifier of the FIX parser customization to use.
         * @param msgType Null-terminated std::string. Specifies type of new message.
         * @param ver Application FIX protocol version. Required if parserID is FIXT11 dialect.
         *
         * @warning the user is responsible for the deletion.
         * @warning Method throws Utils::Exception in case message or protocol is not defined.
         */
        virtual FIXMessage* newSkel( ParserID parserID, const char* msgType, FIXVersion ver = NA ) const = 0;

        /**
         * Creates the new skeleton of the Logon message and returns it.
         *
         * @param ver Version of FIX protocol to use.
         *
         * @warning the user is responsible for the deletion.
         * @warning Method throws Utils::Exception in case protocol is not defined.
         */
        virtual FIXMessage* newLogonSkel( FIXVersion ver ) const = 0;

    protected:
        /** Constructor */
        FIXMsgFactory();

        /** Destructor. */
        virtual ~FIXMsgFactory();
    };
} // namespace Engine{

#endif // #ifndef _FIXMsgFactory__h_

