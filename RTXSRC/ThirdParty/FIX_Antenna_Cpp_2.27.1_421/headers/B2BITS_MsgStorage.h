// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_MsgStorage.h
/// Contains Engine::MsgStorage class definition

#ifndef _B2BITS_MSG_STORAGE__H_
#define _B2BITS_MSG_STORAGE__H_ 1

#include <vector>
#include <string>
#include <deque>

#include "B2BITS_PubEngineDefines.h"
#include "B2BITS_FixFieldsContainer.h"
#include "B2BITS_IntDefines.h"

namespace Engine
{
    class FIXMessage;

    /** Abstract storage of the rejected messages */
    class RejectMsgStorage
    {
    public:
        /** Mask message as rejected. Return true when message marked successfully
        * @param msg - rejected message
        */
        virtual bool markRejected( const std::string& msg ) = 0;

        /** Unask rejected message and replace it by another one. Return true when message replaced successfully
        * @param msg - rejected message
        * @param newMsg - message that replace rejected message
        */
        virtual bool unmarkRejected( const std::string& msg, const std::string& newMsg ) = 0;

        /** Return true when storage contains rejected messages */
        virtual bool hasRejectedMessage() = 0;

        /** Retrieves all rejected messages from storage
        * @param pMessages - container that will contains rejected messages
        */
        virtual void retrieveRejectedMessages( std::vector<std::string>* pMessages ) = 0;

        /** Destructor */
        virtual ~RejectMsgStorage() {}
    };

    /**
    * Defines contract of the message to be stored in storage
    */
    class StorableMsg
    {
    public:
        /**
        * Cleans resources
        */
        virtual void release() const = 0;
        /**
        * Returns isOriginal flag
        */
        virtual bool isOriginal() const = 0;
        /**
        * Returns true if messages could be resent.
        */
        virtual bool toBeResent() const throw () = 0;
        /**
        * Returns message number
        */
        virtual int getSeqNum() const = 0;
    protected:
        /*
        Destructor
        */
        virtual ~StorableMsg() {}
    };


    /**
    * Defines contract of the remote message to be stored in storage
    */
    class StorableMsgRemote
    {
    public:
        /**
        * Returns true if message from temporary queue
        */
        virtual bool isFromQueue() const throw () = 0;
    protected:
        /** Destructor */
        virtual ~StorableMsgRemote() {}
    };


    class CacheHolderBase
    {
    public:
        virtual ~CacheHolderBase() {}
    };

    /**
     * Exceptions of this class is thrown by message storages to indicate the storage
     * specific failure.  No other exceptions shall be thrown by the storages.
     */
    class V12_API MessageStorageException : public Utils::Exception
    {
    public:
	explicit MessageStorageException(const std::string& msg)
	    : Utils::Exception(msg)
	{ }
    };

    /**
    * Basic message storage.
    */
    class V12_API MsgStorage
    {
    public:
        struct RestoreOptions {
            bool restoreEnabled_;
            bool rejectFlagSupportRequired_;
        };

    public:
        /**
        * Defines type of the storage. Either primary or backup.
        */
        typedef enum {
            PRIMARY_STORAGE = 0,
            BACKUP_STORAGE
        } StorageType;

        /**
        * Constructor.
        */
        MsgStorage( StorageType type );
        /**
        * Destructor.
        */
        virtual ~MsgStorage();

        /**
        * Clears the storage.
        */
        virtual void clear();

        /**
        * Stores the given outgoing message and then destroys it.
        * @return false if the message with such sequence number already exists
        * in the storage; otherwise true.
        * @param apMsg - storage message
        * @param nSeqNum - message number
        * @param aRawMsg - message buffer
        */
        virtual bool logLocal( StorableMsg const* apMsg, int nSeqNum, const std::string& aRawMsg ) = 0;

        /**
        * Stores the given outgoing message and then destroys it.
        * @return false if the message with such sequence number already exists
        * in the storage; otherwise true.
        * @param apMsg - storage message
        * @param nSeqNum - message number
        * @param pRawMsg - message buffer pointer
        * @param size - message buffer size
        */
        virtual bool logLocal( StorableMsg const* apMsg, int nSeqNum, const char* pRawMsg, int size ) = 0;

        /**
        * Logs the message from the remote FIX engine.
        * @param msgSeqNum - message number
        * param rawMsg - message buffer
        */
        virtual void logRemote( int msgSeqNum, const std::string& rawMsg ) = 0;

        /**
        * Logs the message from the remote FIX engine.
        * @param msgSeqNum - message number
        * @param pRawMsg - message buffer pointer
        * @param size - message buffer size
        */
        virtual void logRemote( int msgSeqNum, const char* pRawMsg, int size ) = 0;

        /**
        * Logs the message from the remote FIX engine.
        * @param apMsg - storage message
        * @param msgSeqNum - message number
        * @param pRawMsg - message buffer pointer
        * @param size - message buffer size
        */
        virtual void logRemote( StorableMsgRemote const* apMsg, int msgSeqNum,  const char* pRawMsg, int size )
          { 
              B2B_USE_ARG( apMsg ); //unreferenced parameter
              logRemote( msgSeqNum , pRawMsg , size ); 
          };


        typedef std::vector<StorableMsg*> FixMsgs;

        /**
        * Prepare for cache message walk process like a series of retrieveLocal() calls
        * returns an object whose lifetime is bound to the whole series of retrieveLocal() calls
        * when retrieveLocal() calls are complete, the object should be destroyed by the caller.
        * Returned pointer can have NULL value. To control the lifetime, should be used in conjunction with std::auto_ptr<>
        * The implementaion would typically load the messages from an external storage to memory to prepare for a series of retrieveLocal() calls
        */
        virtual CacheHolderBase* prepareLocalRetriving( int beginSeqNum, int endSeqNum, int currentOutSeqNum ) = 0;

        /**
        *Locates sent messages
        *@param cacheHolder - cache holder
        *@param parserID - parser ID
        *@param[out] apMsgs - fix message list
        *@param aBeginSeqNum - begin message number
        *@param aEndSeqNum - end message number
        *@param FIXVersion - FIX protocol version
        */
        virtual bool retrieveLocal( CacheHolderBase* cacheHolder, ParserID parserID, std::vector<FIXMessage*>* apMsgs, int aBeginSeqNum,
                                    int aEndSeqNum = 0, FIXVersion preferredVersion = NA ) = 0;

        /** Locates sent message (with seqNum=outSeqNum) and returns it into msg
        * @return true if message was found, false otherwize
        * @param[out] msg - result message
        */
        virtual bool retrieveLocal( int outSeqNum, std::string* msg, int currentOutSeqNum ) = 0;

        /**
        * Fills input list with messages that satisfy selection criteria.
        * @param parserID - unique parser identifier
        * @param pMessages - result messages list
        * @param msgTypes - message types filter
        * @param fieldFilter - message fields filter
        * @param beginSeqNum - begin message number
        * @param endSeqNum - end message number
        * @param fixVersion - FIX protocol version
        */
        virtual void retrieveRemote( ParserID parserID, std::vector<FIXMessage*>* pMessages, const MsgTypes& msgTypes,
                                     const std::vector<FixField>& fieldFilter, int beginSeqNum, int endSeqNum = 0,
                                     FIXVersion fixVersion = NA ) = 0;

        /**
        * Closes the storage.
        */
        virtual void markAsTerminated( bool correctlyTerminated ) = 0;

        /**
        * Returns the maximum message's sequence number in the storage.
        */
        virtual int getMaxSeqNum() = 0;

        /**
        * Returns the minimum message's sequence number in the storage.
        */
        virtual int getMinSeqNum() = 0;

        /**
        * Sets the configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter value
        */
        virtual void setConfigInfo( const std::string& aKey, const std::string& aValue ) = 0;
        /**
        * Retrieves configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter result value
        * @return true if parameter is enabled
        */
        virtual bool getConfigInfo( const std::string& aKey, std::string* aValue ) const = 0;

        /**
        * Sets the configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter value
        */
        virtual void setIntConfigInfo( const std::string& aKey, int aValue ) = 0;
        /**
        * Retrieves configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter result value
        * @return true if parameter is enabled
        */
        virtual bool getIntConfigInfo( const std::string& aKey, int* aValue ) const = 0;
        /**
        * Sets the configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter value
        */
        virtual void setBoolConfigInfo( const std::string& aKey, bool aValue ) = 0;
        /**
        * Retrieves configuration information.
        * @param aKey - parameter key
        * @param aValue - parameter result value
        * @return true if parameter is enabled
        */
        virtual bool getBoolConfigInfo( const std::string& aKey, bool* aValue ) const = 0;

        /** Returns path to the storage without extension
        * @return path to the storage without extension
        * */
        virtual std::string const* getBaseFilePath() const = 0;

        /**
        * Sets the maximum MsgSeqNum of received messages.
        * @param msgSeqNum - maximum MsgSeqNum value
        */
        virtual void setMaxRemoteSeqNum( int msgSeqNum ) = 0;

        /**
        * Sets the minimum MsgSeqNum of sended messages.
        * @param msgSeqNum - minimum MsgSeqNum value
        */
        virtual void setMinSeqNum( int msgSeqNum ) = 0;

        /**
        * Updates current MsgSeqNum of received messages.
        * @param msgSeqNum - MsgSeqNum value
        */
        virtual void setRemoteSeqNum( int msgSeqNum ) = 0;

        /**
        * Return true if storage is persistent
        */
        virtual bool isPersistentStorage() const = 0;

        /** Return interface to the rejected storage, could be NULL.  */
        virtual RejectMsgStorage* getRejectStorage();

        /** Returns type of the storage
        * @return current storage type
        * */
        StorageType currentConnectionType() const;


        typedef std::deque<std::string> OutgoingMessages;

        /**
        * Updates MsgSeqNum of outgoing messages.
        * @param value - LastSentSeqNum value
        */
        virtual void setLastSentSeqNum( int value ) = 0;

        /** 
        * Returns storage creation time as number of 
        * milliseconds sincec Janually 1, 1970
        */
        virtual System::u64 getCreationTime() = 0;

    protected:
        /**
        * Clears collection (delete all its elements).
        */
        void clearFixMsgs( FixMsgs& aMsgs );

        virtual bool rebuildIndex( const std::string& logPath, const std::string& indexPath, const RestoreOptions& options ) = 0;

        StorageType currentConnectionType_;
    };

}

#endif


