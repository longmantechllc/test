// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file CqgClient.cpp Contains CqgClient implementation
/// @author Dmytro Ovdiienko [dmytro_ovdiienko@epam.com]
/// @author Roman Sergeev [roman_sergeev@epam.com]
/// @date 2011-05-25

#include "CqgClient.h"

#include <iostream>
#include <cstring>
#include <map>
#include <string>
#include <cstddef>
#include <cstdio>

#include <B2BITS_FixEngine.h>
#include <B2BITS_Mutex.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXMessage.h>
#include <B2BITS_FIXGroup.h>
#include <B2BITS_CompilerDefines.h>

#include <B2BITS_CqgDefines.h>
#include <B2BITS_CqgInstrumentListener.h>
#include <B2BITS_CqgSecurityDefinitionListener.h>
#include <B2BITS_CqgMDApplicationListener.h>
#include <B2BITS_CqgMDApplication.h>
#include <B2BITS_CqgMDApplicationParams.h>

#include "RefCounter.h"
#include "MarketDataStorage.h"
#include "FixMessageInterpreter.h"
#include "SecurityDef.h"

#if _MSC_VER >= 1500
    // A keyword was used that is not in the C++ standard, for example, one of the override specifiers that also works under /clr
#   pragma warning(disable: 4481)
#endif

#ifdef _WIN32
#   define SNPRINTF sprintf_s
#else
#   define SNPRINTF snprintf
#endif // _WIN32

extern System::Mutex g_mutex;   /// Used to sync output to console.
System::Mutex GLOBAL_MTX;


typedef std::map<std::string, SecurityDef > SecuritiesBySecDescMap;

template< typename Functor, typename Context >
static void split( std::string const& str, char const* delimeter, Functor f, Context* context )
{
    size_t first = 0;
    while( first < str.size() ) {
        first = str.find_first_not_of( delimeter, first );
        if( std::string::npos == first ) {
            break;
        }

        size_t last = str.find_first_of( delimeter, first );
        if( std::string::npos == last ) {
            last = str.size();
        }

        f( str, first, last - first, context );
        first = last;
    }
}

static void vectorPushBack( std::string const& str, size_t start, size_t size, std::vector<std::string>* dest )
{
    dest->push_back( str.substr( start, size ) );
}

void trimWhiteSpace( std::string* apStr )
{
    std::string::size_type pos = 0;
    if( ( pos = apStr->find_last_not_of( " \t" ) ) != std::string::npos ) {
        apStr->resize( pos + 1 );
    } else {
        apStr->resize( 0 );
    }
    pos = apStr->find_first_not_of( " \t" );
    if( pos != 0 && pos != std::string::npos ) {
        apStr->erase( 0, pos );
    }
}
class MGuard
{
private:
    System::Mutex& lock_;

public:
    MGuard( System::Mutex& lock )
        : lock_( lock ) {
        lock_.lock();
    }

    ~MGuard() {
        lock_.unlock();
    }
private:
    MGuard( MGuard const& );
    MGuard& operator=( MGuard const& );
};

//------------------------------------------------------------
//
// BookHandler
//
//------------------------------------------------------------
class BookHandler : public Cqg::InstrumentListener
{
public: // nested types
    typedef RefCounter<BookHandler> Ref;

private: // fields
    mutable System::Mutex mdLock_;
    MarketDataStorage md_;
    SecuritiesBySecDescMap const* securities_;

public: // methods
    BookHandler( Cqg::ASecurityDescription secDesc, SecuritiesBySecDescMap const* securities )
        : md_( secDesc )
        , securities_( securities ) {
    }

    void print() const {
        // make copy of the book
        mdLock_.lock();
        MarketDataStorage md( md_ );
        mdLock_.unlock();
        double df = md.displayFactor_;

        std::cout << "=========================================================" << std::endl;
        std::cout << md.SecurityDescription_ << std::endl;
        std::cout << "=========================================================" << std::endl;
        std::cout << "SecurityID: " << md.securityID_ << std::endl;
        std::cout << "SecurityDescriptionription: " << md.SecurityDescription_ << std::endl;
        std::cout << "- Trade Information -------------------------------------" << std::endl;
        std::cout << "Last trade: " << md.tradeInformation_.trade_.size_ << "@" << md.tradeInformation_.trade_.price_ * df << std::endl;
        std::cout << "MinTradePrice: " << md.tradeInformation_.trade_.minPrice_ * df << std::endl;
        std::cout << "MaxTradePrice: " << md.tradeInformation_.trade_.maxPrice_ * df << std::endl;
        std::cout << "Trade count: " << md.tradeInformation_.trade_.count_ << std::endl;

        std::cout << "TradingSessionHighPrice: " << md.tradeInformation_.tradingSessionHighPrice_ * df << std::endl;
        std::cout << "TradingSessionLowPrice: " << md.tradeInformation_.tradingSessionLowPrice_ * df << std::endl;
        std::cout << "OpeningPrice: " << md.tradeInformation_.openingPrice_ * df << std::endl;
        std::cout << "PrevClosingPrice: " << md.tradeInformation_.prevClosingPrice_ * df << std::endl;
        std::cout << "TheoreticalClosingPrice: " << md.tradeInformation_.theoreticalClosingPrice_ * df << std::endl;
        std::cout << "SettlementPrice: " << md.tradeInformation_.settlementPrice_ * df << std::endl;
        std::cout << "Prior: " << md.tradeInformation_.prior_ * df << std::endl;
        std::cout << "TotalTradeVolume: " << md.tradeInformation_.totalTradeVolume_ << std::endl;
        std::cout << "TotalTradeVolumeCount: " << md.tradeInformation_.totalTradeVolumeCount_ << std::endl;
        std::cout << "Status: " << md.status_ << std::endl;
        std::cout << "OpenInterest: " << md.tradeInformation_.openInterest_ << std::endl;

        if( 0 != md.book_.maxSize_ ) {
            std::cout << "- Book --------------------------------------------------" << std::endl;
            printBook( md.book_, df );
        }

        if( 0 != md.impliedBook_.maxSize_ ) {
            std::cout << "- Implied Book ------------------------------------------" << std::endl;
            printBook( md.impliedBook_, df );
        }
        std::cout << "=========================================================" << std::endl;
    }

    void setStatus( int status );
    void updateStatus( Engine::AsciiString symbol, int status );

private:
    void printBook( MarketDataStorage::Book const& book, double displayFactor ) const {
        char buf[200];
        std::cout << "--+-------------------------+-------------------------" << std::endl;
        std::cout << "  |           Bid           |           Ask           " << std::endl;
        std::cout << " #+------------+------------+------------+------------" << std::endl;
        std::cout << "  |     Qty    |    Px      |     Px     |    Qty     " << std::endl;
        std::cout << "--+------------+------------+------------+------------" << std::endl;

        size_t maxSize = book.ask_.bookSize_ > book.bid_.bookSize_ ? book.ask_.bookSize_ : book.bid_.bookSize_;
        for( size_t i = 0; i < maxSize; ++i ) {
            double priceBid = 0.0;
            double sizeBid = 0.0;

            double priceAsk = 0.0;
            double sizeAsk = 0.0;

            if( i < book.bid_.bookSize_ ) {
                priceBid = book.bid_.prices_[ i ].price_;
                sizeBid = book.bid_.prices_[ i ].size_;
            }

            if( i < book.ask_.bookSize_ ) {
                priceAsk = book.ask_.prices_[ i ].price_;
                sizeAsk = book.ask_.prices_[ i ].size_;
            }

            int size = SNPRINTF( buf, sizeof( buf ), "%-2d %12.4f %12.4f %12.4f %12.4f",
                                 static_cast<int>( i + 1 ),
                                 sizeBid,
                                 priceBid * displayFactor,
                                 priceAsk * displayFactor,
                                 sizeAsk
                               );
            std::cout << std::string( buf, size ) << std::endl;
        }
        std::cout << "--+-------------------------+-------------------------" << std::endl;
    }

private: // Cqg::MDApplicationListene contract
    virtual void onSubscribed( Cqg::ASecurityDescription secDesc,
                               Cqg::ASecurityID secID ) B2B_OVERRIDE;

    /// Faired when successfuly unsubscribed from symbol
    virtual void onUnsubscribed( Cqg::ASecurityDescription secDesc,
                                 Cqg::ASecurityID  secID ) B2B_OVERRIDE;

    /// Faired when information regard to instrument received
    virtual void process( Cqg::ASecurityDescription secDesc, 
                          Cqg::ASecurityID secID, 
                          Engine::FIXMessage const& msg ) B2B_OVERRIDE;

    /// Faired when user should reset book with the bnew values.
    virtual bool onIncrement( Cqg::ASecurityDescription secDesc,
                              Cqg::ASecurityID secID,
                              Engine::FIXMessage const* const* msgs,
                              size_t msgsCount ) B2B_OVERRIDE;

    /// Faired when user should reset book with the bnew values.
    void onSnapshot( Cqg::ASecurityDescription secDesc,
                             Cqg::ASecurityID  secID,
                             Engine::FIXMessage const& msg ) B2B_OVERRIDE;

    virtual void onReset( Cqg::ASecurityDescription secDesc, Cqg::ASecurityID secID ) B2B_OVERRIDE;

    virtual void onError( Cqg::ASecurityDescription secDesc,
                          Cqg::ASecurityID secID,
                          Engine::AsciiString const& error ) B2B_OVERRIDE;

    /// Faired when recovery is started
    virtual void onRecoveryStarted( Cqg::ASecurityDescription secDesc, 
                                    Cqg::ASecurityID secID, 
                                    Cqg::RecoveryReason reason ) B2B_OVERRIDE
    {
        B2B_USE_ARG( secDesc );
        B2B_USE_ARG( secID );
        B2B_USE_ARG( reason );
    }

    /// Faired when recovery is ended
    virtual void onRecoveryStopped( Cqg::ASecurityDescription /*secDesc*/, 
                                    Cqg::ASecurityID /*secID*/, 
                                    Cqg::RecoveryReason /*reason*/ ) B2B_OVERRIDE
    {
    }

private:
    // Hide copy ctor
    BookHandler( BookHandler const& );
    // Hide assign operator
    BookHandler& operator=( BookHandler const& );
};

void BookHandler::onSubscribed( Cqg::ASecurityDescription secDesc, Cqg::ASecurityID /*secID*/ )
{
    std::string buf;
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Subscribed to " << secDesc << std::endl;
    g_mutex.unlock();

    // securities_ is thread safe here
    SecuritiesBySecDescMap::const_iterator i = securities_->find( secDesc );
    assert( i != securities_->end() );

    mdLock_.lock();
    md_.book_.maxSize_ = i->second.compositeDepth_;
    md_.impliedBook_.maxSize_ = i->second.impliedDepth_;
    md_.displayFactor_ = i->second.displayFactor_;
    md_.securityID_ = i->second.securityID_;
    md_.symbol_ = i->second.symbol_;
    mdLock_.unlock();
}

void BookHandler::onUnsubscribed( Cqg::ASecurityDescription secDesc, Cqg::ASecurityID /*secID*/ )
{
    std::string buf;
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Unsubscribed from " << secDesc << std::endl;
    g_mutex.unlock();
}

void BookHandler::setStatus( int status )
{
    mdLock_.lock();
    md_.status_ = status;
    mdLock_.unlock();
}

void BookHandler::updateStatus( Engine::AsciiString symbol, int status )
{
    MGuard lock( mdLock_ );

    if( md_.symbol_ == symbol ) {
        md_.status_ = status;
    }
}

void BookHandler::process( Cqg::ASecurityDescription secDesc, 
                          Cqg::ASecurityID secID, 
                          Engine::FIXMessage const& msg )
{
    B2B_USE_ARG( secID );
    B2B_USE_ARG( secDesc );

    std::string buf;
    g_mutex.lock();

    if( Engine::AsciiString::fromStr( "f" ) == msg.type() ) { // Security Status
        setStatus( msg.getAsInt( FIXFields::SecurityTradingStatus ) );
    } else if( Engine::AsciiString::fromStr( "B" ) == msg.type() ) { // News message
        std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": News: " << std::endl;

        // Log news text
        for( int i = 0, e = msg.getAsInt( FIXField::LinesOfText ); i < e; ++i ) {
            std::cerr << msg.getGroup( FIXField::LinesOfText )->getAsString( FIXField::Text, i ) << std::endl;
        }

        std::cerr << std::endl;
    } else if( Engine::AsciiString::fromStr( "0" ) == msg.type() ) { // Heartbeat message
        std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Heartbeat message." << std::endl;
        std::cerr << std::endl;
    } else { // Other messages
        std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Incoming message " << msg.type() << std::endl;
        std::cerr << std::endl;
    }
    g_mutex.unlock();
}

void BookHandler::onSnapshot( Cqg::ASecurityDescription secDesc,
                              Cqg::ASecurityID /*secID*/,
                              Engine::FIXMessage const& W_msg )
{
    std::string buf;
    mdLock_.lock();
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Snapshot for '" << secDesc << "' received" << std::endl;
    g_mutex.unlock();

    // Process snapshot here (check SDKFFCore.pdf and SDKFFMessageSpecs.pdf)
    FixMessageInterpreter::applySnapshot( W_msg, &md_ );
    mdLock_.unlock();
}

bool BookHandler::onIncrement( Cqg::ASecurityDescription secDesc,
                               Cqg::ASecurityID /*secID*/,
                               Engine::FIXMessage const* const* msgs,
                               size_t msgsCount )
{
    std::string buf;
    mdLock_.lock();
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": Increments for '" << secDesc << "' received. Messages count: " << msgsCount << std::endl;
    g_mutex.unlock();

    // Process increment update here (check SDKFFCore.pdf and SDKFFMessageSpecs.pdf)
    FixMessageInterpreter::applyIncrements( &msgs[0], msgsCount, &md_ );
    mdLock_.unlock();
    return true;
}

void BookHandler::onReset( Cqg::ASecurityDescription secDesc,
                           Cqg::ASecurityID /*secID*/ )
{
    std::string buf;
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": CqgClient::onBookReset for '" << secDesc << std::endl;
    g_mutex.unlock();

    md_.reset();
}

void BookHandler::onError( Cqg::ASecurityDescription secDesc,
                           Cqg::ASecurityID secID,
                           Engine::AsciiString const& error )
{
    B2B_USE_ARG( secDesc );
    B2B_USE_ARG( secID );

    std::string buf;
    g_mutex.lock();
    std::cerr << Engine::UTCTimestamp::now( true ).toFixString( &buf ) <<  ": CqgClient::onError for '" << md_.SecurityDescription_ << "': " << error << std::endl;
    g_mutex.unlock();
}

//------------------------------------------------------------
//
// CqgClient::Impl
//
//------------------------------------------------------------
class CqgClient::Impl
    : public virtual Cqg::MDApplicationListener
    , public virtual Cqg::SecurityDefinitionListener
    , public virtual Cqg::Logger
{
public:
    typedef std::map<std::string, RefCounter<BookHandler> > BookHandlersMap;

    System::Mutex handlersLock_;
    BookHandlersMap handlers_;
    SecuritiesBySecDescMap securities_;
    Cqg::MDApplication* application_;

    Impl()
        : application_( NULL ) {
    }

public: // MDApplicationListener
    void process( Engine::FIXMessage const& msg, Cqg::FeedID const &feedId ) {
        B2B_USE_ARG( feedId );

        if( Engine::AsciiString::fromStr( "f" ) == msg.type() && msg.hasValue( FIXFields::Symbol ) ) {
            Engine::AsciiString symbol = msg.getAsString( FIXFields::Symbol );
            int status = msg.getAsInt( FIXFields::SecurityTradingStatus );

            MGuard lock( handlersLock_ );

            for( BookHandlersMap::iterator i = handlers_.begin(); i != handlers_.end(); ++i ) {
                i->second->updateStatus( symbol, status );
            }
        }
    }

public: // Logger
    /// Notifies about error event
    void onError( std::string const& message ) {
        ( void )message;
    }

    /// Notifies about warning
    void onWarning( std::string const& message ) {
        ( void )message;
    }

    /// Notifies about note event
    void onNote( std::string const& message ) {
        ( void )message;
    }

    /// Notifies about debug event
    void onDebug( std::string const& message ) {
        ( void )message;
    }

    void subscribeAll( CqgClient* client ) {
        SecuritiesBySecDescMap::iterator it = securities_.begin();
        SecuritiesBySecDescMap::iterator end = securities_.end();

        for( ; it != end; ++it ) {
            client->subscribe( it->first );
        }
    }

public: // SecurityDefinitionListener
    /// Raised when new incoming security definition message is received
    void onSecurityDefinition( Cqg::AFeedID feedID, const std::string& exCode, Engine::FIXMessage const& msg ) {
        ( void ) feedID;
        ( void ) exCode;

        Cqg::SecurityDescription sd = msg.getAsString( FIXFields::SecurityDesc ).toStdString();
        FixMessageInterpreter::applySecurityDef( msg, &securities_[ sd ] );
    }

    /// Raised to notify about security definition request complete
    void onSecurityDefinitionListComplete() {
        //GLOBAL_MTX.unlock();
    }

private:
    ~Impl() {
    }
}; // MDApplicationListenerImpl

//------------------------------------------------------------
//
// CqgClient
//
//------------------------------------------------------------

CqgClient::CqgClient( int argc, char const* const* argv, MarketData::DataReaderAbstract* dataReader )
    : impl_( NULL )
{
    try {
        impl_ = new Impl();

        int arg_num = 1;
        Cqg::MDApplicationParams params;

        if( arg_num < argc ) {
            params.listenInterfaceIP_ = argv[ arg_num++ ];
        }

        if( arg_num < argc ) {
            params.senderCompId_ = argv[ arg_num++ ];
        }

        if( arg_num < argc ) {
            params.sdsLogin_ = argv[ arg_num++ ];
        }

        if( arg_num < argc ) {
            params.sdsPassword_ = argv[ arg_num++ ];
        }

        for( ; arg_num < argc; ++arg_num ) {
            std::vector<std::string> strs;
            split( argv[ arg_num ], ":", &vectorPushBack, &strs );

            if( 3 != strs.size() ) {
                throw Utils::Exception( "Incorrect SDS address string: " + std::string( argv[ arg_num ] ) );
            }

            params.feedParams_.push_back( Cqg::MDApplicationParams::FeedParams( strs[0], "CQG", strs[1], atoi( strs[2].c_str() ) ) );
        }

        //params.feedParams_.push_back( Cqg::MDApplicationParams::FeedParams( "13", "10.1.0.71", 2222 ) );
        params.templatesFn_ = "templates.xml";
        params.logIncomingUdpMessages_ = true;
        params.logIncomingMessages_ = true;
        params.reconnectAttemptCount_ = -1;
        params.incrementalProcessorInterfaceIP_ = params.listenInterfaceIP_;
        
        //GLOBAL_MTX.lock();
        impl_->application_ = Engine::FixEngine::singleton()->createCqgMDApplication( impl_, params, dataReader );
    } catch( ... ) {
        if( NULL != impl_ ) {
            if( NULL != impl_->application_ ) {
                impl_->application_->release();
                impl_->application_ = NULL;
            }

            impl_->release();
        }

        throw;
    }
}

void CqgClient::subscribeAll()
{
    assert( NULL != impl_ );
    assert( NULL != impl_->application_ );
    impl_->subscribeAll( this );
}

void CqgClient::subscribe( std::string const& symbol )
{
    assert( NULL != impl_ );
    assert( NULL != impl_->application_ );

    std::vector<std::string> strs;
    split( symbol, ",", &vectorPushBack, &strs );

    for( size_t i = 0; i < strs.size(); ++i ) {
        trimWhiteSpace( &strs[i] );
        std::pair<Impl::BookHandlersMap::iterator, bool> iterator;

        {
            MGuard lock( impl_->handlersLock_ );
            iterator = impl_->handlers_.insert( std::make_pair( strs[i], BookHandler::Ref( NULL ) ) );
        }

        if( iterator.second ) {
            iterator.first->second = BookHandler::Ref( new BookHandler( strs[i], &impl_->securities_ ) );
        }

        impl_->application_->subscribeBySecurityDescription( iterator.first->second.get(), strs[i] );
    }
}

void CqgClient::unsubscribe( std::string const& symbol )
{
    assert( NULL != impl_ );
    assert( NULL != impl_->application_ );

    impl_->application_->unsubscribeBySecurityDescription( symbol );
}

CqgClient::~CqgClient()
{
    assert( NULL != impl_ );
    assert( NULL != impl_->application_ );

    // Destroy MDApplication
    impl_->application_->release();
    impl_->application_ = NULL;
    impl_->release();
    impl_ = NULL;
}

void CqgClient::unsubscribeAll()
{
    assert( NULL != impl_ );
    assert( NULL != impl_->application_ );

    impl_->application_->unsubscribeAll();
}

void CqgClient::print( std::string const& symbol ) const
{
    Impl::BookHandlersMap::const_iterator i;
    Impl::BookHandlersMap::const_iterator e;

    {
        MGuard lock( impl_->handlersLock_ );
        i = impl_->handlers_.find( symbol );
        e = impl_->handlers_.end();
    }

    if( e == i ) {
        std::cout << "Symbol not found: " << symbol << std::endl;
        return;
    }

    i->second->print();
}
