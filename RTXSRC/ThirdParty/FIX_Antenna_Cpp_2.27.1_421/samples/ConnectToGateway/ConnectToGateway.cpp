/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

/**
* This application connects to the remote FIX Engine and sends New Order messages.
*/

#include <iostream>
#include <ctime>
#include <cstdio>
#include <memory>
#include <fstream>

#include <B2BITS_V12.h>

#include "Helper.h"
#include "Properties.h"
#include "ApplicationProperties.h"
#include "MyApp.h"
#include "Listener.h"

using namespace ::std;
using namespace ::Engine;
using namespace ::Utils;
using namespace ::Aux;

void startApplication( std::string const& ctgw_properties_file );

///////////////////////////////////////////////////////////////////////////////
/// Application entry point.
int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // executable name.
    string applicationName( argv[0] );

    // command line parameters
    std::string ctgw_properties_file = "cngw.properties";
    std::string engine_properties_file = "engine.properties";

    Helper::writeLineToConsole( "ConnectToGateway. (c) B2BITS 2000-2016" );
    Helper::writeLineToConsole();

    if ( 3 == argc ) {
        ctgw_properties_file = argv[1];
        engine_properties_file = argv[2];
    } else if( argc > 1 ) {
        Helper::writeLineToConsole( "Usage:" );
        Helper::writeLineToConsole( "ConnectToGateway <ConnectToGateway properties file> <engine properties file>" );
        Helper::writeLineToConsole();
        return 1;
    }

    try {
        // Initializes engine.
        FixEngine::init( engine_properties_file );
        Listener listener;

        // Registers event listener.
        FixEngine::singleton()->registerEventListener( &listener );

        startApplication( ctgw_properties_file );

        Helper::writeLineToConsole( "Press Enter to exit." );
        cin.get();

        // Unregisters listener before engine destroying.
        FixEngine::singleton()->unregisterEventListener( &listener );

        // Releases engine resources.
        FixEngine::destroy();
    } catch ( const std::exception& ex ) {
        Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
        return -1;
    }

    return 0;
}

void startApplication( std::string const& ctgw_properties_file )
{
    ifstream in( ctgw_properties_file.c_str() );

    if( !in ) {
        throw Utils::Exception( "Cannot open file '" + string( ctgw_properties_file ) + "'" );
    }

    Aux::Properties p( &in );
    ApplicationProperties appParams;
    Helper::parseCommandLine( p, &appParams );


    // Sample message to send to server.
    std::auto_ptr<FIXMessage> pMessage;

    if ( !appParams.messageFileName_.empty() ) {

        // Load message from file, as specified in command line.
        string rawMessage = Helper::loadString( appParams.messageFileName_ );

        // Parses raw message with preferred version parameter.
        pMessage.reset( FIXMsgProcessor::singleton()->parse(
                            rawMessage,
                            NULL,
                            false,
                            false,
                            appParams.sessionParams_.isFIXTSsnProtocol_ ? appParams.sessionParams_.fixVersion_ : Engine::NA )
                      );
    } else {

        // Creates 'New Order - Single' message skeleton.
        pMessage.reset( FIXMsgFactory::singleton()->newSkel( appParams.sessionParams_.fixVersion_, "D" ) );

        char uniqueId[20];

        // Makes unigue identifier.
        sprintf( uniqueId, "%lld", time( NULL ) );

        // Sets up values for message.
        pMessage->set( FIXField::ClOrdID, uniqueId );
        pMessage->set( FIXField::HandlInst,    "2" );
        pMessage->set( FIXField::Symbol,     "IBM" );
        pMessage->set( FIXField::OrderQty,   "200" );
        pMessage->set( FIXField::Side,         "1" );
        pMessage->set( FIXField::OrdType,      "5" );
        pMessage->set( FIXField::TimeInForce,  "0" );

        if( pMessage->isSupported( FIXField::TransactTime ) ) {
            pMessage->set( FIXField::TransactTime, "20030101-00:00:00" );
        }
    }

    // Application instance.
    MyApp application( appParams.sessionParams_);

    // Reports session state.
    Helper::writeLineToConsole( string( "State=" ) +
                                Session::state2string( application.getState() ) );

    application.connect();

    // Processes commands.
    string command;

    do {
        // Writes menu.
        if( !application.isResendInProgress() ) {
            Helper::writeLineToConsole( "Please choose command:" );
            Helper::writeLineToConsole( "'1' - to send the message" );
            Helper::writeLineToConsole( "'2' - to close the session" );
            Helper::writeLineToConsole( "'3' - to switch session's connection (when backup connection defined)" );
            Helper::writeLineToConsole( "'4' - to recreate session" );
        }

        getline( cin, command );

        if( application.isResendInProgress() ) {
            if( "y" == command || "n" == command || "Y" == command || "N" == command ) {
                application.postResendCommand( command );
            } else if( "2" == command ) {
                break;
            } else {
                Helper::writeLineToConsole( "Please enter 'y' to resend the message, otherwise - 'n'" );
            }
        } else if ( "1" == command ) {
            try {
                // Sends the given message to the remote FIX engine.
                application.put( pMessage.get() );

                // Reports about sent message.
                if( 6400 > pMessage->toString()->size() ) {
                    Helper::writeLineToConsole( "Sent: " + * ( pMessage->toString() ) + "\n" );
                } else {
                    Helper::writeLineToConsole( "Sent: Message too big to show it!\n" );
                }
            } catch( const Utils::Exception& ex ) {
                Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
            }
        } else if ( "3" == command ) {
            try {
                // Sends the given message to the remote FIX engine.
                application.switchConnection();
            } catch( const Utils::Exception& ex ) {
                Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
            }
        } else if ( "4" == command ) {
            try {
                application.connect();
            } catch( const Utils::Exception& ex ) {
                Helper::writeErrorLine( "ERROR: " + string( ex.what() ) );
            }
        }

    } while ( "2" != command );

    Helper::writeLineToConsole( "The session will be closed..." );

    application.disconnect();
}

////////////////////////////////////////////////////////////////////////////////
// End of file.
