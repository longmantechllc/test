// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file CqgClient.h Contains CqgClient declaration
/// @author Dmytro Ovdiienko [dmytro_ovdiienko@epam.com]
/// @date 2011-05-25

#ifndef H_B2BITS_SAMPLE_CQG_CqgCLIENT_H
#define H_B2BITS_SAMPLE_CQG_CqgCLIENT_H

#include <string>

namespace Cqg
{
    class MDApplication;
}

namespace MarketData
{
    class DataReaderAbstract;
}

/// Represents sample CQG FIX/FAST Direct client
class CqgClient
{
public:
    CqgClient( int argc, char const* const* argv, MarketData::DataReaderAbstract* dataReader = NULL );
    ~CqgClient();

    void subscribeAll();

    /// Process new instrument
    /// @param symbol Instrument symbol
    void subscribe( std::string const& symbol );

    /// Stop instrument processing
    /// @param symbol Instrument symbol
    void unsubscribe( std::string const& symbol );

    /// Stop processing data for all instruments
    void unsubscribeAll();

    /// Prints Trade information related to symbol
    /// @param symbol Security Description of the symbol
    void print( std::string const& symbol ) const;

private:
    class Impl;
    Impl* impl_;

private:
    /// Copy ctor disabled
    CqgClient( CqgClient const& );
    /// Assign operator disabled
    CqgClient& operator=( CqgClient const& );
};

#endif // H_B2BITS_SAMPLE_CQG_CqgCLIENT_H

