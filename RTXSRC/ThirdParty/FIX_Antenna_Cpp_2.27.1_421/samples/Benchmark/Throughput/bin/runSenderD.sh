#!/bin/sh

# Cleanup logs
sh clean.sh sender

LD_LIBRARY_PATH=$LD_LIBRARY_PATH:../../../../lib:/usr/local/lib
export LD_LIBRARY_PATH

if [ ! -f /logs.sender  ]; then
    mkdir logs.sender
fi

if [ ! -f /logs.sender/backup  ]; then
    mkdir logs.sender/backup
fi

./Sender_D engine.sender.properties benchmark.properties

