// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_String.h
/// Contains Engine::ByteArray, Engine::WideString and Engine::AsciiString classes definition

#ifdef HAVE_PRAGMA_ONCE
#   pragma once
#endif // ifdef( HAVE_PRAGMA_ONCE )

#ifndef B2BITS_FIX_TYPES_String_H
#define B2BITS_FIX_TYPES_String_H

#include <iosfwd>
#include <string>
#include <cassert>
#include <wctype.h>

#include <B2BITS_StringUtils.h>
#include <B2BITS_IntDefines.h>
#include <B2BITS_V12_Defines.h>

#include <B2BITS_CompilerDefines.h>

namespace Engine
{

    /// Represents reference to the memory block.
    /// Object does not own stored memory block.
    template< typename T >
    class MemBlock
    {
    public:
        /// Provides access to the template parameter
        typedef T value_type;

    private:
        const value_type* data_;
        size_t size_;

    public:
        /// Returns pointer to the first character of the block
        ///
        /// @note Memory block does not have null-termination character.
        /// Use size method to get size of the block.
        value_type const* data() const throw() {
            return data_;
        }

        /// Returns size of the block
        std::size_t size() const throw() {
            return size_;
        }

        /// Returns pointer to the next after last character
        value_type const* end() const throw() {
            return data_ + size_;
        }

        /// Returns pointer to first character
        value_type const* begin() const throw() {
            return data_;
        }

        /// Indexer
        value_type const& operator[] ( std::size_t index ) const throw() {
            assert( index < size_ );
            return data()[ index ];
        }

        /// Compares two MemBlock
        /// @return The return value indicates the relationship between the buffers.
        int compareTo( MemBlock rv ) const throw() {
            if( size() < rv.size() ) {
                int result = std::memcmp( data(), rv.data(), size() * sizeof( value_type ) );

                if( 0 == result ) {
                    return -1;
                }

                return result;
            } else if( size() > rv.size() ) {
                int result = std::memcmp( data(), rv.data(), rv.size() * sizeof( value_type ) );

                if( 0 == result ) {
                    return 1;
                }

                return result;
            } else {
                return std::memcmp( data(), rv.data(), size() * sizeof( value_type ) );
            }
        }

        /// Constructor
        /// Constructs instance from address to memory and size
        ///
        /// @param aData address to the block
        /// @param aSize size of the block
        MemBlock( const value_type* aData, size_t aSize ) throw() : data_( aData ), size_( aSize ) {}

        /// Constructor
        /// Constructs instance from address to memory and size
        ///
        /// @param aBegin address to the first byte of the block
        /// @param aEnd address of the byte after last byte of the block
        template< typename Iterator >
        MemBlock( Iterator aBegin, Iterator aEnd ) throw() : data_( &*aBegin ), size_( aEnd - aBegin ) {}

        /// Default constructor
        MemBlock() throw() : data_( NULL ), size_( 0 ) {}
    }; // MemBlock

    /// Represents ASCII std::string
    template< typename CharType >
    class BasicString: public MemBlock<CharType>
    {
    public:
        /// Provides access to the template parameter
        typedef CharType value_type;
        typedef BasicString<value_type> MyT;

        /// Default constructor
        BasicString() throw() : MemBlock<value_type>( empty(), 0 ) {}

        /// Constructor
        /// Constructs instance from address to memory and size
        ///
        /// @param aData address to the block
        /// @param aSize size of the block
        BasicString( const value_type* aData, size_t aSize ) throw()
            : MemBlock<CharType>( aData, aSize )
        {}

        /// Constructor
        /// Constructs instance from address to memory and size
        ///
        /// @param aBegin address to the first byte of the block
        /// @param aEnd address of the byte after last byte of the block
        template< typename Iterator >
        BasicString( Iterator aBegin, Iterator aEnd ) throw() 
            : MemBlock<CharType>( aBegin, aEnd ) 
        {}

        /// Constructor
        /// Constructs instance from std::basic_string.
        ///
        /// @param str Source std::string
        BasicString( std::basic_string<CharType > const& str ) throw()
            : MemBlock<CharType>( str.c_str(), str.size() )
        {}

        /// Constructor
        /// Constructs instance from ANSI std::string
        ///
        /// @param aData address to the ASCII std::string
        BasicString( value_type const* aData ) throw()
            : MemBlock<CharType>( aData, strlen( aData ) )
        {}

        /// Creates instance from array of char
        /// @param aData Array of chars
        template< int N >
        static MyT fromStr( value_type const ( &aData ) [N] ) throw() {
            return MyT( aData, N - 1 );
        }

        int compareTo( BasicString rv ) const throw() {
            return MemBlock<CharType>::compareTo( rv );
        }

        bool equals( BasicString rv ) const throw() {
            return 0 == compareTo( rv );
        }

        /// Converts AsciiString to std::string.
        /// This method creates copy of the stored buffer and returns it as std::string.
        std::basic_string<value_type> toStdString() const {
            return std::basic_string<value_type>( this->data(), this->size() );
        }

        /// Converts AsciiString to std::string
        /// This method creates copy of the stored buffer and returns it as std::string.
        /// @deprecated Use Engine::AsciiString::toStdString instead
        B2B_DEPRECATED( "Use Engine::AsciiString::toStdString instead" )
        std::basic_string<value_type> toString() const {
            return std::basic_string<value_type>( this->data(), this->size() );
        }

        /// Returns true if given string is empty (e.g. size equal to 0)
        bool isEmpty() const throw() {
            return 0 == this->size() && NULL != this->data();
        }

        /// Returns true if given string is NULL
        bool isNull() const throw() {
            return 0 == this->data();
        }

        /// Returns part of the string by given offset and size
        BasicString substr( std::size_t offset, std::size_t size ) const throw() {
            assert( offset + size <= this->size() );
            return BasicString( this->data() + offset, size );
        }

        /// Converts string value to int
        int toInt() const throw() {
            return Utils::StringUtils<value_type>::toInt( this->data(), this->size() );
        }

        /// Converts string value to int
        System::i16 toInt16() const throw() {
            return Utils::StringUtils<value_type>::toInt16( this->data(), this->size() );
        }

        /// Converts string value to int
        System::i32 toInt32() const throw() {
            return Utils::StringUtils<value_type>::toInt32( this->data(), this->size() );
        }

        /// Converts string value to int
        System::i64 toInt64() const throw() {
            return Utils::StringUtils<value_type>::toInt64( this->data(), this->size() );
        }

        /// Converts string value to int
        unsigned int toUInt() const throw() {
            return Utils::StringUtils<value_type>::toUInt( this->data(), this->size() );
        }

        /// Converts string value to int
        System::u16 toUInt16() const throw() {
            return Utils::StringUtils<value_type>::toUInt16( this->data(), this->size() );
        }

        /// Converts string value to int
        System::u32 toUInt32() const throw() {
            return Utils::StringUtils<value_type>::toUInt32( this->data(), this->size() );
        }

        /// Converts string value to int
        System::u64 toUInt64() const throw() {
            return Utils::StringUtils<value_type>::toUInt64( this->data(), this->size() );
        }


        /// Calculates size of string and returns new object of BasicString
        MyT normalize() const throw() {
            for( std::size_t i = 0; i < this->size(); ++i ) {
                if( 0 == this->data()[i] ) {
                    return MyT(this->data(), i );
                }
            }

            return *this;
        }

        /// Search a given character in the string
        /// @param val value to find
        /// @param pos position to start the search
        /// @return position of matching element, if found, otherwise std::string::npos
        /// @code
        /// AsciiString abcd("abcd");
        /// size_t pos = abcd.find('b');
        /// @endcode
        size_t find(value_type val, size_t pos = 0) const {
            for (size_t i = pos; i < this->size(); i++) {
                if (this->data()[i] == val) {
                    return i;
                }
            }
            return std::string::npos;
        }

        /// Find a given substring in the string
        /// @param sub substring to look for as a raw pointer
        /// @param pos position where the search starts
        /// @param count length of the substring to look for
        /// @return if found, offset to the first element of sub, otherwise std::string::npos
        /// @code
        /// AsciiString abcd("abcd");
        /// char name[] = "ab";
        /// size_t pos = abcd.find(name, 0, sizeof(name) - 1);
        /// @endcode
        size_t find(const value_type* sub, size_t pos, size_t count) const {
            for (size_t i = pos; i < this->size(); i++) {
                if (this->size() < i + count) {
                    return std::string::npos;
                }
                if (memcmp(this->data() + i, sub, count*sizeof(value_type)) == 0) {
                    return i;
                }
            }
            return std::string::npos;
        }

        /// Find a given substring in the string
        /// @param sub substring to look for
        /// @param pos position where the search starts
        /// @return if found, offset to the first element of sub, otherwise std::string::npos
        /// @code
        /// AsciiString abcd("abcd");
        /// std::string ab("ab");
        /// size_t pos = abcd.find(ab);
        /// @endcode
        template<typename S>
        size_t find(const S& sub, size_t pos = 0) const {
            return this->find(sub.data(), pos, sub.size());
        }

        /// Find a given substring in the string
        /// @param sub substring to look for as a null terminated character array
        /// @param pos position where the search starts
        /// @return if found, offset to the first element of sub, otherwise std::string::npos
        /// @code
        /// AsciiString abcd("abcd");
        /// const char* ab = "ab";
        /// size_t pos = abcd.find(ab);
        /// @endcode
        size_t find(const value_type* sub, size_t pos = 0) const {
            return this->find(sub, pos, Utils::StringUtils<value_type>::strlen(sub));
        }

    private:
        static value_type const* empty() throw();

        static size_t strlen( CharType const* str ) throw() {
            return Utils::StringUtils<value_type>::strlen( str );
        }

        static int strnicmp( CharType const* str1, CharType const* str2, size_t count ) throw() {
            return Utils::StringUtils<value_type>::strnicmp( str1, str2, count );
        }

        static int strncmp( CharType const* str1, CharType const* str2, size_t count ) throw() {
            return Utils::StringUtils<value_type>::strncmp( str1, str2, count );
        }
    }; // BasicString

    /// String class. 
    /// Extends std::string with some methods.
    ///
    class StringEx
    {
    private:
        std::string impl_;

    public: // constructors/destructors
        /// Default constructor
        StringEx() {}

        /// Constructs from BasicString
        StringEx( BasicString<char> src ) 
        {
            append( src );
        }

        /// Constructs from std::string
        StringEx( std::string const& src ) 
        {
            append( src );
        }

        /// Constructs from C string
        StringEx( char const* src ) 
        {
            append( src );
        }

    public: // operators
        /// Assign operator
        template< typename T >
        StringEx& operator=( T const& v ) {
            impl_.clear();
            *this << v;
            return *this;
        }

        /// Increment operator
        template< typename T >
        StringEx& operator+=( T const& v ) {
            return *this << v;
        }

        /// Shift operator
        template< typename T >
        StringEx& operator<<( T const& v ) {
            return append( v );
        }

        /// Shift operator
        template< int N >
        StringEx& operator<<( char const ( &value ) [N] ) {
            return append( value, N - 1 );
        }

        /// Conversion operator
        operator std::string const&() const { return impl_; }

        /// Conversion operator
        operator std::string&() { return impl_; }
    
    public:
        /// Returns inner string
        std::string& str() { return impl_; }

        /// Returns inner string
        std::string const& str() const { return impl_; }

        /// Returns reference to string
        BasicString<char> reference() const {
            return impl_;
        }

        /// Reserves memory
        StringEx& reserve( std::size_t size ) {
            impl_.reserve( size );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( BasicString<char> value ) {
            impl_.append( value.data(), value.size() );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( char const* value, std::size_t size ) {
            impl_.append( value, size );
            return *this;
        }

        /// Appends given value to string 
        template< int N >
        StringEx& appendStr( char const ( &value ) [N] ) {
            impl_.append( value, N - 1 );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( StringEx const& value ) {
            impl_.append( value.impl_ );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( char const* value ) {
            impl_.append( value );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( std::string const& value ) {
            impl_.append( value );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( char value ) {
            impl_.append( 1, value );
            return *this;
        }

        /// Appends given value to string 
        StringEx& append( std::size_t count, char value ) {
            impl_.append( count, value );
            return *this;
        }

        /// Appends given value to string 
        V12_API StringEx& append( System::i32 value );

        /// Appends given value to string 
        V12_API StringEx& append( System::i64 value );

        /// Appends given value to string 
        V12_API StringEx& append( System::u32 value );

        /// Appends given value to string 
        V12_API StringEx& append( System::u64 value );
    }; // StringEx

    /// BasicString::empty specialization for wchar_t
    template<>
    inline char const* BasicString<char>::empty() throw()
    {
        return "";
    }

    /// BasicString::empty specialization for char
    template<>
    inline wchar_t const* BasicString<wchar_t>::empty() throw()
    {
        return L"";
    }

    /// ByteArray
    typedef MemBlock<unsigned char> ByteArray;
    /// WideString
    typedef BasicString<wchar_t> WideString;
    /// AsciiString
    typedef BasicString<char> AsciiString;
    /// Utf8String
    typedef ByteArray Utf8String;
    /// AsciiString
    typedef BasicString<char> StringRef;
    /// WideString
    typedef BasicString<wchar_t> WStringRef;

    /// Tests if the std::string on the left side of the operator is less than the std::string on the right side.
    inline bool operator <( AsciiString lv, AsciiString rv ) throw()
    {
        return 0 > lv.compareTo( rv );
    }
    /// Tests if the iterator std::string on the left side of the operator is greater than the iterator std::string on the right side.
    inline bool operator >( AsciiString lv, AsciiString rv ) throw()
    {
        return 0 < lv.compareTo( rv );
    }
    /// Tests if the std::string on the left side of the operator is equal to the std::string on the right side.
    inline bool operator ==( AsciiString lv, AsciiString rv ) throw()
    {
        return 0 == lv.compareTo( rv );
    }
    /// Tests if the std::string on the left side of the operator is not equal to the std::string on the right side.
    inline bool operator !=( AsciiString lv, AsciiString rv ) throw()
    {
        return 0 != lv.compareTo( rv );
    }

    /// Tests if the std::string on the left side of the operator is less than the std::string on the right side.
    inline bool operator <( WideString lv, WideString rv ) throw()
    {
        return 0 > lv.compareTo( rv );
    }
    /// Tests if the iterator std::string on the left side of the operator is greater than the iterator std::string on the right side.
    inline bool operator >( WideString lv, WideString rv ) throw()
    {
        return 0 < lv.compareTo( rv );
    }
    /// Tests if the std::string on the left side of the operator is equal to the std::string on the right side.
    inline bool operator ==( WideString lv, WideString rv ) throw()
    {
        return 0 == lv.compareTo( rv );
    }
    /// Tests if the std::string on the left side of the operator is not equal to the std::string on the right side.
    inline bool operator !=( WideString lv, WideString rv ) throw()
    {
        return 0 != lv.compareTo( rv );
    }

    /// Tests if the std::vector on the left side of the operator is less than the std::vector on the right side.
    inline bool operator <( ByteArray lv, ByteArray rv ) throw()
    {
        return 0 > lv.compareTo( rv );
    }
    /// Tests if the iterator std::vector on the left side of the operator is greater than the iterator std::vector on the right side.
    inline bool operator >( ByteArray lv, ByteArray rv ) throw()
    {
        return 0 < lv.compareTo( rv );
    }
    /// Tests if the std::vector on the left side of the operator is equal to the std::vector on the right side.
    inline bool operator ==( ByteArray lv, ByteArray rv ) throw()
    {
        return 0 == lv.compareTo( rv );
    }
    /// Tests if the std::vector on the left side of the operator is not equal to the std::vector on the right side.
    inline bool operator !=( ByteArray lv, ByteArray rv ) throw()
    {
        return 0 != lv.compareTo( rv );
    }

    /// Tests if the object on the left side of the operator is equal to the object on the right side.
    template< typename T >
    inline bool operator ==( MemBlock<T> lv, MemBlock<T> rv ) throw()
    {
        return 0 == lv.compareTo( rv );
    }

    /// Tests if the object on the left side of the operator is not equal to the object on the right side.
    template< typename T >
    inline bool operator !=( MemBlock<T> lv, MemBlock<T> rv ) throw()
    {
        return 0 != lv.compareTo( rv );
    }

    /// Tests if the iterator object on the left side of the operator is greater than the iterator object on the right side.
    template< typename T >
    inline bool operator< ( MemBlock<T> lv, MemBlock<T> rv ) throw()
    {
        return 0 > lv.compareTo( rv );
    }

    /// Tests if the iterator object on the left side of the operator is greater than the iterator object on the right side.
    template< typename T >
    inline bool operator> ( MemBlock<T> lv, MemBlock<T> rv ) throw()
    {
        return 0 < lv.compareTo( rv );
    }

    /// Writes AsciiString to the stream
    extern V12_API std::ostream& operator<< ( std::ostream& stream, AsciiString value );

    /// Writes AsciiString to the stream
    extern V12_API std::ostream& operator<< ( std::ostream& stream, WideString value );

    /// Writes ByteArray to the stream
    extern V12_API std::ostream& operator<< ( std::ostream& stream, ByteArray value );

    /// Writes ByteArray to the stream
    extern V12_API std::ostream& operator<< ( std::ostream& stream, StringEx const& value );
} //namespace Engine

#endif //B2BITS_FIX_TYPES_String_H

