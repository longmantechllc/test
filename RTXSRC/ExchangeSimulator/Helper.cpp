#include "Helper.h"
#include "ApplicationProperties.h"

#include <iostream>
#include <set>
#include <fstream>
#include <sys/stat.h>
#include <cstring>
#include <regex>

using namespace std;
using namespace ::Aux;

// Writes message to console.
void Helper::writeLineToConsole(const string& message)
{
    //outputLock_.lock();
    clog << message << endl;
    //outputLock_.unlock();
}

// Writes line to stderr.
void Helper::writeErrorLine(const string& message)
{
    cerr << message << endl;
}

void Helper::parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params)
{
    params->acceptor_cfg = prop.getString(Aux::Properties::ACCEPTOR_CFG);
    if (prop.isPropertyExists(Aux::Properties::LitVenueList)) {
        auto lites = prop.getString(Aux::Properties::LitVenueList);
        regex re("[,]");
        for(sregex_token_iterator it(lites.begin(), lites.end(), re, -1),  reg_end;
            it != reg_end; ++it) {
            params->litVenueList.emplace(it->str());
        }
        clog << "LitVenueList has " << params->litVenueList.size() << " venues" << endl;
    }

    if (prop.isPropertyExists(Aux::Properties::IOCOrderFillRate)) {
        params->iOCOrderFillRate = prop.getInteger(Aux::Properties::IOCOrderFillRate);
    }

    if (prop.isPropertyExists(Aux::Properties::LitIOCOrderFillRate)) {
        params->litIOCOrderFillRate = prop.getInteger(Aux::Properties::LitIOCOrderFillRate);
    }

    if (prop.isPropertyExists(Aux::Properties::DayOrderFillRate)) {
        params->dayOrderFillRate = prop.getInteger(Aux::Properties::DayOrderFillRate);
    }

}

///////////////////////////////////////////////////////////////////////////////
// Writes application usage instructions.

void Helper::writeUsageInfo(const string& applicationName)
{
    Helper::writeErrorLine();
    Helper::writeErrorLine("Usage: " + applicationName + " <property file>");
    Helper::writeErrorLine();
}

