//
// Created by schen on 10/6/2020.
//

#include <string>
#include "../include/Order.h"

#ifndef RTXROUTER_APPCONSTANTS_H
#define RTXROUTER_APPCONSTANTS_H


static std::string OrderTypeString( int orderType )
{
    switch (orderType)
    {
        case OrderType::TYPE_MARKET : return "mkt";
        case OrderType::TYPE_LIMIT : return "lmt";
        case OrderType::TYPE_STOP_MARKET : return "stpMkt";
        case OrderType::TYPE_STOP_LIMIT : return "stpLmt";
        default : return "UNKNOWN";
    }
}

static std::string OrderTifString( int tif )
{
    switch (tif)
    {
        case TimeInForce::TIF_Day : return "day";
        case TimeInForce::TIF_GTC : return "gtc";
        case TimeInForce::TIF_OPG : return "opg";
        case TimeInForce::TIF_IOC : return "ioc";
        case TimeInForce::TIF_FOK : return "fok";
        case TimeInForce::TIF_GTX : return "gtx";
        case TimeInForce::TIF_GTD : return "gtd";
        case TimeInForce::TIF_CLS : return "cls";

        default : return "UNKNOWN";
    }
}

static std::string SessionTypeStr( int sessionType )
{
    switch (sessionType)
    {
        case SessionType::SESSION_TYPE_PRE_MARKET : return "preMkt";
        case SessionType::SESSION_TYPE_IN_MARKET : return "inMkt";
        case SessionType::SESSION_TYPE_POST_MARKET : return "pstMkt";
        default : return "UNKNOWN";
    }
}

static std::string OrderSideString( int orderSide )
{
    switch (orderSide)
    {
        case OrderSide::SIDE_BUY : return "buy";
        case OrderSide::SIDE_SELL : return "sel";
        case OrderSide::SIDE_SHORT : return "sht";
        default : return "UNKNOWN";
    }
}

namespace AppConstants
{
//Argos
    static const std::string ARGO_RetailSWP_str = "RetailSWP";
    static const std::string ARGO_RetailRouting_str = "RetailRouting";
    static const std::string ARGO_DarkSweep_str = "SWP";
    static const std::string ARGO_Targeting_str = "Targeting";
    static const std::string ARGO_PassivePosting_str = "Posting";

// default logging levels:
    static const int LEVEL_VERBOSE = -1;
    static const int LEVEL_DEBUG = 0;
    static const int LEVEL_INFO = 1;
    static const int LEVEL_EVENT = 2;
    static const int LEVEL_WARN = 3;
    static const int LEVEL_ERROR = 4;
    static const int LEVEL_FATAL = 5;

    static const int LOG_BUFSIZE = 4096;
    static const int READ_BUFSIZE = 1024;

    enum LOG_TO_FILE
    {
        ORDER,
        SYSTEM,
        EXCEPTION
    };

}

#endif //RTXROUTER_APPCONSTANTS_H
