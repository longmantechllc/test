#pragma once

#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include <utility>
#include <shared_mutex>
#include <thread>
#include <thread>

#include "MDInterface.h"

class SimMD : public MDInterface {
public:
    SimMD(const std::string& symbolFile, const std::string& exchs);
    ~SimMD();

    const auto& getSymbols() const { return m_symbols;}
    std::vector <std::shared_ptr<TopOfBook>>  subscribeTOB(const std::string& symbol ) override;
    Bbo subscribeBbo(const std::string& symbol);

    void removeTOBSubscription(const std::string& symbol) override;
    void removeBboSubscription(const std::string& symbol);
    void cancelOpeningTradeRequest(const std::string& symbol) override;
    OpenTrade requestOpeningTrade(const std::string& symbol, int oid)  override;
    bool isInit() { return m_init;}
    auto getMDMsgQueuePtr() { return &m_queue;}

private:
    struct PriceRecord {
        double midPx, bid, ask;
    };
    bool loadDataFile(const std::string& symbolFile);
    void loadExchanges(const std::string& exchanges);
    void genTOB( const std::string& symbol);
    void genBBO( const std::string& symbol);

    std::shared_mutex m_mutex;
    std::unordered_set<std::string> m_subs;
    std::vector<std::string> m_exchs;

    bool m_init {true};
     std::unordered_map<std::string, PriceRecord> m_symbols;
    FILE *m_fp;

    MDMsgQueue m_queue;
    std::thread m_tobThread;
    std::atomic<bool> m_tobThread_running{false};
};
