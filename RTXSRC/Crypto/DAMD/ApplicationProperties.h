#pragma once

#include <string>

/// Application parameters
struct ApplicationProperties
{
    std::string initiator_cfg, preload_file, mmids, rlp_ids, client_name, symbol_file;
    int port {6637};
    bool logging {false};
};

