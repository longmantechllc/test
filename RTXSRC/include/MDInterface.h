#pragma once

#include "MDMsgQueue.h"

class MDInterface;

class MDSubscriber {
public:
    /*
    virtual void OnTopOfBookUpdate( const BatchTOB& tobs) =0;
    //virtual void OnTopOfBookUpdate( const TopOfBook& tob)=0;
    virtual void UpdateBbo(const Bbo& bbo)=0;
    virtual void UpdateOpeningTrade(const OpenTrade& openTrade)=0;
     */
    void SetMktDataIO(MDInterface* md) { m_md = md; }
    void SetMsgQueue(MDMsgQueue* queue) { m_queue = queue; }

protected:
    MDMsgQueue *m_queue;
private:
    MDInterface *m_md;
};

class MDInterface {
public:
    bool addSubscriber(MDSubscriber * sub) {m_sub = sub;}
    MDSubscriber* getSubscriber() const {return m_sub;}
    virtual std::vector <std::shared_ptr<TopOfBook>> subscribeTOB(const std::string& symbol )=0;
    //virtual Bbo subscribeBbo(const std::string& symbol)=0;
    virtual OpenTrade requestOpeningTrade(const std::string& symbol, int oid)=0;

    virtual void removeTOBSubscription(const std::string& symbol)=0;
    //virtual void removeBboSubscription(const std::string& symbol)=0;
    virtual void cancelOpeningTradeRequest(const std::string& symbol)=0;

private:
    MDSubscriber *m_sub {nullptr} ;
};
