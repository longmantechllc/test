//
// Created by schen on 4/27/2021.
//

#ifndef RTXROUTER_RETAILROUTING_H
#define RTXROUTER_RETAILROUTING_H

#include "../include/IOIInterface.h"
#include "IOrderLogicBase.h"

class ExecutionReport;
class RTXOrder;
class IOrderLogicBase;
struct RetailMarketData;

class RetailRouting : public IOrderLogicBase {
public:
    RetailRouting() = default;
    explicit RetailRouting(RTXOrder *order);
    ~RetailRouting();

    bool InitOrder() override;
    bool ProcessOrder() override;
    bool SendChildOrder(std::shared_ptr <Order> order) override;
    bool ProcessChildOrderER(const ExecutionReport *er, std::shared_ptr <ChildOrder> &child) override;
    bool ProcessMktData() override;
    bool m_useOrderLimitPrice = false;

private:
    bool IsRouteEligible(const std::string &routeName);
    bool IsEligible();
    bool IsRouteMarketable(const std::string &routeName);
    bool IsMarketable();

    std::unordered_map <std::string, std::string> m_mmidRouteMap;
//    std::vector<std::shared_ptr<IOIMarketData>> m_contraQuotes;
    std::vector<IOIMarketData> m_contraQuotes;
    bool m_isDone = false;
    bool m_isFirstTimeRetailRouting = true;
    int m_bdcm = 0;
};
#endif //RTXROUTER_RETAILROUTING_H
