#include <iostream>
#include <fstream>
#include <boost/interprocess/allocators/allocator.hpp>
#include <boost/interprocess/managed_mapped_file.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <regex>

#include "Helper.h"
#include "spdlog/spdlog.h"
#include "spdlog/fmt/ostr.h" // must be included
#include "Acceptor.h"
#include "APIcache.h"

using namespace ::std;
namespace bip = boost::interprocess;

void APIcache::loadDataFile() {
    ifstream in(params_.preload_file.c_str());
    if (!in) {
        spdlog::error("Cannot open preload file {}", params_.preload_file);
        return;
    }

    std::string line;
    int lineNum = 0;
    vector<string> exchanges;

    while (std::getline(in, line)) {
        if (line.size()) {
            if (line[0] == '#')
                continue;
            ++lineNum;
            if (lineNum > 2) {
                regex re("[|]");
                sregex_token_iterator it(line.begin(), line.end(), re, -1);

                sregex_token_iterator reg_end;
                string symbol, tags;
                for (int i = 0; it != reg_end; ++it, ++i) {
                    if (i == 0) symbol = it->str();
                    else if (i < exchanges.size()+1) {
                        tags = it->str();

                        //PFOF,PI,Size,Time
                        double pfof, pi;
                        int qty;
                        uint64_t ts;
                        if (!tags.empty()) {
                            regex re2("[,]+");
                            sregex_token_iterator it2(tags.begin(), tags.end(), re2, -1);
                            for (int j = 0; it2 != reg_end; ++it2, ++j) {
                                if (j == 0)
                                    pi = stod(it2->str());
                                else if (j == 1)
                                    pfof = stod(it2->str());
                                else if (j == 2)
                                    qty = stoi(it2->str());
                                else if (j == 3)
                                    ts = stoull(it2->str());
                            }
                            m_apiCache[symbol][exchanges[i-1]] = {pi, pfof, qty, ts};
                        }
                    }
                }
            }
            else if (lineNum == 1) {
                regex re("[|]+");
                sregex_token_iterator it(line.begin(), line.end(), re, -1);
                sregex_token_iterator reg_end;

                for (; it != reg_end; ++it) {
                    exchanges.push_back(it->str());
                }
            }
        }
    }
    spdlog::info("exchanges size = {}, symbol size = {}", exchanges.size(), m_apiCache.size());
}

void APIcache::loadMMID() {
    string line = params_.mmids;
    //logger << "mmids=" << line << endl;
    regex re("[|]+");
    sregex_token_iterator it(line.begin(), line.end(), re, -1);
    sregex_token_iterator reg_end;
    string from, to;
    for (int i = 0; it != reg_end; ++it, ++i) {
        string tags = it->str();
        string from, to;
        double pi, pfof;
        if (!tags.empty()) {
            regex re2("[:]+");
            sregex_token_iterator it2(tags.begin(), tags.end(), re2, -1);
            for (int j = 0; it2 != reg_end; ++it2, ++j) {
                if (j == 0)
                    from = it2->str();
                else if (j == 1)
                    to = it2->str();
                else if (j == 2)
                    pi = atof(it2->str().c_str());
                else if (j == 3) {
                    pfof = atof(it2->str().c_str());

                    m_session2Exch.emplace(from, make_tuple(to, pi, pfof));
                    spdlog::info("map {} to [{},{},{}]", from, to, pi, pfof);
                }
            }
        }
    }
}

APIcache::APIcache(const ApplicationProperties& params)
    : params_(params)
{
    try {
        FIX::SessionSettings settings( params.initiator_cfg );
        FIX::FileStoreFactory storeFactory( settings );
        FIX::FileLogFactory logFactory( settings );
        m_initiator = make_unique<FIX::SocketAcceptor>( *this, storeFactory, settings, logFactory);
        m_initiator->start();
        spdlog::info("connecting to gateway...\n");
    }
    catch ( std::exception & e )
    {
        spdlog::error("Exception {}", e.what());
        return;
    }
    loadMMID();
    // loading test data file
    if (!params_.preload_file.empty()) {
        loadDataFile();
        m_randomizerThread = std::thread([this]{
            spdlog::info("start randomizer thread");
            std::this_thread::sleep_for(std::chrono::seconds(5));
            using keys = std::pair<std::string, std::string>;
            vector<keys> randomVector;
            for(const auto& symExch: m_apiCache) {
                for(const auto& exchIoi: symExch.second)
                    randomVector.push_back({symExch.first, exchIoi.first});
            }
            const auto N = randomVector.size();
            srand((unsigned) time(0));
        });
    }

    loadRLPids();

    m_acceptor = make_unique<Acceptor>(params_.port, this);
    spdlog::info("run acceptor in different thread...\n");
    m_acceptorThread = std::thread([this]{
        spdlog::info("start acceptor thread");
        m_acceptor->start();
    });

    m_init = connectRedline(); // true

    m_loggingThread = std::thread([this]{
        spdlog::info("start logging thread");
        while (m_loggingRunning) {
            int total = 0;
            {
                std::lock_guard lk(m_mutex);
                //symbol,side -> exchange -> data
                for(const auto& e: m_apiCache)
                    total += e.second.size();
            }
            spdlog::info("total FIX msg number: {}, total elements in cache: {}", m_msgNum, total);
            std::this_thread::sleep_for(60s);
        }
    });
}

void APIcache::stopAcceptor() {
    m_acceptor->stop();
}

APIcache::~APIcache()
{
    m_randomizerStop = true;
    m_acceptor->stop();
    m_initiator->stop();
    m_loggingRunning = false;
    spdlog::info("stop both client & APICache sessions");
    std::this_thread::sleep_for(std::chrono::seconds(1));

    if (m_init) {
        if (m_acceptorThread.joinable()) {
            std::this_thread::sleep_for(std::chrono::seconds(1));
        }
        spdlog::info("all threads are shut down");
    }
    spdlog::info("close redline connection");
    for(auto& h : m_symbolHandlers ) {
        auto rc = fh_unsubscribe( m_clientHandle,               // client handle
                             &h.second,         // ptr to sub handle
                             NULL,                    // optional user data
                             FH_DEFAULT_FLAG );       // sub flag
        if ( rc != FH_SUCCESS )
            spdlog::error("fh_unsubscribe failed");

    }
    auto rc = fh_destroy_subscription_template( &sub_template,
                                           FH_RESERVED_FLAG );
    if ( rc != FH_SUCCESS )
        spdlog::error("fh_destroy_subscription_template failed");

    fh_close( &m_clientHandle, FH_DEFAULT_FLAG );

}

void APIcache::onCreate( const FIX::SessionID& sessionID )
{
    spdlog::info("onCreate - {}", sessionID);
}

void APIcache::onLogon( const FIX::SessionID& sessionID )
{

    spdlog::info("onLogon - {}", sessionID);
}

void APIcache::onLogout( const FIX::SessionID& sessionID )
{
    spdlog::info("onLogout - {}", sessionID);
}

void APIcache::toAdmin( FIX::Message& message, const FIX::SessionID& sessionID)
{
    spdlog::info("toAdmin - {}", sessionID);
}

void APIcache::fromAdmin( const FIX::Message& message, const FIX::SessionID& )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon )
{
    spdlog::info("Admin IN: {}", message);
}

void APIcache::fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType )
{
    spdlog::info("App IN: {}", message);
    crack( message, sessionID );
}

void APIcache::toApp( FIX::Message& message, const FIX::SessionID& sessionID )
EXCEPT ( DoNotSend )
{
    try
    {
        FIX::PossDupFlag possDupFlag;
        message.getHeader().getField( possDupFlag );
        if ( possDupFlag ) throw FIX::DoNotSend();
    }
    catch ( FIX::FieldNotFound& ) {}

    spdlog::info("App OUT: {}", message);
}
namespace {
    uint64_t getNow() {
        std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
        //std::time_t currentTime = std::chrono::system_clock::to_time_t(now);
        std::chrono::microseconds now2 = std::chrono::duration_cast<std::chrono::microseconds>(now.time_since_epoch());
        return now2.count();
    }
}
void APIcache::onMessage( const FIX44::IOI& msg, const FIX::SessionID& sessionId) {
    ++m_msgNum;
    try  {
        auto senderId = sessionId.getTargetCompID().getValue();
        //logger << "got API msg:" << msg << "\nsendCompID=["
        //       << senderId << "]" << endl;
        const auto [exchange, d_pi, d_pfof]  = m_session2Exch.at(senderId);
        //logger << "exchange =" << exchange << endl;
        string ioiId = getFieldValue<FIX::IOIID>(msg);
        char ioiTransType = getFieldValue<FIX::IOITransType>(msg);
        auto& exchIoiIds = m_ioiIds[exchange];
        string symbol = getFieldValue<FIX::Symbol>(msg);
        if (ioiTransType == FIX::IOITransType_NEW) {
            if (exchIoiIds.count(ioiId)) {
                spdlog::error("NEW msg tag 23(IOIID) exists before");
                return;
            }
            exchIoiIds.emplace(ioiId, symbol);
        }
        else {
            string ioiRefId = getFieldValue<FIX::IOIRefID>(msg);
            if (exchIoiIds.count(ioiRefId) == 0) {
                spdlog::error("msg tag 26(IOIRefID) {} doesn't exists", ioiRefId);
                return;
            }
            else if (exchIoiIds[ioiRefId] != symbol) {
                spdlog::error("ioi symbol changed");
                return;
            }
            exchIoiIds.erase(ioiRefId);
            if (ioiTransType == FIX::IOITransType_REPLACE)
                exchIoiIds.emplace(ioiId, symbol);
        }

        int side = getFieldValue<FIX::Side>(msg) -'0';

        std::lock_guard lk(m_mutex);
        if (ioiTransType == FIX::IOITransType_CANCEL) {
            if (auto itor1 = m_apiCache.find(symbol+','+(side==1? 'B':'S')); itor1 != m_apiCache.end()) {
                if (auto itor2 = itor1->second.find(exchange); itor2 != itor1->second.end()) {
                    itor1->second.erase(itor2);
                }
            }
            return;
        }

        auto& ioi = m_apiCache[symbol+','+(side==1? 'B':'S')][exchange];
        if (msg.isSetField(FIX::FIELD::IOIQty)) {
            ioi.size = atoi(getFieldValue<FIX::IOIQty>(msg).c_str());
        }
        else
            ioi.size = 0;

        // 31370 for PI, 31460 for PFOF
        if (msg.isSetField(FIX::FIELD::IoiPi)) {
            FIX::IoiPi ioiField;
            msg.getField(ioiField);
            ioi.pi = ioiField.getValue();
            spdlog::info("got api Pi {}", ioi.pi);
        }
        else
            ioi.pi = d_pi;

        if (msg.isSetField(FIX::FIELD::IoiPfof)) {
            FIX::IoiPfof ioiField;
            msg.getField(ioiField);
            ioi.pfof = ioiField.getValue();
            spdlog::info("got api Pfof {}", ioi.pfof);
        }
        else
            ioi.pfof = d_pfof;

        ioi.timeStamp = ::getNow();
        spdlog::info("symbol={}, side={}, exchange={}, size={}, ts={}, pi={}, pfof={}",
                     symbol, (side==1? 'B':'S'), exchange, ioi.size,
                     ioi.timeStamp, ioi.pi, ioi.pfof);

    } catch (std::exception& ex) {
        spdlog::error("Caught: {}", ex.what());
    } catch (...) {
        spdlog::error("Caught unknown EX");
    }
}

void APIcache::loadSymbolFile() {
    ifstream in(params_.symbol_file.c_str());
    if (!in) {
        spdlog::error("Cannot open symbol file {}", params_.symbol_file);
        return;
    }

    std::string line;
    int lineNum = 0;
    vector<string> exchanges;
    using boost::algorithm::trim;
    while (std::getline(in, line)) {
        trim(line);
        if (line.size()) {
            if (line[0] == '#')
                continue;
            m_symbolHandlers.emplace(line, FhSubscripHandle_t());
        }
    }
    spdlog::info("preload {} symbols", m_symbols.size());
}

static const char* get_pid_name( FhClientHandle_t fh_handle, FhPID_t pid )
{
    if( !fh_handle ) {
        return "UNKNOWN";
    }
    const char* pid_name = NULL;
    fh_get_name_from_pid( fh_handle, pid, &pid_name );
    spdlog::info("pid = {}, pid_name {}", (unsigned)pid, pid_name);
    return pid_name;
}

// PID name shortener for the nonscroll book viewer
static const char* get_concise_pid_name( FhClientHandle_t fh_handle, FhPID_t pid )
{
    const char* pid_name = get_pid_name( fh_handle, pid );
    int idx = -1;
    for(int i = strlen(pid_name)-1; i>=0; --i)
    {
        if(pid_name[i] == '_')
        {
            idx = i;
            break;
        }
    }
    spdlog::info("pid = {}, concise name {}", (unsigned)pid, pid_name+1+idx);
    return pid_name+1+idx;
}

#define FH_API( api_call )           \
     ( {                             \
          FhRC_t _rc = api_call;     \
          if (_rc != FH_SUCCESS ) {  \
            spdlog::error("{} failed: {}", #api_call, fh_get_error_string( _rc )); \
            return false;            \
     }} )

#define FH_API2( api_call )           \
     ( {                             \
          FhRC_t _rc = api_call;     \
          if (_rc != FH_SUCCESS ) {  \
            spdlog::error("{} failed: {}", #api_call, fh_get_error_string( _rc )); \
     }} )
//logger <<  #api_call << "failed with error: " << fh_get_error_string( _rc ) << endl;
void APIcache::loadRLPids() {
    spdlog::info("APIcache::loadRLPids() ...");
    if (params_.rlp_ids.empty() || params_.client_name.empty() || params_.symbol_file.empty()) {
        spdlog::error("missing config parameters for RLP");
        return;
    }

    loadSymbolFile();

    m_clientName = params_.client_name;
    spdlog::info("clientName = {}, rlp_ids = {}", params_.client_name, params_.rlp_ids);
    regex re("[|]+");
    sregex_token_iterator it(params_.rlp_ids.begin(), params_.rlp_ids.end(), re, -1);
    sregex_token_iterator reg_end;
    for (int i = 0; it != reg_end; ++it, ++i) {
        string tags = it->str();
        spdlog::info("process tags={}", tags);
        string exch;
        double pi, pfof;
        if (!tags.empty()) {
            regex re2("[:]+");
            sregex_token_iterator it2(tags.begin(), tags.end(), re2, -1);
            for (int j = 0; it2 != reg_end; ++it2, ++j) {
                if (j == 0)
                    exch = it2->str();
                else if (j == 1)
                    pi = atof(it2->str().c_str());
                else if (j == 2) {
                    pfof = atof(it2->str().c_str());

                    m_exchPfof.emplace(exch, make_pair(pi, pfof));
                    spdlog::info("map {}  to [{}, {}]",
                                 exch, pi, pfof);

                }
            }
        }
    }
}

/** A helper function that converts pid names into Participant IDs
 * @param str: PID name
 * @return: PID represented as FhPID_t
 */
FhPID_t fh_util_pid_from_str(FhClientHandle_t fh_handle, const char *str)
{
    if ( str == NULL ) {
        spdlog::error("empty str");
        return 0;
    }

    // see if "FH_PID_" is present at the beginning.
    // if not, add it.
    const char *cmpstr = "FH_PID_";
    char tempstr[ 32 ];

    int len = min( strlen(str), strlen(cmpstr) );
    if ( strncasecmp(str, cmpstr, len) ){
        // FH_PID_ was NOT found at the beginning of the provided string, so add it
        strcpy( tempstr, cmpstr );
        strcat( tempstr, str );
    } else {
        // FH_PID_ was found at the beginning
        strcpy( tempstr, str );
    }

    FhPID_t pid;
    FhRC_t rc = fh_get_pid_from_name( fh_handle, tempstr, &pid );
    if( rc ) {
        spdlog::error("Failed to get pid from name {}", str);
        return 0;
    }
    spdlog::info("got pid {} for {}", (unsigned)pid, str);
    return pid;
}

typedef struct {
    char feed[ FH_CFG_MAX_NAME_LENGTH ];
    char pid[ FH_CFG_MAX_NAME_LENGTH ];
    FhPID_t real_pid{0};
} FhFeedPIDArg_t;

FhFeedPID_t *fh_util_feedpids_from_args(FhClientHandle_t handle, uint32_t num_feedpidargs,
                                        FhFeedPIDArg_t *feedpidargs )
{
    if( num_feedpidargs == 0 )
        return NULL;
    FhFeedPID_t* feedpids = (FhFeedPID_t*)malloc( num_feedpidargs * sizeof( FhFeedPID_t) );
    if( feedpids == NULL ) {
        spdlog::error("malloc failed");
        return 0;
    }

    for( uint32_t i = 0; i < num_feedpidargs; i++ ) {
        FhRC_t rc;
        rc = fh_get_feed_id_from_name( handle, feedpidargs[i].feed, &feedpids[i].feed );
        if( rc ) {
            spdlog::error("Invalid feed_id {}", feedpidargs[i].feed);
            free( feedpids );
            return NULL;
        }
        feedpids[i].pid = ( feedpidargs[i].pid[0] == '\0' ) ? (__typeof__(feedpids->pid))0 :
                          fh_util_pid_from_str( handle, feedpidargs[i].pid );
        spdlog::info("{}, its feed id: {}, its pid: {}",
                     feedpidargs[i].feed, (unsigned)feedpids[i].feed, (unsigned)feedpids[i].pid);
        feedpidargs[i].real_pid = feedpids[i].pid;
    }

    return feedpids;
}

/* Handy macro for generating bit-field printing code. */
#define PRINT_BITFIELD(string, offset, var, enumval) \
{ \
     if (var & enumval) { \
          char *str = string + offset; \
          int size = string_size - offset; \
          int ret; /* snprintf returns # of bytes that WOULD BE written if no truncate */ \
          ret = snprintf(str, size, "%s%s", \
                    offset ? " | " : "", #enumval); \
          offset += min(size, ret); \
          var &= ~enumval; \
     } \
}

typedef struct strings_array_t {
    char trigger[1024];
    char status[1152];
    char subscription_flag[1280];
    char trade_attributes[1024];
    char subscription_id[1024];
    char time[128];
    char utc_time[128];
    char price_entry[128];
    char order_entry[256];
    char aggressor[16];
    char sale_conditions[FH_MAX_SYMBOL_SIZE];
    char suspect_mask[128];
} strings_array_t;

char *fh_util_sprint_status(FhSecurityStatusExt_t _status)
{
    unsigned int offset = 0;
    uint64_t status = (uint64_t) _status;
    strings_array_t *p_sa;
    const size_t string_size = sizeof(p_sa->status);
    static char string[string_size];

    if (status==0)
        offset += snprintf(string + offset, string_size - offset, "EMPTY");

    PRINT_BITFIELD(string, offset, status, FH_STATUS_BASKET_ALT_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BASKET_ALT_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BASKET_INSTRUMENT_MISSING_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BASKET_INSTRUMENT_MISSING_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BOOK_INTERMEDIATE);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BOOK_REMOVED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BOOK_SUBSTITUTED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_BOOK_SUSPECT);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_AUCTION);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_CLOSED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_HALTED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_NBBO_INELIGIBLE_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_NBBO_INELIGIBLE_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_NONEX_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_NONEX_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_OPENED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_PAUSED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_PREMARKET);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_POSTMARKET);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_QUOTING_ONLY);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_REMOVED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_RETAIL_INTEREST_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_RETAIL_INTEREST_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_SLOW_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_MC_SLOW_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_AUCTION);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_CLOSED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_HALTED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_LIMIT_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_LIMIT_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_NONEX_ASK);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_NONEX_BID);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_OPENED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_PAUSED);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_QUOTING_ONLY);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_REG_SSRI);
    PRINT_BITFIELD(string, offset, status, FH_STATUS_TRADING_STATUS_SUSPECT);

    if (status) {
        offset += snprintf(string + offset, string_size - offset, "%sINVALID",
                           offset ? " | " : "");
    }

    return string;
}

bool APIcache::connectRedline() {
    FhRC_t rc = FH_SUCCESS;

    rc = fh_client_open( &m_clientHandle, m_clientName.c_str(), FH_DEFAULT_FLAG );
    if ( rc != FH_SUCCESS ) {
        spdlog::error("fh_client_open() failed on clientName {}, reason: {}",
            m_clientName, fh_get_error_string(rc));
        return false;
    }
    spdlog::info("got m_clientHandle");
    //Step 1:
    const unordered_map<string, string> exch_feedNames = {
            {"BYX", "FH_PID_BATS_BYX"}, {"BX", "FH_PID_NASDAQ_BX"},
            {"NYSE", "FH_PID_NYSE"}, {"IEX", "FH_PID_IEX"}};

    const int numOfFeed = m_exchPfof.size();
    FhFeedPIDArg_t  feedpidargs[numOfFeed];
    int i = 0;
    for (const auto& f: m_exchPfof) {
        strcpy(feedpidargs[i].feed, "FH_FEED_US_EQUITY_SIP");
        strcpy(feedpidargs[i].pid, exch_feedNames.at(f.first).c_str() );
        ++i;
    }

    spdlog::info("call fh_util_feedpids_from_args ...");
    FhFeedPID_t* feedpids = fh_util_feedpids_from_args(m_clientHandle,numOfFeed , feedpidargs);
    if (!feedpids) {
        spdlog::error("can't create feedpids");
        return false;
    }
    spdlog::info("call fh_util_feedpids_from_args successful");

    i = 0;
    for (const auto& f: m_exchPfof) {
        m_pid2Exch[feedpids[i].pid] = f.first;
        ++i;
        spdlog::info("pid {} == {}", (unsigned)feedpids[i].pid, f.first);
    }

    FH_API(fh_create_composite_subscription_template(m_clientHandle, numOfFeed, feedpids,
             1 /*only 1 depth*/, m_symbolHandlers.size(), // this is the number of symbols
             &sub_template,
             ((FH_UPDATE_NO_QUOTES | FH_UPDATE_NO_TRADES) & ~FH_DELIVERY_MASK & ~FH_UPDATE_NBBO_MODE)));

    spdlog::info("fh_create_composite_subscription_template successfully creates sub_template");

    // Step 2:
    for (auto& [symbol,symbol_handler]: m_symbolHandlers) {
        FH_API(fh_subscribe_from_template(
                sub_template, symbol.c_str(),
                NULL, //this,// user_data to be passed with updates from this subscription
                NULL, &symbol_handler,  FH_DELIVERY_NONE // FH_DELIVERY_CALLBACK
        ));
    }
    spdlog::info("successfully subscribe all the symobls");

    return true;
}

vector<pair<string, IOIData>> APIcache::getRLP(string symbolSide) {
    auto pos = symbolSide.find(",");
    auto symbol = symbolSide.substr(0, pos);
    auto side = symbolSide.substr(pos+1);
    spdlog::info("symbolSide: {}, its symbol: {}, side: [{}]",
        symbolSide, symbol, side);
    if (!m_symbolHandlers.count(symbol))
        return {};
    auto handle = m_symbolHandlers[symbol];
    const FhBookUpdate_t* update = NULL;
    FhRC_t rc = fh_get_book_snapshot( m_clientHandle, handle, NULL,
                               &update, FH_DEFAULT_FLAG );

    if ( rc == FH_ERROR_EMPTY ) {
        spdlog::error("*** Error: redline book for symbol {} not found", symbol);
        return {};
    }

    if (rc != FH_SUCCESS) {
        spdlog::error("fh_get_book_snapshot() returns error {}", fh_get_error_string( rc ));
        return {};
    }

    int i = 0;
    auto flag = (side[0] == 'S'? FH_STATUS_MC_RETAIL_INTEREST_ASK : FH_STATUS_MC_RETAIL_INTEREST_BID);
    vector<pair<string, IOIData>> result;
    for (i = 0; i < update->numParticipantBooks; i++)
    {
        FhSecurityStatusExt_t extStatus;
        const FhBookUpdate_t *participantBook = update->participantBooks[i];
        fh_get_extended_status(participantBook, &extStatus);

        if (extStatus & flag) {
            //logger << "got interest on PID " << (int) participantBook->PID << ", idx = " << i << endl;
            const auto& exch = m_pid2Exch[participantBook->PID];
            const auto& pipf = m_exchPfof[exch];
            result.emplace_back(make_pair(exch, IOIData{pipf.first, pipf.second, 0, ::getNow()}));
        }
    }
    return result;
}
