#!/bin/bash

# if [ $# -ne 2 ]; then
#     echo $"Usage: $0 --use-cxx11-abi={0|1} --searchpath=path_to_libV12.so_directory"
#     exit 1
# fi

for i in "$@"
do
case $i in
    --use-cxx11-abi=*)
    USE_CXX11_ABI="${i#*=}"
    shift # past argument=value
    ;;
    --searchpath=*)
    LIB_DIR="${i#*=}"
    shift # past argument=value
    ;;
    *)
        echo $"Usage: $0 --use-cxx11-abi={0|1} --searchpath={path_to_libV12.so_directory}"
        exit 1  # unknown option
    ;;
esac
done

CUR_DIR=${PWD}
LINK_NAME=libV12.so

# Centos 7 and higher, Redhat, Ubuntu 16.04 and higher
if [ -e "/etc/os-release" ]
then
    PLATFORM_ID=`awk -F= '$1=="ID" { print tolower($2)}' /etc/os-release | tr -d \"`
    PLATFORM_VERSION=`awk -F= '$1=="VERSION_ID" { print $2 ;}' /etc/os-release | sed 's/\.//' | tr -d \"`

    if [ "${PLATFORM_ID}" = "centos" ]; then
        PLATFORM_ID=rh
    fi
# Centos and Redhat (if '/etc/os-release' file doesn't exist)
elif [ -e "/etc/redhat-release" ]
then
    PLATFORM_ID=rh
    PLATFORM_VERSION=`cat /etc/redhat-release | awk '{print $(NF-1)}' | cut -d. -f1`
else
    echo "[WARNING] Unsupported platform or OS"
    exit 1
fi

# get GCC version
GCC_CURRENT_VER=`gcc -dumpversion`
GCC_REQUIRED_VER="5.1"

# get matched library
# here we sort and compare '$GCC_CURRENT_VER' string with '$GCC_REQUIRED_VER'
# if current gcc version greater or equal required gcc version we will get
# following condition 'if["5.1" = "$GCC_REQUIRED_VER"]'
# otherwise we will get condition 'if["$GCC_CURRENT_VER" = "$GCC_REQUIRED_VER"]'.
# This is equivalent to 'if[gcc_cur_ver >= gcc_required_ver]'
if [ "x${USE_CXX11_ABI}" == "x1" ] && [ `printf '%s\n' "$GCC_REQUIRED_VER" "$GCC_CURRENT_VER" | sort -V | head -n1` = "$GCC_REQUIRED_VER" ]
then
    LIB_FILE=`ls ${LIB_DIR} | grep 'libV12_.*.so$' | grep "${PLATFORM_ID}${PLATFORM_VERSION}_cxx11.so"`
else
    LIB_FILE=`ls ${LIB_DIR} | grep 'libV12_.*.so$' | grep "${PLATFORM_ID}${PLATFORM_VERSION}.so"`
fi

if [ "x${LIB_FILE}" = "x" ]; then
    echo "[WARNING] Unsupported platform: ${PLATFORM_ID}${PLATFORM_VERSION} "
    echo "[WARNING] GCC_CURRENT_VER=${GCC_CURRENT_VER} and _GLIBCXX_USE_CXX11_ABI=${USE_CXX11_ABI}"
    exit 1
fi

# get current library on link
if which readlink > /dev/null 
then
    CUR_LIB_FILE=`readlink ${LIB_DIR}/${LINK_NAME}`
else
    echo "[WARNING] readlink command was not found"
fi

if [ "x${CUR_LIB_FILE}" != "x${LIB_FILE}" ]
then
    cd ${LIB_DIR}
    ln -fs ${LIB_FILE} ${LINK_NAME}
    cd ${CUR_DIR}

    if [ -z $CUR_LIB_FILE ]; then
        echo "[INFO] The link ${LINK_NAME} is set to ${LIB_FILE}"
    else
        echo "[INFO] The link ${LINK_NAME} is switched from ${CUR_LIB_FILE} to ${LIB_FILE}"
    fi
else 
    echo "[INFO] ${CUR_LIB_FILE} is used as ${LINK_NAME}"
fi

