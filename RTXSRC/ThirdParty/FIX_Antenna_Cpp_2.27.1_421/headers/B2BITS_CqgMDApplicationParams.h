// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CqgMDApplicationParams.h
/// Contains Cqg::MDApplicationParams class definition


#ifndef H_B2BITS_CQG_MDApplicationParams_H
#define H_B2BITS_CQG_MDApplicationParams_H

#include <string>
#include <vector>
#include <B2BITS_CqgDefines.h>
#include <B2BITS_V12_Defines.h>

#if defined(_MSC_VER)
#   pragma warning(push)
#   pragma warning(disable: 4251)
#endif // defined(_MSC_VER)

namespace Cqg
{

    /// MDApplication parameters
    class V12_API MDApplicationParams
    {
    public:
        /// Feed parameters
        struct V12_API FeedParams {
            struct Addr {
                Addr() : sdsHost_(), sdsPort_( 0 ){}
                Addr( std::string const& sdsAddr, int sdsPort )
                    : sdsHost_( sdsAddr )
                    , sdsPort_( sdsPort )
                {}
                /// SDS server address
                std::string sdsHost_;
                /// SDS server port
                int sdsPort_;
               
            };

            /// Feed(Channel) ID
            FeedID id_;
            std::string exCode_;
            Addr primary_;
            Addr secondary_;
            /// enable TCP recovery for specified feed (default false)
            bool useTcprecovery_;

            FeedParams( std::string const& id, std::string const& exCode, std::string const& sdsAddr, int sdsPort, bool useTcprecovery = false )
                : id_( id )
                , exCode_( exCode )
                , primary_( sdsAddr, sdsPort )
                , useTcprecovery_(useTcprecovery)
            {
            }

            ~FeedParams() {}
        };

        /// Type of transport layer.
        /// Sockets    - standard IP sockets
        /// MyricomDBL - Myricom Datagram Bypass Layer
        enum Transport { Sockets, MyricomDBL };

        /// FIX/FAST Direct protocol type specification
        /// v1 - dictionaries are reset
        /// v2 - dictionaries are not reset - this is by default
        enum CqgProtocolVersion { v1, v2 };

    public:
        /// Login user name to connect to SDS server
        std::string sdsLogin_;

        /// Login user password to connect to SDS server
        std::string sdsPassword_;

        /// SenderCompID value
        std::string senderCompId_;

        /// Feed connection parameters
        std::vector<FeedParams> feedParams_;

        /// IP of network interface to listen on; nullptr or empty string means all interfaces.
        /// Default value is null (all interfaces)
        std::string listenInterfaceIP_;

        /// Path to the file with CQG network configuration
        std::string channelDefinitionFile_;

        /// Path to the FAST templates file
        std::string templatesFn_;

        /// Worker count to process W messages.
        /// Default: 1
        int numberOfWorkers_;

        /// UDP incoming buffer size
        /// If 0 [default], size of the buffer will be defined by the OS
        int incomingUdpBufferSize_;

        /// Pass true to write out to the log file incoming FIX messages
        /// Default value is false
        bool logIncomingMessages_;

        /// Pass true to write out to the binary log file incoming FAST messages
        /// Default value is false
        bool logIncomingUdpMessages_;

        /// Number of incoming messages with seq num out of order to skip before start recovery.
        /// Default value is 10
        size_t detectionHolePackDelay_;

        /// Reconnect attempt count to each SDS server
        int reconnectAttemptCount_;

        /// Transport layer for data reading
        /// Default value is Transport::Sockets
        Transport transportType_;

        /// FIX/FAST Direct protocol version
        CqgProtocolVersion protocolVersion_;

        /// Worker count to process X messages.
        /// If 0, one thread poool will be used to process W and X messages.
        /// Default: 3
        int incrementalProcessorWorkerCount_;

        /// IP of network interface to listen on for separate "X" processing; nullptr or empty string means all interfaces.
        /// Default value is null (all interfaces)
        std::string incrementalProcessorInterfaceIP_;

        /// Transport layer for data reading by separated "X" threads
        /// Default value is Transport::Sockets
        Transport incrementalProcessorTransportType_;

        /// Maximum number of messages could be stored in recovery mode for the particular instrument.
        /// Default value is 50
        size_t incrementQueueSize_;

         /// Count of aditional threads for calling callbacks
        //// Default value is 0
        size_t additionalThreadsCount_;

    public:
        /// Default constructir
        MDApplicationParams()
            : numberOfWorkers_( 1 )
            , incomingUdpBufferSize_( 0 )
            , logIncomingMessages_( false )
            , logIncomingUdpMessages_( false )
            , detectionHolePackDelay_( 10 )
            , reconnectAttemptCount_( -1 )
            , transportType_( Sockets )
            , protocolVersion_( v2 )
            , incrementalProcessorWorkerCount_( 3 )
            , incrementalProcessorTransportType_( Sockets ) 
            , incrementQueueSize_(50) 
            , additionalThreadsCount_(0)
        {
        }

        ~MDApplicationParams() {}
    }; // class MDApplication {
} // namespace Cqg {

#if defined(_MSC_VER)
#   pragma warning( pop )
#endif // defined(_MSC_VER)

#endif // H_B2BITS_CQG_MDApplicationParams_H

