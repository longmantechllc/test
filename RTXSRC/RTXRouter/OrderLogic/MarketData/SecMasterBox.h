//
// Created by schen on 2/19/2021.
//
#ifndef RTXROUTER_LOADSECMASTER_H
#define RTXROUTER_LOADSECMASTER_H

class App;
class Config;
class Logger;

using namespace std;

struct SecMasterData
{
    SecMasterData(){}
    SecMasterData(std::string symbolIn, std::string exchangeIdIn, std::string isIPOIn, int lotSizeIn=100)
    :symbol(symbolIn),
     exchangeId(exchangeIdIn),
     lotSize(lotSizeIn),
     isIPO(isIPOIn)
    {
    }
    std::string symbol;
    std::string exchangeId;
    int lotSize;
    std::string isIPO;  //"Y"
};

class SecMasterBox
{
public:
    SecMasterBox(){}
    SecMasterBox(App* app);
    ~SecMasterBox();
    void InitSecMasterBox();
    void LoadSecMasterBox();
    bool ProcessData(const std::string& line);
    std::string GetExchangeName(const std::string& symbol);
    bool GetIPO(const std::string& symbol);

private:
    Config* m_config;
    std::string m_path;
    std::ifstream m_stream;
    Logger* m_logger = nullptr;

    std::unordered_map<std::string, std::shared_ptr<SecMasterData>> m_secMasterDataList;
};
#endif //RTXROUTER_LOADSECMASTER_H
