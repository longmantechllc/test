// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2019.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SessionsController.h
/// Contains Engine::SessionsController class declaration.

#ifndef _B2BITS_Sessions_Controller__h_
#define _B2BITS_Sessions_Controller__h_

#include <B2BITS_V12_Defines.h>
#include <B2BITS_ReferenceCounter.h>
#include <B2BITS_SessionId.h>
#include <B2BITS_Application.h>
#include <B2BITS_SessionParameters.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

namespace Engine
{
    /** Sessions controller exception class */
    class V12_API SessionsControllerException : public Utils::Exception
    {
    public:
        enum ErrorCode
        {
            /** Session was not found */
            ErrorCode_NotFound = 1,
            /** Session is in a wrong state */
            ErrorCode_BadState,
            /** Bad call arguments */
            ErrorCode_BadArguments,
            /** Some uncertain internal error */
            ErrorCode_Internal
        };
    public:
        SessionsControllerException( int errorCode )
            :errorCode_( errorCode )
        {
        }

        SessionsControllerException( const std::string& aReason, int errorCode )
            :Utils::Exception( aReason )
            , errorCode_( errorCode )
        {
        }

        /**
        * Returns error code
        */
        int getErrorCode() const
        {
            return errorCode_;
        }

    private:
        int errorCode_;
    };

    /** Abstract class(interface) that introduces API contract of sessions controller.*/
    class V12_API SessionsController : public Utils::ReferenceCounter
    {
    public:
        /** Set of sessions registered.*/
        typedef std::set<Engine::SessionId> Sessions;

        /**
        * Starts session.
        * @param sessionId - id of the session to start.
        */
        virtual void startSession( const Engine::SessionId& sessionId ) = 0;

        /**
        * Connects session.
        * @param sessionId - id of the session to connect.
        */
        virtual void connectSession( const Engine::SessionId& sessionId ) = 0;

        /**
        * Disconnects session.
        * @param sessionId - id of the session to disconnect.
        */
        virtual void disconnectSession( const Engine::SessionId& sessionId ) = 0;

        /**
        * Stops session.
        * @param sessionId - id of the session to stop.
        */
        virtual void stopSession( const Engine::SessionId& sessionId ) = 0;

        /**
        * Returns set of sessions registered.
        */
        virtual Sessions getRegisteredSessions() const = 0;

    };
    typedef Utils::ReferenceCounterSharedPtr<SessionsController> SessionsControllerPtr;

    /** 
    * Implementation of sessions controller interface the utilizes FixAntenna engine to operate.
    * This class owns registered sessions. It means that user shouldn't warry about keeping session's pointer or doing other sessions management.
    */
    class V12_API FixEngineSessionsController : public SessionsController
    {
    public:
        /** Smart pointer to hold Engine's session*/
        typedef Utils::ReferenceCounterSharedPtr<Engine::Session> SessionPtr;

        FixEngineSessionsController();
        virtual ~FixEngineSessionsController();

        /**
        * Registers session.
        * @param sessionId - sessionid of the session to register.
        *
        * @param sessionApp - user application to serve as FixAntenna engine callbacks sink.
        *
        * @param sessionExtraParameters - session configuration to use when starting session. This application will be used when session is being started.
        *
        * throws SessionsControllerException with ErrorCode_BadArguments error code set if session with the same id is already registered.
        */
        void registerSession( const Engine::SessionId& sessionId, Engine::Application& sessionApp, const SessionExtraParameters& sessionExtraParameters );

        /**
        * Registers already running session. Sessions controller takes ownership over the session on success.
        * @param activeSession - already running session's pointer.
        *
        * @param sessionNewApp - user application to serve as FixAntenna engine callbacks sink. This application will be used when session is being started next time.
        *
        * @param switchApplication - flag telling whatever to keep current session's application or replace it with sessionNewApp.
        *
        * @param pNewSessionExtraParameters - pointer to session's configuration to use when starting session next time. If this is NULL current session configuration is used.
        *
        * throws SessionsControllerException with ErrorCode_BadArguments error code set if session with the same id is already registered or activeSession doesn't point to valid session.
        */
        void registerSession( const SessionPtr& activeSession, Engine::Application& sessionNewApp, bool switchApplication = false, const SessionExtraParameters* pNewSessionExtraParameters = NULL );

        /**
        * Unregisters session. If session is running(started or connected) it continues to operate but session controller stops owning it.
        * @param sessionId - sessionid of the session to unregister.
        *
        * Returns pointer to session if it is running or empty pointer otherwise.
        *
        * throws SessionsControllerException with ErrorCode_NotFound error code set if session with the id passed is not registered.
        */
        SessionPtr unregisterSession( const Engine::SessionId& sessionId );

        /**
        * Returns list of registered sessions.
        */
        Sessions getRegisteredSessions() const;

        /**
        * Returns session's extra paramters
        * @param sessionId - sessionid of the registered session.
        *
        * throws SessionsControllerException with ErrorCode_NotFound error code set if session with the id passed is not registered.
        */
        SessionExtraParameters getSessionParameters( const Engine::SessionId& sessionId ) const;

        /**
        * Returns session's application
        * @param sessionId - sessionid of the registered session.
        *
        * throws SessionsControllerException with ErrorCode_NotFound error code set if session with the id passed is not registered.
        */
        Engine::Application& getSessionApplication( const Engine::SessionId& sessionId ) const;

        /**
        * Starts session.
        * @param sessionId - id of the session to start.
        *
        * The implementation creates(calls engine's CreateSession() routine) session internally using parameters and application passed with registerSession call.
        *
        * throws SessionsControllerException with the following error codes set:
        *        ErrorCode_NotFound - if session with the id passed is not registered.
        *        ErrorCode_BadState - if session is already started.
        */
        virtual void startSession( const Engine::SessionId& sessionId );

        /**
        * Connects session.
        * @param sessionId - id of the session to connect.
        *
        * The implementation calls session's connect() routine internally.
        *
        * throws SessionsControllerException with the following error codes set:
        *        ErrorCode_NotFound - if session with the id passed is not registered.
        *        ErrorCode_BadState - if session isn't started.
        */
        virtual void connectSession( const Engine::SessionId& sessionId );

        /**
        * Disconnects session.
        * @param sessionId - id of the session to disconnect.
        *
        * The implementation calls session's disconnect() routine internally.
        *
        * throws SessionsControllerException with the following error codes set:
        *        ErrorCode_NotFound - if session with the id passed is not registered.
        *        ErrorCode_BadState - if session isn't started.
        */
        virtual void disconnectSession( const Engine::SessionId& sessionId );

        /**
        * Stops session.
        * @param sessionId - id of the session to stop.
        *
        * The implementation destroys (releases session's pointer and the engine destroys it as a result) session internally.
        *
        * throws SessionsControllerException with the following error codes set:
        *        ErrorCode_NotFound - if session with the id passed is not registered.
        *        ErrorCode_BadState - if session isn't started.
        */
        virtual void stopSession( const Engine::SessionId& sessionId );

        /**
        * Returns active session's pointer
        * @param sessionId - sessionid of the registered and active(started) session.
        *
        * throws SessionsControllerException with the following error codes set:
        *        ErrorCode_NotFound - if session with the id passed is not registered.
        *        ErrorCode_BadState - if session isn't started.
        */
        SessionPtr getSessionPtr( const Engine::SessionId& sessionId ) const;
    private:
        class Impl;
        Impl* pImpl_;
    };
    typedef Utils::ReferenceCounterSharedPtr<FixEngineSessionsController> FixEngineSessionsControllerPtr;

} // namespace Engine{

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // #ifndef _Sessions_Controller__h_

