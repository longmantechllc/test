/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <algorithm>

#include "Params.h"
#include <B2BITS_V12.h>

using namespace b2bitscme;

namespace {
	const std::string prefix = "CMESample.";

	bool getBoolean(const Engine::FAProperties* properties, const std::string& propertyName, Utils::Log::LogCategory* logger) {
		const std::string prop = prefix + propertyName;
		if (!properties->existsProperty(prop)) {
			if (logger)
				logger->note("Property [" + prop + "] was not found");
			return false;
		}
		std::string res = properties->getStringProperty(prop);

		if (res == "true" || res == "True" || res == "TRUE")
			return true;
		
		return false;
		
	}

	int getInteger(const Engine::FAProperties* properties, const std::string& propertyName, Utils::Log::LogCategory* logger) {
		const std::string prop = prefix + propertyName;
		if (!properties->existsProperty(prop)) {
			if (logger)
				logger->note("Property [" + prop + "] was not found");
			return 0;
		}
		return properties->getIntegerProperty(prop);
	}

	std::string getString(const Engine::FAProperties* properties, const std::string& propertyName, Utils::Log::LogCategory* logger) {
		const std::string prop = prefix + propertyName;
		if (!properties->existsProperty(prop)) {
			if (logger)
				logger->note("Property [" + prop + "] was not found");
			return "";
		}
		return properties->getStringProperty(prop);
	}

	std::string trim(const std::string& str) {
		if (str.empty()) {
			return str;
		}

		size_t leftPos = 0;
		while (' ' == str[leftPos] && str.size() > leftPos) {
			leftPos++;
		}

		size_t rightPos = str.size() - 1;
		while (' ' == str[rightPos] && 0 != rightPos) {
			rightPos--;
		}

		if (rightPos < leftPos) {
			return "";
		}

		return str.substr(leftPos, rightPos - leftPos + 1);
	}

	std::vector<std::string> split(const std::string& str, char delim) {
		std::vector<std::string> strings;
		std::string::const_iterator start = str.begin();
		std::string::const_iterator stop = std::find(str.begin(), str.end(), delim);
		std::string val;
		
		while (stop != str.end()) {
			val = trim(std::string(start, stop));
			if (!val.empty()) {
				strings.push_back(val);
			}
			start = stop + 1; //skip delim
			stop = std::find(start, str.end(), delim);
		}

		val = trim(std::string(start, stop));
		if (!val.empty()) {
			strings.push_back(val);
		}

		return strings;
	}

	std::string toString(int val) {
		std::stringstream buff;
		buff << val;
		return buff.str();
	}
}

Params::MarketSegment::MarketSegment() {

}

Params::MarketSegment::MarketSegment(const std::string &id, const std::string &primaryHost, const std::string &backupHost):
	id_(id), primaryHost_(primaryHost), backupHost_(backupHost) {
	
}

Params::Params(const Engine::FAProperties* properties, Utils::Log::LogCategory* logger):
	port_(getInteger(properties, "Port", logger)),
	senderCompID_(getString(properties, "SenderCompId", logger)),
	keysFilePath_(getString(properties, "KeysFilePath", logger)),
	senderLocationID_(getString(properties, "SenderLocationId", logger)),
	senderSubID_(getString(properties, "SenderSubId", logger)),
	applicationSystemName_("FIXAntenna"),
	tradingSystemVersion_(Engine::FixEngine::getEngineVersion()), 
	applicationSystemVendor_("B2BITS"),
	isDropCopy_(getBoolean(properties, "IsDropCopy", logger)),
	processMessagesOutOfOrder_(getBoolean(properties, "ProcessMessagesOutOfOrder", logger)),
	logger_(logger)
	{
	
	std::vector<std::string> marketSegmentIds = split(getString(properties, "MarketSegments", logger), ',');

	for (size_t i = 0; i < marketSegmentIds.size(); ++i) {
		std::string id = marketSegmentIds[i];
		if (id.empty()) {
			continue;
		}

		std::string primaryHost = getString(properties, "MarketSegment." + id + ".PrimaryHost", logger);
		std::string backupHost = getString(properties, "MarketSegment." + id + ".BackupHost", logger);
		if (primaryHost.empty() || backupHost.empty()) {
			continue;
		}

		marketSegments_[id] = MarketSegment(id, primaryHost, backupHost);
	}
}

void Params::showParams() const {
	if (NULL == logger_)
		return;

	logger_->note("-----------------------------------------------------------------------------------------------");
    logger_->note("Parsed parameters (Market Segments with empty Id, Primary or Backup Hosts were skipped):");
    logger_->note("-----------------------------------------------------------------------------------------------");
    logger_->note("Port: " + toString(port_));
    logger_->note("SenderCompId: " + senderCompID_);
	logger_->note("SenderSubId: " + senderSubID_);
	logger_->note("SenderLocationId: " + senderLocationID_);
    logger_->note("KeysFilePath: " + keysFilePath_);
    logger_->note("ApplicationSystemName: " + applicationSystemName_);
    logger_->note("TradingSystemVersion: " + tradingSystemVersion_);
    logger_->note("ApplicationSystemVendor: " + applicationSystemVendor_);

	if (processMessagesOutOfOrder_)
		logger_->note("Handle out of order messages as soon as they come");
	else
		logger_->note("Out of order messages are queued and handled in right order");

	if (isDropCopy_)
		logger_->note("Assume connecting to DropCopy interface");
	else
		logger_->note("Assume connecting to iLink MSGW Trading Interface");

	logger_->note("Market Segments:");
	for (MarketSegmentByIdT::const_iterator it = marketSegments_.begin(); it != marketSegments_.end(); ++it) {
		logger_->note("Id: " + it->first);
		logger_->note("Primary Host: " + it->second.primaryHost_);
		logger_->note("Backup Host: " + it->second.backupHost_);
	}
	logger_->note("-----------------------------------------------------------------------------------------------");
}