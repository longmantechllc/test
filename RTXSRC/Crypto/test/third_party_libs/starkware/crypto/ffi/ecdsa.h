#ifndef STARKWARE_CRYPTO_FFI_ECDSA_H_
#define STARKWARE_CRYPTO_FFI_ECDSA_H_

#include "gsl/gsl-lite.hpp"
#include "starkware/crypto/ffi/utils.h"
#include "starkware/algebra/prime_field_element.h"
namespace starkware {

int GetPublicKey(const char* private_key, char* out);

int Verify(const char* stark_key, const char* msg_hash, const char* r_bytes, const char* w_bytes);

// int Sign(const char* private_key, const char* message, const char* k, char* out);

int Sign(
    const gsl::byte private_key[kElementSize], const gsl::byte message[kElementSize],
    const gsl::byte k[kElementSize], gsl::byte out[kOutBufferSize]);

} // namespace starkware

#endif  // STARKWARE_CRYPTO_FFI_ECDSA_H_
