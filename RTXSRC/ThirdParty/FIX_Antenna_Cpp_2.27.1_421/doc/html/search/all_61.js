var searchData=
[
  ['a1',['a1',['../structSystem_1_1IPAddr.html#a00124c192d7defd1790673b2e245737e',1,'System::IPAddr']]],
  ['a2',['a2',['../structSystem_1_1IPAddr.html#a9900808d4ab400859308214e09b344f8',1,'System::IPAddr']]],
  ['a3',['a3',['../structSystem_1_1IPAddr.html#a9fc37d9d9fef6dc95efcac8818bd005a',1,'System::IPAddr']]],
  ['a4',['a4',['../structSystem_1_1IPAddr.html#a80488ceae8ec8ee29933fc1cfeb0c7df',1,'System::IPAddr']]],
  ['acceptor_5fsession_5frole',['ACCEPTOR_SESSION_ROLE',['../namespaceEngine.html#a766a42d43f584fd43de42d603a030e8ca8dc6afbaa438ec2e6e8ed9db595bf3c4',1,'Engine']]],
  ['acceptwithconfirmlogon',['AcceptWithConfirmLogon',['../classEngine_1_1LogonEvent.html#a8bb1fa2a725d5f7f2dbd3fc11dffdd0da90dfd9f1b86654b84d6483507090f56f',1,'Engine::LogonEvent']]],
  ['account',['Account',['../namespaceFIXFields.html#aa2c213752744b124c0631a6144504b11',1,'FIXFields']]],
  ['accounttype',['AccountType',['../namespaceFIXFields.html#a51655444abbda498ba578f8bad67be75',1,'FIXFields']]],
  ['accruedinterestamt',['AccruedInterestAmt',['../namespaceFIXFields.html#a606368daebbc43740665ce51d8ad0f28',1,'FIXFields']]],
  ['accruedinterestrate',['AccruedInterestRate',['../namespaceFIXFields.html#a8fc87cc5e2c03669341206fb3d0b9da0',1,'FIXFields']]],
  ['acctidsource',['AcctIDSource',['../namespaceFIXFields.html#a32a6a629b0074c01f84925624c65e60e',1,'FIXFields']]],
  ['activeconnection',['ActiveConnection',['../classEngine_1_1Session.html#aa4f9eece15b5234303e837d824613b57',1,'Engine::Session']]],
  ['activeconnection_5f',['activeConnection_',['../structEngine_1_1StorageMgr_1_1PersistentSessionInfo.html#a206e7df719d93773a41b09fcf90a47d5',1,'Engine::StorageMgr::PersistentSessionInfo']]],
  ['activeconnections',['ActiveConnections',['../structGlobex_1_1MDApplicationParams.html#a69ebf14d2ee9bc4afba1ec002c50d12a',1,'Globex::MDApplicationParams']]],
  ['activeconnections20_5f',['activeConnections20_',['../structGlobex_1_1MDApplicationParams.html#a6e8eadd4dee6c24fca5838dd00d28639',1,'Globex::MDApplicationParams']]],
  ['activeconnections_5f',['activeConnections_',['../structGlobex_1_1MDApplicationParams.html#a0fd46765a1bd478749072178187c9d69',1,'Globex::MDApplicationParams']]],
  ['addblock',['addBlock',['../classFixDictionary2_1_1Protocol.html#aa69754b41c0ee3b7a968e5cdb5ab3d7d',1,'FixDictionary2::Protocol']]],
  ['addbookitem',['addBookItem',['../classBats_1_1InstrumentListener.html#acad9c941bac9736489a93c639eb5dc7b',1,'Bats::InstrumentListener']]],
  ['addchild',['addChild',['../classFixDictionary2_1_1MessageItemContainer.html#a86d2bec62eb658bc4a5e40828d3156ff',1,'FixDictionary2::MessageItemContainer']]],
  ['addentrytonestedgroup',['addEntryToNestedGroup',['../classEngine_1_1FIXMsgHelper.html#a8ff7be4ea6604b557b1a09dc6a9437e4',1,'Engine::FIXMsgHelper::addEntryToNestedGroup(TagValue *parent, int leading_group_tag)'],['../classEngine_1_1FIXMsgHelper.html#a8f2f60a7473a651c11f3aaebac861ee8',1,'Engine::FIXMsgHelper::addEntryToNestedGroup(TagValue &amp;parent, int leading_group_tag)'],['../classEngine_1_1FIXMsgHelper.html#a1e6bd460f39d7475832ddcaa0d400360',1,'Engine::FIXMsgHelper::addEntryToNestedGroup(FIXGroup *parent, int leading_group_tag, int index)'],['../classEngine_1_1FIXMsgHelper.html#a9fe80cc1dfa3989df0be82908b85d235',1,'Engine::FIXMsgHelper::addEntryToNestedGroup(FIXGroup &amp;parent, int leading_group_tag, int index)']]],
  ['addfield',['addField',['../classFixDictionary2_1_1Protocol.html#ab46450674a4b9cbfd879ecbdc71b7b73',1,'FixDictionary2::Protocol']]],
  ['addfieldtype',['addFieldType',['../classFixDictionary2_1_1Protocol.html#a1675965bc68a761ce97e78e37f7cd783',1,'FixDictionary2::Protocol']]],
  ['addfieldtypes',['addFieldTypes',['../classFixDictionary2_1_1Protocol.html#a6390418521b7011c4bd7050744b3d539',1,'FixDictionary2::Protocol::addFieldTypes(std::list&lt; FieldTypeT::Ref &gt; *types)'],['../classFixDictionary2_1_1Protocol.html#a6781b6aceca72b7feedee2e39a78f2df',1,'FixDictionary2::Protocol::addFieldTypes(FieldTypeT::RefArray types, std::size_t size)']]],
  ['additionalfields_5f',['additionalFields_',['../structEngine_1_1FixEngine_1_1InitParameters.html#abdcd0e0a6e48144a9a4148585b8cb5c6',1,'Engine::FixEngine::InitParameters']]],
  ['additionalthreadscount_5f',['additionalThreadsCount_',['../classCqg_1_1MDApplicationParams.html#ae5070b66aa28ec952382968196b8283a',1,'Cqg::MDApplicationParams']]],
  ['addmessage',['addMessage',['../classFixDictionary2_1_1Protocol.html#aefa64b4956bdfc0d4de89f27d3132e50',1,'FixDictionary2::Protocol']]],
  ['addorderexpandedmsg',['AddOrderExpandedMsg',['../classBats_1_1AddOrderExpandedMsg.html#a82c317352345b96610a912c12a13ec3b',1,'Bats::AddOrderExpandedMsg']]],
  ['addorderexpandedmsg',['AddOrderExpandedMsg',['../classBats_1_1AddOrderExpandedMsg.html',1,'Bats']]],
  ['addorderlongmsg',['AddOrderLongMsg',['../classBats_1_1AddOrderLongMsg.html',1,'Bats']]],
  ['addorderlongmsg',['AddOrderLongMsg',['../classBats_1_1AddOrderLongMsg.html#a600847f11104db841cd4f14928ab5be5',1,'Bats::AddOrderLongMsg']]],
  ['addordershortmsg',['AddOrderShortMsg',['../classBats_1_1AddOrderShortMsg.html#ae47ee028051409b01e34e1581eb1545c',1,'Bats::AddOrderShortMsg']]],
  ['addordershortmsg',['AddOrderShortMsg',['../classBats_1_1AddOrderShortMsg.html',1,'Bats']]],
  ['addr',['Addr',['../structCqg_1_1MDApplicationParams_1_1FeedParams_1_1Addr.html',1,'Cqg::MDApplicationParams::FeedParams']]],
  ['addr',['Addr',['../structCqg_1_1MDApplicationParams_1_1FeedParams_1_1Addr.html#a423eacfc6ff403b9d2a2ef9d48c57462',1,'Cqg::MDApplicationParams::FeedParams::Addr::Addr()'],['../structCqg_1_1MDApplicationParams_1_1FeedParams_1_1Addr.html#afcc625f4324609bf92844a7d2709f7ba',1,'Cqg::MDApplicationParams::FeedParams::Addr::Addr(std::string const &amp;sdsAddr, int sdsPort)']]],
  ['addref',['addRef',['../classUtils_1_1ReferenceCounter.html#a480927b8580e7b588bed09d54dbb3bd4',1,'Utils::ReferenceCounter']]],
  ['addresses',['Addresses',['../namespaceEngine.html#abc493d900524d2c225edf9e37d2ca5e7',1,'Engine']]],
  ['addvalblock',['addValBlock',['../classFixDictionary2_1_1Protocol.html#a87fe2361092a6d837d26ebe03575b3c4',1,'FixDictionary2::Protocol']]],
  ['addvalue',['addValue',['../classFixDictionary2_1_1ValBlock.html#a95bced54b149977b7d1f1c11114394b1',1,'FixDictionary2::ValBlock::addValue(std::string const &amp;value, std::string const &amp;description, std::string const &amp;id)'],['../classFixDictionary2_1_1ValBlock.html#a960227feab760bdde4f2a2428a196f1b',1,'FixDictionary2::ValBlock::addValue(std::string const &amp;value, std::string const &amp;description, std::string const &amp;id, bool &amp;duplicate)'],['../classFixDictionary2_1_1ValBlock.html#a6c664b5225bc12995d0b17341a94ccd8',1,'FixDictionary2::ValBlock::addValue(std::string const &amp;value, bool &amp;duplicate)']]],
  ['adjustment',['Adjustment',['../namespaceFIXFields.html#ae6c7b75257f4154d180c5613c19670f5',1,'FIXFields']]],
  ['adjustmenttype',['AdjustmentType',['../namespaceFIXFields.html#af5554de2aee55df795b0152ae49dc802',1,'FIXFields']]],
  ['admin_5flisten_5fport',['ADMIN_LISTEN_PORT',['../namespaceEngine.html#a046525c568ddf756aca11728c75a2921a638b66a759528a849ec08426742e6e5f',1,'Engine']]],
  ['adminapplication',['AdminApplication',['../classEngine_1_1AdminApplication.html',1,'Engine']]],
  ['adminapplication',['AdminApplication',['../classEngine_1_1AdminApplication.html#a0196c99a814cc8a8d04f13eb42d70475',1,'Engine::AdminApplication']]],
  ['advid',['AdvId',['../namespaceFIXFields.html#ae69e1c1e2e82bcffb91830f8c7eb882c',1,'FIXFields']]],
  ['advrefid',['AdvRefID',['../namespaceFIXFields.html#a843fd3549863afee638ed236ee454b13',1,'FIXFields']]],
  ['advside',['AdvSide',['../namespaceFIXFields.html#af52be49146723bfdc4b9adc2a4695d29',1,'FIXFields']]],
  ['advtranstype',['AdvTransType',['../namespaceFIXFields.html#a564bb6838ae079402d60164bd2ed13da',1,'FIXFields']]],
  ['afeedid',['AFeedID',['../namespaceCqg.html#a7f33470cdbd49e317aa099b9781c8b72',1,'Cqg']]],
  ['affectedorderid',['AffectedOrderID',['../namespaceFIXFields.html#a02ba1d13d433e6271a890c6a28b14b28',1,'FIXFields']]],
  ['affectedsecondaryorderid',['AffectedSecondaryOrderID',['../namespaceFIXFields.html#acc3ae18d10f90f4577d740ab3c6d29f9',1,'FIXFields']]],
  ['affinity64_5ft',['affinity64_t',['../namespaceSystem.html#a7e324ca619e7922b8ab24830dbb15aa2',1,'System']]],
  ['affirmstatus',['AffirmStatus',['../namespaceFIXFields.html#abe6ce18316a06973f01136b7ff616614',1,'FIXFields']]],
  ['afterrun',['afterRun',['../classSystem_1_1Thread.html#afb0697a565109266bb22a52abddba7fc',1,'System::Thread']]],
  ['aggregatedbook',['AggregatedBook',['../namespaceFIXFields.html#a1ee9ff145ec41f606bccac49015a9d88',1,'FIXFields']]],
  ['aggressive_5freceive_5fsocket_5fop_5fpriority',['AGGRESSIVE_RECEIVE_SOCKET_OP_PRIORITY',['../namespaceEngine.html#a7989b0b9ac83aeaeb3a1adb652ebc6e4abf1b0ac18c8d9a53f4d4be1370df3c4e',1,'Engine']]],
  ['aggressive_5fsend_5fand_5freceive_5fsocket_5fop_5fpriority',['AGGRESSIVE_SEND_AND_RECEIVE_SOCKET_OP_PRIORITY',['../namespaceEngine.html#a7989b0b9ac83aeaeb3a1adb652ebc6e4af4200376ca1fa35a4ad563dc3f504e3b',1,'Engine']]],
  ['aggressive_5fsend_5fsocket_5fop_5fpriority',['AGGRESSIVE_SEND_SOCKET_OP_PRIORITY',['../namespaceEngine.html#a7989b0b9ac83aeaeb3a1adb652ebc6e4a4561edf1d9aec8077778ecd0a20dfd56',1,'Engine']]],
  ['aggressivereceivedelay',['AggressiveReceiveDelay',['../namespaceEngine_1_1SessionParameters.html#a7b752c2fc23bbc37ab499ad25a4547ee',1,'Engine::SessionParameters']]],
  ['aggressivereceivedelay_5f',['aggressiveReceiveDelay_',['../structEngine_1_1SessionExtraParameters.html#a0072949d675aeb956add3648617e2ce1',1,'Engine::SessionExtraParameters']]],
  ['aggressorindicator',['AggressorIndicator',['../namespaceFIXFields.html#ae7a2dd855aeb40bcf503da24085c9515',1,'FIXFields']]],
  ['aggressorside',['AggressorSide',['../namespaceFIXFields.html#a19ed2ea25ccc6914639c6ab3771d6f3c',1,'FIXFields']]],
  ['agreementcurrency',['AgreementCurrency',['../namespaceFIXFields.html#af6ba498ee5ace1257aaf3de4b39cf8d0',1,'FIXFields']]],
  ['agreementdate',['AgreementDate',['../namespaceFIXFields.html#afa6fd9fd3895ce1ababe6449a850f60d',1,'FIXFields']]],
  ['agreementdesc',['AgreementDesc',['../namespaceFIXFields.html#af30509a6c3234b81e7060a6c03f06739',1,'FIXFields']]],
  ['agreementid',['AgreementID',['../namespaceFIXFields.html#a82bc480525d28643141af0ce36c31257',1,'FIXFields']]],
  ['alert',['Alert',['../classEngine_1_1FastScp11_1_1Alert.html',1,'Engine::FastScp11']]],
  ['allocaccount',['AllocAccount',['../namespaceFIXFields.html#ac3490922e415abc30561113f1632d1af',1,'FIXFields']]],
  ['allocaccounttype',['AllocAccountType',['../namespaceFIXFields.html#af64d90c783371d47b23a671da389eeba',1,'FIXFields']]],
  ['allocaccruedinterestamt',['AllocAccruedInterestAmt',['../namespaceFIXFields.html#a3a97bfcdf3055ae5376b1a0611326468',1,'FIXFields']]],
  ['allocacctidsource',['AllocAcctIDSource',['../namespaceFIXFields.html#a15d2a99f1c94d0f9808f996cf855dd62',1,'FIXFields']]],
  ['allocavgpx',['AllocAvgPx',['../namespaceFIXFields.html#a2ebc2d63d74436f12a35a77442233fd5',1,'FIXFields']]],
  ['alloccancreplacereason',['AllocCancReplaceReason',['../namespaceFIXFields.html#a13ffb4403ee642310f839a09d156b6ad',1,'FIXFields']]],
  ['allocclearingfeeindicator',['AllocClearingFeeIndicator',['../namespaceFIXFields.html#a5d2d055c86bdaa7a859689a9629f7c97',1,'FIXFields']]],
  ['alloccustomercapacity',['AllocCustomerCapacity',['../namespaceFIXFields.html#ae4ce85782e48a29500372b9207ca2bb8',1,'FIXFields']]],
  ['allochandlinst',['AllocHandlInst',['../namespaceFIXFields.html#ab5e17d2289a916071bc8b6610eb2d555',1,'FIXFields']]],
  ['allocid',['AllocID',['../namespaceFIXFields.html#a6ba7afc5bb6d2104acdfe52241bd8eeb',1,'FIXFields']]],
  ['allocinterestatmaturity',['AllocInterestAtMaturity',['../namespaceFIXFields.html#ac5b2fc58b9964a8ae5f356dd77890705',1,'FIXFields']]],
  ['allocintermedreqtype',['AllocIntermedReqType',['../namespaceFIXFields.html#adeddc3781730be85302bd7d3964e3483',1,'FIXFields']]],
  ['alloclinkid',['AllocLinkID',['../namespaceFIXFields.html#a81833b9e8610a0529044ab951b21f80a',1,'FIXFields']]],
  ['alloclinktype',['AllocLinkType',['../namespaceFIXFields.html#aade815725928603780928726b93e1950',1,'FIXFields']]],
  ['allocmethod',['AllocMethod',['../namespaceFIXFields.html#a946babf52e168f3ce41655c54bc842fb',1,'FIXFields']]],
  ['allocnetmoney',['AllocNetMoney',['../namespaceFIXFields.html#a0fc68e541a1c9aeee8108eecb2ab4bb4',1,'FIXFields']]],
  ['allocnoorderstype',['AllocNoOrdersType',['../namespaceFIXFields.html#acdc1129678202aa2cc0ab2dcbfbc7c8e',1,'FIXFields']]],
  ['allocpositioneffect',['AllocPositionEffect',['../namespaceFIXFields.html#a22fbf7b1a545d094a36356e50d37ae32',1,'FIXFields']]],
  ['allocprice',['AllocPrice',['../namespaceFIXFields.html#abff34982681ff8f76026d1657e4570c7',1,'FIXFields']]],
  ['allocqty',['AllocQty',['../namespaceFIXFields.html#ad5d8e9baf79782df901853fd20dbfb59',1,'FIXFields']]],
  ['allocrejcode',['AllocRejCode',['../namespaceFIXFields.html#a7e7aebcb118e3240e71d576cd3be6254',1,'FIXFields']]],
  ['allocreportid',['AllocReportID',['../namespaceFIXFields.html#a1ecdf9af4c1a3f3d49de8a92aa47b72b',1,'FIXFields']]],
  ['allocreportrefid',['AllocReportRefID',['../namespaceFIXFields.html#aff73d72dc78025148d76f9234cd4ac2c',1,'FIXFields']]],
  ['allocreporttype',['AllocReportType',['../namespaceFIXFields.html#afb1e749b73058341a3325283f906e5a9',1,'FIXFields']]],
  ['allocsettlcurramt',['AllocSettlCurrAmt',['../namespaceFIXFields.html#a7d12db9ede1276cec87b5825aa087287',1,'FIXFields']]],
  ['allocsettlcurrency',['AllocSettlCurrency',['../namespaceFIXFields.html#a354e1a1256ea7e6a64188f2fe0bed502',1,'FIXFields']]],
  ['allocsettlinsttype',['AllocSettlInstType',['../namespaceFIXFields.html#a0ac844f2e5f494f63380fc48a0bc2b47',1,'FIXFields']]],
  ['allocshares',['AllocShares',['../namespaceFIXFields.html#a543b631546d0686758a9ec8373d9f762',1,'FIXFields']]],
  ['allocstatus',['AllocStatus',['../namespaceFIXFields.html#ae9840aed7a8c0981810d7180cefb44c6',1,'FIXFields']]],
  ['alloctext',['AllocText',['../namespaceFIXFields.html#ab6c7344ad72fe6b76a38767b99945f50',1,'FIXFields']]],
  ['alloctranstype',['AllocTransType',['../namespaceFIXFields.html#a3a474fd8c815d6c5626d2409e04bf7d9',1,'FIXFields']]],
  ['alloctype',['AllocType',['../namespaceFIXFields.html#a3f7a2c8a150de070fb089340e1c458fa',1,'FIXFields']]],
  ['allow_5fzero_5fnumingroup',['ALLOW_ZERO_NUMINGROUP',['../classEngine_1_1FIXMsgProcessor.html#a66a0b64cc2db49da91f984924ec477b8a6bc22cf907f1a5064b3144a6e40e6989',1,'Engine::FIXMsgProcessor']]],
  ['allowableonesidednesscurr',['AllowableOneSidednessCurr',['../namespaceFIXFields.html#a84ddd8ba06a87f2057bc8669e8c41903',1,'FIXFields']]],
  ['allowableonesidednesspct',['AllowableOneSidednessPct',['../namespaceFIXFields.html#ae1fa57e0bfd35e010da54dbc2415c3a3',1,'FIXFields']]],
  ['allowableonesidednessvalue',['AllowableOneSidednessValue',['../namespaceFIXFields.html#ab4eacaf9ed05bd9584ecbed617c99e73',1,'FIXFields']]],
  ['allowmessagewithoutpossdupflag',['AllowMessageWithoutPossDupFlag',['../namespaceEngine_1_1SessionParameters.html#a86f5e759c0c47d892beb941b687bd3f7',1,'Engine::SessionParameters']]],
  ['allowmessagewithoutpossdupflag_5f',['allowMessageWithoutPossDupFlag_',['../structEngine_1_1SessionExtraParameters.html#aeb05e67822e3d36dfce36645c1ff7b24',1,'Engine::SessionExtraParameters']]],
  ['allowzeronumingroup_5f',['allowZeroNumInGroup_',['../structEngine_1_1SessionExtraParameters_1_1ValidationParameters.html#a6169fd2e6aa943c7daee2e9ed23af029',1,'Engine::SessionExtraParameters::ValidationParameters']]],
  ['altmdsourceid',['AltMDSourceID',['../namespaceFIXFields.html#a7420dff08a6e1639f3cfebd5946a1ef6',1,'FIXFields']]],
  ['anchor_5ffile_5fname',['ANCHOR_FILE_NAME',['../namespaceEngine_1_1FIXPropertiesNames.html#a93eddcdc3e14aeb6a40b36dc7b1e1213',1,'Engine::FIXPropertiesNames']]],
  ['append',['append',['../classEngine_1_1StringEx.html#abc806f098ddd3372720cc34b7ea51dbb',1,'Engine::StringEx::append(BasicString&lt; char &gt; value)'],['../classEngine_1_1StringEx.html#a91abfdc8279e1e46b77e3eb3b9fb0a9f',1,'Engine::StringEx::append(char const *value, std::size_t size)'],['../classEngine_1_1StringEx.html#a747f30abbe314b25724c4dcfe7c744a2',1,'Engine::StringEx::append(StringEx const &amp;value)'],['../classEngine_1_1StringEx.html#a03bce8beb37a759aef072ba5028ac25a',1,'Engine::StringEx::append(char const *value)'],['../classEngine_1_1StringEx.html#a704f37023c624713b9e95eac6b676181',1,'Engine::StringEx::append(std::string const &amp;value)'],['../classEngine_1_1StringEx.html#aa2240ea6cf0ae2b8f01a5b7df03d67ef',1,'Engine::StringEx::append(char value)'],['../classEngine_1_1StringEx.html#a83aa77212a86daeb0ea024d983ced793',1,'Engine::StringEx::append(std::size_t count, char value)'],['../classEngine_1_1StringEx.html#a80d7df23766726af494b50f3f3071dcb',1,'Engine::StringEx::append(System::i32 value)'],['../classEngine_1_1StringEx.html#aa0b25b968af5f869ee99f4908b2c77ec',1,'Engine::StringEx::append(System::i64 value)'],['../classEngine_1_1StringEx.html#a620b5dbfc58bc115e471d749ecd43944',1,'Engine::StringEx::append(System::u32 value)'],['../classEngine_1_1StringEx.html#a49a2e75d1622d8c7363880178077ff76',1,'Engine::StringEx::append(System::u64 value)']]],
  ['appendstr',['appendStr',['../classEngine_1_1StringEx.html#a501b723bbaeab307bc8504a0346aee8a',1,'Engine::StringEx']]],
  ['applbegseqnum',['ApplBegSeqNum',['../namespaceFIXFields.html#a41abe7ce2fc070e5b30ee14d6c537bc4',1,'FIXFields']]],
  ['applendseqnum',['ApplEndSeqNum',['../namespaceFIXFields.html#a0f908ca9ec22c7511280b50971d096d5',1,'FIXFields']]],
  ['applextid',['ApplExtID',['../namespaceFIXFields.html#ade815e4c6fe918424702205a10b579ac',1,'FIXFields']]],
  ['application',['Application',['../classBats_1_1Application.html',1,'Bats']]],
  ['application',['Application',['../classEngine_1_1Application.html',1,'Engine']]],
  ['applicationlistener',['ApplicationListener',['../classBats_1_1ApplicationListener.html',1,'Bats']]],
  ['applicationoptions',['ApplicationOptions',['../structBats_1_1ApplicationOptions.html',1,'Bats']]],
  ['applid',['ApplID',['../namespaceFIXFields.html#a26ff206aa75d2361836d19370417498e',1,'FIXFields']]],
  ['appllastseqnum',['ApplLastSeqNum',['../namespaceFIXFields.html#af8c77500e85d3eff0537734fc2b3836a',1,'FIXFields']]],
  ['applnewseqnum',['ApplNewSeqNum',['../namespaceFIXFields.html#a1d0309617ac286d61c6fee182d9c0d4c',1,'FIXFields']]],
  ['applqueueaction',['ApplQueueAction',['../namespaceFIXFields.html#a8ae078c72d0d2d987c99c440a29f44ed',1,'FIXFields']]],
  ['applqueuedepth',['ApplQueueDepth',['../namespaceFIXFields.html#a58f15f6c120bc252999e604f6812fc02',1,'FIXFields']]],
  ['applqueuemax',['ApplQueueMax',['../namespaceFIXFields.html#adfb6faef46721c3d9415a012c8bbbbe9',1,'FIXFields']]],
  ['applqueueresolution',['ApplQueueResolution',['../namespaceFIXFields.html#ab21595b04988e614295e26d7891c7fef',1,'FIXFields']]],
  ['applreportid',['ApplReportID',['../namespaceFIXFields.html#a444003b0ef91722d187e1e2a3d411d51',1,'FIXFields']]],
  ['applreporttype',['ApplReportType',['../namespaceFIXFields.html#aff82f0ab1ef803c1cf77ab02fbcc08d5',1,'FIXFields']]],
  ['applreqid',['ApplReqID',['../namespaceFIXFields.html#ab91e76aa2b6ed9aedaeb3169c70a8a03',1,'FIXFields']]],
  ['applreqtype',['ApplReqType',['../namespaceFIXFields.html#ac483059fceba019b5c5f2f90061ad06f',1,'FIXFields']]],
  ['applresendflag',['ApplResendFlag',['../namespaceFIXFields.html#a504133650447b966730ee998e7003256',1,'FIXFields']]],
  ['applresponseerror',['ApplResponseError',['../namespaceFIXFields.html#a376b19c2dd491cd1ae6beff739515598',1,'FIXFields']]],
  ['applresponseid',['ApplResponseID',['../namespaceFIXFields.html#ad58e40a030c1b6ab55e59d6c731722ca',1,'FIXFields']]],
  ['applresponsetype',['ApplResponseType',['../namespaceFIXFields.html#ae4f73d05f3faa3b98cff7fa891e65a38',1,'FIXFields']]],
  ['applseqnum',['ApplSeqNum',['../namespaceFIXFields.html#a78987dea0d6af208d32f865a82840d84',1,'FIXFields']]],
  ['appltotalmessagecount',['ApplTotalMessageCount',['../namespaceFIXFields.html#ae00b3f196059d03dfeae7b9a8b52c890',1,'FIXFields']]],
  ['applverid',['ApplVerID',['../namespaceFIXFields.html#a27442f7c9c27b7a17bf00776a1273ad2',1,'FIXFields']]],
  ['apply',['apply',['../classEngine_1_1FIXGroup.html#a4190b2739a7d9a0208679d3ef04c99eb',1,'Engine::FIXGroup::apply()'],['../classEngine_1_1TagValue.html#a8cfd76c32c532c5558830ad5338f002b',1,'Engine::TagValue::apply()']]],
  ['appprotocolsnames',['AppProtocolsNames',['../structEngine_1_1ParserDescription.html#af3e8947b5c646b55e460d4ee5c107345',1,'Engine::ParserDescription']]],
  ['appprotocolsnames_5f',['appProtocolsNames_',['../structEngine_1_1ParserDescription.html#a457d1ffe8596a4630e9e3a4c46fdfef7',1,'Engine::ParserDescription']]],
  ['ascii',['ASCII',['../namespaceEngine_1_1RawDataTypeProcessingStrategies.html#aaf1eaabf07b5f6622532f4d87dd7c205ab04255e0c4139da68aa625a9ca30429a',1,'Engine::RawDataTypeProcessingStrategies']]],
  ['asciistring',['AsciiString',['../namespaceEngine.html#a4ae124f1b4abd471959febad480c1e83',1,'Engine']]],
  ['asecuritydescription',['ASecurityDescription',['../namespaceCqg.html#a771a56c5b76774bcdd1d827ede2d04fd',1,'Cqg']]],
  ['asecurityid',['ASecurityID',['../namespaceCqg.html#a46b07917a63ae419a26e4a6b18d372ee',1,'Cqg']]],
  ['asgnrptid',['AsgnRptID',['../namespaceFIXFields.html#a82e89a68542b3f034e50cbf35c0b5321',1,'FIXFields']]],
  ['asofindicator',['AsOfIndicator',['../namespaceFIXFields.html#a29dce644238fe26e7e5d95a495273155',1,'FIXFields']]],
  ['assignmentmethod',['AssignmentMethod',['../namespaceFIXFields.html#a3f169a3179e63ea0f83c1dab12f7b9ee',1,'FIXFields']]],
  ['assignmentunit',['AssignmentUnit',['../namespaceFIXFields.html#a9da351734cbc4b09e8489bbf2dd25519',1,'FIXFields']]],
  ['asubchannelid',['ASubChannelID',['../namespaceCqg.html#af9a4c23a70312216184ecabbd0316490',1,'Cqg']]],
  ['asynchronous',['ASYNCHRONOUS',['../classSystem_1_1Thread.html#ae3664379e05d2a1007b31edd939b47a3aa47c2cfecd81f6d08a315c673c6cc8bc',1,'System::Thread']]],
  ['atoi64',['atoi64',['../namespaceUtils.html#a16bff877ef8beddea9657a3a8d6ab074',1,'Utils']]],
  ['attach',['attach',['../classUtils_1_1Log_1_1LogSystem.html#a898f27aee10d7d40ae59840e848f87ed',1,'Utils::Log::LogSystem']]],
  ['attachmentpoint',['AttachmentPoint',['../namespaceFIXFields.html#a4b22e072ce305faaabcf7726dffb8ae9',1,'FIXFields']]],
  ['attachschedulemanager',['attachScheduleManager',['../classEngine_1_1SessionsSchedule.html#a6570cdcdcebf83639e9c7786be703570',1,'Engine::SessionsSchedule']]],
  ['attachstateobserver',['attachStateObserver',['../classEngine_1_1Session.html#af73f316fab085f62da2ab3bb1ecc42b6',1,'Engine::Session']]],
  ['auctionsummarymsg',['AuctionSummaryMsg',['../classBats_1_1AuctionSummaryMsg.html',1,'Bats']]],
  ['auctionsummarymsg',['AuctionSummaryMsg',['../classBats_1_1AuctionSummaryMsg.html#aea9c27dc1c4266f717d7fce9fce86b19',1,'Bats::AuctionSummaryMsg']]],
  ['auctionupdatemsg',['AuctionUpdateMsg',['../classBats_1_1AuctionUpdateMsg.html',1,'Bats']]],
  ['auctionupdatemsg',['AuctionUpdateMsg',['../classBats_1_1AuctionUpdateMsg.html#a7eba51abf03476a7d8bc884adff7184f',1,'Bats::AuctionUpdateMsg']]],
  ['autoacceptindicator',['AutoAcceptIndicator',['../namespaceFIXFields.html#a93b0ef7fb1b7d28d4ac59c9d618dc49a',1,'FIXFields']]],
  ['autoevent',['AutoEvent',['../classSystem_1_1AutoEvent.html#aa349e780cb23c699d49d920f3279f5ed',1,'System::AutoEvent']]],
  ['autoevent',['AutoEvent',['../classSystem_1_1AutoEvent.html',1,'System']]],
  ['autoptr',['AutoPtr',['../classUtils_1_1AutoPtr.html',1,'Utils']]],
  ['autoptr',['AutoPtr',['../classUtils_1_1AutoPtr.html#a33a4c085d9d1d7652a3a2a1d8db2148d',1,'Utils::AutoPtr::AutoPtr()'],['../classUtils_1_1AutoPtr.html#a00c4438b267791f88bd92aa7b12a42cc',1,'Utils::AutoPtr::AutoPtr(T *ptr, ReleaseMethod releaseMethod)'],['../classUtils_1_1AutoPtr.html#a0083acb0a2982d52e2f36d96b1cf7623',1,'Utils::AutoPtr::AutoPtr(AutoPtr&lt; T &gt; &amp;ptr)']]],
  ['autoptr2',['AutoPtr2',['../classUtils_1_1AutoPtr2.html#a6f7b78296c62015e45bbb8bf9929b27f',1,'Utils::AutoPtr2::AutoPtr2()'],['../classUtils_1_1AutoPtr2.html#a675fc0614a331f7edfe615d7473d49fc',1,'Utils::AutoPtr2::AutoPtr2(T *ptr)'],['../classUtils_1_1AutoPtr2.html#ae71a24e19f40c9be178b2e8afb4a9729',1,'Utils::AutoPtr2::AutoPtr2(AutoPtr2&lt; T, RELEASE_FUNC &gt; const &amp;ptr)']]],
  ['autoptr2',['AutoPtr2',['../classUtils_1_1AutoPtr2.html',1,'Utils']]],
  ['avgparpx',['AvgParPx',['../namespaceFIXFields.html#a5175ccda9f03b96b31d26ed8631fbf0b',1,'FIXFields']]],
  ['avgprxprecision',['AvgPrxPrecision',['../namespaceFIXFields.html#a570028813ab8ea3640e88571274178d7',1,'FIXFields']]],
  ['avgpx',['AvgPx',['../namespaceFIXFields.html#ae231197a72a6b50c61263d51c1a5b32e',1,'FIXFields']]],
  ['avgpxindicator',['AvgPxIndicator',['../namespaceFIXFields.html#acb30e3e0d45017c784b972eb18723424',1,'FIXFields']]],
  ['avgpxprecision',['AvgPxPrecision',['../namespaceFIXFields.html#a731bb3dc4dc8f48672e459dc180ec131',1,'FIXFields']]]
];
