// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

#include "stdafx.h"

#include <iostream>
#include <fstream>
#include <cassert>
#include <memory>

#include <B2BITS_V12.h>
#include <B2BITS_Semaphore.h>
#include <B2BITS_Exception.h>

using namespace std;
using namespace Utils;
using namespace System;
using namespace Engine;

const int hbi = 10;
const string sender = "FixMTSender";
const string target = "FixMTTarget";

/**
* InitiatorApplication.
*/
class InitiatorApplication : public Engine::Application
{
public:
    /**
    * Constructor.
    *
    * @param pSem Is posted when the Logout message is received.
    */
    InitiatorApplication( Semaphore* pLogoutSem, Semaphore* pEstablishedSem )
        : pLogoutSem_( pLogoutSem )
        , pEstablishedSem_( pEstablishedSem ) {
    }

    /** A call-back method to process incoming messages.  */
    virtual bool process( const Engine::FIXMessage& , const Engine::Session& ) {
        return true;
    }

    /**
    * This call-back method is called to notify about the Logon event.
    */
    virtual void onLogonEvent( const Engine::LogonEvent*, const Engine::Session& ) {
    }

    /**
    * This call-back method is called to notify about the Logout event.
    */
    virtual void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {
        pLogoutSem_->post();
    }

    /** This call-back method is called to notify about SequenceGap event. */
    virtual void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {}

    /** This call-back method is called to notify about SessionLevelReject event. */
    virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent*, const Engine::Session& ) {}

    virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ ) {
        cout << "Event onUnableToRouteMessage received - unable to route message into " << *event.getDeliverTo() << endl;
    }

    /**
    * This call-back method is called when a Heartbeat message (MsgType = 0)
    * with the TestReqID field (tag = 112) has been received.
    *
    * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    virtual void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when a Resend Request (MsgType = 2) has been received.
    */
    virtual void onResendRequestEvent( const Engine::ResendRequestEvent& /*event*/, const Engine::Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when the session has changed its state.
    */
    virtual void onNewStateEvent( const Engine::NewStateEvent& event, const Session& /*sn*/ ) {
        if( Engine::Session::ESTABLISHED == event.getNewState() ) {
            pEstablishedSem_->post();
        }
    }

    /**
    * This call-back method is called when an outgoing message is about to be resent
    * as a reply to the incoming ResendRequest message.
    *
    * If the method returns 'true' then the Engine resends the message to counterpart,
    * otherwise it sends the SequenceReset Gap Fill message.
    *
    * @param msg Outgoing message.
    * @param sn FIX session.
    *
    @return true if the message should be resent, otherwise false.
    */
    virtual bool onResend( const FIXMessage& /*msg*/, const Session& /*sn*/ ) {
        return true;
    }

    virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) {}

private:
    /// Is posted when the Logout message is received.
    Semaphore* pLogoutSem_;

    /// Is posted when session is ESTABLISHED.
    Semaphore* pEstablishedSem_;
};

/**
* Receives messages. Sends the Logout message after the receiving of the given number of messages.
*/
class AcceptorApplication : public Application
{
public:
    /**
    * Constructor.
    *
    * @param pSem Is posted when the Logout message is received.
    */
    AcceptorApplication( int expectedNumberOfMessages , System::Semaphore* pSem )
        : expectedNumberOfMessages_( expectedNumberOfMessages )
        , nMsgs_ ( 0 )
        , isFirstMsgReceived_( false )
        , startTime_(0)
        , pLogoutSem_( pSem )
    {}

    /** A call-back method to process incoming messages. */
    virtual bool process( const Engine::FIXMessage& , const Engine::Session& ) {
        return true;
    }

    /**
    * This call-back method is called to notify about Logon event.
    */
    virtual void onLogonEvent( const Engine::LogonEvent* , const Engine::Session& ) {
    }

    /** This call-back method is called to notify about Logout event. */
    virtual void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {
        pLogoutSem_->post();
    }

    /** This call-back method is called to notify about SequenceGap event.  */
    virtual void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {
        ;
    }

    /** * This call-back method is called to notify about SessionLevelReject event. */
    virtual void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent* , const Engine::Session& ) {
        ;
    }

    /**
    * This call-back method is called when a Heartbeat message (MsgType = 0)
    * with the TestReqID field (tag = 112) has been received.
    *
    * Usually such a message is sent in reply to a Test Request (MsgType = 1) message.
    */
    virtual void onHeartbeatWithTestReqIDEvent( const HeartbeatWithTestReqIDEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when a Resend Request (MsgType = 2) has been received.
    */
    virtual void onResendRequestEvent( const ResendRequestEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    /**
    * This call-back method is called when the session has changed its state.
    */
    virtual void onNewStateEvent( const NewStateEvent& /*event*/, const Session& /*sn*/ ) {
        ;
    }

    virtual void onUnableToRouteMessage( const UnableToRouteMessageEvent& event, const Session& /*sn*/ ) {
        cout << "Event onUnableToRouteMessage received - unable to reoute message into " << *event.getDeliverTo() << endl;
    }

    /**
    * This call-back method is called when an outgoing message is about to be resent
    * as a reply to the incoming ResendRequest message.
    *
    * If the method returns 'true' then the Engine resends the message to counterpart,
    * otherwise it sends the SequenceReset Gap Fill message.
    *
    * @param msg Outgoing message.
    * @param sn FIX session.
    *
    @return true if the message should be resent, otherwise false.
    */
    virtual bool onResend( const FIXMessage& /*msg*/, const Session& /*sn*/ ) {
        return true;
    }

    virtual void onMsgRejectEvent( const MsgRejectEvent* /*event*/, const Session& /*sn*/ ) {}

private:
    /// Expected number of incoming applicatin-level messages.
    int expectedNumberOfMessages_;

    /// Number of received applicatin-level messages.
    int nMsgs_ ;

    /// Whether first message was received or not.
    bool isFirstMsgReceived_;

    /// Time of the first received message.
    unsigned long startTime_;

    /// Is posted when the Logout message is received.
    System::Semaphore* pLogoutSem_;
};

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    // command line default parameters
    std::string properties = "engine.properties";
    std::string message = "order.msg";
    unsigned long volume = 100;
    std::string host = "localhost";
    int port = -1;

    if( 2 == argc && 0 == strcmp( "/?", argv[1] ) ) {
        cout << "Transient Session sample (c) B2BITS 2010." << endl
             << "Usage: [<engine_properties_file> [<file_with_message> [<message_amount> [<host> <port>]]]]" << endl
             << "Defaults: " << endl
             << "\tengine_properties_file = " << properties << std::endl
             << "\tfile_with_message = " << message << std::endl
             << "\tmessage_amount = " << volume << std::endl
             << "\thost = " << host << std::endl
             << "\tport = -1 (Engine::ListenPort)" << std::endl;

        return 1;
    }

    // Read input parameters
    int arg_i = 1;

    if( argc > arg_i ) {
        properties = argv[arg_i];
    }
    ++arg_i;

    if( argc > arg_i ) {
        message = argv[arg_i];
    }
    ++arg_i;

    if( argc > arg_i ) {
        volume = atoi( argv[arg_i] );
    }
    ++arg_i;

    if( argc > arg_i ) {
        host = argv[arg_i];
    }
    ++arg_i;

    if( argc > arg_i ) {
        port = atoi( argv[arg_i] );
    }
    ++arg_i;

    std::cout << "Parameters:" << std::endl
              << "\tengine_properties_file = " << properties << std::endl
              << "\tfile_with_message = " << message << std::endl
              << "\tmessage_amount = " << volume << std::endl
              << "\thost = " << host << std::endl
              << "\tport = " << port << std::endl;

    try {
        // Load the measuring message.
        ifstream msgFileNameIn( message.c_str() );

        if( ! msgFileNameIn ) {
            throw Exception( string( "Cannot open file with the measuring message '" ) + message + "'" );
        }

        string rawMsg;
        getline( msgFileNameIn, rawMsg );
        rawMsg.resize( rawMsg.find_last_of( '\x01' ) + 1 );

        FixEngine::init( properties );
        std::auto_ptr<FIXMessage> pFixMsg( FIXMsgProcessor::singleton()->parse( rawMsg ) );

        FIXVersion fixVersion = pFixMsg->getApplicationVersion();
        
        Semaphore initSem;
        Semaphore establishedSem;
        InitiatorApplication initApp( &initSem, &establishedSem );

        Semaphore acceptSem;
        AcceptorApplication acceptApp( volume, &acceptSem );

        cout << "Creates session acceptor. ";
        // Create a transient acceptor session.
        SessionExtraParameters params;
        params.storageType_ = transient_storageType;
        Session* acceptSsn = FixEngine::singleton()->createSession( &acceptApp, target, sender, fixVersion, &params );
        acceptSsn->connect(); // Connect as acceptor.
        cout << "Done." << endl;

        // Create a transient initiator session.
        cout << "Create session initiator. ";
        Session* initSsn = FixEngine::singleton()->createSession( &initApp, sender, target, fixVersion, &params );
        initSsn->connect( hbi, host, -1 == port ? FixEngine::singleton()->getListenPort() : port ); // Connect as Initiator.
        establishedSem.wait();
        cout << "Done." << endl;

        cout << "Send " << volume << " messages to acceptor. ";
        // Send the messages.
        for( unsigned long i = 0; i < volume; ++i ) {
            initSsn->put( pFixMsg.get() );
        }
        pFixMsg.reset();
        cout << "Done." << endl;

        // Disconnect and wait for the conforming Logout.
        cout << "All " << volume << " messages have been sent. Close session." << endl;

        // Wait until the conforming Logout message is received.
        initSsn->disconnect( "FixMT complete." );
        initSem.wait();

        acceptSsn->disconnect();
        acceptSem.wait();

        // Free resources.
        acceptSsn->registerApplication( NULL );
        initSsn->registerApplication( NULL );
        acceptSsn->release();
        initSsn->release();

        FixEngine::destroy();

        return 0;
    } catch( const Utils::Exception& ex ) {
        cout << "Error: " << ex.what();
        return 3;
    }
}
