// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CqgDefines.h
/// Contains CQG types definitions

#ifndef H_B2BITS_CQG_Defines_H
#define H_B2BITS_CQG_Defines_H

#include <string>
#include <B2BITS_String.h>

namespace Cqg
{

    enum RecoveryOptions
    {
        RO_USE_MARKET_RECOVERY = 0,
        RO_USE_NATURAL_REFRESH,
        RO_USE_ALL_METHODS,
        RO_USE_MARKET_START_NATURAL_REFRESH_RECOVERY,
        RO_USE_MARKET_TCP_AS_POSIBLE_RECOVERY
    };


    /// Feed ID
    /// A feed is a pair of UDP channels used to produce market 
    /// data messages for a set of instruments and/or a set of 
    /// instrument groups
    typedef std::string FeedID;

    /// FeedID argument
    typedef FeedID const& AFeedID;

    /// Sub-channel identifier. 
	/// Extract from Message Specification: 
	/// A preamble is sent before any FAST encoded message. It consists of 
	/// 5 non-FAST encoded bytes in Big Endian format, of which the first 
	/// 4 bytes represent the sequence number, followed by the 1-byte 
	/// sub-channel identifier, which is always 0x00 at the moment. 
    typedef System::u8 SubChannelID;

    /// SubChannelID argument
	typedef SubChannelID ASubChannelID;

	/// SecurityID value
    typedef System::u64 SecurityID;

    /// SecurityID argument
    typedef SecurityID ASecurityID;

    /// SecurityID value
    typedef std::string SecurityDescription;

    /// SecurityID argument
    typedef std::string const& ASecurityDescription;

    /// UDP Channel configuration
    struct UdpChannel {
        std::string mcastGroup_;
        int port_;
    }; // struct UdpChannel {

    /// UDP Channel configuration
    struct TcpAddress {
        std::string ipAddress_;
        int port_;
    }; // struct UdpChannel {

    /// Market data group
    struct MdGroup {
        /// ID of the feed
        FeedID id_;
        /// Parameters of the incremental channel
        UdpChannel incrementChannel_;
        /// Parameters of the snapshot channel
        UdpChannel snapshotChannel_;
        /// Parameters of the Historical replay channel
        TcpAddress replayChannel_;
    };

    enum RecoveryReason {
        RR_NA = 0,
        //Stop recovery reasons
        RR_PACKET_ORDER, ///< Market Data Handler processes UDP messages in parallel inside.
                         ///< sometimes one thread can process UDP message before another and pass it to the processing engine.
                         ///< It causes UDP message mix inside the library. Issue is not critical and handled inside library.
        RR_SNAPSHOT,
        RR_NATURAL_RECOVERY,
        RR_RESET,
        RR_UNSUBSCRIBE,
        RR_EMPTYDATA,
        //Start recovery reasons
        RR_HOLE_REASON,
        RR_OVERFLOW_REASON, ///< InstrumentListener processes messages too slow and causes message loss
        RR_USER_REASON,
        RR_FIRST_RECOVERY_REASON,
        RR_FIRST_DATA_REASON
    };

} // namespace Cqg {

#endif // H_B2BITS_CQG_Defines_H
