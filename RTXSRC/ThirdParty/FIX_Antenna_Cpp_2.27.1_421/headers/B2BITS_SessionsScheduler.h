// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2019.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_SessionsScheduler.h

#ifndef H_B2BITS_ENGINE_SessionsScheduler_H
#define H_B2BITS_ENGINE_SessionsScheduler_H

#include <B2BITS_V12_Defines.h>
#include <B2BITS_PubEngineDefines.h>
#include <B2BITS_TZTimeHelper.h>
#include <B2BITS_SessionId.h>
#include <B2BITS_Scheduler.h>
#include <B2BITS_SessionsController.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
// 'identifier' : class 'type' needs to have dll-interface to be used by clients of class 'type2'
#   pragma warning(disable: 4251)
#endif

namespace Engine
{
    /** Execution plan actions.*/
    enum ScheduleSessionActions
    {
        /** Ask sessions controller to start session.*/
        ScheduleSessionActions_Start,
        /** Ask sessions controller to connect session.*/
        ScheduleSessionActions_Connect,
        /** Ask sessions controller to disconnect session.*/
        ScheduleSessionActions_Disconnect,
        /** Ask sessions controller to stop session.*/
        ScheduleSessionActions_Stop
    };

    /**
    * Sessions schedule manager execution plan type.
    * Execution order is guaranteed to be the same as elements order inside ScheduleExecutionPlan.
    * User can add/remove/modify entries.
    * Sessions get to ScheduleExecutionPlan in the same order as they are returned by underlying SessionsController instance.
    * So the first session appears first in ScheduleExecutionPlan the second is second and so on.
    */
    typedef std::vector<std::pair<Engine::SessionId, ScheduleSessionActions> > ScheduleExecutionPlan;

    /** Abstract class(interface) that introduces API contract of sessions schedule manager.*/
    class V12_API SessionsScheduleManager
    {
    public:
        
        /**
        * Callback routine that is called when trade period begin event is triggered.
        * @param executionPlan - proposed execution plan as reaction on event.
        *        SessionsSchedule Schedule implementation populates execution plan with two actions for each session as a reaction on trade period begin event.
        *        The first is ScheduleSessionActions_Start and the second is ScheduleSessionActions_Connect. User can remove any action from or add to the plan if necessary.
        */
        virtual void onTradePeriodBegin( ScheduleExecutionPlan& executionPlan )
        {
            B2B_USE_ARG( executionPlan );
        };

        /**
        * Callback routine that is called when trade period end event is triggered.
        * @param executionPlan - proposed execution plan as reaction on event.
        *        SessionsSchedule Schedule implementation populates execution plan with two actions for each session as a reaction on trade period end event.
        *        The first is ScheduleSessionActions_Disconnect and the second is ScheduleSessionActions_Stop. User can remove any action from or add to the plan if necessary.
        */
        virtual void onTradePeriodEnd( ScheduleExecutionPlan& executionPlan )
        {
            B2B_USE_ARG( executionPlan );
        };

        /**
        * Callback routine that is called when an exception of class Utils::Exception or derived from it has been thrown during plan execution.
        * @param executionException - the exception thrown.
        * Return true to suppress the exception and continue plan execution or false to pass exception further and stop execution of the plan.
        */
        virtual bool onPlanExecutionException( const Utils::Exception& executionException )
        {
            B2B_USE_ARG( executionException );
            return false;
        };
    };

    /** Implementation of System::Schedule that allows to set SessionsScheduleManager as callbacks sink.*/
    class V12_API SessionsSchedule : public System::Schedule
    {
    public:
        /** Event tags used by SessionsSchedule.*/
        enum EventTags
        {
            /** Trade period begin event.*/
            SessionsSchedule_TradePeriodBeginEvent = 1,
            /** Trade period end event.*/
            SessionsSchedule_TradePeriodEndEvent
        };

    public:
        /**
        * Constructor
        * @param scheduleId - schedule id.
        *
        * @param timelinePtr - events generator instance pointer.
        *
        * @param sessionsControllerPtr - sessions controller instance pointer.
        *
        * @param pScheduleManager - pointer to SessionsScheduleManager instance to use as callbacks sink. If NULL no callbacks are possible. Execution plan is executed as is(as generated internally).
        */
        SessionsSchedule( const std::string& scheduleId, const System::ScheduleTimelinePtr& timelinePtr, const SessionsControllerPtr& sessionsControllerPtr, SessionsScheduleManager* pScheduleManager = NULL );
        virtual ~SessionsSchedule();

        /**
        * Attachs SessionsScheduleManager instance as callback sink.
        * @param pScheduleManager - pointer to SessionsScheduleManager instance to serve as callback sink. Pass NULL to detach previously attached instance.
        */
        void attachScheduleManager( SessionsScheduleManager* pScheduleManager );

        /** Returns sessions controller instance used. */
        SessionsControllerPtr getSessionsController() const;

        /** Sets sessions controller instance used. 
        *
        * @param sessionsControllerPtr - sessions controller instance pointer.
        *
        */
        void setSessionsController( const SessionsControllerPtr& sessionsControllerPtr );

        /** Implements System::Schedule API contract callback routine. */
        virtual void onTimeEvent( const System::SchedulerTimeEvent& hitEvent, const Engine::TZTimeHelper::UTCTimestamp& timestamp );
    private:
        class Impl;
        Impl* pImpl_;
    };
    typedef Utils::ReferenceCounterSharedPtr<SessionsSchedule> SessionsSchedulePtr;

    /** Cron expression based schedule events generator implementation parameters holder class.*/
    struct V12_API CronSessionsScheduleParameters
    {
        CronSessionsScheduleParameters( const std::string& tradePeriodBegin = std::string(), const std::string& tradePeriodEnd = std::string(), const std::string& dayOffs = std::string(), const std::string& timezone = std::string(), bool lateExecution = false, const TimezoneConverterPtr& pTimezoneConverter = TimezoneConverterPtr() )
            : tradePeriodBegin( tradePeriodBegin )
            , tradePeriodEnd( tradePeriodEnd )
            , dayOffs( dayOffs )
            , timezone( timezone )
            , pTimezoneConverter( pTimezoneConverter )
            , lateExecution( lateExecution )
        {
        }

        /** Cron expression of trade period begin events.*/
        std::string tradePeriodBegin;

        /** Cron expression of trade period end events.*/
        std::string tradePeriodEnd;

        /** Cron expression of day offs. Events that match dayOffs expression have dontNotify_ flag set to true. @see System::SchedulerTimeEvent */
        std::string dayOffs;

        /** Timezone name as accepted by instance of TimezoneConverter passed with pTimezoneConverter below. */
        std::string timezone;

        /** 
        * Timezone converter pointer @see Engine::TimezoneConverter. 
        * engine.properties configured instance can be obtained with FixEngine::singleton()->getProperties()->getTimezoneConverter() call.
        * If this parameter represents empty pointer no conversion can be made. So all times are in UTC in this case.
        */
        TimezoneConverterPtr pTimezoneConverter;

        /**
        * Flag that determines whatever to do late trade begin event execution or not.
        * If this flag is true generator checks if start(first call to getNextEvent()) is made inside trade period (between trade period begin and trade period end events)
        * and generates trade period begin event if so with eventTime_ set to current time. This forces scheduler to execute this event immediately.
        * If this flag is false no such check is made at start.
        */
        bool lateExecution;
    };

    /** Cron expression based schedule events generator implementation.*/
    class V12_API CronSessionScheduleTimeline : public System::ScheduleTimeline
    {
    public:
        /**
        * Constructor
        * @param params - configuration parameters. @see CronSessionsScheduleParameters.
        */
        CronSessionScheduleTimeline( const CronSessionsScheduleParameters& params );
        virtual ~CronSessionScheduleTimeline();

        /** Implements System::ScheduleTimeline API contract routine. */
        virtual System::SchedulerTimeEvent getNextEvent( const Engine::TZTimeHelper::UTCTimestamp& now );

        /** Implements System::ScheduleTimeline API contract routine. */
        virtual System::SchedulerTimeEvent getPrevEvent( const Engine::TZTimeHelper::UTCTimestamp& now ) const;

        /** Returns configuration parameters. */
        CronSessionsScheduleParameters getParameters() const;

    protected:
        const CronSessionsScheduleParameters params_;

    private:
        class Impl;
        Impl* pImpl_;
    };
    typedef Utils::ReferenceCounterSharedPtr<CronSessionScheduleTimeline> CronSessionScheduleTimelinePtr;

} // namespace Engine

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif // H_B2BITS_ENGINE_SessionsScheduler_H
