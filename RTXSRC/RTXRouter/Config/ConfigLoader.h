//
// Created by schen on 10/4/2020.
//

#ifndef RTXROUTER_CONFIGLOADER_H
#define RTXROUTER_CONFIGLOADER_H

class Logger;
class Config;
class LoaderBase;
class ConfigGlobalLoader;
class ConfigRouterLoader;
class ConfigSecIdfnsLoader;
class ConfigUserLoader;
class ConfigUserLoader;

class ConfigLoader {
public:
    ConfigLoader(){};
    explicit ConfigLoader(Logger* logger, Config* config );
    virtual ~ConfigLoader();

    bool InitConfigLoader();
    bool LoadConfigFile( const std::string& filename);

    void FillLoader()
    {
        loaders["Global"] = (m_globalLoader = new ConfigGlobalLoader(m_loggerSys));
        loaders["Routes"] = (m_routerLoader = new ConfigRouterLoader(m_loggerSys));
        loaders["SecIdfns"] = (m_secIdfnsLoader = new ConfigSecIdfnsLoader(m_loggerSys));
        loaders["Users"] = (m_usersLoader = new ConfigUserLoader(m_loggerSys));
    }

    Logger* m_loggerSys;        //log to system.log file
    Config* m_config;

    ConfigGlobalLoader* m_globalLoader;
    ConfigRouterLoader* m_routerLoader;
    ConfigSecIdfnsLoader* m_secIdfnsLoader;
    ConfigUserLoader* m_usersLoader;

    std::unordered_map <std::string, LoaderBase* > loaders ;
};

#endif //RTXROUTER_CONFIGLOADER_H
