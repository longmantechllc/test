// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file main.cpp Small CQG FIX/FAST connection sample
/// @author Dmytro Ovdiienko [dmytro_ovdiienko@epam.com]
/// @date 2011-05-25

#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

#include <B2BITS_Mutex.h>
#include <B2BITS_FixEngine.h>
#include <B2BITS_SystemDefines.h>

#include "CqgClient.h"


enum {
    CHOISE_SUBSCRIBEALL     = 0,
    CHOICE_SUBSCRIBE        = 1,
    CHOICE_UNSUBSCRIBE      = 2,
    CHOICE_UNSUBSCRIBEALL   = 3,
    CHOICE_PRINT            = 4,
    CHOICE_STOP             = 5
};

static void printMenu();

typedef std::auto_ptr<CqgClient> CqgClientPtr;

System::Mutex g_mutex;  /// Used to sync output to console.

void printUsage()
{
    std::cout << "USAGE: [<ListenInterfaceIP>] [<sender>] [<login>] [<password>] [<SDS list>]" << std::endl;
    std::cout << "SDS list := [channel_id_1:host_1:port_1 [channel_id_2:host_2:port_2 [...]]]" << std::endl;
    std::cout << std::endl;
    std::cout << "Sample of command line: " << std::endl;
    std::cout << "CQGMarketDataClient 10.1.0.166 test test test 13:10.1.0.71:2222" << std::endl;
    std::cout << std::endl;
}

int main( int argc, char const* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    std::cout << "CQG Market Data client" << std::endl;

    if( 1 == argc ) {
        printUsage();
    }

    if( 2 == argc && ( 0 == strcmp( argv[ 1 ], "/?" ) || 0 == strcmp( argv[ 1 ], "-h" ) ) ) {
        printUsage();
        return 0;
    }

    try {
        Engine::FixEngine::init();
        CqgClientPtr app( new CqgClient( argc, argv ) );


        for( ;; ) {
            printMenu();
    
            std::string symbol;
            std::string line;

            std::getline( std::cin, line );
            int const choice = atoi( line.c_str() );

            switch( choice ) {
            case CHOISE_SUBSCRIBEALL:
                g_mutex.lock();
                std::cout << "Subscribe to all instruments";
                g_mutex.unlock();
                app->subscribeAll();
                std::cout << "[NOTE] All incoming messages will be forwarded to the '_log' file." << std::endl;
                break;
            case CHOICE_SUBSCRIBE:
                g_mutex.lock();
                std::cout << "Subscribe: >> Enter Symbol: ";
                g_mutex.unlock();
                std::getline( std::cin, symbol );
                app->subscribe( symbol );
                std::cout << "[NOTE] All incoming messages will be forwarded to the '_log' file." << std::endl;
                break;
            case CHOICE_UNSUBSCRIBE:
                g_mutex.lock();
                std::cout << "Unsubscribe: >> Enter Symbol: ";
                g_mutex.unlock();
                std::getline( std::cin, symbol );
                app->unsubscribe( symbol );
                break;
            case CHOICE_UNSUBSCRIBEALL:
                app->unsubscribeAll();
                break;
            case CHOICE_PRINT:
                g_mutex.lock();
                std::cout << "Print book: >> Enter Symbol: ";
                g_mutex.unlock();
                std::getline( std::cin, symbol );
                g_mutex.lock();
                app->print( symbol );
                g_mutex.unlock();
                break;
            case CHOICE_STOP:
                app.reset();
                Engine::FixEngine::destroy();
                return EXIT_SUCCESS;
            default:
                std::cout << "Unrecognized command: " << line << ". Please repeat again." << std::endl;
                break;
            }
        }
    } catch( std::exception const& e ) {
        g_mutex.lock();
        std::cerr << "ERROR: " << e.what() << std::endl;
        g_mutex.unlock();
        return EXIT_FAILURE;
    }
}

void printMenu()
{
    g_mutex.lock();
    std::cout << "MDApplication sample menu:" << std::endl;
    std::cout << CHOISE_SUBSCRIBEALL << ". Subscribe to all instruments" << std::endl;
    std::cout << CHOICE_SUBSCRIBE << ". Subscribe to instrument" << std::endl;
    std::cout << CHOICE_UNSUBSCRIBE << ". Unsubscribe from instrument" << std::endl;
    std::cout << CHOICE_UNSUBSCRIBEALL << ". Unsubscribe all" << std::endl;
    std::cout << CHOICE_PRINT << ". Print book" << std::endl;
    std::cout << CHOICE_STOP << ". Exit" << std::endl;
    std::cout << std::endl;
    std::cout << "What is your choice? ";
    g_mutex.unlock();
}
