#pragma once

#include <string_view>
#include "json_dto/pub.hpp"

namespace Crypto::ftx {
static constexpr std::string_view INVALID_STRING_VIEW = "N/A";

namespace WS {

    enum class ChannelType : std::uint8_t {
        INVALID_CHANNEL_TYPE = 0,
        orderbook,
        trades,
        ticker
    };
    
    enum class RequestType : std::uint8_t {
        INVALID_REQUEST_TYPE = 0,
        subscribe,
        unsubscribe,
        ping,
        login
    };

    enum class ResponseType : std::uint8_t {
        INVALID_RESPONSE_TYPE = 0,
        error,
        subscribed,
        unsubscribed,
        info,
        partial,
        update,
        pong
    };
}

}  // namespace Crypto::ftx

namespace json_dto {

#define read_json_for_enum(enum_type) \
template<> \
void read_json_value(enum_type &v, const rapidjson::Value &object)

read_json_for_enum(Crypto::ftx::WS::ChannelType);
read_json_for_enum(Crypto::ftx::WS::RequestType);
read_json_for_enum(Crypto::ftx::WS::ResponseType);

#undef read_json_for_enum
}  // namespace json_dto