/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#ifndef H_B2BITS_Engine_Samples_CTGW_Helper_H
#define H_B2BITS_Engine_Samples_CTGW_Helper_H

#include <string>
#include <B2BITS_Mutex.h>

#include "Properties.h"
#include "ApplicationProperties.h"

/// Class Helper is a collection of useful routines.
class Helper
{
public:
    /// Returns FIXVersion from it's string representation.
    static Engine::FIXVersion string2Version( const std::string& str );

    /// Retrives required arguments offset from application arguments array.
    static void checkOptionalArgument( char** arguments, int quantity );

    /// Retrives required arguments offset from application arguments array.
    static int findRequiredArguments( char** arguments, int quantity );

    /// Finds optional argument value.
    static char* findOptionalArgument( const char* name,
                                       char** arguments, int quantity );

    /// Loads file content into std::string.
    static std::string loadString( const std::string& fileName );

    /// Writes line to console.
    static void writeLineToConsole( const std::string& message = std::string() );

    /// Writes line to stderr.
    static void writeErrorLine( const std::string& message = std::string() );

    static void writeUsageInfo( const std::string& applicationName );

    static void parseCommandLine( const Aux::Properties& prop, ApplicationProperties* params );

    /// screen output synchronizer.
    static System::Mutex outputLock_;

private:
    /// Prohibits class instances appearance.
    Helper();
};

/// Application level exception.
class AppException : public std::exception
{

public:
    /// Remembers reason of exception.
    explicit AppException( const std::string& reason ) throw()
        : reason_( reason ) {}

    /// Destructor to satisfy C++ compliance.
    virtual ~AppException() throw() {}

    /// Reimplements std::exception interface.
    virtual const char* what() const throw() {
        return reason_.c_str();
    }

private:
    std::string reason_;
};

#endif // H_B2BITS_Engine_Samples_CTGW_Helper_H
