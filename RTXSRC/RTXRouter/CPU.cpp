//
// Created by schen on 2/22/2021.
//
#include <vector>
#include <string>
#include <unordered_map>
#include <memory>
#include <functional>
#include <unordered_set>

#include "RTXUtils.h"
#include "Logger.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/Config.h"
#include "OrderLogic/MarketData/SecMasterBox.h"
#include "OrderLogic/MarketData/RetailMarket.h"

#include "xTimer.h"
#include "TimerScheduler.h"

#include "../include/RTXOrderIO.h"
#include "../include/MktDataIO.h"

#include "../include/App.h"
#include "CPU.h"

using namespace std::placeholders;  // for _1, _2, _3...

CPU::CPU(App* app)
    :m_app(app),
     m_loggerSys(app->m_loggerSys),
     m_RTXOrderIO(app->m_RTXOrderIO)
{
}

CPU::~CPU()
{
    DESTROY(m_timerScheduler);
    DESTROY(m_secMasterBox);
    DESTROY(m_retailMarketBox);
}

void CPU::InitCPU()
{
    InitTimerScheduler();
    InitSecMaster();
    InitRetailMarketBox();

    m_loggerSys->logInfo("Market open time %s.", m_app->m_config->GlobalConfig()->MarketOpenStr().c_str());
    m_loggerSys->logInfo("Market close time %s.", m_app->m_config->GlobalConfig()->MarketCloseStr().c_str());
    m_marketStartTimeMssm = m_app->m_config->GlobalConfig()->MarketOpenTime();
    m_marketEndTimeMssm = m_app->m_config->GlobalConfig()->MarketCloseTime();
    m_postMarketEndTimeMssm = m_app->m_config->GlobalConfig()->PostMarketCloseTime();

//    m_swpStartTimeMssm = m_app->m_config->GlobalConfig()->SWPStartTime();
//    m_swpEndTimeMssm = m_app->m_config->GlobalConfig()->SWPEndTime();

    m_utcLocalTimeDiff = RTXUtils::TimeDiffLocalvsUTC();

    auto now = RTXUtils::NowMssm();
    if(now < m_marketStartTimeMssm)
        StartMarketOpenTimer();
    else if(now > m_marketStartTimeMssm && now < m_marketEndTimeMssm)
        StartMarketEndTimer();
    else if(now < m_postMarketEndTimeMssm)
        StartPostMarketEndTimer();
    else
        m_postMarketEnded = true;

    if(m_app->m_config->GlobalConfig()->ExtraLogging()) {
        m_loggerSys->logInfo("Start Test Timer, duration 1s.");
        StartTestTimer();
    }
}

void CPU::InitTimerScheduler()
{
    int threadNo = m_app->m_config->GlobalConfig()->ThreadNumber();
    m_timerScheduler = new TimerScheduler(threadNo, m_app->m_loggerSys);
}

void CPU::InitSecMaster()
{
    m_secMasterBox = new SecMasterBox(m_app);
    m_secMasterBox->InitSecMasterBox();
}

void CPU::InitRetailMarketBox()
{
    m_retailMarketBox = new RetailMarketBox(m_app);
    m_retailMarketBox->InitRetailMarketBox();
}

void CPU::StartMarketOpenTimer()
{
    auto now = RTXUtils::NowMssm();
    if(now >= m_marketStartTimeMssm)
    {
        if(now < m_marketEndTimeMssm) {
            m_loggerSys->logInfo("Market is already opened.");
            m_marketSession = MarketSession::In_Market;
            StartMarketEndTimer();
        }
        else if(now < m_postMarketEndTimeMssm)
        {
            m_loggerSys->logInfo("Market is already closed.");
            m_marketSession = MarketSession::Post_Market;
        }
        else
            m_postMarketEnded = true;

        return;
    }
    auto diff = m_marketStartTimeMssm - now;
    m_loggerSys->logInfo("Start Market Open Timer. duration:%d ms", diff);

    m_marketOpenTimer = new xTimer(m_timerScheduler);
    m_marketOpenTimer->StartTimer(diff, std::bind(&CPU::FireMarketOpenTimer, this, _1));
}

void CPU::FireMarketOpenTimer(xTimer& timer)
{
    m_loggerSys->logInfo("Market is opened.");
    m_marketSession = MarketSession::In_Market;
    StartMarketEndTimer();
}

void CPU::StartMarketEndTimer()
{
    if(m_marketEndTimer)
        return;
    auto now = RTXUtils::NowMssm();
    if(now < m_marketEndTimeMssm) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
        now = RTXUtils::NowMssm();
    }
    if(now >= m_marketEndTimeMssm && now < m_postMarketEndTimeMssm)
    {
        m_loggerSys->logInfo("Market is already closed.");
        m_marketSession = MarketSession::Post_Market;
        StartPostMarketEndTimer();
        return;
    }
    else if(now >= m_postMarketEndTimeMssm)
    {
        m_postMarketEnded = true;
        return;
    }
    m_marketSession = MarketSession::In_Market;

    auto diff = m_marketEndTimeMssm - now;
    m_loggerSys->logInfo("Start Market Close Timer. duration:%d ms", diff);
    m_marketEndTimer = new xTimer(m_timerScheduler);
    m_marketEndTimer->StartTimer(diff, std::bind(&CPU::FireMarketEndTimer, this, _1));
}

void CPU::FireMarketEndTimer(xTimer& timer)
{
    m_loggerSys->logInfo("Market is closed.");
    auto now = RTXUtils::NowMssm();
    if(now >= m_postMarketEndTimeMssm)
    {
        m_postMarketEnded = true;
        return;
    }
    m_marketSession = MarketSession::Post_Market;
    StartPostMarketEndTimer();
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
    m_RTXOrderIO->EndOfDayCancelOrders();
}

void CPU::StartPostMarketEndTimer()
{
    if(m_postMarketEndTimer)
        return;
    auto now = RTXUtils::NowMssm();
    auto diff = m_postMarketEndTimeMssm - now;
    if(diff <= 0)
    {
        m_postMarketEnded = true;
        return;
    }
    if(now >= m_marketEndTimeMssm)
        m_marketSession = MarketSession::Post_Market;
    m_loggerSys->logInfo("Start post Market Close Timer. duration:%d ms", diff);
    m_postMarketEndTimer = new xTimer(m_timerScheduler);
    m_postMarketEndTimer->StartTimer(diff, std::bind(&CPU::FirePostMarketEndTimer, this, _1));
}

void CPU::FirePostMarketEndTimer(xTimer& timer)
{
    m_postMarketEnded = true;
    m_marketClosed = true;
    m_loggerSys->logInfo("Post Market is closed.");
    m_RTXOrderIO->EndOfDayCancelOrders(true);
}

void CPU::StartTestTimer()
{
    m_erCnt = 0;
    m_testTimer = new xTimer(m_timerScheduler);
    m_testTimer->StartTimer(1000, std::bind(&CPU::FireTestTimer, this, _1));
}
void CPU::FireTestTimer(xTimer& timer)
{
    int p = m_oId;
    int c = m_coNumber;
    int e = m_erCnt;

    m_loggerSys->logInfo("Parent #%d, Child #%d, ERCnt:%d.", p, c, e);
    std::this_thread::sleep_for(std::chrono::milliseconds(100));
    StartTestTimer();
}
