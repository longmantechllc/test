//
// Created by schen on 4/1/2021.
//

#ifndef RTXROUTER_REROUTE_H
#define RTXROUTER_REROUTE_H

class ExecutionReport;
class RTXOrder;

using namespace std;

class Reroute : public IOrderLogicBase
{
public:
    Reroute() =default;
    Reroute(RTXOrder* order);
    ~Reroute() override;
    bool InitOrder() override;

    bool ProcessOrder() override;
    virtual bool SendChildOrder(std::shared_ptr<Order> order) override;

    bool ProcessChildOrderER(const ExecutionReport* er, std::shared_ptr<ChildOrder>& child) override;
    bool ProcessMktData() override;

private:
    bool IsVenueEligible(const std::string& routeName);
    bool IsEligible();

    const std::vector<std::string> m_destinationList;
};

#endif //RTXROUTER_REROUTE_H
