/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstddef>
#include <cassert>
#include <memory>
#include <limits>
#include <cmath>
#include <algorithm>

#include <B2BITS_SystemDefines.h>
#include <B2BITS_AutoEvent.h>
#include <B2BITS_FixEngine.h>
#include <B2BITS_FAProperties.h>
#include <B2BITS_Session.h>
#include <B2BITS_Application.h>
#include <B2BITS_PreparedMessage.h>
#include <B2BITS_SessionParameters.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_FIXMsgProcessor.h>

#include "B2BITS_HRTimer.h"
#include "B2BITS_Thread.h"

#include "B2BITS_StringUtils.h"

// SenderCompID
static std::string const SENDER = "sender";
// TargetCompID
static std::string const TARGET = "target";
// Raw FIX message used for measurement
static std::string const RAW_MSG = "8=FIX.4.2""\001""9=1""\001""35=D""\001""49=BLP""\001""56=SCHB""\001""34=01""\001""50=30737""\001""52=20000809-20:20:50""\001""11=90001008""\001""1=10030003""\001""21=2""\001""55=TESTA""\001""54=1""\001""38=4000""\001""40=2""\001""59=0""\001""44=30""\001""47=I""\001""60=20000809-18:20:32""\001""10=000""\001";

// Number of experiments
#define BENCHMARK_VOLUME_PROPERTY "benchmark.volume"
#ifdef NDEBUG
static size_t TEST_COUNT = 1000000;
#else // NDEBUG
static size_t TEST_COUNT = 1000;
#endif // NDEBUG

static size_t const MsgSeqNumFieldSize = 7;

Engine::MessageStorageType storgeType = Engine::default_storageType;

// Application is registered for events as session-acceptor observer
class LatencyMeasurementApplication: public Engine::Application
{
private:
    // Eperiment index
    size_t testIdx_;

    // Array of times before Session::put is called
    std::vector<System::u64> putTime_;
    // Array of times before byte array is put to socket
    std::vector<System::u64>  sendTime_;
    // Array of times after byte arrage is received from socket
    std::vector<System::u64>  receiveTime_;
    // Array of times when message is delivered to the user application
    std::vector<System::u64>  processTime_;


    // Session to send messages to
    Engine::Session* initiator_;
    // Synchronization event
    System::AutoEvent msgIsReceivedEvent_;

public:

    // Basic initialization.
    // Must be called before starting tests series (e.g. in case of multiple runs)
    // @param initiator Session to send messages to
    void init( Engine::Session* initiator ) {
        // Make sure session is not NULL
        assert( NULL == initiator_ );

        // Initialize variables, reset to initial values
        initiator_ = initiator;
        testIdx_ = 0;

        putTime_.resize(TEST_COUNT + 1);
        sendTime_.resize(TEST_COUNT + 1);
        receiveTime_.resize(TEST_COUNT + 1);
        processTime_.resize(TEST_COUNT + 1);
    }

    void recordTime() {
        // Record current time as the start time for sending latency
        putTime_[ testIdx_ ] = System::HRTimer::value();
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::FIXMessage* msg ) {
        // Make sure session is not NULL
        assert( NULL != initiator_ );

        // Put message to the session
        initiator_->put( msg );
    }

    /**
    * Sets static session level fields in the message and reserves space for volatile session level fields.
    * @param[out] pMsg Pointer to the FIXMessage to store field values
    * @param[in] msgSeqNumSize Number of characters for MsgSeqNum field to reserve.
    * @param[in] options Extra parameters. @see Engine::PutMessageOptions.
    */
    void inflate( Engine::FIXMessage* msg ) const {
        Engine::PutMessageOptions putOptions;
        putOptions.overrideSendingTime_ = false;
        initiator_->inflate( msg, MsgSeqNumFieldSize, &putOptions );
    }

    // Send message to session-initiator
    // @param msg Message to be sent
    void sendMessage( Engine::PreparedMessage* msg ) {
        // Make sure session is not NULL
        assert( NULL != initiator_ );

        // Put message to the session
        initiator_->put( msg );
    }

    // Wait untill message sent to the session-initiator is received by session-acceptor
    void waitForReceivedMessage() {
        //while( !msgIsReceivedEvent_.wait( 0 ) )
        //    ;

        msgIsReceivedEvent_.wait();

        // Increase experiments counter
        ++testIdx_;
    }

    double round( double value ) {
        return floor( value + 0.5 );
    }

    // Calculate results and print them
    // @param file stream to print results to
    void printResult() {
        char const* file_name = "latency.csv";
        std::ofstream file;
        file.open( file_name );

        // System::HRTimer::frequency() is number of operations per
        // second. To get frequency in nanoseconds,
        // devide this value to 10^9
        double ns_frequency = System::HRTimer::frequency() / 1000000000.0;

        System::u64 send_min = std::numeric_limits<System::u64>::max();
        System::u64 send_max = std::numeric_limits<System::u64>::min();
        System::u64 send_summ = 0;

        System::u64 recv_min = std::numeric_limits<System::u64>::max();
        System::u64 recv_max = std::numeric_limits<System::u64>::min();
        System::u64 recv_summ = 0;

        System::u64 tot_min = std::numeric_limits<System::u64>::max();
        System::u64 tot_max = std::numeric_limits<System::u64>::min();
        System::u64 tot_summ = 0;

        file << "Put\tRecv\tPutToProcess" << std::endl;

        for( size_t i = 0; i < TEST_COUNT; ++i ) {
            System::u64 send_latency = ( System::u64 ) round( ( sendTime_[ i ] - putTime_[ i ] ) / ns_frequency );
            System::u64 recv_latency = ( System::u64 ) round( ( processTime_[ i ] - receiveTime_[ i ] ) / ns_frequency );
            System::u64 total_latency = ( System::u64 ) round( ( processTime_[ i ] - putTime_[ i ] ) / ns_frequency );

            file << send_latency << '\t' << recv_latency << '\t' << total_latency << std::endl;

            send_min = std::min( send_min, send_latency );
            recv_min = std::min( recv_min, recv_latency );
            tot_min = std::min( tot_min, total_latency );

            send_max = std::max( send_max, send_latency );
            recv_max = std::max( recv_max, recv_latency );
            tot_max = std::max( tot_max, total_latency );

            send_summ += send_latency;
            recv_summ += recv_latency;
            tot_summ += total_latency;
        }

        std::cout << "Send latency (nanoseconds): " << std::endl;
        std::cout << "\tMIN: " << send_min << std::endl;
        std::cout << "\tMAX: " << send_max << std::endl;
        std::cout << "\tAVG: " << round( ( double )send_summ / TEST_COUNT ) << std::endl;

        std::cout << "Receive latency (nanoseconds): " << std::endl;
        std::cout << "\tMIN: " << recv_min << std::endl;
        std::cout << "\tMAX: " << recv_max << std::endl;
        std::cout << "\tAVG: " << round( ( double )recv_summ / TEST_COUNT ) << std::endl;

        std::cout << "Total latency (nanoseconds): " << std::endl;
        std::cout << "\tMIN: " << tot_min << std::endl;
        std::cout << "\tMAX: " << tot_max << std::endl;
        std::cout << "\tAVG: " << round( ( double )tot_summ / TEST_COUNT ) << std::endl;

        std::cout << std::endl << "Time samples have been saved to " << file_name << std::endl;
    }

public:

    // Constructor
    LatencyMeasurementApplication()
        : testIdx_( 0 )
        , initiator_( NULL ) 
    {
    }

private:


    /**
     * A call-back method to process incoming messages.
     * If the application can not process the given message, the FIX Engine will try to deliver it later
     * according to "Delayed Processing Algorithm".
     *
     * @warning This method should complete as quickly as possible. Do not perform time-consuming tasks inside it.
     *
     * @param msg the incoming message.
     * @param sn The corresponding FIX session.
     *
     * @return true if the application can process the given message, otherwise false.
     */
    bool process( const Engine::FIXMessage&, const Engine::Session& ) {
        // Record current time as the end time for receiving latency
        processTime_[ testIdx_ ] = System::HRTimer::value();
        // Norify sending thread that the  message is received, time is recorded
        // so the show can go on
        msgIsReceivedEvent_.set();

        return true;
    }

    /**
     * Fired just before message is sent.
     * @param[in] msgSeqNum Sequence number of the sent message.
     * @param[in] sn Reference to FIX session related to the event.
     */
    void onBeforeMessageIsSent( int, const Engine::Session& , size_t ) {
        // Record current time as the end time for sending latency
        sendTime_[ testIdx_ ] = System::HRTimer::value();
    }

    /**
     * Fired just after message is received from socket.
     * @param[in] sn Reference to FIX session related to the event.
     */
    void onAfterMessageIsReceived( char const*, size_t, const Engine::Session& ) {
        // Record current time as the end time for receiving latency
        receiveTime_[ testIdx_ ] = System::HRTimer::value();
    }

    void onLogonEvent( const Engine::LogonEvent*, const Engine::Session& ) {}
    void onLogoutEvent( const Engine::LogoutEvent*, const Engine::Session& ) {}
    void onSequenceGapEvent( const Engine::SequenceGapEvent*, const Engine::Session& ) {}
    void onSessionLevelRejectEvent( const Engine::SessionLevelRejectEvent*, const Engine::Session& ) {}
    void onMsgRejectEvent( const Engine::MsgRejectEvent*, const Engine::Session& ) {}
    void onHeartbeatWithTestReqIDEvent( const Engine::HeartbeatWithTestReqIDEvent&, const Engine::Session& ) {}
    void onResendRequestEvent( const Engine::ResendRequestEvent&, const Engine::Session& ) {}
    void onNewStateEvent( const Engine::NewStateEvent&, const Engine::Session& ) {}
    void onUnableToRouteMessage( const Engine::UnableToRouteMessageEvent&, const Engine::Session& ) {}
    bool onResend( const Engine::FIXMessage&, const Engine::Session& ) {
        return false;
    }
};

// Main measurement routine
void run( int argc, char* argv[] )
{
    if( argc < 2 || ( strcmp( argv[1], "transient" ) != 0 &&
                       strcmp( argv[1], "persistent" ) != 0 &&
                       strcmp( argv[1], "persistentMM" ) != 0 &&
                       strcmp( argv[1], "splitPersistent" ) != 0 &&
                       strcmp( argv[1], "null" ) != 0 && 
                       strcmp( argv[1], "default" ) != 0 ) ) {
        std::cout << "Usage: latency [default|null|transient|persistent|splitPersistent|persistentMM] <useSSL> <aff NUMBER>" << std::endl;
    }

    if( argc > 1 ) {
        storgeType = Engine::Session::stringToMessageStorageType( argv[1] );
    }

    bool useSSL = false;
    System::u64 aff = 0;

    for (int i = 2; i < argc; ++i) {
    	if(strcmp( argv[i], "useSSL" ) == 0)
    	    useSSL = true;
    	if(strcmp( argv[i], "aff" ) == 0)
			if (++i < argc) {
				aff = Utils::strtou64(argv[i], strlen(argv[i]));
			}
    }

    if(aff != 0) {
    	std::cout << "Affinity of main thread is " << aff << std::endl;
    	System::Thread::setCurrentThreadAffinity(aff);
    }

    // Initialize B2BITS FIX engine
    Engine::FixEngine::init();

    if ( Engine::FixEngine::singleton()->getProperties()->existsProperty(BENCHMARK_VOLUME_PROPERTY) )
        TEST_COUNT = Engine::FixEngine::singleton()->getProperties()->getIntegerProperty(BENCHMARK_VOLUME_PROPERTY);

    // Create an instance of user applciation
    std::auto_ptr<LatencyMeasurementApplication> application( new LatencyMeasurementApplication() );

    // Settings
    // For convenience settings are moved to engine.proeprties file
    Engine::SessionExtraParameters a_params;
    a_params.storageType_ = storgeType;
  //  a_params.recvCpuAffinity_ = 0x2;

    // For convenience settings are moved to engine.proeprties file
    Engine::SessionExtraParameters i_params;
    i_params.storageType_ = storgeType;
  //  i_params.sendCpuAffinity_ = 0x1;

    int port = Engine::FixEngine::singleton()->getListenPort();
    
    if(useSSL)
    {
        std::cout << "Using SSL." << std::endl;
        i_params.sslContext_ = System::SSLClientContext();
        port += 1;

    }

    std::cout << "Using " << Engine::Session::messageStorageTypeToString( storgeType ) << " message storage." << std::endl;
    std::cout << "Create session..." << std::endl;

    // Create session-initiator and session-acceptor
    Engine::Session* initiator = Engine::FixEngine::singleton()->createSession(
                                     application.get(), SENDER, TARGET, Engine::FIX42, &i_params );
    Engine::Session* acceptor = Engine::FixEngine::singleton()->createSession(
                                    application.get(), TARGET, SENDER, Engine::FIX42, &a_params );

    application->init( initiator );

    std::cout << "Connecting..." << std::endl;
    // Establish connection
    acceptor->connect();
    initiator->connect( 0, "localhost", port );

    // Wait while connection is not established
    while( initiator->getState() != Engine::Session::ESTABLISHED || acceptor->getState() != Engine::Session::ESTABLISHED ) {
        System::Thread::sleep( 1 );
    }

	// Create object message
    Engine::FIXMessage* msg = Engine::FIXMsgProcessor::singleton()->parse( RAW_MSG );
    msg->reserve( FIXFields::ClOrdID,       0, 10 );
    msg->reserve( FIXFields::Price,         1, 7 );
    msg->reserve( FIXFields::OrderQty,      2, 2 );
    msg->reserve( FIXFields::TransactTime,  3, Engine::UTCTimestamp::ValueSizeWithMilliseconds );
    msg->reserve( FIXFields::SendingTime,   4, Engine::UTCTimestamp::ValueSizeWithMilliseconds );

    application->inflate( msg );

    Engine::GenericPreparedMessage preparedMessage( msg );

    std::cout << "Testing..." << std::endl;
    // Main sending cycle
    size_t sz;
    for( size_t i = 0; i < TEST_COUNT; ++i ) {
        application->recordTime();

        preparedMessage.set( 0, static_cast<int>( i ) ); // ClOrdID
        preparedMessage.set( 1, Engine::Decimal( 100023, -2 ) ); // Price
        preparedMessage.set( 2, 10 ); // OrderQty
        preparedMessage.set( 3, Engine::UTCTimestamp::now(true) ); // TransactTime
        preparedMessage.set( 4, preparedMessage.get( 3, &sz ), Engine::UTCTimestamp::ValueSizeWithMilliseconds ); // TransactTime

        application->sendMessage( &preparedMessage );
        //application->sendMessage( msg );
        application->waitForReceivedMessage();
    }

    // Release object message resources
    msg->release();
    std::cout << "Done." << std::endl;

    // Print the results
    application->printResult();

    // Close connection
    acceptor->disconnectNonGracefully();
    initiator->disconnectNonGracefully();

    initiator->release();
    acceptor->release();

    // Destroy FIX engine
    Engine::FixEngine::destroy();
}

int main( int argc, char* argv[] )
{
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    if( System::HRTimer::frequency() < 1000000 ) {
        std::cerr << "Timer frequency is not sufficient to perform measurement." << std::endl;
        return 1;
    }

    try {
        run( argc, argv );
    } catch( std::exception const& e ) {
        std::cerr << "ERROR: "  << e.what() << std::endl;
        return 1;
    }

    return 0;
}
