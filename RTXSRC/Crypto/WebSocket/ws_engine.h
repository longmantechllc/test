#pragma once

#include <utility>
#include <iostream>
#include <memory>

#include <rapidjson/document.h>
#include <rapidjson/prettywriter.h>
#include <rapidjson/stringbuffer.h>

#include <libwebsockets.h>
#include <spdlog/spdlog.h>
#include <gzip/decompress.hpp>

namespace crypto::exchanges::connection {

class WsEngine {
public:
    enum class protocol_type : std::uint8_t {
        default_type,
        skip_compression,
        larger_buffer
    };

private:
    template<bool skip_compression = false>
    static int event_cb(struct lws *wsi, enum lws_callback_reasons reason,
        void *user, void *in, size_t len);
    static constexpr struct lws_protocols protocols[][2] = {
        {
            {
                "websocket",
                WsEngine::event_cb<>,
                0,
                4096 * 100,
                0,
                NULL,
                0
            },
            {nullptr, nullptr, 0, 0}
        },
        {
            {
                "websocket",
                WsEngine::event_cb<true>,
                0,
                4096 * 100,
                0,
                NULL,
                0
            },
            {nullptr, nullptr, 0, 0}
        },
        {
            {
                "websocket",
                WsEngine::event_cb<>,
                0,
                4096 * 100 * 3,
                0,
                NULL,
                0
            },
            {nullptr, nullptr, 0, 0}
        }
    };

    static std::string decompressed_data;

public:
    struct lws_context *context;
    std::vector<std::string> allowed_interfaces;

    WsEngine(protocol_type type = protocol_type::default_type);
    ~WsEngine();

    virtual void init_context(protocol_type type);

    virtual void service();
};

}  // namespace crypto::exchanges::connection
