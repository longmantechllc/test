
//From http://coolcowstudio.wordpress.com/2010/05/14/c-raii-adapter-for-xerces/

//#include <xercesc/util/XMLString.hpp>

//class XMLString;

template <typename T>
class auto_xerces_ptr
{
    // Hide copy constructor and assignment operator
    auto_xerces_ptr(const auto_xerces_ptr&);
    auto_xerces_ptr& operator=(const auto_xerces_ptr&);

    // Function to release Xerces data type
//    template <typename T>
    static void do_release(T*& item)
    {
        // Only release this if it has no parent (otherwise
        // parent will release it)
        if (0 == item->getOwnerDocument())
            item->release();
    }

    // Specializations for character types, which needs to be
    // released by XMLString::release
//    template <>
    static void do_release(char*& item)
    {
        xercesc_3_2::XMLString::release(&item);
    }

//    template <>
//    static void do_release(XMLCh*& item)
//    {
//        xercesc_3_2::XMLString::release(&item);
//    }
//     The actual data we're holding
    T* item_;

public:
    auto_xerces_ptr()
        : item_(0)
    {}

    explicit auto_xerces_ptr(T* i)
        : item_(i)
    {}

    ~auto_xerces_ptr()
    {
//        xerces_release();
    }

    // Assignment of data to guard (not chainable)
    void operator=(T* i)
    {
        reassign(i);
    }

    // Release held data (i.e. delete/free it)
    void xerces_release()
    {
        if (!is_released())
        {
            // Use type-specific release mechanism
            do_release(item_);
            item_ = 0;
        }
    }

    // Give up held data (i.e. return data without releasing)
    T* yield()
    {
        T* tempItem = item_;
        item_ = 0;
        return tempItem;
    }

    // Release currently held data, if any, to hold another
    void assign(T* i)
    {
        xerces_release();
        item_ = i;
    }

    // Get pointer to the currently held data, if any
    T* get()
    {
        return item_;
    }

    // Return true if no data is held
    bool is_released() const
    {
        return (0 == item_);
    }
};