//
// Created by schen on 11/11/2020.
//
#include <string>
#include <unordered_map>
#include <vector>
#include <unordered_set>
#include <memory>
#include <iostream>

#include "RTXUtils.h"
#include "ConfigSecIdfnsBox.h"

Target::Target()
{
}

void Target::AddDefinedRoute(const std::string& routeName)
{
    definedRoutes.insert(routeName);
}

void Target::AddMDIDmapping(const std::string& mmid, const std::string& routeName)
{
    if(MDIDmapping.find(mmid) == MDIDmapping.end())
    {
        auto pr = std::make_pair(mmid, routeName);
        MDIDmapping.insert(pr);

        definedMmid.insert(mmid);
    }
}

void SecIdfn::AddOrderParam(const std::string& type, std::string value, const std::string& action, std::string destStr)
{
    auto dest = RTXUtils::stringToVector(destStr, ",");
    auto ptr = std::make_shared<OrderParamAction>(action, dest);        //reroute - dest

    if (type == "orderTIF") {
        int tp = RTXUtils::StrToInt(value);
        auto pr2 = std::make_pair(tp, ptr);
        orderTifActionList.insert(pr2);
    }
    else if (type == "IPO") {
        auto pr = std::make_pair(value, ptr);
        IPOActionList.insert(pr);
    }
}

ConfigSecIdfnsBox::ConfigSecIdfnsBox()
{
}

ConfigSecIdfnsBox::~ConfigSecIdfnsBox()
{
    secIdfnList.clear();
}

void ConfigSecIdfnsBox::InitConfigSecIdfnsBox()
{
}

void ConfigSecIdfnsBox::AddSecId(const std::string& name, const std::shared_ptr<SecIdfn>& secId)
{
    if(secIdfnList.find(name) == secIdfnList.end())
    {
        auto pr = std::make_pair(name, secId);
        secIdfnList.insert(pr);
    }
}

std::shared_ptr<SecIdfn>& ConfigSecIdfnsBox::GetExchange(const std::string& secId)
{
    auto sec = secIdfnList.find(secId);
    if(sec != secIdfnList.end())
        return sec->second;

    return secI_;
}

