// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_StringUtils.h
/// Contains StringUtils template class definition

#ifndef H_B2BITS_UTILS_STRINGUTILS_H
#define H_B2BITS_UTILS_STRINGUTILS_H

#include <cstring>
#include <cstdlib>
#include <cctype>
#include <cwctype>
#include <cwchar>

#include <B2BITS_IntDefines.h>
#include <B2BITS_V12_Defines.h>

/// FIX Antenna Utils framework
namespace Utils
{
    /// This function is called to make a case-insensitive comparison of two ANSI strings.
    int V12_API strnicmp( const char* string1, const char* string2, std::size_t count ) throw();
    /// Performs a lowercase comparison of strings.
    int V12_API stricmp( const char* string1, const char* string2 ) throw();

    /// Converts a string to a 64-bit integer.
    System::i64 V12_API atoi64( const char* str ) throw();

    /// Compare characters of two strings without regard to case.
    int V12_API wstrnicmp( const wchar_t* string1, const wchar_t* string2, std::size_t count ) throw();
    /// Converts a wide-character std::string to an integer
    int V12_API wstrtoi( const wchar_t* string ) throw();

    /// Converts a string to a 64-bit integer.
    System::i64 V12_API wstrtoi64( const wchar_t* str ) throw();

    /// Converts a string to a 16-bit integer.
    int V12_API strtoi( const char* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit integer.
    unsigned int V12_API strtou( const char* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit integer.
    System::i16 V12_API strtoi16( const char* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit unsigned integer.
    System::u16 V12_API strtou16( const char* str, std::size_t size ) throw();
    /// Converts a string to a 32-bit integer.
    System::i32 V12_API strtoi32( const char* str, std::size_t size ) throw();
    /// Converts a string to a 32-bit unsigned integer.
    System::u32 V12_API strtou32( const char* str, std::size_t size ) throw();
    /// Converts a string to a 64-bit integer.
    System::i64 V12_API strtoi64( const char* str, std::size_t size ) throw();
    /// Converts a string to a 64-bit unsigned integer.
    System::u64 V12_API strtou64( const char* str, std::size_t size ) throw();

    /// Converts a string to a 16-bit integer.
    int V12_API wstrtoi( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit unsigned integer.
    unsigned int V12_API wstrtou( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit integer.
    System::i16 V12_API wstrtoi16( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 16-bit unsigned integer.
    System::u16 V12_API wstrtou16( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 32-bit integer.
    System::i32 V12_API wstrtoi32( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 32-bit unsigned integer.
    System::u32 V12_API wstrtou32( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 64-bit integer.
    System::i64 V12_API wstrtoi64( const wchar_t* str, std::size_t size ) throw();
    /// Converts a string to a 64-bit unsigned integer.
    System::u64 V12_API wstrtou64( const wchar_t* str, std::size_t size ) throw();

    /// Encapsulates generic std::string processing methods
    template< typename CharT >
    class StringUtils;

    /// Encapsulates ASCII std::string processing methods
    template<>
    class StringUtils< char >
    {
    public:
        /// Returns length of the std::string
        ///
        /// @param str Null-terminated std::string.
        static std::size_t strlen( char const* str ) throw() {
            return std::strlen( str );
        }

        /// This function is called to make a case-insensitive comparison of two ANSI strings.
        ///
        /// @param str1 The first std::string.
        /// @param str2 The second std::string.
        /// @param count The number of characters to compare.
        static int strnicmp( char const* str1, char const* str2, std::size_t count ) throw() {
            return strnicmp( str1, str2, count );
        }

        /// Compare characters of two strings.
        ///
        /// @param str1 Strings to compare.
        /// @param str2 Strings to compare.
        /// @param count Number of characters to compare.
        static int strncmp( char const* str1, char const* str2, std::size_t count ) throw() {
            return std::strncmp( str1, str2, count );
        }

        /// Find a character in a std::string.
        ///
        /// @param str Null-terminated source std::string.
        /// @param c Character to be located.
        static char const* strchr( char const* str, char c ) throw() {
            return std::strchr( str, c );
        }

        /// Converts strings to integer.
        ///
        /// @param str String to be converted.
        static System::i32 toInt32( const char* str ) throw() {
            return std::atoi( str );
        }

        /// Converts a std::string to a 64-bit integer.
        ///
        /// @param str String to be converted.
        static System::i64 toInt64( const char* str ) throw() {
            return atoi64( str );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static unsigned int toUInt( const char* str, std::size_t size ) throw() {
            return strtou( str, size );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u16 toUInt16( const char* str, std::size_t size ) throw() {
            return strtou16( str, size );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u32 toUInt32( const char* str, std::size_t size ) throw() {
            return strtou32( str, size );
        }

        /// Converts a string to a 64-bit unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u64 toUInt64( const char* str, std::size_t size ) throw() {
            return strtou64( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static int toInt( const char* str, std::size_t size ) throw() {
            return strtoi( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static System::i16 toInt16( const char* str, std::size_t size ) throw() {
            return strtoi16( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static System::i32 toInt32( const char* str, std::size_t size ) throw() {
            return strtoi32( str, size );
        }

        /// Converts a string to a 64-bit integer.
        ///
        /// @param str String to be converted.
        static System::i64 toInt64( const char* str, std::size_t size ) throw() {
            return strtoi64( str, size );
        }

        /// Returns nonzero if c is a particular representation of a decimal-digit character.
        ///
        /// @param c char to test.
        static bool isDigit( char c ) throw() {
            return 0 != ::isdigit( static_cast<int>( c ) );
        }
    };

    /** Encapsulates Unicode std::string processing methods */
    template<>
    class StringUtils< wchar_t >
    {
    public:
        /// Returns length of the std::string
        ///
        /// @param str Null-terminated std::string.
        static std::size_t strlen( wchar_t const* str ) throw() {
            return std::wcslen( str );
        }

        /// This function is called to make a case-insensitive comparison of two ANSI strings.
        ///
        /// @param str1 The first std::string.
        /// @param str2 The second std::string.
        /// @param count The number of characters to compare.
        static int strnicmp( wchar_t const* str1, wchar_t const* str2, std::size_t count ) throw() {
            return wstrnicmp( str1, str2, count );
        }

        /// Compare characters of two strings.
        ///
        /// @param str1 Strings to compare.
        /// @param str2 Strings to compare.
        /// @param count Number of characters to compare.
        static int strncmp( wchar_t const* str1, wchar_t const* str2, std::size_t count ) throw() {
            return std::wcsncmp( str1, str2, count );
        }

        /// Find a character in a std::string.
        ///
        /// @param str Null-terminated source std::string.
        /// @param c Character to be located.
        static wchar_t const* strchr( wchar_t const* str, wchar_t c ) throw() {
            return std::wcschr( str, c );
        }

        /// Converts strings to integer.
        ///
        /// @param str String to be converted.
        static System::i32 toInt32( const wchar_t* str ) throw() {
            return wstrtoi( str );
        }

        /// Converts a std::string to a 64-bit integer.
        ///
        /// @param str String to be converted.
        static System::i64 toInt64( const wchar_t* str ) throw() {
            return wstrtoi64( str );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static unsigned int toUInt( const wchar_t* str, std::size_t size ) throw() {
            return wstrtou( str, size );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u16 toUInt16( const wchar_t* str, std::size_t size ) throw() {
            return wstrtou16( str, size );
        }

        /// Converts string to unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u32 toUInt32( const wchar_t* str, std::size_t size ) throw() {
            return wstrtou32( str, size );
        }

        /// Converts a string to a 64-bit unsigned integer.
        ///
        /// @param str String to be converted.
        static System::u64 toUInt64( const wchar_t* str, std::size_t size ) throw() {
            return wstrtou64( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static int toInt( const wchar_t* str, std::size_t size ) throw() {
            return wstrtoi( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static System::i16 toInt16( const wchar_t* str, std::size_t size ) throw() {
            return wstrtoi16( str, size );
        }

        /// Converts string to integer.
        ///
        /// @param str String to be converted.
        static System::i32 toInt32( const wchar_t* str, std::size_t size ) throw() {
            return wstrtoi32( str, size );
        }

        /// Converts a string to a 64-bit integer.
        ///
        /// @param str String to be converted.
        static System::i64 toInt64( const wchar_t* str, std::size_t size ) throw() {
            return wstrtoi64( str, size );
        }

        /// Returns nonzero if c is a particular representation of a decimal-digit wchar_tacter.
        ///
        /// @param c wchar_t to test.
        static bool isDigit( wchar_t c ) throw() {
            return 0 != std::iswdigit( c );
        }
    };
}

#endif // H_B2BITS_UTILS_STRINGUTILS_H
