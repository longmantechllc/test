// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#include "UnitListener.h"

#include "BatsClient.h"

using namespace Bats;

/* -------------------------------------------------------------------------------------/
    single unit listener
*/
bool UnitListener::isConnected() const
{
    return connected_;
}
bool UnitListener::isLinkUp() const
{
    return linkup_;
}
unsigned UnitListener::totalMessages() const
{
    return totalMessages_;
}
unsigned UnitListener::totalUnits() const
{
    return totalUnits_;
}

std::deque<std::string> UnitListener::lastNotifications() const
{
    guard_type g(mutex_);
    return notifications_;
}

void UnitListener::onReset(Bats::UnitService* service, ResetReason::Type type)
{
    std::string str;
    guard_type g(mutex_);

    if (type == ResetReason::rrSpin)
        str = "RstSpin";
    else
    if (type == ResetReason::rrSequenceNumber)
        str = "Rst#1";
    else 
    if(type == ResetReason::rrUnitClear)
        str = "RstClr";
    else 
	if(type == ResetReason::rrNaturalRefresh)
        str = "RstNR";

    notifications_.push_back(str);

    if (notifications_.size() > 4) notifications_.pop_front();

    totalMessages_ = 0;
    totalUnits_ = 0;
    bc_->notify();
}

void UnitListener::onMessage(UnitService* service, const PitchMsg* msg, System::u32 seconds, Timestamp& /*ts*/)
{
    ++totalMessages_;
    bc_->notify();
}
void UnitListener::onUnitProcessed(ReadingCtx* ctx, Timestamp& /*ts*/)
{
    ++totalUnits_;
    bc_->notify();
}

void UnitListener::onNotification(UnitService* /*service*/, Notification::Type type) 
{
    guard_type g(mutex_);
    switch(type)
    {
    case Bats::ServiceListener::Notification::LinkUp:
        notifications_.push_back("LnkUp");
        linkup_ = true;
        break;
    case Bats::ServiceListener::Notification::LinkDown:
        notifications_.push_back("LnkDwn");
        linkup_ = false;
        break;
    case Bats::ServiceListener::Notification::Connected:
        notifications_.push_back("Connect");
        connected_ = true;
        linkup_ = true;
        break;
    case Bats::ServiceListener::Notification::Disconnected:
        notifications_.push_back("Disconnect");
        connected_ = false;
        totalMessages_ = 0;
        totalUnits_ = 0;
        linkup_ = false;
        break;
    case Bats::ServiceListener::Notification::RecoveryStarted:
        notifications_.push_back("SpinStart");
        break;
    case Bats::ServiceListener::Notification::RecoverySucceeded:
        notifications_.push_back("SpinOK");
        break;
    case Bats::ServiceListener::Notification::RecoveryFailed:
        notifications_.push_back("SpinFail");
        break;
    case Bats::ServiceListener::Notification::ReplayStarted:
        notifications_.push_back("RplStart");
        break;
    case Bats::ServiceListener::Notification::ReplayFailed:
        notifications_.push_back("RplFail");
        break;
    case Bats::ServiceListener::Notification::ReplayRequested:
        notifications_.push_back("RplReq");
        break;
    case Bats::ServiceListener::Notification::SequenceRestored:
        notifications_.push_back("SeqOK");
        break;

    }
    if (notifications_.size() > 4) notifications_.pop_front();

    bc_->notify();
}