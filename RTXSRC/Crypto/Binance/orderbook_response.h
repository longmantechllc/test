#pragma once

#include <string_view>
#include <vector>
#include <json_dto/pub.hpp>

namespace Crypto::binance {

struct orderbook_response_t {
    std::uint64_t last_update_id {0};
    std::vector<std::vector<std::string_view>> bids;
    std::vector<std::vector<std::string_view>> asks;

    void reset() {
        last_update_id = 0;
        bids.clear();
        asks.clear();        
    }
};


}  // namespace Crypto::binance

namespace json_dto {

template<typename Json_Io>
void json_io(
        Json_Io &io, 
        Crypto::binance::orderbook_response_t &m) {
    io
    & json_dto::mandatory("lastUpdateId", m.last_update_id)
    & json_dto::mandatory("bids", m.bids)
    & json_dto::mandatory("asks", m.asks);
}

}  // namespace json_dto
