/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/

#include "CustomCMEApplication.h"
#include <iostream>
#include <string>
#include <sstream>
#include "B2BITS_FIXEnums.h"

namespace CmeFixFields {
    using namespace FIXFields;
	
	static const int StartSequenceNumber = 5024;
}

namespace {
	std::string toString(int val) {
		std::stringstream buff;
		buff << val;
		return buff.str();
	}
}

// This parameter controls the lenght of XML tag. 
// It encapsulates FIX Message for Drop Copy (http://www.cmegroup.com/confluence/display/EPICSANDBOX/Drop+Copy+4.0#DropCopy4.0-EncapsulatedXMLNon-Fix(tag35-MsgType=n)Message)
// At this moment the XML tag is <RTRF> so  set this parameter to 4.
const int CustomCMEApplication::xmlDataTagSize_ = 4;

CustomCMEApplication::CustomCMEApplication(b2bitscme::Params* params, Utils::Log::LogCategory* logger, ISem* sem)
	: b2bitscme::CMEApplication(params, logger)
	, b2bitscme::IMessageProcessor(logger)
	, sem_(sem)
	, isDropCopy_(params->isDropCopy_)
{
	if (logger_)
		logger_->note("Creating CustomCMEApplication...");

	b2bitscme::CMEApplication::attachMessageProcessor(this);
}

CustomCMEApplication::~CustomCMEApplication() {
	if (logger_)
		logger_->note("Destroying CustomCMEApplication...");
}

bool CustomCMEApplication::process(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
	if (logger_)
		logger_->note("CustomCMEApplication::process was called");

	return b2bitscme::CMEApplication::process(fixMsg, aSn);
}

void CustomCMEApplication::onSessionLevelRejectEvent(const Engine::SessionLevelRejectEvent* apEvent, const Engine::Session& aSn) {
    // This call-back method is called when a session-level Reject message (MsgType = 3) is received.
	
	if (logger_) {
		logger_->note("SessionLevelRejectEvent received for the session " + *aSn.getId());
		logger_->note("SessionLevelReject message is " + *apEvent->m_pRejectMsg->toString());
	}
	
	if (isDropCopy_) {
		Engine::FIXFieldValue val;
		std::string marketSegmentId;
		int startSequenceNumber = 0;

		// Get Market Segment Id from 50 tag
		if (apEvent->m_pRejectMsg->get(FIXField::SenderSubID, &val)) {
			marketSegmentId = std::string(val.data_, val.length_);
		}

		// Get StartSequenceNumber from 5024 tag
		if (apEvent->m_pRejectMsg->get(CmeFixFields::StartSequenceNumber, &val)) {
			startSequenceNumber = val.toSigned();
		}
		else
			return;
		
		// Show information to the user
		if (!marketSegmentId.empty() && startSequenceNumber > 0 && logger_) {
			logger_->note("[CustomCMEApplication::onSessionLevelRejectEvent] Session Level Reject message received for Market Segment Id [" 
					  + marketSegmentId + "] with StartSequenceNumber [" 
					  + toString(startSequenceNumber) + "]. ");
			logger_->note("Please use Mid Week Session reconnect to reconnect session and change it's InSeqNum to StartSequenceNumber. Modified Resend Request will be send automatically. ");
		}
	}
}


void CustomCMEApplication::onLogonEvent(const Engine::LogonEvent* apEvent, const Engine::Session& aSn) 
{
	CMEApplication::onLogonEvent(apEvent, aSn);

	sem_->post();
}

void CustomCMEApplication::onLogoutEvent(const Engine::LogoutEvent* apEvent, const Engine::Session& aSn) 
{
	CMEApplication::onLogoutEvent(apEvent, aSn);

	sem_->post();


}

void CustomCMEApplication::process(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn, bool isFromQueue) {
	if (logger_) {
		std::string fromQueue = isFromQueue ? "from queue" : "not from queue";
        logger_->note("Message " + fromQueue + " in session " + *aSn.getId());
        logger_->note(*fixMsg.toString(' '));
    }

	if (isDropCopy_) {
		// Parse FIX message from Drop Copy report and print parsed message
		if (FIXEnums::MsgType::XML_MESSAGE == fixMsg.type() ) {
		
			parseDropCopyReport(fixMsg, aSn);
		}
	}
}

bool  CustomCMEApplication::parseDropCopyReport(const Engine::FIXMessage& fixMsg, const Engine::Session& aSn) {
	// This method:
	// - extracts XmlData (tag 213) from incoming Drop Copy Report message (35=n)
	// - constructs raw message without XML tags (this step uses parameter DropCopySample.XmlDataTagSize from the Engine properties)
	// - parses raw message and creates corresponding Engine::FIXMessage
	// - outputs parsed message for a user to see

	// Example incoming message (SOH replaced by spaces):
	//	8=FIX.4.2 9=475 35=n 49=CME 56=TSTCIDN 34=4 50=G 57=PAA 143=RU 52=20151110-12:57:23.715 122=20151007-16:09:32.854 212=356 213=<RTRF>8=FIX.4.2 9=320 35=8 34=4291 369=4267 52=20151007-16:09:32.854 49=CME 50=G 56=G86000N 57=DUMMY 143=US,IL 1=00521 6=0 11=ACP1444234172815 14=0 17=99218:12219 20=0 37=9913161431 38=1 39=0 40=2 41=0 44=9980.5 48=998902 54=1 55=90 59=0 60=20151007-16:09:32.852 107=2EJZ4 150=0 151=1 167=FUT 432=20151007 1028=Y 1091=N 9717=ACP1444234172815 10=235 </RTRF> 369=995 10=175 
	// Extracted XmlData:
	//	<RTRF>8=FIX.4.2 9=320 35=8 34=4291 369=4267 52=20151007-16:09:32.854 49=CME 50=G 56=G86000N 57=DUMMY 143=US,IL 1=00521 6=0 11=ACP1444234172815 14=0 17=99218:12219 20=0 37=9913161431 38=1 39=0 40=2 41=0 44=9980.5 48=998902 54=1 55=90 59=0 60=20151007-16:09:32.852 107=2EJZ4 150=0 151=1 167=FUT 432=20151007 1028=Y 1091=N 9717=ACP1444234172815 10=235 </RTRF>
	// Constructed raw message by removing XML open and close tags (<RTRF> and </RTRF>, DropCopySample.XmlDataTagSize = 4 in this case):
	//  8=FIX.4.2 9=320 35=8 34=4291 369=4267 52=20151007-16:09:32.854 49=CME 50=G 56=G86000N 57=DUMMY 143=US,IL 1=00521 6=0 11=ACP1444234172815 14=0 17=99218:12219 20=0 37=9913161431 38=1 39=0 40=2 41=0 44=9980.5 48=998902 54=1 55=90 59=0 60=20151007-16:09:32.852 107=2EJZ4 150=0 151=1 167=FUT 432=20151007 1028=Y 1091=N 9717=ACP1444234172815 10=235 
	

	Engine::FIXFieldValue val;
	// Get data from 213 tag
	if (fixMsg.get(FIXField::XmlData, &val)) {

		// Construct raw message without XML tags
		std::string rawData(val.data_ + xmlDataTagSize_ + 2, val.length_ - (xmlDataTagSize_ + 2) - (xmlDataTagSize_ + 3));

		// Parse raw message
		std::auto_ptr<Engine::FIXMessage> msg( Engine::FIXMsgProcessor::singleton()->parse(aSn.parserID(), rawData));

		// Output the message
		if (logger_) {
			logger_->note("Parsed message from Drop Copy report in session " + *aSn.getId());
			logger_->note(*msg->toString(' '));
		}
	}

	return true;
}
