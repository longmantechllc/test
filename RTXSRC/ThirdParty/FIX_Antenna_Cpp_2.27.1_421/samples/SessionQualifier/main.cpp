// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.

#include <iostream>
#include <exception>

#include <B2BITS_FixEngine.h>
#include <B2BITS_Session.h>
#include <B2BITS_SessionId.h>
#include <B2BITS_FIXMsgFactory.h>
#include <B2BITS_FIXFields.h>
#include <B2BITS_UTCTimestamp.h>

#include "MySessionsManager.h"
#include "InitiatorApp.h"

namespace
{
    const char Acceptor1Name[] = "Acceptor1";
    const char Acceptor2Name[] = "Acceptor2";
    const char InitiatorName[] = "Initiator";
    const char Q1[] = "1"; //Session Qualifier
    const char Q2[] = "2"; //Session Qualifier

    const char* LISTENER_IP = "127.0.0.1";
    const int LISTENER_PORT = 9026;
}

int main() {
#if defined(_MSC_VER)
    // Switch to the application folder to be able to debug application
    std::string exe_path( _pgmptr );
    size_t slash_pos = exe_path.find_last_of( '\\' );
    if( std::string::npos != slash_pos ) {
        exe_path.resize( slash_pos );
        SetCurrentDirectory( exe_path.c_str() );
    }
#endif // defined(_MSC_VER)

    try {
        // Initialize FE
        std::cout << "Initializing FIX engine..."  << std::endl;
        Engine::FixEngine::init("engine.properties");

        // create SessionsManager
        std::auto_ptr<MySessionsManager> apSM (new MySessionsManager());

        // register SessionsManager
        Engine::FixEngine::singleton()->registerSessionsManager( apSM.get() );

        // create two acceptor sessions with the same sender/target but with different qualifiers
        Engine::SessionId accSession1(Acceptor1Name, InitiatorName, Q1);
        apSM->createAcceptor( accSession1, Engine::FIX44 );

        Engine::SessionId accSession2(Acceptor1Name, InitiatorName, Q2);
        apSM->createAcceptor( accSession2, Engine::FIX44 );

        // create FIX 4.4 New Order Single using the Flat model
        std::auto_ptr<Engine::FIXMessage> pMessage( Engine::FIXMsgFactory::singleton()->newSkel( Engine::FIX44, "D" ) );
        std::string clrodid;
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pMessage->set( FIXField::ClOrdID, clrodid );
        pMessage->set( FIXField::Symbol, "MSFT" );
        pMessage->set( FIXField::Side, '1' ); // Buy
        pMessage->set( FIXField::OrderQty, 400 );
        pMessage->set( FIXField::OrdType, '2' ); // Limit
        pMessage->set( FIXField::Price, Engine::Decimal( 1132, -2 ) );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );


        // create two  initiator sessions corresponding to the registered acceptor sessions and establish connections
        InitiatorApp app1, app2;
        Engine::Session* ps1 =  Engine::FixEngine::singleton()->createSession( &app1, Engine::SessionId(InitiatorName, Acceptor1Name, Q1),  Engine::FIX44 );
        ps1->connect( 30, LISTENER_IP, LISTENER_PORT );

        Engine::Session* ps2 =  Engine::FixEngine::singleton()->createSession( &app2, Engine::SessionId(InitiatorName, Acceptor1Name, Q2),  Engine::FIX44 );
        ps2->connect( 30, LISTENER_IP, LISTENER_PORT );

        // Sending order by initiator1
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pMessage->set( FIXField::ClOrdID, clrodid );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );
        ps1->put( pMessage.get() );

        // Sending order by initiator2
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pMessage->set( FIXField::ClOrdID, clrodid );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );
        ps2->put( pMessage.get() );

        //wait for trade results
        app1.tradeReceived_.wait();
        app2.tradeReceived_.wait();

        // create another two  initiator sessions and connect to unregistered acceptor sessions
        InitiatorApp app3, app4;
        Engine::Session* ps3 =  Engine::FixEngine::singleton()->createSession( &app3, Engine::SessionId(InitiatorName, Acceptor2Name, Q1),  Engine::FIX44 );
        ps3->connect( 30, LISTENER_IP, LISTENER_PORT );

        Engine::Session* ps4 =  Engine::FixEngine::singleton()->createSession( &app4, Engine::SessionId(InitiatorName, Acceptor2Name, Q2),  Engine::FIX44 );
        ps4->connect( 30, LISTENER_IP, LISTENER_PORT );

        // Sending order by initiator3
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pMessage->set( FIXField::ClOrdID, clrodid );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );
        ps3->put( pMessage.get() );

        // Sending order by initiator4
        Engine::UTCTimestamp::now().toFixString( &clrodid );
        pMessage->set( FIXField::ClOrdID, clrodid );
        pMessage->set( FIXField::TransactTime, Engine::UTCTimestamp::now() );
        ps4->put( pMessage.get() );

        //wait for trade results
        app3.tradeReceived_.wait();
        app4.tradeReceived_.wait();

        ps1->disconnect();
        ps2->disconnect();
        ps3->disconnect();
        ps4->disconnect();

        ps1->waitForTerminated();
        ps1->release();
        ps2->waitForTerminated();
        ps2->release();
        ps3->waitForTerminated();
        ps3->release();
        ps4->waitForTerminated();
        ps4->release();

	apSM.reset();
	Engine::FixEngine::destroy();

    } catch( std::exception const& e ) {
        std::cout << "[ERROR] " << e.what() << std::endl;
    }

}
