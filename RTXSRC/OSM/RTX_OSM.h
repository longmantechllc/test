#pragma once

#include <stdint.h>
#include <unordered_map>
#include <mutex>
#include <cassert>
#include <memory>
#include <thread>
#include <array>

#include <zmq.hpp>

#include "quickfix/FileStore.h"
#include "quickfix/FileLog.h"
#include "quickfix/SocketAcceptor.h"
#include "quickfix/SocketInitiator.h"
#include "quickfix/Log.h"
#include "quickfix/SessionSettings.h"
#include "quickfix/Session.h"
#include "quickfix/Application.h"
#include "quickfix/MessageCracker.h"
#include "quickfix/Values.h"
#include "quickfix/Utility.h"
#include "quickfix/Mutex.h"
#include "quickfix/fix44/NewOrderSingle.h"
#include "quickfix/fix44/OrderCancelReplaceRequest.h"
#include "quickfix/fix44/OrderCancelRequest.h"
#include "quickfix/fix44/ExecutionReport.h"
#include "quickfix/fix44/OrderCancelReject.h"

#include "ExecutionReport.h"
#include "Order.h"
#include "ApplicationProperties.h"
#include "OSMOrder.h"
#include "RedlineMD.h"
#include "SimMD.h"

#include "DBConnector.h"

// SOR
#include "App.h"
#include "RTXOrderIO.h"
#include "MktDataIO.h"
#include "RTXApp.h"
#include "IOIDataIO.h"

class Admin;
/// Application implementation (processes the incoming messages).
class RTX_OSM : public FIX::Application, public FIX::MessageCracker
        , public RTXApp, public IOIInterface
{
public:
    explicit RTX_OSM(const ApplicationProperties& params);
    ~RTX_OSM();

    // Application overloads
    void onCreate( const FIX::SessionID& ) override;
    void onLogon( const FIX::SessionID& sessionID ) override;
    void onLogout( const FIX::SessionID& sessionID ) override;
    void toAdmin( FIX::Message&, const FIX::SessionID& ) override;
    void toApp( FIX::Message&, const FIX::SessionID& )
        EXCEPT ( DoNotSend )  override;
    void fromAdmin( const FIX::Message&, const FIX::SessionID& )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, RejectLogon ) override;
    void fromApp( const FIX::Message& message, const FIX::SessionID& sessionID )
        EXCEPT ( FieldNotFound, IncorrectDataFormat, IncorrectTagValue, UnsupportedMessageType ) override;

    /// Sends message to the remote side
    void send(FIX::SessionID& session, FIX44::Message& msg)
    {
        FIX::Session::sendToTarget(msg, session);
    }
    void setCliSession(const FIX::SessionID& cliSession) {
        cliSession_ = cliSession;
    }

    // stop update DB
    void stopDB() { m_db->setRunning(false);}
    void stopAdmin();
    void stopMonitor() { m_monitor_running = false; }

    // SOR callbacks
    void sendChild(const Order&) override;
    void sendACK(const ExecutionReport&) override;
    void sendER(const ExecutionReport&) override;
    bool cancelChild(std::string childOrderId) override;
    std::vector<IOIMarketData> SubscribeIOI(std::string symbol, bool isBuy) override;
    bool isInited() {return m_init;}
    // admin utilities
    void cancelLogin(const string& str);
    void cancelSubAccount(const string& str);
    void cancelVenue(const string& str);
    std::string connectNormalizer();
    auto& getExchStatus() { return m_exchStatus; }

private: // Engine::Application contract implementation

    /// Blocks the calling thread until the session is closed.
    void waitUntilClosed();

    // utilities
    void sendNewACK(int orderId,const OSMOrder& order /*const FIX::Message* fixMsg*/);

    // MessageCracker overloads
    void onMessage( const FIX44::NewOrderSingle& msg, const FIX::SessionID& );
    void onMessage( const FIX44::OrderCancelReplaceRequest& msg, const FIX::SessionID& );
    void onMessage( const FIX44::OrderCancelRequest& msg, const FIX::SessionID& );
    void onMessage( const FIX44::ExecutionReport& msg, const FIX::SessionID& );
    void onRecoveryMessage( const FIX44::ExecutionReport& msg);

    void ackPendingCancel(int orderId, const FIX44::OrderCancelRequest& msg, time_t& ts);
    void ackPendingCancelReplace(int orderId, const FIX44::OrderCancelReplaceRequest& msg, time_t& ts);
    void sendRejectCancelReplace(int orderId, const FIX44::OrderCancelReplaceRequest& msg, time_t& ts);

    void sendRejectCancelReplace(OSMOrder& order);
    void sendCancelReplaceAck(const OSMOrder& order);
    void sendRejectCancel(const OSMOrder& order, const std::string& clOrdId);

    friend class con_handler;

    const ApplicationProperties& params_;

    unique_ptr<FIX::SocketAcceptor> m_acceptor;
    unique_ptr<FIX::SocketInitiator> m_initiator;
    FIX::SessionID session_, cliSession_;

    // SOR instance
    App sor;
    std::shared_ptr<RTXOrderIO> RTXOrderIO_{};
    std::shared_ptr<MktDataIO> RTXMarketIO_{};
    std::shared_ptr<IOIDataIO> RTXIoiIO_{};

    struct ToKeep {
        atomic<uint> parentOrdId {};
        atomic<uint> execId {};
        atomic<uint> gwId{};
    };

    ToKeep *m_toKeep{};

    std::mutex mutex_, m_queueProtection[4];
    std::shared_mutex m_mutex, m_pmutex;
    std::unordered_map<std::string, int> m_clOrdId2OrderId;
    std::unordered_map<std::string, OSMOrder> m_childOrderMap;
    std::unordered_map<int, OSMOrder> m_parentOrderMap;
    std::unordered_map<std::string, std::string> m_pendingOrderMap;
    std::unordered_map<std::string, std::string> m_exch2ChildOrderMap;
    std::unordered_map<int, OSMRecoveredOrder> m_recoveryPOrderMap;
    std::unordered_map<std::string, RecoveredGWOrder> m_recoveryCOrderMap;

    VirtualDestMap m_virtualDestMap;
    std::unordered_map<std::string, bool> m_exchStatus;
    std::unique_ptr<DBConnector> m_db;
    //RedisPublisher m_pub;

    LockFree::LockFreeQueue<tblState> m_stateQ{819200}; // double for test. 1638400, 819200, 409600
    LockFree::LockFreeQueue<tblChildOrder> m_childQ{204800};
    LockFree::LockFreeQueue<tblParentOrder> m_parentQ{102400};
    LockFree::LockFreeQueue<tblExecution> m_exeQ{307200};
    std::array<atomic<uint32_t>, 4> m_tblRecordNum = {0,0,0,0};
    std::unique_ptr<Admin> m_admin;
    std::thread m_dbThread, m_adminThread, m_monitorThread;
    atomic<bool> m_monitor_running {true};
    bool m_init{};
    //RedlineMD m_md;
    MDInterface *m_md {nullptr};
    std::string getNewGWId();
    std::string getNewExecId();
    void markExchanges(bool status);

    zmq::context_t m_context;
    zmq::socket_t m_socket;
    bool m_sendingIOI{true};

};


