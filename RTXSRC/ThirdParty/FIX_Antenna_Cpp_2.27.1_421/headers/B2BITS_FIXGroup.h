// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_FIXGroup.h
/// Contains Engine::FIXGroup class declaration.

#ifndef __B2BITS_FIXGroup_h__
#define __B2BITS_FIXGroup_h__

#include <string>

#include <B2BITS_CompilerDefines.h>
#include "B2BITS_V12_Defines.h"
#include "B2BITS_PubEngineDefines.h"
#include <B2BITS_IntDefines.h>
#include <B2BITS_Types.h>
#include <B2BITS_Exception.h>
#include <B2BITS_TZTimeHelper.h>

namespace Engine
{
    struct FIXFieldValue;
    class TagValue;
    class FixFieldsFunctor;

    /** FIX Repeating Group interface. */
    class V12_API FIXGroup
    {
    public:
        /** Default constructor. */
        FIXGroup();

        /** Returns the number of entries in this repeating group
         *
         * @return Number of entries in this repeating group.
         */
        virtual int size() const throw() = 0;

        /** Returns the leading field tag of the repeating group
         *
         * @return Leading field tag of the repeating group .
         */
        virtual int leadingField() const throw() = 0;

        /**
         * Returns field value by tag number and entry's index.
         *
         * @param index group's entry index (starts at 0).
         * @param tag FIX field tag to get data from.
         *
         * @return Returned value may be NULL, if the given tag defined for the group
         * (according to the FIX protocol), but not present in requested group's entry.
         * If the tag not defined for the group then the method throws an exception.
         *
         * @warning The returned buffer is shared for all calls. This means
         * stored content is valid untill the next call of the get(). Can return NULL.
         *
         * @throw Utils::Exception if tag not defined for the group or
         * entry index is invalid.
         *
         * @deprecated Use Engine::FIXGroup::get( int, FIXFieldValue *, int) const instead.
         *
         * @see get( int, FIXFieldValue *, int) const
         */
        B2B_DEPRECATED( "Use 'Engine::FIXGroup::get( int, FIXFieldValue *, int) const' instead" )
        virtual const std::string* get( int tag, int index ) const = 0;

        /** Retrieves field value by tag number and entry's index and stores it
         *  into instance of FIXFieldValue class.
         *
         * @param index group's entry index (starts at 0).
         * @param tag FIX field tag to get data from.
         * @param[out] value pointer to the FIXFieldValue object to store data. Cannot be NULL.
         *
         * @return "false", if the given tag defined for the group
         * (according to the FIX protocol), but not present in requested group's entry.
         * "true", if the given tag present in requested group's entry.
         *
         * @throw Utils::Exception if tag not defined by FIX protocol for the group or
         * entry index is invalid.
        */
        virtual bool get( int tag, FIXFieldValue* value, int index ) const = 0;

        /** Assignes value to the field, defined by tag and entry index.
         *
         * @param index group's entry index (starts at 0).
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @throw Utils::Exception if tag not defined by FIX protocol for the group or
         * entry index is invalid.
         */
        virtual bool set( int tag, const FIXFieldValue& value, int index ) = 0;

        /**
         * Assignes value to the field, defined by tag and entry index.
         *
         * @param index group's entry index (starts at 0).
         * @param tag FIX field tag to set data to.
         * @param value New value for field.
         *
         * @return "true" if the field defined by input tag was initialized
         * previously, If field is just inserted, returned value is "false".
         *
         * @throw Utils::Exception if tag not defined for the group or
         * entry index is invalid.
         */
        virtual bool set( int tag, const std::string& value, int index ) = 0;

        /**
         * Removes field, defined by input tag, from the given entry.
         *
         * @param index group's entry index (starts at 0).
         * @param tag Tag of field to remove.
         *
         * @return "true" if the field defined by tag was initialized
         * previously (i.e. had non-empty value), otherwise "false".
         *
         * @throw Utils::Exception if tag not defined for the group or
         * entry index is invalid.
         */
        virtual bool remove( int tag, int index ) = 0;

        /**
         * Verifies is field acceptable inside this group
         *
         * @param tag Tag number of the field to verify.
         *
         * @return "true" if field supported by this group,
         * otherwise returns false.
         */
        virtual bool isSupported( int tag ) const throw() = 0;

        /**
         * Returns nested repeating group.
         * The nested FIXGroup instance's memory is managed by FIX Engine. 
         * FIXGroup instance is guaranteed to exist within it's 
         * parent FIXMessage's lifetime or until trimming to zero length.
         *
         * @param index group's entry index (starts at 0).
         * @param tag Tag number of the leading field value.
         *
         * @return Returned value may be NULL, if the given tag exists for
         * the group (according to the FIX protocol), but not present
         * in requested group's entry. If the tag not defined for the
         * group then the method throws an exception. When you don't need
         * returned value call release() to cleanup resources.
         *
         * @throw Utils::Exception if tag not defined for the group or
         * entry index is invalid.
        */
        virtual FIXGroup* getGroup( int tag, int index ) = 0;
        virtual FIXGroup const* getGroup( int tag, int index ) const = 0;

        /**
         * Releases resources used by the given group instance.
         *
         * @param pGroup Pointer to FIXGroup
         *
         * @warning must be called before group's holder is released.
         * Group's holder may be FIXMessage or other repeating group's entry.
         * FIXMessage is released by calling FIXMessage::release(). Repeating
         * group's entry become invalid when it's removed from the group by
         * shrinking or entier outer group releasing.
         *
         * @see FIXMessage::release
         * @deprecated Is not required anymore.
         */
        B2B_DEPRECATED( "Method will be removed soon" )
        static void release( FIXGroup* pGroup );

        /**
         * Releases resources used by the given group instance.
         *
         * @param pGroup Pointer to FIXGroup
         *
         * @warning must be called before group's holder is released.
         * Group's holder may be FIXMessage or other repeating group's entry.
         * FIXMessage is released by calling FIXMessage::release(). Repeating
         * group's entry become invalid when it's removed from the group by
         * shrinking or entier outer group releasing.
         *
         * @see FIXMessage::release
         * @deprecated Is not required anymore.
         */
        B2B_DEPRECATED( "Method will be removed soon" )
        static void release( FIXGroup const* pGroup );

        /**
         * Returns list of the message fields and their values.
         *
         * @return list list of the message fields and values
         */
        virtual FixFieldsContainer* getFields() const = 0;

        virtual void write( int tag, std::ostream& out, int index ) const = 0;
        virtual bool hasFlag( int tag, int index ) const = 0;

        virtual System::i64 getAsInt64( int tag, int index ) const = 0;
        virtual System::u64 getAsUInt64( int tag, int index ) const = 0;
        virtual System::i32 getAsInt32( int tag, int index ) const = 0;
        virtual System::u32 getAsUInt32( int tag, int index ) const = 0;
        virtual int getAsInt( int tag, int index ) const = 0;
        virtual unsigned int getAsUInt( int tag, int index ) const = 0;

        /// Returns value as double
        /// @return value of the field
        virtual double getAsDouble( int tag, int index ) const = 0;
        virtual Decimal getAsDecimal( int tag, int index ) const = 0;

        /// Returns value as double
        /// @return value of the field
        virtual char getAsChar( int tag, int index ) const = 0;

        /// Returns value as std::string
        /// @return value of the field
        virtual AsciiString getAsString( int tag, int index ) const  = 0;

        /// Returns value as byte array
        /// @return value of the field
        virtual ByteArray getAsRaw( int tag, int index ) const  = 0;

        /// Returns value as byte array
        /// @return value of the field
        virtual bool getAsBoolean( int tag, int index ) const  = 0;

        virtual Engine::MonthYear getAsMonthYear( int tag, int index ) const  = 0;
        virtual Engine::LocalMktDate getAsLocalMktDate( int tag, int index ) const = 0;
        virtual Engine::UTCDateOnly getAsDateOnly( int tag, int index ) const = 0;
        virtual Engine::UTCTimeOnly getAsTimeOnly( int tag, int index ) const = 0;
        virtual Engine::UTCTimestamp getAsTimestamp( int tag, int index ) const = 0;
        virtual bool isEqual( int tag, int index, TagValue const* rv, int rvTag ) const = 0;

        /** Returns true if value of the field is empty; false otherwise.*/
        virtual bool isEmpty( int tag, int index ) const = 0;

        /** Returns true if value value is defined; false otherwise.*/
        bool hasValue( int tag, int index ) const;

        /**
         * Assignes value to the field, defined by tag and entry index.
         *
         * @param index group's entry index (starts at 0).
         * @param tag Tag number of field to set data.
         * @param value New value for field.
         *
         * @return "true" if the field defined by input tag was initialized
         * previously, If field is just inserted, returned value is "false".
         *
         * @throw Utils::Exception if tag not defined for the group or
         * entry index is invalid.
         */
        virtual bool set( int tag, Engine::MonthYear value, int index ) = 0;
        virtual bool set( int tag, Engine::LocalMktDate value, int index ) = 0;
        virtual bool set( int tag, Engine::UTCDateOnly value, int index ) = 0;
        virtual bool set( int tag, Engine::UTCTimeOnly value, int index ) = 0;
        virtual bool set( int tag, Engine::UTCTimestamp value, int index ) = 0;
        virtual bool set( int tag, System::i32 value, int index ) = 0;
        virtual bool set( int tag, System::u32 value, int index ) = 0;
        virtual bool set( int tag, System::i64 value, int index ) = 0;
        virtual bool set( int tag, System::u64 value, int index ) = 0;
        virtual bool set( int tag, double value, int index ) = 0;
        virtual bool set( int tag, Decimal value, int index ) = 0;
        virtual bool set( int tag, AsciiString value, int index ) = 0;
        virtual bool set( int tag, ByteArray value, int index ) = 0;
        virtual bool set( int tag, char value, int index ) = 0;
        virtual bool set( int tag, bool value, int index ) = 0;
        bool set( int tag, char const* value, int index ) {
            return set( tag, AsciiString( value ), index );
        }

        /// Returns group entry by index
        /// @return group entry by index
        virtual TagValue* getEntry( int index ) = 0;

        /// Returns group entry by index
        /// @return group entry by index
        virtual TagValue const* getEntry( int index ) const = 0;

        virtual void apply(FixFieldsFunctor& f) const = 0;

    protected:
        /** Destructor. */
        virtual ~FIXGroup();

    /*Convenience expansion*/
    public:
        template <class ValueT>
        struct forward_iteratorT {
            friend class FIXGroup;

            typedef std::ptrdiff_t difference_type;
            typedef ValueT value_type;
            typedef ValueT& reference;
            typedef ValueT* pointer;
            typedef std::forward_iterator_tag iterator_category;

        public:
            forward_iteratorT(const forward_iteratorT& other) : group_(other.group_), index_(other.index_) {}
            forward_iteratorT& operator=(const forward_iteratorT& other) {
                group_ = other.group_;
                index_ = other.index_;
                return *this;
            }

            bool operator==(const forward_iteratorT& other) const {
                return (group_ == other.group_) && (index_ == other.index_ || ( isEmpty() && other.isEmpty() ) );
            }

            bool operator!=(const forward_iteratorT& other) const {
                return !(*this == other);
            }

            reference operator*() {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");
                return *(group_->getEntry(index_));
            }
            reference operator*() const {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");
                return *(group_->getEntry(index_));
            }

            pointer operator->() {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");
                return group_->getEntry(index_);
            }
            pointer operator->() const {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");
                return group_->getEntry(index_);
            }

            forward_iteratorT& operator++() {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");                
                ++index_;
                return *this;
            }

            forward_iteratorT& operator++(int) {
                if (isEmpty())
                    throw Utils::Exception("Repeating Group iterator out of range");                
                forward_iteratorT current(*this);
                ++index_;
                return current;
            }

            bool isEmpty() const {
                return index_ >= group_->size() || index_ == -1;
            }
        protected:

            FIXGroup* group_;
            int index_;
            forward_iteratorT(FIXGroup* group, int index=0) : group_(group), index_(index) {}
            forward_iteratorT(const FIXGroup* group, int index=0) : group_(const_cast<FIXGroup*>(group)), index_(index) {}
        };
        
        typedef forward_iteratorT<TagValue> forward_iterator;
        typedef forward_iteratorT<const TagValue> const_forward_iterator;

        /// Returns forward iterator pointing to the first entry of group
        /// @return forward_iterator pointing to the first entry of group
        forward_iterator begin() { return forward_iterator(this);}
        const_forward_iterator begin() const { return const_forward_iterator(this);}
        const_forward_iterator cbegin() const { return const_forward_iterator(this);}

        /// Returns forward iterator with marker of the end of group
        /// @return forward_iterator with marker of the end of group
        forward_iterator end() { return forward_iterator(this, -1);}
        const_forward_iterator end() const { return const_forward_iterator(this, -1);}
        const_forward_iterator cend() const { return const_forward_iterator(this, -1);}

    };

    namespace TZTimeHelper
    {
        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param msg Message
         * @param tag FIX field tag to set data to.
         * @param index group's entry index (starts at 0).
         * @param timestamp New time UTC for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagUTCTimestamp( FIXGroup *msg , int tag , int index , const UTCTimestamp &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferUTCTimestamp ];
            return msg->set( tag , Engine::AsciiString( buf , utcTimestampToString( buf , ValueSizeBufferUTCTimestamp , timestamp , flags ) ) , index );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param grp FIXGroup
         * @param tag FIX field tag to set data to.
         * @param index group's entry index (starts at 0).
         * @param timestamp New time for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagTZTimestamp( FIXGroup *grp , int tag , int index , const TZTimestamp &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferTZTimestamp ];
            return grp->set( tag , Engine::AsciiString( buf , tzTimestampToString( buf , ValueSizeBufferTZTimestamp , timestamp , flags ) ) , index );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param grp FIXGroup
         * @param tag FIX field tag to set data to.
         * @param index group's entry index (starts at 0).
         * @param timestamp New date and time UTC for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagUTCTimeOnly( FIXGroup *grp , int tag , int index , const UTCTimeOnly &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferUTCTimeOnly ];
            return grp->set( tag , Engine::AsciiString( buf , utcTimeOnlyToString( buf , ValueSizeBufferUTCTimeOnly , timestamp , flags ) ) , index );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param grp FIXGroup
         * @param tag FIX field tag to set data to.
         * @param index group's entry index (starts at 0).
         * @param timestamp New  date and time for field.
         * @param flags Time precision flags for conversion to string representation
         *
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagTZTimeOnly( FIXGroup *grp , int tag , int index , const TZTimeOnly &timestamp , TimeFlags flags = Milliseconds )
        {
            char buf[ ValueSizeBufferTZTimeOnly ];
            return grp->set( tag , Engine::AsciiString( buf , tzTimeOnlyToString( buf , ValueSizeBufferTZTimeOnly , timestamp , flags ) ) , index );
        }

        /**
         * Updates field value by tag number. The returned value indicates previous
         * value existance, i.e if previous value wasn't empty, set returns true,
         * otherwise false.
         *
         * @param grp FIXGroup.
         * @param tag FIX field tag to set data to.
         * @param index group's entry index (starts at 0).
         * @param timestamp New date for field.
         * @return "true" if request field had value before call of set,
         * otherwise "false" (i.e. tag was inserted into message).
         *
         * @throw Utils::Exception if requested field is not defined this message type.
         */
        inline bool setTagDateOnly( FIXGroup *grp , int tag , int index , const UTCDateOnly &timestamp )
        {
            char buf[ ValueSizeBufferUTCDateOnly ];
            return grp->set( tag , Engine::AsciiString( buf , utcDateOnlyToString( buf , ValueSizeBufferUTCDateOnly , timestamp ) ) , index );
        }

        /**
         * Returns field value as UTCTimestamp.
         * @param grp FIXGroup.
         * @param[in] tag Tag of the field.
         * @param index group's entry index (starts at 0).
         */
        UTCTimestamp V12_API getAsUTCTimestamp( const FIXGroup *grp , int tag , int index );

        /**
         * Returns field value as TZTimestamp.
         * @param grp FIXGroup.
         * @param[in] tag Tag of the field.
         * @param index group's entry index (starts at 0).
         */
        TZTimestamp V12_API getAsTZTimestamp( const FIXGroup *grp , int tag , int index );

        /**
         * Returns field value as UTCTimeOnly.
         * @param grp FIXGroup.
         * @param[in] tag Tag of the field.
         * @param index group's entry index (starts at 0).
         */
        UTCTimeOnly V12_API getAsUTCTimeOnly( const FIXGroup *grp , int tag , int index );

        /**
         * Returns field value as TZTimeOnly.
         * @param grp FIXGroup.
         * @param[in] tag Tag of the field.
         * @param index group's entry index (starts at 0).
         */
        TZTimeOnly V12_API getAsTZTimeOnly( const FIXGroup *grp , int tag , int index );

        /**
         * Returns field value as UTCDateOnly.
         * @param grp FIXGroup.
         * @param[in] tag Tag of the field.
         * @param index group's entry index (starts at 0).
         */
        UTCDateOnly V12_API getAsUTCDateOnly( const FIXGroup *grp , int tag , int index );
    }

}


#endif // __B2BITS_FIXGroup_h__

