#pragma once

#include <string>
//#include <B2BITS_Mutex.h>

#include "Properties.h"
#include "ApplicationProperties.h"

/// Class Helper is a collection of useful routines.
class Helper
{
public:
    /// Loads file content into std::string.
    static std::string loadString(const std::string& fileName);

    /// Writes line to console.
    static void writeLineToConsole(const std::string& message = std::string());

    /// Writes line to stderr.
    static void writeErrorLine(const std::string& message = std::string());

    static void writeUsageInfo(const std::string& applicationName);

    static void parseCommandLine(const Aux::Properties& prop, ApplicationProperties* params);

    /// screen output synchronizer.
    //static System::Mutex outputLock_;

private:
    /// Prohibits class instances appearance.
    Helper();
};
/*
/// Application level exception.
class AppException : public std::exception
{

public:
    /// Remembers reason of exception.
    explicit AppException(const std::string& reason) throw()
        : reason_(reason) {}

    /// Destructor to satisfy C++ compliance.
    virtual ~AppException() throw() {}

    /// Reimplements std::exception interface.
    virtual const char* what() const throw()
    {
        return reason_.c_str();
    }

private:
    std::string reason_;
};
*/
