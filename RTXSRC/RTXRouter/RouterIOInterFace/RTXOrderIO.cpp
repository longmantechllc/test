//
// Created by schen on 10/5/2020.
//
#include <dlfcn.h>

#include <vector>
#include <unordered_map>
#include <memory>
#include <functional>
#include <ctime>
#include <optional>
#include <iostream>
#include <unordered_set>
//#include <mutex>

#include "../include/TopOfBook.h"
#include "../include/Bbo.h"
#include "../include/Order.h"
#include "../include/ExecutionReport.h"

#include "Config/ConfigGlobalBox.h"
#include "Config/ConfigSecIdfnsBox.h"
#include "Config/ConfigRouterBox.h"
#include "Config/ConfigUserBox.h"
#include "Config/Config.h"

#include "CPU.h"
#include "../include/App.h"
#include "Logger.h"

#include "OrderLogic/RTXOrder.h"
#include "../include/RTXOrderIO.h"
#include "../include/RTXApp.h"

RTXOrderIO::RTXOrderIO()
{

}

RTXOrderIO::RTXOrderIO(App* app)
    :m_app(app),
    m_logger(app->m_loggerSys)
{

}

RTXOrderIO::~RTXOrderIO()
{

}

//OSM call this function to send parent order to SOR
bool RTXOrderIO::StartParentOrder( Order* order )
{
//    if(m_app->m_cpu->m_marketClosed) {
//        m_logger->logInfo("Market Closed. for: %d", order->orderID_);
//        return false;
//    }
    auto ordPtr = std::make_shared<Order>(*order->Clone());
    auto ordr  = std::make_shared< RTXOrder >(ordPtr, m_app);
    int oId = order->orderID_;
    m_logger->logInfo("Received parent order: %d", oId);
    /////////
    ++m_app->m_cpu->m_oId;

    if(ordr->InitValidate() == false)
        return false;

    {
        std::lock_guard<std::mutex> lk(m_mutex);
        auto pr = std::make_pair(oId, ordr);
        orderList.insert(pr);
    }
    ordr->InitOrder();
    return true;
}

//OSM send cancel parent order to SOR
void RTXOrderIO::CancelParentOrder( const int pOrderId)
{
    RTXOrder*  rtxOrderPtr = GetParentptr(pOrderId);
    if(rtxOrderPtr)
        rtxOrderPtr->CancelParentByClient("Cancel parent order by client ");
}

//SOR send child order to OSM
void RTXOrderIO::SendChildOrder( std::shared_ptr<Order>  order )
{
    ++m_app->m_cpu->m_coNumber; /////////
    orderIO->sendChild(*order.get());
}

//SOR send cancel child order request to OSM
void RTXOrderIO::CancelChildOrderRequest( const int pOrderId, const std::string& coId )
{
    orderIO->cancelChild(coId);
}

//OSM call this function to send Execution report to SOR
void RTXOrderIO::ReceiveExecutionReport( const ExecutionReport* er)
{
//    m_logger->logErr("SOR interface ReceiveExecutionReport: oId: %d, status_: %d", er->orderId_, er->status_);
    int oId = er->orderId_;
    RTXOrder*  rtxOrderPtr = GetParentptr(oId);
    if(rtxOrderPtr) {
        ++m_app->m_cpu->m_erCnt;    /////////
        rtxOrderPtr->OnChildExecutionReport(er);
    }
}

//send Ack or fill or cancel report to client
void RTXOrderIO::SendExecutionReport( const ExecutionReport* er)
{
    orderIO->sendER(*er);
}

//send Ack to client
void RTXOrderIO::SendParentAck( const ExecutionReport* er)
{
    orderIO->sendACK(*er);
}

//inside use, remove parent order object
void RTXOrderIO::RemoveParentOrder(const int poId)
{
    std::lock_guard<std::mutex> lk(m_mutex);
    auto odr = orderList.find(poId);
    if(odr == orderList.end())
    {
        m_logger->logErr("RemoveParentOrder: the parent order: %d does not exist.", poId);
        return;
    }
    orderList.erase(poId);
}

void RTXOrderIO::EndOfDayCancelOrders(bool isPostMarketCancel)
{
    std::lock_guard<std::mutex> lk(m_mutex);
    try
    {
        std::string msg;
        if(isPostMarketCancel)
            msg = "End of post market cancel";
        else
            msg = "End of market cancel";

        for (auto order : orderList)
        {
            order.second->EndOfDayCancelParents(isPostMarketCancel, msg);
        }
    }
    catch(exception e)
    {
        m_logger->logErr("EndOfDayCancelOrders: exception: %s .", e.what());
    }
    catch (...)
    {
        m_logger->logErr("EndOfDayCancelOrders: exception...");
    }
}

RTXOrder* RTXOrderIO::GetParentptr(const int poId)
{
    std::lock_guard<std::mutex> lk(m_mutex);
    auto odr = orderList.find(poId);
    if (odr == orderList.end()) {
        m_logger->logErr("Parent order: %d does not exist.", poId);
        return nullptr;
    }
   return (odr->second.get());
}

//SOR ask OSM the exchange status
bool RTXOrderIO::IsDestinationAvailable( const std::string& destinationName )
{
    if(availabeDest.find(destinationName) != availabeDest.end())
        return true;
    return false;
}

//OSM inform SOR the exchange status change
void RTXOrderIO::UpdateDestinationAvailability( const std::string& destinationName, bool available )
{
    if(available) {
        availabeDest.insert(destinationName);
        m_logger->logInfo("Destination: %s is up", destinationName.c_str());
    }
    else {
        availabeDest.erase(destinationName);
        m_logger->logInfo("Destination: %s is down", destinationName.c_str());
    }
}

std::string RTXOrderIO::RouteAdminCmd(std::string adminCmd)
{
    std::string msg;
    return msg;
}