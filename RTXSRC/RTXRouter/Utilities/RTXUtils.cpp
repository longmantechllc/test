//
// Created by schen on 10/14/2020.
//
#include <climits>
#include <string>
#include <algorithm>
#include <ctime>

#include <climits>
#include <stdexcept>
#include <bits/stdc++.h>
#include <stdio.h>

#include "Price.h"
#include "RTXUtils.h"


double RTXUtils::StrToDouble(char* val, double deflt)
{
    std::string str = std::string(val);
    return StrToDouble( str,  deflt );
}
Price RTXUtils::StrToPrice(char* val, double deflt)
{
    std::string str = std::string(val);
    return StrToPrice( str,  deflt );
}
bool RTXUtils::StrToBool(char* val, bool deflt)
{
    std::string str = std::string(val);
    return StrToBool( str,  deflt );
}
int RTXUtils::StrToInt(char* val, int deflt)
{
    std::string str = std::string(val);
    return StrToInt( str,  deflt );
}

__int64_t RTXUtils::StrToLint(char* val, int deflt)
{
    std::string str = std::string(val);
    return StrToLint( str,  deflt );
}


double RTXUtils::StrToDouble( std::string& str, double deflt )
{
    trimString(str);

    if ( str.size() == 0 )
        return deflt;

    char* ends;
    double val = strtod( str.c_str(), &ends );
    if ( *ends != '\0' )
        return deflt;

    return val;
}

Price RTXUtils::StrToPrice( std::string& str, double deflt )
{
    Price val(StrToDouble( str, deflt ));
    return val;
}

bool RTXUtils::StrToBool( std::string& val, bool deflt )
{
    trimString(val);

    if ( val.size() == 0 )
        return deflt;

    std::transform(val.begin(), val.end(), val.begin(),  [=](unsigned char c) { return std::toupper(c); });

    if ( (val=="T") || (val == "TRUE" ) || (val == "1" ) )
        return true;
    else if ( (val=="F") || (val == "FALSE" ) || (val == "0" ) )
        return false;
    else
    {
        std::string errorText("Invalid boolean string: ");
        errorText.append( val );
        return deflt;
    }
}

bool RTXUtils::IsValidStrToBoolFormat(std::string val)
{
    std::string tempVal = val;
    trimString(tempVal);
    if ( tempVal.size() == 0 )
        return false;

    std::transform(tempVal.begin(), tempVal.end(), tempVal.begin(),  [=](unsigned char c) { return std::toupper(c); });

    if ( (tempVal=="T") || (tempVal == "TRUE" ) || (tempVal == "1" ) || (tempVal=="F") || (tempVal == "FALSE" ) || (tempVal == "0" ))
        return true;
    else
        return false;
}

int RTXUtils::StrToInt( std::string& val, int deflt )
{
    trimString(val);

    if ( val.size() == 0 )
        return deflt;

    char* ends;
    long valInt = strtol( val.c_str(), &ends, 10 /*base 10*/ );
    if ( *ends != '\0' )
        return deflt;

    return valInt;
}

__int64_t RTXUtils::StrToLint( std::string& val, int deflt )
{
    trimString(val);

    if ( val.size() == 0 )
        return deflt;

    char* ends;
    long valInt = strtoll( val.c_str(), &ends, 10 /*base 10*/ );
    if ( *ends != '\0' )
        return deflt;

    return valInt;
}

int RTXUtils::RoundToLotSize(int val, int lotSize, int upOrDown)
{
    //0:nearest, 1:down, 2:up
    if(val < lotSize)
        return lotSize;
    if(val % lotSize == 0)
        return val;

    int size = 0;
    if(upOrDown == 0)
    {
        size = ((int) (val + lotSize / 2) / lotSize) * lotSize;
        return size;
    }
    size = (int) (val / lotSize);
    if(upOrDown == 1)
        return size * lotSize;
    else
         return (size + 1) * lotSize;
}

bool RTXUtils::DoubleEqual(double val1, double val2)
{
    if(val1 > val2 - 0.0000001 && val1 < val2 + 0.0000001)
        return true;
    else
        return false;
}
bool RTXUtils::DoubleGT(double val1, double val2)
{
    return (val1 > val2 + 0.0000001);
}
bool RTXUtils::DoubleLS(double val1, double val2)
{
    return (val1 < val2 - 0.0000001);
}
bool RTXUtils::DoubleGE(double val1, double val2)
{
    return (DoubleEqual(val1, val2) || DoubleGT(val1, val2));
}
bool RTXUtils::DoubleLE(double val1, double val2)
{
    return (DoubleEqual(val1, val2) || DoubleLS(val1, val2));
}


int RTXUtils::StrToTimeSsm(const std::string& val, int deflt)
{
    if (val.length() == 0)
        return deflt;

    //HH:MM:SS to SSM (Seconds Since Midnight)
    int hh,mm,ss;
    if ( sscanf( val.c_str(), "%u:%u:%u", &hh, &mm, &ss ) != 3 )
    {
        return deflt;
    }
    return GetSsm( hh, mm , ss);
}

int RTXUtils::GetSsm( int hours, int mins, int secs )
{
    return secs + 60*mins + 60*60*hours;
}

int RTXUtils::StrToTimeMssm(const std::string& val, int deflt )
{
    if (val.length() == 0)
        return deflt;
    //HH:MM:SS.Ms to MSSM (MilliSeconds Since Midnight)
    int hh,mm,ss, ms;
    if ( sscanf( val.c_str(), "%u:%u:%u.%u", &hh, &mm, &ss, &ms ) != 4 )
    {
        return deflt;
    }
    return GetMssm( hh, mm , ss, ms);
}

int RTXUtils::GetMssm( int hours, int mins, int secs, int msecs )
{
    return ( GetSsm( hours, mins, secs) * 1000 ) + msecs;
}

int RTXUtils::NowSsm()
{
    time_t now = time(0);
    struct tm *tm = localtime(&now);

    int nowSsm = (tm->tm_hour *3600) + (tm->tm_min*60) + tm->tm_sec ;
    return nowSsm;
}

int RTXUtils::NowMssm()
{
    auto now = std::chrono::system_clock::now();

    time_t tnow = std::chrono::system_clock::to_time_t(now);
    tm *date = std::localtime(&tnow);
    date->tm_hour = 0;
    date->tm_min = 0;
    date->tm_sec = 0;
    auto midnight = std::chrono::system_clock::from_time_t(std::mktime(date));
    auto since_midnight = now-midnight;
    auto milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>( since_midnight );

    return milliseconds.count();
}
int RTXUtils::TimeDiffLocalvsUTC()
{
    std::tm* utcTm;
    std::tm* localTm;
    std::time_t t = std::time(nullptr);
    utcTm = std::gmtime(&t);
    int h; //,m,s;
    h = utcTm->tm_hour;
//    m = utcTm->tm_min;
//    s = utcTm->tm_sec;
    localTm = std::localtime(&t);

//    std::cout << "UTC: " << h << ":" << m << ":" << s << endl;
//    std::cout << "Local: " << localTm->tm_hour << ":" << localTm->tm_min << ":" << localTm->tm_sec << endl;
    return (h - localTm->tm_hour);
}

std::vector<std::string> RTXUtils::stringToVector( std::string& str, const std::string& separator )
{
    char list[256];
    std::vector<std::string> vect;

    strcpy(list, str.c_str());
    char *token = std::strtok(list, separator.c_str());
    while (token != nullptr)
    {
        std::string tk = token;
        trimString(tk);
        vect.push_back(tk);
        token = std::strtok(nullptr, separator.c_str());
    }
    return vect;
}


void RTXUtils::trimString(string& str)
{
    int len = (int)str.length();
    if (len == 0)   return;
    bool hasLeading = false, hasTrailing = false;
    if (isspace(str.at(0)))
        hasLeading = true;
    if (isspace(str.at(len-1)))
        hasTrailing = true;

    if (!hasLeading && !hasTrailing)
        return;
    int i, start=0, end = len;
    for (i=1; i < len && start<end; i++)
    {
        if (hasLeading && !isspace(str.at(i)))
        {
            start = i;
            hasLeading = false;
        }
        if (hasTrailing && start<end && !isspace(str.at(len-i-1)))
        {
            end = (len-i);
            hasTrailing = false;
        }
        if (!hasLeading && !hasTrailing)   break;
    }
    if (hasLeading && hasTrailing)
        end = start;                        //all spaces
    str = str.substr(start,end-start);

    return;
}
