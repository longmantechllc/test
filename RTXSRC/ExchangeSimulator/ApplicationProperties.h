#pragma once

#include <string>
#include <set>

/// Application parameters
struct ApplicationProperties
{
    std::string acceptor_cfg;
    std::set<std::string> litVenueList;
    int iOCOrderFillRate {20};
    int litIOCOrderFillRate {90};
    int dayOrderFillRate {15};
};

