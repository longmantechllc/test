// <copyright>
//
// $Revision: 1.3 $
//
// (c) B2BITS EPAM Systems Company 2000-2017.
// "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).
//
// This software is for the use of the paying client of B2BITS (which may be
// a corporation, business area, business unit or single user) to whom it was
// delivered (the "Licensee"). The use of this software is subject to
// license terms.
//
// The Licensee acknowledges and agrees that the Software and Documentation
// (the "Confidential Information") is confidential and proprietary to
// the Licensor and the Licensee hereby agrees to use the Confidential
// Information only as permitted by the full license  agreement between
// the two parties, to maintain the confidentiality of the Confidential
// Information and not to disclose the confidential information, or any part
// thereof, to any other person, firm or corporation. The Licensee
// acknowledges that disclosure of the Confidential Information may give rise
// to an irreparable injury to the Licensor in-adequately compensable in
// damages. Accordingly the Licensor may seek (without the posting of any
// bond or other security) injunctive relief against the breach of the forgoing
// undertaking of confidentiality and non-disclosure, in addition to any other
// legal remedies which may be available, and the licensee consents to the
// obtaining of such injunctive relief. All of the undertakings and
// obligations relating to confidentiality and non-disclosure, whether
// contained in this section or elsewhere in this agreement, shall survive
// the termination or expiration of this agreement for a period of five (5)
// years.
//
// The Licensor agrees that any information or data received from the Licensee
// in connection with the performance of the support agreement relating to this
// software shall be confidential, will be used only in connection with the
// performance of the Licensor's obligations hereunder, and will not be
// disclosed to third parties, including contractors, without the Licensor's
// express permission in writing.
//
// Information regarding the software may be provided to the Licensee's outside
// auditors and attorneys only to the extent required by their respective
// functions.
//
// </copyright>

/// @file B2BITS_CSRefCounterPtr.h
/// Contains FixDictionary2::CSRefCounterPtr class declaration.
/// @author "Dmytro Ovdiienko" [dmytro_ovdiienko@epam.com] [odp@btobits.com]

#ifndef H_B2BITS_FixDictionary2_CSRefCounterPtr_H
#define H_B2BITS_FixDictionary2_CSRefCounterPtr_H

#include <cassert>
#include <cstddef>

#include <B2BITS_V12_Defines.h>

#if _MSC_VER >= 1500
#   pragma warning(push)
#   pragma warning(disable: 4481)
#endif

/// Contains classes to work with FIX XML protocol files.
namespace FixDictionary2
{

    /// Const sensitive pointer
    template<class T> class CSRefCounterPtr
    {
    private:
        /** pointee */
        T* ptr_;

    public:
        typedef CSRefCounterPtr<T> MyT;

        /** Dereference operator */
        T const& operator*() const throw() {
            assert( NULL != this->ptr_ );
            return *this->ptr_;
        }

        /** Dereference operator */
        T& operator*() throw() {
            assert( NULL != this->ptr_ );
            return *this->ptr_;
        }

        /** Selector operator */
        T const* operator->() const throw() {
            assert( NULL != this->ptr_ );
            return this->ptr_;
        }

        /** Selector operator */
        T* operator->() throw() {
            assert( NULL != this->ptr_ );
            return this->ptr_;
        }

        /** Assign operator */
        CSRefCounterPtr& operator=( CSRefCounterPtr const& rv ) throw() {
            this->reset( rv.ptr_ );
            return *this;
        }

        /** 
        * Releases managed object and assigns new one.
        */
        void reset( T* apI = NULL, bool addRef = true ) throw() {
            if( NULL != apI && addRef ) {
                apI->addRef();
            }

            if( NULL != ptr_ ) {
                ptr_->release();
            }

            this->ptr_ = apI;
        }

        /** 
        * Returns pointer to the managed object.
        */
        T const* get() const throw() {
            return ptr_;
        }

        /** 
        * Returns pointer to the managed object.
        */
        T* get() throw() {
            return ptr_;
        }

        /** 
        * Returns pointer to the managed object and its ownership.
        */
        T* release() throw() {
            T* result = ptr_;
            ptr_ = NULL;

            return result;
        }

    public:
        /** Default Constructor. */
        CSRefCounterPtr() throw()
            : ptr_( NULL ) {
        }

        /** Constructor. */
        explicit CSRefCounterPtr( T* apV ) throw()
            : ptr_( apV ) {
        }

        /** Constructor. */
        CSRefCounterPtr( T* apV, bool increaseRefCounter ) throw()
            : ptr_( apV ) {
            if( NULL != apV && increaseRefCounter ) {
                apV->addRef();
            }
        }

        /** Copy constuctor */
        CSRefCounterPtr( CSRefCounterPtr const& aV ) throw()
            : ptr_( aV.ptr_ ) {
            if( NULL != aV.ptr_ ) {
                //const_cast<T*>(aV.ptr_)->addRef();
                aV.ptr_->addRef();
            }
        }

        /** Another copy constructor. */
        template< typename T2 >
        CSRefCounterPtr( CSRefCounterPtr<T2> const& other ) throw()
            : ptr_( other.get() ) {
            if( NULL != ptr_ ) {
                ptr_->addRef();
            }
        }

        /** Destructor */
        ~CSRefCounterPtr() throw() {
            if( NULL != this->ptr_ ) {
                //const_cast<T*>(this->ptr_)->release();
                this->ptr_->release();
                this->ptr_ = NULL;
            }
        }
    };


} //namespace FixDictionary2

#if _MSC_VER >= 1500
#   pragma warning(pop)
#endif

#endif //H_B2BITS_FixDictionary2_CSRefCounterPtr_H
