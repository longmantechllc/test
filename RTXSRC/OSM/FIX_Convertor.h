#pragma once
#include "quickfix/fix44/NewOrderSingle.h"
#include "quickfix/fix44/ExecutionReport.h"
#include "quickfix/fix44/OrderCancelRequest.h"
#include "OSMOrder.h"
#include "Order.h"
#include "ExecutionReport.h"

class FIX_Convertor //: public  Convertor
{
public:
    // from client order to OSM
    static void toOSM(const FIX44::NewOrderSingle& msg, OSMOrder& ord);

    // order from sor to gw
    static void fromRTX(const Order& from, FIX44::NewOrderSingle& to, const VirtualDestMap& virtualDestMap);
    // ER from sor to client
    static void fromRTX(const ExecutionReport& from, FIX44::ExecutionReport& to);

    // OSM to gw child order
    //static void fromOSM(const OSMOrder& from, FIX44::NewOrderSingle& to);
    static void fromOSM(const OSMOrder& from, FIX44::OrderCancelRequest& to);
    // Client to SOR
    //static void toRTX(const FIX::Message& from, Order& to);
    // GW child to SOR
    static void toRTX(const FIX44::ExecutionReport& from, ExecutionReport& to);

    template<typename T>
    static auto getField(const auto& msg) {
        T field;
        msg.get(field);
        return field;
    };

    template<typename T>
    static auto getFieldValue(const auto& msg) {
        T field;
        msg.get(field);
        return field.getValue();
    };
    template<typename T>
    static auto getFieldValueIf(const auto& msg, auto& var) {
        T field;
        if (msg.isSetField(field)) {
            msg.get(field);
            var = field.getValue();
        }
    };

};

