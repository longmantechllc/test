/*
  <copyright>

  $Revision: 1.3 $

  (c) B2BITS EPAM Systems Company 2000-2017.
  "Licensor" shall mean B2BITS EPAM Systems Company (B2BITS).

  This software is for the use of the paying client of B2BITS (which may be
  a corporation, business area, business unit or single user) to whom it was
  delivered (the "Licensee"). The use of this software is subject to
  license terms.

  The Licensee acknowledges and agrees that the Software and Documentation
  (the "Confidential Information") is confidential and proprietary to
  the Licensor and the Licensee hereby agrees to use the Confidential
  Information only as permitted by the full license agreement between
  the two parties, to maintain the confidentiality of the Confidential
  Information and not to disclose the confidential information, or any part
  thereof, to any other person, firm or corporation. The Licensee
  acknowledges that disclosure of the Confidential Information may give rise
  to an irreparable injury to the Licensor in-adequately compensable in
  damages. Accordingly the Licensor may seek (without the posting of any
  bond or other security) injunctive relief against the breach of the forgoing
  undertaking of confidentiality and non-disclosure, in addition to any other
  legal remedies which may be available, and the licensee consents to the
  obtaining of such injunctive relief. All of the undertakings and
  obligations relating to confidentiality and non-disclosure, whether
  contained in this section or elsewhere in this agreement, shall survive
  the termination or expiration of this agreement for a period of five (5)
  years.

  The Licensor agrees that any information or data received from the Licensee
  in connection with the performance of the support agreement relating to this
  software shall be confidential, will be used only in connection with the
  performance of the Licensor's obligations hereunder, and will not be
  disclosed to third parties, including contractors, without the Licensor's
  express permission in writing.

  Information regarding the software may be provided to the Licensee's outside
  auditors and attorneys only to the extent required by their respective
  functions.

  </copyright>
*/


#ifndef __Auxiliary_h__
#define __Auxiliary_h__

#include <string>

#include <B2BITS_V12.h>
#include <B2BITS_Mutex.h>

namespace Benchmark
{

    /// Returns the current time in milliseconds.
    unsigned long getCurrentTime();

    /// converts integer into the string
    std::string i2str( int aV );

#ifdef  _MSC_VER
#  pragma warning(push)
#  pragma warning(disable:4512)
#endif //  _MSC_VER
    /**
     * Auxiliary class for Mutex
     */
    class MGuard
    {
    public:

        MGuard( System::Mutex& aLock ) : m_lock( aLock ) {
            m_lock.lock();
        }
        ~MGuard() {
            m_lock.unlock();
        }

    private:

        System::Mutex& m_lock;
    };
#ifdef  _MSC_VER
#  pragma warning(pop)
#endif //  _MSC_VER

    /**
     * Other useful methods.
     */
    class Auxiliary
    {
    public:
        /**
         * Converts the given string to the corresponding FIX version.
         *
         * @return Engine::NA if input string cannot be converted.
         */
        static Engine::FIXVersion string2version( const std::string& aFV );

        /**
          * Converts the given string to the corresponding storage type constant.
          */
        static Engine::MessageStorageType string2storageType( const std::string& st );

        /**
         * Prints notification message to the console
         */
        static void notification( const std::string& aMsg );

        /**
         * Prints warning message to the console
         */
        static void warning( const std::string& aMsg );

        /**
         * Prints error message to the console
         */
        static void error( const std::string& aMsg );

    private:
        static System::Mutex m_lock;
    };

}

#endif // __Auxiliary_h__

