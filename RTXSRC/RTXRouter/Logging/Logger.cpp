//
// Created by schen on 10/4/2020.
//
#include <iostream>
#include <stdio.h>
#include <ctime>
#include <chrono>
#include <cstring>
#include <stdarg.h>
#include <thread>
#include <sys/time.h>

#include "Logger.h"

using std::chrono::system_clock;

Logger::Logger()
{

}

Logger::~Logger()
{
    m_stream.flush();
    m_stream.close();
}

void Logger::OpenFile()
{
    std::string fullName = m_filePath + '/' + m_fileName;
    m_stream.open(fullName.c_str(), std::ios_base::app | std::ios_base::out);

    if (m_flushAlways)
        m_stream << std::unitbuf;
    else
        m_stream << std::nounitbuf; //batch mode
    m_fileSize = 0;
}

void Logger::logDebug(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    log(AppConstants::LEVEL_DEBUG, fmt, args);
}

void Logger::logInfo(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    log(AppConstants::LEVEL_INFO, fmt, args);
}

void Logger::logWarn(const char* fmt, ...) {
    va_list args;
    va_start(args, fmt);

    log(AppConstants::LEVEL_WARN, fmt, args);
}

void Logger::logErr(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    log(AppConstants::LEVEL_ERROR, fmt, args);
}

void Logger::logFatal(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    log(AppConstants::LEVEL_FATAL, fmt, args);
}

void Logger::logDirect(const char* fmt, ...)
{
    va_list args;
    va_start(args, fmt);

    m_printTime = false;
    log(AppConstants::LEVEL_INFO, fmt, args);
    m_printTime = true;
}

void Logger::log(int level, const char* fmt, va_list& args)
{
    char message[AppConstants::LOG_BUFSIZE] ;
    char lineBuffer[AppConstants::LOG_BUFSIZE];
    unsigned int sizeWritten = 0;

    if(level >= Threshold())
    {
        try
        {
            memset(message, 0, AppConstants::LOG_BUFSIZE);
            memset(lineBuffer, 0, AppConstants::LOG_BUFSIZE);
            vsnprintf (message, sizeof(message) - 1, fmt, args);

            if(m_printTime)
            {
                sizeWritten = formatLogLine(message, lineBuffer, AppConstants::LOG_BUFSIZE);
            }
            else
            {
                memcpy(lineBuffer, message, sizeof(message));
                sizeWritten = sizeof(message);
            }

        }
        catch(...)
        {
            sprintf(lineBuffer, "*****exception while formatting log message in Logger::log()*****\n") ;
        }

        if (sizeof(lineBuffer) > 0 && lineBuffer[sizeWritten-1] != '\n')
        {
            if (sizeWritten < (AppConstants::LOG_BUFSIZE-1))
            {
                lineBuffer[sizeWritten] = '\n';
                ++sizeWritten;
            }
            else
                lineBuffer[sizeWritten-1] = '\n';
        }
        m_stream << lineBuffer;
        m_fileSize += sizeWritten;
        if (m_fileMaxSize>0 && m_fileSize>m_fileMaxSize)
        {
            moveToNextLogFile();
        }

        return ;
    }
}

unsigned int Logger::formatLogLine(const char* message, char* lineBuffer, int maxBufferSize)
{
    auto threadId = std::this_thread::get_id();
    int lengthOfBuffer = 0;

//    char buffer[80];
//    auto currentTime = std::chrono::system_clock::now();
//    auto transformed = currentTime.time_since_epoch().count() / 1000;
//    auto micros = transformed % 1000000;
//    time_t tt;
//    tt = system_clock::to_time_t ( currentTime );
//    auto timeinfo = localtime (&tt);
//    strftime (buffer,80,"%H:%M:%S",timeinfo);
//
//    std::memset(lineBuffer, 0, maxBufferSize);
//    lengthOfBuffer = sprintf(lineBuffer, "%s:%03d.%ld - %s", buffer,(int)micros, threadId, message);

    time_t now = time(0);
    struct tm* tm = localtime(&now);
    struct timeval tv;
    gettimeofday(&tv, 0);

    std::memset(lineBuffer, 0, maxBufferSize);
    lengthOfBuffer = sprintf(lineBuffer, "%02d:%02d:%02d.%06ld.%ld - %s",tm->tm_hour, tm->tm_min, tm->tm_sec, tv.tv_usec, threadId, message);

    if ( lengthOfBuffer < 0 )
    {
        lineBuffer[maxBufferSize - 1] = '\0';
    }

    return strlen(lineBuffer);
}

void Logger::setMaximumFileSize( unsigned long maxFileSize )
{

}

void Logger::moveToNextLogFile()
{

}
